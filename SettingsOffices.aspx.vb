
Partial Class SettingsOffices
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabSettings As Panel = Master.FindControl("pnlTabSettings")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Offices As Panel = Master.FindControl("pnlTab2Offices")

        pnlMasterTabSettings.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Settings
        pnlMasterTab2Offices.CssClass = "tab2 tab2On"

        If Not Permissions.SettingsOffices(Session("User_ID")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            btnAddOffice.ToolTip = "header=[Add New Office] body=[]"
            CreateOfficeList()
        End If
    End Sub

    Protected Sub CreateOfficeList()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Office_ID, Name FROM Offices WHERE Client_ID = @Client_ID ORDER BY Name ASC"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        rptOfficeList.DataSource = objDR
        rptOfficeList.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptOfficeList_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblOfficeName As Label = e.Item.FindControl("lblOfficeName")
            Dim imgEdit As ImageButton = e.Item.FindControl("imgEdit")
            Dim imgDelete As ImageButton = e.Item.FindControl("imgDelete")
            Dim lblDeleteOfficeName As Label = e.Item.FindControl("lblDeleteOfficeName")
            Dim btnConfirmDelete As Button = e.Item.FindControl("btnConfirmDelete")
            Dim btnCancelDelete As Button = e.Item.FindControl("btnCancelDelete")
            Dim txtEditName As TextBox = e.Item.FindControl("txtEditName")
            Dim btnEditSave As ImageButton = e.Item.FindControl("btnEditSave")
            Dim btnEditCancel As ImageButton = e.Item.FindControl("btnEditCancel")


            lblOfficeName.Text = e.Item.DataItem("Name")
            imgEdit.CommandArgument = e.Item.DataItem("Office_ID")
            imgEdit.ToolTip = "offsetx=[-150] header=[Edit " & lblOfficeName.Text & "] body=[]"
            imgDelete.CommandArgument = e.Item.DataItem("Office_ID")
            imgDelete.ToolTip = "offsetx=[-150] header=[Delete " & lblOfficeName.Text & "] body=[]"

            lblDeleteOfficeName.Text = e.Item.DataItem("Name")

            btnConfirmDelete.CommandName = "ConfirmDelete"
            btnConfirmDelete.CommandArgument = e.Item.DataItem("Office_ID")

            btnCancelDelete.CommandName = "CancelDelete"
            btnCancelDelete.CommandArgument = e.Item.DataItem("Office_ID")

            txtEditName.Text = e.Item.DataItem("Name")

            btnEditSave.CommandArgument = e.Item.DataItem("Office_ID")
            btnEditSave.ToolTip = "offsetx=[-150] header=[Save Changes] body=[]"

            btnEditCancel.CommandArgument = e.Item.DataItem("Office_ID")
            btnEditCancel.ToolTip = "offsetx=[-150] header=[Cancel Changes] body=[]"

        End If
    End Sub

    Protected Sub rptOfficeList_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim intOffice_ID As Int64 = e.CommandArgument
        Dim pnlName As Panel = e.Item.FindControl("pnlName")
        Dim pnlConfirmDelete As Panel = e.Item.FindControl("pnlConfirmDelete")
        Dim pnlEdit As Panel = e.Item.FindControl("pnlEdit")
        Dim pnlNormalButtons As Panel = e.Item.FindControl("pnlNormalButtons")
        Dim pnlEditButtons As Panel = e.Item.FindControl("pnlEditButtons")
        Dim txtEditName As TextBox = e.Item.FindControl("txtEditName")
        Dim lblEditMessage As Label = e.Item.FindControl("lblEditMessage")

        lblEditMessage.Visible = False

        Select Case e.CommandName
            Case "OfficeEdit"
                pnlName.Visible = False
                pnlEdit.Visible = True
                pnlConfirmDelete.Visible = False
                pnlNormalButtons.Visible = False
                pnlEditButtons.Visible = True

            Case "OfficeDelete"
                pnlName.Visible = False
                pnlEdit.Visible = False
                pnlConfirmDelete.Visible = True
                pnlNormalButtons.Visible = True
                pnlEditButtons.Visible = False
            Case "CancelDelete"
                pnlName.Visible = True
                pnlEdit.Visible = False
                pnlConfirmDelete.Visible = False
                pnlNormalButtons.Visible = True
                pnlEditButtons.Visible = False

            Case "ConfirmDelete"
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "DELETE FROM Offices WHERE Client_ID = @Client_ID AND Office_ID = @Office_ID"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objCommand.Parameters.AddWithValue("@Office_ID", intOffice_ID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "UPDATE Users SET Office_ID = 0 WHERE Office_ID = @Office_ID AND Client_ID = @Client_ID"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objCommand.Parameters.AddWithValue("@Office_ID", intOffice_ID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                CreateOfficeList()
            Case "EditCancel"
                txtEditName.Text = Offices.OfficeName(intOffice_ID)

                pnlName.Visible = True
                pnlEdit.Visible = False
                pnlConfirmDelete.Visible = False
                pnlNormalButtons.Visible = True
                pnlEditButtons.Visible = False
            Case "EditSave"
                Dim boolSave As Boolean = True

                If txtEditName.Text = "" Then
                    boolSave = False
                    lblEditMessage.Visible = True
                    lblEditMessage.Text = "Please choose a name"
                    lblEditMessage.ForeColor = Drawing.Color.Red
                Else
                    If Not checkNameAvailability(txtEditName.Text, intOffice_ID) Then
                        boolSave = False
                        lblEditMessage.Visible = True
                        lblEditMessage.Text = "This name is already being used"
                        lblEditMessage.ForeColor = Drawing.Color.Red
                    End If
                End If

                If boolSave Then
                    Dim objCommand As System.Data.SqlClient.SqlCommand
                    Dim objConnection As System.Data.SqlClient.SqlConnection
                    objConnection = New System.Data.SqlClient.SqlConnection

                    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                    objConnection.Open()
                    objCommand = New System.Data.SqlClient.SqlCommand()
                    objCommand.Connection = objConnection
                    objCommand.CommandText = "UPDATE Offices SET Name = @Name WHERE Office_ID = @Office_ID"
                    objCommand.Parameters.AddWithValue("@Name", txtEditName.Text)
                    objCommand.Parameters.AddWithValue("@Office_ID", intOffice_ID)

                    objCommand.ExecuteNonQuery()

                    objCommand = Nothing
                    objConnection.Close()
                    objConnection = Nothing

                    CreateOfficeList()
                End If
        End Select
    End Sub

    Protected Sub btnAddOffice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddOffice.Click
        If txtOfficeName.Text <> "" Then
            If checkNameAvailability(txtOfficeName.Text) Then
                Dim objCommandAdd As System.Data.SqlClient.SqlCommand
                Dim objConnectionAdd As System.Data.SqlClient.SqlConnection
                objConnectionAdd = New System.Data.SqlClient.SqlConnection

                objConnectionAdd.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnectionAdd.Open()
                objCommandAdd = New System.Data.SqlClient.SqlCommand()
                objCommandAdd.Connection = objConnectionAdd
                objCommandAdd.CommandText = "INSERT INTO Offices ([Client_ID], [Name]) VALUES (@Client_ID, @Name)"
                objCommandAdd.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objCommandAdd.Parameters.AddWithValue("@Name", txtOfficeName.Text)

                objCommandAdd.ExecuteNonQuery()

                objCommandAdd = Nothing
                objConnectionAdd.Close()
                objConnectionAdd = Nothing

                txtOfficeName.Text = ""

                'display success message
                lblAddMessage.Visible = True
                lblAddMessage.ForeColor = Drawing.Color.Green
                lblAddMessage.Text = "Office Successfully Added"

                CreateOfficeList()

            Else
                'same name, display error
                lblAddMessage.Visible = True
                lblAddMessage.ForeColor = Drawing.Color.Red
                lblAddMessage.Text = "Error: There is already an office named " & txtOfficeName.Text
            End If
        End If
    End Sub

    Protected Function checkNameAvailability(ByVal strOfficeName As String, Optional ByVal intEditOfficeId As Int64 = 0)
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        Dim strSQLQuery As String

        If intEditOfficeId = 0 Then
            strSQLQuery = "SELECT Office_ID FROM Offices WHERE [Name] = @Name AND Client_ID = @Client_ID"
        Else
            strSQLQuery = "SELECT [Office_ID] FROM [Offices] WHERE [Name] = @Name AND [Client_ID] = @Client_ID AND [Office_ID] <> @Office_ID"
        End If

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection

        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@Office_ID", intEditOfficeId)
        objCommand.Parameters.AddWithValue("@Name", strOfficeName)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        If objDR.HasRows Then
            checkNameAvailability = False
        Else
            checkNameAvailability = True
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Function
End Class