
Imports System.Net.Mail
Imports System.Text.RegularExpressions

Partial Class Utility_2PM_AccountExpires
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Client_ID, NextBillingDate, First_name, Email, Subdomain FROM Clients WHERE Trial = 1 AND Enabled = 1 AND AcctVerified = 1"
        objDR = objCommand.ExecuteReader()

        Dim boolSendEmail As Boolean
        Dim intType As Integer

        While objDR.Read
            boolSendEmail = False

            If DateDiff(DateInterval.Day, Now(), objDR("NextBillingDate")) = 1 Then
                intType = 1
                boolSendEmail = True
            ElseIf DateDiff(DateInterval.Day, Now(), objDR("NextBillingDate")) = 7 Then
                intType = 7
                boolSendEmail = True
            ElseIf DateDiff(DateInterval.Day, Now(), objDR("NextBillingDate")) = 0 Then
                intType = -1
                boolSendEmail = True
            End If

            If (boolSendEmail And (Not String.IsNullOrEmpty(objDR("Email"))) And EmailAddressCheck(objDR("Email"))) Then
                SendEmail(objDR("Client_ID"), intType, objDR("Email"), objDR("First_name"), objDR("Subdomain"))
            End If
        End While

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub SendEmail(ByVal intClientID As Int64, ByVal intType As Integer, ByVal strEmail As String, ByVal strFirstName As String, ByVal strSubdomain As String)
        Trace.Write("AccountExpires.aspx", "Emailing Client " & intClientID & " for type " & intType)

        Dim strSubjectLine As String

        Dim strCommonBody1 As String
        Dim strCommonBody2 As String
        Dim strCommonBody3 As String

        Dim strCommonClose1 As String
        Dim strCommonClose2 As String
        Dim strCommonClose3 As String

        Dim strPlainBody As String
        Dim strHTMLBody As String

        'get the username to put in the bottom box
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Username FROM Users WHERE Client_ID = @Client_ID AND Email = @Email"
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommand.Parameters.AddWithValue("@Email", strEmail)
        objDR = objCommand.ExecuteReader()

        Dim strUsername As String

        If objDR.Read Then
            strUsername = objDR("Username")
            Trace.Write("AccountExpires.aspx", "Username set to " & strUsername)
        Else
            strUsername = "Check Welcome Email"
            Trace.Warn("AccountExpires.aspx", "Error: Username could not be located using Email Address (" & strEmail & ") and Client_ID (" & intClientID & ")")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If intType = 7 Then
            strSubjectLine = "TimeClockWizard trial check-up"

            strCommonBody1 = "Hello " & strFirstName & ","
            strCommonBody2 = "Our team just wanted to check in on your experience with TimeClockWizard and remind you that your trial expires in one week." & vbCrLf
            strCommonBody2 = strCommonBody2 & "If you are ready to activate, please login into your account, click on the Settings tab, and then 'My Subscription' and enter your credit card information."
            strCommonBody3 = "If you have any questions, do not hesitate to contact us.  Our phone number and email address is listed below."

            Trace.Write("AccountExpires.aspx", "Text set as expiring in a WEEK")
        ElseIf intType = 1 Then
            strSubjectLine = "Reminder: your TimeClockWizard trial expires tomorrow"

            strCommonBody1 = "Hello " & strFirstName & ","
            strCommonBody2 = "Your TimeClockWizard trial expires tomorrow.  For uninterrupted service, login into your account, click on the Settings tab, and then 'My Subscription' and enter your credit card information."
            strCommonBody3 = "If you have any questions, do not hesitate to contact us.  Our phone number and email address is listed below.  We look forward to continue serving you!"

            Trace.Write("AccountExpires.aspx", "Text set as expiring TOMORROW")
        ElseIf intType = -1 Then
            strSubjectLine = "Your TimeClockWizard trial expired. Please activate your account."

            strCommonBody1 = "Hello " & strFirstName & ","
            strCommonBody2 = "Your TimeClockWizard trial expired. Please login into your account to activate, click on the Settings tab, and then 'My Subscription' and enter your credit card information."
            strCommonBody3 = "If you have any questions, do not hesitate to contact us.  Our phone number and email address is listed below.  We look forward to continue serving you!"

            Trace.Write("AccountExpires.aspx", "Text set as expired")
        End If

        strCommonClose1 = "Sincerely,"
        strCommonClose2 = "TimeClockWizard Support"
        strCommonClose3 = "866.627.7083 / info@TimeClockWizard.com"

        strPlainBody = strCommonBody1 & vbCrLf & vbCrLf
        strPlainBody &= strCommonBody2 & vbCrLf & vbCrLf
        strPlainBody &= strCommonBody3 & vbCrLf & vbCrLf
        strPlainBody &= strCommonClose1 & vbCrLf
        strPlainBody &= strCommonClose2 & vbCrLf
        strPlainBody &= strCommonClose3 & vbCrLf & vbCrLf
        strPlainBody &= "----------------------------------------" & vbCrLf
        strPlainBody &= "Your TimeClockWizard Account Information" & vbCrLf & vbCrLf
        strPlainBody &= "Login URL: http://" & strSubdomain.ToLower & ".TimeClockWizard.com" & vbCrLf
        strPlainBody &= "Your Username: " & strUsername.ToLower & vbCrLf
        strPlainBody &= "Your Password: (not shown for security purposes)" & vbCrLf & vbCrLf
        strPlainBody &= "Your Free Trial Expires On " & DateAdd(DateInterval.Day, intType, Date.Today).ToLongDateString & vbCrLf
        strPlainBody &= "----------------------------------------"

        strHTMLBody = "<html><head><title>" & strSubjectLine & "</title>"
        strHTMLBody &= "<style type='text/css'>"
        strHTMLBody &= "body {margin:10px;margin-top:0px;padding:0px;background-color:#FFF;font-family:Verdana;font-size:12px;color:#666;}"
        strHTMLBody &= "</style></head>"
        strHTMLBody &= "<body><img src=""cid:ciiLogo""><br><br>"
        strHTMLBody &= strCommonBody1 & "<br><br>"
        strHTMLBody &= strCommonBody2 & "<br><br>"
        strHTMLBody &= strCommonBody3 & "<br><br>"
        strHTMLBody &= strCommonClose1 & "<br>"
        strHTMLBody &= strCommonClose2 & "<br>"
        strHTMLBody &= strCommonClose3 & "<br><br>"
        strHTMLBody &= "<div style='background-color:#EEE;padding:3px;border:1px solid #CCC;'>"
        strHTMLBody &= "<b>Your TimeClockWizard Account Information</b><br><br>"
        strHTMLBody &= "<b>Login URL:</b> <a href='http://" & strSubdomain.ToLower & ".TimeClockWizard.com'>http://" & strSubdomain.ToLower & ".TimeClockWizard.com</a><br>"
        strHTMLBody &= "<b>Your Username:</b> " & strUsername.ToLower & "<br>"
        strHTMLBody &= "<b>Your Password:</b> (not shown for security purposes)<br><br>"
        strHTMLBody &= "Your Free Trial Expires On " & DateAdd(DateInterval.Day, intType, Date.Today).ToLongDateString
        strHTMLBody &= "</div>"
        strHTMLBody &= "</body></html>"

        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
        mail.Subject = strSubjectLine
        mail.To.Add(strEmail.ToLower)

        'set the content
        Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString(strPlainBody, Nothing, "text/plain")
        Dim HTMLView As AlternateView = AlternateView.CreateAlternateViewFromString(strHTMLBody, Nothing, "text/html")

        Try
            Dim logo As New LinkedResource("C:\Inetpub\wwwroot\TimeClockWizard.com\ClockitinSite\Images\emaillogo2.jpg", "image/jpeg")
            logo.ContentId = "ciiLogo"
            HTMLView.LinkedResources.Add(logo)
        Catch ex As Exception

        End Try

        mail.AlternateViews.Add(plainView)
        mail.AlternateViews.Add(HTMLView)

        'send the message
        Dim smtp As New SmtpClient()
        Trace.Write("AccountExpires.aspx", "Email sent to " & strEmail)
        smtp.Send(mail)
    End Sub
    Protected Function EmailAddressCheck(ByVal emailAddress As String) As Boolean
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
        If emailAddressMatch.Success Then
            EmailAddressCheck = True
        Else
            EmailAddressCheck = False
        End If
    End Function
End Class
