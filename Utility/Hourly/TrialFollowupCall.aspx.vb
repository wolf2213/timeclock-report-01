
Imports System.Net.Mail

Partial Class Utility_2PM_AccountExpires
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Client_ID, CompanyName, First_name, Last_name, Phone, SignedUp, Subdomain FROM Clients WHERE Trial = 1 AND Enabled = 1 AND AcctVerified = 1 AND Phone IS NOT NULL"
        objDR = objCommand.ExecuteReader()

        Dim intEmailto As String 'refers to which email address to send to (me or jeremy)

        'this block of code chooses who gets the first email, changes each day
        If Date.Now.Day Mod 2 = 0 Then
            intEmailto = 1
        Else
            intEmailto = 2
        End If

        While objDR.Read
            Dim intDifference As Integer = DateDiff(DateInterval.Hour, objDR("SignedUp"), Now())
            Trace.Write("TrialFollowUpCall.aspx", "Loop, Client Id = " & objDR("Client_ID") & " hours ago = " & intDifference)
            If intDifference = 2 Then
                SendEmail(objDR("Client_ID"), objDR("CompanyName"), objDR("First_name") & " " & objDR("Last_name"), objDR("SignedUp"), objDR("Phone"), objDR("Subdomain"), intEmailto Mod 2)
            End If
            intEmailto += 1 'email the next person
        End While

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub SendEmail(ByVal intClientID As Integer, ByVal strCompanyName As String, ByVal strFullName As String, ByVal strSignedUp As String, ByVal strPhoneNumber As String, ByVal strSubdomain As String, ByVal intEmailTo As Integer)
        Trace.Write("TrialFollowUpCall.aspx", "Emailing for Client ID " & intClientID & ", signed up on " & strSignedUp)

        Dim strSubjectLine As String

        Dim strPlainBody As String

        strSubjectLine = "TimeClockWizard Follow up Call"

        strPlainBody = strFullName & " at '" & strCompanyName & "' (" & intClientID & ")" & vbCrLf
        strPlainBody &= "Phone: " & strPhoneNumber & vbCrLf
        strPlainBody &= "Signed Up: " & strSignedUp & vbCrLf
        strPlainBody &= "Subdomain: " & strSubdomain & vbCrLf
        strPlainBody &= "http://boss.TimeClockWizard.com/findclient.aspx?id=" & intClientID

        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
        mail.Subject = strSubjectLine

        If intEmailTo = 0 Then
            mail.To.Add("info@TimeClockWizard.com")
        Else
            mail.To.Add("info@TimeClockWizard.com")
        End If


        'set the content
        Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString(strPlainBody, Nothing, "text/plain")

        mail.AlternateViews.Add(plainView)

        'send the message
        Dim smtp As New SmtpClient()
        Trace.Write("TrialFollowUpCall.aspx", "Email sent")
        smtp.Send(mail)
    End Sub
End Class
