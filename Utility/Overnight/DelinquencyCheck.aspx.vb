Imports System.Net.mail

Partial Class Overnight_DelinquencyCheck
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'this disables people that were attempted to be billed a week ago and still haven't paid
        Trace.Write("DelinquencyCheck.aspx", "Begin Delinquency Check")
        Trace.Write("DelinquencyCheck.aspx", "Date to Check = " & CStr(DateAdd(DateInterval.Day, -7, Now.Date)) & " 12:00 AM" & " - " & CStr(DateAdd(DateInterval.Day, -7, Now.Date)) & " 11:59 PM")

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "EXEC sprocBillFindDelinquentClients @StartTime, @EndTime"
        objCommand.Parameters.AddWithValue("@StartTime", CStr(DateAdd(DateInterval.Day, -7, Now.Date)) & " 12:00 AM")
        objCommand.Parameters.AddWithValue("@EndTime", CStr(DateAdd(DateInterval.Day, -7, Now.Date)) & " 11:59 PM")
        objDR = objCommand.ExecuteReader()

        While objDR.Read
            Trace.Write("DelinquencyCheck.aspx", "Processing (Disabling) Client_ID = " & objDR("Client_ID"))
            Dim objCommandClient As System.Data.SqlClient.SqlCommand
            Dim objConnectionClient As System.Data.SqlClient.SqlConnection

            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            objCommandClient.CommandText = "UPDATE Clients SET Enabled = 0 WHERE Client_ID = @Client_ID"
            objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
            objCommandClient.ExecuteNonQuery()

            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing

            Dim objDRClient As System.Data.SqlClient.SqlDataReader
            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            objCommandClient.CommandText = "SELECT Email FROM Clients WHERE Client_ID = @Client_ID"
            objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
            objDRClient = objCommandClient.ExecuteReader()

            Dim mailClient As New MailMessage()
            mailClient.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Billing")
            mailClient.Subject = "TimeClockWizard Account Disabled"

            objDRClient.Read()
            'add the recipient
            Trace.Write("DelinquencyCheck.aspx", "Mail Warning to: " & objDRClient("Email"))
            mailClient.To.Add(objDRClient("Email"))

            'set the content
            mailClient.Body = "Your TimeClockWizard account has been disabled because your billing information was not updated.  Please update your billing information to regain access to your account.<br><br>If you have any questions, you can reply to this email or call us at (877) 646-4446.<br><br>Thank you,<br>TimeClockWizard Support"
            mailClient.IsBodyHtml = True

            'send the message
            Dim smtpClient As New SmtpClient()
            smtpClient.Send(mailClient)

            objDRClient.Close()
            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing
        End While

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
        Trace.Write("DelinquencyCheck.aspx", "End Delinquency Check")
    End Sub
End Class
