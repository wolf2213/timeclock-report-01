Imports System.Net.mail
Imports System.Net


Partial Class Overnight_RecurringTransactions
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Trace.Write("RecurringTransactions.aspx", "Begin Execution")

        'go through the DB, select who needs to be billed
        Trace.Write("RecurringTransactions.aspx", "Select Client IDs to be billed and loop through them")
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Client_ID, CompanyName FROM Clients WHERE (NextBillingDate BETWEEN @BillStart AND @BillEnd) AND CCType <> 0 AND Enabled = 1"
        objCommand.Parameters.AddWithValue("@BillStart", CDate(CStr(Now.Date) & " 12:00 AM"))
        objCommand.Parameters.AddWithValue("@BillEnd", CDate(CStr(Now.Date) & " 11:59 PM"))

        'leave these commented so that we can use them to run specific client billing manually
        'objCommand.CommandText = "SELECT Client_ID, CompanyName FROM Clients WHERE client_id = 3585 and (NextBillingDate BETWEEN @BillStart AND @BillEnd) AND CCType <> 0 AND Enabled = 1"
        'objCommand.Parameters.AddWithValue("@BillStart", "11/05/2013 12:00 AM")
        'objCommand.Parameters.AddWithValue("@BillEnd", "12/04/2013 11:59 PM")

        Trace.Write("RecurringTransactions.aspx", "BETWEEN " & CStr(Now.Date) & " 12:00 AM" & " AND " & CStr(Now.Date) & " 11:59 PM")

        objDR = objCommand.ExecuteReader()

        Dim intSuccess As Integer = 0
        Dim intFail As Integer = 0
        Dim intZeroCharge As Integer = 0
        Dim dblTotalAmount As Double = 0
        Dim boolBillChase = True

        'TODO: once webservices are working uncomment this code
        Dim sEmailMsg As String = ""
        'TODO: End

        While objDR.Read
            Dim dblNewAmount As Double

            dblNewAmount = Chase.CreateLineItems(objDR("Client_ID"))
            
            'create billing items and create aggregated transaction row
            If dblNewAmount <> -1 Then
                If dblNewAmount > 0 Then

                    'TODO: once webservices are working uncomment this code
                    'Dim strChaseResponse As String = Chase.NewTransaction(objDR("Client_ID"), dblNewAmount)
                    Dim strChaseResponse As String = "Approved,Approved"
                    'TODO: End

                    Dim arrayResponse As Array = Split(strChaseResponse, ",")

                    Select Case arrayResponse(0)
                        Case "Approved"
                            Trace.Write("RecurringTransactions.aspx", "Approved")

                            'TODO: once webservices are working uncomment this code
                            sEmailMsg = sEmailMsg & "Client ID => " & objDR("Client_ID") & ", " & "Company Name => " & objDR("CompanyName") & ", "
                            sEmailMsg = sEmailMsg & "Amount => " & Strings.FormatCurrency(dblNewAmount, 2) & "<br>"
                            'TODO: End

                            Dim objCommandClient As System.Data.SqlClient.SqlCommand
                            Dim objConnectionClient As System.Data.SqlClient.SqlConnection

                            objConnectionClient = New System.Data.SqlClient.SqlConnection
                            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                            objConnectionClient.Open()
                            objCommandClient = New System.Data.SqlClient.SqlCommand()
                            objCommandClient.Connection = objConnectionClient
                            objCommandClient.CommandText = "EXEC sprocBillSuccess @Client_ID, @Chase_ID, @BilledOn"
                            objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))

                            'TODO: once webservices are working uncomment this code
                            objCommandClient.Parameters.AddWithValue("@Chase_ID", "Manually Processed") 'arrayResponse(1))
                            'TODO: End
                            objCommandClient.Parameters.AddWithValue("@BilledOn", Now())
                            objCommandClient.ExecuteNonQuery()

                            objCommandClient = Nothing
                            objConnectionClient.Close()
                            objConnectionClient = Nothing

                            intSuccess += 1
                            dblTotalAmount += dblNewAmount
                        Case Else
                            Trace.Write("RecurringTransactions.aspx", "Declined or No Profile")

                            'update their client settings
                            Dim objCommandClient As System.Data.SqlClient.SqlCommand
                            Dim objConnectionClient As System.Data.SqlClient.SqlConnection

                            objConnectionClient = New System.Data.SqlClient.SqlConnection
                            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                            objConnectionClient.Open()
                            objCommandClient = New System.Data.SqlClient.SqlCommand()
                            objCommandClient.Connection = objConnectionClient
                            objCommandClient.CommandText = "EXEC sprocBillFail @Client_ID, @Message, @BilledOn"
                            objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
                            objCommandClient.Parameters.AddWithValue("@Message", arrayResponse(1))
                            objCommandClient.Parameters.AddWithValue("@BilledOn", Now())
                            objCommandClient.ExecuteNonQuery()

                            objCommandClient = Nothing
                            objConnectionClient.Close()
                            objConnectionClient = Nothing

                            'email the client a notification that there was a billing error
                            Dim objDRClient As System.Data.SqlClient.SqlDataReader
                            objConnectionClient = New System.Data.SqlClient.SqlConnection
                            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                            objConnectionClient.Open()
                            objCommandClient = New System.Data.SqlClient.SqlCommand()
                            objCommandClient.Connection = objConnectionClient
                            objCommandClient.CommandText = "SELECT Email FROM Clients WHERE Client_ID = @Client_ID"
                            objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
                            objDRClient = objCommandClient.ExecuteReader()

                            Dim mailClient As New MailMessage()
                            mailClient.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Billing")
                            mailClient.Subject = "TimeClockWizard Billing Error"

                            objDRClient.Read()
                            'add the recipients
                            Trace.Write("RecurringTransactions.aspx", "Mail Warning to: " & objDRClient("Email"))
                            mailClient.To.Add(objDRClient("Email"))

                            'set the content
                            mailClient.Body = "On " & Now.Date & ", TimeClockWizard unsuccessfully attempted to bill the credit card your company has on file.  Please update your credit card information as soon as possible to gurantee uninterrupted service.  To update your information, login to your account, click 'Settings' and then 'My Subscription'.<br><br>You have 7 days to update your information, and if you fail to do so your account will be deactivated.<br><br>If you have any questions, you can reply to this email or call us at (877) 646-4446.<br><br>Thank you,<br>TimeClockWizard Support"
                            mailClient.IsBodyHtml = True

                            'send the message
                            Dim smtpClient As New SmtpClient()
                            smtpClient.Send(mailClient)

                            objDRClient.Close()
                            objCommandClient = Nothing
                            objConnectionClient.Close()
                            objConnectionClient = Nothing

                            intFail += 1
                    End Select
                Else
                    'The Bill came out to be $0, most likely due to Credits

                    Dim objCommandClient As System.Data.SqlClient.SqlCommand
                    Dim objConnectionClient As System.Data.SqlClient.SqlConnection

                    objConnectionClient = New System.Data.SqlClient.SqlConnection
                    objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                    objConnectionClient.Open()
                    objCommandClient = New System.Data.SqlClient.SqlCommand()
                    objCommandClient.Connection = objConnectionClient
                    objCommandClient.CommandText = "UPDATE Clients SET NextBillingDate = @NextBillingDate WHERE Client_ID = @Client_ID"
                    objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
                    objCommandClient.Parameters.AddWithValue("@NextBillingDate", DateAdd(DateInterval.Month, 1, Now.Date))
                    objCommandClient.ExecuteNonQuery()

                    objCommandClient = Nothing
                    objConnectionClient.Close()
                    objConnectionClient = Nothing

                    objConnectionClient = New System.Data.SqlClient.SqlConnection
                    objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                    objConnectionClient.Open()
                    objCommandClient = New System.Data.SqlClient.SqlCommand()
                    objCommandClient.Connection = objConnectionClient
                    objCommandClient.CommandText = "UPDATE CCTransactions SET Success = 1, Submitted = @Submitted WHERE Client_ID = @Client_ID AND Success = 0"
                    objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
                    objCommandClient.Parameters.AddWithValue("@Submitted", Now)
                    objCommandClient.ExecuteNonQuery()

                    objCommandClient = Nothing
                    objConnectionClient.Close()
                    objConnectionClient = Nothing

                    intZeroCharge += 1
                End If
            End If
        End While

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'done processing, send emails
        Dim mail As New MailMessage()

        'TODO: once Chase webservices are working fine, uncomment this code
        'set the addresses
        mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Manual Billing")
        mail.Subject = "TimeClockWizard Manual Billing for date => " & CStr(Now.Date)
        mail.To.Add("info@TimeClockWizard.com")
		
        'set the content
        If sEmailMsg = "" Then
            mail.Body = "!!! No records found for manual billing for today !!!"
        Else
            mail.Body = sEmailMsg
        End If
        mail.IsBodyHtml = True

        'send the message
        Dim smtp2 As New SmtpClient()
        smtp2.Send(mail)
        'TODO: End

        'TODO: this is not required
        ''set the addresses
        'mail.From = New MailAddress("chase@TimeClockWizard.com", "TimeClockWizard Recurring Billing")
        'mail.Subject = "TimeClockWizard Recurring Billing"
        'mail.To.Add("info@TimeClockWizard.com")

        ''set the content
        'mail.Body = "Success: " & intSuccess & "<br>Zero Charge: " & intZeroCharge & "<br>Fail: " & intFail & "<br>Amount: " & Strings.FormatCurrency(dblTotalAmount, 2)
        'mail.IsBodyHtml = True

        ''send the message
        'Dim smtp As New SmtpClient()
        'smtp.Send(mail)
        'TODO: End

        Trace.Write("RecurringTransactions.aspx", "End Execution")
    End Sub
End Class
