
Partial Class Utility_Overnight_AnnualAbsenceAccrual
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Trace.Write("AnnualAbsenceAccrual.aspx", "Begin Execution")

        Dim AbsenceTypes(6) As String
        AbsenceTypes(1) = "Vacation"
        AbsenceTypes(2) = "Holiday"
        AbsenceTypes(3) = "SickDay"
        AbsenceTypes(4) = "Personal"
        AbsenceTypes(5) = "PaidLeave"
        AbsenceTypes(6) = "Other"

        Dim i As Integer
        i = 1

        While i < 7
            Trace.Write("AnnualAbsenceAccrual.aspx", "Cycling For " & AbsenceTypes(i))

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection

            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "UPDATE AbsenceUserSettings SET [" & AbsenceTypes(i) & "Total] = [" & AbsenceTypes(i) & "Total] + [" & AbsenceTypes(i) & "Rate] WHERE User_ID IN (SELECT User_ID FROM Users WHERE HireDate = @Today) AND [" & AbsenceTypes(i) & "Enabled] = '3'"
            objCommand.Parameters.AddWithValue("@Today", CDate(CStr(Now.Date) & " 12:00 AM"))

            objDR = objCommand.ExecuteReader()

            objDR.Close()
            objDR = Nothing

            Trace.Write("AnnualAbsenceAccrual.aspx", "End Cycling For " & AbsenceTypes(i))

            i = i + 1
        End While
    End Sub
End Class
