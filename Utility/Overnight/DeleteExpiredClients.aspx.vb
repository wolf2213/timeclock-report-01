
Partial Class Utility_Overnight_DeleteExpiredClients
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Trace.Write("DeleteExpiredClients.aspx", "Begin Execution")

        'go through the DB, select who needs to be billed
        Trace.Write("DeleteExpiredClients.aspx", "Select Client IDs to be deleted and loop through them")
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Client_ID, Trial FROM Clients WHERE (Deleted = 0 AND Enabled = 0 AND NextBillingDate < @PaidDate) OR (Trial = 1 AND NextBillingDate < @TrialDate)"
        objCommand.Parameters.AddWithValue("@PaidDate", DateAdd(DateInterval.Day, -14, Now.Date))
        objCommand.Parameters.AddWithValue("@TrialDate", DateAdd(DateInterval.Day, -7, Now.Date))

        Trace.Write("DeleteExpiredClients.aspx", "Deleting paid clients expired before " & DateAdd(DateInterval.Day, -14, Now.Date) & " and trial clients expired before " & DateAdd(DateInterval.Day, -7, Now.Date))

        objDR = objCommand.ExecuteReader()

        While objDR.Read
            Trace.Write("DeleteExpiredClients.aspx", "Deleting Client_ID " & objDR("Client_ID") & "...")

            Dim objCommandClient As System.Data.SqlClient.SqlCommand
            Dim objConnectionClient As System.Data.SqlClient.SqlConnection

            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            If objDR("Trial") Then
                Trace.Write("DeleteExpiredClients.aspx", "EXEC sprocDeleteClientTrial")
                objCommandClient.CommandText = "EXEC sprocDeleteClientTrial @Client_ID"
            Else
                Trace.Write("DeleteExpiredClients.aspx", "EXEC sprocDeleteClientPaid")
                objCommandClient.CommandText = "EXEC sprocDeleteClientPaid @Client_ID"
            End If
            objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
            objCommandClient.ExecuteNonQuery()

            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing

            Trace.Write("DeleteExpiredClients.aspx", "Deleted")
        End While

        Trace.Write("DeleteExpiredClients.aspx", "End Execution")
    End Sub
End Class
