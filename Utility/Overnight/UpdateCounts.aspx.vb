
Partial Class Utility_UpdateCounts
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Trace.Write("UpdateCounts.aspx", "Begin Execution")

        'go through the DB, select who needs to be billed
        Trace.Write("UpdateCounts.aspx", "Select all Client IDs and loop through them")
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Client_ID FROM Clients WHERE (Deleted = 0 AND Enabled = 1)"

        objDR = objCommand.ExecuteReader()

        While objDR.Read
            Trace.Write("UpdateCounts.aspx", "Updating Client_ID " & objDR("Client_ID") & "...")

            Dim objCommandClient As System.Data.SqlClient.SqlCommand
            Dim objConnectionClient As System.Data.SqlClient.SqlConnection

            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            Trace.Write("UpdateCounts.aspx", "EXEC sprocUpdateCurrentUsers")
            objCommandClient.CommandText = "EXEC sprocUpdateCurrentUsers @Client_ID"
            objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
            objCommandClient.ExecuteNonQuery()

            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing

            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            Trace.Write("UpdateCounts.aspx", "EXEC sprocUpdateClockGuard")
            objCommandClient.CommandText = "EXEC sprocUpdateClockGuard @Client_ID"
            objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
            objCommandClient.ExecuteNonQuery()

            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing

            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            Trace.Write("UpdateCounts.aspx", "EXEC sprocUpdateMobileAccess")
            objCommandClient.CommandText = "EXEC sprocUpdateMobileAccess @Client_ID"
            objCommandClient.Parameters.AddWithValue("@Client_ID", objDR("Client_ID"))
            objCommandClient.ExecuteNonQuery()

            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing

            Trace.Write("UpdateCounts.aspx", "Finished")
        End While

        Trace.Write("UpdateCounts.aspx", "End Execution")
    End Sub
End Class
