ALTER TABLE Preferences
ADD ApproveOvertime BIT

GO


IF OBJECT_ID(N'sprocUpdatePreferences') > 0
	DROP PROCEDURE sprocUpdatePreferences
GO


-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/19/2014
-- Description:	Save Payroll Summary
-- =============================================
CREATE PROCEDURE [dbo].[sprocUpdatePreferences]
	@Client_ID					BIGINT
	,@QuickClockIn				BIT
	,@QuickClockInPassword		BIT
	,@SelfRegistration			BIT
	,@SelfRegistrationPin		VARCHAR(20)
	,@WhosInRestriction			BIT
	,@PaidBreaks				BIT
	,@TimeRounding				TINYINT
	,@MileageRate				FLOAT
	,@DashboardMessageText		VARCHAR(255)
	,@DashboardMessageRed		BIT	
	,@DashboardMessageBold		BIT
	,@LoginMessageText			VARCHAR(255)
	,@LoginMessageRed			BIT
	,@LoginMessageBold			BIT
	,@BirthdayNotification		BIT
	,@OverTimetype				INT -- 1:Daily Overtime Pay ,2:Weekly Overtime Pay ,3:Both daily and weekly 
	,@OvertimeDailyHours		FLOAT = NULL
	,@OvertimeWeeklyHours		FLOAT = NULL
	,@IsSaved					BIGINT = 0 OUT
	,@ApproveOvertime			BIT 
AS
BEGIN
	BEGIN TRY
	
		UPDATE	p
		SET		p.QuickClockIn = @QuickClockIn
				,p.QuickClockInPassword =@QuickClockInPassword
				,p.SelfRegistration = @SelfRegistration
				,p.SelfRegistrationPin = @SelfRegistrationPin
				,p.WhosInRestriction = @WhosInRestriction 
				,p.PaidBreaks  = @PaidBreaks
				,p.TimeRounding = @TimeRounding
				,p.MileageRate = @MileageRate
				,p.DashboardMessageText	 = @DashboardMessageText		
				,p.DashboardMessageRed	 = @DashboardMessageRed		
				,p.DashboardMessageBold	 = @DashboardMessageBold		
				,p.LoginMessageText		 = @LoginMessageText			
				,p.LoginMessageRed		 = @LoginMessageRed			
				,p.LoginMessageBold		 = @LoginMessageBold			
				,p.BirthdayNotification	 = @BirthdayNotification		
				,p.OvertimeType			 = @OverTimetype				
				,p.OvertimeDailyHours	 = @OvertimeDailyHours		
				,p.OvertimeWeeklyHours   = @OvertimeWeeklyHours	
				,p.ApproveOvertime        = @ApproveOvertime		
		FROM Preferences p
		WHERE p.Client_ID = @Client_ID
			
		SELECT @IsSaved = SCOPE_IDENTITY()
		
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocUpdatePreferences :%s',15, 1, @ErrMsg)   
	END CATCH
END


GO