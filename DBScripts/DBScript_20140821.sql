USE [Clockitin]
GO

--======================= ALTER Columns =====================================================
ALTER TABLE PayrollDetail ADD PayAmount NUMERIC(18, 2)
	,OTAmount NUMERIC(18, 2)
GO

ALTER TABLE Preferences ADD OvertimeType INT
	,OverTimeDailyHours FLOAT
	,OverTimeWeeklyHours FLOAT
GO

--======================= END ALTER Columns =====================================================

--============================ PROCEDURE =========================================================

IF OBJECT_ID(N'sprocUpdatePreferences') > 0
	DROP PROCEDURE sprocUpdatePreferences
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/19/2014
-- Description:	Save Payroll Summary
-- =============================================
CREATE PROCEDURE [dbo].[sprocUpdatePreferences]
	@Client_ID					BIGINT
	,@QuickClockIn				BIT
	,@QuickClockInPassword		BIT
	,@SelfRegistration			BIT
	,@SelfRegistrationPin		VARCHAR(20)
	,@WhosInRestriction			BIT
	,@PaidBreaks				BIT
	,@TimeRounding				TINYINT
	,@MileageRate				FLOAT
	,@DashboardMessageText		VARCHAR(255)
	,@DashboardMessageRed		BIT	
	,@DashboardMessageBold		BIT
	,@LoginMessageText			VARCHAR(255)
	,@LoginMessageRed			BIT
	,@LoginMessageBold			BIT
	,@BirthdayNotification		BIT
	,@OverTimetype				INT -- 1:Daily Overtime Pay ,2:Weekly Overtime Pay ,3:Both daily and weekly 
	,@OvertimeDailyHours		FLOAT = NULL
	,@OvertimeWeeklyHours		FLOAT = NULL
	,@IsSaved					BIGINT = 0 OUT
AS
BEGIN
	BEGIN TRY
	
		UPDATE	p
		SET		p.QuickClockIn = @QuickClockIn
				,p.QuickClockInPassword =@QuickClockInPassword
				,p.SelfRegistration = @SelfRegistration
				,p.SelfRegistrationPin = @SelfRegistrationPin
				,p.WhosInRestriction = @WhosInRestriction 
				,p.PaidBreaks  = @PaidBreaks
				,p.TimeRounding = @TimeRounding
				,p.MileageRate = @MileageRate
				,p.DashboardMessageText	 = @DashboardMessageText		
				,p.DashboardMessageRed	 = @DashboardMessageRed		
				,p.DashboardMessageBold	 = @DashboardMessageBold		
				,p.LoginMessageText		 = @LoginMessageText			
				,p.LoginMessageRed		 = @LoginMessageRed			
				,p.LoginMessageBold		 = @LoginMessageBold			
				,p.BirthdayNotification	 = @BirthdayNotification		
				,p.OvertimeType			 = @OverTimetype				
				,p.OvertimeDailyHours	 = @OvertimeDailyHours		
				,p.OvertimeWeeklyHours   = @OvertimeWeeklyHours			
		FROM Preferences p
		WHERE p.Client_ID = @Client_ID
			
		SELECT @IsSaved = SCOPE_IDENTITY()
		
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocUpdatePreferences :%s',15, 1, @ErrMsg)   
	END CATCH
END


GO

IF OBJECT_ID(N'sprocSavePayrollSummary') > 0
	DROP PROCEDURE sprocSavePayrollSummary
GO

-- =============================================
-- Author:		Technobrains Solution
-- Create date: 08/19/2014
-- Description:	Save Payroll Summary
-- =============================================
--sprocSavePayrollSummary 1823, 0, 0, '2007-08-10', '2007-08-16'
CREATE PROCEDURE [dbo].[sprocSavePayrollSummary] (
	@Client_ID BIGINT
	,@User_ID INT
	,@Office_Id BIGINT = 0
	,@StartDate DATETIME
	,@EndDate DATETIME
	,@Payroll_ID BIGINT = 0 OUT
	)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
		
		INSERT INTO PayrollSummary (
			Client_ID
			,Office_ID
			,StartDate
			,EndDate
			)
		SELECT @Client_ID
			,@Office_Id
			,@StartDate
			,@EndDate

		SELECT @Payroll_ID = SCOPE_IDENTITY()

		DECLARE @MileageRate NUMERIC(18, 3)
		DECLARE @OverTimeMethod INT,
				@MaxDailyHours	INT,
				@MaxWeeklyHours INT,
				@PaidBreaks		BIT,
				@TimeRounding	INT,
				@OverTimeEnable	INT

		SELECT	@MileageRate = MileageRate,
				@OverTimeMethod = ISNULL(OvertimeType,0),
				@MaxDailyHours = ISNULL(OvertimeDailyHours,0) * 60,
				@MaxWeeklyHours = ISNULL(OvertimeWeeklyHours,0) * 60,
				@PaidBreaks = ISNULL(PaidBreaks,0),
				@TimeRounding = CASE ISNULL(TimeRounding,0) WHEN 0 THEN 0 WHEN 1 THEN 15 WHEN 2 THEN 6 END,
				@OverTimeEnable = ISNULL(Overtime,0)
		FROM dbo.Preferences
		WHERE Client_ID = @Client_ID

		INSERT INTO PayrollDetail (
			Client_ID
			,User_ID
			,Payroll_ID
			,Wage
			,OTWage
			,RegularMinutes
			,OTMinutes
			,PayAmount
			,OTAmount
			,TotalOtherPay
			)
		SELECT @Client_ID
			,u.User_ID
			,@Payroll_ID
			,x.Wage
			,x.OTWage
			,x.Minutes
			,x.OTMinutes
			,x.PayAmount
			,x.OTPayAmount
			,x.TotalOtherPay
		FROM Users u
		CROSS APPLY dbo.fn_CalculatePayroll(u.User_ID, @StartDate, @EndDate, u.Wage, @OverTimeEnable, @OverTimeMethod, @MaxDailyHours, @MaxWeeklyHours, @MileageRate, @PaidBreaks, @TimeRounding) x
		WHERE u.Client_ID = @Client_ID

		SELECT @Client_ID
			,u.User_ID
			,@Payroll_ID
			,x.Wage
			,x.OTWage
			,x.Minutes
			,x.OTMinutes
			,x.PayAmount
			,x.OTPayAmount
			,x.TotalOtherPay
		FROM Users u
		CROSS APPLY dbo.fn_CalculatePayroll(u.User_ID, @StartDate, @EndDate, u.Wage, @OverTimeEnable, @OverTimeMethod, @MaxDailyHours, @MaxWeeklyHours, @MileageRate, @PaidBreaks, @TimeRounding) x
		WHERE u.Client_ID = @Client_ID


		UPDATE [PayrollSummary]
		SET [RegularMinutes] = d.RegularMinutes
			,[OvertimeMinutes] = d.OvertimeMinutes
			,[WagePayment] = d.WagePayment
			,OtherPayment = d.OtherPayment
		FROM	[PayrollSummary] s
		INNER JOIN (
			SELECT	d.Payroll_ID,
					SUM([RegularMinutes]) AS [RegularMinutes],
					SUM([OTMinutes]) AS [OvertimeMinutes],
					SUM([PayAmount] + OTAmount) AS [WagePayment],
					SUM([TotalOtherPay]) AS OtherPayment
			FROM	PayrollDetail d 
			WHERE	d.Payroll_ID = @Payroll_ID
			GROUP BY d.Payroll_ID
		) d ON d.Payroll_ID = s.Payroll_ID
		WHERE s.Client_ID = @Client_ID
			AND s.Payroll_ID = @Payroll_ID

		UPDATE [TimeRecords]
		SET [Payroll_ID] = @Payroll_ID
		WHERE [Client_ID] = @Client_ID
		AND	[StartTime] BETWEEN @StartDate AND @EndDate

		UPDATE [TimeRecords]
		SET [VerifiedBy] = @User_ID
		WHERE [Client_ID] = @Client_ID
			AND VerifiedBy = 0
			AND [StartTime] BETWEEN @StartDate AND @EndDate
			
		COMMIT TRAN

	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR ('Error in sprocSavePayrollSummary :%s',15,1,@ErrMsg)
	END CATCH
END
GO

IF OBJECT_ID(N'RoundTime') > 0
	DROP FUNCTION RoundTime
GO

-- =============================================
-- Author:		Technobrains IT Solution Pvt.LTD
-- Create date: 08/21/2014
-- Description:	Round Time
-- =============================================
--SELECT [dbo].[RoundTime]('2014-08-01 14:04',0)
CREATE FUNCTION [dbo].[RoundTime] (
	@Time DATETIME
	,@RoundToMin INT
	)
RETURNS DATETIME
AS
BEGIN
	DECLARE @iMinutes INT
		,@iHours INT

	SELECT @iHours = LEFT(CONVERT(VARCHAR(5), @Time, 108), 2)
		,@iMinutes = RIGHT(CONVERT(VARCHAR(5), @Time, 108), 2)

	IF @RoundToMin = 15
	BEGIN
		IF (
				(@iMinutes > 0)
				AND (@iMinutes <= 7)
				)
			SET @iMinutes = 0
		ELSE IF (
				(@iMinutes > 7)
				AND (@iMinutes <= 15)
				)
			SET @iMinutes = 15
		ELSE IF (
				@iMinutes > 15
				AND @iMinutes <= 22
				)
			SET @iMinutes = 15
		ELSE IF (
				@iMinutes > 22
				AND @iMinutes <= 30
				)
			SET @iMinutes = 30
		ELSE IF (
				@iMinutes > 30
				AND @iMinutes <= 37
				)
			SET @iMinutes = 30
		ELSE IF (
				@iMinutes > 37
				AND @iMinutes <= 45
				)
			SET @iMinutes = 45
		ELSE IF (
				@iMinutes > 45
				AND @iMinutes <= 52
				)
			SET @iMinutes = 45
		ELSE IF (
				@iMinutes > 52
				AND @iMinutes <= 59
				)
		BEGIN
			SET @iHours = @iHours + 1
			SET @iMinutes = 0
		END
	END
	ELSE IF @RoundToMin = 06
	BEGIN
		IF (
				(@iMinutes > 0)
				AND (@iMinutes <= 3)
				)
			SET @iMinutes = 0
		ELSE IF (
				(@iMinutes > 3)
				AND (@iMinutes <= 9)
				)
			SET @iMinutes = 6
		ELSE IF (
				@iMinutes > 10
				AND @iMinutes <= 15
				)
			SET @iMinutes = 12
		ELSE IF (
				@iMinutes > 15
				AND @iMinutes <= 21
				)
			SET @iMinutes = 18
		ELSE IF (
				@iMinutes > 21
				AND @iMinutes <= 27
				)
			SET @iMinutes = 24
		ELSE IF (
				@iMinutes > 27
				AND @iMinutes <= 33
				)
			SET @iMinutes = 30
		ELSE IF (
				@iMinutes > 33
				AND @iMinutes <= 39
				)
			SET @iMinutes = 36
		ELSE IF (
				@iMinutes > 39
				AND @iMinutes <= 45
				)
			SET @iMinutes = 42
		ELSE IF (
				@iMinutes > 45
				AND @iMinutes <= 51
				)
			SET @iMinutes = 48
		ELSE IF (
				@iMinutes > 51
				AND @iMinutes <= 57
				)
			SET @iMinutes = 54
		ELSE IF (
				@iMinutes > 57
				AND @iMinutes <= 59
				)
		BEGIN
			SET @iHours = @iHours + 1
			SET @iMinutes = 0
		END
	END

	SELECT @Time = DATEADD("hh", @iHours, CAST(CONVERT(VARCHAR(10), @Time, 101) AS DATETIME))

	SELECT @Time = DATEADD("mi", @iMinutes, @Time)

	RETURN @Time
END
	--===================================================================================================================
GO

IF OBJECT_ID(N'fn_CalculatePayroll') > 0
	DROP FUNCTION fn_CalculatePayroll
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:		Technobrains IT Solution Pvt.LTD
-- Create date: 08/19/2014
-- Description:	payroll Calculation for Daily OR Weekly of Employee
-- =========================================================================
/*

SELECT *
FROM dbo.fn_CalculatePayroll(1801, '2007-08-17','2007-08-23', 10.00, 1, 0, 0,0, 0.585, 0, 0)

SELECT *
FROM dbo.fn_CalculatePayroll(1801, '2007-08-17','2007-08-23', 10.00, 1, 1, 480,0, 0.585, 0, 0)

SELECT *
FROM dbo.fn_CalculatePayroll(1801, '2007-08-17','2007-08-23', 10.00, 1, 2,0, 2400, 0.585, 0, 0)

SELECT *
FROM dbo.fn_CalculatePayroll(1801, '2007-08-17','2007-08-23', 10.00, 1, 3, 480,2400, 0.585, 0, 0)

*/
CREATE FUNCTION [dbo].[fn_CalculatePayroll] (
	@UserID INT
	,@StartDate DATE
	,@EndDate DATE
	,@Wage NUMERIC(18, 2)
	,@OverTimeEnable INT
	,@OverTimeMethod INT -- 0=No Overtime, 1=Daily Overtime, 2=Weekly Overtime, 3=Daily and weekly(both) Overtime 
	,@MaxDailyHours INT
	,@MaxWeeklyHours INT
	,@MileageRate NUMERIC(18, 3)
	,@PaidBreaks BIT
	,@TimeRounding INT -- 0=Disabled, use exact time, 15=Enabled, round all times to the nearest 1/4 hour (15 minute rule), 6=Enabled, round all times to the nearest 1/10 hour (6 minute rule)
	)
RETURNS @summary TABLE (
	--RETURNS @RESULT TABLE (
	UserID INT
	,Minutes INT
	,OTMinutes INT
	,Wage NUMERIC(18, 2)
	,OTWage NUMERIC(18, 2)
	,PayAmount NUMERIC(18, 2)
	,OTPayAmount NUMERIC(18, 2)
	,TotalOtherPay NUMERIC(18, 2)
	)
AS
BEGIN
	DECLARE @Details TABLE (
		AutoID INT IDENTITY(1, 1)
		,UserID INT
		,DATE DATETIME
		,StartTime DATETIME
		,EndTime DATETIME
		,Minutes INT
		)
	DECLARE @result TABLE (
		--DECLARE @summary TABLE (
		UserID INT
		,Minutes INT
		,OTMinutes INT
		,Wage NUMERIC(18, 2)
		,OTWage NUMERIC(18, 2)
		,PayAmount NUMERIC(18, 2)
		,OTPayAmount NUMERIC(18, 2)
		,TotalOtherPay NUMERIC(18, 2)
		)
	DECLARE @TotalMinutes INT
		,@OTWage NUMERIC(18, 2)
	DECLARE @cnt INT
		,@inc INT
	DECLARE @Minutes INT
	DECLARE @Mileage INT
	DECLARE @MaxHours INT
	DECLARE @WeeklyBased BIT = 0

	SELECT @Mileage = SUM([Miles])
	FROM [dbo].[Mileage]
	WHERE [User_ID] = @UserID
		AND [DateTime] BETWEEN @StartDate
			AND @EndDate

	SELECT @Mileage = ISNULL(@Mileage, 0)

	SELECT @OTWage = @Wage * 1.5

	INSERT INTO @Details (
		UserID
		,DATE
		,StartTime
		,EndTime
		,Minutes
		)
	SELECT t.User_ID
		,CAST(t.StartTime AS DATE) AS DATE
		,t.StartTime
		,t.EndTime
		,CASE 
			WHEN t.Type = 2 --break Time
				THEN (
						CASE 
							WHEN @PaidBreaks = 1
								THEN 0
							ELSE - DATEDIFF(mi, dbo.RoundTime(t.StartTime, @TimeRounding), dbo.RoundTime(t.EndTime, @TimeRounding))
							END
						)
			ELSE DATEDIFF(mi, dbo.RoundTime(t.StartTime, @TimeRounding), dbo.RoundTime(t.EndTime, @TimeRounding))
			END AS Minutes
	FROM TimeRecords t(NOLOCK)
	WHERE t.User_ID = @UserID
		AND t.StartTime BETWEEN @StartDate
			AND @EndDate
		AND t.Payroll_ID = 0
		AND t.Type IN (
			1
			,2
			,4
			) --1=Work Time, 2=BreakTime, 4=AbsenceTime
	ORDER BY Minutes

	SELECT @TotalMinutes = SUM(Minutes)
		,@cnt = COUNT(1)
	FROM @Details

	-- SET MaxHours as per OverTimeMethod - Daily / Weekly
	IF @OverTimeMethod = 2
		OR (
			@OverTimeMethod = 3
			AND @TotalMinutes >= @MaxWeeklyHours
			)
	BEGIN
		SET @WeeklyBased = 1
		SET @MaxHours = @MaxWeeklyHours
	END
	ELSE
	BEGIN
		SET @WeeklyBased = 0
		SET @MaxHours = @MaxDailyHours
	END

	IF @OverTimeEnable = 0
		OR @OverTimeMethod = 0
		OR @TotalMinutes <= @MaxHours
	BEGIN
		INSERT INTO @result (
			UserID
			,Minutes
			,OTMinutes
			,Wage
			,OTWage
			,PayAmount
			,OTPayAmount
			)
		SELECT UserID
			,Minutes
			,0
			,@Wage
			,@OTWage
			,(Minutes * @Wage) / 60.0
			,0
		FROM @Details
	END
	ELSE
	BEGIN
		SET @inc = 1

		WHILE @inc <= @cnt
		BEGIN
			SELECT @Minutes = 0

			SELECT @Minutes = d.Minutes
			FROM @Details d
			WHERE AutoID = @inc

			IF @MaxHours > @Minutes
			BEGIN
				INSERT INTO @result (
					UserID
					,Minutes
					,OTMinutes
					,Wage
					,OTWage
					,PayAmount
					,OTPayAmount
					)
				SELECT @UserID
					,@Minutes
					,0
					,@Wage
					,@OTWage
					,(@Minutes * @Wage) / 60.0
					,0

				IF @WeeklyBased = 1
					SET @MaxHours = @MaxHours - @Minutes
			END
			ELSE
			BEGIN
				INSERT INTO @result (
					UserID
					,Minutes
					,OTMinutes
					,Wage
					,OTWage
					,PayAmount
					,OTPayAmount
					)
				SELECT @UserID
					,@MaxHours
					,@Minutes - @MaxHours
					,@Wage
					,@OTWage
					,(@MaxHours * @Wage) / 60.0
					,((@Minutes - @MaxHours) * @otWage) / 60.0

				IF @WeeklyBased = 1
					SET @MaxHours = 0
			END

			SET @inc = @inc + 1
		END
	END

	INSERT INTO @Summary (
		UserID
		,Minutes
		,OTMinutes
		,Wage
		,OTWage
		,PayAmount
		,OTPayAmount
		,TotalOtherPay
		)
	SELECT UserID
		,SUM(Minutes)
		,SUM(OTMinutes)
		,@Wage
		,@OTWage
		,SUM(PayAmount)
		,SUM(OTPayAmount)
		,(@Mileage * @MileageRate)
	FROM @result
	GROUP BY UserID

	RETURN
END
--============================ END FUNCTIONS =======================================================================================