USE [Clockitin]
GO

ALTER TABLE Users
ADD OverTimetype INT WITH DEFAULT(0)
	,OvertimeDailyHours FLOAT 
	,OvertimeWeeklyHours FLOAT 
	,ApproveOvertime BIT WITH DEFAULT(0)

GO
/****** Object:  StoredProcedure [dbo].[sprocUpdatePreferencesLogo]    Script Date: 08/27/2014 18:40:58 ******/
IF object_ID(N'sprocUpdatePreferencesLogo')>0
	DROP PROCEDURE sprocUpdatePreferencesLogo
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/26/2014
-- Description:	sprocUpdatePreferencesLogo
-- =============================================
CREATE PROCEDURE [dbo].[sprocUpdatePreferencesLogo]
	@ClientID		BIGINT
	,@Logo			VARCHAR(MAX) = NULL
	,@LogoWidth		INT
	,@LogoHeight	INT
	,@MIMEType		VARCHAR(50)
	,@IsSaved		BIGINT = 0 OUT
AS
BEGIN
	BEGIN TRY
		
		UPDATE	p 
		SET		p.Logo			=ISNULL(@Logo,0)
				,p.LogoWidth	= @LogoWidth 
				,p.LogoHeight	= @LogoHeight
				,p.MIMEType		= @MIMEType 
		FROM	Preferences p
		WHERE Client_ID = @ClientID
		
		SELECT @IsSaved = 1
		
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocUpdatePreferencesLogo :%s',15, 1, @ErrMsg)   
	END CATCH
END




GO

/****** Object:  StoredProcedure [dbo].[sprocGetPreferences]    Script Date: 08/27/2014 18:40:58 ******/

IF object_ID(N'sprocGetPreferences')>0
	DROP PROCEDURE sprocGetPreferences
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/25/2014
-- Description:	sproc Get Preferences
-- =============================================
/*
	EXEC sprocGetPreferences 1823
*/
CREATE PROCEDURE [dbo].[sprocGetPreferences]
	@ClientID		BIGINT
AS
BEGIN
	BEGIN TRY
		
		SELECT 
			p.Client_ID
			,p.Logo
			,p.LogoWidth
			,p.LogoHeight
			,p.MIMEType
			,p.PaidBreaks
			,p.Overtime
			,p.QuickClockIn
			,p.QuickClockInPassword
			,p.WhosInRestriction
			,p.ClockPointRestriction
			,p.MileageRate
			,p.TimeRounding
			,p.SelfRegistration
			,p.SelfRegistrationPin
			,p.BirthdayNotification
			,p.UserCustom1Label
			,p.UserCustom2Label
			,p.UserCustom3Label
			,p.UserCustom4Label
			,p.UserCustom5Label
			,p.LoginMessageText
			,p.LoginMessageRed
			,p.LoginMessageBold
			,p.DashboardMessageText
			,p.DashboardMessageRed
			,p.DashboardMessageBold
			,p.OvertimeType
			,(p.OvertimeDailyHours / 60) AS OvertimeDailyHours
			,(p.OvertimeWeeklyHours / 60) AS OvertimeWeeklyHours
			,p.ApproveOvertime
			,c.Subdomain 
		FROM  Preferences p 
		INNER JOIN Clients c ON p.Client_ID = c.Client_ID 
		WHERE p.Client_ID = @ClientID
		
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocGetPreferences :%s',15, 1, @ErrMsg)   
	END CATCH
END




GO

/****** Object:  StoredProcedure [dbo].[sprocSavePayrollSummary]    Script Date: 08/27/2014 18:40:58 ******/
IF object_ID(N'sprocSavePayrollSummary')>0
	DROP PROCEDURE sprocSavePayrollSummary
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Technobrains Solution
-- Create date: 08/19/2014
-- Description:	Save Payroll Summary
-- =============================================
--sprocSavePayrollSummary 1823, 0, 0, '2007-08-10', '2007-08-16'
CREATE PROCEDURE [dbo].[sprocSavePayrollSummary] (
	@Client_ID BIGINT
	,@User_ID INT
	,@Office_Id BIGINT = 0
	,@StartDate DATETIME
	,@EndDate DATETIME
	,@Payroll_ID BIGINT = 0 OUT
	)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
		
		INSERT INTO PayrollSummary (
			Client_ID
			,Office_ID
			,StartDate
			,EndDate
			)
		SELECT @Client_ID
			,@Office_Id
			,@StartDate
			,@EndDate

		SELECT @Payroll_ID = SCOPE_IDENTITY()

		DECLARE @MileageRate		NUMERIC(18, 3)
		DECLARE @OverTimeMethod		INT,
				@MaxDailyHours		INT,
				@MaxWeeklyHours		INT,
				@PaidBreaks			BIT,
				@TimeRounding		INT,
				@OverTimeEnable		INT,
				@ApproveOvertime	INT
		
		SELECT	@OverTimeMethod		= ISNULL(u.OverTimetype,0)
				,@MaxDailyHours		= ISNULL(u.OvertimeDailyHours,0) * 60 
				,@MaxWeeklyHours	= ISNULL(u.OvertimeWeeklyHours,0) * 60
				,@OverTimeEnable	= ISNULL(u.Overtime,0)
		FROM	Users u
		WHERE	u.User_ID = @User_ID
		AND u.Client_ID = @Client_ID
		
		SELECT	@MileageRate	 = p.MileageRate,
				@PaidBreaks      =	ISNULL(p.PaidBreaks,0),
				@TimeRounding	 =	CASE ISNULL(p.TimeRounding,0) 
											WHEN 0 THEN 0 
											WHEN 1 THEN 15 
											WHEN 2 THEN 6 
										END
		FROM	dbo.Preferences p
		WHERE	p.Client_ID = @Client_ID
			
		IF @OverTimeMethod = 0 BEGIN
			SELECT	@OverTimeMethod  =	ISNULL(p.OvertimeType,0),
					@MaxDailyHours   =	ISNULL(p.OvertimeDailyHours,0) * 60,
					@MaxWeeklyHours  =	ISNULL(p.OvertimeWeeklyHours,0) * 60,
					@OverTimeEnable  =  ISNULL(Overtime,0)
			FROM	dbo.Preferences p
			WHERE	p.Client_ID = @Client_ID
		END
		
		INSERT INTO PayrollDetail (
			Client_ID
			,User_ID
			,Payroll_ID
			,Wage
			,OTWage
			,RegularMinutes
			,OTMinutes
			,PayAmount
			,OTAmount
			,TotalOtherPay
			)
		SELECT @Client_ID
			,u.User_ID
			,@Payroll_ID
			,x.Wage
			,x.OTWage
			,x.Minutes
			,x.OTMinutes
			,x.PayAmount
			,x.OTPayAmount
			,x.TotalOtherPay
		FROM Users u
		CROSS APPLY dbo.fn_CalculatePayroll(u.User_ID, @StartDate, @EndDate, u.Wage, @OverTimeEnable, @OverTimeMethod, @MaxDailyHours, @MaxWeeklyHours, @MileageRate, @PaidBreaks, @TimeRounding) x
		WHERE u.Client_ID = @Client_ID

	
		UPDATE [PayrollSummary]
		SET [RegularMinutes] = d.RegularMinutes
			,[OvertimeMinutes] = d.OvertimeMinutes
			,[WagePayment] = d.WagePayment
			,OtherPayment = d.OtherPayment
		FROM	[PayrollSummary] s
		INNER JOIN (
			SELECT	d.Payroll_ID,
					SUM([RegularMinutes]) AS [RegularMinutes],
					SUM([OTMinutes]) AS [OvertimeMinutes],
					SUM([PayAmount] + OTAmount) AS [WagePayment],
					SUM([TotalOtherPay]) AS OtherPayment
			FROM	PayrollDetail d 
			WHERE	d.Payroll_ID = @Payroll_ID
			GROUP BY d.Payroll_ID
		) d ON d.Payroll_ID = s.Payroll_ID
		WHERE s.Client_ID = @Client_ID
			AND s.Payroll_ID = @Payroll_ID

		UPDATE [TimeRecords]
		SET [Payroll_ID] = @Payroll_ID
		WHERE [Client_ID] = @Client_ID
		AND	[StartTime] BETWEEN @StartDate AND @EndDate

		UPDATE [TimeRecords]
		SET [VerifiedBy] = @User_ID
		WHERE [Client_ID] = @Client_ID
			AND VerifiedBy = 0
			AND [StartTime] BETWEEN @StartDate AND @EndDate
			
		COMMIT TRAN

	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR ('Error in sprocSavePayrollSummary :%s',15,1,@ErrMsg)
	END CATCH
END


GO

/****** Object:  StoredProcedure [dbo].[sprocUpdatePreferences]    Script Date: 08/27/2014 18:40:58 ******/

IF object_ID(N'sprocUpdatePreferences')>0
	DROP PROCEDURE sprocUpdatePreferences
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/19/2014
-- Description:	Save Payroll Summary
-- =============================================
CREATE PROCEDURE [dbo].[sprocUpdatePreferences]
	@ClientID					BIGINT
	,@QuickClockIn				BIT
	,@QuickClockInPassword		BIT
	,@SelfRegistration			BIT
	,@SelfRegistrationPin		VARCHAR(20)
	,@WhosInRestriction			BIT
	,@PaidBreaks				BIT
	,@TimeRounding				TINYINT
	,@MileageRate				FLOAT
	,@DashboardMessageText		VARCHAR(255)
	,@DashboardMessageRed		BIT	
	,@DashboardMessageBold		BIT
	,@LoginMessageText			VARCHAR(255)
	,@LoginMessageRed			BIT
	,@LoginMessageBold			BIT
	,@BirthdayNotification		BIT
	,@OverTimetype				INT -- 1:Daily Overtime Pay ,2:Weekly Overtime Pay ,3:Both daily and weekly 
	,@OvertimeDailyHours		FLOAT = NULL
	,@OvertimeWeeklyHours		FLOAT = NULL
	,@ApproveOvertime			BIT 
	,@Subdomain					VARCHAR(50)
	,@IsSaved					BIGINT = 0 OUT
AS
BEGIN
	BEGIN TRY
		
		IF (SELECT COUNT(c.Client_ID) 
			FROM	Clients c
			WHERE	c.Subdomain = @Subdomain 
				AND c.Client_ID <> @ClientID) != 0
		BEGIN 
			SELECT @IsSaved = -1 
			RETURN
		END
		
		UPDATE	p
		SET		p.QuickClockIn = @QuickClockIn
				,p.QuickClockInPassword =@QuickClockInPassword
				,p.SelfRegistration = @SelfRegistration
				,p.SelfRegistrationPin = @SelfRegistrationPin
				,p.WhosInRestriction = @WhosInRestriction 
				,p.PaidBreaks  = @PaidBreaks
				,p.TimeRounding = @TimeRounding
				,p.MileageRate = @MileageRate
				,p.DashboardMessageText	 = @DashboardMessageText		
				,p.DashboardMessageRed	 = @DashboardMessageRed		
				,p.DashboardMessageBold	 = @DashboardMessageBold		
				,p.LoginMessageText		 = @LoginMessageText			
				,p.LoginMessageRed		 = @LoginMessageRed			
				,p.LoginMessageBold		 = @LoginMessageBold			
				,p.BirthdayNotification	 = @BirthdayNotification		
				,p.OvertimeType			 = @OverTimetype				
				,p.OvertimeDailyHours	 = @OvertimeDailyHours		
				,p.OvertimeWeeklyHours   = @OvertimeWeeklyHours	
				,p.ApproveOvertime        = @ApproveOvertime		
		FROM Preferences p
		WHERE p.Client_ID = @ClientID
			
		UPDATE	c
		SET		c.Subdomain = @Subdomain 
		FROM	Clients c
		WHERE   c.Client_ID = @ClientID
		
		SELECT @IsSaved = 1
		
		
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocUpdatePreferences :%s',15, 1, @ErrMsg)   
	END CATCH
END




GO

/****** Object:  StoredProcedure [dbo].[sprocCheckUserOKtoRemoveOverniteAttribute]    Script Date: 08/27/2014 18:40:58 ******/
IF object_ID(N'sprocCheckUserOKtoRemoveOverniteAttribute')>0
	DROP PROCEDURE sprocCheckUserOKtoRemoveOverniteAttribute
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Technobrains Solution
-- Updated date: 08/25/2014
-- Updated by  : chetan koriya
-- Description:	sprocCheckUserOKtoRemoveOverniteAttribute
-- =============================================
/*
	EXEC sprocCheckUserOKtoRemoveOverniteAttribute
		@ClientId	 = 1823
		,@UserId		= 11976
	
	
*/
CREATE PROCEDURE [dbo].[sprocCheckUserOKtoRemoveOverniteAttribute] 
	@ClientId	INT,
	@UserId		INT,
	@IsCount	INT = 0 OUT --Added by technobrains
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--check for current year and current week and above
	SELECT @IsCount = count(*)
	FROM Schedules
	WHERE Client_ID = @ClientId
	AND User_ID = @UserId
	AND year = Year(GETDATE())
	AND weeknum >= DATEPART( WW, GETDATE())  
	AND (
		CONVERT(VARCHAR(10),SunStart,111) < CONVERT(VARCHAR(10),SunEnd,111) or
		CONVERT(VARCHAR(10),MonStart,111) < CONVERT(VARCHAR(10),MonEnd,111) or
		CONVERT(VARCHAR(10),TueStart,111) < CONVERT(VARCHAR(10),TueEnd,111) or
		CONVERT(VARCHAR(10),WedStart,111) < CONVERT(VARCHAR(10),WedEnd,111) or
		CONVERT(VARCHAR(10),ThuStart,111) < CONVERT(VARCHAR(10),ThuEnd,111) or
		CONVERT(VARCHAR(10),FriStart,111) < CONVERT(VARCHAR(10),FriEnd,111) or
		CONVERT(VARCHAR(10),SatStart,111) < CONVERT(VARCHAR(10),SatEnd,111) 
		)

	--check for future year
	IF (@IsCount = 0)
	BEGIN
		SELECT @IsCount = count(*)
		FROM Schedules
		WHERE Client_ID = @ClientId
		AND User_ID = @UserId
		AND year > Year(GETDATE())
		AND (
			CONVERT(VARCHAR(10),SunStart,111) < CONVERT(VARCHAR(10),SunEnd,111) or
			CONVERT(VARCHAR(10),MonStart,111) < CONVERT(VARCHAR(10),MonEnd,111) or
			CONVERT(VARCHAR(10),TueStart,111) < CONVERT(VARCHAR(10),TueEnd,111) or
			CONVERT(VARCHAR(10),WedStart,111) < CONVERT(VARCHAR(10),WedEnd,111) or
			CONVERT(VARCHAR(10),ThuStart,111) < CONVERT(VARCHAR(10),ThuEnd,111) or
			CONVERT(VARCHAR(10),FriStart,111) < CONVERT(VARCHAR(10),FriEnd,111) or
			CONVERT(VARCHAR(10),SatStart,111) < CONVERT(VARCHAR(10),SatEnd,111) 
			)
	END
	
	SELECT @IsCount
END

GO

/****** Object:  StoredProcedure [dbo].[sprocSaveUsers]    Script Date: 08/27/2014 18:40:58 ******/
IF object_ID(N'sprocSaveUsers')>0
	DROP PROCEDURE sprocSaveUsers
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/22/2014
-- Description:	Create User
-- =============================================
CREATE PROCEDURE [dbo].[sprocSaveUsers]
	@ClientID					BIGINT
	,@UserID					BIGINT
	,@OfficeID					BIGINT
	,@Manager					BIT
	,@TZOffset					NUMERIC(3, 1)
	,@DST						BIT
	,@Overtime					TINYINT
	,@Username					VARCHAR(16)
	,@Password					VARCHAR(32)
	,@FirstName					VARCHAR(25)
	,@LastName					VARCHAR(25)
	,@Email						VARCHAR(50)
	,@HomePhone					VARCHAR(14)
	,@CellPhone					VARCHAR(14)
	,@WorkPhone					VARCHAR(14)
	,@WorkExt					VARCHAR(14)
	,@HomeAddress1				VARCHAR(50)
	,@HomeAddress2				VARCHAR(50)
	,@City						VARCHAR(25)	
	,@State						VARCHAR(2)
	,@ZIPCode					VARCHAR(5)
	,@TimeDisplay				BIT
	,@Birthday					SMALLDATETIME
	,@ClockGuard				BIT
	,@AllowedDeviation			INT
	,@MobileAccess				BIT
	,@CurrencyType				VARCHAR(50)
	,@Wage						NUMERIC(5, 2)
	,@Custom1Value				VARCHAR(50)
	,@Custom2Value				VARCHAR(50)
	,@Custom3Value				VARCHAR(50)
	,@Custom4Value				VARCHAR(50)
	,@Custom5Value				VARCHAR(50)
	,@OvernightEmployee			BIT
	,@ManagerUserId				BIGINT
	,@OverTimetype				INT -- 1:Daily Overtime Pay ,2:Weekly Overtime Pay ,3:Both daily and weekly 
	,@OvertimeDailyHours		FLOAT = NULL
	,@OvertimeWeeklyHours		FLOAT = NULL
	,@IsSaved					BIGINT = 0 OUT
	,@ApproveOvertime			BIT 
	
AS
BEGIN
	BEGIN TRY
		
		BEGIN TRAN
		IF @UserID = -1 BEGIN 
			INSERT INTO Users (Client_ID,
				Office_ID
				,Manager
				,TZOffset
				,DST
				,Overtime
				,Username
				,Password
				,FirstName
				,LastName
				,Email
				,HomePhone
				,CellPhone
				,WorkPhone
				,WorkExt
				,HomeAddress1
				,HomeAddress2
				,City
				,State
				,ZIPCode
				,TimeDisplay
				,Birthday
				,ClockGuard
				,AllowedDeviation
				,MobileAccess
				,CurrencyType
				,Wage
				,Custom1Value
				,Custom2Value
				,Custom3Value
				,Custom4Value
				,Custom5Value
				,OvernightEmployee
				,ManagerUserId
				,OvertimeType			
				,OvertimeDailyHours
				,OvertimeWeeklyHours
				,ApproveOvertime    
				)
			SELECT
				@ClientID,
				@OfficeID,
				@Manager,
				@TZOffset,
				@DST,
				@Overtime,
				@Username,
				@Password,
				@FirstName,
				@LastName,
				@Email,
				@HomePhone,
				@CellPhone,
				@WorkPhone,
				@WorkExt,
				@HomeAddress1,
				@HomeAddress2,
				@City,
				@State,
				@ZIPCode,
				@TimeDisplay,
				@Birthday,
				@ClockGuard,
				@AllowedDeviation,
				@MobileAccess,
				@CurrencyType,
				@Wage,
				@Custom1Value,
				@Custom2Value,
				@Custom3Value,
				@Custom4Value,
				@Custom5Value,
				@OvernightEmployee,
				@ManagerUserId,
				@OverTimetype,				
				@OvertimeDailyHours	,
				@OvertimeWeeklyHours,	
				@ApproveOvertime
				
			SELECT @UserID = SCOPE_IDENTITY()
			
			INSERT INTO AbsenceUserSettings (
				Client_ID
				,User_ID
				,VacationTotal
				,VacationUsed
				,VacationEnabled
				,VacationRate
				,HolidayTotal
				,HolidayUsed
				,HolidayEnabled
				,HolidayRate
				,SickDayTotal
				,SickDayUsed
				,SickDayEnabled
				,SickDayRate
				,PersonalTotal
				,PersonalUsed
				,PersonalEnabled
				,PersonalRate
				,PaidLeaveTotal
				,PaidLeaveUsed
				,PaidLeaveEnabled
				,PaidLeaveRate
				,OtherTotal
				,OtherUsed
				,OtherEnabled
				,OtherRate)
			SELECT 
				@ClientID
				,@UserID
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
			
			SELECT @IsSaved = @UserID
		END 
		ELSE BEGIN
			UPDATE	u
			SET		u.Office_ID				= @OfficeID
					,u.Manager				= @Manager
					,u.TZOffset				= @TZOffset
					,u.DST					= @DST
					,u.Overtime				= @Overtime
					,u.Username				= @Username
					,u.Password				= @Password
					,u.FirstName			= @FirstName
					,u.LastName				= @LastName
					,u.Email				= @Email
					,u.HomePhone			= @HomePhone
					,u.CellPhone			= @CellPhone
					,u.WorkPhone			= @WorkPhone
					,u.WorkExt				= @WorkExt
					,u.HomeAddress1 		= @HomeAddress1
					,u.HomeAddress2 		= @HomeAddress2
					,u.City					= @City
					,u.State				= @State
					,u.ZIPCode				= @ZIPCode
					,u.TimeDisplay			= @TimeDisplay
					,u.Birthday				= @Birthday
					,u.ClockGuard			= @ClockGuard
					,u.AllowedDeviation		= @AllowedDeviation
					,u.MobileAccess 		= @MobileAccess
					,u.CurrencyType 		= @CurrencyType
					,u.Wage					= @Wage
					,u.Custom1Value 		= @Custom1Value
					,u.Custom2Value 		= @Custom2Value
					,u.Custom3Value 		= @Custom3Value
					,u.Custom4Value 		= @Custom4Value
					,u.Custom5Value 		= @Custom5Value
					,u.OvernightEmployee	= @OvernightEmployee
					,u.ManagerUserId		= @ManagerUserId
					,u.OverTimetype			= @OverTimetype
					,u.OvertimeDailyHours	= @OvertimeDailyHours
					,u.OvertimeWeeklyHours	= @OvertimeWeeklyHours
					,u.ApproveOvertime		= @ApproveOvertime 
			FROM	Users u
			WHERE	u.User_ID = @UserID 
			AND		u.Client_ID = @ClientID
			SELECT	@IsSaved = @UserID
		END 
		COMMIT TRAN
		
	END TRY
	BEGIN CATCH	
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocSaveUsers :%s',15, 1, @ErrMsg)   
	END CATCH
END



GO

/****** Object:  StoredProcedure [dbo].[sprocGetUsersDetail]    Script Date: 08/27/2014 18:40:58 ******/
IF object_ID(N'sprocGetUsersDetail')>0
	DROP PROCEDURE sprocGetUsersDetail
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/23/2014
-- Description:	Create User
-- =============================================
/*
	EXEC sprocGetUsersDetail 1823,11973
		 
*/
CREATE PROCEDURE [dbo].[sprocGetUsersDetail]
	@ClientID		BIGINT
	,@UserID		BIGINT			
AS
BEGIN 
	BEGIN TRY
		EXEC sprocGetCustomFields
			@ClientID = @ClientID
			
		EXEC sprocSelectAllOffices
			@Client_ID = @ClientID
		
		EXEC sprocSelectManagers
			@Client_ID	= @ClientID
			,@User_ID	= @UserID
		SELECT  
			u.Office_ID
			,u.Active
			,u.Manager
			,u.SupportNumber
			,u.TZOffset
			,u.DST
			,u.Overtime
			,u.Username
			,u.FirstName
			,u.LastName
			,u.Email
			,u.HomePhone
			,u.CellPhone
			,u.WorkPhone
			,u.WorkExt
			,u.HomeAddress1
			,u.HomeAddress2
			,u.City
			,u.State
			,u.ZIPCode
			,u.Birthday
			,u.TimeDisplay
			,u.ClockGuard
			,u.CGAlertWhen
			,u.CGEmail
			,u.CGPhone
			,u.CGCarrier
			,u.AllowedDeviation
			,u.MobileAccess
			,u.Wage
			,u.DeletedOn
			,u.Custom1Value
			,u.Custom2Value
			,u.Custom3Value
			,u.Custom4Value
			,u.Custom5Value
			,u.TCA_ID
			,u.WalkthroughCompleted
			,u.HireDate
			,u.CurrencyType
			,u.OvernightEmployee
			,u.ManagerUserId
			,u.OverTimetype
			,(u.OvertimeDailyHours/60) AS OvertimeDailyHours
			,(u.OvertimeWeeklyHours/60) AS OvertimeWeeklyHours
			,u.ApproveOvertime
		FROM Users u
		WHERE u.User_ID = @UserID
		AND u.Client_ID = @ClientID
		
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocGetUsersDetail :%s',15, 1, @ErrMsg)   
	END CATCH
END

GO

/****** Object:  StoredProcedure [dbo].[sprocGetSelfRegistrationpin]    Script Date: 08/27/2014 18:40:58 ******/

IF object_ID(N'sprocGetSelfRegistrationpin')>0
	DROP PROCEDURE sprocGetSelfRegistrationpin
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/23/2014
-- Description:	Get CustomFields
-- =============================================
/*
	EXEC sprocGetSelfRegistrationpin 1823
*/
CREATE PROCEDURE [dbo].[sprocGetSelfRegistrationpin]
	@ClientID		BIGINT
AS
BEGIN 
	BEGIN TRY
		SELECT	
			p.SelfRegistration
			,p.SelfRegistrationPin 
		FROM Preferences p
		WHERE	p.Client_ID = @ClientID
		
		SELECT  p.OvertimeType
			,p.OvertimeDailyHours
			,p.OvertimeWeeklyHours
			,p.ApproveOvertime
		FROM Preferences p
		
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocGetSelfRegistrationpin :%s',15, 1, @ErrMsg)   
	END CATCH
END

GO

/****** Object:  StoredProcedure [dbo].[sprocUpdateUsers]    Script Date: 08/27/2014 18:40:58 ******/

IF object_ID(N'sprocUpdateUsers')>0
	DROP PROCEDURE sprocUpdateUsers
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/23/2014
-- Description:	Update User
-- =============================================
CREATE PROCEDURE [dbo].[sprocUpdateUsers]
	@ClientID					BIGINT
	,@UserID					BIGINT
	,@OfficeID					BIGINT
	,@Manager					BIT
	,@TZOffset					NUMERIC(3, 1)
	,@DST						BIT
	,@Overtime					TINYINT
	,@Username					VARCHAR(16)
	,@Password					VARCHAR(32)
	,@FirstName					VARCHAR(25)
	,@LastName					VARCHAR(25)
	,@Email						VARCHAR(50)
	,@HomePhone					VARCHAR(14)
	,@CellPhone					VARCHAR(14)
	,@WorkPhone					VARCHAR(14)
	,@WorkExt					VARCHAR(14)
	,@HomeAddress1				VARCHAR(50)
	,@HomeAddress2				VARCHAR(50)
	,@City						VARCHAR(25)	
	,@State						VARCHAR(2)
	,@ZIPCode					VARCHAR(5)
	,@TimeDisplay				BIT
	,@Birthday					SMALLDATETIME
	,@ClockGuard				BIT
	,@AllowedDeviation			INT
	,@MobileAccess				BIT
	,@CurrencyType				VARCHAR(50)
	,@Wage						NUMERIC(5, 2)
	,@Custom1Value				VARCHAR(50)
	,@Custom2Value				VARCHAR(50)
	,@Custom3Value				VARCHAR(50)
	,@Custom4Value				VARCHAR(50)
	,@Custom5Value				VARCHAR(50)
	,@OvernightEmployee			BIT
	,@ManagerUserId				BIGINT
	,@OverTimetype				INT -- 1:Daily Overtime Pay ,2:Weekly Overtime Pay ,3:Both daily and weekly 
	,@OvertimeDailyHours		FLOAT = NULL
	,@OvertimeWeeklyHours		FLOAT = NULL
	,@ApproveOvertime			BIT 
	,@IsSaved					BIGINT = 0 OUT
	
AS
BEGIN
	BEGIN TRY
		
		UPDATE	u
		SET		u.Office_ID				= @OfficeID
				,u.Manager				= @Manager
				,u.TZOffset				= @TZOffset
				,u.DST					= @DST
				,u.Overtime				= @Overtime
				,u.Username				= @Username
				,u.Password				= @Password
				,u.FirstName			= @FirstName
				,u.LastName				= @LastName
				,u.Email				= @Email
				,u.HomePhone			= @HomePhone
				,u.CellPhone			= @CellPhone
				,u.WorkPhone			= @WorkPhone
				,u.WorkExt				= @WorkExt
				,u.HomeAddress1 		= @HomeAddress1
				,u.HomeAddress2 		= @HomeAddress2
				,u.City					= @City
				,u.State				= @State
				,u.ZIPCode				= @ZIPCode
				,u.TimeDisplay			= @TimeDisplay
				,u.Birthday				= @Birthday
				,u.ClockGuard			= @ClockGuard
				,u.AllowedDeviation		= @AllowedDeviation
				,u.MobileAccess 		= @MobileAccess
				,u.CurrencyType 		= @CurrencyType
				,u.Wage					= @Wage
				,u.Custom1Value 		= @Custom1Value
				,u.Custom2Value 		= @Custom2Value
				,u.Custom3Value 		= @Custom3Value
				,u.Custom4Value 		= @Custom4Value
				,u.Custom5Value 		= @Custom5Value
				,u.OvernightEmployee	= @OvernightEmployee
				,u.ManagerUserId		= @ManagerUserId
				,u.OverTimetype			= @OverTimetype
				,u.OvertimeDailyHours	= @OvertimeDailyHours
				,u.OvertimeWeeklyHours	= @OvertimeWeeklyHours
				,u.ApproveOvertime		= @ApproveOvertime 
		FROM	Users u
		WHERE	u.User_ID = @UserID 
		AND		u.Client_ID = @ClientID
		SELECT	@IsSaved = 1
	END TRY
	BEGIN CATCH	
		SELECT	@IsSaved = -1
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocUpdateUsers :%s',15, 1, @ErrMsg)   
	END CATCH
END



GO

/****** Object:  StoredProcedure [dbo].[sprocGetCustomFields]    Script Date: 08/27/2014 18:40:59 ******/
IF object_ID(N'sprocGetCustomFields')>0
	DROP PROCEDURE sprocGetCustomFields
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/23/2014
-- Description:	Get CustomFields
-- =============================================
/*
	EXEC sprocGetCustomFields 1823,11973
*/
CREATE PROCEDURE [dbo].[sprocGetCustomFields]
	@ClientID		BIGINT
AS
BEGIN 
	BEGIN TRY
		SELECT	
			c.Custom1Enabled
			,c.Custom1Name
			,c.Custom1Mandatory
			,c.Custom1UserEdit
			,c.Custom2Enabled
			,c.Custom2Name
			,c.Custom2Mandatory
			,c.Custom2UserEdit
			,c.Custom3Enabled
			,c.Custom3Name
			,c.Custom3Mandatory
			,c.Custom3UserEdit
			,c.Custom4Enabled
			,c.Custom4Name
			,c.Custom4Mandatory
			,c.Custom4UserEdit
			,c.Custom5Enabled
			,c.Custom5Name
			,c.Custom5Mandatory
			,c.Custom5UserEdit
		FROM	CustomFields c
		WHERE	c.Client_ID = @ClientID
		
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocGetCustomFields :%s',15, 1, @ErrMsg)   
	END CATCH
END

GO

/****** Object:  StoredProcedure [dbo].[sprocSaveUserPermissions]    Script Date: 08/27/2014 18:40:59 ******/

IF object_ID(N'sprocSaveUserPermissions')>0
	DROP PROCEDURE sprocSaveUserPermissions
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/23/2014
-- Description:	Save User Permissions
-- =============================================
CREATE PROCEDURE [dbo].[sprocSaveUserPermissions]
	@UserID		BIGINT
	,@ClientID	BIGINT
AS
BEGIN
	BEGIN TRY
	
		INSERT INTO Permissions (
			User_Id
			,Client_ID)
		SELECT @UserID 
			,@ClientID
	
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocSaveUserPermissions :%s',15, 1, @ErrMsg)   
	END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[sprocDeleteUsersPermissions]    Script Date: 08/27/2014 18:40:59 ******/


IF object_ID(N'sprocDeleteUsersPermissions')>0
	DROP PROCEDURE sprocDeleteUsersPermissions
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Technobrains Solution
-- Create date: 08/23/2014
-- Description:	Delete User Permissions
-- =============================================
CREATE PROCEDURE [dbo].[sprocDeleteUsersPermissions]
	@UserID		BIGINT
	,@ClientID	BIGINT
AS
BEGIN
	BEGIN TRY
	
		DELETE 
		FROM	Permissions 
		WHERE	User_Id = @UserID 
		AND Client_Id = @ClientID
	
	END TRY
	BEGIN CATCH	
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR('Error in sprocDeleteUsersPermissions :%s',15, 1, @ErrMsg)   
	END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[sprocSelectManagers]    Script Date: 08/27/2014 18:40:59 ******/
IF object_ID(N'sprocSelectManagers')>0
	DROP PROCEDURE sprocSelectManagers
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--For given user returns list of managers who has permissions to manage employees.
--If user is manager and has permissions to manage, the return list excludes him/her.
--if user is account holder, the return list should be empty.
CREATE PROCEDURE [dbo].[sprocSelectManagers] 
	@Client_ID BIGINT,
	@User_ID BIGINT
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @iIsAccountHolder INT
	--if user is account hoder
	SELECT @iIsAccountHolder = COUNT(*) FROM Permissions WHERE User_ID = @User_ID and AccountHolder = 1
	IF (@iIsAccountHolder <> 1)
	BEGIN
		-- Insert statements for procedure here
		SELECT (U.FirstName + ' ' + U.LastName) as ManagerName, U.User_ID as Manager_User_ID
		FROM Users U inner join Permissions P
				on U.User_ID = P.User_ID
				and P.ManageEmployees = 1
		WHERE U.Client_ID = @Client_ID
				AND U.User_ID <> @User_ID
				AND U.manager = 1
		ORDER BY ManagerName ASC
	END
END

GO

/****** Object:  StoredProcedure [dbo].[sprocSelectAllOffices]    Script Date: 08/27/2014 18:40:59 ******/

IF object_ID(N'sprocSelectAllOffices')>0
	DROP PROCEDURE sprocSelectAllOffices
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sprocSelectAllOffices] 
	@Client_ID BIGINT
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT 
		Office_ID
		,Name
	FROM Offices
	WHERE Client_ID = @Client_ID
	ORDER BY Name ASC
END

GO

IF object_ID(N'sprocSavePayrollSummary')>0
	DROP PROCEDURE sprocSavePayrollSummary
GO

/****** Object:  StoredProcedure [dbo].[sprocSavePayrollSummary]    Script Date: 08/28/2014 14:43:46 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Technobrains Solution
-- Create date: 08/19/2014
-- Description:	Save Payroll Summary
-- =============================================
--sprocSavePayrollSummary 1823, 0, 0, '2007-08-10', '2007-08-16'
CREATE PROCEDURE [dbo].[sprocSavePayrollSummary] (
	@Client_ID BIGINT
	,@User_ID INT
	,@Office_Id BIGINT = 0
	,@StartDate DATETIME
	,@EndDate DATETIME
	,@Payroll_ID BIGINT = 0 OUT
	)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
		
		INSERT INTO PayrollSummary (
			Client_ID
			,Office_ID
			,StartDate
			,EndDate
			)
		SELECT @Client_ID
			,@Office_Id
			,@StartDate
			,@EndDate

		SELECT @Payroll_ID = SCOPE_IDENTITY()

		DECLARE @MileageRate		NUMERIC(18, 3)
		DECLARE @OverTimeMethod		INT,
				@MaxDailyHours		INT,
				@MaxWeeklyHours		INT,
				@PaidBreaks			BIT,
				@TimeRounding		INT,
				@OverTimeEnable		INT,
				@ApproveOvertime	INT
		
		SELECT	@OverTimeMethod		= ISNULL(u.OverTimetype,0)
				,@MaxDailyHours		= ISNULL(u.OvertimeDailyHours,0) 
				,@MaxWeeklyHours	= ISNULL(u.OvertimeWeeklyHours,0)
				,@OverTimeEnable	= ISNULL(u.Overtime,0)
		FROM	Users u
		WHERE	u.User_ID = @User_ID
		AND u.Client_ID = @Client_ID
		
		SELECT	@MileageRate	 = p.MileageRate,
				@PaidBreaks      =	ISNULL(p.PaidBreaks,0),
				@TimeRounding	 =	CASE ISNULL(p.TimeRounding,0) 
											WHEN 0 THEN 0 
											WHEN 1 THEN 15 
											WHEN 2 THEN 6 
										END
		FROM	dbo.Preferences p
		WHERE	p.Client_ID = @Client_ID
			
		IF @OverTimeMethod = 0 BEGIN
			SELECT	@OverTimeMethod  =	ISNULL(p.OvertimeType,0),
					@MaxDailyHours   =	ISNULL(p.OvertimeDailyHours,0), 
					@MaxWeeklyHours  =	ISNULL(p.OvertimeWeeklyHours,0),
					@OverTimeEnable  =  ISNULL(Overtime,0)
			FROM	dbo.Preferences p
			WHERE	p.Client_ID = @Client_ID
		END
		
		INSERT INTO PayrollDetail (
			Client_ID
			,User_ID
			,Payroll_ID
			,Wage
			,OTWage
			,RegularMinutes
			,OTMinutes
			,PayAmount
			,OTAmount
			,TotalOtherPay
			)
		SELECT @Client_ID
			,u.User_ID
			,@Payroll_ID
			,x.Wage
			,x.OTWage
			,x.Minutes
			,x.OTMinutes
			,x.PayAmount
			,x.OTPayAmount
			,x.TotalOtherPay
		FROM Users u
		CROSS APPLY dbo.fn_CalculatePayroll(u.User_ID, @StartDate, @EndDate, u.Wage, @OverTimeEnable, @OverTimeMethod, @MaxDailyHours, @MaxWeeklyHours, @MileageRate, @PaidBreaks, @TimeRounding) x
		WHERE u.Client_ID = @Client_ID

	
		UPDATE [PayrollSummary]
		SET [RegularMinutes] = d.RegularMinutes
			,[OvertimeMinutes] = d.OvertimeMinutes
			,[WagePayment] = d.WagePayment
			,OtherPayment = d.OtherPayment
		FROM	[PayrollSummary] s
		INNER JOIN (
			SELECT	d.Payroll_ID,
					SUM([RegularMinutes]) AS [RegularMinutes],
					SUM([OTMinutes]) AS [OvertimeMinutes],
					SUM([PayAmount] + OTAmount) AS [WagePayment],
					SUM([TotalOtherPay]) AS OtherPayment
			FROM	PayrollDetail d 
			WHERE	d.Payroll_ID = @Payroll_ID
			GROUP BY d.Payroll_ID
		) d ON d.Payroll_ID = s.Payroll_ID
		WHERE s.Client_ID = @Client_ID
			AND s.Payroll_ID = @Payroll_ID

		UPDATE [TimeRecords]
		SET [Payroll_ID] = @Payroll_ID
		WHERE [Client_ID] = @Client_ID
		AND	[StartTime] BETWEEN @StartDate AND @EndDate

		UPDATE [TimeRecords]
		SET [VerifiedBy] = @User_ID
		WHERE [Client_ID] = @Client_ID
			AND VerifiedBy = 0
			AND [StartTime] BETWEEN @StartDate AND @EndDate
			
		COMMIT TRAN

	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN
		DECLARE @ErrMsg VARCHAR(MAX)
		SELECT @ErrMsg = ERROR_MESSAGE()
		RAISERROR ('Error in sprocSavePayrollSummary :%s',15,1,@ErrMsg)
	END CATCH
END


GO


IF object_ID(N'fn_CalculatePayroll')>0
	DROP FUNCTION fn_CalculatePayroll
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CalculatePayroll]    Script Date: 08/28/2014 14:27:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:		Technobrains IT Solution Pvt.LTD
-- Create date: 08/19/2014
-- Description:	payroll Calculation for Daily OR Weekly of Employee
-- =========================================================================
/*

SELECT *
FROM dbo.fn_CalculatePayroll(11662, '2014-04-27','2014-06-02', 8.00, 1, 1, 480,0, 0.585, 0, 0)

SELECT *
FROM dbo.fn_CalculatePayroll(1801, '2007-08-17','2007-08-23', 10.00, 1, 1, 480,0, 0.585, 0, 0)

SELECT *
FROM dbo.fn_CalculatePayroll(1801, '2007-08-17','2007-08-23', 10.00, 1, 2,0, 2400, 0.585, 0, 0)

SELECT *
FROM dbo.fn_CalculatePayroll(1801, '2007-08-17','2007-08-23', 10.00, 1, 3, 480,2400, 0.585, 0, 0)

*/
CREATE FUNCTION [dbo].[fn_CalculatePayroll] (
	@UserID INT
	,@StartDate DATE
	,@EndDate DATE
	,@Wage NUMERIC(18, 2)
	,@OverTimeEnable INT
	,@OverTimeMethod INT -- 0=No Overtime, 1=Daily Overtime, 2=Weekly Overtime, 3=Daily and weekly(both) Overtime 
	,@MaxDailyHours INT
	,@MaxWeeklyHours INT
	,@MileageRate NUMERIC(18, 3)
	,@PaidBreaks BIT
	,@TimeRounding INT -- 0=Disabled, use exact time, 15=Enabled, round all times to the nearest 1/4 hour (15 minute rule), 6=Enabled, round all times to the nearest 1/10 hour (6 minute rule)
	)
RETURNS @summary TABLE (
--	RETURNS @RESULT TABLE (
	UserID INT
	,Minutes INT
	,OTMinutes INT
	,Wage NUMERIC(18, 2)
	,OTWage NUMERIC(18, 2)
	,PayAmount NUMERIC(18, 2)
	,OTPayAmount NUMERIC(18, 2)
	,TotalOtherPay NUMERIC(18, 2)
	)
AS
BEGIN

	SELECT @EndDate = DATEADD(d,1,@EndDate)
	DECLARE @Details TABLE (
		AutoID INT IDENTITY(1, 1)
		,UserID INT
		,DATE DATETIME
		,StartTime DATETIME
		,EndTime DATETIME
		,Minutes INT
		)
	DECLARE @result TABLE (
	--	DECLARE @summary TABLE (
		UserID INT
		,Minutes INT
		,OTMinutes INT
		,Wage NUMERIC(18, 2)
		,OTWage NUMERIC(18, 2)
		,PayAmount NUMERIC(18, 2)
		,OTPayAmount NUMERIC(18, 2)
		,TotalOtherPay NUMERIC(18, 2)
		)
	DECLARE @TotalMinutes INT
		,@OTWage NUMERIC(18, 2)
	DECLARE @cnt INT
		,@inc INT
	DECLARE @Minutes INT
	DECLARE @Mileage INT
	DECLARE @MaxHours INT
	DECLARE @WeeklyBased BIT = 0

	SELECT @Mileage = SUM([Miles])
	FROM [dbo].[Mileage]
	WHERE [User_ID] = @UserID
		AND [DateTime] BETWEEN @StartDate
			AND @EndDate

	SELECT @Mileage = ISNULL(@Mileage, 0)

	SELECT @OTWage = @Wage * 1.5

	INSERT INTO @Details (
		UserID
		,DATE
		--,StartTime
		--,EndTime
		,Minutes
		)
	SELECT t.User_ID
		,CAST(t.StartTime AS DATE) AS DATE
		--,t.StartTime
		--,t.EndTime
		,SUM(CASE 
			WHEN t.Type = 2 --break Time
				THEN (
						CASE 
							WHEN @PaidBreaks = 1
								THEN 0
							ELSE - DATEDIFF(mi, dbo.RoundTime(t.StartTime, @TimeRounding), dbo.RoundTime(t.EndTime, @TimeRounding))
							END
						)
			ELSE DATEDIFF(mi, dbo.RoundTime(t.StartTime, @TimeRounding), dbo.RoundTime(t.EndTime, @TimeRounding))
			END) AS Minutes
	FROM TimeRecords t(NOLOCK)
	WHERE t.User_ID = @UserID
		AND t.StartTime BETWEEN @StartDate
			AND @EndDate
		AND t.Payroll_ID = 0
		AND t.Type IN (
			1
			,2
			,4
			) --1=Work Time, 2=BreakTime, 4=AbsenceTime
	GROUP BY t.User_ID,CAST(t.StartTime AS DATE)
	ORDER BY Minutes

	SELECT @TotalMinutes = SUM(Minutes)
		,@cnt = COUNT(1)
	FROM @Details

	-- SET MaxHours as per OverTimeMethod - Daily / Weekly
	IF @OverTimeMethod = 2
		OR (
			@OverTimeMethod = 3
			AND @TotalMinutes >= @MaxWeeklyHours
			)
	BEGIN
		SET @WeeklyBased = 1
		SET @MaxHours = @MaxWeeklyHours
	END
	ELSE
	BEGIN
		SET @WeeklyBased = 0
		SET @MaxHours = @MaxDailyHours
	END

	IF @OverTimeEnable = 0
		OR @OverTimeMethod = 0
		OR @TotalMinutes <= @MaxHours
	BEGIN
		INSERT INTO @result (
			UserID
			,Minutes
			,OTMinutes
			,Wage
			,OTWage
			,PayAmount
			,OTPayAmount
			)
		SELECT UserID
			,Minutes
			,0
			,@Wage
			,@OTWage
			,(Minutes * @Wage) / 60.0
			,0
		FROM @Details
	END
	ELSE
	BEGIN
		SET @inc = 1

		WHILE @inc <= @cnt
		BEGIN
			SELECT @Minutes = 0

			SELECT @Minutes = d.Minutes
			FROM @Details d
			WHERE AutoID = @inc

			IF @MaxHours > @Minutes
			BEGIN
				INSERT INTO @result (
					UserID
					,Minutes
					,OTMinutes
					,Wage
					,OTWage
					,PayAmount
					,OTPayAmount
					)
				SELECT @UserID
					,@Minutes
					,0
					,@Wage
					,@OTWage
					,(@Minutes * @Wage) / 60.0
					,0

				IF @WeeklyBased = 1
					SET @MaxHours = @MaxHours - @Minutes
			END
			ELSE
			BEGIN
				INSERT INTO @result (
					UserID
					,Minutes
					,OTMinutes
					,Wage
					,OTWage
					,PayAmount
					,OTPayAmount
					)
				SELECT @UserID
					,@MaxHours
					,@Minutes - @MaxHours
					,@Wage
					,@OTWage
					,(@MaxHours * @Wage) / 60.0
					,((@Minutes - @MaxHours) * @otWage) / 60.0

				IF @WeeklyBased = 1
					SET @MaxHours = 0
			END

			SET @inc = @inc + 1
		END
	END

	INSERT INTO @Summary (
		UserID
		,Minutes
		,OTMinutes
		,Wage
		,OTWage
		,PayAmount
		,OTPayAmount
		,TotalOtherPay
		)
	SELECT UserID
		,SUM(Minutes)
		,SUM(OTMinutes)
		,@Wage
		,@OTWage
		,SUM(PayAmount)
		,SUM(OTPayAmount)
		,(@Mileage * @MileageRate)
	FROM @result
	GROUP BY UserID

	RETURN
END
--============================ END FUNCTIONS =======================================================================================