<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="UserEdit.aspx.vb" Inherits="UserEdit" Title="TimeClockWizard - Edit User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="main-content">
                <div class="edit-user">
                    <strong>Edit
                        <asp:Literal ID="litUsersName" runat="server"></asp:Literal></strong>
                </div>
                <br />
                <br />
                <div class="box1-info">
                    <h4>
                        mandatory information</h4>
                    <div class="form-holder" style="padding-bottom: 0px;">
                        <div class="field-holder">
                            <label>
                                username</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtUsername" MaxLength="16" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                password</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtPassword" MaxLength="32" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                confirm password</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder" style="margin-top: 20px;">
                            <label>
                                first name</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtFirstName" MaxLength="25" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                last name</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtLastName" MaxLength="25" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder" style="margin: 25px 0;">
                            <label>
                                email address</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtEmail" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <asp:Image ID="imgHelpEmail" runat="server" ImageAlign="Top" ImageUrl="~/images/help_small.png"
                                Style="position: relative; top: 3px;" />
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                overtime</label>
                            <asp:DropDownList ID="ddlOT" runat="server">
                                <asp:ListItem Value="1">Enabled (40 Hr Rule)</asp:ListItem>
                                <asp:ListItem Value="0">Disabled</asp:ListItem>
                            </asp:DropDownList>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                time zone</label>
                            <asp:DropDownList ID="ddlTimeZone" runat="server" OnSelectedIndexChanged="ddlTimeZone_SelectedIndexChanged"
                                AutoPostBack="True">
                                <asp:ListItem Value="-10.0">(GMT-10:00) Hawaii</asp:ListItem>
                                <asp:ListItem Value="-9.0">(GMT-09:00) Alaska</asp:ListItem>
                                <asp:ListItem Value="-8.0">(GMT-08:00) Pacific Time</asp:ListItem>
                                <asp:ListItem Value="-7.0">(GMT-07:00) Mountain Time</asp:ListItem>
                                <asp:ListItem Value="-6.0">(GMT-06:00) Central Time</asp:ListItem>
                                <asp:ListItem Value="-5.0">(GMT-05:00) Eastern Time</asp:ListItem>
                                <asp:ListItem Value="-4.0">(GMT-04:00) Atlantic Time</asp:ListItem>
                                <asp:ListItem Value="-3.5">(GMT-03:30)</asp:ListItem>
                                <asp:ListItem Value="-3.0">(GMT-03:00)</asp:ListItem>
                                <asp:ListItem Value="-2.0">(GMT-02:00) </asp:ListItem>
                                <asp:ListItem Value="-1.0">(GMT-01:00)</asp:ListItem>
                                <asp:ListItem Value="0.0">(GMT)</asp:ListItem>
                                <asp:ListItem Value="1.0">(GMT+01:00)</asp:ListItem>
                                <asp:ListItem Value="2.0">(GMT+02:00)</asp:ListItem>
                                <asp:ListItem Value="3.0">(GMT+03:00)</asp:ListItem>
                                <asp:ListItem Value="3.5">(GMT+03:30)</asp:ListItem>
                                <asp:ListItem Value="4.0">(GMT+04:00)</asp:ListItem>
                                <asp:ListItem Value="4.5">(GMT+04:30)</asp:ListItem>
                                <asp:ListItem Value="5.0">(GMT+05:00)</asp:ListItem>
                                <asp:ListItem Value="5.3">(GMT+05:30) India Standard Time Zone</asp:ListItem>
                                <asp:ListItem Value="6.0">(GMT+06:00)</asp:ListItem>
                                <asp:ListItem Value="6.5">(GMT+06:30)</asp:ListItem>
                                <asp:ListItem Value="7.0">(GMT+07:00)</asp:ListItem>
                                <asp:ListItem Value="8.0">(GMT+08:00)</asp:ListItem>
                                <asp:ListItem Value="9.0">(GMT+09:00)</asp:ListItem>
                                <asp:ListItem Value="9.5">(GMT+09:30)</asp:ListItem>
                                <asp:ListItem Value="10.0">(GMT+10:00)</asp:ListItem>
                                <asp:ListItem Value="11.0">(GMT+11:00)</asp:ListItem>
                                <asp:ListItem Value="12.0">(GMT+12:00)</asp:ListItem>
                                <asp:ListItem Value="13.0">(GMT+13:00)</asp:ListItem>
                            </asp:DropDownList>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder" style="margin-bottom: 40px;">
                            <!-- checkbox -->
                            <label style="margin-right: 17px;">
                                Daylight Savings Time</label>
                            <asp:CheckBox ID="chkDST" runat="server" />
                        </div>
                        <asp:Panel ID="pnlRequiredCheckBoxes" runat="server">
                            <div style="padding-bottom:15px;">
                                <div>
                                    <label style="margin-right: 77px; line-height: 19px;">
                                        manager</label>
                                    <asp:CheckBox ID="chkManager" runat="server" />
                                </div>
                                <div>
                                    <label style="margin-right: 59px; line-height: 19px;">
                                        clockguard</label>
                                    <asp:CheckBox ID="chkClockGuard" runat="server" />
                                </div>
                                <div>
                                    <label style="margin-right: 37px; line-height: 19px;">
                                        mobile access</label>
                                    <asp:CheckBox ID="chkMobileAccess" runat="server" />
                                </div>
                                <div style="position: relative">
                                    <label style="margin-right: 10px; line-height: 19px;">
                                        Allow User Work Overnight</label>
                                    <asp:CheckBox ID="chkEmployeeWorksOvernight" runat="server" AutoPostBack="true" />&nbsp;<asp:Image
                                        ID="imageEmployeeWorksOvernight" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png"
                                        runat="server" />
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlMandCust1" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblMandCust1" runat="server" Text="Custom Field 1"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtMandCust1" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlMandCust2" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblMandCust2" runat="server" Text="Custom Field 2"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtMandCust2" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlMandCust3" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblMandCust3" runat="server" Text="Custom Field 3"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtMandCust3" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlMandCust4" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblMandCust4" runat="server" Text="Custom Field 4"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtMandCust4" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlMandCust5" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblMandCust5" runat="server" Text="Custom Field 5"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtMandCust5" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <!-- end of formholder -->
                </div>
                <!-- end of mandatory -->
                <div class="box2-info">
                    <h4>
                        optional information</h4>
                    <div class="form-holder" style="padding-bottom: 0px;">
                        <div class="field-holder">
                            <label>
                                address line 1</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtAddress1" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                address line 2</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtAddress2" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder" style="margin-bottom: 20px;">
                            <label>
                                city, state, zip</label>
                            <span class="left" style="margin-left: 4px;">
                                <div class="right">
                                    <asp:TextBox ID="txtCity" MaxLength="25" Width="70px" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtState" MaxLength="2" Width="20px" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtZip" MaxLength="5" Width="40px" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                home phone</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtHomePhone" MaxLength="14" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                cell phone</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtCellPhone" MaxLength="14" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                work phone</label>
                            <span class="left">
                                <div class="right">
                                    <asp:TextBox ID="txtWorkPhone" MaxLength="14" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder" style="margin-bottom: 25px;">
                            <label>
                                work extention</label>
                            <span class="left" style="float: none; margin-left: 32px;">
                                <div class="right">
                                    <asp:TextBox ID="txtWorkExt" Width="40" MaxLength="5" runat="server"></asp:TextBox>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                office</label>
                            <span class="left" style="float: none; margin-left: 97px;">
                                <div class="right">
                                    <asp:DropDownList ID="drpOffice" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                manager</label>
                            <span class="left" style="float: none; margin-left: 77px;">
                                <div class="right">
                                    <asp:DropDownList ID="drpManagersList" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <asp:Panel ID="pnlWageAndDeviation" runat="server">
                            <div class="field-holder">
                                <label>
                                    wage per hour</label>
                                <span class="left" style="float: none; margin-left: 34px;">
                                    <div class="right">
                                        <asp:DropDownList ID="ddlCurrencyType" runat="server">
                                            <asp:ListItem Value="0" Selected="True">$</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </span><span class="left" style="float: none;">
                                    <div class="right">
                                        <asp:TextBox ID="txtWage" MaxLength="7" Width="50" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="field-holder">
                                <label>
                                    allowed deviation</label>
                                <span class="left" style="float: none; margin-left: 13px;">
                                    <div class="right">
                                        <asp:TextBox ID="txtAllowedDeviation" MaxLength="4" Width="35" runat="server"></asp:TextBox>&nbsp;&nbsp;Minutes&nbsp;<asp:Image
                                            ID="imgHelpAllowedDeviation" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png"
                                            runat="server" />
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="field-holder">
                            <label>
                                time display</label>
                            <span class="left" style="float: none; margin-left: 52px;">
                                <div class="right">
                                    <asp:DropDownList ID="drpTimeDisplay" runat="server">
                                        <asp:ListItem Value="False">Hours:Minutes (6:30)</asp:ListItem>
                                        <asp:ListItem Value="True">Decimal (6.5)</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="field-holder">
                            <label>
                                Birthday</label>
                            <span class="left" style="float: none; margin-left: 52px;">
                                <div class="right">
                                    <asp:DropDownList ID="drpMonth" runat="server">
                                        <asp:ListItem Value="0" Text=""></asp:ListItem>
                                        <asp:ListItem Value="1">Jan</asp:ListItem>
                                        <asp:ListItem Value="2">Feb</asp:ListItem>
                                        <asp:ListItem Value="3">Mar</asp:ListItem>
                                        <asp:ListItem Value="4">Apr</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">Jun</asp:ListItem>
                                        <asp:ListItem Value="7">Jul</asp:ListItem>
                                        <asp:ListItem Value="8">Aug</asp:ListItem>
                                        <asp:ListItem Value="9">Sep</asp:ListItem>
                                        <asp:ListItem Value="10">Oct</asp:ListItem>
                                        <asp:ListItem Value="11">Nov</asp:ListItem>
                                        <asp:ListItem Value="12">Dec</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="drpDay" runat="server">
                                        <asp:ListItem Value="0" Text=""></asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                        <asp:ListItem Value="9">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="11">11</asp:ListItem>
                                        <asp:ListItem Value="12">12</asp:ListItem>
                                        <asp:ListItem Value="13">13</asp:ListItem>
                                        <asp:ListItem Value="14">14</asp:ListItem>
                                        <asp:ListItem Value="15">15</asp:ListItem>
                                        <asp:ListItem Value="16">16</asp:ListItem>
                                        <asp:ListItem Value="17">17</asp:ListItem>
                                        <asp:ListItem Value="18">18</asp:ListItem>
                                        <asp:ListItem Value="19">19</asp:ListItem>
                                        <asp:ListItem Value="20">20</asp:ListItem>
                                        <asp:ListItem Value="21">21</asp:ListItem>
                                        <asp:ListItem Value="22">22</asp:ListItem>
                                        <asp:ListItem Value="23">23</asp:ListItem>
                                        <asp:ListItem Value="24">24</asp:ListItem>
                                        <asp:ListItem Value="25">25</asp:ListItem>
                                        <asp:ListItem Value="26">26</asp:ListItem>
                                        <asp:ListItem Value="27">27</asp:ListItem>
                                        <asp:ListItem Value="28">28</asp:ListItem>
                                        <asp:ListItem Value="29">29</asp:ListItem>
                                        <asp:ListItem Value="30">30</asp:ListItem>
                                        <asp:ListItem Value="31">31</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="drpYear" runat="server">
                                        <asp:ListItem Value="0" Text=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <asp:Panel ID="pnlCust1" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblCust1" runat="server" Text="Custom Field 1"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtCust1" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlCust2" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblCust2" runat="server" Text="Custom Field 2"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtCust2" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlCust3" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblCust3" runat="server" Text="Custom Field 3"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtCust3" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlCust4" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblCust4" runat="server" Text="Custom Field 4"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtCust4" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlCust5" Visible="false" runat="server">
                            <div class="field-holder">
                                <label>
                                    <asp:Label ID="lblCust5" runat="server" Text="Custom Field 5"></asp:Label></label>
                                <span class="left">
                                    <div class="right">
                                        <asp:TextBox ID="txtCust5" MaxLength="25" runat="server"></asp:TextBox>
                                    </div>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <!-- end of optional -->
                <!--                    <tr><td colspan="2">&nbsp;</td></tr>
                        <tr>
                            <td align="right" style="height: 20px; text-align: right">
                                Hire Date:
                           </td>
                            <td style="height: 20px; text-align: left">
                                <asp:DropDownList ID="ddlHireDateMonth" runat="server">
                                    <asp:ListItem Value="0" Text=""></asp:ListItem>
                                    <asp:ListItem Value="1">Jan</asp:ListItem>
                                    <asp:ListItem Value="2">Feb</asp:ListItem>
                                    <asp:ListItem Value="3">Mar</asp:ListItem>
                                    <asp:ListItem Value="4">Apr</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">Jun</asp:ListItem>
                                    <asp:ListItem Value="7">Jul</asp:ListItem>
                                    <asp:ListItem Value="8">Aug</asp:ListItem>
                                    <asp:ListItem Value="9">Sep</asp:ListItem>
                                    <asp:ListItem Value="10">Oct</asp:ListItem>
                                    <asp:ListItem Value="11">Nov</asp:ListItem>
                                    <asp:ListItem Value="12">Dec</asp:ListItem>
                                </asp:DropDownList>
                                    
                                <asp:DropDownList ID="ddlHireDateDay" runat="server">
                                    <asp:ListItem Value="0" Text=""></asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                    <asp:ListItem Value="6">6</asp:ListItem>
                                    <asp:ListItem Value="7">7</asp:ListItem>
                                    <asp:ListItem Value="8">8</asp:ListItem>
                                    <asp:ListItem Value="9">9</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                    <asp:ListItem Value="13">13</asp:ListItem>
                                    <asp:ListItem Value="14">14</asp:ListItem>
                                    <asp:ListItem Value="15">15</asp:ListItem>
                                    <asp:ListItem Value="16">16</asp:ListItem>
                                    <asp:ListItem Value="17">17</asp:ListItem>
                                    <asp:ListItem Value="18">18</asp:ListItem>
                                    <asp:ListItem Value="19">19</asp:ListItem>
                                    <asp:ListItem Value="20">20</asp:ListItem>
                                    <asp:ListItem Value="21">21</asp:ListItem>
                                    <asp:ListItem Value="22">22</asp:ListItem>
                                    <asp:ListItem Value="23">23</asp:ListItem>
                                    <asp:ListItem Value="24">24</asp:ListItem>
                                    <asp:ListItem Value="25">25</asp:ListItem>
                                    <asp:ListItem Value="26">26</asp:ListItem>
                                    <asp:ListItem Value="27">27</asp:ListItem>
                                    <asp:ListItem Value="28">28</asp:ListItem>
                                    <asp:ListItem Value="29">29</asp:ListItem>
                                    <asp:ListItem Value="30">30</asp:ListItem>
                                    <asp:ListItem Value="31">31</asp:ListItem>
                                </asp:DropDownList>
                                
                                <asp:DropDownList ID="ddlHireDateYear" runat="server">
                                    <asp:ListItem Value="0" Text=""></asp:ListItem>
                                </asp:DropDownList>
                           </td>
                        </tr>
                        <tr><td colspan="2">&nbsp;</td></tr> -->
                <div class="box1-info">
                    <h4>
                        overtime information</h4>
                    <div class="form-holder" style="padding: 10px;">
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 0px;">
                            <div class="radio-btn">
                                <asp:RadioButtonList ID="rbtnlistOverTime" runat="server" AutoPostBack="true">
                                    <asp:ListItem Value="1" Text="Daily Overtime Pay"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Weekly Overtime Pay"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Both (Daily and Weekly)"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div id="divDailyhours" runat="server">
                                &nbsp;&nbsp;&nbsp;&nbsp; <span style="color: #454545; display: inline-block; font-family: arial;
                                    font-size: 14px; font-weight: bold; line-height: 27px; text-transform: capitalize;">
                                    Daily overtime will be applied after</span>
                                <asp:DropDownList ID="ddlDailyHours" runat="server">
                                    <asp:ListItem Value="0" Text="0"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                    <asp:ListItem Selected="True" Value="8" Text="8"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                    <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                    <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                    <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                    <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                    <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                    <asp:ListItem Value="18" Text="18"></asp:ListItem>
                                    <asp:ListItem Value="19" Text="19"></asp:ListItem>
                                    <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                    <asp:ListItem Value="21" Text="21"></asp:ListItem>
                                    <asp:ListItem Value="22" Text="22"></asp:ListItem>
                                    <asp:ListItem Value="23" Text="23"></asp:ListItem>
                                    <asp:ListItem Value="24" Text="24"></asp:ListItem>
                                </asp:DropDownList>
                                <span style="color: #454545; display: inline-block; font-family: arial; font-size: 14px;
                                    font-weight: bold; line-height: 27px; text-transform: capitalize;">hours</span>
                            </div>
                            <div id="divWeeklyhours" runat="server">
                                &nbsp;&nbsp;&nbsp;&nbsp; <span style="color: #454545; display: inline-block; font-family: arial;
                                    font-size: 14px; font-weight: bold; line-height: 27px; text-transform: capitalize;">
                                    Weekly overtime will be applied after</span>
                                <asp:DropDownList ID="ddlWeeklyHours" runat="server">
                                    <asp:ListItem Value="0" Text="0"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                    <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                    <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                    <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                    <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                    <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                    <asp:ListItem Value="18" Text="18"></asp:ListItem>
                                    <asp:ListItem Value="19" Text="19"></asp:ListItem>
                                    <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                    <asp:ListItem Value="21" Text="21"></asp:ListItem>
                                    <asp:ListItem Value="22" Text="22"></asp:ListItem>
                                    <asp:ListItem Value="23" Text="23"></asp:ListItem>
                                    <asp:ListItem Value="24" Text="24"></asp:ListItem>
                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>
                                    <asp:ListItem Value="26" Text="26"></asp:ListItem>
                                    <asp:ListItem Value="27" Text="27"></asp:ListItem>
                                    <asp:ListItem Value="28" Text="28"></asp:ListItem>
                                    <asp:ListItem Value="29" Text="29"></asp:ListItem>
                                    <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                    <asp:ListItem Value="31" Text="31"></asp:ListItem>
                                    <asp:ListItem Value="32" Text="32"></asp:ListItem>
                                    <asp:ListItem Value="33" Text="33"></asp:ListItem>
                                    <asp:ListItem Value="34" Text="34"></asp:ListItem>
                                    <asp:ListItem Value="35" Text="35"></asp:ListItem>
                                    <asp:ListItem Value="36" Text="36"></asp:ListItem>
                                    <asp:ListItem Value="37" Text="37"></asp:ListItem>
                                    <asp:ListItem Value="38" Text="38"></asp:ListItem>
                                    <asp:ListItem Value="39" Text="39"></asp:ListItem>
                                    <asp:ListItem Selected="True" Value="40" Text="40"></asp:ListItem>
                                    <asp:ListItem Value="41" Text="41"></asp:ListItem>
                                    <asp:ListItem Value="42" Text="42"></asp:ListItem>
                                    <asp:ListItem Value="43" Text="43"></asp:ListItem>
                                    <asp:ListItem Value="44" Text="44"></asp:ListItem>
                                    <asp:ListItem Value="45" Text="45"></asp:ListItem>
                                    <asp:ListItem Value="46" Text="46"></asp:ListItem>
                                    <asp:ListItem Value="47" Text="47"></asp:ListItem>
                                    <asp:ListItem Value="48" Text="48"></asp:ListItem>
                                    <asp:ListItem Value="48" Text="48"></asp:ListItem>
                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                    <asp:ListItem Value="51" Text="51"></asp:ListItem>
                                    <asp:ListItem Value="52" Text="52"></asp:ListItem>
                                    <asp:ListItem Value="53" Text="53"></asp:ListItem>
                                    <asp:ListItem Value="54" Text="54"></asp:ListItem>
                                    <asp:ListItem Value="55" Text="55"></asp:ListItem>
                                    <asp:ListItem Value="56" Text="56"></asp:ListItem>
                                    <asp:ListItem Value="57" Text="57"></asp:ListItem>
                                    <asp:ListItem Value="58" Text="58"></asp:ListItem>
                                    <asp:ListItem Value="59" Text="59"></asp:ListItem>
                                    <asp:ListItem Value="60" Text="60"></asp:ListItem>
                                    <asp:ListItem Value="61" Text="61"></asp:ListItem>
                                    <asp:ListItem Value="62" Text="62"></asp:ListItem>
                                    <asp:ListItem Value="63" Text="63"></asp:ListItem>
                                    <asp:ListItem Value="64" Text="64"></asp:ListItem>
                                    <asp:ListItem Value="65" Text="65"></asp:ListItem>
                                    <asp:ListItem Value="66" Text="66"></asp:ListItem>
                                    <asp:ListItem Value="67" Text="67"></asp:ListItem>
                                    <asp:ListItem Value="68" Text="68"></asp:ListItem>
                                    <asp:ListItem Value="69" Text="69"></asp:ListItem>
                                    <asp:ListItem Value="70" Text="70"></asp:ListItem>
                                    <asp:ListItem Value="71" Text="71"></asp:ListItem>
                                    <asp:ListItem Value="72" Text="72"></asp:ListItem>
                                    <asp:ListItem Value="73" Text="73"></asp:ListItem>
                                    <asp:ListItem Value="74" Text="74"></asp:ListItem>
                                    <asp:ListItem Value="75" Text="75"></asp:ListItem>
                                    <asp:ListItem Value="76" Text="76"></asp:ListItem>
                                    <asp:ListItem Value="77" Text="77"></asp:ListItem>
                                    <asp:ListItem Value="78" Text="78"></asp:ListItem>
                                    <asp:ListItem Value="79" Text="79"></asp:ListItem>
                                    <asp:ListItem Value="80" Text="80"></asp:ListItem>
                                </asp:DropDownList>
                                <span style="color: #454545; display: inline-block; font-family: arial; font-size: 14px;
                                    font-weight: bold; line-height: 27px; text-transform: capitalize;">hours</span>
                            </div>
                            <br />
                            <div>
                                <span style="color: #454545; display: inline-block; font-family: arial; font-size: 14px;
                                    font-weight: bold; line-height: 27px; text-transform: capitalize;">
                                    <asp:CheckBox ID="chkAproveOverTime" runat="server" Text="Approve OverTime" />
                                </span>
                            </div>
                        </div>
                        <%-- <h5>
                            Mileage</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <span>Pay</span> <span class="left" style="float: none;">
                                <div class="right">
                                    <asp:TextBox ID="txtMileagePay" Width="30" MaxLength="5" runat="server"></asp:TextBox>
                                </div>
                            </span><span>cents per mile</span>
                        </div>--%>
                        <%--  <div>
                            <asp:Button ID="btnSaveCompensation" runat="server" Text="Save All Settings" OnClick="btnSaveCompensation_Click"
                                CssClass="save-all-setting" />
                            <asp:Label ID="lblCompensationMessage" ForeColor="green" runat="server"></asp:Label></div>--%>
                    </div>
                </div>

                <div class="clear">
                </div>
                <div style="position:relative;left:190px; width:150px;">
                    <asp:Button ID="btnEdit" runat="server" Text="Edit User" OnClick="btnEdit_Click"
                        CssClass="submit-btn" /><br />
                    <asp:Label ID="lblLoginText" runat="server" Text=""></asp:Label>
                </div>
                <!-- Validation -->
                <asp:Panel ID="pnlValidation" Visible="true" runat="server">
                    <center>
                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtUsername" ID="valUsername"
                            runat="server" ErrorMessage="Username is required<br>"></asp:RequiredFieldValidator>
                        <asp:CompareValidator Display="Dynamic" ControlToValidate="txtPassword" ID="valPasswordsEqual"
                            ControlToCompare="txtConfirmPassword" runat="server" ErrorMessage="The passwords do not match<br>"></asp:CompareValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtFirstName" ID="valFirstName"
                            runat="server" ErrorMessage="First Name is required<br>"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtLastName" ID="valLastName"
                            runat="server" ErrorMessage="Last Name is required<br>"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtEmail" ID="valEmail"
                            runat="server" ErrorMessage="Email Address is required<br>"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust1"
                            ID="valMandCust1" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust2"
                            ID="valMandCust2" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust3"
                            ID="valMandCust3" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust4"
                            ID="valMandCust4" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust5"
                            ID="valMandCust5" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                    </center>
                </asp:Panel>
                <!-- Validation -->
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
