<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UserListOne.ascx.vb" Inherits="UserListOne" %>

<div class="userChooserBox">
    <asp:Label ID="lblChosenUser" runat="server" Text=""></asp:Label>
    <div style="position:absolute; top:1px; right:1px;"><asp:ImageButton ID="btnShowHideUserChooser" runat="server" CausesValidation="false" ImageUrl="~/images/userlist_down.png" OnClick="btnShowHideUserChooser_Click" /></div>
</div>
<asp:Panel ID="pnlUserChooser" runat="server" CssClass="userChooserList" Visible="false">
    <asp:RadioButtonList ID="rblUsers" runat="server" OnSelectedIndexChanged="rblUsers_SelectedIndexChanged" AutoPostBack="true"></asp:RadioButtonList>
</asp:Panel>
