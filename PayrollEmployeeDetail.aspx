<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PayrollEmployeeDetail.aspx.vb" Inherits="PayrollEmployeeDetail" title="TimeClockWizard - Employee Detail Payroll" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <span class="title doNotPrint">
        <table width="100%" cellpadding="0px" cellspacing="0px">
            <tr>
                <td align="left">
                    <asp:HyperLink ID="lnkBack" ToolTip="header=[Back] body=[]" runat="server"><img src="images/back.png" border="0" style="vertical-align:middle;" /></asp:HyperLink>&nbsp;&nbsp;Payroll Detail for <asp:Literal ID="litPayrollTitleDates" runat="server"></asp:Literal>
                </td>
                <td align="center">
                    <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/images/print-btn.png" OnClientClick="window.print()" />
                </td>
            </tr>
        </table>
    </span>
    <div style="float: left; margin-left: 25px;">
    <asp:Repeater ID="rptUsers" runat="server">
        <ItemTemplate>
            <asp:Panel ID="pnlPageBreak" runat="server" CssClass="pageBreak"></asp:Panel>
            <asp:Panel ID="pnlUserInfo" runat="server">
                <div class="tableTitle" style="position:relative; top:21px;"><strong><asp:Label ID="lblUserName" runat="server" Text="Name"></asp:Label></strong></div>
            </asp:Panel>
            <div class="schClear"></div>
            <br />
            <asp:Repeater ID="rptEmployeeDetail" runat="server" OnItemDataBound="rptEmployeeDetail_ItemDataBound">
                <HeaderTemplate>
                    <table width="100%">
                        <tr>
                            <td width="33%">
                                &nbsp;
                            </td>
                            <td width="34%" align="center">
                                <span class="tableTitle printOnly">
                                    Payroll for <asp:Literal ID="litPayrollDates" runat="server"></asp:Literal>
                                </span>
                            </td>
                            <td width="33%" align="right">
                                <span class="tableTitle printOnly">
                                    <asp:Literal ID="litDate" runat="server"></asp:Literal>
                                </span>
                            </td>
                        </tr>
                    </table>
                    <ul style="border-left: 1px solid #c6ced0; width: 200px;" class="dates">
										<li style="text-align: center;" class="column-head column-title">Date</li>
            </ul>
                    <ul style="width: 200px;" class="dates">
										<li style="text-align: center;" class="column-head column-title">Start Time</li>
            </ul>
                    <ul style="width: 200px;" class="dates">
										<li style="text-align: center;" class="column-head column-title">End Time</li>
            </ul>
                    <ul style="width: 200px;" class="dates">
										<li style="text-align: center;" class="column-head column-title">Type</li>
            </ul>
                    <ul style="border-right: 1px solid #c6ced0; width: 200px;" class="total-hours">
										<li style="text-align: center;" class="column-head column-title">Total</li>
            </ul>
                           
                </HeaderTemplate>
                <ItemTemplate>
                    <ul style="width: 200px; font-size: 13px;" class="dates">
                <li style="text-align: center;">
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </li>
            </ul>
                     <ul style="width: 200px; font-size: 13px;" class="dates">
                <li style="text-align: center;">
                    <asp:Label ID="lblStartTime" runat="server"></asp:Label>
                </li>
            </ul>
                    <ul style="width: 200px; font-size: 13px;" class="dates">
                <li style="text-align: center;">
                    <asp:Label ID="lblEndTime" runat="server"></asp:Label>
                </li>
            </ul>
                    <ul style="width: 200px; font-size: 13px;" class="dates">
                <li style="text-align: center;">
                    <asp:Label ID="lblType" runat="server"></asp:Label>
                </li>
            </ul>
                    <ul style="width: 200px; font-size: 13px;" class="total-hours">
                <li style="text-align: center;">
                    <asp:Label ID="lblTotal" runat="server" ></asp:Label>
                </li>
            </ul>
  
                </ItemTemplate>

                <FooterTemplate>
                    <ul style="width: 200px; float: left; font-size: 13px;">
                <li style="text-align: center;">&nbsp;</li>
                    </ul>
                    <ul style="width: 200px; float: left; font-size: 13px;">
                <li style="text-align: center;">&nbsp;</li>
                    </ul>
                    <ul style="width: 200px; float: left; font-size: 13px;">
                <li style="text-align: center;">&nbsp;</li>
                    </ul>
                    <ul style="width: 200px; float: left; font-size: 13px;">
                <li style="text-align: center;">&nbsp;</li>
                    </ul>
                    <ul style="width: 200px; float: left; font-size: 13px;">
                <li style="text-align: center;">
                     <strong>Total: </strong><asp:Label ID="lblTotalHours" runat="server" Text="Label"></asp:Label>
                </li>
            </ul>    
                </FooterTemplate>
            </asp:Repeater>
        </ItemTemplate>
        <SeparatorTemplate>
            <span class="doNotPrint"><br /></span>
        </SeparatorTemplate>
    </asp:Repeater>
        </div>
</asp:Content>

