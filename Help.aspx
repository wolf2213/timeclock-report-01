﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Help.aspx.vb" Inherits="Help" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div class="page">
        
        <div class="userManual" style="width:1000px;">
          

      
      <div style="padding-left:30px;padding-top:10px;font-family:Arial;font-size:16px;color:#1f497d;font-weight:bold;">TimeClockWizard User Manual</div>
            <br />
      <div style="padding-left:50px;">
       <ol>
          <li class="heading_bold" >1. <a href="#C1" class="heading_bold_link">Introduction</a></li>
        <!--  <li class="heading_bold" >2. <a href="#C2" class="heading_bold_link">Terms and Conditions</a></li> -->
          <li class="heading_bold" >2. <a href="#C3" class="heading_bold_link">Employee’s User Guide</a></li>
</ol>
          </div>
<br />
            <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">
      1. <a name="C1" style="text-decoration:none;">Introduction:</a>  <br /><br />
                </div>
            <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#000000;">
       <p> TimeClockWizard is a Time & Attendance Program that helps you to take the pain out of creating payroll and managing the tardiness, absenteeism and controlling the payroll costs. The learning curve for TimeClockWizard is very short.</p>
            </div>    
                
                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
<!--
    <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">2. <a name="C2" style="text-decoration:none;">Terms and conditions:</a><br /><br /></div>
            <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#000000;">
   <p>To know the terms and conditions:</p>
                </div>
                <br />
                <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;">
<ol >
          <li >1. Click on “Start your free trial” link on the top menu bar in the TimeClockWizard page or“Signup Now” button in the TimeClockWizard page. </li>
     
    <li >2. Click on Terms of service in the Start your free 14 day trail now box on the right hand side of the start free trail page to read the terms of service to know about the terms and conditions.  </li>
</ol>
                    </div>
       
       
       <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>

    -->
               
           <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">2. <a name="C3" style="text-decoration:none;">Employees User Guide:</a> <br /><br /></div>
          <div style="padding-left:50px;">
           
         <ol>
               
               <li class="heading_bold_small">1. <a href="#C19" class="heading_bold_link">Log in</a></li>

             </ol>
</div>
            
               <div style="padding-left:75px;">
           
                   <ul>
           <li class="heading_bold_small">a. <a href="#C20" class="heading_bold_link">My Dashboard</a></li>
               <li class="heading_bold_small">b. <a href="#C21" class="heading_bold_link">Time Sheets</a></li>
           
          <li class="heading_bold_small">c. <a href="#C22" class="heading_bold_link">Timesheet Requests</a></li>
           
           <li class="heading_bold_small">d. <a href="#C23" class="heading_bold_link">Who’s In</a></li>
           
           <li class="heading_bold_small">e. <a href="#C24" class="heading_bold_link"> Mileage</a></li>

                       </ul>
                   </div>

            <div style="padding-left:50px;">
                <ol>
           
          <li class="heading_bold_small">2. <a href="#C25" class="heading_bold_link">Schedule</a></li>

                    </ol>

               </div>

            <div style="padding-left:75px;">

                <ul>
           
           <li class="heading_bold_small">a. <a href="#C26" class="heading_bold_link">View Schedule</a></li>
           
          <li class="heading_bold_small">b.<a href="#C27" class="heading_bold_link"> Request Absence</a></li>

                    </ul>

                </div>

            <div style="padding-left:50px;">

                <ol>
           
           <li class="heading_bold_small">3. <a href="#C28" class="heading_bold_link">Payroll</a></li>

                    </ol>

                </div>

            <div style="padding-left:50px;">

                <ol>
           
           <li class="heading_bold_small">4. <a href="#C29" class="heading_bold_link">Users</a></li>

                    </ol>

                </div>

               <div style="padding-left:75px;">   
                   
                   <ul>   
           
          <li class="heading_bold_small">a. <a href="#C30" class="heading_bold_link">Edit My Info</a></li>
           
         <li class="heading_bold_small">b. <a href="#C31" class="heading_bold_link">Change My Password</a></li>  
                       
                       </ul>                 

                   </div>
           

           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">1. <a name="C19" style="text-decoration:none;">Log In</a> <br /><br /></div>          

          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">Log in to TimeClockWizard:<br /><br /></div>

             <div style="padding-left:75px;">
           
           <ol><li>1. Open any internet browser and enter the custom Login page URL (TimeClockWizard program will send you this url information in the email when you register for a free trail).</li>
           
           <li>2. Enter the username and password and click on “log in” button to log in to your account.</li></ol>
                 
            </div>     
                 
                 <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">To Clockin/Clock Out:<br /><br />           </div>

            <div style="padding-left:75px;">
         <ol><li>1. Open the TimeClockWizard login page.</li>
           
           <li>2. To Clockin: Enter username, password and click on “log In” button to login Or Enter username, password and click on “log in” button to login.  Click on the “Clock in” button on top left hand side of this page.</li>
           
          <li>3. To Clock Out: Enter username, password and click on “Clock Out” button to Clock out or Enter username, password and click on “log in” page to login and then Click on the “Clock Out”  button on top left hand side of this page.</li>

         </ol>
             </div>   
                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">Start Break/Stop Break:           <br /><br />           </div>
            <div style="padding-left:50px;">
          <p> *Users can only use this option after they Clock in.</p><br />
                </div>
           
            <div style="padding-left:75px;">
           <ol><li>1. Open the TimeClockWizard login page.</li>
           
           <li>2. Enter username, password and click on “login” button to login</li>
           
         <li>3. Click on “Start Break” button to start break.</li>
           
           <li>4. Click on “End Break” button to end break.</li></ol>
               </div> 
                
                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">a. <a name="C20" style="text-decoration:none;">My Dashboard</a>    <br /><br />           </div>
            <div style="padding-left:50px;">
          <p> Displays Current Clockin/out status, My alerts and My schedule for the current week.</p>
                </div>
            <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">b. <a name="C21" style="text-decoration:none;">TimeSheets:</a>  <br /><br />           </div>

            <div style="padding-left:50px;">
          <p> Sometimes users forgot to Clock in or Clock out. Then they can time sheets to request their manager to adjust the Clockin or Clockout hours. Mangers can use this feature to check/approve/deny user’s requests.</p><br />
          <p>To send a request to edit Clockin/Clock Out times to the manager using Timesheets:           </p>

                </div>

            <div style="padding-left:75px;">

          <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Timesheets” tab.</li>
           
          <li>3. Go to “Manually add a Record” box which on the top left hand side of the page.</li>
           
         <li>4. Select Clocked in date, Clocked in time, Clocked Out Date, Clocked Out time, enter a brief reason and click on “Request Record” Button.</li></ol>
               </div> 
                
                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">c. <a name="C22" style="text-decoration:none;">TimeSheet Request:</a>    <br /><br />           </div>
            <div style="padding-left:50px;">
           <p>To View/Delete Timesheet Requests:</p>

                </div>

            <div style="padding-left:75px;">
           
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Timesheet Request” tab.</li>
           
           <li>3. To delete Timesheet Request click on red “X” mark at the end of the record.</li></ol>
                
                </div>
                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">d. <a name="C23" style="text-decoration:none;">Who's In</a> <br /><br />           </div>
            <div style="padding-left:50px;">
           <p>Displays the list of Clocked in users.</p>
                </div>

                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">e. <a name="C24" style="text-decoration:none;">Mileage</a>  <br /><br />           </div>

            <div style="padding-left:50px;">
          <p> To enter/check Mileage:</p>

                </div>

            <div style="padding-left:75px;">
           
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Mileage” tab.</li>
           
          <li>3. Select the date and enter total number of miles for that day.</li>
           
          <li>4. Select a date from the Display mileage box and it will display the total miles for that week.</li></ol>
                
             </div>
               
                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
         <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">To Delete Mileage Request: <br /><br />           </div>

            <div style="padding-left:75px;">
           
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Mileage” tab.</li>
           
         <li>3. Select the date from the Display mileage box</li>
           
           <li>4. Select a date and click on “X” mark at the end of the date record.</li></ol>
               </div> 
                
                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">2. <a name="C25" style="text-decoration:none;">Schedule</a>     <br /><br /></div> 

          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;"><a name="C26" style="text-decoration:none;">To View Schedule:</a><br /><br /></div>

             <div style="padding-left:75px;">
           
           <ol><li>1. Log in to TimeClockWizard program and It will displays the schedule on the left hand side of the “My Dashboard” (Home Page) page.</li>
           
           <li>2. Click on “Schedule” tab.</li></ol>
                 </div>
                 
                 <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;"><a name="C27" style="text-decoration:none;">To Request Absence:</a>  <br /><br /></div>

            <div style="padding-left:75px;">

           <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Schedule” tab.</li>
           
          <li>3. Click on “Request Absence” tab.</li>
           
          <li>4. Select the absence type from the request absence box on the top left hand side.</li>
           
           <li>5. Select the start date, end date and enter a note.</li>
           
        <li>6. Click on “Request Absence” button to send this request to the manager.</li></ol>
                
                </div>

                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">3. <a name="C28" style="text-decoration:none;">Payroll</a>    <br /><br /></div> 
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;"><a name="C29" style="text-decoration:none;">To View Payroll:</a>
               </div>
           
             <div style="padding-left:75px;">
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Payroll” tab on the top</li></ol>
                 
                 </div>

                 <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">4. <a name="C29" style="text-decoration:none;">Users</a>  <br /><br /></div> 
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;"><a name="C30" style="text-decoration:none;">To Edit My Info:</a><br /><br /></div>

            <div style="padding-left:75px;">
           
          <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Users” tab on the top.</li>
           
          <li>3. Edit Mandatory Information or Optional Information.</li>
           
           <li>4. Click on “Edit User” Button to apply the changes.</li></ol>
                </div>
                
                <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;"><a name="C31" style="text-decoration:none;">To Change My Password:</a> <br /><br />  </div>

            <div style="padding-left:75px;">

           <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Users” tab on the top</li>
           
           <li>3. Click on “Change My Password” tab.</li>
           
          <li>4. Enter current password, new password and confirm password.</li>
           
          <li>5. Click on “Change Password” button.</li></ol>  
                
                </div>

                 <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>       
         
      </div>
    </div>
 


</asp:Content>

