
Partial Class News
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabNews As Panel = Master.FindControl("pnlTabNews")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2News As Panel = Master.FindControl("pnlTab2News")

        pnlMasterTabNews.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.News
        pnlMasterTab2News.CssClass = "tab2 tab2On"
    End Sub
End Class
