<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Terms.aspx.vb" Inherits="Terms" title="TimeClockWizard - Terms of Service" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div style="margin-left: 25px;">
    <p style="margin: 0in 0in 7.5pt; line-height: 21.6pt">
        <span style="font-size: 13.5pt;">
            Terms of Service</span></p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt; line-height: 21.6pt;">
            By using timeclockwizard.com or any of its sub-domain websites ("Service"), you are agreeing
            to be bound by the following terms and conditions ("Terms of Service").</p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt; line-height: 21.6pt;">
            Time Clock Wizard reserves the right to update and change the Terms of Service
            from time to time without notice. Any new features that augment or enhance the current
            Service, including the release of new tools and resources, shall be subject to the
            Terms of Service. Continued use of the Service after any such changes shall constitute
            your consent to such changes. You can review the most current version of the Terms
            of Service at any time at: http://www.timeclockwizard.com/ClockitinApp/Terms.aspx</p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt; line-height: 21.6pt;">
            Violation of any of the terms below will result in the termination of your Account.
            While Time Clock Wizard prohibits such conduct and Content on the Service,
            you understand and agree that Time Clock Wizard cannot be responsible for the
            Content posted on the Service and you nonetheless may be exposed to such materials.
            You agree to use the Service at your own risk.</p>
    <p style="margin: 0in 0in 0pt; line-height: 21.6pt"></p>
    <div style="border-right: medium none; padding-right: 0in; border-top: medium none;
        padding-left: 0in; padding-bottom: 0in; border-left: medium none;
        padding-top: 0in; border-bottom: #cccccc 1pt dotted;">
        <p style="border-right: medium none; padding-right: 0in; border-top: medium none;
            padding-left: 0in; padding-bottom: 0in; margin: 0in 0in 11.25pt;
            border-left: medium none; line-height: 16.8pt; padding-top: 0in; border-bottom: medium none;">
            <b><span style="font-size: 11.5pt; color: #555555;">Account Terms</span></b></p>
    </div>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        1.&nbsp;&nbsp;
        You must be 13 years or older to use this Service.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        2.&nbsp;&nbsp;
        You must be a human. Accounts registered by �bots� or other automated methods are
        not permitted.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        3.&nbsp;&nbsp;
        You must provide your legal full name, a valid email address, and any other information
        requested in order to complete the signup process.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        4.&nbsp;&nbsp;
        Your login may only be used by one person � a single login shared by multiple people
        is not permitted. You may create separate logins for as many people as you'd like.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        5.&nbsp;&nbsp;
        You are responsible for maintaining the security of your account and password. Time
        Clock America LLC cannot and will not be liable for any loss or damage from your
        failure to comply with this security obligation.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        6.&nbsp;&nbsp;
        You are responsible for all Content posted and activity that occurs under your account
        (even when Content is posted by others who have accounts under your account).
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        7.&nbsp;&nbsp;
        You may not use the Service for any illegal or unauthorized purpose. You must not,
        in the use of the Service, violate any laws in your jurisdiction (including but
        not limited to copyright laws).
    </p>
        <!--
    <div style="border-right: medium none; padding-right: 0in; border-top: medium none;
        padding-left: 0in; padding-bottom: 0in; border-left: medium none;
        padding-top: 0in; border-bottom: #cccccc 1pt dotted;">
        <p style="border-right: medium none; padding-right: 0in; border-top: medium none;
            padding-left: 0in; padding-bottom: 0in; margin: 0in 0in 11.25pt;
            border-left: medium none; line-height: 16.8pt; padding-top: 0in; border-bottom: medium none;">
            <b><span style="font-size: 11.5pt; color: #555555;">
            Payment, Refunds, Upgrading and Downgrading Terms</span></b></p>
    </div>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        1.&nbsp;&nbsp;
        A valid credit card is required to activate your account. Trial accounts are not
        required to provide a credit card number.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        2.&nbsp;&nbsp;
        It is your responsibility to keep your credit card information up to date. If we are unable
        to bill the credit card on file, you will be provided a seven day grace period in which you
        will be able to update your information. If you fail to do so, your account will be terminated.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        3.&nbsp;&nbsp;
        The Service is billed in advance on a monthly basis and is non-refundable. There
        will be no refunds or credits for partial months of service, upgrade/downgrade refunds,
        or refunds for months unused with an open account. In order to treat everyone equally,
        no exceptions will be made.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        4.&nbsp;&nbsp;
        All fees are exclusive of all taxes, levies, or duties imposed by taxing authorities,
        and you shall be responsible for payment of all such taxes, levies, or duties, excluding
        only United States (federal or state) taxes.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in;">
        5.&nbsp;&nbsp;
        For any upgrade or downgrade in services, your credit card that you provided will
        automatically be charged the new rate on your next billing cycle.<br />
    </p>

            -->
    <div style="border-right: medium none; padding-right: 0in; border-top: medium none;
        padding-left: 0in; padding-bottom: 0in; border-left: medium none;
        padding-top: 0in; border-bottom: #cccccc 1pt dotted;">
        <p style="border-right: medium none; padding-right: 0in; border-top: medium none;
            padding-left: 0in; padding-bottom: 0in; margin: 0in 0in 11.25pt;
            border-left: medium none; line-height: 16.8pt; padding-top: 0in; border-bottom: medium none;">
            <b><span style="font-size: 11.5pt; color: #555555;">
            Cancellation and Termination</span></b></p>
    </div>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        1.&nbsp;&nbsp;
        You are solely responsible for properly canceling your account. An email or phone
        request to cancel your account is not considered cancellation.&nbsp; All cancellation
        requests must be submitted through the form on the "My Subscription" tab.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        2.&nbsp;&nbsp;
        All of your Content may be deleted from the Service upon cancellation. This information
        can not be recovered once your account is cancelled.
    </p>
        <!--
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        3.&nbsp;&nbsp;
        If you cancel the Service before the end of your current paid up month, your cancellation
        will take effect immediately and you will not be charged again.
    </p>
        -->
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        3.&nbsp;&nbsp;
        Time Clock Wizard, in its sole discretion, has the right to suspend or terminate
        your account and refuse any and all current or future use of the Service, or any
        other Time Clock Wizard service, for any reason at any time. Such termination
        of the Service will result in the deactivation or deletion of your Account or your
        access to your Account, and the forfeiture and relinquishment of all Content in
        your Account. Time Clock Wizard reserves the right to refuse service to anyone
        for any reason at any time.
    </p>

        <!--
    <div style="border-right: medium none; padding-right: 0in; border-top: medium none;
        padding-left: 0in; padding-bottom: 0in; border-left: medium none;
        padding-top: 0in; border-bottom: #cccccc 1pt dotted;">
        <p style="border-right: medium none; padding-right: 0in; border-top: medium none;
            padding-left: 0in; padding-bottom: 0in; margin: 0in 0in 11.25pt;
            border-left: medium none; line-height: 16.8pt; padding-top: 0in; border-bottom: medium none;">
            <b><span style="font-size: 11.5pt; color: #555555;">
            Modifications to the Service and Prices</span></b></p>
    </div>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        1.&nbsp;&nbsp;
        Time Clock Wizard reserves the right at any time and from time to time to modify
        or discontinue, temporarily or permanently, the Service (or any part thereof) with
        or without notice.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        2.&nbsp;&nbsp;
        Prices of all Services, including but not limited to monthly subscription plan fees
        to the Service, are subject to change upon 30 days notice from us. Such notice may
        be provided at any time by posting the changes to the Time Clock Wizard Site
        (<a href="http://www.timeclockwizard.com"><span style="color: #787777; text-decoration: none;">www.timeclockwizard.com</span></a>)
        or the Service itself.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        3.&nbsp;&nbsp;
        Time Clock Wizard shall not be liable to you or to any third party for any
        modification, price change, suspension or discontinuance of the Service.
    </p>

            -->
    <div style="border-right: medium none; padding-right: 0in; border-top: medium none;
        padding-left: 0in; padding-bottom: 0in; border-left: medium none;
        padding-top: 0in; border-bottom: #cccccc 1pt dotted;">
        <p style="border-right: medium none; padding-right: 0in; border-top: medium none;
            padding-left: 0in; padding-bottom: 0in; margin: 0in 0in 11.25pt;
            border-left: medium none; line-height: 16.8pt; padding-top: 0in; border-bottom: medium none;">
            <b><span style="font-size: 11.5pt; color: #555555;">
            Copyright and Content Ownership</span></b></p>
    </div>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        1.&nbsp;&nbsp;
        We claim no intellectual property rights over the material you provide to the Service.
        Your profile and materials uploaded remain yours.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in;">
        2.&nbsp;&nbsp;
        Time Clock Wizard does not pre-screen Content, but Time Clock Wizard and
        its designee have the right (but not the obligation) in their sole discretion to
        refuse or remove any Content that is available via the Service.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in;">
        3.&nbsp;&nbsp;
        The look and feel of the Service is copyright �2008 Time Clock Wizard. All rights
        reserved. You may not duplicate, copy, or reuse any portion of the HTML/CSS or visual
        design elements without express written permission from Time Clock Wizard.
    </p>
    <div style="border-right: medium none; padding-right: 0in; border-top: medium none;
        padding-left: 0in; padding-bottom: 0in; border-left: medium none;
        padding-top: 0in; border-bottom: #cccccc 1pt dotted;">
        <p style="border-right: medium none; padding-right: 0in; border-top: medium none;
            padding-left: 0in; padding-bottom: 0in; margin: 0in 0in 11.25pt;
            border-left: medium none; line-height: 16.8pt; padding-top: 0in; border-bottom: medium none;">
            <b><span style="font-size: 11.5pt; color: #555555;">
            General Conditions</span></b></p>
    </div>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        1.&nbsp;&nbsp;
        Your use of the Service is at your sole risk. The service is provided on an �as
        is� and �as available� basis.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        2.&nbsp;&nbsp;
        Technical support is only provided with no explicit or implicit guarantees. All quoted response times are estimates
        only.  Technical support requests may or may not be read/listened to by our staff and those requests that are acknowledged may not be acted upon.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        3.&nbsp;&nbsp;
        E-mail is the preferred method of support. Any account that abuses the telephone
        support option, in our sole discretion, may be terminated without notice.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        4.&nbsp;&nbsp;
        You understand that Time Clock Wizard maintains the hardware and software required
        to run the timeclockwizard.com service in order to provide maximum uptime. Scheduled and unscheduled
        downtime may occur, and Time Clock Wizard will attempt to notify you in such cases if possible.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        5.&nbsp;&nbsp;
        You must not modify, adapt or hack the Service or modify another website so as to
        falsely imply that it is associated with the Service, Time Clock Wizard, or
        any other Time Clock Wizard service.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        6.&nbsp;&nbsp;
        You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion
        of the Service, use of the Service, or access to the Service without the express
        written permission by Time Clock Wizard.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        7.&nbsp;&nbsp;
        We may, but have no obligation to, remove Content and Accounts containing Content
        that we determine in our sole discretion are unlawful, offensive, threatening, libelous,
        defamatory, pornographic, obscene or otherwise objectionable or violates any party�s
        intellectual property or these Terms of Service.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        8.&nbsp;&nbsp;
        Verbal, physical, written or other abuse (including threats of abuse or retribution)
        of any Time Clock Wizard customer, employee, member, or officer will result
        in immediate account termination.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        9.&nbsp;&nbsp;
        You must not upload, post, host, or transmit unsolicited email, SMSs, or �spam�
        messages.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        10.&nbsp;
        You must not transmit any worms or viruses or any code of a destructive nature.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        11.&nbsp;
        Time Clock Wizard does not warrant that (i) the service will meet your specific
        requirements, (ii) the service will be uninterrupted, timely, secure, or error-free,
        (iii) the results that may be obtained from the use of the service will be accurate
        or reliable, (iv) the quality of any products, services, information, or other material
        purchased or obtained by you through the service will meet your expectations, and
        (v) any errors in the Service will be corrected.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        11.&nbsp;
        You expressly understand and agree that Time Clock Wizard shall not be liable
        for any direct, indirect, incidental, special, consequential or exemplary damages,
        including but not limited to, damages for loss of profits, goodwill, use, data or
        other intangible losses (even if Time Clock Wizard has been advised of the
        possibility of such damages), resulting from: (i) the use or the inability to use
        the service; (ii) the cost of procurement of substitute goods and services resulting
        from any goods, data, information or services purchased or obtained or messages
        received or transactions entered into through or from the service; (iii) unauthorized
        access to or alteration of your transmissions or data; (iv) statements or conduct
        of any third party on the service; (v) or any other matter relating to the service.
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        12.&nbsp;
        The failure of Time Clock Wizard to exercise or enforce any right or provision
        of the Terms of Service shall not constitute a waiver of such right or provision.
        The Terms of Service constitutes the entire agreement between you and Time Clock
        America LLC and govern your use of the Service, superseding any prior agreements
        between you and Time Clock Wizard (including, but not limited to, any prior
        versions of the Terms of Service).
    </p>
    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        13.&nbsp;
        Time Clock Wizard reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Service (or any part thereof) with or without notice.
    </p>

                    <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        14.&nbsp;
  Time Clock Wizard shall not be liable to you or to any third party for any modification, changes, suspension or discontinuance of the Service.      
    </p> 

            <p style="font-size: 9pt; margin: 0in 0in 10pt 0.5in; text-indent: -0.25in; line-height: 21.6pt;">
        15.&nbsp;
        Questions about the Terms of Service should be sent to support@timeclockwizard.com.
    </p>
        </div>
</asp:Content>

