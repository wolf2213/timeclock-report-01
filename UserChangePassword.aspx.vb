
Partial Class UserChangePassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabUsers As Panel = Master.FindControl("pnlTabUsers")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2ChangePassword As Panel = Master.FindControl("pnlTab2ChangePassword")

        pnlMasterTabUsers.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users
        pnlMasterTab2ChangePassword.CssClass = "tab2 tab2On"
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Trace.Write("UserChangePassword.aspx", "Begin change password sub")
        Dim boolUpdate As Boolean = True

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Password FROM Users WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objDR = objCommand.ExecuteReader()

        objDR.Read()

        If Not Crypto.VerifyHash(txtCurrent.Text, objDR.Item("Password")) Then
            boolUpdate = False
            Trace.Write("UserChangePassword.aspx", "INCORRECT current password entered")
            lblMessage.Visible = True
            lblMessage.ForeColor = System.Drawing.Color.Red
            lblMessage.Text = "The value in the 'Current Password' field did not match your current password."
        Else
            Trace.Write("UserChangePassword.aspx", "CORRECT current password entered")
            lblMessage.Visible = True
            lblMessage.ForeColor = System.Drawing.Color.Green
            lblMessage.Text = "Password Successfully Changed."
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If boolUpdate Then
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "UPDATE Users SET Password = @Password WHERE User_ID = @User_ID"
            objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@Password", Crypto.ComputeMD5Hash(txtPassword.Text))
            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
            Trace.Write("UserChangePassword.aspx", "Password Changed")
        End If

        Trace.Write("UserChangePassword.aspx", "End change password sub")
    End Sub
End Class