
Partial Class TimesheetRequests
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabHome As Panel = Master.FindControl("pnlTabHome")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2TimesheetRequests As Panel = Master.FindControl("pnlTab2TimesheetRequests")

        pnlMasterTabHome.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Timesheet
        pnlMasterTab2TimesheetRequests.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            If Request.QueryString("highlight") = "true" Then
                'get the ID of the row to highlight
            End If

            ShowRequests()
        End If
    End Sub

    Protected Sub ShowRequests()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection

        If Permissions.TimesheetsEdit(Session("User_ID")) Then
            If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
                objCommand.CommandText = "SELECT TimeRequests.*, Users.LastName + ', ' + Users.FirstName AS FullName FROM TimeRequests, Users WHERE TimeRequests.User_ID = Users.User_ID AND Users.Client_ID = @Client_ID AND TimeRequests.Approved = 0 AND Users.Active = 1 AND Users.ManagerUserId = @ManagerUserId ORDER BY FullName ASC"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
            Else
                objCommand.CommandText = "SELECT TimeRequests.*, Users.LastName + ', ' + Users.FirstName AS FullName FROM TimeRequests, Users WHERE TimeRequests.User_ID = Users.User_ID AND Users.Client_ID = @Client_ID AND TimeRequests.Approved = 0 AND Users.Active = 1 ORDER BY FullName ASC"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            End If
        Else
            objCommand.CommandText = "SELECT TimeRequests.*, Users.LastName + ', ' + Users.FirstName AS FullName FROM TimeRequests, Users WHERE TimeRequests.User_ID = Users.User_ID AND Users.User_ID = @User_ID AND TimeRequests.Client_ID = @Client_ID ORDER BY FullName ASC"
            objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        End If


        objDR = objCommand.ExecuteReader()

        rptTimesheetRequests.DataSource = objDR
        rptTimesheetRequests.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptTimesheetRequests_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Header Then
            Dim imgHelpDuration As Image = e.Item.FindControl("imgHelpDuration")
            Dim imgHelpEffect As Image = e.Item.FindControl("imgHelpEffect")
            imgHelpDuration.ToolTip = "cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Duration] body=[The new duration of the time record if the change is accepted.  A ? appears if the user is not clocked out.]"
            imgHelpEffect.ToolTip = "cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Effect] body=[The change in the time record's duration if the request is accepted.  A ? appears if the change in duration cannot be determined]"
        End If

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim litChangeType As Literal = e.Item.FindControl("litChangeType")
            Dim litUserID As Literal = e.Item.FindControl("litUserID")
            Dim lblName As Label = e.Item.FindControl("lblName")
            Dim imgType As Image = e.Item.FindControl("imgType")
            Dim imgTrType As Image = e.Item.FindControl("imgTrType")
            Dim lblDates As Label = e.Item.FindControl("lblDates")
            Dim lblReqDateTime As Label = e.Item.FindControl("lblReqDateTime")
            Dim lblDuration As Label = e.Item.FindControl("lblDuration")
            Dim lblEffect As Label = e.Item.FindControl("lblEffect")
            Dim lblNote As Label = e.Item.FindControl("lblNote")
            Dim imgStatus As Image = e.Item.FindControl("imgStatus")
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
            Dim btnApprove As ImageButton = e.Item.FindControl("btnApprove")
            Dim btnDeny As ImageButton = e.Item.FindControl("btnDeny")

            Trace.Write("TimeSheetRequests.aspx", "Request_ID = " & e.Item.DataItem("Request_ID"))
            Trace.Write("TimeSheetRequests.aspx", "User_ID = " & e.Item.DataItem("User_Id"))

            Dim intType As Integer = 1
            Dim dtmRecordStartTime As Date = "1/1/1900 12:00 AM"
            Dim dtmRecordEndTime As Date = "1/1/1900 12:00 AM"

            btnDelete.ToolTip = "offsetx=[-150] header=[Delete Request] body=[]"
            btnApprove.ToolTip = "offsetx=[-150] header=[Approve Request] body=[]"
            btnDeny.ToolTip = "offsetx=[-150] header=[Deny Request] body=[]"

            lblName.Text = e.Item.DataItem("FullName")
            litUserID.Text = e.Item.DataItem("User_ID")
            If e.Item.DataItem("DateRequested") Is DBNull.Value Then
                lblReqDateTime.Text = ""
            Else
                lblReqDateTime.Text = e.Item.DataItem("DateRequested")
            End If

            Dim strAlternateText As String = ""
            Dim strAltTextBody As String = ""

            If e.Item.DataItem("Time_ID") = 0 Then
                imgType.ImageUrl = "~/images/timesheet_manual.png"
                strAlternateText = "Add"
                litChangeType.Text = 1
            Else
                imgType.ImageUrl = "~/images/timesheet_edited.png"
                strAlternateText = "Edit"
                litChangeType.Text = 2

                Dim objDR As System.Data.SqlClient.SqlDataReader
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT Type, StartTime, EndTime FROM TimeRecords WHERE Time_ID = @Time_ID"
                objCommand.Parameters.AddWithValue("@Time_ID", e.Item.DataItem("Time_ID"))
                objDR = objCommand.ExecuteReader()

                If objDR.Read Then
                    intType = objDR("Type")
                    dtmRecordStartTime = objDR("StartTime")

                    If Not objDR("EndTime") Is DBNull.Value Then
                        dtmRecordEndTime = objDR("EndTime")
                    End If
                End If

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing
                End If

            If intType = 2 Or intType = 3 Or e.Item.DataItem("AddType") = 2 Or e.Item.DataItem("AddType") = 3 Then
                imgTrType.ImageUrl = "~/images/timesheet_break.png"
                imgTrType.AlternateText = strAlternateText & " Break Time Record Request"
            Else
                imgTrType.ImageUrl = "~/images/timesheet_clockinout.png"
                imgTrType.AlternateText = strAlternateText & " Time Record Request"
            End If

                imgTrType.ToolTip = "header=[" & imgTrType.AlternateText & "] body=[" & strAltTextBody & "]"

                If litChangeType.Text = 2 Then
                    Dim strEndAltText As String = ""
                    Dim strTREndAltText As String = ""

                    If Not e.Item.DataItem("EndTime") Is DBNull.Value Then
                        strEndAltText = e.Item.DataItem("EndTime")
                    Else
                        strEndAltText = "?"
                    End If

                    If dtmRecordEndTime <> "1/1/1900 12:00 AM" Then
                        strTREndAltText = dtmRecordEndTime
                    Else
                        strTREndAltText = "?"
                    End If

                    strAltTextBody = "From:<br>" & dtmRecordStartTime & " - " & strTREndAltText & vbCrLf & "<br><br>To:<br>" & e.Item.DataItem("StartTime") & " - " & strEndAltText
                    imgTrType.ToolTip = "header=[" & imgTrType.AlternateText & "] body=[" & strAltTextBody & "]"
                    imgTrType.AlternateText = imgTrType.AlternateText & vbCrLf & strAltTextBody
                End If

                imgType.ToolTip = imgTrType.ToolTip
                imgType.AlternateText = imgTrType.AlternateText

                Dim strDateDisplay As String
                Dim dtStart As DateTime = e.Item.DataItem("StartTime")
                Dim intMinutesDifference As Int64 = 0
                Dim boolDifferenecNegative = False
                Dim boolNullError = False


                If Not e.Item.DataItem("EndTime") Is DBNull.Value Then
                    Dim dtEnd As DateTime = e.Item.DataItem("EndTime")

                    If dtStart.Date = dtEnd.Date Then
                        strDateDisplay = MonthName(dtStart.Month, True) & " " & dtStart.Day & ", " & Time.QuickStrip(dtStart) & " - " & Time.QuickStrip(dtEnd)
                    Else
                        strDateDisplay = MonthName(dtStart.Month, True) & " " & dtStart.Day & ", " & Time.QuickStrip(dtStart) & " - " & MonthName(dtEnd.Month, True) & " " & dtEnd.Day & ", " & Time.QuickStrip(dtEnd)
                    End If

                    lblDuration.Text = TimeRecords.FormatMinutes(TimeRecords.TimeDifference(e.Item.DataItem("StartTime").ToString, e.Item.DataItem("EndTime").ToString), Session("User_ID"), True)

                    If dtmRecordEndTime <> "1/1/1900 12:00 AM" Then
                        intMinutesDifference = DateDiff(DateInterval.Minute, e.Item.DataItem("StartTime"), dtmRecordStartTime) + DateDiff(DateInterval.Minute, dtmRecordEndTime, e.Item.DataItem("EndTime"))
                    Else
                        boolNullError = True
                    End If

                Else
                    strDateDisplay = MonthName(dtStart.Month, True) & " " & dtStart.Day & ", " & Time.QuickStrip(dtStart) & " - ?"
                    lblDuration.Text = "?"

                    intMinutesDifference = DateDiff(DateInterval.Minute, e.Item.DataItem("StartTime"), dtmRecordStartTime)
                End If

                If boolNullError Then
                    lblEffect.Text = "?"
                Else
                    If intMinutesDifference = 0 Then
                        lblEffect.Text = "No Change"
                    Else
                        If intMinutesDifference < 0 Then
                            boolDifferenecNegative = True
                        End If

                        intMinutesDifference = Math.Abs(intMinutesDifference)

                        If boolDifferenecNegative Then
                            lblEffect.ForeColor = Drawing.Color.Red
                            lblEffect.Text = "- "
                        Else
                            lblEffect.ForeColor = Drawing.Color.Green
                            lblEffect.Text = "+ "
                        End If

                        lblEffect.Text = lblEffect.Text & TimeRecords.FormatMinutes(intMinutesDifference, Session("User_ID"), False)
                    End If
                End If

                lblDates.Text = strDateDisplay

                lblNote.Text = e.Item.DataItem("Reason")

                If Not Permissions.TimesheetsEdit(Session("User_ID")) Then
                    imgStatus.Visible = True
                    Select Case e.Item.DataItem("Approved")
                        Case 0
                            'not checked
                            imgStatus.ImageUrl = "~/images/placeholder.png"
                        Case 1
                            'approved
                            imgStatus.ImageUrl = "~/images/check-green_small.png"
                            imgStatus.ToolTip = "offsetx=[-150] header=[Request Approved] body=[By " & UserInfo.NameFromID(e.Item.DataItem("ApprovedDeniedBy"), 1) & "]"
                            imgStatus.AlternateText = "Request approved by " & UserInfo.NameFromID(e.Item.DataItem("ApprovedDeniedBy"), 1)
                        Case 2
                            'deny
                            imgStatus.ImageUrl = "~/images/red-x_small.png"
                            imgStatus.ToolTip = "offsetx=[-150] header=[Request Denied] body=[By " & UserInfo.NameFromID(e.Item.DataItem("ApprovedDeniedBy"), 1) & "]"
                            imgStatus.AlternateText = "Request denied by " & UserInfo.NameFromID(e.Item.DataItem("ApprovedDeniedBy"), 1)
                    End Select
                    btnDelete.Visible = True
                    btnDelete.CommandArgument = e.Item.DataItem("Request_ID")

                    btnApprove.Visible = False
                    btnDeny.Visible = False
                Else
                    btnDelete.Visible = False
                    btnApprove.CommandArgument = e.Item.DataItem("Request_ID")
                    btnDeny.CommandArgument = e.Item.DataItem("Request_ID")
                End If
        End If

        If e.Item.ItemType = ListItemType.Header Then
            If Not Permissions.TimesheetsEdit(Session("User_ID")) Then
                Dim litStatus As Literal = e.Item.FindControl("litStatus")
                litStatus.Visible = True
                litStatus.Text = "Status"
            End If
        End If
    End Sub

    Protected Sub rptTimesheetRequests_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim strCommandName As String = e.CommandName
        Dim intRequestID As Int64 = e.CommandArgument
        Dim litUserID As Literal = e.Item.FindControl("litUserID")
        Dim intUserID As Int64 = litUserID.Text
        Dim intApprove As Integer = 0
        Dim boolUpdateRecord As Boolean = True

        Trace.Write("TimeSheetRequests.aspx", "Start Processing ID " & intRequestID)

        Select Case strCommandName
            Case "Delete"
                boolUpdateRecord = False
                Trace.Write("TimeSheetRequests.aspx", "Deleting Request ID: " & intRequestID)

                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection
                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "DELETE FROM TimeRequests WHERE Request_ID = @Request_ID"
                objCommand.Parameters.AddWithValue("@Request_ID", intRequestID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing
            Case "Deny"
                intApprove = 2

                lblMessage.ForeColor = Drawing.Color.Green
                lblMessage.Text = "The request has been denied.<br><br>"

            Case "Approve"
                intApprove = 1
                Dim litChangeType As Literal = e.Item.FindControl("litChangeType")
                'add or edit timerecord now

                If litChangeType.Text = 1 Then
                    'add the record
                    Dim dtNewStart As DateTime
                    Dim dtNewEnd As DateTime
                    Dim intAddType As Integer

                    Trace.Write("TimeSheetRequests.aspx", "Running SQL for Add")
                    Dim objCommandAdd As System.Data.SqlClient.SqlCommand
                    Dim objConnectionAdd As System.Data.SqlClient.SqlConnection
                    Dim objDR As System.Data.SqlClient.SqlDataReader

                    objConnectionAdd = New System.Data.SqlClient.SqlConnection

                    objConnectionAdd.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                    objConnectionAdd.Open()
                    objCommandAdd = New System.Data.SqlClient.SqlCommand()
                    objCommandAdd.Connection = objConnectionAdd
                    objCommandAdd.CommandText = "SELECT AddType, StartTime, EndTime FROM TimeRequests WHERE Request_ID = @Request_ID"
                    objCommandAdd.Parameters.AddWithValue("@Request_ID", intRequestID)
                    objDR = objCommandAdd.ExecuteReader()
                    objDR.Read()

                    intAddType = objDR("AddType")
                    dtNewStart = objDR("StartTime")
                    dtNewEnd = objDR("EndTime")

                    objDR = Nothing
                    objCommandAdd = Nothing
                    objConnectionAdd.Close()
                    objConnectionAdd = Nothing

                    objConnectionAdd = New System.Data.SqlClient.SqlConnection
                    objConnectionAdd.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                    objConnectionAdd.Open()
                    objCommandAdd = New System.Data.SqlClient.SqlCommand()
                    objCommandAdd.Connection = objConnectionAdd
                    objCommandAdd.CommandText = "INSERT INTO TimeRecords (User_ID, Client_ID, Type, StartTime, StartLocation_ID, StartLocation, EndTime, EndLocation_ID, EndLocation, VerifiedBy, ManualBy) VALUES (@User_ID, @Client_ID, @Type, @StartTime, @StartLocation_ID, @StartLocation, @EndTime, @EndLocation_ID, @EndLocation, @VerifiedBy, @ManualBy)"
                    objCommandAdd.Parameters.AddWithValue("@User_ID", intUserID)
                    objCommandAdd.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                    objCommandAdd.Parameters.AddWithValue("@Type", intAddType)
                    objCommandAdd.Parameters.AddWithValue("@StartTime", dtNewStart)
                    objCommandAdd.Parameters.AddWithValue("@StartLocation_ID", Locations.LocationID(Session("Client_ID"), Request.ServerVariables("REMOTE_ADDR")))
                    objCommandAdd.Parameters.AddWithValue("@StartLocation", Request.ServerVariables("REMOTE_ADDR"))
                    objCommandAdd.Parameters.AddWithValue("@EndTime", dtNewEnd)
                    objCommandAdd.Parameters.AddWithValue("@EndLocation_ID", Locations.LocationID(Session("Client_ID"), Request.ServerVariables("REMOTE_ADDR")))
                    objCommandAdd.Parameters.AddWithValue("@EndLocation", Request.ServerVariables("REMOTE_ADDR"))

                    If Permissions.TimesheetsValidate(Session("User_ID")) Then
                        objCommandAdd.Parameters.AddWithValue("@VerifiedBy", Session("User_ID"))
                    Else
                        objCommandAdd.Parameters.AddWithValue("@VerifiedBy", 0)
                    End If
                    objCommandAdd.Parameters.AddWithValue("@ManualBy", Session("User_ID"))

                    objCommandAdd.ExecuteNonQuery()

                    objCommandAdd = Nothing
                    objConnectionAdd.Close()
                    objConnectionAdd = Nothing

                Else
                    'edit the record
                    Trace.Write("TimeSheetRequests.aspx", "Running SQL for Edit")
                    Dim objCommandEdit As System.Data.SqlClient.SqlCommand
                    Dim objConnectionEdit As System.Data.SqlClient.SqlConnection
                    objConnectionEdit = New System.Data.SqlClient.SqlConnection
                    objConnectionEdit.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                    objConnectionEdit.Open()
                    objCommandEdit = New System.Data.SqlClient.SqlCommand()
                    objCommandEdit.Connection = objConnectionEdit
                    objCommandEdit.CommandText = "UPDATE TimeRecords SET TimeRecords.StartTime = TimeRequests.StartTime, TimeRecords.EndTime = TimeRequests.EndTime FROM TimeRequests WHERE TimeRequests.Time_ID = TimeRecords.Time_ID AND TimeRequests.Request_ID = @RequestID"
                    objCommandEdit.Parameters.AddWithValue("@RequestID", intRequestID)

                    objCommandEdit.ExecuteNonQuery()

                    objCommandEdit = Nothing
                    objConnectionEdit.Close()
                    objConnectionEdit = Nothing
                End If

                lblMessage.ForeColor = Drawing.Color.Green
                lblMessage.Text = "The request has been approved.<br><br>"
        End Select

        If boolUpdateRecord Then
            Trace.Write("TimeSheetRequests.aspx", "Setting Approve/Deny for Record")
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "UPDATE TimeRequests SET Approved = @Approve, ApprovedDeniedBy = @Manager_ID WHERE Request_ID = @Request_ID"
            objCommand.Parameters.AddWithValue("@Approve", intApprove)
            objCommand.Parameters.AddWithValue("@Request_ID", intRequestID)
            objCommand.Parameters.AddWithValue("@Manager_ID", Session("User_ID"))

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If

        'update the page
        ShowRequests()
    End Sub
End Class