<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SettingsSubscriptionDelinquent.aspx.vb" Inherits="SettingsSubscriptionDelinquent" title="TimeClockWizard - My Subscription" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script language="javascript" type="text/javascript">
        function disableUpdateButton() {
            document.getElementById('<%=btnUpdate.ClientID%>').disabled = true;
            document.getElementById('<%=btnUpdate.ClientID%>').value = 'Processing...';
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td colspan="2" style="padding-top:10px;">
                        <asp:Panel ID="Panel1" runat="server" CssClass="blueBox" style="width:526px;">
                            <span class="subtitle">Delinquent Total: <asp:Label ID="lblDelinquentTotal" runat="server" Text=""></asp:Label></span>
                            <br />
                            <asp:Label ID="lblDelinquentDays" runat="server" Text="Your account has been delinquent for 0 days" CssClass="subtitle"></asp:Label>
                            <asp:Label ID="lblDelinquentWarning" runat="server" Text="<br />After 7 days, your account will be disabled"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="pnlCreditCardInfo" runat="server">
                <table class="yellowBox" style="width:542px;" align="center">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblCCInfoPrompt" runat="server" Font-Bold="true" Text="Please enter your credit card information below"></asp:Label>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Name as it appears on card</td>
                        <td align="left"><asp:TextBox ID="txtCardName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right">Card Type</td>
                        <td align="left">
                        <asp:DropDownList ID="drpCardType" runat="server">
                                <asp:ListItem Value="1">Visa</asp:ListItem>
                                <asp:ListItem Value="2">MasterCard</asp:ListItem>
                                <asp:ListItem Value="3">American Express</asp:ListItem>
                                <asp:ListItem Value="4">Discover</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Card Number</td>
                        <td align="left"><asp:TextBox ID="txtCardNumber" runat="server" MaxLength="16"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right">Expiration Date</td>
                        <td align="left">
                            <asp:DropDownList ID="drpExpMonth" runat="server">
                                <asp:ListItem Value="01">01 - Jan</asp:ListItem>
                                <asp:ListItem Value="02">02 - Feb</asp:ListItem>
                                <asp:ListItem Value="03">03 - Mar</asp:ListItem>
                                <asp:ListItem Value="04">04 - Apr</asp:ListItem>
                                <asp:ListItem Value="05">05 - May</asp:ListItem>
                                <asp:ListItem Value="06">06 - Jun</asp:ListItem>
                                <asp:ListItem Value="07">07 - Jul</asp:ListItem>
                                <asp:ListItem Value="08">08 - Aug</asp:ListItem>
                                <asp:ListItem Value="09">09 - Sep</asp:ListItem>
                                <asp:ListItem Value="10">10 - Oct</asp:ListItem>
                                <asp:ListItem Value="11">11 - Nov</asp:ListItem>
                                <asp:ListItem Value="12">12 - Dec</asp:ListItem>
                            </asp:DropDownList>
                            /
                            <asp:DropDownList ID="drpExpYear" runat="server">
                                <asp:ListItem Value="08">2008</asp:ListItem>
                                <asp:ListItem Value="09">2009</asp:ListItem>
                                <asp:ListItem Value="10">2010</asp:ListItem>
                                <asp:ListItem Value="11">2011</asp:ListItem>
                                <asp:ListItem Value="12">2012</asp:ListItem>
                                <asp:ListItem Value="13">2013</asp:ListItem>
                                <asp:ListItem Value="14">2014</asp:ListItem>
                                <asp:ListItem Value="15">2015</asp:ListItem>
                                <asp:ListItem Value="16">2016</asp:ListItem>
                                <asp:ListItem Value="17">2017</asp:ListItem>
                                <asp:ListItem Value="18">2018</asp:ListItem>
                                <asp:ListItem Value="19">2019</asp:ListItem>
                                <asp:ListItem Value="20">2020</asp:ListItem>
                                <asp:ListItem Value="21">2021</asp:ListItem>
                                <asp:ListItem Value="22">2022</asp:ListItem>
                                <asp:ListItem Value="23">2023</asp:ListItem>
                                <asp:ListItem Value="24">2024</asp:ListItem>
                                <asp:ListItem Value="25">2025</asp:ListItem>
                                <asp:ListItem Value="26">2026</asp:ListItem>
                                <asp:ListItem Value="27">2027</asp:ListItem>
                                <asp:ListItem Value="28">2028</asp:ListItem>
                                <asp:ListItem Value="29">2029</asp:ListItem>
                                <asp:ListItem Value="30">2030</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Card Verification Code</td>
                        <td align="left"><asp:TextBox ID="txtCardVerify" runat="server" Width="50px" MaxLength="4"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <br />
                            <asp:Button ID="btnUpdate" runat="server" Text="Update Information" OnClientClick="disableUpdateButton()" UseSubmitBehavior="false" />
                            <br />
                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    Processing...
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlCreditCardApproved" runat="server" Visible="false">
                <table class="yellowBox" style="width:542px;" align="center">
                    <tr>
                        <td colspan="2" align="center">
                            <strong>Your credit card has been approved<asp:Label ID="lblActivated" runat="server" Text=", and your account is now active" Visible="false"></asp:Label>.</strong>
                            <br />
                            Your account is no longer delinquent.
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="text-align:center;">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Terms.aspx">Terms of Service</asp:HyperLink>
        <br /><br />
        If you have any questions, please contact us via phone at (877) 646-4446 or email at <a href="mailto:info@timeclockwizard.com">info@timeclockwizard.com</a>.
    </div>
</asp:Content>

