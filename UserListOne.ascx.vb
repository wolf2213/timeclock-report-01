
Partial Class UserListOne
    Inherits System.Web.UI.UserControl

    Public Event UserChanged(ByVal sender As Object, ByVal e As CommandEventArgs)
    Public intSelectedUser As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT User_ID, (LastName + ', ' + FirstName) As FullName FROM [Users] WHERE ([Client_ID] = @Client_ID AND Active = 1) ORDER BY LastName ASC"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

            objDR = objCommand.ExecuteReader()

            rblUsers.DataSource = objDR
            rblUsers.DataTextField = "FullName"
            rblUsers.DataValueField = "User_ID"
            rblUsers.DataBind()

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            lblChosenUser.Text = Session("LastName") & ", " & Session("FirstName")
            rblUsers.SelectedValue = Session("User_ID")
            Session("tempSelectedUser") = Session("User_ID")
        End If
    End Sub

    Protected Sub rblUsers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblChosenUser.Text = rblUsers.SelectedItem.Text
        Session("tempSelectedUser") = rblUsers.SelectedValue
        ShowHideUserChooser()

        Dim Args As New CommandEventArgs("rblUsers", rblUsers.Items)
        RaiseEvent UserChanged(Me, Args)
    End Sub

    Protected Sub btnShowHideUserChooser_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowHideUserChooser()
    End Sub

    Protected Sub ShowHideUserChooser()
        If pnlUserChooser.Visible Then
            pnlUserChooser.Visible = False
            btnShowHideUserChooser.ImageUrl = "~/images/userlist_down.png"
        Else
            pnlUserChooser.Visible = True
            btnShowHideUserChooser.ImageUrl = "~/images/userlist_up.png"
        End If
    End Sub

End Class