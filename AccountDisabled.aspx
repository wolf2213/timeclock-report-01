<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AccountDisabled.aspx.vb" Inherits="AccountDisabled" title="TimeClockWizard - Account Disabled" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="false">
    </asp:ScriptManager>
    <strong><span style="font-size: 14pt">Account Disabled<br />
    </span></strong>
    <p>
        Your TimeClockWizard account has been disabled. Please contact TimeClockWizard Support at
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="mailto:info@timeclockwizard.com">info@timeclockwizard.com</asp:HyperLink>
        in order to re-enable this account.</p>
</asp:Content>

