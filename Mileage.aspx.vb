
Partial Class Mileage
    Inherits System.Web.UI.Page

    Dim intTotalMiles As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabHome As Panel = Master.FindControl("pnlTabHome")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Mileage As Panel = Master.FindControl("pnlTab2Mileage")

        pnlMasterTabHome.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Timesheet
        pnlMasterTab2Mileage.CssClass = "tab2 tab2On"

        If Permissions.MileageDisplay(Session("User_ID")) Then
            '          culUserList.Visible = True
            lblUserFullName.Visible = False
        Else
            '           culUserList.Visible = False
            lblUserFullName.Visible = True
            lblUserFullName.Text = Session("FirstName") & " " & Session("LastName")
        End If

        If Not IsPostBack Then
            calMileage.SelectedDate = calMileage.TodaysDate
            lblDate.Text = calMileage.SelectedDate
            lblStartDate.Text = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, Clock.GetNow()), Year(Clock.GetNow()), 0)
            lblEndDate.Text = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, Clock.GetNow()), Year(Clock.GetNow()), 1)
            calMileageDisp.SelectedDates.SelectRange(lblStartDate.Text, lblEndDate.Text)

            GetUsers(0, Session("User_ID"))
        End If

        lblSuccess.Visible = False
    End Sub

    Protected Sub GetUsers(ByVal intMethod As Integer, ByVal strCriteria As String)
        Dim SQLQuery As String

        Me.ViewState.Item("intMethodHolder") = intMethod
        Me.ViewState.Item("strCriteriaHolder") = strCriteria

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection

        If intMethod = 0 Then
            'Select by user
            If strCriteria = "*" Then
                If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
                    SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE ([Client_ID] = @Client_ID) AND [Active] = 1 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) ORDER BY LastName ASC"
                    objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
                    objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
                Else
                    SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE ([Client_ID] = @Client_ID) AND [Active] = 1 ORDER BY LastName ASC"
                End If
            ElseIf strCriteria = "" Then
                SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE ([User_ID] = 0) ORDER BY LastName ASC"
            Else
                SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE ([User_ID] IN (" & strCriteria & ")) ORDER BY LastName ASC"
            End If
        Else
            'Select by office
            If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
                SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE [Office_ID] = @Office_ID AND [Active] = 1 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) ORDER BY LastName ASC"
                objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
                objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
            Else
                SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE [Office_ID] = @Office_ID AND [Active] = 1 ORDER BY LastName ASC"
            End If
        End If

            objCommand.CommandText = SQLQuery
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@Office_ID", strCriteria)

            objDR = objCommand.ExecuteReader()

            rptUsers.DataSource = objDR
            rptUsers.DataBind()

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
    End Sub

    Protected Sub rptUsers_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            intTotalMiles = 0

            Dim lblNoMileage As Label = e.Item.FindControl("lblNoMileage")
            Dim litNoMileageBRs As Literal = e.Item.FindControl("litNoMileageBRs")
            Dim rptMileage As Repeater = e.Item.FindControl("rptMileage")

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT * FROM [Mileage] WHERE ([User_ID] = @User_ID) AND ([DateTime] BETWEEN @StartDate AND @EndDate) ORDER BY DateTime DESC"
            objCommand.Parameters.AddWithValue("@User_ID", e.Item.DataItem("User_ID"))
            objCommand.Parameters.AddWithValue("@StartDate", lblStartDate.Text)
            objCommand.Parameters.AddWithValue("@EndDate", lblEndDate.Text)

            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                rptMileage.DataSource = objDR
                rptMileage.DataBind()
                lblNoMileage.Visible = False
                litNoMileageBRs.Visible = False
            Else
                lblNoMileage.Visible = True
                litNoMileageBRs.Visible = True
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
    End Sub

    Protected Sub rptMileage_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            intTotalMiles = intTotalMiles + e.Item.DataItem("Miles")

            Dim lblDate As Label = e.Item.FindControl("lblDate")
            Dim btnVerify As ImageButton = e.Item.FindControl("btnVerify")
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

            lblDate.Text = Time.StripTime(e.Item.DataItem("DateTime"))

            If e.Item.DataItem("VerifiedBy") <> 0 Then
                btnVerify.Visible = True
                btnVerify.ImageUrl = "~/images/timesheet_verified.png"
                btnVerify.AlternateText = "Verified By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("VerifiedBy"), 1)
                btnVerify.ToolTip = "header=[Verified] body=[By " & UserInfo.NameFromID(e.Item.DataItem("VerifiedBy"), 1) & "]"
                btnDelete.ToolTip = "offsetx=[-150] header=[Delete Mileage] body=[]"
            Else
                If Permissions.MileageValidate(Session("User_ID")) Then
                    btnVerify.Enabled = True
                    btnVerify.AlternateText = "Unverified" & vbCrLf & "Click to verify"
                    btnVerify.ToolTip = "header=[Unverified] body=[Click to Verify]"
                    btnVerify.CommandName = "Verify"
                    btnVerify.CommandArgument = e.Item.DataItem("Mileage_ID")
                Else
                    btnVerify.ToolTip = "header=[Unverified] body=[]"
                    btnDelete.ToolTip = "offsetx=[-150] header=[Delete Mileage Request] body=[]"
                End If
            End If

            If e.Item.DataItem("User_ID") <> Session("User_ID") Then
                If Permissions.MileageDelete(Session("User_ID")) Then
                    Trace.Write("Mileage", "Check 1")
                    btnDelete.Visible = True
                Else
                    Trace.Write("Mileage", "Check 2")
                    btnDelete.Visible = False
                End If
            ElseIf e.Item.DataItem("VerifiedBy") = 0 Then
                Trace.Write("Mileage", "Check 3")
                btnDelete.Visible = True
            Else
                Trace.Write("Mileage", "Check 4")
                If Permissions.MileageDelete(Session("User_ID")) Then
                    Trace.Write("Mileage", "Check 5")
                    btnDelete.Visible = True
                Else
                    Trace.Write("Mileage", "Check 6")
                    btnDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Protected Sub rptMileage_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim intMileage_ID As Int64 = e.CommandArgument
        Dim lblDate As Label = e.Item.FindControl("lblDate")
        Dim strSQLQuery As String

        Select Case e.CommandName
            Case "Delete"
                strSQLQuery = "DELETE FROM [Mileage] WHERE [Mileage_ID] = @Mileage_ID"

                Dim intPayrollExisting As Int64 = PayrollClass.PayrollExists(Session("Client_ID"), lblDate.Text, lblDate.Text)

                If intPayrollExisting <> 0 Then
                    PayrollClass.InvalidatePayroll(intPayrollExisting)
                End If
            Case "Verify"
                strSQLQuery = "UPDATE [Mileage] SET VerifiedBy = @VerifiedBy WHERE [Mileage_ID] = @Mileage_ID"
            Case Else
                strSQLQuery = ""
        End Select

        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@VerifiedBy", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@Mileage_ID", intMileage_ID)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        GetUsers(Me.ViewState.Item("intMethodHolder"), Me.ViewState.Item("strCriteriaHolder"))
    End Sub

    Protected Sub btnAddMileage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If txtMileage.Text <> "" Then
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "INSERT INTO Mileage (User_ID, Client_ID, DateTime, Miles, VerifiedBy) VALUES (@User_ID, @Client_ID, @DateTime, @Miles, @VerifiedBy)"
            objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@DateTime", lblDate.Text)
            objCommand.Parameters.AddWithValue("@Miles", txtMileage.Text)
            If Permissions.MileageValidate(Session("User_ID")) Then
                objCommand.Parameters.AddWithValue("@VerifiedBy", Session("User_ID"))
            Else
                objCommand.Parameters.AddWithValue("@VerifiedBy", 0)
            End If

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            txtMileage.Text = ""
            lblSuccess.Visible = True

            GetUsers(Me.ViewState.Item("intMethodHolder"), Me.ViewState.Item("strCriteriaHolder"))
        End If
    End Sub

    Protected Function TotalMiles() As Integer
        TotalMiles = intTotalMiles
    End Function

    '   Protected Sub calMileage_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '       lblDate.Text = calMileage.SelectedDate
    '       calMileage.Visible = False
    '       btnShowHideCal.ImageUrl = "~/images/arrowdown_small.png"
    '  End Sub

    Protected Sub weekpicker_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim caldate As Date
        caldate = weekpicker.Text
        lblStartDate.Text = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, caldate), Year(caldate), 0)
        lblEndDate.Text = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, caldate), Year(calMileageDisp.SelectedDate), 1)
        '    calMileageDisp.SelectedDates.SelectRange(lblStartDate.Text, lblEndDate.Text)
        '   calMileageDisp.Visible = False
        '      btnShowHideCalDisp.ImageUrl = "~/images/arrowdown_small.png"
        GetUsers(Me.ViewState.Item("intMethodHolder"), Me.ViewState.Item("strCriteriaHolder"))
    End Sub

    '   Protected Sub btnShowHideCal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '      If calMileage.Visible Then
    '          calMileage.Visible = False
    '         btnShowHideCal.ImageUrl = "~/images/arrowdown_small.png"
    '     Else
    '         calMileage.Visible = True
    '        btnShowHideCal.ImageUrl = "~/images/arrowup_small.png"
    '    End If
    ' End Sub

    '  Protected Sub btnShowHideCalDisp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '      If calMileageDisp.Visible Then
    '         calMileageDisp.Visible = False
    '       btnShowHideCalDisp.ImageUrl = "~/images/arrowdown_small.png"
    '    Else
    '       calMileageDisp.Visible = True
    '       btnShowHideCalDisp.ImageUrl = "~/images/arrowup_small.png"
    '   End If
    'End Sub

    '    Protected Sub AllUsersSelection(ByVal sender As Object, ByVal e As CommandEventArgs) Handles culUserList.AllUsersSelected
    '       GetUsers(0, "*")
    '    End Sub

    '    Protected Sub UserListSelection(ByVal sender As Object, ByVal e As CommandEventArgs) Handles culUserList.UserChanged
    'Dim cblUserList As ListItemCollection = e.CommandArgument

    '   Dim strUserList As String = ""
    '   Dim li As ListItem

    '       For Each li In cblUserList
    '          If li.Selected = True Then
    '             strUserList = strUserList & li.Value & ", "
    '         End If
    '     Next

    '    If strUserList <> "" Then
    '         strUserList = Left(strUserList, Len(strUserList) - 2)
    '     End If

    '   GetUsers(0, strUserList)
    '  End Sub

    '   Protected Sub OfficeListSelection(ByVal sender As Object, ByVal e As CommandEventArgs) Handles culUserList.OfficeChanged
    'Dim strSelectedOffice As String = e.CommandArgument
    '     GetUsers(1, strSelectedOffice)
    ' End Sub



End Class
