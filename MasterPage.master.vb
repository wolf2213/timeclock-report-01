
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("User_ID") = "" Then
            If InStr(Request.ServerVariables("SCRIPT_NAME"), "Login.aspx") = 0 And InStr(Request.ServerVariables("SCRIPT_NAME"), "ForgotPassword.aspx") = 0 And InStr(Request.ServerVariables("SCRIPT_NAME"), "UsersAdd.aspx") = 0 And InStr(Request.ServerVariables("SCRIPT_NAME"), "InvalidLink.aspx") = 0 And InStr(Request.ServerVariables("SCRIPT_NAME"), "AccountDisabled.aspx") = 0 Then
                Response.Redirect("Login.aspx")
            End If
        Else
            lblClientName.Text = Session("ClientName")
            If Request.ServerVariables("HTTP_HOST") = "dev.TimeClockWizard.com" Then
                lblClientName.Text = lblClientName.Text & " <font color='red'>(Development Server)</font>"
            End If
            lblUserName.Text = Session("FirstName") & " " & Session("LastName")
            lblDate.Text = Format(Now, "MMMM d, yyyy")
        End If

        If Not Request.IsSecureConnection And Request.ServerVariables("HTTP_HOST") = "www.TimeClockWizard.com" Then
            Response.Redirect(Strings.Replace(Request.Url.AbsoluteUri, "http://", "http://"))
        End If

        If Session("TrialExpired") Then
            If InStr(Request.ServerVariables("SCRIPT_NAME"), "TrialExpired.aspx") = 0 And InStr(Request.ServerVariables("SCRIPT_NAME"), "SettingsSubscription.aspx") = 0 And InStr(Request.ServerVariables("SCRIPT_NAME"), "Terms.aspx") = 0 Then
                Response.Redirect("TrialExpired.aspx")
            End If
        End If

        If Session("AccountDisabled") Then
            If Session("Manager") Then
                'TODO: Once Chase web services are working remove this code
                'If InStr(Request.ServerVariables("SCRIPT_NAME"), "SettingsSubscriptionDelinquent.aspx") = 0 And InStr(Request.ServerVariables("SCRIPT_NAME"), "Terms.aspx") = 0 Then
                '    Response.Redirect("SettingsSubscriptionDelinquent.aspx")
                'End If
                If InStr(Request.ServerVariables("SCRIPT_NAME"), "SettingsSubscription.aspx") = 0 And InStr(Request.ServerVariables("SCRIPT_NAME"), "Terms.aspx") = 0 Then
                    'Response.Redirect("SettingsSubscription.aspx")
                End If
                'TODO: End
            Else
                If InStr(Request.ServerVariables("SCRIPT_NAME"), "Login.aspx") = 0 Then
                    Response.Redirect("Login.aspx")
                End If
            End If
        End If

        'if not manager, don't show settings tab and manager's user manual tab
        If Not Session("Manager") Then
            pnlTabSettings.Visible = False
            pnlTabManagerHelp.Visible = False
            '            lblTimeSheets.Text = "Timesheet Request"
            '            lblTimesheetRequests.Text = "Timesheet Request Status"
        End If

        If Session("UpdateContactInfo") And InStr(Request.ServerVariables("SCRIPT_NAME"), "Login.aspx") = 0 And InStr(Request.ServerVariables("SCRIPT_NAME"), "SettingsContact.aspx") = 0 Then
            'Response.Redirect("SettingsContact.aspx")
        End If

        If InStr(Request.ServerVariables("SCRIPT_NAME"), "Login.aspx") <> 0 Or InStr(Request.ServerVariables("SCRIPT_NAME"), "ForgotPassword.aspx") <> 0 Or (InStr(Request.ServerVariables("SCRIPT_NAME"), "UsersAdd.aspx") <> 0 And Session("User_ID") = "") Then
            pnlTabContainer.Visible = True
            lblUserName.Text = "Welcome"
            lblPleaseLogin.Visible = True
            btnLogout.Visible = False
            If Request.ServerVariables("HTTP_HOST") = "dev.TimeClockWizard.com" Then
                lblClientName.Text = "<font color='red'>(Development Server)</font>"
            Else
                lblClientName.Text = "Welcome"
            End If
            timerLogout.Enabled = False
        Else
            pnlTabContainer.Visible = True
            lblPleaseLogin.Visible = False
            btnLogout.Visible = True
            timerLogout.Interval = Constants.Settings.InactivityTimeout
            timerLogout.Enabled = True
        End If

        If InStr(Request.ServerVariables("SCRIPT_NAME"), "InvalidLink.aspx") <> 0 Then
            pnlTabContainer.Visible = False
            lblUserName.Text = "Invalid Link"
            lblPleaseLogin.Visible = True
            lblPleaseLogin.Text = "Please Try Again"
            btnLogout.Visible = False
            If Request.ServerVariables("HTTP_HOST") = "dev.TimeClockWizard.com" Then
                lblClientName.Text = "<font color='red'>(Development Server)</font>"
            Else
                lblClientName.Visible = False
            End If
            timerLogout.Enabled = False
        End If

        If InStr(Request.ServerVariables("SCRIPT_NAME"), "AccountDisabled.aspx") <> 0 Then
            pnlTabContainer.Visible = False
            lblUserName.Text = "Account Disabled"
            lblPleaseLogin.Visible = True
            lblPleaseLogin.Text = "Contact Support"
            btnLogout.Visible = False
            If Request.ServerVariables("HTTP_HOST") = "dev.TimeClockWizard.com" Then
                lblClientName.Text = "<font color='red'>(Development Server)</font>"
            Else
                lblClientName.Visible = False
            End If
            timerLogout.Enabled = False
        End If

        If InStr(Request.ServerVariables("SCRIPT_NAME"), "ScheduleEdit.aspx") <> 0 Or InStr(Request.ServerVariables("SCRIPT_NAME"), "Timesheets.aspx") <> 0 Then
            timerLogout.Enabled = False
        End If

        If viewTab2Timesheet.Visible = True Then
            If Not Session("WhosInRestriction") Is Nothing Then
                If Session("WhosInRestriction") And Not Session("Manager") Then
                    pnlTab2WhosIn.Visible = False
                End If
            Else
                Dim objDR As System.Data.SqlClient.SqlDataReader
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT WhosInRestriction FROM [Preferences] WHERE Client_ID = @Client_ID"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

                objDR = objCommand.ExecuteReader()
                objDR.Read()

                If objDR.HasRows Then
                    Session("WhosInRestriction") = objDR("WhosInRestriction")
                Else
                    Session("WhosInRestriction") = False
                End If

                If Session("WhosInRestriction") And Not Session("Manager") Then
                    pnlTab2WhosIn.Visible = False
                End If

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing
            End If
        End If

        If viewTab2Schedule.Visible Then
            If Permissions.ScheduleEdit(Session("User_ID")) Then
                Trace.Write(Permissions.ScheduleEdit(Session("User_ID")))
                pnlTab2ViewSchedule.Visible = False
            Else
                pnlTab2EditSchedule.Visible = False
            End If

            If Permissions.AbsencesAdd(Session("User_ID")) Then
                pnlTab2RequestAbsence.Visible = False
            Else
                pnlTab2AddAbsence.Visible = False
            End If

            If Not Permissions.AbsencesApprove(Session("User_ID")) Then
                pnlTab2ApproveAbsences.Visible = False
            End If

            If Not Permissions.AbsencesSettings(Session("User_ID")) Then
                pnlTab2AbsenceSettings.Visible = False
            End If
        End If

        If viewTab2Payroll.Visible Then
            If Permissions.PayrollView(Session("User_ID")) Then
                pnlTab2ViewPayroll.Visible = False
            Else
                pnlTab2Payroll.Visible = False
            End If
        End If

        If viewTab2Users.Visible Then
            If Not Permissions.UsersEdit(Session("User_ID")) And Not Permissions.UsersClockGuard(Session("User_ID")) And Not Permissions.UsersMobileAccess(Session("User_ID")) Then
                pnlTab2ActiveUsers.Visible = False
            End If

            If Not Permissions.UsersEdit(Session("User_ID")) Then
                pnlTab2Edit.Visible = True
                pnlTab2ChangePassword.Visible = True
            Else
                pnlTab2Edit.Visible = False
                pnlTab2ChangePassword.Visible = False
            End If

            If Not Permissions.UsersRecover(Session("User_ID")) Then
                pnlTab2DeletedUsers.Visible = False
            End If

            If Not Permissions.UsersAdd(Session("User_ID")) Then
                pnlTab2AddUser.Visible = False
            End If

            If Not Permissions.UsersCustomFields(Session("User_ID")) Then
                pnlTab2CustomFields.Visible = False
            End If
        End If

        Dim intInvisibleCount As Integer = 0

        If pnlTabSettings.Visible = True Then
            If Not Permissions.SettingsGeneral(Session("User_ID")) Then
                pnlTab2Preferences.Visible = False
                intInvisibleCount += 1
            End If

            If Not Permissions.SettingsOffices(Session("User_ID")) Then
                pnlTab2Offices.Visible = False
                intInvisibleCount += 1
            End If

            If Not Permissions.SettingsClockPoints(Session("User_ID")) Then
                pnlTab2ClockPoints.Visible = False
                intInvisibleCount += 1
            End If

            'If Not Permissions.AccountHolder(Session("User_ID")) Then
            'pnlTab2BusinessInfo.Visible = False
            'pnlTab2MySubscription.Visible = False
            'pnlTab2TransactionHistory.Visible = False
            'intInvisibleCount += 1
            'End If

            If intInvisibleCount = 4 Then
                pnlTabSettings.Visible = False
            End If
        End If

        'pnlTabPayroll.Visible = False
    End Sub

    Protected Sub btnLogout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogout.Click
        Response.Redirect("Logout.aspx")
    End Sub

    Protected Sub timerLogout_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timerLogout.Tick
        Response.Redirect(Constants.InactiveLogoutURL)
    End Sub
End Class