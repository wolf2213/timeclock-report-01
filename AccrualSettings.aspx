<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AccrualSettings.aspx.vb" Inherits="AccrualSettings" title="TimeClockWizard - Absence Accrual Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <div style="margin-left: 25px;">
            <span class="title"><asp:Literal ID="litUsersName" runat="server"></asp:Literal>'s <asp:Literal ID="litAbsenceType" runat="server"></asp:Literal> Accrual Settings</span>
            (<asp:LinkButton ID="btnDisable" runat="server">Stop Accruing <asp:Literal ID="litAbsenceType5" runat="server"></asp:Literal> Time</asp:LinkButton>)

            </div>
                <br /><br /><br /><br />
            <center>

<ul class="accrue-hours" style="width:425px;" >
<li class="column-head column-title">Accrue <asp:Literal ID="litAbsenceType1" runat="server"></asp:Literal> Hours as a Percentage of Time Worked</li></ul>

             <br />

<ul class="accrue-hours" style="width:425px;">
<li>Add <asp:TextBox ID="txtVariable" Width="20" runat="server"></asp:TextBox>% of the employee's work time to <asp:Literal ID="litAbsenceType2" runat="server"></asp:Literal> hours.
                            <br /><br />
                            <asp:Button ID="btnSubmitVariable" runat="server" Text="Save Variable Rate" OnClick="btnSubmitVariable_Click" />&nbsp;&nbsp;&nbsp;<asp:Label ID="lblVariableMessage" runat="server" Text=""></asp:Label>

</li></ul>


<br /><br />
                            - OR - 
<br /><br />

<ul class="accrue-hours" style="width:425px;" >
<li class="column-head column-title">Accrue <asp:Literal ID="litAbsenceType3" runat="server"></asp:Literal> Hours at a Flat Rate</li></ul>


                <br />
                        

<ul class="accrue-hours" style="width:425px;">
<li>
                            Add <asp:TextBox ID="txtFlat" Width="20" runat="server"></asp:TextBox> hours to <asp:Literal ID="litAbsenceType4" runat="server"></asp:Literal> time every week.
                            <br /><br />
                            <asp:Button ID="btnSubmitFlat" runat="server" Text="Save Flat Rate" OnClick="btnSubmitFlat_Click" />&nbsp;&nbsp;&nbsp;<asp:Label ID="lblFlatMessage" runat="server" Text=""></asp:Label>
  

    </li></ul>

<br /><br />
                            - OR - 
<br /><br />

<ul class="accrue-hours" style="width:425px;" >
<li class="column-head column-title">Accrue <asp:Literal ID="litAbsenceType6" runat="server"></asp:Literal> Hours annually at a flat rate</li></ul>
                          
                <br />
<ul class="accrue-hours" style="width:425px;">
<li>

                            Add <asp:TextBox ID="txtAnnual" Width="20" runat="server"></asp:TextBox> hours to <asp:Literal ID="litAbsenceType7" runat="server"></asp:Literal> time every year<br />(based on hire date).
                            <br /><br />
                            <asp:Button ID="btnSubmitAnnual" runat="server" Text="Save Annual Rate" />&nbsp;&nbsp;&nbsp;<asp:Label ID="lblAnnualMessage" runat="server" Text=""></asp:Label>
  
    
        </li></ul>
                     
            </center>
            <br /><br /><br />
<div style="margin-left: 25px;">
            <asp:HyperLink ID="hypCancel" runat="server">Cancel</asp:HyperLink>
</div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>