Imports System.Net.Mail
Imports System.Net

Partial Class SettingsSubscription
    Inherits System.Web.UI.Page

    Dim dblUserCharge As Double
    Dim dblUsageCharge As Double
    Dim dblClockGuardCharge As Double
    Dim dblMobileAccessCharge As Double
    Dim dblFeaturesCharge As Double
    Dim dblTotalCredits As Double
    Dim dblTotalCharge As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabSettings As Panel = Master.FindControl("pnlTabSettings")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Subscription As Panel = Master.FindControl("pnlTab2MySubscription")

        pnlMasterTabSettings.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Settings
        pnlMasterTab2Subscription.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            'TODO: once Chase web services are working fine uncomment this code
            'If Chase.Delinquency(Session("Client_ID")) <> 8 Then
            '    Response.Redirect("SettingsSubscriptionDelinquent.aspx")
            'End If
            'TODO: End

            If Clock.GetNow().Month < 10 Then
                drpExpMonth.SelectedValue = "0" & Clock.GetNow().Month
            Else
                drpExpMonth.SelectedValue = Clock.GetNow().Month
            End If

            drpExpYear.SelectedValue = Right(Clock.GetNow().Year, 2)

            GetBillingInfo()
        End If
    End Sub

    Protected Sub GetBillingInfo()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM Clients WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        If objDR("Trial") Then
            pnlTrialExpiration.Visible = True
            pnlCancelAccount.Visible = False

            Dim intTrialDaysLeft As Integer = DateDiff(DateInterval.Day, CDate(Now().Date & " 12:00 AM"), objDR("NextBillingDate"))
            If intTrialDaysLeft > 1 Then
                lblTrialExpirationDays.Text = " in " & intTrialDaysLeft & " days"
                lblTrialExpirationDate.Text = " on " & CDate(objDR("NextBillingDate")).ToLongDateString
            ElseIf intTrialDaysLeft = 1 Then
                lblTrialExpirationDays.Text = "tomorrow!"
                lblTrialExpirationDate.Visible = False
            ElseIf intTrialDaysLeft = 0 Then
                lblTrialExpirationDays.Text = "today!"
                lblTrialExpirationDate.Visible = False
            Else
                pnlTrialExpiration.Visible = False
                pnlTrialExpired.Visible = True
                lblTrialExpiredOn.Text = CDate(objDR("NextBillingDate")).ToLongDateString
            End If
        End If

        Dim dblPricingBase As Double = Constants.Pricing.Base
        Dim dblPricingUser As Double = Constants.Pricing.User
        Dim dblPricingClockGuard As Double = Constants.Pricing.ClockGuard
        Dim dblPricingMobileAccess As Double = Constants.Pricing.MobileAccess

        If Not objDR("Trial") Then
            dblUserCharge = objDR("MaxUsers") * dblPricingUser
            dblClockGuardCharge = objDR("MaxClockGuard") * dblPricingClockGuard
            dblMobileAccessCharge = objDR("MaxMobileAccess") * dblPricingMobileAccess
        Else
            dblUserCharge = objDR("CurrentUsers") * dblPricingUser
            dblClockGuardCharge = objDR("CurrentClockGuard") * dblPricingClockGuard
            dblMobileAccessCharge = objDR("CurrentMobileAccess") * dblPricingMobileAccess
        End If

        dblUsageCharge = dblPricingBase + dblUserCharge
        dblFeaturesCharge = dblClockGuardCharge + dblMobileAccessCharge
        dblTotalCharge = dblUsageCharge + dblFeaturesCharge

        lblBaseFee.Text = FormatCurrency(dblPricingBase, 2)
        lblUserFeeTotal.Text = FormatCurrency(dblUserCharge, 2)
        lblUsersTotal.Text = objDR("MaxUsers")
        lblUsageTotal.Text = FormatCurrency(dblUsageCharge, 2)

        If Not objDR("Trial") Then
            lblUsersTotal.Text = objDR("MaxUsers")
            lblClockGuardUsers.Text = objDR("MaxClockGuard")
            lblMobileAccessUsers.Text = objDR("MaxMobileAccess")
        Else
            lblUsersTotal.Text = objDR("CurrentUsers")
            lblClockGuardUsers.Text = objDR("CurrentClockGuard")
            lblMobileAccessUsers.Text = objDR("CurrentMobileAccess")
        End If

        lblClockGuardFee.Text = FormatCurrency(dblClockGuardCharge, 2)

        lblMobileAccessFee.Text = FormatCurrency(dblMobileAccessCharge, 2)

        lblFeaturesTotal.Text = FormatCurrency(dblFeaturesCharge, 2)

        ProcessCredits()

        lblMonthlyTotal.Text = FormatCurrency(dblTotalCharge, 2)

        If objDR("CCType") <> 0 Then
            ViewState.Add("strNextBillingDate", CDate(objDR("NextBillingDate")).Date)
            lblBillingDate.Text = "Your next billing date is " & ViewState("strNextBillingDate")

            Select Case objDR("CCType")
                Case 1
                    lblCCType.Text = "Visa"
                Case 2
                    lblCCType.Text = "MasterCard"
                Case 3
                    lblCCType.Text = "American Express"
                Case 4
                    lblCCType.Text = "Discover"
                Case Else
                    lblCCType.Text = "Other"
            End Select

            lblCCLastFour.Text = objDR("CCLastFour")
            lblCCExpDate.Text = CDate(objDR("CCExpDate")).Month & "/" & CDate(objDR("CCExpDate")).Year
            txtCardName.Text = objDR("First_name") & " " & objDR("Last_name")
            drpCardType.SelectedItem.Value = objDR("CCType")
            txtCardNumber.Text = "XXXXXXXXXXXX" & objDR("CCLastFour")
            If CDate(objDR("CCExpDate")).Month < 10 Then
                drpExpMonth.SelectedValue = "0" & CDate(objDR("CCExpDate")).Month
            Else
                drpExpMonth.SelectedValue = CDate(objDR("CCExpDate")).Month
            End If
            drpExpYear.SelectedValue = Right(CDate(objDR("CCExpDate")).Year, 2)

            lblCCInfoPrompt.Text = "Please update your credit card information below"
            pnlCreditCardInfo.DefaultButton = "btnUpdate"
            btnActivate.Visible = False
            btnUpdate.Visible = True
        Else
            lblBillingDate.Visible = False
            lblBillingDateTrial.Visible = True
            pnlCreditCardStored.Visible = False
            pnlCreditCardInfo.Visible = True
            pnlCreditCardInfo.CssClass = "show"
        End If

        If Not objDR("Enabled") Then
            pnlCurrentSummary.Visible = False
            pnlAccountCancelled.Visible = True
            lblCancelDisable.Text = ViewState("strNextBillingDate")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub ProcessCredits()
        Dim objDRCredits As System.Data.SqlClient.SqlDataReader
        Dim objCommandCredits As System.Data.SqlClient.SqlCommand
        Dim objConnectionCredits As System.Data.SqlClient.SqlConnection
        objConnectionCredits = New System.Data.SqlClient.SqlConnection

        objConnectionCredits.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnectionCredits.Open()
        objCommandCredits = New System.Data.SqlClient.SqlCommand()
        objCommandCredits.Connection = objConnectionCredits
        objCommandCredits.CommandText = "SELECT * FROM Credits WHERE Client_ID = @Client_ID AND AmountRemaining > 0 ORDER BY AddedOn ASC"
        objCommandCredits.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDRCredits = objCommandCredits.ExecuteReader()

        If objDRCredits.HasRows Then
            pnlCredits.Visible = True
            rptCredits.DataSource = objDRCredits
            rptCredits.DataBind()
        End If
    End Sub

    Protected Sub rptCredits_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptCredits.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If dblTotalCharge >= 0 Then
                Dim lblDescription As Label = e.Item.FindControl("lblDescription")
                Dim lblAmount As Label = e.Item.FindControl("lblAmount")

                Dim dblCreditAmount As Double

                If e.Item.DataItem("Type") = 1 Then
                    lblDescription.Text = Strings.FormatCurrency(e.Item.DataItem("Amount")) & " " & e.Item.DataItem("Description")
                    dblCreditAmount = (e.Item.DataItem("AmountRemaining") * -1)
                Else
                    lblDescription.Text = e.Item.DataItem("Amount") & "% off " & e.Item.DataItem("Description")
                    dblCreditAmount = (dblTotalCharge * (e.Item.DataItem("AmountRemaining") / 100) * -1)
                End If

                If (dblCreditAmount * -1) > dblTotalCharge Then
                    dblCreditAmount = (dblTotalCharge * -1)
                End If

                dblTotalCharge += dblCreditAmount

                If dblCreditAmount < 0 Then
                    lblAmount.Text = Strings.FormatCurrency(dblCreditAmount, 2)
                Else
                    lblAmount.Text = "not applied"
                End If
            End If
        End If
    End Sub

    Protected Sub btnCancelAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAccount.Click
        pnlCurrentSummary.Visible = False
        pnlCancelConfirm.Visible = True
    End Sub

    Protected Sub btnCancelConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelConfirm.Click
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Firstname + ' ' + Lastname as UserName, Password FROM Users WHERE User_ID = @User_ID AND Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        If (Crypto.VerifyHash(txtCancelPassword.Text, objDR("Password"))) Or (Session("BOSS-User_ID") <> Nothing) Then
            lblCancelIncorrectPass.Visible = False
            DisableEnableAccount(False)
            AccountCancellationEmail(objDR("UserName"))
            SendAccountCancellationFollowupEmail()
            lblCancelDisable.Text = ViewState("strNextBillingDate")
            pnlAccountCancelled.Visible = True
            pnlCancelConfirm.Visible = False
        Else
            lblCancelIncorrectPass.Visible = True
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub btnEnableAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnableAccount.Click
        DisableEnableAccount(True)
        pnlAccountCancelled.Visible = False
        pnlCurrentSummary.Visible = True
    End Sub

    Protected Sub btnChangeCCInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeCCInfo.Click
        pnlCreditCardStored.Visible = False
        pnlCreditCardInfo.Visible = True
        pnlCreditCardInfo.CssClass = "show"
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim strChaseResponse As String

        If Len(txtCardName.Text) < 1 Then
            strChaseResponse = "ErrorCN,Please enter the name on the card"
        ElseIf Not IsNumeric(txtCardNumber.Text) Then
            strChaseResponse = "ErrorCC,Invalid Card Number"
        ElseIf Len(txtCardVerify.Text) < 2 Or Len(txtCardVerify.Text) > 5 Or Not IsNumeric(txtCardVerify.Text) Then
            strChaseResponse = "ErrorCVV,Invalid Card Verification Code"
        Else
            'TODO: once Chase webservices are working, uncomment the code
            'strChaseResponse = Chase.UpdateProfile(Session("Client_ID"), txtCardNumber.Text, drpExpMonth.SelectedValue & drpExpYear.SelectedValue, txtCardName.Text)
            strChaseResponse = "Approved,Approved"
            Dim sEmailMsg As String = String.Empty
            sEmailMsg = sEmailMsg & "Credit card information updated for client_id => " & Session("Client_ID") & " (Client Name : " & Offices.ClientName(Session("Client_ID")) & ")" & "<br><br><br>"
            sEmailMsg = sEmailMsg & "Name on the Credit Card => " & txtCardName.Text & "<br>"
            sEmailMsg = sEmailMsg & "Credit Card Type => " & drpCardType.SelectedItem.Text & "<br>"
            sEmailMsg = sEmailMsg & "New Card Number => " & txtCardNumber.Text & "<br>"
            sEmailMsg = sEmailMsg & "Expiration Date => " & drpExpMonth.SelectedValue & "/" & drpExpYear.SelectedValue & "(MM/YY)<br>"
            sEmailMsg = sEmailMsg & "Security Code => " & txtCardVerify.Text & "<br><br><br>"
            sEmailMsg = sEmailMsg & "Please update the Chase PaymentTech with the above information." & "<br>"
            NotifyCreditCardInfoChanged(sEmailMsg, False)
            'TODO: End
        End If

        Try
            Dim arrayResponse As Array = Split(strChaseResponse, ",")

            Select Case arrayResponse(0)
                Case "Approved"
                    Chase.UpdateBillingInfo(Session("Client_ID"), drpCardType.SelectedValue, Right(txtCardNumber.Text, 4), drpExpMonth.SelectedValue & "/1/" & drpExpYear.SelectedValue & " 12:00 AM")
                    pnlCreditCardInfo.Visible = False
                    pnlCreditCardApproved.Visible = True

                Case "Declined"
                    lblResults.ForeColor = Drawing.Color.Red
                    lblResults.Text = "Declined, please try again.<br />Please contact TimeClockWizard Support if you keep receiving this error."
                Case Else
                    lblResults.ForeColor = Drawing.Color.Red
                    lblResults.Text = arrayResponse(1)
            End Select
        Catch
            lblResults.ForeColor = Drawing.Color.Red
            lblResults.Text = "An error has occured.<br />Please contact TimeClockWizard Support with the following information:<br />" & strChaseResponse
        End Try
    End Sub

    Protected Sub btnActivate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivate.Click
        Dim strChaseResponse As String

        If Len(txtCardName.Text) < 1 Then
            strChaseResponse = "ErrorCN,Please enter the name on the card"
        ElseIf Not IsNumeric(txtCardNumber.Text) Then
            strChaseResponse = "ErrorCC,Invalid Card Number"
        ElseIf Len(txtCardVerify.Text) < 2 Or Len(txtCardVerify.Text) > 5 Or Not IsNumeric(txtCardVerify.Text) Then
            strChaseResponse = "ErrorCVV,Invalid Card Verification Code"
        Else
            GetBillingInfo()
            'TODO: once Chase webservices are working, uncomment the code
            'strChaseResponse = Chase.NewTransactionNewProfile(Session("Client_ID"), txtCardNumber.Text, drpExpMonth.SelectedValue & drpExpYear.SelectedValue, txtCardVerify.Text, txtCardName.Text, dblTotalCharge)
            strChaseResponse = "Approved,Approved"
            Dim sEmailMsg As String = String.Empty
            sEmailMsg = sEmailMsg & "The following customer activated account:" & "<br><br><br>"
            sEmailMsg = sEmailMsg & "Credit card information for client_id => " & Session("Client_ID") & " (Client Name : " & Offices.ClientName(Session("Client_ID")) & ")" & "<br>"
            sEmailMsg = sEmailMsg & "Name on the Credit Card => " & txtCardName.Text & "<br>"
            sEmailMsg = sEmailMsg & "Credit Card Type => " & drpCardType.SelectedItem.Text & "<br>"
            sEmailMsg = sEmailMsg & "New Card Number => " & txtCardNumber.Text & "<br>"
            sEmailMsg = sEmailMsg & "Expiration Date => " & drpExpMonth.SelectedValue & "/" & drpExpYear.SelectedValue & "(MM/YY)<br>"
            sEmailMsg = sEmailMsg & "Security Code => " & txtCardVerify.Text & "<br><br><br>"
            sEmailMsg = sEmailMsg & "Charge amount => " & Strings.FormatCurrency(dblTotalCharge, 2) & "<br><br><br>"
            sEmailMsg = sEmailMsg & "Please create account in the Chase PaymentTech with the above information and charge the amount." & "<br>"
            NotifyCreditCardInfoChanged(sEmailMsg, True)
            'TODO: End
        End If

        Try
            Dim arrayResponse As Array = Split(strChaseResponse, ",")

            Select Case arrayResponse(0)
                Case "Approved"
                    Chase.StoreBillingInfo(Session("Client_ID"), DateAdd(DateInterval.Month, 1, Now.Date), drpCardType.SelectedValue, Right(txtCardNumber.Text, 4), drpExpMonth.SelectedValue & "/1/" & drpExpYear.SelectedValue & " 12:00 AM")
                    Chase.RecordTransaction(Session("Client_ID"), arrayResponse(1), dblTotalCharge, "Activated Account on " & Now.ToShortDateString)
                    pnlCreditCardInfo.Visible = False
                    lblActivated.Visible = True
                    pnlCreditCardApproved.Visible = True
                    lblBillingDateTrial.Visible = False
                    lblBillingDate.Visible = True
                    lblBillingDate.Text = "Your next billing date is " & DateAdd(DateInterval.Month, 1, Now.Date)
                Case "Declined"
                    lblResults.ForeColor = Drawing.Color.Red
                    lblResults.Text = "Declined, please try again.<br />Please call TimeClockWizard Support if you keep receiving this error."
                Case Else
                    lblResults.ForeColor = Drawing.Color.Red
                    lblResults.Text = arrayResponse(1)
            End Select
        Catch
            lblResults.ForeColor = Drawing.Color.Red
            lblResults.Text = "An error has occured.<br />Please contact TimeClockWizard Support with the following information:<br />" & strChaseResponse
        End Try
    End Sub

    Protected Sub DisableEnableAccount(ByVal boolEnable As Boolean)
        'if boolEnable is true, then they are RE-ENABLING the account
        'if it is false, they are DISABLING the account

        Dim strSupportSubject As String
        Dim strSupportBody As String

        'Enable/Disable the accouont
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE Clients SET Enabled = @EnableValue WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@EnableValue", boolEnable)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'Make the support log entry

        If boolEnable Then
            strSupportSubject = "Billing Enabled"
        Else
            strSupportSubject = "Billing Disabled"
        End If

        strSupportBody = "Requested by " & UserInfo.NameFromID(Session("User_ID"), 1) & ".  Next billing date is " & ViewState("strNextBillingDate") & "."

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "INSERT INTO BossSupportHistory (Client_ID, User_ID, Boss_User_Id, Type, Timestamp, Subject, Body) VALUES (@Client_ID, @User_ID, 8, 4, @Timestamp, @Subject, @Body)"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@Timestamp", Date.Now())
        objCommand.Parameters.AddWithValue("@Subject", strSupportSubject)
        objCommand.Parameters.AddWithValue("@Body", strSupportBody)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If boolEnable Then
            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT Firstname + ' ' + Lastname as UserName FROM Users WHERE User_ID = @User_ID AND Client_ID = @Client_ID"
            objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

            objDR = objCommand.ExecuteReader()
            objDR.Read()

            AccountActivateEmail(objDR("UserName"))

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
    End Sub

    'TODO: once Chase webservices are working fine, uncomment this code
    Protected Sub NotifyCreditCardInfoChanged(ByVal strMessage As String, ByVal bActivate As Boolean)
        Dim mail As New MailMessage()

        If bActivate Then
            'set the addresses
            mail.From = New MailAddress("info@TimeClockWizard.com", "New Account Activated")
            mail.Subject = "New Account Activated"
        Else
            'set the addresses
            mail.From = New MailAddress("info@TimeClockWizard.com", "Credit Card Information Changed")
            mail.Subject = "Credit Card Information Changed"
        End If

        mail.To.Add("info@TimeClockWizard.com")
        mail.Body = strMessage
        mail.IsBodyHtml = True

        'send the message
        Dim smtp2 As New SmtpClient()
        smtp2.Send(mail)

    End Sub 'TODO: End

    Protected Sub SendAccountCancellationFollowupEmail()
        Dim mail As New MailMessage()
        Dim strMessage As String = String.Empty
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT [CompanyName], [Address], [Address2], [City], [State], [Zip], [First_name], [Last_name], [Email], [Phone] FROM Clients WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()
        objDR.Read()

        strMessage = strMessage & "Client cancelled account, client_id => " & Session("Client_ID") & "<br>"
        strMessage = strMessage & "Client Company Name => " & Trim(objDR("CompanyName")) & "<br>"
        strMessage = strMessage & "Client Name => " & Trim(objDR("First_name")) & " " & Trim(objDR("Last_name")) & "<br>"
        strMessage = strMessage & "Client Phone # => " & Trim(Null.Replace(objDR("Phone"))) & "<br>"
        strMessage = strMessage & "Client Email => " & Trim(objDR("Email")) & "<br><br><br>"
        strMessage = strMessage & "Please follow-up with client on why they cancelled their account. If there is anything we can do to bring them back.<br><br><br>"
        strMessage = strMessage & "Thank you," & "<br>"
        strMessage = strMessage & "TimeClockWizard Application" & "<br>"

        'set the addresses
        mail.From = New MailAddress("info@TimeClockWizard.com", "Account Cancelled by Customer")
        mail.Subject = "!!!Account Cancelled by Customer!!!"

        mail.To.Add("info@TimeClockWizard.com")
        mail.Body = strMessage
        mail.IsBodyHtml = True

        'send the message
        Dim smtp2 As New SmtpClient()
        smtp2.Send(mail)

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub AccountCancellationEmail(ByVal strAccountCancelledUserName As String)
        Trace.Write("SettingsSubscription.aspx", "Emailing Client " & Session("Client_ID") & " for Account Cancellation by User Name " & Session("User_ID"))

        Dim strSubjectLine As String

        Dim strCommonBody1 As String
        Dim strCommonBody2 As String
        Dim strCommonBody3 As String

        Dim strCommonClose1 As String
        Dim strCommonClose2 As String
        Dim strCommonClose3 As String

        Dim strEmail As String = String.Empty

        Dim strPlainBody As String
        Dim strHTMLBody As String

        'get the username to put in the bottom box
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT [Email] FROM Clients WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        If objDR.Read Then
            strEmail = objDR("Email")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        strSubjectLine = "Account Cancellation Confirmation"

        strCommonBody1 = "Hello " & strAccountCancelledUserName & ","
        strCommonBody2 = "We are sorry to see you go. As per your request we have cancelled your TimeClockWizard account." & vbCrLf
        strCommonBody2 = strCommonBody2 & "You account will be active and you can use it until your next billing date." & vbCrLf
        strCommonBody2 = strCommonBody2 & "After that date has passed, your account will be deactivated and you will not be billed."
        strCommonBody3 = "Thank you for being a valued customer, if there is anything we can do to make you come back, please let us know. " & vbCrLf
        strCommonBody3 = strCommonBody3 & "We will do our best to ensure your satisfaction. We appreciate your business."

        strCommonClose1 = "Sincerely,"
        strCommonClose2 = "TimeClockWizard Support Team"
        strCommonClose3 = "866.627.7083 / info@TimeClockWizard.com"

        strPlainBody = strCommonBody1 & vbCrLf & vbCrLf
        strPlainBody &= strCommonBody2 & vbCrLf & vbCrLf
        strPlainBody &= strCommonBody3 & vbCrLf & vbCrLf
        strPlainBody &= strCommonClose1 & vbCrLf
        strPlainBody &= strCommonClose2 & vbCrLf
        strPlainBody &= strCommonClose3 & vbCrLf & vbCrLf

        strHTMLBody = "<html><head><title>" & strSubjectLine & "</title>"
        strHTMLBody &= "<style type='text/css'>"
        strHTMLBody &= "body {margin:10px;margin-top:0px;padding:0px;background-color:#FFF;font-family:Verdana;font-size:12px;color:#666;}"
        strHTMLBody &= "</style></head>"
        strHTMLBody &= "<body><img src=""cid:ciiLogo""><br><br>"
        strHTMLBody &= strCommonBody1 & "<br><br>"
        strHTMLBody &= strCommonBody2 & "<br><br>"
        strHTMLBody &= strCommonBody3 & "<br><br>"
        strHTMLBody &= strCommonClose1 & "<br>"
        strHTMLBody &= strCommonClose2 & "<br>"
        strHTMLBody &= strCommonClose3 & "<br><br>"
        strHTMLBody &= "</body></html>"

        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
        mail.Subject = strSubjectLine
        mail.To.Add(strEmail.ToLower)
        mail.CC.Add("info@TimeClockWizard.com")

        'set the content
        Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString(strPlainBody, Nothing, "text/plain")
        Dim HTMLView As AlternateView = AlternateView.CreateAlternateViewFromString(strHTMLBody, Nothing, "text/html")

        Try
            Dim logo As New LinkedResource("C:\Inetpub\wwwroot\TimeClockWizard.com\ClockitinSite\Images\emaillogo2.jpg", "image/jpeg")
            logo.ContentId = "ciiLogo"
            HTMLView.LinkedResources.Add(logo)
        Catch ex As Exception

        End Try

        mail.AlternateViews.Add(plainView)
        mail.AlternateViews.Add(HTMLView)

        'send the message
        Dim smtp As New SmtpClient()
        smtp.Send(mail)
        Trace.Write("SettingsSubscription.aspx", "Emailing Client " & Session("Client_ID") & " for Account Cancellation by User Name " & Session("User_ID") & ". Done")
    End Sub

    Protected Sub AccountActivateEmail(ByVal strAccountActivatedUserName As String)
        Trace.Write("SettingsSubscription.aspx", "Emailing Client " & Session("Client_ID") & " for Account Activated by User Name " & Session("User_ID"))

        Dim strSubjectLine As String

        Dim strCommonBody1 As String
        Dim strCommonBody2 As String
        Dim strCommonBody3 As String

        Dim strCommonClose1 As String
        Dim strCommonClose2 As String
        Dim strCommonClose3 As String

        Dim strEmail As String = String.Empty

        Dim strPlainBody As String
        Dim strHTMLBody As String

        'get the username to put in the bottom box
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT [Email] FROM Clients WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        If objDR.Read Then
            strEmail = objDR("Email")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        strSubjectLine = "Account Activation Confirmation"

        strCommonBody1 = "Hello " & strAccountActivatedUserName & ","
        strCommonBody2 = "As per your request we have activated your TimeClockWizard account."
        strCommonBody3 = "If you have any questions, do not hesitate to contact us.  Our phone number and email address is listed below."

        strCommonClose1 = "Sincerely,"
        strCommonClose2 = "TimeClockWizard Support Team"
        strCommonClose3 = "866.627.7083 / info@TimeClockWizard.com"

        strPlainBody = strCommonBody1 & vbCrLf & vbCrLf
        strPlainBody &= strCommonBody2 & vbCrLf & vbCrLf
        strPlainBody &= strCommonBody3 & vbCrLf & vbCrLf
        strPlainBody &= strCommonClose1 & vbCrLf
        strPlainBody &= strCommonClose2 & vbCrLf
        strPlainBody &= strCommonClose3 & vbCrLf & vbCrLf

        strHTMLBody = "<html><head><title>" & strSubjectLine & "</title>"
        strHTMLBody &= "<style type='text/css'>"
        strHTMLBody &= "body {margin:10px;margin-top:0px;padding:0px;background-color:#FFF;font-family:Verdana;font-size:12px;color:#666;}"
        strHTMLBody &= "</style></head>"
        strHTMLBody &= "<body><img src=""cid:ciiLogo""><br><br>"
        strHTMLBody &= strCommonBody1 & "<br><br>"
        strHTMLBody &= strCommonBody2 & "<br><br>"
        strHTMLBody &= strCommonBody3 & "<br><br>"
        strHTMLBody &= strCommonClose1 & "<br>"
        strHTMLBody &= strCommonClose2 & "<br>"
        strHTMLBody &= strCommonClose3 & "<br><br>"
        strHTMLBody &= "</body></html>"

        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
        mail.Subject = strSubjectLine
        mail.To.Add(strEmail.ToLower)
        mail.CC.Add("info@TimeClockWizard.com")

        'set the content
        Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString(strPlainBody, Nothing, "text/plain")
        Dim HTMLView As AlternateView = AlternateView.CreateAlternateViewFromString(strHTMLBody, Nothing, "text/html")

        Try
            Dim logo As New LinkedResource("C:\Inetpub\wwwroot\TimeClockWizard.com\ClockitinSite\Images\emaillogo2.jpg", "image/jpeg")
            logo.ContentId = "ciiLogo"
            HTMLView.LinkedResources.Add(logo)
        Catch ex As Exception

        End Try

        mail.AlternateViews.Add(plainView)
        mail.AlternateViews.Add(HTMLView)

        'send the message
        Dim smtp As New SmtpClient()
        smtp.Send(mail)
        Trace.Write("SettingsSubscription.aspx", "Emailing Client " & Session("Client_ID") & " for Account Cancellation by User Name " & Session("User_ID") & ". Done")
    End Sub
End Class
