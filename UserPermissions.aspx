<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserPermissions.aspx.vb" Inherits="UserPermissions" title="TimeClockWizard - User Permissions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   <!-- <asp:Image ID="Shield" ImageUrl="~/images/permissions_large.png" ImageAlign="AbsMiddle" runat="server" /> -->
    <span class="title"><asp:Literal ID="litFullUserName" runat="server"></asp:Literal>'s Permission Settings</span>
    <asp:Label ID="lblMessage" runat="server" ></asp:Label>
    <center>
        <table cellspacing="30px">
            <tr>
                <td valign="top">
                    <table>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Image38" ImageUrl="~/images/permissions.png" ImageAlign="AbsMiddle" runat="server" />
                                        Account Holder                    
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlAccountHolder" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image39" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkAccountHolder" runat="server" AutoPostBack="true" />Account Holder&nbsp;<asp:Image ID="imgHelpAccountHolder" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" ToolTip = "cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Account Holder] body=[Account Holder's automatically have all permissions.  Additionally, they can call support and are presumed to be able to handle billing issues.  You can have as many Account Holders as you would like.]" />
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <tr><td>&nbsp;</td></tr>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Timesheets" ImageUrl="~/images/tab2_timesheets.png" ImageAlign="AbsMiddle" runat="server" />
                                        Timesheets                    
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlTimesheetsDisplay" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image2" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkTimesheetsDisplay" runat="server" AutoPostBack="true" />View other users' time sheets                    
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlTimesheetsValidate" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image32" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image34" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkTimesheetsValidate" runat="server" AutoPostBack="true" />Validate other users' time records                 
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlTimesheetsAdd" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image1" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image49" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image50" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkTimesheetsAdd" runat="server" />Manually add a record for other users                    
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlTimesheetsEdit" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image3" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image36" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image51" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkTimesheetsEdit" runat="server" />Edit time records without verification                    
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlTimesheetsDelete" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image4" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image37" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image52" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkTimesheetsDelete" runat="server" />Delete time records without verification                    
                                        </td>
                                    </tr>
                               </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <tr><td>&nbsp;</td></tr>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Image5" ImageUrl="~/images/tab2_mileage.png" ImageAlign="AbsMiddle" runat="server" />
                                        Mileage                    
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlMileageDisplay" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image6" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkMileageDisplay" runat="server" />View other users' mileage                   
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlMileageValidate" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image7" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkMileageValidate" runat="server" />Validate mileage                 
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlMileageDelete" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image8" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkMileageDelete" runat="server" />Delete mileage                   
                                        </td>
                                    </tr>
                               </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <tr><td>&nbsp;</td></tr>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Image9" ImageUrl="~/images/tab2_viewschedule.png" ImageAlign="AbsMiddle" runat="server" />
                                        Schedule                    
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlScheduleEdit" runat="server">
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Image10" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                        <asp:CheckBox ID="chkScheduleEdit" runat="server" AutoPostBack="true" />Edit schedule                  
                                    </td>
                                </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlScheduleCopy" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image11" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image13" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkScheduleCopy" runat="server" />Copy schedule                 
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlScheduleDelete" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image12" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image14" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkScheduleDelete" runat="server" />Delete schedule                   
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <tr><td>&nbsp;</td></tr>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Image15" ImageUrl="~/images/tab2_absence.png" ImageAlign="AbsMiddle" runat="server" />
                                        Absences                    
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlAbsencesAdd" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image16" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkAbsencesAdd" runat="server" />Add absences for any user              
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlAbsencesApprove" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image17" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkAbsencesApprove" runat="server" />Approve absence requests                
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlAbsencesDelete" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image19" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkAbsencesDelete" runat="server" />Delete absences                 
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlAbsencesSettings" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image21" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkAbsencesSettings" runat="server" />Edit absence settings                
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Image18" ImageUrl="~/images/money_envelope.png" ImageAlign="AbsMiddle" runat="server" />
                                        Payroll                    
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlPayrollView" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image23" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkPayrollView" runat="server" AutoPostBack="true" />View other users' payroll                   
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlPayrollAdd" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image20" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image54" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkPayrollAdd" runat="server" />Add Payroll                   
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlPayrollDelete" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image22" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image53" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkPayrollDelete" runat="server" />Delete Payroll                   
                                        </td>
                                    </tr>
                               </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <tr><td>&nbsp;</td></tr>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Image25" ImageUrl="~/images/tab2_activeusers.png" ImageAlign="AbsMiddle" runat="server" />
                                        Users                    
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlUsersEdit" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image26" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersEdit" runat="server" />Edit other users                 
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersDelete" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image27" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersDelete" runat="server" />Inactivate users               
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersClockGuard" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image28" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersClockGuard" runat="server" />Enable/disable ClockGuard                  
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersMobileAccess" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image24" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersMobileAccess" runat="server" />Enable/disable Mobile Access                  
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersRecover" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image40" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersRecover" runat="server" />Activate users
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersAdd" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image41" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersAdd" AutoPostBack="true" runat="server" />Add new users
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersAddManager" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image42" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image44" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersAddManager" runat="server" />Add new managers
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersAddManagerFull" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image43" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" /><asp:Image ID="Image45" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersAddManagerFull" runat="server" />Add new managers (full permissions)&nbsp;<asp:Image ID="imgHelpAddFull" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" ToolTip="cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Add Manager (Full Permissions)] body=[This user can add a manger with full permissions, regardless of what his or her permissions are.]" />
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersEditPermissions" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image46" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersEditPermissions" runat="server" />Edit other users' permissions
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersEditPermissionsFull" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image47" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersEditPermissionsFull" runat="server" />Edit other users' permissions (full permissions)&nbsp;<asp:Image ID="imgHelpEditFull" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" ToolTip="cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Edit User (Full Permissions)] body=[This user can edit all user fields including fields they may not have permission to control otherwise.]" />
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlUsersCustomFields" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image35" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkUsersCustomFields" runat="server" />Manage custom field settings
                                        </td>
                                    </tr>
                               </asp:Panel>
                                <asp:Panel ID="pnlManageEmployees" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image48" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkManageEmployees" runat="server" />Manage Employees&nbsp;<asp:Image ID="Image55" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" ToolTip="cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Manage Employees] body=[This manager manages only assigned employees. You must select this if you want to assign employees to this manager.<br><br>Unselecting this permission will reassign all employees managed by this manager are reassigned to account holder.]" />
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <tr><td>&nbsp;</td></tr>
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Image29" ImageUrl="~/images/gear_small.png" ImageAlign="AbsMiddle" runat="server" />
                                        Settings                    
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlSettingsGeneral" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image30" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkSettingsGeneral" runat="server" />General Preferences                
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlSettingsOffices" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image31" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkSettingsOffices" runat="server" />Offices                
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlSettingsClockPoints" runat="server">
                                    <tr>
                                        <td align="left">
                                            <asp:Image ID="Image33" ImageUrl="~/images/placeholder.png" ImageAlign="AbsMiddle" runat="server" />
                                            <asp:CheckBox ID="chkSettingsClockPoints" runat="server" />ClockPoints                   
                                        </td>
                                    </tr>
                               </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnSet" runat="server" Text="Save Permissions" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnClear" runat="server" Text="Reset Form" />
                    <br /><br />
                    Users may need to logout and log back in before permission changes take effect.
                </td>
            </tr>
        </table>
    </center>
</asp:Content>