<%@ WebHandler Language="VB" Class="Logo" %>

Imports System
Imports System.Web

Public Class Logo : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Logo, MIMEType FROM Preferences WHERE Client_ID = @Client_ID AND MIMEType <> '0'"
        objCommand.Parameters.AddWithValue("@Client_ID", context.Request.QueryString("id"))
        objDR = objCommand.ExecuteReader()
   
        If objDR.Read() Then
            context.Response.ContentType = objDR("MIMEType")
            context.Response.BinaryWrite(objDR("Logo"))
        Else
            context.Response.ContentType = "text/plain"
            context.Response.Write("No logo for this ID")
        End If
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class