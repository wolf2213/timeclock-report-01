
Partial Class Absences_approve
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        If Not Permissions.AbsencesApprove(Session("User_ID")) Then
            Server.Transfer("Absences.aspx", False)
        End If

        Dim pnlMasterTabSchedule As Panel = Master.FindControl("pnlTabSchedule")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2ApproveAbsences As Panel = Master.FindControl("pnlTab2ApproveAbsences")

        pnlMasterTabSchedule.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Schedule
        pnlMasterTab2ApproveAbsences.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            ShowRequests()
        End If
    End Sub

    Protected Sub ShowRequests()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
            objCommand.CommandText = "SELECT *, Users.LastName + ', ' + Users.FirstName AS FullName FROM Absences, Users WHERE Absences.User_ID = Users.User_ID AND Users.Client_ID = @Client_ID AND Absences.Approved = 0 AND Users.Active = 1 AND (Users.ManagerUserId = @ManagerUserId OR Users.User_Id = @User_Id) ORDER BY FullName ASC"
            objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
        Else
            objCommand.CommandText = "SELECT *, Users.LastName + ', ' + Users.FirstName AS FullName FROM Absences, Users WHERE Absences.User_ID = Users.User_ID AND Users.Client_ID = @Client_ID AND Absences.Approved = 0 AND Users.Active = 1 ORDER BY FullName ASC"
        End If
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        rptAbsences.DataSource = objDR
        rptAbsences.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptAbsences_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Header Then
            Dim imgHelpHours As Image = e.Item.FindControl("imgHelpHours")
            imgHelpHours.ToolTip = "header=[Hours To Deduct] body=[The number of hours that should be deducted from the user's alloted time for the specified absence type.]"
        End If

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim litHiddenUserID As Literal = e.Item.FindControl("litHiddenUserID")
            Dim lblName As Label = e.Item.FindControl("lblName")
            Dim lblType As Label = e.Item.FindControl("lblType")
            Dim lblStartDate As Label = e.Item.FindControl("lblStartDate")
            Dim lblEndDate As Label = e.Item.FindControl("lblEndDate")
            Dim txtHours As TextBox = e.Item.FindControl("txtHours")
            Dim lblDays As Label = e.Item.FindControl("lblDays")
            Dim lblNote As Label = e.Item.FindControl("lblNote")
            Dim btnApprove As ImageButton = e.Item.FindControl("btnApprove")
            Dim btnDeny As ImageButton = e.Item.FindControl("btnDeny")

            litHiddenUserID.Text = e.Item.DataItem("User_ID")
            lblName.Text = e.Item.DataItem("FullName")
            lblType.Text = AbsencesClass.AbsenceNameFromType(e.Item.DataItem("Type"))
            lblStartDate.Text = e.Item.DataItem("StartDate")
            lblEndDate.Text = e.Item.DataItem("EndDate")
            lblDays.Text = DateDiff(DateInterval.Day, e.Item.DataItem("StartDate"), e.Item.DataItem("EndDate")) + 1
            lblNote.Text = e.Item.DataItem("Note")


            btnApprove.CommandArgument = e.Item.DataItem("Absence_ID")
            btnDeny.CommandArgument = e.Item.DataItem("Absence_ID")

            btnApprove.ToolTip = "offsetx=[-150] header=[Approve Absence Request] body=[]"
            btnDeny.ToolTip = "offsetx=[-150] header=[Deny Absence Request] body=[]"


        End If
    End Sub

    Protected Sub rptAbsences_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim strCommandName As String = e.CommandName
        Dim intAbsenceID As Int64 = e.CommandArgument

        Select Case strCommandName
            Case "Deny"
                Trace.Write("Absences_approve", "Deny Absence ID " & intAbsenceID)
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection
                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "UPDATE Absences SET Approved = 2, ApprovedDeniedBy = @Manager_ID WHERE Absence_ID = @Absence_ID"
                objCommand.Parameters.AddWithValue("@Absence_ID", intAbsenceID)
                objCommand.Parameters.AddWithValue("@Manager_ID", Session("User_ID"))

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                lblMessage.ForeColor = Drawing.Color.Green
                lblMessage.Text = "The absence has been denied.<br><br>"

                'update the page
                ShowRequests()

            Case "Approve"
                Dim boolHoursError As Boolean
                Dim strErrorText As String
                Dim txtHours As TextBox = e.Item.FindControl("txtHours")
                Dim lblDays As Label = e.Item.FindControl("lblDays")
                Dim litHiddenUserID As Literal = e.Item.FindControl("litHiddenUserID")

                Dim intUser_Id As Int64 = CDbl(litHiddenUserID.Text)
                Dim intHoursAllowed = CInt(lblDays.Text) * 24

                If txtHours.Text = "" Then
                    boolHoursError = True
                    strErrorText = "Must enter how many hours the absence should deduct."
                Else
                    If CDbl(txtHours.Text) > CDbl(intHoursAllowed) Then
                        boolHoursError = True
                        strErrorText = "Cannot deduct more than 24 hours per day."
                    End If

                    If CDbl(txtHours.Text) <= 0 Then
                        boolHoursError = True
                        strErrorText = "Hours to deduct must be greater than 0."
                    End If
                End If

                If boolHoursError Then
                    txtHours.BorderColor = Drawing.Color.Red
                    txtHours.BackColor = Drawing.Color.Pink
                    txtHours.BorderStyle = BorderStyle.Solid
                    txtHours.BorderWidth = 1
                    lblMessage.Text = strErrorText & "<br><br>"
                    lblMessage.ForeColor = Drawing.Color.Red
                Else
                    'there is no error, approve the request
                    AbsencesClass.AddAbsence(intUser_Id, Session("Client_Id"), intAbsenceID, Session("User_ID"), txtHours.Text)

                    Trace.Write("Absences_approve", "Hours Updated, display success message")
                    lblMessage.ForeColor = Drawing.Color.Green
                    lblMessage.Text = "The absence has been approved and added to the schedule.<br><br>"

                    'update the page
                    ShowRequests()
                End If
        End Select
    End Sub
End Class