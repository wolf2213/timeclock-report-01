
Partial Class AccrualSettings
    Inherits System.Web.UI.Page
    Dim intUserId As Int64
    Dim intClientID As Int64
    Dim intAbsenceType As Integer
    Dim intEnabled As Integer
    Dim dblRate As Double
    Dim strColBase As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabSchedule As Panel = Master.FindControl("pnlTabSchedule")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")

        pnlMasterTabSchedule.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Schedule

        intUserId = Request.QueryString("user_id")
        intClientID = Session("Client_ID")

        litUsersName.Text = UserInfo.NameFromID(intUserId, 1)
        intAbsenceType = CInt(Request.QueryString("type"))
        strColBase = Replace(AbsencesClass.AbsenceNameFromType(intAbsenceType), " ", "")

        If Not IsPostBack Then
            litAbsenceType.Text = AbsencesClass.AbsenceNameFromType(intAbsenceType)
            litAbsenceType1.Text = litAbsenceType.Text
            litAbsenceType2.Text = litAbsenceType.Text
            litAbsenceType3.Text = litAbsenceType.Text
            litAbsenceType4.Text = litAbsenceType.Text
            litAbsenceType5.Text = litAbsenceType.Text
            litAbsenceType6.Text = litAbsenceType.Text
            litAbsenceType7.Text = litAbsenceType.Text

            hypCancel.NavigateUrl = "AbsencesSettings.aspx?type=" & intAbsenceType

            UpdateSettingsPane()
        End If
    End Sub

    Protected Sub UpdateSettingsPane()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT [" & strColBase & "Enabled] AS Enabled, [" & strColBase & "Rate] AS Rate FROM AbsenceUserSettings WHERE User_ID = @User_ID AND Client_ID = @Client_Id"
        objCommand.Parameters.AddWithValue("@User_ID", intUserId)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            intEnabled = cdbl(objDR("Enabled"))
            dblRate = CDbl(objDR("Rate"))
        Else
            intEnabled = 0
            dblRate = 0
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If intEnabled <> 0 Then
            If intEnabled = 1 Then
                txtVariable.Text = dblRate
                txtFlat.Text = ""
                txtAnnual.Text = ""
            ElseIf intEnabled = 2 Then
                txtFlat.Text = dblRate
                txtVariable.Text = ""
                txtAnnual.Text = ""
            ElseIf intEnabled = 3 Then
                txtAnnual.Text = dblRate
                txtVariable.Text = ""
                txtFlat.Text = ""
            End If
        End If
    End Sub

    Protected Sub btnSubmitVariable_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsNumeric(txtVariable.Text) Then
            lblVariableMessage.Text = "You must enter a numeric value"
            lblVariableMessage.ForeColor = System.Drawing.Color.Red
        Else
            Dim dblUpdateRate As Double = CDbl(txtVariable.Text)
            If dblUpdateRate < 0 Then
                lblVariableMessage.Text = "The value must be positive"
                lblVariableMessage.ForeColor = System.Drawing.Color.Red

            Else
                'clear out the other sides message
                lblFlatMessage.Text = ""
                lblAnnualMessage.Text = ""

                'peform the update
                UpdateAccrualSettings(1, dblUpdateRate)
            End If
        End If
    End Sub

    Protected Sub btnSubmitFlat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsNumeric(txtFlat.Text) Then
            lblFlatMessage.Text = "You must enter a numeric value"
            lblFlatMessage.ForeColor = System.Drawing.Color.Red
        Else
            Dim dblUpdateRate As Double = FormatNumber(CDbl(txtFlat.Text), 2)
            If dblUpdateRate < 0 Then
                lblFlatMessage.Text = "The value must be positive"
                lblFlatMessage.ForeColor = System.Drawing.Color.Red
            Else
                'clear out the other sides message
                lblVariableMessage.Text = ""
                lblAnnualMessage.Text = ""

                'peform the update
                UpdateAccrualSettings(2, dblUpdateRate)
            End If
        End If
    End Sub

    Protected Sub btnSubmitAnnual_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitAnnual.Click
        If Not IsNumeric(txtAnnual.Text) Then
            lblAnnualMessage.Text = "You must enter a numeric value"
            lblAnnualMessage.ForeColor = System.Drawing.Color.Red
        Else
            Dim dblUpdateRate As Double = FormatNumber(CDbl(txtAnnual.Text), 2)
            If dblUpdateRate < 0 Then
                lblAnnualMessage.Text = "The value must be positive"
                lblAnnualMessage.ForeColor = System.Drawing.Color.Red
            Else
                'clear out the other sides message
                lblVariableMessage.Text = ""
                lblFlatMessage.Text = ""

                'peform the update
                UpdateAccrualSettings(3, dblUpdateRate)
            End If
        End If
    End Sub

    Protected Sub UpdateAccrualSettings(ByVal intAccrualType As Integer, ByVal dblRate As Double)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE AbsenceUserSettings SET [" & strColBase & "Enabled] = @AccrualType, [" & strColBase & "Rate] = @Rate WHERE User_ID = @User_ID AND Client_ID = @Client_ID "
        objCommand.Parameters.AddWithValue("@AccrualType", intAccrualType)
        objCommand.Parameters.AddWithValue("@Rate", dblRate)
        objCommand.Parameters.AddWithValue("@User_ID", intUserId)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        UpdateSettingsPane()

        If intAccrualType = 1 Then
            lblVariableMessage.Text = "Settings Saved"
            lblVariableMessage.ForeColor = System.Drawing.Color.Green
        ElseIf intAccrualType = 2 Then
            lblFlatMessage.Text = "Settings Saved"
            lblFlatMessage.ForeColor = System.Drawing.Color.Green
        Else
            lblAnnualMessage.Text = "Settings Saved"
            lblAnnualMessage.ForeColor = System.Drawing.Color.Green
        End If
    End Sub

    Protected Sub btnDisable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisable.Click
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE AbsenceUserSettings SET [" & strColBase & "Enabled] = 0 WHERE User_ID = @User_ID AND Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserId)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        Response.Redirect("AbsencesSettings.aspx?type=" & intAbsenceType)
    End Sub

   
End Class
