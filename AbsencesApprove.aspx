<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AbsencesApprove.aspx.vb" Inherits="Absences_approve" title="TimeClockWizard - Approve Absences" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="margin-left: 25px;">
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            
                 <asp:Repeater ID="rptAbsences" OnItemDataBound="rptAbsences_ItemDataBound" OnItemCommand="rptAbsences_ItemCommand" EnableViewState="True" runat="server">
                    <HeaderTemplate>
<ul class="row-user">
<li class="column-head column-title" style="line-height: 34px;">Name</li></ul>

<ul class="absence-type">
<li class="column-head column-title">Type</li></ul>

<ul class="date-start">
<li class="column-head column-title">Start Date</li></ul>
                               
<ul class="date-end">
<li class="column-head column-title">End Date</li></ul>

<ul class="days">
<li class="column-head column-title">Days</li></ul>
                 
<ul class="hours-used" style="width: 115px;">
<li class="column-head column-title">Hours <asp:Image ID="imgHelpHours" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" /></li></ul>

<ul class="notes">
<li class="column-head column-title">Note</li></ul>
                               
<ul class="editlinks" style="width: 50px;">
 <li class="column-head column-title">&nbsp;</li> </ul>                                   
                                    
                      
                    </HeaderTemplate>
                    <ItemTemplate>
                        <ul class="row-user">
                            <li>
                                <asp:Literal ID="litHiddenUserID" Visible="false" runat="server"></asp:Literal>
                                <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                            </li>
                        </ul>

                        <ul class="absence-type">
                            <li><asp:Label ID="lblType" runat="server" Text=""></asp:Label>
                            </li>
                        </ul>

                        <ul class="date-start">
                            <li><asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                            </li>
                        </ul>
                               
                        <ul class="date-end">
                            <li><asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                            </li>
                        </ul>

                        <ul class="days">
                            <li><asp:Label ID="lblDays" runat="server" Text=""></asp:Label>
                            </li>
                        </ul>
                 
                        <ul class="hours-used" style="width: 115px;">
                            <li><asp:TextBox ID="txtHours" width="115px" runat="server"></asp:TextBox>
                            </li>
                        </ul>
      
                        <ul class="notes">
                            <li><asp:Label ID="lblNote" runat="server" Text=""></asp:Label>
                            </li>
                        </ul>
                               
                        <ul class="editlinks" style="width: 50px;">
                            <li><asp:ImageButton ID="btnApprove" runat="server" CommandName="Approve" ImageUrl="~/images/check-green_small.png" AlternateText="Approve this request" />
                                <asp:ImageButton ID="btnDeny" runat="server" CommandName="Deny" ImageUrl="~/images/red-x_small.png" AlternateText="Deny this request" />
                            </li> 
                        </ul> 
                        
                    </ItemTemplate>
 
                    <FooterTemplate>
                        
                    </FooterTemplate>
                </asp:Repeater>
         </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

