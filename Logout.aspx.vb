
Partial Class Logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session("PermissionTimesheetsDisplay") = Nothing
        Session("PermissionTimesheetsEdit") = Nothing
        Session("PermissionAbsencesApprove") = Nothing
        Session("PermissionSettingsGeneral") = Nothing
        Session("PermissionSettingsOffices") = Nothing
        Session("PermissionSettingsClockPoints") = Nothing
        Session("PermissionAccountHolder") = Nothing
        Session("PermissionUsersEdit") = Nothing
        Session("PermissionUsersClockGuard") = Nothing
        Session("PermissionUsersMobileAccess") = Nothing
        Session("PermissionUsersEditPermissions") = Nothing
        Session("PermissionUsersDelete") = Nothing
        Session("PermissionUsersRecover") = Nothing
        Session("PermissionUsersAdd") = Nothing
        Session("PermissionUsersCustomFields") = Nothing
        Session("PermissionUsersAddManagerFull") = Nothing
        Session("Manager") = Nothing



        If Session("BackToBoss") Then
            Dim strBOSSFirstName As String = Session("BOSS-FirstName")
            Dim strBOSSLastName As String = Session("BOSS-LastName")
            Dim strBOSSUser_ID As String = Session("BOSS-User_ID")
            Dim strBOSSClient_ID As Int64 = Session("BOSS-Client_ID")
            Dim strBOSSCompanyname As String = Session("BOSS-CompanyName")

            Session.RemoveAll()
            Session.Clear()

            Session("BOSS-FirstName") = strBOSSFirstName
            Session("BOSS-LastName") = strBOSSLastName
            Session("BOSS-User_ID") = strBOSSUser_ID
            Session("BOSS-Client_ID") = strBOSSClient_ID
            Session("BOSS-CompanyName") = strBOSSCompanyname

            Response.Redirect("Boss/Users.aspx")
        Else
            Session.RemoveAll()
            Session.Clear()
        End If

        If Not Request.Cookies("Subdomain") Is Nothing Then
            If Request.QueryString("reason") = "" Then
                Response.Redirect("Login.aspx?client=" & Request.Cookies("Subdomain").Value)
            Else
                Response.Redirect("Login.aspx?reason=" & Request.QueryString("reason") & "&client=" & Request.Cookies("Subdomain").Value)
            End If
        Else
            Response.Redirect("Login.aspx?client=unknown")
        End If
    End Sub
End Class



