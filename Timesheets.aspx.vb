Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data

Partial Class Timesheets
    Inherits System.Web.UI.Page

    Dim boolFirstPageBreak As Boolean = False
    Public intDurationTotal As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabHome As Panel = Master.FindControl("pnlTabHome")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Timesheets As Panel = Master.FindControl("pnlTab2Timesheets")

        pnlMasterTabHome.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Timesheet
        pnlMasterTab2Timesheets.CssClass = "tab2 tab2On"

        lblSuccess.Visible = False
        lblClockTimesError.Visible = False

        timerLogout.Interval = Constants.Settings.InactivityTimeout
        timerLogout.Enabled = True

        If Not Session("Manager") Then
            lblManuallyAddRecord.Text = "Submit Timesheet Request Record&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
        End If

        If Permissions.TimesheetsAdd(Session("User_ID")) Then
            lblReason.Visible = False
            txtReason.Visible = False
            btnAddRecord.Visible = True
            btnAddRequest.Visible = False
            lblSuccess.Text = "<br /><br />Record was added successfully<br />"
        Else
            lblReason.Visible = True
            txtReason.Visible = True
            btnAddRecord.Visible = False
            btnAddRequest.Visible = True
            lblSuccess.Text = "<br /><br />Record was requested successfully<br />"
        End If

        If Not IsPostBack Then
            litInOut1.Text = "Clocked In"
            litInOut2.Text = "Clocked Out"

            calClockedIn.SelectedDate = calClockedIn.TodaysDate
            lblClockedInDate.Text = calClockedIn.SelectedDate
            txtClockedInHour.Text = Time.Convert24HourTo12Hour(Hour(Now))
            txtClockedInMinute.Text = Time.AddZero(Minute(Now))
            drpClockedInAMPM.SelectedValue = Time.AMPM(Hour(Now))

            calClockedOut.SelectedDate = calClockedOut.TodaysDate
            lblClockedOutDate.Text = calClockedOut.SelectedDate
            txtClockedOutHour.Text = Time.Convert24HourTo12Hour(Hour(Now))
            txtClockedOutMinute.Text = Time.AddZero(Minute(Now))
            drpClockedOutAMPM.SelectedValue = Time.AMPM(Hour(Now))

            Dim dtmStartDate As Date



            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT MAX(EndDate) AS NewStartDate FROM PayrollSummary WHERE Client_ID = @Client_ID"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()

                dtmStartDate = Now.AddDays(-14)
                If Null.Replace(objDR("NewStartDate"), "null") <> "null" Then
                    If Math.Abs(DateDiff(DateInterval.Day, Now, objDR("NewStartDate"))) <= 31 Then
                        dtmStartDate = DateAdd(DateInterval.Day, 1, objDR("NewStartDate"))
                    End If
                End If
            Else
                dtmStartDate = Now.AddDays(-14)
            End If

            Dim strStartDate As String = dtmStartDate.Date & " 12:00 AM"

            Trace.Write("Default.aspx", "Display start date = " & strStartDate)

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            calDisplayStartDate.SelectedDate = strStartDate
            lblDisplayStartDate.Text = calDisplayStartDate.SelectedDate
            calDisplayEndDate.SelectedDate = calDisplayStartDate.TodaysDate
            lblDisplayEndDate.Text = calDisplayEndDate.SelectedDate

            ViewState("DisplayMethod") = 0
            ViewState("DisplayCriteria") = Session("User_ID")

            If Permissions.TimesheetsDisplay(Session("User_ID")) Then
                BindUserDrop()
                culDisplayUsers.InitialSelection = 1
            Else
                ddlChooseUser.Visible = False
                lblManualUser.Visible = True
                lblManualUser.Text = Session("FirstName") & " " & Session("LastName")

                culDisplayUsers.Visible = False
                lblDisplayUser.Visible = True
                lblDisplayUser.Text = Session("FirstName") & " " & Session("LastName")
                chkRemainClockedIn.Visible = False
                litClockInSpacer.Visible = False
            End If

            If Permissions.TimesheetsValidate(Session("User_ID")) Then
                pnlMassVerify.Visible = True
            End If

            If Request.QueryString("view") = "errors" Then
                pnlDisplayCriteria.Visible = False
                lblDisplayErrors.Visible = True
                pnlManualAdd.Visible = False
                lblVerifyAll.Text = "Clear All Errors"

                If Permissions.TimesheetsDisplay(Session("User_ID")) Then
                    ViewState("DisplayCriteria") = "*"
                End If
            End If

            GetTimeRecords()
        End If
    End Sub

    Protected Sub GetTimeRecords()
        Dim SQLQuery As String

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection

        If ViewState("DisplayMethod") = 0 Then
            'Select by user
            If ViewState("DisplayCriteria") = "*" Then
                If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
                    SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE (([Client_ID] = @Client_ID) AND ([Active] = 1) AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id)) ORDER BY LastName ASC"
                    objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
                    objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
                Else
                    SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE (([Client_ID] = @Client_ID) AND ([Active] = 1)) ORDER BY LastName ASC"
                End If
            ElseIf ViewState("DisplayCriteria") = "" Then
                SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE ([User_ID] = 0) AND ([Active] = 1) ORDER BY LastName ASC"
            Else
                SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE ([User_ID] IN (" & ViewState("DisplayCriteria") & ")) ORDER BY LastName ASC"
            End If
            Else
                'Select by office
                If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
                SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE [Office_ID] = @Office_ID AND ([Active] = 1) AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) ORDER BY LastName ASC"
                    objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
                    objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
                Else
                SQLQuery = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName FROM [Users] WHERE [Office_ID] = @Office_ID AND ([Active] = 1) ORDER BY LastName ASC"
                End If
            End If

        objCommand.CommandText = SQLQuery
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@Office_ID", ViewState("DisplayCriteria"))

        objDR = objCommand.ExecuteReader()

        rptUsers.DataSource = objDR
        rptUsers.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        UpdatePanel2.Update()
    End Sub

    Protected Sub BindUserDrop()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
            objCommand.CommandText = "SELECT User_ID, LastName + ', ' + FirstName AS FullName FROM Users WHERE Client_ID = @Client_ID AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) AND Active = 1 ORDER BY LastName, FirstName"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
        Else
            objCommand.CommandText = "SELECT User_ID, LastName + ', ' + FirstName AS FullName FROM Users WHERE Client_ID = @Client_ID AND Active = 1 ORDER BY LastName, FirstName"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        End If
        objDR = objCommand.ExecuteReader()

        ddlChooseUser.DataSource = objDR
        ddlChooseUser.DataTextField = "FullName"
        ddlChooseUser.DataValueField = "User_ID"
        ddlChooseUser.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        ddlChooseUser.SelectedValue = Session("User_ID")
    End Sub

    Protected Sub btnAddRecord_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddRecord.Click
        Trace.Write("Timesheets.aspx", "Begin btnAddRecord_Click")
        Dim dtStartTime As DateTime = lblClockedInDate.Text & " " & txtClockedInHour.Text & ":" & txtClockedInMinute.Text & " " & drpClockedInAMPM.SelectedValue
        Dim dtEndTime As DateTime = lblClockedOutDate.Text & " " & txtClockedOutHour.Text & ":" & txtClockedOutMinute.Text & " " & drpClockedOutAMPM.SelectedValue

        If DateDiff(DateInterval.Minute, dtStartTime, dtEndTime) >= 1 Or chkRemainClockedIn.Checked Then
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection

            Dim intType As Integer

            If rblType.SelectedValue = "1" Then
                intType = 1
            Else
                Dim objDR As System.Data.SqlClient.SqlDataReader
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT PaidBreaks FROM Preferences WHERE Client_ID = @Client_ID"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objDR = objCommand.ExecuteReader()

                If objDR.Read Then
                    If objDR("PaidBreaks") Then
                        intType = 3
                    Else
                        intType = 2
                    End If
                End If

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing
            End If

            Dim strSQLQuery As String
            If chkRemainClockedIn.Checked Then
                strSQLQuery = "INSERT INTO TimeRecords (User_ID, Client_ID, Type, StartTime, StartLocation_ID, StartLocation, ManualBy) VALUES (@User_ID, @Client_ID, @Type, @StartTime, @StartLocation_ID, @StartLocation, @ManualBy)"
            Else
                strSQLQuery = "INSERT INTO TimeRecords (User_ID, Client_ID, Type, StartTime, StartLocation_ID, StartLocation, EndTime, EndLocation_ID, EndLocation, VerifiedBy, ManualBy) VALUES (@User_ID, @Client_ID, @Type, @StartTime, @StartLocation_ID, @StartLocation, @EndTime, @EndLocation_ID, @EndLocation, @VerifiedBy, @ManualBy)"
            End If

            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = strSQLQuery
            objCommand.Parameters.AddWithValue("@User_ID", ddlChooseUser.SelectedValue)
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@Type", intType)
            objCommand.Parameters.AddWithValue("@StartTime", dtStartTime)
            objCommand.Parameters.AddWithValue("@StartLocation_ID", Locations.LocationID(Session("Client_ID"), Request.ServerVariables("REMOTE_ADDR")))
            objCommand.Parameters.AddWithValue("@StartLocation", Request.ServerVariables("REMOTE_ADDR"))
            objCommand.Parameters.AddWithValue("@EndTime", dtEndTime)
            objCommand.Parameters.AddWithValue("@EndLocation_ID", Locations.LocationID(Session("Client_ID"), Request.ServerVariables("REMOTE_ADDR")))
            objCommand.Parameters.AddWithValue("@EndLocation", Request.ServerVariables("REMOTE_ADDR"))
            If Permissions.TimesheetsValidate(Session("User_ID")) Then
                objCommand.Parameters.AddWithValue("@VerifiedBy", Session("User_ID"))
            Else
                objCommand.Parameters.AddWithValue("@VerifiedBy", 0)
            End If
            objCommand.Parameters.AddWithValue("@ManualBy", Session("User_ID"))

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            lblSuccess.Visible = True
            lblClockTimesError.Visible = False

            Dim intPayrollExisting As Int64 = PayrollClass.PayrollExists(Session("Client_ID"), dtStartTime.Date, dtEndTime.Date)

            If intPayrollExisting <> 0 Then
                PayrollClass.InvalidatePayroll(intPayrollExisting)
            End If
        Else
            lblSuccess.Visible = False
            lblClockTimesError.Visible = True
        End If
        Trace.Write("Timesheets.aspx", "End btnAddRecord_Click")
    End Sub

    Protected Sub btnAddRequest_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddRequest.Click
        Trace.Write("Timesheets", "Begin btnAddRequest_Click")
        Dim dtStartTime As DateTime = lblClockedInDate.Text & " " & txtClockedInHour.Text & ":" & txtClockedInMinute.Text & " " & drpClockedInAMPM.SelectedValue
        Dim dtEndTime As DateTime = lblClockedOutDate.Text & " " & txtClockedOutHour.Text & ":" & txtClockedOutMinute.Text & " " & drpClockedOutAMPM.SelectedValue

        Trace.Write("btnAddRequest_Click", "DateDiff Result: " & DateDiff(DateInterval.Minute, dtStartTime, dtEndTime))
        If DateDiff(DateInterval.Minute, dtStartTime, dtEndTime) >= 1 Then
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            Dim DateRequested As Date = Date.Now

            Trace.Write("btnAddRequest_Click", "Executing insert")
            Dim intType As Integer

            If rblType.SelectedValue = "1" Then
                intType = 1
            Else
                Dim objDR As System.Data.SqlClient.SqlDataReader
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT PaidBreaks FROM Preferences WHERE Client_ID = @Client_ID"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objDR = objCommand.ExecuteReader()

                If objDR.Read Then
                    If objDR("PaidBreaks") Then
                        intType = 3
                    Else
                        intType = 2
                    End If
                End If

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing
            End If

            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "INSERT INTO TimeRequests ([Client_ID], [User_ID], [Time_ID], [AddType], [StartTime], [EndTime], [Reason],[DateRequested]) VALUES (@Client_ID, @User_ID, 0, @AddType, @StartTime, @EndTime, @Reason,@DateRequested)"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@AddType", intType)
            objCommand.Parameters.AddWithValue("@StartTime", dtStartTime)
            objCommand.Parameters.AddWithValue("@EndTime", dtEndTime)
            If txtReason.Text <> "" Then
                objCommand.Parameters.AddWithValue("@Reason", txtReason.Text)
            Else
                objCommand.Parameters.AddWithValue("@Reason", "<em>No reason specified</em>")
            End If
            objCommand.Parameters.AddWithValue("@DateRequested", DateRequested)
            objCommand.ExecuteNonQuery()
            Trace.Write("btnAddRequest_Click", "Insert complete")

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            lblSuccess.Visible = True
            lblClockTimesError.Visible = False

            GetTimeRecords()
        Else
            lblSuccess.Visible = False
            lblClockTimesError.Visible = True
        End If
        Trace.Write("Timesheets", "End btnAddRequest_Click")
    End Sub

    Protected Sub rptUsers_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim SQLQuery As String = ""
            Dim pnlPageBreak As Panel = e.Item.FindControl("pnlPageBreak")
            Dim pnlUserInfo As Panel = e.Item.FindControl("pnlUserInfo")
            Dim lblUserName As Label = e.Item.FindControl("lblUserName")
            Dim lblNoTimes As Label = e.Item.FindControl("lblNoTimes")
            Dim rptTimes As Repeater = e.Item.FindControl("rptTimes")

            lblUserName.Text = e.Item.DataItem("FullName")
            intDurationTotal = 0

            If rblSort.SelectedValue = "DESC" Then
                SQLQuery = "SELECT * FROM [TimeRecords] WHERE ([User_ID] = @User_ID) AND ([StartTime] BETWEEN @StartTime AND @EndTime) ORDER BY StartTime DESC"
            ElseIf rblSort.SelectedValue = "ASC" Then
                SQLQuery = "SELECT * FROM [TimeRecords] WHERE ([User_ID] = @User_ID) AND ([StartTime] BETWEEN @StartTime AND @EndTime) ORDER BY StartTime ASC"
            End If

            If Request.QueryString("view") = "errors" Then
                SQLQuery = "SELECT * FROM [TimeRecords] WHERE ([User_ID] = @User_ID) AND [PossibleError] = 1 ORDER BY StartTime DESC"
            End If

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = SQLQuery
            objCommand.Parameters.AddWithValue("@User_ID", e.Item.DataItem("User_ID"))
            objCommand.Parameters.AddWithValue("@StartTime", lblDisplayStartDate.Text & " 12:00 AM")
            objCommand.Parameters.AddWithValue("@EndTime", lblDisplayEndDate.Text & " 11:59 PM")

            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                rptTimes.DataSource = objDR
                rptTimes.DataBind()
                lblNoTimes.Visible = False
                If Not boolFirstPageBreak Then
                    pnlPageBreak.Visible = False
                    boolFirstPageBreak = True
                End If
            Else
                pnlPageBreak.Visible = False
                pnlUserInfo.CssClass = "doNotPrint"
                lblNoTimes.Visible = True

                If Request.QueryString("view") = "errors" Then
                    lblNoTimes.Text = "&nbsp;&nbsp;-No Possible Errors"
                End If
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
    End Sub

    Protected Sub rptTimes_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim pnlTimeRow As Panel = e.Item.FindControl("pnlTimeRow")
            Dim pnlEditEndTime As Panel = e.Item.FindControl("pnlEditEndTime")
            Dim pnlEditNoEndTime As Panel = e.Item.FindControl("pnlEditNoEndTime")
            Dim btnEditClockOut As Button = e.Item.FindControl("btnEditClockOut")
            Dim imgError As Image = e.Item.FindControl("imgError")
            Dim imgRecordType As Image = e.Item.FindControl("imgRecordType")
            Dim imgEditRecordType As Image = e.Item.FindControl("imgEditRecordType")
            Dim lblDate As Label = e.Item.FindControl("lblDate")
            Dim imgStartLocation As Image = e.Item.FindControl("imgStartLocation")
            Dim lblStartTime As Label = e.Item.FindControl("lblStartTime")
            Dim txtStartHour As TextBox = e.Item.FindControl("txtStartHour")
            Dim txtStartMin As TextBox = e.Item.FindControl("txtStartMin")
            Dim drpStartAMPM As DropDownList = e.Item.FindControl("drpStartAMPM")
            Dim txtStartDate As TextBox = e.Item.FindControl("txtStartDate")
            Dim txtEndHour As TextBox = e.Item.FindControl("txtEndHour")
            Dim txtEndMin As TextBox = e.Item.FindControl("txtEndMin")
            Dim drpEndAMPM As DropDownList = e.Item.FindControl("drpEndAMPM")
            Dim txtEndDate As TextBox = e.Item.FindControl("txtEndDate")
            Dim dtStart As Date = e.Item.DataItem("StartTime")
            Dim imgEndLocation As Image = e.Item.FindControl("imgEndLocation")
            Dim lblEndTime As Label = e.Item.FindControl("lblEndTime")
            Dim btnVerify As ImageButton = e.Item.FindControl("btnVerify")
            Dim lblDuration As Label = e.Item.FindControl("lblDuration")
            Dim lblEditReason As Label = e.Item.FindControl("lblEditReason")
            Dim txtEditReason As TextBox = e.Item.FindControl("txtEditReason")

            pnlTimeRow.CssClass = "timeTR tr" & litAlternate.Text
            If litAlternate.Text = "Normal" Then
                litAlternate.Text = "Alternate"
            Else
                litAlternate.Text = "Normal"
            End If

            If e.Item.DataItem("PossibleError") Then
                imgError.Visible = True
            End If

            imgRecordType.ToolTip = "header=[Clock In/Out]"
            imgEditRecordType.ToolTip = "header=[Clock In/Out]"

            If CInt(e.Item.DataItem("Type")) = 2 Or CInt(e.Item.DataItem("Type")) = 3 Then
                'break
                imgRecordType.ImageUrl = "~/images/timesheet_break.png"
                imgEditRecordType.ImageUrl = "~/images/timesheet_break.png"

                If CInt(e.Item.DataItem("Type")) = 2 Then
                    imgRecordType.AlternateText = "Unpaid Break"
                    imgRecordType.ToolTip = "header=[Unpaid Break]"
                    imgEditRecordType.AlternateText = "Unpaid Break"
                    imgEditRecordType.ToolTip = "header=[Unpaid Break]"
                Else
                    imgRecordType.AlternateText = "Paid Break"
                    imgRecordType.ToolTip = "header=[Paid Break]"
                    imgEditRecordType.AlternateText = "Paid Break"
                    imgEditRecordType.ToolTip = "header=[Paid Break]"
                End If
            ElseIf CInt(e.Item.DataItem("Type")) = 4 Then
                'absence
                imgRecordType.ImageUrl = "~/images/tab2_absence.png"
                imgRecordType.AlternateText = "Absence"
                imgRecordType.ToolTip = "header=[Absence]"
                imgEditRecordType.ImageUrl = "~/images/tab2_absence.png"
                imgEditRecordType.AlternateText = "Absence"
                imgEditRecordType.ToolTip = "header=[Absence]"
            End If

            lblDate.Text = Time.FormatLongDate(dtStart.ToLongDateString)
            lblStartTime.Text = Time.QuickStrip(e.Item.DataItem("StartTime"))

            If DatePart(DateInterval.Hour, dtStart) < 12 Then
                txtStartHour.Text = DatePart(DateInterval.Hour, dtStart)
                drpStartAMPM.SelectedValue = "AM"
            ElseIf DatePart(DateInterval.Hour, dtStart) = 12 Then
                txtStartHour.Text = "12"
                drpStartAMPM.SelectedValue = "PM"
            Else
                txtStartHour.Text = DatePart(DateInterval.Hour, dtStart) - 12
                drpStartAMPM.SelectedValue = "PM"
            End If

            txtStartMin.Text = Time.AddZero(DatePart(DateInterval.Minute, dtStart))
            txtStartDate.Text = dtStart.Date

            Dim intDuration As Integer = TimeRecords.TimeDifference(e.Item.DataItem("StartTime").ToString, e.Item.DataItem("EndTime").ToString)
            Dim intDurationToAdd As Integer = 0

            imgRecordType.ToolTip &= " body=[Duration: " & TimeRecords.FormatMinutes(intDuration, Session("User_ID"), False) & "]"
            imgEditRecordType.ToolTip &= " body=[Duration: " & TimeRecords.FormatMinutes(intDuration, Session("User_ID"), False) & "]"

            If e.Item.DataItem("Type") = 2 Then
                'the break is unpaid, add the negative break duration
                intDurationToAdd = intDuration * -1
            ElseIf e.Item.DataItem("Type") = 3 Then
                'this is a paid break, set duration = 0 so that the time isn't counted twice
                intDurationToAdd = 0
            Else
                intDurationToAdd = intDuration
            End If

            intDurationTotal = intDurationTotal + intDurationToAdd

            lblDuration.Text = TimeRecords.FormatMinutes(intDuration, Session("User_ID"), True)

            If e.Item.DataItem("StartLocation_ID") <> 99999999 Then
                Select Case Locations.LocationType(e.Item.DataItem("StartLocation_ID"))
                    Case 0 'Unverified Computer
                        imgStartLocation.ImageUrl = "~/images/timesheet_computer0.png"
                        imgStartLocation.AlternateText = "Location: Unverified" & vbCrLf & "IP Address: " & e.Item.DataItem("StartLocation")
                        imgStartLocation.ToolTip = "header=[Location] body=[Unverified<br>IP Address: " & e.Item.DataItem("StartLocation") & "]"
                    Case 1 'Verified Cookie
                        imgStartLocation.ImageUrl = "~/images/timesheet_computer.png"
                        imgStartLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("StartLocation_ID"))
                        imgStartLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("StartLocation_ID")) & "]"
                    Case 2 'Verified IP Address
                        imgStartLocation.ImageUrl = "~/images/timesheet_computer.png"
                        imgStartLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("StartLocation_ID"))
                        imgStartLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("StartLocation_ID")) & "]"
                    Case 3 'Verified Caller ID
                        imgStartLocation.ImageUrl = "~/images/timesheet_phone.png"
                        imgStartLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("StartLocation_ID"))
                        imgStartLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("StartLocation_ID")) & "]"
                    Case 4 'Mobile Access
                        imgStartLocation.ImageUrl = "~/images/timesheet_mobile.png"
                        imgStartLocation.AlternateText = "Location: Mobile Access" & vbCrLf & "IP Address: " & e.Item.DataItem("StartLocation")
                        imgStartLocation.ToolTip = "header=[Location] body=[Mobile Access<br>IP Address: " & e.Item.DataItem("StartLocation") & "]"
                End Select
            Else
                imgStartLocation.ImageUrl = "~/images/timesheet_phone0.png"
                imgStartLocation.AlternateText = "Location: Unverified" & vbCrLf & "Caller ID: " & e.Item.DataItem("StartLocation")
                imgStartLocation.ToolTip = "header=[Location] body=[Unverified<br>Caller ID: " & e.Item.DataItem("StartLocation") & "]"
            End If

            If Not e.Item.DataItem("EndTime") Is System.DBNull.Value Then
                Dim dtEnd As Date = e.Item.DataItem("EndTime")

                'Some times end location id is null
                If CInt(Null.Replace(e.Item.DataItem("EndLocation_ID"), "0")) <> 99999999 Then
                    Select Case Locations.LocationType(CInt(Null.Replace(e.Item.DataItem("EndLocation_ID"), "0")))
                        Case 0 'Unverified Computer
                            imgEndLocation.ImageUrl = "~/images/timesheet_computer0.png"
                            imgEndLocation.AlternateText = "Location: Unverified" & vbCrLf & "IP Address: " & e.Item.DataItem("EndLocation")
                            imgEndLocation.ToolTip = "header=[Location] body=[Unverified<br>IP Address: " & e.Item.DataItem("EndLocation") & "]"
                        Case 1 'Verified Cookie
                            imgEndLocation.ImageUrl = "~/images/timesheet_computer.png"
                            imgEndLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("EndLocation_ID")) & vbCrLf & "IP Address: " & e.Item.DataItem("EndLocation")
                            imgEndLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("EndLocation_ID")) & "]"
                        Case 2 'Verified IP Address
                            imgEndLocation.ImageUrl = "~/images/timesheet_computer.png"
                            imgEndLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("EndLocation_ID")) & vbCrLf & "IP Address: " & e.Item.DataItem("EndLocation")
                            imgEndLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("EndLocation_ID")) & "]"
                        Case 3 'Verified Caller ID
                            imgEndLocation.ImageUrl = "~/images/timesheet_phone.png"
                            imgEndLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("EndLocation_ID")) & vbCrLf & "Caller ID: " & e.Item.DataItem("EndLocation")
                            imgEndLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("EndLocation_ID")) & "]"
                        Case 4 'Mobile Access
                            imgEndLocation.ImageUrl = "~/images/timesheet_mobile.png"
                            imgEndLocation.AlternateText = "Location: Mobile Access" & vbCrLf & "IP Address: " & e.Item.DataItem("EndLocation")
                            imgEndLocation.ToolTip = "header=[Location] body=[Mobile Access<br>IP Address: " & e.Item.DataItem("EndLocation") & "]"
                    End Select
                Else
                    imgEndLocation.ImageUrl = "~/images/timesheet_phone0.png"
                    imgEndLocation.AlternateText = "Location: Unverified" & vbCrLf & "Caller ID: " & e.Item.DataItem("EndLocation")
                    imgEndLocation.ToolTip = "header=[Location] body=[Unverified<br>Caller ID: " & e.Item.DataItem("EndLocation") & "]"
                End If

                If Time.StripTime(e.Item.DataItem("StartTime")) = Time.StripTime(e.Item.DataItem("EndTime")) Then
                    lblEndTime.Text = Time.QuickStrip(e.Item.DataItem("EndTime"))
                Else
                    lblEndTime.Text = Time.QuickStrip(e.Item.DataItem("EndTime")) & " (" & Time.StripTime(e.Item.DataItem("EndTime")) & ")"
                End If

                If DatePart(DateInterval.Hour, dtEnd) < 12 Then
                    txtEndHour.Text = DatePart(DateInterval.Hour, dtEnd)
                    drpEndAMPM.SelectedValue = "AM"
                ElseIf DatePart(DateInterval.Hour, dtEnd) = 12 Then
                    txtEndHour.Text = "12"
                    drpEndAMPM.SelectedValue = "PM"
                Else
                    txtEndHour.Text = DatePart(DateInterval.Hour, dtEnd) - 12
                    drpEndAMPM.SelectedValue = "PM"
                End If

                txtEndMin.Text = Time.AddZero(DatePart(DateInterval.Minute, dtEnd))
                txtEndDate.Text = dtEnd.Date
            Else
                imgEndLocation.Visible = False
                lblEndTime.Text = ""
                lblDuration.Text = "<em>" & lblDuration.Text & "</em>"

                pnlEditEndTime.Visible = False
                pnlEditNoEndTime.Visible = True
                txtEndDate.Text = txtStartDate.Text
            End If

                If e.Item.DataItem("VerifiedBy") <> 0 Then
                    btnVerify.ImageUrl = "~/images/timesheet_verified.png"
                    btnVerify.AlternateText = "Verified By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("VerifiedBy"), 1)
                    btnVerify.ToolTip = "header=[Verified] body=[By " & UserInfo.NameFromID(e.Item.DataItem("VerifiedBy"), 1) & "]"
                Else
                    If Permissions.TimesheetsValidate(Session("User_ID")) Then
                        btnVerify.Enabled = True
                        btnVerify.AlternateText = "Unverified" & vbCrLf & "Click to verify"
                        btnVerify.ToolTip = "header=[Unverified] body=[Click to verify]"
                        btnVerify.CommandName = "Verify"
                        btnVerify.CommandArgument = e.Item.DataItem("Time_ID")
                    End If
                End If

                If CInt(e.Item.DataItem("Type")) = 4 Then
                    'you cannot edit, or request edits, for absences
                    btnEdit.Enabled = False
                    btnEdit.ImageUrl = "~/images/placeholder.png"
                End If

                If Permissions.TimesheetsEdit(Session("User_ID")) Then
                    btnSave.CommandName = "Save"
                    lblEditReason.Visible = False
                    txtEditReason.Visible = False
                Else
                    btnSave.CommandName = "RequestEdit"
                    btnSave.AlternateText = "Request Edit"
                    btnSave.ToolTip = "header=[Request Edit] body=[]"
                End If

                btnSave.CommandArgument = e.Item.DataItem("Time_ID")
                btnDelete.CommandArgument = e.Item.DataItem("Time_ID")

                If e.Item.DataItem("ManualBy") <> 0 Then
                    imgStartLocation.ImageUrl = "~/images/timesheet_manual.png"
                    imgStartLocation.AlternateText = "Manually Added By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1)
                    imgStartLocation.ToolTip = "header=[Manually Added] body=[By " & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1) & "]"
                    imgEndLocation.ImageUrl = "~/images/timesheet_manual.png"
                    imgEndLocation.AlternateText = "Manually Added By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1)
                    imgEndLocation.ToolTip = "header=[Manually Added] body=[By " & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1) & "]"
                End If

                If e.Item.DataItem("EditedBy") <> 0 Then
                    imgStartLocation.ImageUrl = "~/images/timesheet_edited.png"
                    imgStartLocation.AlternateText = "Edited By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1)
                    imgStartLocation.ToolTip = "header=[Edited] body=[By " & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1) & "]"
                    imgEndLocation.ImageUrl = "~/images/timesheet_edited.png"
                    imgEndLocation.AlternateText = "Edited By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1)
                    imgEndLocation.ToolTip = "header=[Edited] body=[By " & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1) & "]"
                End If
            End If
    End Sub

    Protected Function DurationTotal() As String
        DurationTotal = TimeRecords.FormatMinutes(intDurationTotal, Session("User_ID"), True)
    End Function

    Protected Sub rptTimes_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Trace.Write("Timesheets", "Begin rptTimes_ItemCommand")
        Trace.Write("rptTimes_ItemCommand", "CommandName = " & e.CommandName)
        Trace.Write("rptTimes_ItemCommand", "CommandArgument = " & e.CommandArgument)
        Dim strSQLQuery As String = ""
        Dim strStart As String = ""
        Dim boolEndTime As Boolean
        Dim strEnd As String = ""
        Dim intTime_ID As Int64
        Dim boolRedirToRequests As Boolean = False

        Dim upnlTimeRow As UpdatePanel = e.Item.FindControl("upnlTimeRow")
        Dim pnlTimeRow As Panel = e.Item.FindControl("pnlTimeRow")
        Dim pnlEditEndTime As Panel = e.Item.FindControl("pnlEditEndTime")
        Dim pnlEditNoEndTime As Panel = e.Item.FindControl("pnlEditNoEndTime")
        Dim btnEditClockOut As Button = e.Item.FindControl("btnEditClockOut")
        Dim litOriginalClass As Literal = e.Item.FindControl("litOriginalClass")
        Dim pnlTimeView As Panel = e.Item.FindControl("pnlTimeView")
        Dim pnlTimeEdit As Panel = e.Item.FindControl("pnlTimeEdit")
        Dim imgError As Image = e.Item.FindControl("imgError")
        Dim imgRecordType As Image = e.Item.FindControl("imgRecordType")
        Dim lblDate As Label = e.Item.FindControl("lblDate")
        Dim lblStartTime As Label = e.Item.FindControl("lblStartTime")
        Dim imgStartLocation As Image = e.Item.FindControl("imgStartLocation")
        Dim txtStartHour As TextBox = e.Item.FindControl("txtStartHour")
        Dim txtStartMin As TextBox = e.Item.FindControl("txtStartMin")
        Dim drpStartAMPM As DropDownList = e.Item.FindControl("drpStartAMPM")
        Dim lblEndTime As Label = e.Item.FindControl("lblEndTime")
        Dim imgEndLocation As Image = e.Item.FindControl("imgEndLocation")
        Dim txtEndHour As TextBox = e.Item.FindControl("txtEndHour")
        Dim txtEndMin As TextBox = e.Item.FindControl("txtEndMin")
        Dim drpEndAMPM As DropDownList = e.Item.FindControl("drpEndAMPM")
        Dim txtStartDate As TextBox = e.Item.FindControl("txtStartDate")
        Dim txtEndDate As TextBox = e.Item.FindControl("txtEndDate")
        Dim lblDuration As Label = e.Item.FindControl("lblDuration")
        Dim txtEditReason As TextBox = e.Item.FindControl("txtEditReason")
        Dim btnVerify As ImageButton = e.Item.FindControl("btnVerify")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnSave As ImageButton = e.Item.FindControl("btnSave")
        Dim btnCancel As ImageButton = e.Item.FindControl("btnCancel")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim EditUser_ID As String = ""
        Dim Time_ID1 As String = e.CommandArgument

        Select Case e.CommandName
            Case "Verify"
                intTime_ID = e.CommandArgument
                strSQLQuery = "UPDATE [TimeRecords] SET PossibleError = 0, VerifiedBy = @VerifiedBy WHERE [Time_ID] = @ID"
                imgError.Visible = False
                btnVerify.Enabled = False
                btnVerify.ImageUrl = "~/images/timesheet_verified.png"
                btnVerify.AlternateText = "Verified By" & vbCrLf & UserInfo.NameFromID(Session("User_ID"), 1)
                btnVerify.ToolTip = "header=[Verified] body=[By" & vbCrLf & UserInfo.NameFromID(Session("User_ID"), 1) & "]"
            Case "Edit"
                If pnlTimeRow.CssClass <> "schSaved" Then
                    litOriginalClass.Text = pnlTimeRow.CssClass
                End If
                pnlTimeRow.Height = 64
                pnlTimeRow.CssClass = "schSelected"
                pnlTimeView.Visible = False
                pnlTimeEdit.Visible = True
                btnEdit.Visible = False
                btnSave.Visible = True
                btnCancel.Visible = True
                If Permissions.TimesheetsDelete(Session("User_ID")) Then
                    btnDelete.Visible = True
                Else
                    btnDelete.Visible = False
                End If
            Case "ClockOut"
                pnlEditNoEndTime.Visible = False
                pnlEditEndTime.Visible = True
            Case "Save"
                'Inserting into TimeClockWizard Audit
                Dim Time_ID As String = e.CommandArgument
                Dim sqlQuery As String = ""
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                sqlQuery = "SELECT USER_ID,Client_ID,StartTime,EndTime  FROM [TimeRecords] WHERE [Time_ID] = " + Time_ID
                Dim da As New SqlDataAdapter()
                da.SelectCommand = New SqlCommand(sqlQuery, objConnection)
                Dim dt As New DataTable()
                Dim ds As New DataSet()
                da.Fill(ds)
                Dim Coloumname1 As String = ds.Tables(0).Rows(0)("StartTime").ToString()
                Dim Coloumname2 As String = ds.Tables(0).Rows(0)("EndTime").ToString()
                Dim Coloumname As String = "StartTime" + " =" + Coloumname1 + " " + "EndTime " + " = " + Coloumname2
                Dim Description As String = "This" + " " + Time_ID1 + " Record was modified by" + " " + Session("User_ID")
                Dim USERID As String = ds.Tables(0).Rows(0)("USER_ID").ToString()
                Dim TableName As String = "TimeRecords"
                ViewState("TableName") = TableName
                ViewState("Coloumname") = Coloumname
                ViewState("Description") = Description
                ViewState("EditUser_ID") = USERID
                InsertCLockInAudit()

                intTime_ID = e.CommandArgument
                If VerifyTimes(txtStartHour, txtStartMin, drpStartAMPM, txtStartDate, pnlEditEndTime, txtEndHour, txtEndMin, drpEndAMPM, txtEndDate) Then
                    strStart = txtStartDate.Text & " " & txtStartHour.Text & ":" & txtStartMin.Text & " " & drpStartAMPM.SelectedValue
                    Trace.Write("rptTimes_ItemCommand", "strStart = " & strStart)
                    strEnd = txtEndDate.Text & " " & txtEndHour.Text & ":" & txtEndMin.Text & " " & drpEndAMPM.SelectedValue
                    Trace.Write("rptTimes_ItemCommand", "strEnd = " & strEnd)

                    If pnlEditEndTime.Visible Then
                        boolEndTime = True
                        strSQLQuery = "UPDATE TimeRecords SET [StartTime] = @StartTime, [StartLocation] = '', [EndTime] = @EndTime, [EndLocation_ID] = 0, [EndLocation] = '', PossibleError = 0, VerifiedBy = @VerifiedBy, [EditedBy] = @EditedBy WHERE [Time_ID] = @ID"
                        imgEndLocation.Visible = True
                        imgEndLocation.ImageUrl = "~/images/timesheet_edited.png"
                        imgEndLocation.AlternateText = "Edited By" & vbCrLf & UserInfo.NameFromID(Session("User_ID"), 1)
                        imgEndLocation.ToolTip = "header=[Edited] body=[By" & vbCrLf & UserInfo.NameFromID(Session("User_ID"), 1) & "]"
                        lblEndTime.Text = txtEndHour.Text & ":" & txtEndMin.Text & " " & drpEndAMPM.SelectedValue

                        If txtStartDate.Text <> txtEndDate.Text Then
                            lblEndTime.Text = lblEndTime.Text & " (" & txtEndDate.Text & ")"
                        End If

                        btnVerify.Enabled = False
                        btnVerify.ImageUrl = "~/images/timesheet_verified.png"
                        btnVerify.AlternateText = "Verified By" & vbCrLf & UserInfo.NameFromID(Session("User_ID"), 1)
                        btnVerify.ToolTip = "header=[Verified] body=[By" & vbCrLf & UserInfo.NameFromID(Session("User_ID"), 1) & "]"
                    Else
                        boolEndTime = False
                        strSQLQuery = "UPDATE TimeRecords SET [StartTime] = @StartTime, [StartLocation] = '', [EditedBy] = @EditedBy WHERE [Time_ID] = @ID"
                    End If
                    Dim intDuration As Integer

                    Trace.Write("rptTimes_ItemCommand", "boolEndTime = " & boolEndTime)
                    If boolEndTime Then
                        Trace.Write("rptTimes_ItemCommand", "strStart = " & strStart)
                        intDuration = TimeRecords.TimeDifference(strStart, strEnd)
                    Else
                        intDuration = TimeRecords.TimeDifference(strStart, Clock.GetNow())
                    End If

                    pnlTimeRow.Height = 20
                    pnlTimeRow.CssClass = "schSaved"
                    pnlTimeView.Visible = True
                    pnlTimeEdit.Visible = False
                    imgError.Visible = False
                    btnEdit.Visible = True
                    btnSave.Visible = False
                    btnCancel.Visible = False
                    btnDelete.Visible = False

                    imgStartLocation.ImageUrl = "~/images/timesheet_edited.png"
                    imgStartLocation.AlternateText = "Edited By" & vbCrLf & UserInfo.NameFromID(Session("User_ID"), 1)
                    imgStartLocation.ToolTip = "header=[Edited] body=[By" & vbCrLf & UserInfo.NameFromID(Session("User_ID"), 1) & "]"

                    lblStartTime.Text = txtStartHour.Text & ":" & txtStartMin.Text & " " & drpStartAMPM.SelectedValue

                    lblDuration.Text = TimeRecords.FormatMinutes(intDuration, Session("User_ID"), True)

                    If boolEndTime Then
                        Dim intPayrollExisting As Int64 = PayrollClass.PayrollExists(Session("Client_ID"), strStart, strEnd)

                        If intPayrollExisting <> 0 Then
                            PayrollClass.InvalidatePayroll(intPayrollExisting)
                        End If
                    End If
                End If
            Case "RequestEdit"
                intTime_ID = e.CommandArgument
                If VerifyTimes(txtStartHour, txtStartMin, drpStartAMPM, txtStartDate, pnlEditEndTime, txtEndHour, txtEndMin, drpEndAMPM, txtEndDate) Then
                    strStart = txtStartDate.Text & " " & txtStartHour.Text & ":" & txtStartMin.Text & " " & drpStartAMPM.SelectedValue
                    strEnd = txtEndDate.Text & " " & txtEndHour.Text & ":" & txtEndMin.Text & " " & drpEndAMPM.SelectedValue
                    strSQLQuery = "INSERT INTO TimeRequests ([Client_ID], [User_ID], [Time_ID], [StartTime], [EndTime], [Reason]) VALUES (@Client_ID, @User_ID, @ID, @StartTime, @EndTime, @Reason)"
                    If pnlEditEndTime.Visible Then
                        boolEndTime = True
                    End If
                    boolRedirToRequests = True
                End If
            Case "Cancel"
                strSQLQuery = ""
                pnlTimeRow.Height = 20
                pnlTimeRow.CssClass = litOriginalClass.Text
                pnlTimeView.Visible = True
                pnlTimeEdit.Visible = False
                If imgEndLocation.Visible = False Then
                    pnlEditEndTime.Visible = False
                    pnlEditNoEndTime.Visible = True
                Else
                    pnlEditEndTime.Visible = True
                    pnlEditNoEndTime.Visible = False
                End If
                btnEdit.Visible = True
                btnSave.Visible = False
                btnCancel.Visible = False
                btnDelete.Visible = False
            Case "Delete"

                Dim Time_ID As String = e.CommandArgument
                Dim sqlQuery As String = ""
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection
                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                sqlQuery = "SELECT USER_ID,Client_ID,StartTime,EndTime  FROM [TimeRecords] WHERE [Time_ID] = " + Time_ID
                Dim da As New SqlDataAdapter()
                da.SelectCommand = New SqlCommand(sqlQuery, objConnection)
                Dim dt As New DataTable()
                Dim ds As New DataSet()
                da.Fill(ds)
                Dim Coloumname1 As String = ds.Tables(0).Rows(0)("StartTime").ToString()
                Dim Coloumname2 As String = ds.Tables(0).Rows(0)("EndTime").ToString()
                Dim Coloumname As String = "StartTime" + " =" + Coloumname1 + " " + "EndTime " + " = " + Coloumname2
                Dim Description As String = "This" + " " + Time_ID1 + " " + "Was Deleted" + " by " + " " + Session("User_ID")
                Dim USERID As String = ds.Tables(0).Rows(0)("USER_ID").ToString()
                Dim TableName As String = "TimeRecords"
                ViewState("Table") = TableName
                ViewState("Coloumn") = Coloumname
                ViewState("Des") = Description
                ViewState("EditUS") = USERID

                InsertDeletedRecord()

                intTime_ID = e.CommandArgument
                strSQLQuery = "DELETE FROM [TimeRecords] WHERE [Time_ID] = @ID; DELETE FROM [TimeRequests] WHERE [Time_ID] = @ID"
                pnlTimeRow.Height = 20
                pnlTimeView.Visible = True
                pnlTimeEdit.Visible = False
                pnlTimeRow.BackColor = Drawing.Color.DarkRed
                btnEdit.Visible = False
                btnSave.Visible = False
                btnCancel.Visible = False
                btnDelete.Visible = False
                imgRecordType.ImageUrl = "~/images/delete_small.png"
                imgRecordType.AlternateText = "Deleted"
                imgRecordType.ToolTip = "header=[Deleted] body=[]"

                Dim strDateWithYear As Date
                If IsDate(lblDate.Text) Then
                    strDateWithYear = CDate(lblDate.Text)
                Else
                    strDateWithYear = CDate(lblDate.Text & ", " & Now.AddYears(-1).Year.ToString())
                End If

                Dim intPayrollExisting As Int64 = PayrollClass.PayrollExists(Session("Client_ID"), strDateWithYear, strDateWithYear)

                If intPayrollExisting <> 0 Then
                    PayrollClass.InvalidatePayroll(intPayrollExisting)
                End If
            Case Else
                strSQLQuery = ""
        End Select

        If strSQLQuery <> "" Then
            Trace.Write("rptTimes_ItemCommand", "Begin SQL Query: " & strSQLQuery)
            Trace.Write("rptTimes_ItemCommand", "StartTime: " & strStart)
            Trace.Write("rptTimes_ItemCommand", "EndTime: " & strEnd)
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = strSQLQuery
            objCommand.Parameters.AddWithValue("@StartTime", strStart)
            Trace.Write("rptTimes_ItemCommand", "boolEndTime = " & boolEndTime)
            If boolEndTime Then
                objCommand.Parameters.AddWithValue("@EndTime", strEnd)
            Else
                objCommand.Parameters.AddWithValue("@EndTime", System.DBNull.Value)
            End If
            objCommand.Parameters.AddWithValue("@VerifiedBy", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@EditedBy", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@ID", intTime_ID)
            If txtEditReason.Text <> "" Then
                objCommand.Parameters.AddWithValue("@Reason", txtEditReason.Text)
            Else
                objCommand.Parameters.AddWithValue("@Reason", "<em>No reason specified</em>")
            End If

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
            Trace.Write("rptTimes_ItemCommand", "End SQL Query")

            If boolRedirToRequests Then
                Response.Redirect("TimesheetRequests.aspx?highlight=true")
            End If
        End If
        Trace.Write("Timesheets", "End rptTimes_ItemCommand")
    End Sub
    Public Function InsertCLockInAudit() As Boolean
        Audit.CreateNewClockitinAudit(Session("Client_ID"), ViewState("EditUser_ID"), ViewState("TableName"), ViewState("Coloumname"), ViewState("Description"))
    End Function
    Public Function InsertDeletedRecord() As Boolean
        Audit.CreateNewClockitinAudit(Session("Client_ID"), ViewState("EditUS"), ViewState("Table"), ViewState("Coloumn"), ViewState("Des"))
    End Function
    '   Protected Sub calClockedIn_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '      lblClockedInDate.Text = calClockedIn.SelectedDate
    '       calClockedIn.Visible = False
    '       btnShowHideCalClockedIn.ImageUrl = "~/images/arrowdown_small.png"
    '   End Sub

    '   Protected Sub calClockedOut_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '       lblClockedOutDate.Text = calClockedOut.SelectedDate
    '      calClockedOut.Visible = False
    '    btnShowHideCalClockedOut.ImageUrl = "~/images/arrowdown_small.png"
    ' End Sub

    Protected Sub lblDisplayStartDate_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '       lblDisplayStartDate.Text = calDisplayStartDate.SelectedDate
        '       calDisplayStartDate.Visible = False
        '        btnShowHideCalDispStart.ImageUrl = "~/images/arrowdown_small.png"

        GetTimeRecords()
    End Sub

    Protected Sub lblDisplayEndDate_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '       lblDisplayEndDate.Text = calDisplayEndDate.SelectedDate
        '       calDisplayEndDate.Visible = False
        '       btnShowHideCalDispEnd.ImageUrl = "~/images/arrowdown_small.png"

        GetTimeRecords()
    End Sub

    '    Protected Sub btnShowHideCalClockedIn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '      If calClockedIn.Visible Then
    '       calClockedIn.Visible = False
    '     btnShowHideCalClockedIn.ImageUrl = "~/images/arrowdown_small.png"
    '  Else
    '     calClockedIn.Visible = True
    '     btnShowHideCalClockedIn.ImageUrl = "~/images/arrowup_small.png"
    '  End If
    ' End Sub

    '  Protected Sub btnShowHideCalClockedOut_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '      If calClockedOut.Visible Then
    '        calClockedOut.Visible = False
    '        btnShowHideCalClockedOut.ImageUrl = "~/images/arrowdown_small.png"
    '   Else
    '       calClockedOut.Visible = True
    '        btnShowHideCalClockedOut.ImageUrl = "~/images/arrowup_small.png"
    '    End If
    'End Sub

    '   Protected Sub btnShowHideCalDispStart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '      If calDisplayStartDate.Visible Then
    '         calDisplayStartDate.Visible = False
    '        btnShowHideCalDispStart.ImageUrl = "~/images/arrowdown_small.png"
    '   Else
    '      calDisplayStartDate.Visible = True
    '     btnShowHideCalDispStart.ImageUrl = "~/images/arrowup_small.png"
    'End If
    'End Sub

    '    Protected Sub btnShowHideCalDispEnd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '       If calDisplayEndDate.Visible Then
    '          calDisplayEndDate.Visible = False
    '         btnShowHideCalDispEnd.ImageUrl = "~/images/arrowdown_small.png"
    '   Else
    '      calDisplayEndDate.Visible = True
    '     btnShowHideCalDispEnd.ImageUrl = "~/images/arrowup_small.png"
    '  End If
    ' End Sub

    Protected Sub rblSort_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblSort.SelectedIndexChanged
        GetTimeRecords()
    End Sub

    Protected Sub AllUsersSelection(ByVal sender As Object, ByVal e As CommandEventArgs) Handles culDisplayUsers.AllUsersSelected
        ViewState("DisplayMethod") = 0
        ViewState("DisplayCriteria") = "*"

        GetTimeRecords()
    End Sub

    Protected Sub UserListSelection(ByVal sender As Object, ByVal e As CommandEventArgs) Handles culDisplayUsers.UserChanged
        Dim cblUserList As ListItemCollection = e.CommandArgument

        Dim strUserList As String = ""
        Dim li As ListItem
        Dim sb As New StringBuilder

        For Each li In cblUserList
            If li.Selected = True Then
                sb.Append(li.Value & ", ")
            End If
        Next

        strUserList = sb.ToString

        If strUserList <> "" Then
            strUserList = Left(strUserList, Len(strUserList) - 2)
        End If

        ViewState("DisplayMethod") = 0
        ViewState("DisplayCriteria") = strUserList

        GetTimeRecords()
    End Sub

    Protected Sub OfficeListSelection(ByVal sender As Object, ByVal e As CommandEventArgs) Handles culDisplayUsers.OfficeChanged
        Dim strSelectedOffice As String = e.CommandArgument

        ViewState("DisplayMethod") = 1
        ViewState("DisplayCriteria") = strSelectedOffice

        GetTimeRecords()
    End Sub

    Protected Function VerifyTimes(ByVal txtStartHour As TextBox, ByVal txtStartMin As TextBox, ByVal drpStartAMPM As DropDownList, ByVal txtStartDate As TextBox, ByVal pnlEditEndTime As Panel, ByVal txtEndHour As TextBox, ByVal txtEndMin As TextBox, ByVal drpEndAMPM As DropDownList, ByVal txtEndDate As TextBox) As Boolean
        'Returns True if times verify correctly, False otherwise
        Dim boolDoCompare As Boolean = True

        If Not IsNumeric(txtStartHour.Text) Then
            Trace.Write("VerifyTimes", "Fail 1A")
            txtStartHour.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtStartHour.Text > 12 Then
            Trace.Write("VerifyTimes", "Fail 1B")
            txtStartHour.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtStartHour.Text < 0 Then
            Trace.Write("VerifyTimes", "Fail 1C")
            txtStartHour.CssClass = "textBoxError"
            boolDoCompare = False
        Else
            Trace.Write("VerifyTimes", "Pass 1")
            txtStartHour.BorderColor = Drawing.Color.Empty
            txtStartHour.BackColor = Drawing.Color.Empty
        End If

        If Not IsNumeric(txtStartMin.Text) Then
            Trace.Write("VerifyTimes", "Fail 2A")
            txtStartMin.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtStartMin.Text > 59 Then
            Trace.Write("VerifyTimes", "Fail 2B")
            txtStartMin.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtStartMin.Text < 0 Then
            Trace.Write("VerifyTimes", "Fail 2C")
            txtStartMin.CssClass = "textBoxError"
            boolDoCompare = False
        Else
            Trace.Write("VerifyTimes", "Pass 2")
            txtStartMin.BorderColor = Drawing.Color.Empty
            txtStartMin.BackColor = Drawing.Color.Empty
        End If

        If pnlEditEndTime.Visible Then
            If Not IsNumeric(txtEndHour.Text) Then
                Trace.Write("VerifyTimes", "Fail 3A")
                txtEndHour.CssClass = "textBoxError"
                boolDoCompare = False
            ElseIf txtEndHour.Text > 12 Then
                Trace.Write("VerifyTimes", "Fail 3B")
                txtEndHour.CssClass = "textBoxError"
                boolDoCompare = False
            ElseIf txtEndHour.Text < 0 Then
                Trace.Write("VerifyTimes", "Fail 3C")
                txtEndHour.CssClass = "textBoxError"
                boolDoCompare = False
            Else
                Trace.Write("VerifyTimes", "Pass 3")
                txtEndHour.BorderColor = Drawing.Color.Empty
                txtEndHour.BackColor = Drawing.Color.Empty
            End If

            If Not IsNumeric(txtEndMin.Text) Then
                Trace.Write("VerifyTimes", "Fail 4A")
                txtEndMin.CssClass = "textBoxError"
                boolDoCompare = False
            ElseIf txtEndMin.Text > 59 Then
                Trace.Write("VerifyTimes", "Fail 4B")
                txtEndMin.CssClass = "textBoxError"
                boolDoCompare = False
            ElseIf txtEndMin.Text < 0 Then
                Trace.Write("VerifyTimes", "Fail 4C")
                txtEndMin.CssClass = "textBoxError"
                boolDoCompare = False
            Else
                Trace.Write("VerifyTimes", "Pass 4")
                txtEndMin.BorderColor = Drawing.Color.Empty
                txtEndMin.BackColor = Drawing.Color.Empty
            End If

            If boolDoCompare Then
                Dim dtStart As DateTime = txtStartDate.Text & " " & txtStartHour.Text & ":" & txtStartMin.Text & " " & drpStartAMPM.SelectedValue
                Dim dtEnd As DateTime = txtEndDate.Text & " " & txtEndHour.Text & ":" & txtEndMin.Text & " " & drpEndAMPM.SelectedValue

                If DateDiff(DateInterval.Minute, dtStart, dtEnd) > 0 Then
                    txtStartHour.BorderColor = Drawing.Color.Empty
                    txtStartHour.BackColor = Drawing.Color.Empty
                    txtStartMin.BorderColor = Drawing.Color.Empty
                    txtStartMin.BackColor = Drawing.Color.Empty
                    drpStartAMPM.BorderColor = Drawing.Color.Empty
                    drpStartAMPM.BackColor = Drawing.Color.Empty
                    txtStartDate.BorderColor = Drawing.Color.Empty
                    txtStartDate.BackColor = Drawing.Color.Empty
                    txtEndHour.BorderColor = Drawing.Color.Empty
                    txtEndHour.BackColor = Drawing.Color.Empty
                    txtEndMin.BorderColor = Drawing.Color.Empty
                    txtEndMin.BackColor = Drawing.Color.Empty
                    drpEndAMPM.BorderColor = Drawing.Color.Empty
                    drpEndAMPM.BackColor = Drawing.Color.Empty
                    txtEndDate.BorderColor = Drawing.Color.Empty
                    txtEndDate.BackColor = Drawing.Color.Empty
                    VerifyTimes = True
                Else
                    VerifyTimes = False
                    txtEndHour.CssClass = "textBoxError"
                    txtEndMin.CssClass = "textBoxError"
                    drpEndAMPM.CssClass = "textBoxError"
                    txtEndDate.CssClass = "textBoxError"
                End If
            Else
                VerifyTimes = False
            End If
        Else
            If boolDoCompare Then
                VerifyTimes = True
            Else
                VerifyTimes = False
            End If
        End If
    End Function

    Protected Sub btnVerifyAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim strSQLQuery As String

        If Request.QueryString("view") = "errors" Then
            strSQLQuery = "UPDATE [TimeRecords] SET [VerifiedBy] = @User_ID, PossibleError = 0 WHERE ([Client_ID] = @Client_ID) AND ([VerifiedBy] = 0 OR [PossibleError] = 1)"
        Else
            strSQLQuery = "UPDATE [TimeRecords] SET [VerifiedBy] = @User_ID, PossibleError = 0 WHERE ([Client_ID] = @Client_ID) AND ([StartTime] BETWEEN @StartTime AND @EndTime) AND ([VerifiedBy] = 0 OR [PossibleError] = 1)"
        End If

        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@StartTime", lblDisplayStartDate.Text & " 12:00 AM")
        objCommand.Parameters.AddWithValue("@EndTime", lblDisplayEndDate.Text & " 11:59 PM")
        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        GetTimeRecords()
    End Sub

    Protected Sub ddlChooseUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChooseUser.SelectedIndexChanged
        chkRemainClockedIn.Checked = False
        pnlClockOut.Visible = True
        lblAlreadyClockedIn.Visible = False
        btnAddRecord.Enabled = True
    End Sub

    Protected Sub chkRemainClockedIn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRemainClockedIn.CheckedChanged
        If chkRemainClockedIn.Checked Then
            If rblType.SelectedValue = 1 Then
                'trying to add regular record
                lblAlreadyClockedIn.Text = "<br />This user is already clocked in"
                If UserInfo.UserStatus(ddlChooseUser.SelectedValue) = 1 Then
                    pnlClockOut.Visible = False
                Else
                    lblAlreadyClockedIn.Visible = True
                    btnAddRecord.Enabled = False
                End If
            Else
                'trying to add a break
                lblAlreadyClockedIn.Text = "<br />This user is not clocked in so the break must have an end time."
                If UserInfo.UserStatus(ddlChooseUser.SelectedValue) = 1 Then
                    Trace.Write("Timesheets.aspx", "Trying to add break with no end time when they are not clocked in")
                    lblAlreadyClockedIn.Visible = True
                    btnAddRecord.Enabled = False
                Else
                    pnlClockOut.Visible = False
                End If
            End If
        Else
            pnlClockOut.Visible = True
            lblAlreadyClockedIn.Visible = False
            btnAddRecord.Enabled = True
        End If
    End Sub

    Protected Sub timerLogout_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timerLogout.Tick
        Response.Redirect(Constants.InactiveLogoutURL)
    End Sub

    Protected Sub rblType_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        If rblType.SelectedValue = 1 Then
            litInOut1.Text = "Clocked In"
            litInOut2.Text = "Clocked Out"
            chkRemainClockedIn.Text = "Remain clocked in"
        Else
            litInOut1.Text = "Start Break"
            litInOut2.Text = "End Break"
            chkRemainClockedIn.Text = "Remain on break"
        End If
    End Sub
End Class
