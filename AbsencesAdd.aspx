<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AbsencesAdd.aspx.vb" Inherits="AddAbsences" title="TimeClockWizard - Absences" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <div class="main-content" style="width:1050px;">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="float: left; margin-left: 25px;">
                 
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div style="width: 300px; text-align: center; background: #004a71; padding: 15px 0; margin-bottom: 20px;">
                                <asp:Panel ID="pnlRequestAbsence" runat="server">
                                    <h4 style="padding: 12px 0;">CHOOSE USER</h4>
                                    
                                    <asp:DropDownList ID="ddlChooseUser" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChooseUser_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                               
												
												
                                 </asp:Panel>
                                <div class="clear"></div>
                                </div>

                                <div style="width: 300px;">
                                 <asp:Panel ID="pnlAddAbsence" runat="server">
                                    
										<h4>ADD ABSENCE FOR <asp:Literal ID="litFirstName" runat="server"></asp:Literal></h4>
                                                 <div style="border: 1px solid #c6ced0; padding: 30px 0 0 30px;">


														<div style="margin-bottom: 5px;">
															<label style="margin-right: 44px;">type:</label>
																                                         
                                           
                                                <asp:DropDownList ID="drpRequestType" runat="server">
                                                    <asp:ListItem Value="1">Vacation</asp:ListItem>
                                                    <asp:ListItem Value="2">Holiday</asp:ListItem>
                                                    <asp:ListItem Value="3">Sick Day</asp:ListItem>
                                                    <asp:ListItem Value="4">Personal</asp:ListItem>
                                                    <asp:ListItem Value="5">Paid Leave</asp:ListItem>
                                                    <asp:ListItem Value="6">Other</asp:ListItem>
                                                </asp:DropDownList>
                                            
															<div class="clear"></div>
														</div>



                                            <div style="margin-bottom: 5px;">
															<label style="margin-right: 35px;">Hours:</label>

                                         
                                                <asp:TextBox ID="txtHours" width="30px" runat="server">0</asp:TextBox>&nbsp;<asp:Image ID="imgHelpHours" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" ToolTip = "header=[Hours To Deduct] body=[The number of hours that should be deducted from the user's allotted time for the absence type.]" />
                                           
														<div class="clear"></div>
														</div>

                                            <div style="margin-bottom: 5px;">
															<label style="margin-right: 5px;">Start Date:</label>

                                                <asp:Textbox ID="lblDateStart" runat="server" Width="70px"></asp:Textbox>
                                                <br />
                                                <div class="calendar">
                                                    <asp:Calendar ID="calAbsenceStart" runat="server" BackColor="White" BorderColor="#c6ced0" Font-Names="Verdana" ForeColor="Black" Visible="false">
                                                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                                        <SelectorStyle BackColor="#CCCCCC" />
                                                        <WeekendDayStyle BackColor="#FFFFCC" />
                                                        <TodayDayStyle BackColor="White" ForeColor="Black" />
                                                        <OtherMonthDayStyle ForeColor="#c6ced0" />
                                                        <NextPrevStyle VerticalAlign="Bottom" />
                                                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" />
                                                        <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                                                    </asp:Calendar>  
                                                </div>    
                                            </div>

                                            <div style="margin-bottom: 5px;">
															<label style="margin-right: 11px;">End Date:</label>

                                            
                                                <asp:Textbox ID="lblDateEnd" runat="server" Width="70px"></asp:Textbox>
                                          <br />
                                                <div class="calendar">
                                                    <asp:Calendar ID="calAbsenceEnd" runat="server" BackColor="White" BorderColor="#c6ced0" Font-Names="Verdana" ForeColor="Black" Visible="false">
                                                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                                        <SelectorStyle BackColor="#CCCCCC" />
                                                        <WeekendDayStyle BackColor="#FFFFCC" />
                                                        <TodayDayStyle BackColor="White" ForeColor="Black" />
                                                        <OtherMonthDayStyle ForeColor="#c6ced0" />
                                                        <NextPrevStyle VerticalAlign="Bottom" />
                                                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" />
                                                        <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                                                    </asp:Calendar>
                                                </div> 
                                              </div> 


                                            <div>
                                                <label style="float: left; margin-right: 53px;">Note:</label>
                                                <asp:TextBox ID="txtNote" Width="150" Height="100" runat="server" TextMode="MultiLine"></asp:TextBox>
                                           
                                            <div class="clear"></div>
                                             </div>


                                            <div style="margin: 12px 0 20px 84px;" >

                                                <asp:Button ID="btnAddRequest" runat="server" Text="Add Absence" OnClick="btnAddRequest_Click" CssClass="add-absence-btn" />

                                                <asp:Label ID="lblRequestError" runat="server" Visible="false" Text=""></asp:Label>
                                            </div>

                                        <asp:UpdateProgress ID="prgClockInOut" runat="server">
                                            <ProgressTemplate>
                                                Processing...
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>



                                     </div> <!-- 1strow-bottom-holder -->
 
                                     
                       
                                </asp:Panel>
                                </div>
                               
												
											
										


                            </ContentTemplate>
                        </asp:UpdatePanel>

                                    </div> <!-- end 1strow -->
                                         

 <div style="margin-left: 350px; width: 630px;">
                        <asp:Repeater ID="rptAbsences" OnItemDataBound="rptAbsences_ItemDataBound" OnItemCommand="rptAbsences_ItemCommand" EnableViewState="True" runat="server">
                            <HeaderTemplate>
                                <ul class="absence-type">
<li class="column-head column-title">ABSENCE TYPE</li></ul>

<ul class="date-start">
<li class="column-head column-title">START DATE</li></ul>

<ul class="date-end">
<li class="column-head column-title">END DATE</li></ul>

<ul class="hours-used" style="width: 115px;">
<li class="column-head column-title">HOURS USED</li></ul>

<ul class="approved">
<li class="column-head column-title">APPROVED</li></ul>

<ul class="editlinks" style="width: 50px;">
 <li class="column-head column-title">&nbsp;</li> </ul>

                            </HeaderTemplate>
                            <ItemTemplate>
<ul class="absence-type">
<li><asp:Image ID="imgNote" ImageAlign="AbsMiddle" runat="server" /><asp:Label ID="lblType" runat="server" Text=""></asp:Label></li></ul>
                                        
<ul class="date-start">
<li> <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label></li></ul>

<ul class="date-end">
<li><asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label></li></ul>
                                       
<ul class="hours-used" style="width: 115px;">
<li><asp:Label ID="lblHours" runat="server" Text=""></asp:Label></li></ul> 
                                
<ul class="approved">
<li><asp:Image ID="imgApproved" Visible="false" runat="server" /></li></ul>   
                                
<ul class="editlinks" style="width: 50px;">
<li><asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete_small.png" AlternateText="Delete this absence" /></li> </ul>                                                                                           
                                        
                                
         
                            </ItemTemplate>

                            <FooterTemplate>
                                
                            </FooterTemplate>
                        </asp:Repeater>                                       
</div>

                        <div class="greyBox" style="visibility:hidden;">
                            <span class="subtitle"><asp:Literal ID="litFirstName2" runat="server"></asp:Literal>'s Available Absence Time</span>
                            <br /><br />    
                            <table cellpadding="5" cellspacing="0" style="text-align:center">
                                <tr><td align="right" valign="top"><b>Vacation:</b></td><td align="left" valign="top"><asp:Label ID="lblVacation" Text="0 Hours" runat="server"></asp:Label></td></tr>
                                <tr><td align="right" valign="top"><b>Holiday:</b></td><td align="left" valign="top"><asp:Label ID="lblHoliday" Text="0 Hours" runat="server"></asp:Label></td></tr>
                                <tr><td align="right" valign="top"><b>Sick Day:</b></td><td align="left" valign="top"><asp:Label ID="lblSickDay"  Text="0 Hours" runat="server"></asp:Label></td></tr>
                                <tr><td align="right" valign="top"><b>Personal:</b></td><td align="left" valign="top"><asp:Label ID="lblPersonal" Text="0 Hours" runat="server"></asp:Label></td></tr>
                                <tr><td align="right" valign="top"><b>Paid Leave:</b></td><td align="left" valign="top"><asp:Label ID="lblPaidLeave" Text="0 Hours" runat="server"></asp:Label></td></tr>
                                <tr><td align="right" valign="top"><b>Other:</b></td><td align="left" valign="top"><asp:Label ID="lblOther"  Text="0 Hours" runat="server"></asp:Label></td></tr>
                            </table>
                        </div>
                    </td>
                    <td valign="top">
                        <asp:Panel ID="pnlWarning" runat="server" Visible="False">
                            <asp:Literal ID="litAbsenceIDHolder" Text="" Visible="False" runat="server"></asp:Literal>
                            <table width="100%" cellspacing="10" class="redbox">
                                <tr>
                                    <td valign="top">
                                     <asp:Image ID="imgLargeHelp_CG" ImageUrl="~/images/warning.png" ImageAlign="top" runat="server" />
                                    </td>
                                    <td>
                                        <span class="subtitle">Deleting an Approved Absence</span>
                                        <br />
                                        <span class="helpText">Deleting this approved absence will invalidate a payroll.  Are you sure you want to continue?</span>
                                        <br /><br />
                                        <asp:Button ID="btnConfirmDelete" runat="server" Text="Delete Absence" OnClick="btnConfirmDelete_Click" />
                                        &nbsp;&nbsp;
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>


                   
        </ContentTemplate>
    </asp:UpdatePanel>
        </div>
</asp:Content>