
Partial Class SettingsSubscriptionDelinquent
    Inherits System.Web.UI.Page

    Dim dblDelinquentTotal As Double
    Dim intDelinquentDays As Integer
    Dim dtOriginalBillingDate As DateTime
    Dim dtNewBillingDate As DateTime

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabSettings As Panel = Master.FindControl("pnlTabSettings")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Subscription As Panel = Master.FindControl("pnlTab2MySubscription")

        pnlMasterTabSettings.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Settings
        pnlMasterTab2Subscription.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            If Chase.Delinquency(Session("Client_ID")) = 8 Then
                Response.Redirect("SettingsSubscription.aspx")
            End If

            If Clock.GetNow().Month < 10 Then
                drpExpMonth.SelectedValue = "0" & Clock.GetNow().Month
            Else
                drpExpMonth.SelectedValue = Clock.GetNow().Month
            End If

            drpExpYear.SelectedValue = Right(Clock.GetNow().Year, 2)

            GetBillingInfo()
        End If
    End Sub

    Protected Sub GetBillingInfo()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM CCTransactions WHERE Client_ID = @Client_ID AND Success = 0"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        dblDelinquentTotal = objDR("Amount")
        dtOriginalBillingDate = objDR("Submitted")
        dtNewBillingDate = DateAdd(DateInterval.Month, 1, objDR("Submitted"))
        intDelinquentDays = DateDiff(DateInterval.Day, dtOriginalBillingDate, Now())

        lblDelinquentDays.Text = "Your account has been delinquent for " & intDelinquentDays & " days"
        lblDelinquentTotal.Text = FormatCurrency(dblDelinquentTotal, 2)

        If intDelinquentDays > 6 Then
            lblDelinquentWarning.Text = "<br />Your account will be re-enabled once you update your credit card information"
            lblDelinquentDays.Text &= ", and has been disabled"
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim strChaseResponse As String

        If Len(txtCardName.Text) < 1 Then
            strChaseResponse = "ErrorCN,Please enter the name on the card"
        ElseIf Not IsNumeric(txtCardNumber.Text) Then
            strChaseResponse = "ErrorCC,Invalid Card Number"
        ElseIf Len(txtCardVerify.Text) < 2 Or Len(txtCardVerify.Text) > 5 Or Not IsNumeric(txtCardVerify.Text) Then
            strChaseResponse = "ErrorCVV,Invalid Card Verification Code"
        Else
            strChaseResponse = Chase.UpdateProfile(Session("Client_ID"), txtCardNumber.Text, drpExpMonth.SelectedValue & drpExpYear.SelectedValue, txtCardName.Text)
        End If

        Dim arrayResponse As Array = Split(strChaseResponse, ",")

        Select Case arrayResponse(0)
            Case "Approved"
                Chase.UpdateBillingInfo(Session("Client_ID"), drpCardType.SelectedValue, Right(txtCardNumber.Text, 4), drpExpMonth.SelectedValue & "/1/" & drpExpYear.SelectedValue & " 12:00 AM")
                SubmitToChase()
            Case "Declined"
                lblResults.ForeColor = Drawing.Color.Red
                lblResults.Text = "Declined, please try again.<br />Please call TimeClockWizard Support if you keep receiving this error."
            Case Else
                lblResults.ForeColor = Drawing.Color.Red
                lblResults.Text = arrayResponse(1)
        End Select
    End Sub

    Protected Sub SubmitToChase()
        GetBillingInfo()
        Dim strChaseResponse As String = Chase.NewTransaction(Session("Client_ID"), dblDelinquentTotal)
        Dim arrayResponse As Array = Split(strChaseResponse, ",")

        Select Case arrayResponse(0)
            Case "Approved"
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "EXEC sprocClearDelinquent @Client_ID, @Date, @Chase_ID, @NewBillingDate"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                Trace.Write("SettingsSubscriptionDelinquent.aspx", "SQL Client_ID = " & Session("Client_ID"))
                objCommand.Parameters.AddWithValue("@Date", Now.Date)
                Trace.Write("SettingsSubscriptionDelinquent.aspx", "SQL Date = " & Now.Date)
                objCommand.Parameters.AddWithValue("@Chase_ID", arrayResponse(1))
                Trace.Write("SettingsSubscriptionDelinquent.aspx", "SQL Chase_ID = " & arrayResponse(1))
                objCommand.Parameters.AddWithValue("@NewBillingDate", dtNewBillingDate)
                Trace.Write("SettingsSubscriptionDelinquent.aspx", "SQL NewBillingDate = " & dtNewBillingDate)

                objCommand.ExecuteNonQuery()
                pnlCreditCardInfo.Visible = False

                lblDelinquentTotal.Text = "$0.00"
                lblDelinquentDays.Visible = False
                lblDelinquentWarning.Visible = False

                pnlCreditCardApproved.Visible = True
            Case "Declined"
                lblResults.ForeColor = Drawing.Color.Red
                lblResults.Text = "Declined, please try again.<br />Please call TimeClockWizard Support if you keep receiving this error."
            Case Else
                lblResults.ForeColor = Drawing.Color.Red
                lblResults.Text = arrayResponse(1)
        End Select
    End Sub
End Class
