<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UsersDeleted.aspx.vb" Inherits="UsersDeleted" title="TimeClockWizard - Inactivated Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="margin-left: 25px;">
            <asp:Panel ID="pnlUsersFilter" runat="server" DefaultButton="ibSoryUsersBy">
                Search for Users <asp:TextBox ID="txtUsersFilter" runat="server"></asp:TextBox>
                &nbsp;Sort Users By:
                <asp:DropDownList ID="ddlSortUsersBy" runat="server">
                    <asp:ListItem Value="1">First Name</asp:ListItem>
                    <asp:ListItem Value="2" Selected="True">Last Name</asp:ListItem>
                    <asp:ListItem Value="3">Level</asp:ListItem> 
                    <asp:ListItem Value="4">Office</asp:ListItem>
                    <asp:ListItem Value="5">Inactivated Date</asp:ListItem>
                    <asp:ListItem Value="6">Assigned Manager</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlSortUsersOderBy" runat="server">
                    <asp:ListItem Value="1" Selected="True">Ascending</asp:ListItem>
                    <asp:ListItem Value="2">Decending</asp:ListItem>
                </asp:DropDownList>
                <asp:ImageButton ID="ibSoryUsersBy" runat="server" ImageUrl="~/images/mileage_add.png" ImageAlign="AbsMiddle" OnClick="ibSoryUsersBy_Click" />
            </asp:Panel>
                </div>
            <br />
            <asp:Panel ID="pnlChangeUsername" Visible="false" runat="server">
                <asp:Literal ID="litChangeUserID" Visible="false" runat="server"></asp:Literal>
                <div class="yellowBox">
                <span class="subtitle">Change Username</span>
                <br />
                <span style="margin-bottom:100px;"><asp:Literal ID="litChangeUser" runat="server"></asp:Literal>'s username is already being used, please choose another one.</span>
                <br />
                <asp:TextBox ID="txtChangeUser" runat="server"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton ID="btnChangeUser" ImageAlign="AbsMiddle" ImageUrl="~/images/users_activate.png" AlternateText="Change username and reactivate user" ToolTip="header=[Change Username] body=[Change username and recover the user] " runat="server" OnClick="btnChangeUser_Click" />
                <asp:Label ID="lblChangeError" Visible="false" ForeColor="Red" runat="server" Text="Label"><br /><br />This username is already taken, please try another.</asp:Label>
                </div>
                <br />
            </asp:Panel>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ForeColor="green" Text="User successfully recovered.<br><br>"></asp:Label>

            <div style="margin-left: 25px;">
            <asp:Repeater ID="rptUsersList" runat="server" OnItemDataBound="rptUsersList_ItemDataBound" OnItemCommand="rptUserList_ItemCommand" EnableViewState="True">
            <HeaderTemplate>
              <ul class="row-user" style="width: 150px;">
										<li class="column-head column-title">Name</li>
              </ul>
              <ul class="assigned-manager" style="width: 130px;">
										<li class="column-head column-title">Assigned</li>
              </ul>

               <ul class="offices" style="width: 130px;">
										<li class="column-head column-title">Office</li>
              </ul>

               <ul class="level" style="width: 130px;">
										<li class="column-head column-title">Level</li>
              </ul>

               <ul class="inactive" style="width: 210px;">
										<li class="column-head column-title">Inactivated On</li>
              </ul>
          
               <ul class="active" style="width: 150px;">
										<li class="column-head column-title">Activate User&nbsp;<asp:Image ID="imgHelpRecoverUser" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" ToolTip = "header=[Activate User] body=[Reactivate this user so they can login, clock in and out, be scheduled, etc...]" /></li>
              </ul>
              
          
            </HeaderTemplate>
            <ItemTemplate>
              <ul class="row-user" style="width: 150px;">
										<li><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></li>
              </ul>
                          
              <ul class="assigned-manager" style="width: 130px;">
										<li><asp:Label ID="lblAssignedManager" runat="server" Text="Label"></asp:Label></li>
              </ul>
                          
               <ul class="offices" style="width: 130px;">
										<li><asp:Label ID="lblOffice" runat="server" Text="Label"></asp:Label></li>
              </ul>
                          
               <ul class="level" style="width: 130px;">
										<li><asp:Label ID="lblLevel" runat="server" Text="Label"></asp:Label></li>
              </ul>
                          
               <ul class="inactive" style="width: 210px;">
										<li><asp:Label ID="lblDeletedOn" runat="server" Text="Label"></asp:Label></li>
              </ul>

               <ul class="active" style="width: 150px;">
										<li><asp:ImageButton ID="btnRecover" ImageUrl="~/images/users_activate.png" runat="server" ImageAlign="AbsMiddle" /></li>
              </ul>
                          
            </ItemTemplate>

            <FooterTemplate>
                
            </FooterTemplate>
            </asp:Repeater>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

