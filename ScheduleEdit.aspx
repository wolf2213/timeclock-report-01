<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ScheduleEdit.aspx.vb" Inherits="ScheduleEdit" title="TimeClockWizard - Edit Schedule" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script language="javascript" type="text/javascript">
        function disableCostGuardUpdateButton() {
           // document.getElementById('<%'=btnCostGuardUpdate.ClientID%>').disabled = true; 
           // document.getElementById('<%'=btnCostGuardUpdate.ClientID%>').value = 'Updating...';
            document.getElementById('<%=btnCostGuardUpdateTop.ClientID%>').disabled = true;
            document.getElementById('<%=btnCostGuardUpdateTop.ClientID%>').value = 'Updating...';
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">

    </asp:ScriptManager>
    <asp:Literal ID="litAlternate" runat="server" Text="Normal" Visible="false"></asp:Literal>

        <div style="font-family: arial; font-size: 14px; margin-left: 25px;">
    <asp:UpdatePanel ID="upnlSettings" runat="server">
        <ContentTemplate>

									<ul class="week-copy">
										<li>

                                           
<asp:TextBox ID="weekpicker" Text="" runat="server" ClientIDMode="Static" Width="10px" OnTextChanged="weekpicker_SelectionChanged" AutoPostBack="True" CssClass="search1" ForeColor="White" >
          </asp:TextBox>
<asp:Image ID="imageWeekPicker" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" />

            <asp:Label ID="lblWeek" runat="server" Text="Week"></asp:Label> 

            <asp:Label ID="lblStartDate" runat="server" Text="Date"></asp:Label>
            -
            <asp:Label ID="lblEndDate" runat="server" Text="Date"></asp:Label>
                                        


                                        </li>

           <li> <asp:ImageButton ID="btnCopySchedule" runat="server" ImageUrl="~/images/copy-sched-btn.png" ImageAlign="AbsMiddle" Style="padding-bottom:2px;" AlternateText="Copy this schedule to another week" CssClass="copy-sched-btn" />
            </li>
                                   
<li>
            <asp:ImageButton ID="btnScheduleCopy" runat="server" ImageUrl="~/images/smallcopy-sched-btn.png" ImageAlign="AbsMiddle" Style="padding-bottom:2px;" AlternateText="Copy Schedule" Visible="false" CssClass="smallcopy-sched-btn" />


            <asp:Label ID="lblCopyWarning" runat="server" ForeColor="Red" Text="This will replace any existing schedule for the destination week" Visible="false"></asp:Label>

            <asp:Label ID="lblCopySuccess" runat="server" ForeColor="Green" Text="Copied successfully" Visible="false"></asp:Label><br />
    </li>
                                    </ul>




            <div class="calendar">
                <asp:Calendar ID="calScheduleDisp" runat="server" BackColor="White" BorderColor="#c6ced0"
                                Font-Names="Verdana" ForeColor="Black" Visible="false" SelectionMode="DayWeek">
                    <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                    <SelectorStyle BackColor="#CCCCCC" />
                    <WeekendDayStyle BackColor="#FFFFCC" />
                    <TodayDayStyle BackColor="White" ForeColor="Black" />
                    <OtherMonthDayStyle ForeColor="#c6ced0" />
                    <NextPrevStyle VerticalAlign="Bottom" />
                    <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" />
                    <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                </asp:Calendar>
            </div>
            <asp:Timer ID="timerLogout" runat="server">
            </asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
 
    <asp:UpdatePanel ID="upnlCostGuardTop" runat="server" UpdateMode="Always" Visible="false">
        <ContentTemplate>
            <asp:Panel ID="pnlCostGuardTop" runat="server">
                <ul class="costguard">
                    <li>
                                      <asp:Button ID="btnCostGuardUpdateTop" runat="server" Text="Update" OnClientClick="disableCostGuardUpdateButton()" UseSubmitBehavior="false" CssClass="update-btn" />&nbsp;&nbsp;
                CostGuard Total Payroll This Week&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:Label ID="lblCostGuardTotalTop" runat="server" Text="Label"></asp:Label>
  
                    </li>
                </ul>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    <div class="clear"></div>
    <div style="margin-left: 25px;">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <div>
                    
                        <ul class="row-user">
                        <li class="column-head column-title">Name</li>
                        </ul>
                    <ul class="date-n-time">
                        <li class="column-head cal-date"><p>Sunday<br />
                        <asp:Label ID="lblSunDate" runat="server" Text="SunDate"></asp:Label></p></li>
                    </ul>
                        
                    <ul class="date-n-time">
						<li class="column-head cal-date"><p>Monday<br />
                        <asp:Label ID="lblMonDate" runat="server" Text="MonDate"></asp:Label></p></li>
                    </ul>

                    <ul class="date-n-time">
						<li class="column-head cal-date"><p>Tuesday<br />
                        <asp:Label ID="lblTueDate" runat="server" Text="TueDate"></asp:Label></p></li>
                    </ul>

                    <ul class="date-n-time">
						<li class="column-head cal-date"><p>Wednesday<br />
                        <asp:Label ID="lblWedDate" runat="server" Text="WedDate"></asp:Label></p></li>
                    </ul>

                   <ul class="date-n-time">
						<li class="column-head cal-date"><p>Thursday<br />
                        <asp:Label ID="lblThuDate" runat="server" Text="ThuDate"></asp:Label></p></li>
                   </ul>

                    <ul class="date-n-time">
						<li class="column-head cal-date"><p>Friday<br />
                        <asp:Label ID="lblFriDate" runat="server" Text="FriDate"></asp:Label></p></li>
                   </ul>

                    <ul class="date-n-time">
						<li class="column-head cal-date"><p>Saturday<br />
                        <asp:Label ID="lblSatDate" runat="server" Text="SatDate"></asp:Label></p></li>
                   </ul>

                    <ul class="row-edit">
												<li class="column-head"></li>
                    </ul>

                </div>
                <div class="schClear"></div>
            <asp:Repeater ID="rptSchedules" runat="server">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlScheduleRow" runat="server">
                                <asp:Literal ID="litOriginalClass" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litScheduleVisible" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litCurrencyType" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litWage" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litSunCode" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litSunCodeOrig" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litSunStart" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litSunEnd" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litMonCode" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litMonCodeOrig" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litMonStart" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litMonEnd" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litTueCode" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litTueCodeOrig" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litTueStart" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litTueEnd" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litWedCode" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litWedCodeOrig" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litWedStart" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litWedEnd" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litThuCode" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litThuCodeOrig" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litThuStart" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litThuEnd" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litFriCode" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litFriCodeOrig" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litFriStart" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litFriEnd" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litSatCode" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litSatCodeOrig" runat="server" Text="NW" Visible="false"></asp:Literal>
                                <asp:Literal ID="litSatStart" runat="server" Visible="false"></asp:Literal>
                                <asp:Literal ID="litSatEnd" runat="server" Visible="false"></asp:Literal>
                                <div>
                                    <ul class="row-user" style="height:200px;">
                                        <li class="text-bold">
                                    <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                                        </li>
                                        <li>
                                    <asp:Label ID="lblWeekTotalHours" runat="server" Text="TotalHours" ForeColor="Gray"></asp:Label>
                                    <asp:Label ID="lblWeekTotalPay" runat="server" Text="TotalPay" ForeColor="Gray"></asp:Label>
                                        </li>
                                    </ul>
                                </div>
                                <asp:Panel ID="pnlScheduleDays" runat="server">
                                    <div class="schSun">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Label ID="lblSunStart" runat="server" Text="SunStart"></asp:Label><asp:Label ID="lblSunDash" runat="server" Text="-"></asp:Label><asp:Label ID="lblSunEnd" runat="server" Text="SunEnd"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblSunOffice" runat="server" Text="SunOffice"></asp:Label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schMon">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Label ID="lblMonStart" runat="server" Text="MonStart"></asp:Label><asp:Label ID="lblMonDash" runat="server" Text="-"></asp:Label><asp:Label ID="lblMonEnd" runat="server" Text="MonEnd"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblMonOffice" runat="server" Text="MonOffice"></asp:Label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schTue">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Label ID="lblTueStart" runat="server" Text="TueStart"></asp:Label><asp:Label ID="lblTueDash" runat="server" Text="-"></asp:Label><asp:Label ID="lblTueEnd" runat="server" Text="TueEnd"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblTueOffice" runat="server" Text="TueOffice"></asp:Label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schWed">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Label ID="lblWedStart" runat="server" Text="WedStart"></asp:Label><asp:Label ID="lblWedDash" runat="server" Text="-"></asp:Label><asp:Label ID="lblWedEnd" runat="server" Text="WedEnd"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblWedOffice" runat="server" Text="WedOffice"></asp:Label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schThu">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Label ID="lblThuStart" runat="server" Text="ThuStart"></asp:Label><asp:Label ID="lblThuDash" runat="server" Text="-"></asp:Label><asp:Label ID="lblThuEnd" runat="server" Text="ThuEnd"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblThuOffice" runat="server" Text="ThuOffice"></asp:Label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schFri">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Label ID="lblFriStart" runat="server" Text="FriStart"></asp:Label><asp:Label ID="lblFriDash" runat="server" Text="-"></asp:Label><asp:Label ID="lblFriEnd" runat="server" Text="FriEnd"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblFriOffice" runat="server" Text="FriOffice"></asp:Label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schSat">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Label ID="lblSatStart" runat="server" Text="SatStart"></asp:Label><asp:Label ID="lblSatDash" runat="server" Text="-"></asp:Label><asp:Label ID="lblSatEnd" runat="server" Text="SatEnd"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblSatOffice" runat="server" Text="SatOffice"></asp:Label>
                                            </li>
                                        </ul>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlScheduleEdit" runat="server" Visible="false">
                                    <div class="schSun">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Panel ID="pnlScheduleSunChooser" runat="server">
                                            <asp:Label ID="lblSunStatus" runat="server" Font-Bold="true" Text="Not Scheduled"></asp:Label><br /><br />
                                            <asp:Button ID="btnSunWorking" runat="server" Text="Working" CommandName="Working" CommandArgument="Sun" /><br /><br />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlScheduleSun" runat="server" Visible="false">
                                            Start<br />
                                            <asp:TextBox ID="txtSunStartHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtSunStartMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpSunStartAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            End<br />
                                            <asp:TextBox ID="txtSunEndHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtSunEndMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpSunEndAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:CheckBox ID="chkSunEmployeeWorksOvernight" Text="User Works Overnight" Font-Bold="true" runat="server" visible="false" />
                                            <br />
                                            Office<br />
                                            <asp:DropDownList ID="drpSunOffice" runat="server">
                                            </asp:DropDownList><asp:LinkButton ID="btnSunAssignOffice" runat="server" CommandName="AssignOffice" CommandArgument="Sun" Visible="false">Assign Office</asp:LinkButton><br />
                                            <asp:LinkButton ID="btnSunNoOffice" runat="server" CommandName="NoOffice" CommandArgument="Sun">No Office</asp:LinkButton><asp:Label ID="lblSunNoOfficeSep" runat="server" Text=" | "></asp:Label><asp:LinkButton ID="btnSunNotWorking" runat="server" CommandName="NotWorking" CommandArgument="Sun">Not Scheduled</asp:LinkButton>

                                        </asp:Panel>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schMon">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Panel ID="pnlScheduleMonChooser" runat="server">
                                            <asp:Label ID="lblMonStatus" runat="server" Font-Bold="true" Text="Not Scheduled"></asp:Label><br /><br />
                                            <asp:Button ID="btnMonWorking" runat="server" Text="Working" CommandName="Working" CommandArgument="Mon" /><br /><br />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlScheduleMon" runat="server" Visible="false">
                                            Start<br />
                                            <asp:TextBox ID="txtMonStartHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtMonStartMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpMonStartAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            End<br />
                                            <asp:TextBox ID="txtMonEndHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtMonEndMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpMonEndAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:CheckBox ID="chkMonEmployeeWorksOvernight" Text="User Works Overnight" Font-Bold="true" runat="server" visible="false" />
                                            <br />
                                            Office<br />
                                            <asp:DropDownList ID="drpMonOffice" runat="server">
                                            </asp:DropDownList><asp:LinkButton ID="btnMonAssignOffice" runat="server" CommandName="AssignOffice" CommandArgument="Mon" Visible="false">Assign Office</asp:LinkButton><br />
                                            <asp:LinkButton ID="btnMonNooffice" runat="server" CommandName="NoOffice" CommandArgument="Mon">No Office</asp:LinkButton><asp:Label ID="lblMonNoOfficeSep" runat="server" Text=" | "></asp:Label><asp:LinkButton ID="btnMonNotWorking" runat="server" CommandName="NotWorking" CommandArgument="Mon">Not Scheduled</asp:LinkButton>
                                        </asp:Panel>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schTue">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Panel ID="pnlScheduleTueChooser" runat="server">
                                            <asp:Label ID="lblTueStatus" runat="server" Font-Bold="true" Text="Not Scheduled"></asp:Label><br /><br />
                                            <asp:Button ID="btnTueWorking" runat="server" Text="Working" CommandName="Working" CommandArgument="Tue" /><br /><br />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlScheduleTue" runat="server" Visible="false">
                                            Start<br />
                                            <asp:TextBox ID="txtTueStartHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtTueStartMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpTueStartAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            End<br />
                                            <asp:TextBox ID="txtTueEndHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtTueEndMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpTueEndAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:CheckBox ID="chkTueEmployeeWorksOvernight" Text="User Works Overnight" Font-Bold="true" runat="server" visible="false" />
                                            <br />
                                            Office<br />
                                            <asp:DropDownList ID="drpTueOffice" runat="server">
                                            </asp:DropDownList><asp:LinkButton ID="btnTueAssignOffice" runat="server" CommandName="AssignOffice" CommandArgument="Tue" Visible="false">Assign Office</asp:LinkButton><br />
                                            <asp:LinkButton ID="btnTueNoOffice" runat="server" CommandName="NoOffice" CommandArgument="Tue">No Office</asp:LinkButton><asp:Label ID="lblTueNoOfficeSep" runat="server" Text=" | "></asp:Label><asp:LinkButton ID="btnTueNotWorking" runat="server" CommandName="NotWorking" CommandArgument="Tue">Not Scheduled</asp:LinkButton>
                                        </asp:Panel>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schWed">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Panel ID="pnlScheduleWedChooser" runat="server">
                                            <asp:Label ID="lblWedStatus" runat="server" Font-Bold="true" Text="Not Scheduled"></asp:Label><br /><br />
                                            <asp:Button ID="btnWedWorking" runat="server" Text="Working" CommandName="Working" CommandArgument="Wed" /><br /><br />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlScheduleWed" runat="server" Visible="false">
                                            Start<br />
                                            <asp:TextBox ID="txtWedStartHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtWedStartMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpWedStartAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            End<br />
                                            <asp:TextBox ID="txtWedEndHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtWedEndMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpWedEndAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:CheckBox ID="chkWedEmployeeWorksOvernight" Text="User Works Overnight" Font-Bold="true" runat="server" visible="false" />
                                            <br />
                                            Office<br />
                                            <asp:DropDownList ID="drpWedOffice" runat="server">
                                            </asp:DropDownList><asp:LinkButton ID="btnWedAssignOffice" runat="server" CommandName="AssignOffice" CommandArgument="Wed" Visible="false">Assign Office</asp:LinkButton><br />
                                            <asp:LinkButton ID="btnWedNoOffice" runat="server" CommandName="NoOffice" CommandArgument="Wed">No Office</asp:LinkButton><asp:Label ID="lblWedNoOfficeSep" runat="server" Text=" | "></asp:Label><asp:LinkButton ID="btnWedNotWorking" runat="server" CommandName="NotWorking" CommandArgument="Wed">Not Scheduled</asp:LinkButton>
                                        </asp:Panel>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schThu">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Panel ID="pnlScheduleThuChooser" runat="server">
                                            <asp:Label ID="lblThuStatus" runat="server" Font-Bold="true" Text="Not Scheduled"></asp:Label><br /><br />
                                            <asp:Button ID="btnThuWorking" runat="server" Text="Working" CommandName="Working" CommandArgument="Thu" /><br /><br />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlScheduleThu" runat="server" Visible="false">
                                            Start<br />
                                            <asp:TextBox ID="txtThuStartHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtThuStartMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpThuStartAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            End<br />
                                            <asp:TextBox ID="txtThuEndHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtThuEndMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpThuEndAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:CheckBox ID="chkThuEmployeeWorksOvernight" Text="User Works Overnight" Font-Bold="true" runat="server" visible="false" />
                                            <br />
                                            Office<br />
                                            <asp:DropDownList ID="drpThuOffice" runat="server">
                                            </asp:DropDownList><asp:LinkButton ID="btnThuAssignOffice" runat="server" CommandName="AssignOffice" CommandArgument="Thu" Visible="false">Assign Office</asp:LinkButton><br />
                                            <asp:LinkButton ID="btnThuNoOffice" runat="server" CommandName="NoOffice" CommandArgument="Thu">No Office</asp:LinkButton><asp:Label ID="lblThuNoOfficeSep" runat="server" Text=" | "></asp:Label><asp:LinkButton ID="btnThuNotWorking" runat="server" CommandName="NotWorking" CommandArgument="Thu">Not Scheduled</asp:LinkButton>
                                        </asp:Panel>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schFri">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Panel ID="pnlScheduleFriChooser" runat="server">
                                            <asp:Label ID="lblFriStatus" runat="server" Font-Bold="true" Text="Not Scheduled"></asp:Label><br /><br />
                                            <asp:Button ID="btnFriWorking" runat="server" Text="Working" CommandName="Working" CommandArgument="Fri" /><br /><br />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlScheduleFri" runat="server" Visible="false">
                                            Start<br />
                                            <asp:TextBox ID="txtFriStartHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtFriStartMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpFriStartAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            End<br />
                                            <asp:TextBox ID="txtFriEndHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtFriEndMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpFriEndAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:CheckBox ID="chkFriEmployeeWorksOvernight" Text="User Works Overnight" Font-Bold="true" runat="server" visible="false" />
                                            <br />
                                            Office<br />
                                            <asp:DropDownList ID="drpFriOffice" runat="server">
                                            </asp:DropDownList><asp:LinkButton ID="btnFriAssignOffice" runat="server" CommandName="AssignOffice" CommandArgument="Fri" Visible="false">Assign Office</asp:LinkButton><br />
                                            <asp:LinkButton ID="btnFriNoOffice" runat="server" CommandName="NoOffice" CommandArgument="Fri">No Office</asp:LinkButton><asp:Label ID="lblFriNoOfficeSep" runat="server" Text=" | "></asp:Label><asp:LinkButton ID="btnFriNotWorking" runat="server" CommandName="NotWorking" CommandArgument="Fri">Not Scheduled</asp:LinkButton>
                                        </asp:Panel>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="schSat">
                                        <ul class="date-n-time">
                                            <li>
                                        <asp:Panel ID="pnlScheduleSatChooser" runat="server">
                                            <asp:Label ID="lblSatStatus" runat="server" Font-Bold="true" Text="Not Scheduled"></asp:Label><br /><br />
                                            <asp:Button ID="btnSatWorking" runat="server" Text="Working" CommandName="Working" CommandArgument="Sat" /><br /><br />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlScheduleSat" runat="server" Visible="false">
                                            Start<br />
                                            <asp:TextBox ID="txtSatStartHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtSatStartMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpSatStartAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            End<br />
                                            <asp:TextBox ID="txtSatEndHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtSatEndMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                            <asp:DropDownList ID="drpSatEndAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:CheckBox ID="chkSatEmployeeWorksOvernight" Text="User Works Overnight" Font-Bold="true" runat="server" visible="false" />
                                            <br />
                                            Office<br />
                                            <asp:DropDownList ID="drpSatOffice" runat="server">
                                            </asp:DropDownList><asp:LinkButton ID="btnSatAssignOffice" runat="server" CommandName="AssignOffice" CommandArgument="Sat" Visible="false">Assign Office</asp:LinkButton><br />
                                            <asp:LinkButton ID="btnSatNoOffice" runat="server" CommandName="NoOffice" CommandArgument="Sat">No Office</asp:LinkButton><asp:Label ID="lblSatNoOfficeSep" runat="server" Text=" | "></asp:Label><asp:LinkButton ID="btnSatNotWorking" runat="server" CommandName="NotWorking" CommandArgument="Sat">Not Scheduled</asp:LinkButton>
                                        </asp:Panel>
                                            </li>
                                        </ul>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlScheduleNone" runat="server" Visible="false">
                                    <div class="schNoneFiller">
                                        &nbsp;
                                    </div>
                                </asp:Panel>
                                <div>
                                    <ul class="row-edit">
                                        <li>
                                    <asp:ImageButton ID="btnEditSchedule" runat="server" CommandName="EditSchedule" CommandArgument="1" ImageUrl="~/images/edit_small.png" AlternateText="Edit Schedule" ToolTip="header=[Edit Schedule] body=[] offsetx=[-150]" />
                                    <asp:ImageButton ID="btnSaveSchedule" runat="server" CommandName="SaveSchedule" CommandArgument="1" ImageUrl="~/images/save_small.png" AlternateText="Save Schedule" ToolTip="header=[Save Schedule] body=[] offsetx=[-150]" Visible="false" Style="position:relative; top:6px;" /><br />
                                    <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" CommandArgument="1" ImageUrl="~/images/cancel_small.png" AlternateText="Cancel" ToolTip="header=[Cancel] body=[] offsetx=[-150]" Visible="false" Style="position:relative; top:12px;" /><br />
                                    <asp:ImageButton ID="btnDeleteSchedule" runat="server" CommandName="DeleteSchedule" CommandArgument="1" ImageUrl="~/images/delete_small.png" AlternateText="Delete Schedule" ToolTip="header=[Delete Schedule] body=[] offsetx=[-150]" Visible="false" Style="position:relative; top:18px;" />
                                        </li>
                                    </ul>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ItemTemplate>
                <SeparatorTemplate>
                    <div class="schClear"></div>
                </SeparatorTemplate>
                <FooterTemplate>
                    </div>
                    <div class="schClear"></div>
                </FooterTemplate>
            </asp:Repeater>    
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
   
</asp:Content>



