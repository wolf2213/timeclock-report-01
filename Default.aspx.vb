Imports System.Configuration
Partial Class _Default
    Inherits System.Web.UI.Page

    Dim strLocation As String
    Dim intThisWeekTotalMinutes As Integer
    Dim intLastWeekTotalMinutes As Integer
    Dim hiddenControlValue As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TextBox1.Text = ConfigurationManager.AppSettings.Get("Timevalue")
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabHome As Panel = Master.FindControl("pnlTabHome")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2MyDashboard As Panel = Master.FindControl("pnlTab2MyDashboard")

        pnlMasterTabHome.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Timesheet
        pnlMasterTab2MyDashboard.CssClass = "tab2 tab2On"

        If Not Request.Cookies("ClockPoint") Is Nothing Then
            strLocation = "CP" & Request.Cookies("ClockPoint").Value
        Else
            strLocation = Request.ServerVariables("REMOTE_ADDR")
        End If

        If Not IsPostBack Then
            Dim dtmSchedStart As Date = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, Clock.GetNow()), Year(Clock.GetNow()), 0)
            Dim dtmSchedEnd As Date = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, Clock.GetNow()), Year(Clock.GetNow()), 1)

            lblScheduleTitle.Text = "My Schedule for "

            If Month(dtmSchedStart) = Month(dtmSchedEnd) Then
                lblScheduleTitle.Text = lblScheduleTitle.Text & MonthName(Month(dtmSchedStart)) & " " & Time.DaySuffix(Day(dtmSchedStart)) & " - " & Time.DaySuffix(Day(dtmSchedEnd))
            Else
                lblScheduleTitle.Text = lblScheduleTitle.Text & MonthName(Month(dtmSchedStart)) & " " & Time.DaySuffix(Day(dtmSchedStart)) & " - " & MonthName(Month(dtmSchedEnd)) & " " & Time.DaySuffix(Day(dtmSchedEnd))
            End If

            InitializePage()
            GetAlerts()
            GetUserSchedule()
            GetUserTimeRecords()
        End If
    End Sub

    Protected Sub InitializePage()
        Dim intUserStatus As String = UserInfo.UserStatus(Session("User_ID"))
        Trace.Write("Default", "Calling Sub to begin Initializing Page.  intUserStatus = '" & intUserStatus & "'")
        If IsNumeric(intUserStatus) Then
            If intUserStatus = 1 Then
                'clocked out
                lblClockInMessage.Text = "Currently clocked out"

                btnClockOut.Enabled = False

                btnClockIn.Visible = True
                btnClockOut.Visible = True
                btnBreakIn.Visible = False
                btnBreakOut.Visible = False

                Trace.Write("Default", "Initialized to status 1 (Clocked Out)")
            ElseIf intUserStatus = 2 Then
                'clocked in
                Dim objDR As System.Data.SqlClient.SqlDataReader
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT StartTime FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL"
                objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))

                objDR = objCommand.ExecuteReader()
                objDR.Read()

                Session("ClockedIn") = objDR("StartTime")

                lblClockInMessage.Text = "Clocked in for <b>" & TimeRecords.FormatMinutes(TimeRecords.TimeDifference(objDR("StartTime"), Clock.GetNow()), Session("User_ID"), True) & "</b>"

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                btnClockOut.Enabled = True

                btnClockIn.Visible = False
                btnClockOut.Visible = True
                btnBreakIn.Visible = True
                btnBreakOut.Visible = False

                Trace.Write("Default", "Initialized to status 2 (Clocked In)")
            ElseIf intUserStatus = 3 Then
                'on break
                Dim objDR As System.Data.SqlClient.SqlDataReader
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT StartTime FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL AND Type <> 1"
                objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))

                objDR = objCommand.ExecuteReader()
                objDR.Read()

                Session("ClockedIn") = objDR("StartTime")

                lblClockInMessage.Text = "On Break for <b>" & TimeRecords.FormatMinutes(TimeRecords.TimeDifference(objDR("StartTime"), Clock.GetNow()), Session("User_ID"), True) & "</b>"

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                btnClockOut.Enabled = True

                btnClockIn.Visible = False
                btnClockOut.Visible = True
                btnBreakIn.Visible = False
                btnBreakOut.Visible = True

                Trace.Write("Default", "Initialized to status 3 (Currently on break)")
            End If
        Else
            'there was an error, display it
            lblClockInMessage.Text = intUserStatus
        End If

        If Not Session("AllowClockInOut") Then
            lblNoClockPoint.Visible = True
            btnClockIn.Visible = False
            btnClockOut.Visible = False
            btnBreakIn.Visible = False
            btnBreakOut.Visible = False
        End If
    End Sub

    Protected Sub GetUserTimeRecords()
        Dim dtmStartDate As Date

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT MAX(EndDate) AS NewStartDate FROM PayrollSummary WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            dtmStartDate = Now.AddDays(-14)
            If Null.Replace(objDR("NewStartDate"), "null") <> "null" Then
                If Math.Abs(DateDiff(DateInterval.Day, Now, objDR("NewStartDate"))) <= 31 Then
                    dtmStartDate = DateAdd(DateInterval.Day, 1, objDR("NewStartDate"))
                End If
            End If
        Else
            dtmStartDate = Now.AddDays(-14)
        End If

        Dim strStartDate As String = dtmStartDate.Date & " 12:00 AM"

        Trace.Write("Default.aspx", "Display start date = " & strStartDate)

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM [TimeRecords] WHERE (([User_ID] = @User_ID) AND ([StartTime] BETWEEN @StartTime AND @EndTime)) ORDER BY [StartTime] DESC, [Type] DESC"
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@StartTime", strStartDate)
        objCommand.Parameters.AddWithValue("@EndTime", CDate(Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, Clock.GetNow()), Year(Clock.GetNow()), 1) & " 11:59 PM"))

        objDR = objCommand.ExecuteReader()

        rptUserTimeRecords.DataSource = objDR
        rptUserTimeRecords.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptUserRecords_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim litRepeater As Literal = e.Item.FindControl("litRepeater")
            Dim pnlTimeRow As Panel = e.Item.FindControl("pnlTimeRow")
            Dim imgError As Image = e.Item.FindControl("imgError")
            Dim imgRecordType As Image = e.Item.FindControl("imgRecordType")
            Dim lblDate As Label = e.Item.FindControl("lblDate")
            Dim imgStartLocation As Image = e.Item.FindControl("imgStartLocation")
            Dim lblStartTime As Label = e.Item.FindControl("lblStartTime")
            Dim dtStart As Date = e.Item.DataItem("StartTime")
            Dim imgEndLocation As Image = e.Item.FindControl("imgEndLocation")
            Dim lblEndTime As Label = e.Item.FindControl("lblEndTime")
            Dim imgVerified As Image = e.Item.FindControl("imgVerified")
            Dim lblDuration As Label = e.Item.FindControl("lblDuration")

            pnlTimeRow.CssClass = "timeTR tr" & litAlternate.Text
            If litAlternate.Text = "Normal" Then
                litAlternate.Text = "Alternate"
            Else
                litAlternate.Text = "Normal"
            End If

            imgRecordType.ToolTip = "header=[Clock In/Out]"

            If CInt(e.Item.DataItem("Type")) = 2 Or CInt(e.Item.DataItem("Type")) = 3 Then
                'break
                imgRecordType.ImageUrl = "~/images/timesheet_break.png"

                If CInt(e.Item.DataItem("Type")) = 2 Then
                    imgRecordType.AlternateText = "Unpaid Break"
                    imgRecordType.ToolTip = "header=[Unpaid Break]"
                Else
                    imgRecordType.AlternateText = "Paid Break"
                    imgRecordType.ToolTip = "header=[Paid Break]"
                End If
            ElseIf CInt(e.Item.DataItem("Type")) = 4 Then
                'absence
                imgRecordType.ImageUrl = "~/images/tab2_absence.png"
                imgRecordType.AlternateText = "Absence"
                imgRecordType.ToolTip = "header=[Absence]"
            End If

            Dim intCurrentDuration As Int64 = TimeRecords.TimeDifference(e.Item.DataItem("StartTime").ToString, e.Item.DataItem("EndTime").ToString)
            Dim intDurationToAdd As Int64 = 0

            imgRecordType.ToolTip &= " body=[Duration: " & TimeRecords.FormatMinutes(intCurrentDuration, Session("User_ID"), False) & "]"

            lblDate.Text = Time.FormatLongDate(dtStart.ToLongDateString)
            lblStartTime.Text = Time.QuickStrip(e.Item.DataItem("StartTime"))
            lblDuration.Text = TimeRecords.FormatMinutes(intCurrentDuration, Session("User_ID"), True)

            If e.Item.DataItem("PossibleError") Then
                imgError.Visible = True
            End If

            If e.Item.DataItem("Type") = 2 Then
                'the break is unpaid, add the negative break duration
                intDurationToAdd = intCurrentDuration * -1
            ElseIf e.Item.DataItem("Type") = 3 Then
                'this is a paid break, set duration = 0 so that the time isn't counted twice
                intDurationToAdd = 0
            Else
                intDurationToAdd = intCurrentDuration
            End If

            If Not IsPostBack Then
                If litRepeater.Text = "1" Then
                    intThisWeekTotalMinutes = intThisWeekTotalMinutes + intDurationToAdd
                Else
                    intLastWeekTotalMinutes = intLastWeekTotalMinutes + intDurationToAdd
                End If
            End If

            If e.Item.DataItem("StartLocation_ID") <> 99999999 Then
                Select Case Locations.LocationType(e.Item.DataItem("StartLocation_ID"))
                    Case 0 'Unverified Computer
                        imgStartLocation.ImageUrl = "~/images/timesheet_computer0.png"
                        imgStartLocation.AlternateText = "Location: Unverified" & vbCrLf & "IP Address: " & e.Item.DataItem("StartLocation")
                        imgStartLocation.ToolTip = "header=[Location] body=[Unverified<br>IP Address: " & e.Item.DataItem("StartLocation") & "]"
                    Case 1 'Verified Cookie
                        imgStartLocation.ImageUrl = "~/images/timesheet_computer.png"
                        imgStartLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("StartLocation_ID"))
                        imgStartLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("StartLocation_ID")) & "]"
                    Case 2 'Verified IP Address
                        imgStartLocation.ImageUrl = "~/images/timesheet_computer.png"
                        imgStartLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("StartLocation_ID"))
                        imgStartLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("StartLocation_ID")) & "]"
                    Case 3 'Verified Caller ID
                        imgStartLocation.ImageUrl = "~/images/timesheet_phone.png"
                        imgStartLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("StartLocation_ID"))
                        imgStartLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("StartLocation_ID")) & "]"
                    Case 4 'Mobile Access
                        imgStartLocation.ImageUrl = "~/images/timesheet_mobile.png"
                        imgStartLocation.AlternateText = "Location: Mobile Access" & vbCrLf & "IP Address: " & e.Item.DataItem("StartLocation")
                        imgStartLocation.ToolTip = "header=[Location] body=[Mobile Access<br>IP Address: " & e.Item.DataItem("StartLocation") & "]"
                End Select
            Else
                imgStartLocation.ImageUrl = "~/images/timesheet_phone0.png"
                imgStartLocation.AlternateText = "Location: Unverified" & vbCrLf & "Caller ID: " & e.Item.DataItem("StartLocation")
                imgStartLocation.ToolTip = "header=[Location] body=[Unverified<br>Caller ID: " & e.Item.DataItem("StartLocation") & "]"
            End If

            If e.Item.DataItem("ManualBy") <> 0 Then
                imgStartLocation.ImageUrl = "~/images/timesheet_manual.png"
                imgStartLocation.AlternateText = "Manually Added By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1)
                imgStartLocation.ToolTip = "header=[Manually Added] body=[By " & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1) & "]"
            End If

            If e.Item.DataItem("EditedBy") <> 0 Then
                imgStartLocation.ImageUrl = "~/images/timesheet_edited.png"
                imgStartLocation.AlternateText = "Edited By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1)
                imgStartLocation.ToolTip = "header=[Edited] body=[By " & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1) & "]"
            End If

            If Not e.Item.DataItem("EndTime") Is System.DBNull.Value Then
                'Some times end location id is null
                If CInt(Null.Replace(e.Item.DataItem("EndLocation_ID"), "0")) <> 99999999 Then
                    Select Case Locations.LocationType(CInt(Null.Replace(e.Item.DataItem("EndLocation_ID"), "0")))
                        Case 0 'Unverified Computer
                            imgEndLocation.ImageUrl = "~/images/timesheet_computer0.png"
                            imgEndLocation.AlternateText = "Location: Unverified" & vbCrLf & "IP Address: " & e.Item.DataItem("EndLocation")
                            imgEndLocation.ToolTip = "header=[Location] body=[Unverified<br>IP Address: " & e.Item.DataItem("EndLocation") & "]"
                        Case 1 'Verified Cookie
                            imgEndLocation.ImageUrl = "~/images/timesheet_computer.png"
                            imgEndLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("EndLocation_ID"))
                            imgEndLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("EndLocation_ID")) & "]"
                        Case 2 'Verified IP Address
                            imgEndLocation.ImageUrl = "~/images/timesheet_computer.png"
                            imgEndLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("EndLocation_ID"))
                            imgEndLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("EndLocation_ID")) & "]"
                        Case 3 'Verified Caller ID
                            imgEndLocation.ImageUrl = "~/images/timesheet_phone.png"
                            imgEndLocation.AlternateText = "Location: " & Locations.LocationName(e.Item.DataItem("EndLocation_ID"))
                            imgEndLocation.ToolTip = "header=[Location] body=[" & Locations.LocationName(e.Item.DataItem("EndLocation_ID")) & "]"
                        Case 4 'Mobile Access
                            imgEndLocation.ImageUrl = "~/images/timesheet_mobile.png"
                            imgEndLocation.AlternateText = "Location: Mobile Access" & vbCrLf & "IP Address: " & e.Item.DataItem("EndLocation")
                            imgEndLocation.ToolTip = "header=[Location] body=[Mobile Access<br>IP Address: " & e.Item.DataItem("EndLocation") & "]"
                    End Select
                Else
                    imgEndLocation.ImageUrl = "~/images/timesheet_phone0.png"
                    imgEndLocation.AlternateText = "Location: Unverified" & vbCrLf & "Caller ID: " & e.Item.DataItem("EndLocation")
                    imgEndLocation.ToolTip = "header=[Location] body=[Unverified<br>Caller ID: " & e.Item.DataItem("EndLocation") & "]"
                End If

                If e.Item.DataItem("ManualBy") <> 0 Then
                    imgStartLocation.ImageUrl = "~/images/timesheet_manual.png"
                    imgStartLocation.AlternateText = "Manually Added By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1)
                    imgStartLocation.ToolTip = "header=[Manually Added] body=[By " & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1) & "]"
                    imgEndLocation.ImageUrl = "~/images/timesheet_manual.png"
                    imgEndLocation.AlternateText = "Manually Added By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1)
                    imgEndLocation.ToolTip = "header=[Manually Added] body=[By " & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1) & "]"
                End If

                If e.Item.DataItem("EditedBy") <> 0 Then
                    imgStartLocation.ImageUrl = "~/images/timesheet_edited.png"
                    imgStartLocation.AlternateText = "Edited By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1)
                    imgStartLocation.ToolTip = "header=[Edited] body=[By " & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1) & "]"
                    imgEndLocation.ImageUrl = "~/images/timesheet_edited.png"
                    imgEndLocation.AlternateText = "Edited By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1)
                    imgEndLocation.ToolTip = "header=[Edited] body=[By " & UserInfo.NameFromID(e.Item.DataItem("EditedBy"), 1) & "]"
                End If

                If Time.StripTime(e.Item.DataItem("StartTime")) = Time.StripTime(e.Item.DataItem("EndTime")) Then
                    lblEndTime.Text = Time.QuickStrip(e.Item.DataItem("EndTime"))
                Else
                    lblEndTime.Text = Time.QuickStrip(e.Item.DataItem("EndTime")) & " (" & Time.StripTime(e.Item.DataItem("EndTime")) & ")"
                End If
            Else
                imgEndLocation.Visible = False
                lblEndTime.Text = ""
                lblDuration.Text = "<em>" & lblDuration.Text & "</em>"
            End If

            imgVerified.ToolTip = "header=[Unverified] body=[]"

            If e.Item.DataItem("VerifiedBy") <> 0 Then
                imgVerified.ImageUrl = "~/images/timesheet_verified.png"
                imgVerified.AlternateText = "Verified By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("VerifiedBy"), 1)
                imgVerified.ToolTip = "header=[Verified] body=[By " & UserInfo.NameFromID(e.Item.DataItem("VerifiedBy"), 1) & "]"
            End If

            If e.Item.DataItem("ManualBy") <> 0 Then
                imgStartLocation.ImageUrl = "~/images/timesheet_manual.png"
                imgStartLocation.AlternateText = "Manually Added By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1)
                imgEndLocation.ImageUrl = "~/images/timesheet_manual.png"
                imgEndLocation.AlternateText = "Manually Added By" & vbCrLf & UserInfo.NameFromID(e.Item.DataItem("ManualBy"), 1)
            End If
        ElseIf e.Item.ItemType = ListItemType.Footer Then
            Dim litRepeater As Literal = e.Item.FindControl("litRepeater")
            Dim lblTotalDuration As Label = e.Item.FindControl("lblTotalDuration")

            If Not IsPostBack Then
                Me.ViewState.Item("intThisWeekTotalMinutes") = intThisWeekTotalMinutes
                Me.ViewState.Item("intLastWeekTotalMinutes") = intLastWeekTotalMinutes
            End If

            If litRepeater.Text = "1" Then
                lblTotalDuration.Text = TimeRecords.FormatMinutes(Me.ViewState.Item("intThisWeekTotalMinutes"), Session("User_ID"), False)
            Else
                lblTotalDuration.Text = TimeRecords.FormatMinutes(Me.ViewState.Item("intLastWeekTotalMinutes"), Session("User_ID"), False)
            End If
        End If
    End Sub

    Protected Sub GetAlerts()
        'Dashboard message
        Dim objDRMessage As System.Data.SqlClient.SqlDataReader
        Dim objCommandMessage As System.Data.SqlClient.SqlCommand
        Dim objConnectionMessage As System.Data.SqlClient.SqlConnection
        objConnectionMessage = New System.Data.SqlClient.SqlConnection

        objConnectionMessage.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnectionMessage.Open()
        objCommandMessage = New System.Data.SqlClient.SqlCommand()
        objCommandMessage.Connection = objConnectionMessage
        objCommandMessage.CommandText = "SELECT DashboardMessageText, DashboardMessageRed, DashboardMessageBold FROM Preferences WHERE Client_ID = @Client_ID"
        objCommandMessage.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDRMessage = objCommandMessage.ExecuteReader()
        objDRMessage.Read()

        If objDRMessage("DashboardMessageText") <> "" Then
            lblAlertDashboard.Text = objDRMessage("DashboardMessageText") & "<br /><br />"
            If objDRMessage("DashboardMessageRed") Then
                lblAlertDashboard.ForeColor = Drawing.Color.Red
            End If
            lblAlertDashboard.Font.Bold = objDRMessage("DashboardMessageBold")
        Else
            lblAlertDashboard.Visible = False
        End If

        objDRMessage = Nothing
        objCommandMessage = Nothing
        objConnectionMessage.Close()
        objConnectionMessage = Nothing

        'Possible timesheet errors
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        Dim strSQLQuery As String

        If Permissions.TimesheetsDisplay(Session("User_ID")) Then
            strSQLQuery = "SELECT COUNT(Time_ID) AS PossibleErrorCount FROM [TimeRecords] WHERE PossibleError = 1 AND [Client_ID] = @Client_ID"
        Else
            strSQLQuery = "SELECT COUNT(Time_ID) AS PossibleErrorCount FROM [TimeRecords] WHERE PossibleError = 1 AND [User_ID] = @User_ID"
        End If

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        If objDR("PossibleErrorCount") = 1 Then
            lblAlertErrorsCount.Text = "1"
            lblAlertErrorsCount.Font.Bold = True
            lblAlertErrorsLabel.Font.Bold = True
            lblAlertErrorsSuffix.Visible = False
        ElseIf objDR("PossibleErrorCount") > 1 Then
            lblAlertErrorsCount.Text = objDR("PossibleErrorCount")
            lblAlertErrorsCount.Font.Bold = True
            lblAlertErrorsLabel.Font.Bold = True
            lblAlertErrorsSuffix.Font.Bold = True
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'Timesheet requests
        Dim objDR2 As System.Data.SqlClient.SqlDataReader
        Dim objCommand2 As System.Data.SqlClient.SqlCommand
        Dim objConnection2 As System.Data.SqlClient.SqlConnection
        objConnection2 = New System.Data.SqlClient.SqlConnection

        If Permissions.TimesheetsEdit(Session("User_ID")) Then
            strSQLQuery = "SELECT COUNT(Request_ID) AS RequestCount FROM [TimeRequests] WHERE ApprovedDeniedBy = 0 AND [Client_ID] = @Client_ID"
        Else
            strSQLQuery = "SELECT COUNT(Request_ID) AS RequestCount FROM [TimeRequests] WHERE ApprovedDeniedBy = 0 AND [User_ID] = @User_ID"
        End If

        objConnection2.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection2.Open()
        objCommand2 = New System.Data.SqlClient.SqlCommand()
        objCommand2.Connection = objConnection2
        objCommand2.CommandText = strSQLQuery
        objCommand2.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand2.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR2 = objCommand2.ExecuteReader()
        objDR2.Read()

        If objDR2("RequestCount") = 1 Then
            lblAlertTimesheetCount.Text = "1"
            lblAlertTimesheetCount.Font.Bold = True
            lblAlertTimesheetLabel.Font.Bold = True
            lblAlertTimesheetSuffix.Visible = False
        ElseIf objDR2("RequestCount") > 1 Then
            lblAlertTimesheetCount.Text = objDR2("RequestCount")
            lblAlertTimesheetCount.Font.Bold = True
            lblAlertTimesheetLabel.Font.Bold = True
            lblAlertTimesheetSuffix.Font.Bold = True
        End If

        objDR2 = Nothing
        objCommand2 = Nothing
        objConnection2.Close()
        objConnection2 = Nothing

        'Absence requests
        Dim objDR3 As System.Data.SqlClient.SqlDataReader
        Dim objCommand3 As System.Data.SqlClient.SqlCommand
        Dim objConnection3 As System.Data.SqlClient.SqlConnection
        objConnection3 = New System.Data.SqlClient.SqlConnection

        If Permissions.AbsencesApprove(Session("User_ID")) Then
            strSQLQuery = "SELECT COUNT(Absence_ID) AS AbsenceCount FROM [Absences] WHERE ApprovedDeniedBy = 0 AND [Client_ID] = @Client_ID"
        Else
            strSQLQuery = "SELECT COUNT(Absence_ID) AS AbsenceCount FROM [Absences] WHERE ApprovedDeniedBy = 0 AND [User_ID] = @User_ID"
        End If

        objConnection3.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection3.Open()
        objCommand3 = New System.Data.SqlClient.SqlCommand()
        objCommand3.Connection = objConnection3
        objCommand3.CommandText = strSQLQuery
        objCommand3.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand3.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR3 = objCommand3.ExecuteReader()
        objDR3.Read()

        If objDR3("AbsenceCount") = 1 Then
            lblAlertAbsencesCount.Text = "1"
            lblAlertAbsencesCount.Font.Bold = True
            lblAlertAbsencesLabel.Font.Bold = True
            lblAlertAbsencesSuffix.Visible = False
        ElseIf objDR3("AbsenceCount") > 1 Then
            lblAlertAbsencesCount.Text = objDR3("AbsenceCount")
            lblAlertAbsencesCount.Font.Bold = True
            lblAlertAbsencesLabel.Font.Bold = True
            lblAlertAbsencesSuffix.Font.Bold = True
        End If

        objDR3 = Nothing
        objCommand3 = Nothing
        objConnection3.Close()
        objConnection3 = Nothing

        'Birthdays
        Dim objDR5 As System.Data.SqlClient.SqlDataReader
        Dim objCommand5 As System.Data.SqlClient.SqlCommand
        Dim objConnection5 As System.Data.SqlClient.SqlConnection
        objConnection5 = New System.Data.SqlClient.SqlConnection

        objConnection5.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection5.Open()
        objCommand5 = New System.Data.SqlClient.SqlCommand()
        objCommand5.Connection = objConnection5
        objCommand5.CommandText = "SELECT [BirthdayNotification] FROM [Preferences] WHERE [Client_ID] = @Client_ID"
        objCommand5.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR5 = objCommand5.ExecuteReader()
        objDR5.Read()

        Dim boolShowBirthdays As Boolean

        boolShowBirthdays = objDR5("BirthdayNotification")

        objDR5 = Nothing
        objCommand5 = Nothing
        objConnection5.Close()
        objConnection5 = Nothing

        If boolShowBirthdays Then
            Dim objDR4 As System.Data.SqlClient.SqlDataReader
            Dim objCommand4 As System.Data.SqlClient.SqlCommand
            Dim objConnection4 As System.Data.SqlClient.SqlConnection
            objConnection4 = New System.Data.SqlClient.SqlConnection

            objConnection4.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection4.Open()
            objCommand4 = New System.Data.SqlClient.SqlCommand()
            objCommand4.Connection = objConnection4
            objCommand4.CommandText = "SELECT [FirstName], [LastName], [Birthday] FROM [Users] WHERE (DATEPART(dayofyear, [Birthday]) BETWEEN DATEPART(dayofyear, DATEADD(d, -1, GetDate())) AND DATEPART(dayofyear, DATEADD(d, 4, GetDate()))) AND [Client_ID] = @Client_ID ORDER BY DATEPART(dayofyear, [Birthday])"
            objCommand4.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

            objDR4 = objCommand4.ExecuteReader()

            ' rptAlertBirthdays.DataSource = objDR4
            ' rptAlertBirthdays.DataBind()

            objDR4 = Nothing
            objCommand4 = Nothing
            objConnection4.Close()
            objConnection4 = Nothing
        End If
    End Sub


    Protected Sub GetUserSchedule()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM [Schedules] WHERE (([User_ID] = @User_ID) AND ([Year] = @Year) AND (WeekNum = @WeekNum))"
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@WeekNum", DatePart(DateInterval.WeekOfYear, Date.Now))
        objCommand.Parameters.AddWithValue("@Year", DatePart(DateInterval.Year, Date.Now))

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        Dim arrDayPref(6) As String
        arrDayPref(0) = "Sun"
        arrDayPref(1) = "Mon"
        arrDayPref(2) = "Tue"
        arrDayPref(3) = "Wed"
        arrDayPref(4) = "Thu"
        arrDayPref(5) = "Fri"
        arrDayPref(6) = "Sat"

        If objDR.HasRows Then
            lblScheduleError.Visible = False
            scheduleTimeDisplay(objDR("SunCode"), objDR("SunStart"), objDR("SunEnd"), lblSchSun)
            scheduleTimeDisplay(objDR("MonCode"), objDR("MonStart"), objDR("MonEnd"), lblSchMon)
            scheduleTimeDisplay(objDR("TueCode"), objDR("TueStart"), objDR("TueEnd"), lblSchTue)
            scheduleTimeDisplay(objDR("WedCode"), objDR("WedStart"), objDR("WedEnd"), lblSchWed)
            scheduleTimeDisplay(objDR("ThuCode"), objDR("ThuStart"), objDR("ThuEnd"), lblSchThu)
            scheduleTimeDisplay(objDR("FriCode"), objDR("FriStart"), objDR("FriEnd"), lblSchFri)
            scheduleTimeDisplay(objDR("SatCode"), objDR("SatStart"), objDR("SatEnd"), lblSchSat)
        Else
            lblScheduleError.Text = "<br><br>Not scheduled"
            pnlSchedule.Visible = False
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub ScheduleTimeDisplay(ByVal strDayCode As String, ByVal dtmStartTime As Date, ByVal dtmEndTime As Date, ByVal lblName As Label)
        'scheduleTimeDisplay(Container.DataItem("SunCode"), Container.DataItem("SunStart"), Container.DataItem("SunEnd")
        If Not IsNumeric(strDayCode) Then
            'the person is not working this day
            If strDayCode = "NW" Then
                lblName.Text = "Not Working<br>&nbsp;"
                lblName.ForeColor = Drawing.Color.DarkGray
            Else
                lblName.Text = AbsencesClass.AbsenceNameFromCode(strDayCode) & " - " & DateDiff(DateInterval.Hour, dtmStartTime, dtmEndTime) & " Hours"
                lblName.ForeColor = Drawing.Color.DarkRed
            End If
        ElseIf strDayCode = "0" Then
            'working, not assigned an office
            lblName.Text = Time.QuickStrip(dtmStartTime) & " - " & Time.QuickStrip(dtmEndTime) & "<br>&nbsp;"
        Else
            'working and assigned office
            lblName.Text = Time.QuickStrip(dtmStartTime) & " - " & Time.QuickStrip(dtmEndTime) & " - " & Offices.OfficeName(CInt(strDayCode)) & "<br>&nbsp;"

        End If
    End Sub

    Protected Sub btnClockIn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClockIn.Click


        Dim intClockInStatus As Int16

        If (Not Clock.CheckUserAllowedToClockInBeforeScheduleTime(Session("Client_ID"), Session("User_ID"))) Then
            intClockInStatus = 4
        Else
            intClockInStatus = Clock.ClockIn(Session("Client_ID"), Session("User_ID"), Session("Manager"), strLocation)
        End If

        Select Case intClockInStatus
            Case 1
                'success
                'reload the page
                InitializePage()
            Case 2
                'trying to clock in when the person is already clocked in
                lblClockInMessage.Text = "Already Clocked In"
            Case 3
                'userStatus error
                lblClockInMessage.Text = "User Status Error"
            Case 4
                'userStatus error
                lblClockInMessage.ForeColor = Drawing.Color.Red
                lblClockInMessage.Text = "You are not allowed to Clock In before and after scheduled work hours. Please contact your manager."
        End Select

        GetUserTimeRecords()
    End Sub

    Protected Sub btnClockOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClockOut.Click
        Dim bOverrideUserClockOutTime As Boolean = False
        Dim dtScheduleEndTime As DateTime
        Dim intMostRecentTRID As Int64 = TimeRecords.GetMostRecentTimeID(Convert.ToInt64(Session("User_ID")))

        Dim intClockInStatus = Clock.ClockOut(Session("Client_ID"), Session("User_ID"), Session("Manager"), strLocation, Session("ClockedIn"))

        Select Case intClockInStatus
            Case 1
                'success
                'check if user not allowed to work overtime, override end time with schedule end time
                If (Not Clock.CheckUserAllowedToClockOutAfterScheduleTime(Session("Client_ID"), Session("User_ID"), dtScheduleEndTime)) Then
                    Clock.ClockOutOverride(intMostRecentTRID, dtScheduleEndTime)
                    bOverrideUserClockOutTime = True
                End If
                'reload page
                InitializePage()
            Case 2
                'record deleted
                'reload page
                InitializePage()
                lblClockInMessage.Text = "<strong style='color:red;'>Record deleted:</strong> Clocked in for 0 minutes."
            Case 3
                lblClockInMessage.Text = "Already Clocked Out"
            Case 4
                lblClockInMessage.Text = "User Status Error"
        End Select

        If bOverrideUserClockOutTime Then
            lblClockInMessage.Text = "You are not allowed to work after your schedule end time. Your clock out time is set to your schedule end time. Please contact your manager."
        End If

        GetUserTimeRecords()
    End Sub

    Protected Sub btnBreakIn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim intClockInStatus = Clock.BreakIn(Session("Client_ID"), Session("User_ID"), Session("Manager"), strLocation)

        Select Case intClockInStatus
            Case 1
                'clocked in to the break successfully, reload the page
                InitializePage()
            Case Else
                lblClockInMessage.Text = "Break In Error. Status = " & intClockInStatus
        End Select

        GetUserTimeRecords()
    End Sub

    Protected Sub btnBreakOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim intClockInStatus = Clock.BreakOut(Session("Client_ID"), Session("User_ID"), Session("Manager"), strLocation, Session("dtClockedIn"))

        Select Case intClockInStatus
            Case 1
                'success
                'reload page
                InitializePage()
            Case 2
                'record deleted
                'reload page
                InitializePage()
                lblClockInMessage.Text = "<strong style='color:red;'>Break deleted:</strong> Clocked in for 0 minutes."
            Case 3
                lblClockInMessage.Text = "Not On Break"
            Case 4
                lblClockInMessage.Text = "User Status Error"
        End Select

        GetUserTimeRecords()
    End Sub

    Protected Sub btnAlertErorrs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAlertErrors.Click
        Response.Redirect("Timesheets.aspx?view=errors")
    End Sub

    Protected Sub btnAlertTimesheet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAlertTimesheet.Click
        Response.Redirect("TimesheetRequests.aspx")
    End Sub

    Protected Sub btnAlertAbsences_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAlertAbsences.Click
        Response.Redirect("AbsencesApprove.aspx")
    End Sub
End Class