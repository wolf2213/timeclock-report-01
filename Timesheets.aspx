<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Timesheets.aspx.vb" Inherits="Timesheets" title="TimeClockWizard - Timesheets" %>
<%@ Register TagPrefix="cii" TagName="UserList" Src="~/UserList.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">


    
    <asp:Literal ID="litAlternate" runat="server" Text="Normal" Visible="false"></asp:Literal>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div class="main-content" style="width:1050px;">

         
          
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    <div style="margin-left: 25px; float: left;">
                        <div style="width: 300px; text-align:center;">

                            
									<h4 style="text-align:center;"><center><asp:Label ID="lblManuallyAddRecord" runat="server" Text="Manually Add a Record"></asp:Label></center>
                                        <h4></h4>
                                        <div style="border: 1px solid #c6ced0; padding-top: 30px;">
                                            <asp:Panel ID="pnlManualAdd" runat="server">
                                                <strong>For: </strong>
                                                <asp:DropDownList ID="ddlChooseUser" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblManualUser" runat="server" Text="Name" Visible="false"></asp:Label>
                                                <br />
                                                <center>
                                                    <asp:RadioButtonList ID="rblType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblType_SelectedIndexChanged1" RepeatDirection="Horizontal">
                                                        <asp:ListItem Selected="true" Value="1">Clock In/Out</asp:ListItem>
                                                        <asp:ListItem Value="2">Break</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </center>
                                                <br />
                                                <strong>
                                                <asp:Literal ID="litInOut1" runat="server"></asp:Literal>
                                                </strong>

                                 
 <asp:TextBox ID="lblClockedInDate" runat="server" ClientIDMode="Static" Width="70px">
          </asp:TextBox>                                 
                                 

                                                <br />
                                                <center>
                                                    <asp:Calendar ID="calClockedIn" runat="server" BackColor="White" BorderColor="#c6ced0" Font-Names="Verdana" ForeColor="Black" Visible="false">
                                                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                                        <SelectorStyle BackColor="#CCCCCC" />
                                                        <WeekendDayStyle BackColor="#FFFFCC" />
                                                        <TodayDayStyle BackColor="White" ForeColor="Black" />
                                                        <OtherMonthDayStyle ForeColor="#c6ced0" />
                                                        <NextPrevStyle VerticalAlign="Bottom" />
                                                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" />
                                                        <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                                                    </asp:Calendar>
                                                </center>
                                                <strong>At</strong>
                                                <asp:TextBox ID="txtClockedInHour" runat="server" MaxLength="2" Width="16"></asp:TextBox>
                                                :<asp:TextBox ID="txtClockedInMinute" runat="server" MaxLength="2" Width="16"></asp:TextBox>
                                                <asp:DropDownList ID="drpClockedInAMPM" runat="server">
                                                    <asp:ListItem>AM</asp:ListItem>
                                                    <asp:ListItem>PM</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RangeValidator ID="valClockedInHour" runat="server" ControlToValidate="txtClockedInHour" Display="Dynamic" ErrorMessage="&lt;br /&gt;Invalid hour" MaximumValue="12" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                                                <asp:RangeValidator ID="valClockedInMinute" runat="server" ControlToValidate="txtClockedInMinute" Display="Dynamic" ErrorMessage="&lt;br /&gt;Invalid minute" MaximumValue="59" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                                                <br />
                                                <br />
                                                <asp:CheckBox ID="chkRemainClockedIn" runat="server" AutoPostBack="true" Text="Remain clocked in" />
                                                <asp:Label ID="lblAlreadyClockedIn" runat="server" ForeColor="Red" Text="&lt;br /&gt;This user is already clocked in" Visible="false"></asp:Label>
                                                <asp:Literal ID="litClockInSpacer" runat="server" Text="&lt;br /&gt;"></asp:Literal>
                                                <asp:Panel ID="pnlClockOut" runat="server">
                                                    <strong>
                                                    <asp:Literal ID="litInOut2" runat="server"></asp:Literal>
                                                    </strong>
 
 <asp:TextBox ID="lblClockedOutDate" runat="server" ClientIDMode="Static" Width="70px">
          </asp:TextBox> 
                                                    <br />
                                                    <center>
                                                        <asp:Calendar ID="calClockedOut" runat="server" BackColor="White" BorderColor="#c6ced0" CellPadding="4" Font-Names="Verdana" ForeColor="Black" Visible="false">
                                                            <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                                            <SelectorStyle BackColor="#CCCCCC" />
                                                            <WeekendDayStyle BackColor="#FFFFCC" />
                                                            <TodayDayStyle BackColor="White" ForeColor="Black" />
                                                            <OtherMonthDayStyle ForeColor="#c6ced0" />
                                                            <NextPrevStyle VerticalAlign="Bottom" />
                                                            <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" />
                                                            <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                                                        </asp:Calendar>
                                                    </center>
                                                    <strong>At</strong>
                                                    <asp:TextBox ID="txtClockedOutHour" runat="server" MaxLength="2" Width="16"></asp:TextBox>
                                                    :<asp:TextBox ID="txtClockedOutMinute" runat="server" MaxLength="2" Width="16"></asp:TextBox>
                                                    <asp:DropDownList ID="drpClockedOutAMPM" runat="server">
                                                        <asp:ListItem>AM</asp:ListItem>
                                                        <asp:ListItem>PM</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RangeValidator ID="valClockedOutHour" runat="server" ControlToValidate="txtClockedOutHour" Display="Dynamic" ErrorMessage="&lt;br /&gt;Invalid hour" MaximumValue="12" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                                                    <asp:RangeValidator ID="valClockedOutMinute" runat="server" ControlToValidate="txtClockedOutMinute" Display="Dynamic" ErrorMessage="&lt;br /&gt;Invalid minute" MaximumValue="59" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                                                    <asp:Label ID="lblReason" runat="server" Font-Bold="true" Text="&lt;br /&gt;&lt;br /&gt;Reason&lt;br /&gt;"></asp:Label>
                                                    <asp:TextBox ID="txtReason" runat="server" MaxLength="255"></asp:TextBox>
                                                    <br />
                                                </asp:Panel>
                                                <br />
                                                <asp:ImageButton ID="btnAddRecord" runat="server" ImageUrl="~/images/addrecord-btn.png" Visible="false" />
                                                <asp:ImageButton ID="btnAddRequest" runat="server" ImageUrl="~/images/requestrecord-btn.png" />
                                                <asp:Label ID="lblSuccess" runat="server" ForeColor="Green" Text="&lt;br /&gt;Record was requested successfully" Visible="false"></asp:Label>
                                                <asp:Label ID="lblClockTimesError" runat="server" ForeColor="Red" Text="&lt;br /&gt;Clock in must be before clock out" Visible="false"></asp:Label>
                                                <br />
                                                <br />
                                            </asp:Panel>
                                        </div>
                                    </h4>

                
                            </div>

<br />
                        <div style="width: 300px; text-align:center;">

                            
									<h4><asp:Label ID="lblVerifyAll" runat="server" Text="Verify Visible Records"></asp:Label></h4>

                <div style="border: 1px solid #c6ced0; padding-top: 30px;">


                        <asp:Panel ID="pnlMassVerify" runat="server" Visible="false" DefaultButton="btnAddRecord">
                            
                           
                            <asp:ImageButton ID="btnVerifyAll" ImageUrl="~/images/verify-btn.png" runat="server" OnClick="btnVerifyAll_Click" />
                            <br /><br />
                        </asp:Panel>

                    </div>
                            </div>
<br />
                        <div style="width: 300px; text-align:center;">
                            
									<h4>Display Timesheets</h4>

                <div style="border: 1px solid #c6ced0; padding-top: 30px;">
                   
                           <asp:Label ID="lblDisplayErrors" runat="server" Text="<br />Showing possible errors<br />" Visible="false"></asp:Label>
                            <asp:Panel ID="pnlDisplayCriteria" runat="server">
                            <br />
                            <strong>From</strong>
                               
<asp:TextBox ID="lblDisplayStartDate" runat="server" ClientIDMode="Static" Width="70px" OnTextChanged="lblDisplayStartDate_SelectionChanged" AutoPostBack="True" >
          </asp:TextBox>                              


                            <strong>To</strong>
                            
 <asp:TextBox ID="lblDisplayEndDate" runat="server" ClientIDMode="Static" Width="70px" OnTextChanged="lblDisplayEndDate_SelectionChanged" AutoPostBack="True">
          </asp:TextBox>                           

                                
                             <asp:Calendar ID="calDisplayStartDate" runat="server" BackColor="White" BorderColor="#c6ced0" Font-Names="Verdana" ForeColor="Black" Visible="false">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="White" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#c6ced0" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" />
                                <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>


                            <asp:Calendar ID="calDisplayEndDate" runat="server" BackColor="White" BorderColor="#c6ced0"
                                Font-Names="Verdana" ForeColor="Black" Visible="false">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="White" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#c6ced0" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" />
                                <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>                               
                                

 
                            <br />
                            <br />
                            <div align="center">
                            <strong>Sort</strong>
                                
                                <asp:RadioButtonList ID="rblSort" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                <asp:ListItem Value="DESC" Text="Newest first" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="ASC" Text="Oldest first"></asp:ListItem>
                            </asp:RadioButtonList>
                            <br />
                            <strong>For</strong>
                                <center>
                            <cii:UserList runat="server" ID="culDisplayUsers" InitialSelection="0" />
                                
                            <asp:Label ID="lblDisplayUser" runat="server" Visible="false" Text="Name"></asp:Label>
                             
                                    
                                    </center>     
                            </asp:Panel>
                    
                            <br />
                            <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/images/print-btn.png" OnClientClick="window.print()" />
                    <br /><br />

                            </div>
                        </div>
              </div>          
                    </ContentTemplate>
                </asp:UpdatePanel>
                
         

<div style="margin-left: 350px; width: 600px;">

                <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>



                        <asp:Repeater ID="rptUsers" runat="server" OnItemDataBound="rptUsers_ItemDataBound" EnableViewState="true">
                            <ItemTemplate>
                                <asp:Panel ID="pnlPageBreak" runat="server" CssClass="pageBreak"></asp:Panel>
                                <asp:Panel ID="pnlUserInfo" runat="server">
                                    <strong><asp:Label ID="lblUserName" runat="server" Text="Name"></asp:Label></strong><asp:Label ID="lblNoTimes" runat="server" Text="&nbsp;&nbsp;- No Times" Width="272px"></asp:Label>
                                </asp:Panel>

                                <asp:Repeater ID="rptTimes" runat="server" OnItemDataBound="rptTimes_ItemDataBound" OnItemCommand="rptTimes_ItemCommand">
                                    <HeaderTemplate>

                                 <!-- new info -->

                                        <div class="timeTable" style="width: 600px;">
                                            <div class="timeTR trHeader">
                                                <div class="timeDate trHeader">Date</div>
                                                <div class="timeStart trHeader">Start</div>
                                                <div class="timeEnd trHeader">End</div>
                                                <div class="timeDurationWithButtons trHeader">Duration</div>
                                                <div class="timeButtons trHeader">&nbsp;</div>
                                            </div>
                                    </HeaderTemplate>


                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="upnlTimeRow" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="pnlTimeRow" runat="server" CssClass="timeTR">
                                                    <asp:Literal ID="litOriginalClass" runat="server" Visible="false"></asp:Literal>
                                                    <asp:Panel ID="pnlTimeView" runat="server">



                                                        <div class="timeTR timeDate"><asp:Image ID="imgError" runat="server" ImageUrl="~/images/timesheet_error.png" ImageAlign="AbsMiddle" AlternateText="Possible error" ToolTip="header=[Possible error] body=[]" Visible="false" /><asp:Image ID="imgRecordType" runat="server" ImageUrl="~/images/timesheet_clockinout.png" ImageAlign="AbsMiddle" AlternateText="Clock In/Out" />&nbsp;<asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label></div>


                                                        <div class="timeTR timeStart"><asp:Image ID="imgStartLocation" runat="server" ImageAlign="AbsMiddle" />&nbsp;<asp:Label ID="lblStartTime" runat="server" Text="StartTime"></asp:Label></div>


                                                        <div class="timeTR timeEnd"><asp:Image ID="imgEndLocation" runat="server" ImageAlign="AbsMiddle" />&nbsp;<asp:Label ID="lblEndTime" runat="server" Text="EndTime"></asp:Label></div>


                                                        <div class="timeTR timeDurationWithButtons"><asp:ImageButton ID="btnVerify" runat="server" ImageUrl="~/images/timesheet_unverified.png" ImageAlign="AbsMiddle" AlternateText="Unverified" Enabled="false" ToolTip="header=[Unverified] body=[]" />&nbsp;<asp:Label ID="lblDuration" runat="server" Text="Duration"></asp:Label></div>
                                                    </asp:Panel>


                                                    <asp:Panel ID="pnlTimeEdit" runat="server" Visible="false">

                                                        <div class="timeDate"><asp:Image ID="imgEditRecordType" runat="server" ImageUrl="~/images/timesheet_clockinout.png" ImageAlign="AbsMiddle" AlternateText="Clock In/Out" /></div>


                                                        <div class="timeStart">
                                                            <asp:TextBox ID="txtStartHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtStartMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                                            <asp:DropDownList ID="drpStartAMPM" runat="server">
                                                                <asp:ListItem>AM</asp:ListItem>
                                                                <asp:ListItem>PM</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <br />
                                                            <asp:TextBox ID="txtStartDate" runat="server" Width="70" MaxLength="10"></asp:TextBox>
                                                        </div>
                                                        <asp:Panel ID="pnlEditEndTime" runat="server" CssClass="timeEnd">
                                                            <asp:TextBox ID="txtEndHour" runat="server" Width="16" MaxLength="2"></asp:TextBox>:<asp:TextBox ID="txtEndMin" runat="server" Width="16" MaxLength="2"></asp:TextBox>
                                                            <asp:DropDownList ID="drpEndAMPM" runat="server">
                                                                <asp:ListItem>AM</asp:ListItem>
                                                                <asp:ListItem>PM</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <br />
                                                            <asp:TextBox ID="txtEndDate" runat="server" Width="70" MaxLength="10"></asp:TextBox>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlEditNoEndTime" runat="server" CssClass="timeEnd" Visible="false">
                                                            <asp:Button ID="btnEditClockOut" runat="server" CommandName="ClockOut" CommandArgument="1" Text="Add End Time" />
                                                        </asp:Panel>
                                                        <div class="timeDurationWithButtons">
                                                            <asp:Label ID="lblEditReason" runat="server" Text="Reason<br />"></asp:Label>
                                                            <asp:TextBox ID="txtEditReason" runat="server" Width="100" MaxLength="255"></asp:TextBox>
                                                        </div>
                                                    </asp:Panel>


                                                    <div class="timeButtons">
                                                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit" CommandArgument="1" ImageUrl="~/images/edit_small.png" AlternateText="Edit Time Record" ToolTip="header=[Edit Time Record] body=[] offsetx=[-150]" Style="position:relative; top:0px;" />
                                                        <asp:ImageButton ID="btnSave" runat="server" CommandName="Save" CommandArgument="1" ImageUrl="~/images/save_small.png" AlternateText="Save Time Record" ToolTip="header=[Save Time Record] body=[] offsetx=[-150]" Visible="false" Style="position:relative; top:0px;" /><br />
                                                        <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" CommandArgument="1" ImageUrl="~/images/cancel_small.png" AlternateText="Cancel" ToolTip="header=[Cancel] body=[] offsetx=[-150]" Visible="false" Style="position:relative; top:6px;" /><br />
                                                        <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" CommandArgument="1" ImageUrl="~/images/delete_small.png" AlternateText="Delete Time Record" ToolTip="header=[Delete Time Record] body=[] offsetx=[-150]" Visible="false" Style="position:relative; top:12px;" />
                                                    </div>                                           



                                                   
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                            <div class="timeTR trFooter">
                                                <div class="trFooter timeDate">&nbsp;</div>
                                                <div class="trFooter timeStart">&nbsp;</div>
                                                <div class="trFooter timeEnd">&nbsp;</div>
                                                <div class="trFooter timeDurationWithButtons"><%#DurationTotal()%></div>
                                                <div class="trFooter timeButtons">&nbsp;</div>
                                            </div>
                                            
                                        </div>
                                    </FooterTemplate>
                                    <SeparatorTemplate>
                                    
                                    </SeparatorTemplate>
                                </asp:Repeater>
                            </ItemTemplate>
                            <SeparatorTemplate>
                                <span class="doNotPrint"><br /></span>
                            </SeparatorTemplate>
                        </asp:Repeater>
   
                    </ContentTemplate>
                </asp:UpdatePanel>
     </div>

        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <asp:Timer ID="timerLogout" runat="server">
            </asp:Timer>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>