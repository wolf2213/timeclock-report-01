<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SettingsClockPoints.aspx.vb" Inherits="SettingsClockPoints" title="TimeClockWizard - ClockPoints" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" ></asp:ScriptManager>
 <div class="main-content" style="width:1050px;">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="float: left; margin-left: 25px;">
                 
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
<div style="width: 300px;">
                    <asp:Panel ID="pnlAdd" runat="server">

                        <h4>Add New ClockPoint&nbsp;<asp:Image ID="imgHelpClockPoints" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" /></h4>

                        <div style="border: 1px solid #c6ced0; padding: 30px 0 0 30px;">


														<div style="margin-bottom: 5px;">

                                    <asp:RadioButtonList ID="rblType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="rblType_SelectedIndexChanged">
                                        <asp:ListItem Value="1" Text="Current Computer (Cookie)" Selected="true"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Current Internet Connection (IP)"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Manually Enter IP Address"></asp:ListItem>
                                    </asp:RadioButtonList>

                                    <center>
                                        <asp:Label ID="lblTypeDescription" runat="server" ForeColor="Gray" Font-Size="Smaller"></asp:Label>
                                    </center>
                                <br /><br />
                                    <asp:Label ID="lblIPLabel" runat="server" Text="IP Address" Visible="false"></asp:Label>
                                <br /><br />
                                    <asp:TextBox ID="txtIPAddress" runat="server" Width="100" MaxLength="15" Visible="false"></asp:TextBox>                                  
                               <br /><br />
                                    <asp:Label ID="lblNameLabel" runat="server" Text="Name"></asp:Label>
                                <br /><br />
                                    <asp:TextBox ID="txtClockPointName" runat="server" MaxLength="20" Width="100"></asp:TextBox>&nbsp;<asp:ImageButton ID="btnAddClockPoint" runat="server" ImageAlign="absMiddle" ImageUrl="~/images/add_small.png" AlternateText="Add New ClockPoint"  />                                   
                                    <br />
                                    <asp:Label ID="lblExample" runat="server" ForeColor="Gray" Font-Size="Smaller" Text="Ex. Mike's Computer, or Detroit Office" ></asp:Label>
                                
                        <asp:Label ID="lblAddMessage" runat="server" Text="Label" Visible="false"></asp:Label>
</div>
                            </div>

                    </asp:Panel>
</div>

                                </ContentTemplate>
                        </asp:UpdatePanel>

                
<div class="clear"></div>
                <br />

<div style="width: 300px;">
                    <asp:Panel ID="pnlClockPointRestriction" runat="server">

                        <asp:UpdatePanel ID="upnlClockPointRestriction" runat="server" ChildrenAsTriggers="true">
                            <ContentTemplate>

                                    <h4>ClockPoint Restriction&nbsp;<asp:Image ID="imgClockPointRestrictionHelp" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" ToolTip="cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[ClockPoint Restriction] body=[When enabled, ClockPoint Restriction requires users to clock in and out only from authorized ClockPoints.]" /></h4>

                                    <div style="border: 1px solid #c6ced0; padding: 30px 0 0 30px;">


														<div style="margin-bottom: 5px;">

                               
                                <asp:RadioButtonList ID="rblClockPointRestriction" runat="server">
                                    <asp:ListItem Text="Enabled" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Disabled" Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:Button ID="btnSaveClockPointRestriction" runat="server" Text="Save" OnClick="btnSaveClockPointRestriction_Click" />
                                <asp:Label ID="lblChanged" runat="server" Text="Changed" ForeColor="White" Visible="false"></asp:Label>

                                                            </div>
                                    </div>


                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>

    </div>
                </div>

<div style="margin-left: 350px; width: 550px;">
                    <asp:Repeater ID="rptClockPointList" OnItemDataBound="rptClockPointList_ItemDataBound" OnItemCommand="rptClockPointList_ItemCommand" EnableViewState="True" runat="server">
                        <HeaderTemplate>
                            <ul class="row-user1">
                        <li class="column-head column-title">Name</li>
                        </ul>
                                        
                          <ul class="clocktype">
                        <li class="column-head column-title">Type</li>
                        </ul>

<ul class="editlinks">
 <li class="column-head column-title">&nbsp;</li> </ul>

                        </HeaderTemplate>
                        <ItemTemplate>
                            <ul class="row-user1">
                        <li><asp:Panel ID="pnlName" runat="server">
                                        <asp:Label ID="lblClockPointName" runat="server" Text="Label"></asp:Label>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlConfirmDelete" Visible="false" runat="server">
                                        Delete the '<asp:Label ID="lblDeleteClockPointName" runat="server"></asp:Label>' ClockPoint?  
                                        <asp:Button ID="btnConfirmDelete" runat="server" Text="Delete" />
                                        <asp:Button ID="btnCancelDelete" runat="server" Text="Cancel" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEdit" runat="server" Visible="false">
                                        <asp:TextBox ID="txtEditName" runat="server" Width="100">&nbsp;</asp:TextBox><asp:Label ID="lblEditMessage" runat="server" Visible="false"></asp:Label>
                                    </asp:Panel></li>
                        </ul>
                                    
                          <ul class="clocktype">
                        <li><asp:Label ID="lblType" runat="server"></asp:Label></li>
                        </ul>
                                    
<ul class="editlinks">
 <li>
     <asp:Panel ID="pnlNormalButtons" runat="server">
                                        <asp:ImageButton ID="imgEdit" CommandName="ClockPointEdit" ImageUrl="~/images/edit_small.png" runat="server" />
                                        <asp:ImageButton ID="imgDelete" commandName="ClockPointDelete" ImageUrl="~/images/delete_small.png" runat="server" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEditButtons" runat="server" Visible="false">
                                        <asp:ImageButton ID="btnEditSave" CommandName="EditSave" ImageUrl="~/images/save_small.png" runat="server" AlternateText="Save Changes" />
                                        <asp:ImageButton ID="btnEditCancel" commandName="EditCancel" ImageUrl="~/images/cancel_small.png" runat="server" AlternateText="Cancel Changes" />
                                    </asp:Panel></li> </ul>

                       
                        </ItemTemplate>
 
                        <FooterTemplate>
                          
                        </FooterTemplate>
                    </asp:Repeater>
             </div>
        <asp:Timer ID="timMsg" runat="server" Interval="2000" Enabled="false" OnTick="timMsg_Tick" >
        </asp:Timer>
    </ContentTemplate>
</asp:UpdatePanel>
                </div>
</asp:Content>

