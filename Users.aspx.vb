
Partial Class Users
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        If Not Permissions.UsersEdit(Session("User_ID")) And Not Permissions.UsersClockGuard(Session("User_ID")) And Not Permissions.UsersMobileAccess(Session("User_ID")) Then
            Server.Transfer("UserPersonalEdit.aspx")
        End If

        Dim pnlMasterTabUsers As Panel = Master.FindControl("pnlTabUsers")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2ActiveUsers As Panel = Master.FindControl("pnlTab2ActiveUsers")

        pnlMasterTabUsers.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users
        pnlMasterTab2ActiveUsers.CssClass = "tab2 tab2On"


        litClockGuardPrice.Text = Constants.Pricing.ClockGuard
        litMobileAccessPrice.Text = Constants.Pricing.MobileAccess

        If Not IsPostBack Then
            If Not Request.Cookies("Subdomain") Is Nothing Then
                If Request.QueryString("reason") = "" Then
                    litMASubdomain.Text = Request.Cookies("Subdomain").Value
                Else
                    litMASubdomain.Text = "<em>subdomain</em>"
                End If
            Else
                litMASubdomain.Text = "<em>subdomain</em>"
            End If
            SearchSortUsersList(String.Empty, ddlSortUsersBy.SelectedItem.Text, ddlSortUsersOderBy.SelectedItem.Text)
        End If
    End Sub

    'Srini Mannem - 02/05/2011 - not required any more search and sort users action combining together
    'Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    CreateUsersList()
    '    txtUsersFilter.Focus()
    'End Sub

    Protected Sub ibSoryUsersBy_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        SearchSortUsersList(txtUsersFilter.Text, ddlSortUsersBy.SelectedItem.Text, ddlSortUsersOderBy.SelectedItem.Text)
        rptUsersList.Focus()
    End Sub

    'Srini Mannem - 02/05/2011 - not required any more search and sort users action combining together
    'Protected Sub CreateUsersList()
    '    Dim objDR As System.Data.SqlClient.SqlDataReader
    '    Dim objCommand As System.Data.SqlClient.SqlCommand
    '    Dim objConnection As System.Data.SqlClient.SqlConnection
    '    objConnection = New System.Data.SqlClient.SqlConnection

    '    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
    '    objConnection.Open()
    '    objCommand = New System.Data.SqlClient.SqlCommand()
    '    objCommand.Connection = objConnection
    '    objCommand.CommandText = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName, Office_ID, Manager, ClockGuard, MobileAccess FROM  Users WHERE Client_ID = @Client_ID AND Active = 1 AND (FirstName LIKE '%" & txtUsersFilter.Text & "%' OR LastName LIKE '%" & txtUsersFilter.Text & "%') ORDER BY FullName ASC"
    '    objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
    '    objDR = objCommand.ExecuteReader()

    '    rptUsersList.DataSource = objDR
    '    rptUsersList.DataBind()

    '    objDR = Nothing
    '    objCommand = Nothing
    '    objConnection.Close()
    '    objConnection = Nothing
    'End Sub

    Protected Sub SearchSortUsersList(ByVal strSearchUsersText As String, ByVal strUsersBy As String, ByVal strSortOrder As String)
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection
        Dim strSQLStatement As String = String.Empty
        Dim strSQLStatementForSearchUsers As String = String.Empty

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection

        If (String.IsNullOrEmpty(strSearchUsersText) = False) Then
            strSQLStatementForSearchUsers = strSQLStatementForSearchUsers & " AND (FirstName LIKE '%" & strSearchUsersText & "%' OR LastName LIKE '%" & strSearchUsersText & "%')"
        End If

        If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
            If (strUsersBy = "First Name") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, (FirstName + ', ' + LastName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, ClockGuard, MobileAccess FROM  Users WHERE Client_ID = @Client_ID AND Active = 1 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY FirstName"
            ElseIf (strUsersBy = "Last Name") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, ClockGuard, MobileAccess FROM  Users WHERE Client_ID = @Client_ID AND Active = 1 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY LastName"
            ElseIf (strUsersBy = "Office") Then
                strSQLStatement = strSQLStatement & "SELECT U.User_ID, (U.LastName + ', ' + U.FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, O.Office_ID, O.Name as 'OfficeName', U.Manager, U.ClockGuard, U.MobileAccess"
                strSQLStatement = strSQLStatement & " FROM Users U LEFT JOIN Offices O ON U.Client_ID = O.Client_ID AND U.Office_ID = O.Office_ID WHERE U.Client_ID = @Client_ID AND U.Active = 1 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY OfficeName"
            ElseIf (strUsersBy = "Manager") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, ClockGuard, MobileAccess FROM  Users WHERE Client_ID = @Client_ID AND Active = 1 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY Manager"
            ElseIf (strUsersBy = "Assigned Manager") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, ClockGuard, MobileAccess FROM  Users WHERE Client_ID = @Client_ID AND Active = 1 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY ManagerName"
            End If

            objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
        Else
            If (strUsersBy = "First Name") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, (FirstName + ', ' + LastName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, ClockGuard, MobileAccess FROM  Users WHERE Client_ID = @Client_ID AND Active = 1 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY FirstName"
            ElseIf (strUsersBy = "Last Name") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, ClockGuard, MobileAccess FROM  Users WHERE Client_ID = @Client_ID AND Active = 1 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY LastName"
            ElseIf (strUsersBy = "Office") Then
                strSQLStatement = strSQLStatement & "SELECT U.User_ID, (U.LastName + ', ' + U.FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, O.Office_ID, O.Name as 'OfficeName', U.Manager, U.ClockGuard, U.MobileAccess"
                strSQLStatement = strSQLStatement & " FROM Users U LEFT JOIN Offices O ON U.Client_ID = O.Client_ID AND U.Office_ID = O.Office_ID WHERE U.Client_ID = @Client_ID AND U.Active = 1 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY OfficeName"
            ElseIf (strUsersBy = "Manager") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, ClockGuard, MobileAccess FROM  Users WHERE Client_ID = @Client_ID AND Active = 1 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY Manager"
            ElseIf (strUsersBy = "Assigned Manager") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, ClockGuard, MobileAccess FROM  Users WHERE Client_ID = @Client_ID AND Active = 1 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY ManagerName"
            End If
        End If

        If (strSortOrder = "Ascending") Then
            strSQLStatement = strSQLStatement & " ASC"
        Else
            strSQLStatement = strSQLStatement & " DESC"
        End If

        objCommand.CommandText = strSQLStatement
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        rptUsersList.DataSource = objDR
        rptUsersList.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptUsersList_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblName As Label = e.Item.FindControl("lblName")
            Dim lblOffice As Label = e.Item.FindControl("lblOffice")
            Dim lblAssignedManager As Label = e.Item.FindControl("lblAssignedManager")
            Dim imgManager As Image = e.Item.FindControl("imgManager")
            Dim btnClockGuard As ImageButton = e.Item.FindControl("btnClockGuard")
            Dim btnMobileAccess As ImageButton = e.Item.FindControl("btnMobileAccess")
            Dim btnAbsence As ImageButton = e.Item.FindControl("btnAbsence")
            Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
            Dim btnPermissions As ImageButton = e.Item.FindControl("btnPermissions")
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

            lblName.Text = e.Item.DataItem("FullName")

            If e.Item.DataItem("Office_ID") Is DBNull.Value Then
                'if the office isnt set it causes an error, so do this check
                lblOffice.Text = ""
            Else
                lblOffice.Text = Offices.OfficeName(e.Item.DataItem("Office_ID"))
            End If

            If String.IsNullOrEmpty(e.Item.DataItem("ManagerName")) Then
                lblAssignedManager.Text = ""
            Else
                lblAssignedManager.Text = e.Item.DataItem("ManagerName")
            End If

            If e.Item.DataItem("Manager") Then
                imgManager.Visible = True
                imgManager.ToolTip = "header=[Manager] body=[]"
            End If

            If e.Item.DataItem("ClockGuard") Then
                If Permissions.UsersClockGuard(Session("User_ID")) Then
                    btnClockGuard.ImageUrl = "~/images/clockGuard_enabled.png"
                    btnClockGuard.AlternateText = "Click to Configure ClockGuard"
                    btnClockGuard.CommandName = "ConfigureClockGuard"
                    btnClockGuard.ToolTip = "header=[ClockGuard] body=[Click to Configure ClockGuard]"
                Else
                    btnClockGuard.ImageUrl = "~/images/check-green_small.png"
                    btnClockGuard.AlternateText = "Enabled"
                    btnClockGuard.ToolTip = "header=[ClockGuard] body=[Enabled]"
                    btnClockGuard.Enabled = False
                End If
            Else
                If Permissions.UsersClockGuard(Session("User_ID")) Then
                    btnClockGuard.ImageUrl = "~/images/clockGuard_disabled.png"
                    btnClockGuard.AlternateText = "Click to Enable ClockGuard"
                    btnClockGuard.ToolTip = "header=[ClockGuard] body=[Click to Enable ClockGuard]"
                    btnClockGuard.CommandName = "EnableClockGuard"
                Else
                    btnClockGuard.ImageUrl = "~/images/check-grey_small.png"
                    btnClockGuard.AlternateText = "Disabled"
                    btnClockGuard.ToolTip = "header=[ClockGuard] body=[Disabled]"
                    btnClockGuard.Enabled = False
                End If
            End If

            If e.Item.DataItem("MobileAccess") Then
                If Permissions.UsersMobileAccess(Session("User_ID")) Then
                    btnMobileAccess.ImageUrl = "~/images/mobileAccess_enabled.png"
                    btnMobileAccess.CommandName = "DisableMobileAccess"
                    btnMobileAccess.AlternateText = "Click to Disable Mobile Access"
                    btnMobileAccess.ToolTip = "header=[Mobile Access] body=[Click to Disable Mobile Access]"
                Else
                    btnMobileAccess.ImageUrl = "~/images/check-green_small.png"
                    btnMobileAccess.AlternateText = "Enabled"
                    btnMobileAccess.ToolTip = "header=[Mobile Access] body=[Enabled]"
                    btnMobileAccess.Enabled = False
                End If
            Else
                If Permissions.UsersMobileAccess(Session("User_ID")) Then
                    btnMobileAccess.ImageUrl = "~/images/mobileAccess_disabled.png"
                    btnMobileAccess.CommandName = "EnableMobileAccess"
                    btnMobileAccess.AlternateText = "Click to Enable Mobile Access"
                    btnMobileAccess.ToolTip = "header=[Mobile Access] body=[Click to Enable Mobile Access]"
                Else
                    btnMobileAccess.ImageUrl = "~/images/check-grey_small.png"
                    btnMobileAccess.AlternateText = "Disabled"
                    btnMobileAccess.Enabled = False
                    btnMobileAccess.ToolTip = "header=[Mobile Access] body=[Disabled]"
                End If
            End If

            btnClockGuard.CommandArgument = e.Item.DataItem("User_ID")
            btnMobileAccess.CommandArgument = e.Item.DataItem("User_ID")

            If e.Item.DataItem("Manager") And Permissions.UsersEditPermissions(Session("User_ID")) Then
                btnPermissions.CommandArgument = e.Item.DataItem("User_ID")
                btnPermissions.CommandName = "Permissions"
                btnPermissions.ToolTip = "offsetx=[-150] header=[Permissions] body=[]"
            Else
                btnPermissions.Enabled = False
                btnPermissions.AlternateText = ""
                btnPermissions.ImageUrl = "~/images/placeholder.png"
            End If

            If Permissions.UsersEdit(Session("User_ID")) Then
                btnEdit.CommandArgument = e.Item.DataItem("User_ID")
                btnEdit.CommandName = "EditUser"
                btnEdit.ToolTip = "offsetx=[-150] header=[Edit User] body=[]"
            Else
                btnEdit.Enabled = False
                btnEdit.AlternateText = ""
                btnEdit.ImageUrl = "~/images/placeholder.png"
            End If

            If e.Item.DataItem("User_ID") = Session("User_ID") Then
                btnDelete.Enabled = False
                btnDelete.AlternateText = ""
                btnDelete.ImageUrl = "~/images/placeholder.png"
            Else
                If Permissions.UsersDelete(Session("User_ID")) Then
                    btnDelete.CommandName = "DeleteUser"
                    btnDelete.CommandArgument = e.Item.DataItem("User_ID")
                    btnDelete.ToolTip = "offsetx=[-150] header=[Inactivate User] body=[]"
                Else
                    btnDelete.Enabled = False
                    btnDelete.AlternateText = ""
                    btnDelete.ImageUrl = "~/images/placeholder.png"
                End If
            End If
        End If
    End Sub

    Protected Sub rptUserList_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim strSQLQuery As String
        Dim boolRunSQL As Boolean = True
        Dim intUser_ID As Int64 = e.CommandArgument

        Select Case e.CommandName
            Case "EditUser"
                boolRunSQL = False
                Response.Redirect("UsersAdd.aspx?UserID=" & intUser_ID)
            Case "Permissions"
                boolRunSQL = False
                Response.Redirect("UserPermissions.aspx?user_ID=" & intUser_ID)
            Case "ConfigureClockGuard"
                boolRunSQL = False
                Response.Redirect("ClockGuard.aspx?user_id=" & intUser_ID)
            Case "EnableClockGuard"
                strSQLQuery = "UPDATE [Users] SET [ClockGuard] = 1 WHERE [User_ID] = @User_ID"
            Case "DisableMobileAccess"
                strSQLQuery = "UPDATE [Users] SET [MobileAccess] = 0 WHERE [User_ID] = @User_ID"
            Case "EnableMobileAccess"
                strSQLQuery = "UPDATE [Users] SET [MobileAccess] = 1 WHERE [User_ID] = @User_ID"
            Case "ClockGuardHelp"
                boolRunSQL = False
                If pnlClockGuardHelp.Visible Then
                    pnlClockGuardHelp.Visible = False
                Else
                    pnlClockGuardHelp.Visible = True
                    pnlMobileAccessHelp.Visible = False
                End If
            Case "MobileAccessHelp"
                boolRunSQL = False
                If pnlMobileAccessHelp.Visible Then
                    pnlMobileAccessHelp.Visible = False
                Else
                    pnlMobileAccessHelp.Visible = True
                    pnlClockGuardHelp.Visible = False
                End If
            Case "DeleteUser"
                Trace.Write("Users.aspx", "SQL Set to delete user = " & intUser_ID)
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "UPDATE TimeRequests SET Approved = 2, ApprovedDeniedBy = @Manager_ID WHERE User_ID = @User_ID AND Approved = 0"
                objCommand.Parameters.AddWithValue("@Manager_ID", Session("User_ID"))
                objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "DELETE FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL"
                objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                strSQLQuery = "UPDATE [Users] SET [Active] = 0, DeletedOn = '" & Date.Now() & "' WHERE [User_ID] = @User_ID"
            Case Else
                strSQLQuery = "else"
        End Select

        If boolRunSQL Then
            Trace.Write("Users.aspx", "Running SQL: " & strSQLQuery)
            Trace.Write("Users.aspx", "intUser_ID = " & intUser_ID)
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = strSQLQuery
            objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            SearchSortUsersList(txtUsersFilter.Text, ddlSortUsersBy.SelectedItem.Text, ddlSortUsersOderBy.SelectedItem.Text)
        End If

    End Sub

    Protected Sub btnCloseClockGuardHelp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlClockGuardHelp.Visible = False
    End Sub

    Protected Sub btnCloseMobileAccessHelp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlMobileAccessHelp.Visible = False
    End Sub
End Class
