<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SettingsOffices.aspx.vb" Inherits="SettingsOffices" title="TimeClockWizard - Offices" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

  <div class="main-content" style="width:1050px;">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="float: left; margin-left: 25px;">
                 
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
        <div style="width: 300px; text-align: center; background: #004a71; padding: 15px 0; margin-bottom: 20px;">
                    <asp:Panel ID="pnlAdd" runat="server">
                        <h4 style="padding: 12px 0;">Add New Office</h4>
                        <asp:TextBox ID="txtOfficeName" runat="server"></asp:TextBox>&nbsp;
                        <asp:ImageButton ID="btnAddOffice" runat="server" ImageAlign="absMiddle" ImageUrl="~/images/add_small.png" />
                        <br />
                        <asp:Label ID="lblAddMessage" runat="server" Text="Label" Visible="false" ForeColor="White"></asp:Label>
                    </asp:Panel>
                    </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                                    </div> <!-- end 1strow -->

 <div style="margin-left: 350px; width: 550px;">
                    <asp:Repeater ID="rptOfficeList" OnItemDataBound="rptOfficeList_ItemDataBound" OnItemCommand="rptOfficeList_ItemCommand" EnableViewState="True" runat="server">
                        <HeaderTemplate>
                            <ul class="officename" style="width:500px; text-align:left;">
<li class="column-head column-title">Office Name</li></ul>
                                       
<ul class="editlinks" style="width: 50px;">
 <li class="column-head column-title">&nbsp;</li> </ul>
                        </HeaderTemplate>
                        <ItemTemplate>
<ul class="officename" style="width:500px; text-align:left;">
<li><asp:Panel ID="pnlName" runat="server">
                                        <asp:Label ID="lblOfficeName" runat="server" Text="Label"></asp:Label>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlConfirmDelete" Visible="false" runat="server">
                                        Are you sure you want to delete the '<asp:Label ID="lblDeleteOfficeName" runat="server"></asp:Label>' office?  
                                        <asp:Button ID="btnConfirmDelete" runat="server" Text="Delete" />
                                        <asp:Button ID="btnCancelDelete" runat="server" Text="Cancel" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEdit" runat="server" Visible="false">
                                        <asp:TextBox ID="txtEditName" runat="server" Width="100">&nbsp;</asp:TextBox><asp:Label ID="lblEditMessage" runat="server" Visible="false"></asp:Label>
                                    </asp:Panel></li></ul>
                                    
<ul class="editlinks" style="width: 50px;">
 <li><asp:Panel ID="pnlNormalButtons" runat="server">
                                        <asp:ImageButton ID="imgEdit" CommandName="OfficeEdit" ImageUrl="~/images/edit_small.png" runat="server" />
                                        <asp:ImageButton ID="imgDelete" commandName="OfficeDelete" ImageUrl="~/images/delete_small.png" runat="server" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEditButtons" runat="server" Visible="false">
                                        <asp:ImageButton ID="btnEditSave" CommandName="EditSave" ImageUrl="~/images/save_small.png" runat="server" AlternateText="Save Changes" />
                                        <asp:ImageButton ID="btnEditCancel" commandName="EditCancel" ImageUrl="~/images/cancel_small.png" runat="server" AlternateText="Cancel Changes" />
                                    
                                    </asp:Panel>
 </li> </ul>                              
                        </ItemTemplate>
  
                        <FooterTemplate>
                            
                        </FooterTemplate>
                    </asp:Repeater>
          </div> 
    </ContentTemplate>
</asp:UpdatePanel>
            
        </div>
</asp:Content>

