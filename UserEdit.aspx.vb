Imports Microsoft.Practices.EnterpriseLibrary
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration


Partial Class UserEdit
    Inherits System.Web.UI.Page
    Dim arrCustomStatus(5) As Integer
    '0 = Not Enabled, 1 = Optional, 2 = Mandatory
    Dim intUserId
    Dim objUserInfo As New UserInfo
    Dim _db As Database = DatabaseFactory.CreateDatabase("default")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Permissions.UsersEdit(Session("User_ID")) Then
                Server.Transfer("UserPersonalEdit.aspx")
            End If
            Dim pnlMasterTabUsers As Panel = Master.FindControl("pnlTabUsers")
            Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
            pnlMasterTabUsers.CssClass = "tab tabOn"
            MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users
            intUserId = Request.QueryString("user_ID")
            litUsersName.Text = UserInfo.NameFromID(intUserId, 1)
            Trace.Warn("UsersEdit.aspx", "PageLoad()")
            'set tooltips
            imgHelpAllowedDeviation.ToolTip = "offsetx=[-200] cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Allowed Deviation] body=[The number of minutes that a user must clock in after their scheduled time to be considered late.<br><br>It determines being late or early for both clocking and clocking out.<br><br>For example, 0 means they must clock in exactly when scheduled and any deviation would result in being considered early or late.]"
            imageEmployeeWorksOvernight.ToolTip = "cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Allow User Work Overnight] body=[Checking this option allows to schedule user to work overnight.<br><br>e.g., Joe work hours are from tonight 10:00pm to tomorrow morning 6:00am.<br><br>When you uncheck this option, it will check user schedule to make sure user not working overnight. If any records found going forward from this week, it will not allow you to uncheck.]"

            Dim dsCustomFields As DataSet
            dsCustomFields = objUserInfo.sprocGetCustomFields(Convert.ToInt64(Session("Client_ID")))
            If dsCustomFields.Tables.Count > 0 Then
                If dsCustomFields.Tables(0).Rows.Count > 0 Then
                    If dsCustomFields.Tables(0).Rows(0)("Custom1Enabled") = True Then
                        If dsCustomFields.Tables(0).Rows(0)("Custom1Mandatory") = True Then
                            'set mandatory
                            pnlMandCust1.Visible = True
                            lblMandCust1.Text = dsCustomFields.Tables(0).Rows(0)("Custom1Name")
                            valMandCust1.Enabled = True
                            valMandCust1.ErrorMessage = dsCustomFields.Tables(0).Rows(0)("Custom1Name") & " is required<br>"
                            arrCustomStatus(1) = 2
                        Else
                            'set optional
                            pnlCust1.Visible = True
                            lblCust1.Text = dsCustomFields.Tables(0).Rows(0)("Custom1Name")
                            arrCustomStatus(1) = 1
                        End If
                    Else
                        arrCustomStatus(1) = 0
                    End If
                    If dsCustomFields.Tables(0).Rows(0)("Custom2Enabled") = True Then
                        If dsCustomFields.Tables(0).Rows(0)("Custom2Mandatory") = True Then
                            'set mandatory
                            pnlMandCust2.Visible = True
                            lblMandCust2.Text = dsCustomFields.Tables(0).Rows(0)("Custom2Name")
                            valMandCust2.Enabled = True
                            valMandCust2.ErrorMessage = dsCustomFields.Tables(0).Rows(0)("Custom2Name") & " is required<br>"
                            arrCustomStatus(2) = 2
                        Else
                            'set optional
                            pnlCust2.Visible = True
                            lblCust2.Text = dsCustomFields.Tables(0).Rows(0)("Custom2Name")
                            arrCustomStatus(2) = 1
                        End If
                    Else
                        arrCustomStatus(2) = 0
                    End If
                    If dsCustomFields.Tables(0).Rows(0)("Custom3Enabled") = True Then
                        If dsCustomFields.Tables(0).Rows(0)("Custom3Mandatory") = True Then
                            'set mandatory
                            pnlMandCust3.Visible = True
                            lblMandCust3.Text = dsCustomFields.Tables(0).Rows(0)("Custom3Name")
                            valMandCust3.Enabled = True
                            valMandCust3.ErrorMessage = dsCustomFields.Tables(0).Rows(0)("Custom3Name") & " is required<br>"
                            arrCustomStatus(3) = 2
                        Else
                            'set optional
                            pnlCust3.Visible = True
                            lblCust3.Text = dsCustomFields.Tables(0).Rows(0)("Custom3Name")
                            arrCustomStatus(3) = 1
                        End If
                    Else
                        arrCustomStatus(3) = 0
                    End If
                    If dsCustomFields.Tables(0).Rows(0)("Custom4Enabled") = True Then
                        If dsCustomFields.Tables(0).Rows(0)("Custom4Mandatory") = True Then
                            'set mandatory
                            pnlMandCust4.Visible = True
                            lblMandCust4.Text = dsCustomFields.Tables(0).Rows(0)("Custom4Name")
                            valMandCust4.Enabled = True
                            valMandCust4.ErrorMessage = dsCustomFields.Tables(0).Rows(0)("Custom4Name") & " is required<br>"
                            arrCustomStatus(4) = 2
                        Else
                            'set optional
                            pnlCust4.Visible = True
                            lblCust4.Text = dsCustomFields.Tables(0).Rows(0)("Custom4Name")
                            arrCustomStatus(4) = 1
                        End If
                    Else
                        arrCustomStatus(4) = 0
                    End If
                    If dsCustomFields.Tables(0).Rows(0)("Custom5Enabled") = True Then
                        If dsCustomFields.Tables(0).Rows(0)("Custom5Mandatory") = True Then
                            'set mandatory
                            pnlMandCust5.Visible = True
                            lblMandCust5.Text = dsCustomFields.Tables(0).Rows(0)("Custom5Name")
                            valMandCust5.Enabled = True
                            valMandCust5.ErrorMessage = dsCustomFields.Tables(0).Rows(0)("Custom5Name") & " is required<br>"
                            arrCustomStatus(5) = 2
                        Else
                            'set optional
                            pnlCust5.Visible = True
                            lblCust5.Text = dsCustomFields.Tables(0).Rows(0)("Custom5Name")
                            arrCustomStatus(5) = 1
                        End If
                    Else
                        arrCustomStatus(5) = 0
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            objUserInfo = Nothing
        End Try
        If Not IsPostBack Then
            loadForm()
            'loadForms()
        End If
    End Sub

    Protected Sub loadForm()
        Try
            Trace.Warn("UsersEdit.aspx", "execute LoadForm()")
            If CDbl(Session("User_ID")) = CDbl(intUserId) Then
                chkManager.Enabled = False
            End If
            Dim intCounter As Integer = Year(Clock.GetNow())
            Dim ltYear As ListItem
            While intCounter > 1899
                ltYear = New ListItem(intCounter, intCounter)
                drpYear.Items.Add(ltYear)
                intCounter = intCounter - 1
            End While
            Dim ds As New DataSet
            ds = objUserInfo.sprocGetUsersDetail(Convert.ToInt64(Session("Client_ID")), Convert.ToInt64(intUserId))
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    drpOffice.DataSource = ds.Tables(0)
                    drpOffice.DataTextField = "Name"
                    drpOffice.DataValueField = "Office_ID"
                    drpOffice.DataBind()
                End If
                Dim ltSelectNone As ListItem = New ListItem("None", 0)
                drpOffice.Items.Insert(0, ltSelectNone)
                If ds.Tables(1).Rows.Count > 0 Then
                    drpManagersList.DataSource = ds.Tables(1)
                    drpManagersList.DataTextField = "ManagerName"
                    drpManagersList.DataValueField = "Manager_User_ID"
                    drpManagersList.DataBind()
                End If
                Dim ltSelectManagerNone As ListItem = New ListItem("None", 0)
                drpManagersList.Items.Insert(0, ltSelectManagerNone)
                If (Not Permissions.AccountHolder(Session("User_ID"))) And Session("manager") Then
                    drpManagersList.Enabled = False
                End If
                If ds.Tables(2).Rows.Count > 0 Then
                    ViewState("Manager") = ds.Tables(2).Rows(0)("Manager")
                    txtUsername.Text = ds.Tables(2).Rows(0)("Username").ToString()
                    txtFirstName.Text = ds.Tables(2).Rows(0)("FirstName").ToString()
                    txtLastName.Text = ds.Tables(2).Rows(0)("LastName").ToString()
                    txtEmail.Text = ds.Tables(2).Rows(0)("Email").ToString()
                    ddlOT.SelectedValue = ds.Tables(2).Rows(0)("Overtime").ToString()
                    chkManager.Checked = ds.Tables(2).Rows(0)("Manager").ToString()
                    chkClockGuard.Checked = ds.Tables(2).Rows(0)("ClockGuard").ToString()
                    chkMobileAccess.Checked = ds.Tables(2).Rows(0)("MobileAccess").ToString()
                    txtAddress1.Text = ds.Tables(2).Rows(0)("HomeAddress1").ToString()
                    txtAddress2.Text = ds.Tables(2).Rows(0)("HomeAddress2").ToString()
                    txtCity.Text = ds.Tables(2).Rows(0)("City").ToString()
                    txtState.Text = ds.Tables(2).Rows(0)("State").ToString()
                    txtZip.Text = ds.Tables(2).Rows(0)("ZIPCode").ToString()
                    txtHomePhone.Text = ds.Tables(2).Rows(0)("HomePhone").ToString()
                    txtCellPhone.Text = ds.Tables(2).Rows(0)("CellPhone").ToString()
                    txtWorkPhone.Text = ds.Tables(2).Rows(0)("WorkPhone").ToString()
                    txtWorkExt.Text = ds.Tables(2).Rows(0)("WorkExt").ToString()
                    chkEmployeeWorksOvernight.Checked = ds.Tables(2).Rows(0)("OvernightEmployee")

                    If IsNumeric(ds.Tables(2).Rows(0)("Office_ID")) Then
                        drpOffice.SelectedValue = ds.Tables(2).Rows(0)("Office_ID")
                    End If

                    If IsNumeric(ds.Tables(2).Rows(0)("ManagerUserId")) Then
                        drpManagersList.SelectedValue = ds.Tables(2).Rows(0)("ManagerUserId")
                    End If

                    ddlCurrencyType.SelectedValue = ds.Tables(2).Rows(0)("CurrencyType").ToString()
                    txtWage.Text = ds.Tables(2).Rows(0)("Wage").ToString()
                    txtAllowedDeviation.Text = ds.Tables(2).Rows(0)("AllowedDeviation").ToString()
                    drpTimeDisplay.SelectedValue = ds.Tables(2).Rows(0)("TimeDisplay").ToString()
                    Dim dtBirthday As DateTime = ds.Tables(2).Rows(0)("Birthday")
                    drpMonth.SelectedValue = dtBirthday.Month
                    drpDay.SelectedValue = dtBirthday.Day
                    drpYear.SelectedValue = dtBirthday.Year

                    Trace.Warn("UserEdit.aspx", "Mandatory = " & arrCustomStatus(1))

                    If Not ds.Tables(2).Rows(0)("Custom1Value") Is DBNull.Value Then
                        If arrCustomStatus(1) = 2 Then
                            txtMandCust1.Text = ds.Tables(2).Rows(0)("Custom1Value").ToString()
                            Trace.Warn("UserEdit.aspx", "Mand Set")
                        Else
                            txtCust1.Text = ds.Tables(2).Rows(0)("Custom1Value").ToString()
                            Trace.Warn("UserEdit.aspx", "Optional Set")
                        End If
                    End If
                    If Not ds.Tables(2).Rows(0)("Custom2Value") Is DBNull.Value Then
                        If arrCustomStatus(2) = 2 Then
                            txtMandCust2.Text = ds.Tables(2).Rows(0)("Custom2Value").ToString()
                        Else
                            txtCust2.Text = ds.Tables(2).Rows(0)("Custom2Value").ToString()
                        End If
                    End If

                    If Not ds.Tables(2).Rows(0)("Custom3Value") Is DBNull.Value Then
                        If arrCustomStatus(3) = 2 Then
                            txtMandCust3.Text = ds.Tables(2).Rows(0)("Custom3Value").ToString()
                        Else
                            txtCust3.Text = ds.Tables(2).Rows(0)("Custom3Value").ToString()
                        End If
                    End If
                    If Not ds.Tables(2).Rows(0)("Custom4Value") Is DBNull.Value Then
                        If arrCustomStatus(4) = 2 Then
                            txtMandCust4.Text = ds.Tables(2).Rows(0)("Custom4Value").ToString()
                        Else
                            txtCust4.Text = ds.Tables(2).Rows(0)("Custom4Value").ToString()
                        End If
                    End If

                    If Not ds.Tables(2).Rows(0)("Custom5Value") Is DBNull.Value Then
                        If arrCustomStatus(5) = 2 Then
                            txtMandCust5.Text = ds.Tables(2).Rows(0)("Custom5Value").ToString()
                        Else
                            txtCust5.Text = ds.Tables(2).Rows(0)("Custom5Value").ToString()
                        End If
                    End If
                    ddlTimeZone.SelectedValue = ds.Tables(2).Rows(0)("TZOffset")
                    If Convert.ToDecimal(ddlTimeZone.SelectedValue) >= 0.0 Then
                        chkDST.Checked = False
                        chkDST.Enabled = False
                    Else
                        chkDST.Enabled = True
                        chkDST.Checked = ds.Tables(2).Rows(0)("DST")
                    End If
                    rbtnlistOverTime.SelectedValue = ds.Tables(2).Rows(0)("OverTimetype")
                    If rbtnlistOverTime.SelectedValue = "1" Then
                        divDailyhours.Visible = True
                        divWeeklyhours.Visible = False
                        ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(2).Rows(0)("OvertimeDailyHours"))
                        ddlWeeklyHours.SelectedValue = "40"
                    ElseIf rbtnlistOverTime.SelectedValue = "2" Then
                        divDailyhours.Visible = False
                        divWeeklyhours.Visible = True
                        ddlDailyHours.SelectedValue = "8"
                        ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(2).Rows(0)("OvertimeWeeklyHours"))
                    Else
                        divDailyhours.Visible = True
                        divWeeklyhours.Visible = True
                        ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(2).Rows(0)("OvertimeDailyHours"))
                        ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(2).Rows(0)("OvertimeWeeklyHours"))
                    End If
                    chkAproveOverTime.Checked = ds.Tables(2).Rows(0)("ApproveOvertime")
                End If
            End If
            If UserInfo.UserStatus(intUserId) <> 1 Then
                'they are clocked in, => cannot change timezone setting
                ddlTimeZone.Enabled = False
                chkDST.Enabled = False
                If Not IsPostBack Then
                    lblLoginText.ForeColor = Drawing.Color.Red
                    lblLoginText.Text = "<br><br>Note: Cannot edit TZ settings while user is clocked in "
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            objUserInfo = Nothing
        End Try
    End Sub

    Protected Sub loadForms()
        Trace.Warn("UsersEdit.aspx", "execute LoadForm()")
        If CDbl(Session("User_ID")) = CDbl(intUserId) Then
            chkManager.Enabled = False
        End If

        Dim intCounter As Integer = Year(Clock.GetNow())
        Dim ltYear As ListItem

        While intCounter > 1899
            ltYear = New ListItem(intCounter, intCounter)
            drpYear.Items.Add(ltYear)
            intCounter = intCounter - 1
        End While

        'for hire date
        '       intCounter = Year(Clock.GetNow())
        '       While intCounter > 1899
        'ltYear = New ListItem(intCounter, intCounter)
        '        ddlHireDateYear.Items.Add(ltYear)
        '       intCounter = intCounter - 1
        '        End While

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "EXEC sprocSelectAllOffices @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        drpOffice.DataSource = objDR
        drpOffice.DataTextField = "Name"
        drpOffice.DataValueField = "Office_ID"
        drpOffice.DataBind()

        Dim ltSelectNone As ListItem = New ListItem("None", 0)
        drpOffice.Items.Insert(0, ltSelectNone)

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "EXEC sprocSelectManagers @Client_ID, @User_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@User_ID", intUserId)
        objDR = objCommand.ExecuteReader()

        drpManagersList.DataSource = objDR
        drpManagersList.DataTextField = "ManagerName"
        drpManagersList.DataValueField = "Manager_User_ID"
        drpManagersList.DataBind()

        Dim ltSelectManagerNone As ListItem = New ListItem("None", 0)
        drpManagersList.Items.Insert(0, ltSelectManagerNone)

        'if not account holder and logged in user is manager, disable manager selection
        If (Not Permissions.AccountHolder(Session("User_ID"))) And Session("manager") Then
            drpManagersList.Enabled = False
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'all the forms have been initialized, now set default values
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM Users WHERE User_ID = @User_ID AND Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@User_Id", intUserId)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            ViewState("Manager") = objDR("Manager") 'put this in the viewstate as it needs to be used later for permissions

            txtUsername.Text = objDR("Username")
            txtFirstName.Text = objDR("FirstName")
            txtLastName.Text = objDR("LastName")
            txtEmail.Text = objDR("Email")
            ddlOT.SelectedValue = objDR("Overtime")
            chkManager.Checked = objDR("Manager")
            chkClockGuard.Checked = objDR("ClockGuard")
            chkMobileAccess.Checked = objDR("MobileAccess")
            txtAddress1.Text = objDR("HomeAddress1")
            txtAddress2.Text = objDR("HomeAddress2")
            txtCity.Text = objDR("City")
            txtState.Text = objDR("State")
            txtZip.Text = objDR("ZIPCode")
            txtHomePhone.Text = objDR("HomePhone")
            txtCellPhone.Text = objDR("CellPhone")
            txtWorkPhone.Text = objDR("WorkPhone")
            txtWorkExt.Text = objDR("WorkExt")
            chkEmployeeWorksOvernight.Checked = objDR("OvernightEmployee")

            If IsNumeric(objDR("Office_ID")) Then
                drpOffice.SelectedValue = objDR("Office_ID")
            End If

            If IsNumeric(objDR("ManagerUserId")) Then
                drpManagersList.SelectedValue = objDR("ManagerUserId")
            End If

            ddlCurrencyType.SelectedValue = objDR("CurrencyType")
            txtWage.Text = objDR("Wage")
            txtAllowedDeviation.Text = objDR("AllowedDeviation")

            drpTimeDisplay.SelectedValue = objDR("TimeDisplay").ToString

            Dim dtBirthday As DateTime = objDR("Birthday")

            drpMonth.SelectedValue = dtBirthday.Month
            drpDay.SelectedValue = dtBirthday.Day
            drpYear.SelectedValue = dtBirthday.Year

            '            Dim dtHireDate As DateTime = objDR("HireDate")

            '            ddlHireDateMonth.SelectedValue = dtHireDate.Month
            '            ddlHireDateDay.SelectedValue = dtHireDate.Day
            '            ddlHireDateYear.SelectedValue = dtHireDate.Year

            Trace.Warn("UserEdit.aspx", "Mandatory = " & arrCustomStatus(1))
            If Not objDR("Custom1Value") Is DBNull.Value Then
                If arrCustomStatus(1) = 2 Then
                    txtMandCust1.Text = objDR("Custom1Value")
                    Trace.Warn("UserEdit.aspx", "Mand Set")
                Else
                    txtCust1.Text = objDR("Custom1Value")
                    Trace.Warn("UserEdit.aspx", "Optional Set")
                End If
            End If

            If Not objDR("Custom2Value") Is DBNull.Value Then
                If arrCustomStatus(2) = 2 Then
                    txtMandCust2.Text = objDR("Custom2Value")
                Else
                    txtCust2.Text = objDR("Custom2Value")
                End If
            End If

            If Not objDR("Custom3Value") Is DBNull.Value Then
                If arrCustomStatus(3) = 2 Then
                    txtMandCust3.Text = objDR("Custom3Value")
                Else
                    txtCust3.Text = objDR("Custom3Value")
                End If
            End If

            If Not objDR("Custom4Value") Is DBNull.Value Then
                If arrCustomStatus(4) = 2 Then
                    txtMandCust4.Text = objDR("Custom4Value")
                Else
                    txtCust4.Text = objDR("Custom4Value")
                End If
            End If

            If Not objDR("Custom5Value") Is DBNull.Value Then
                If arrCustomStatus(5) = 2 Then
                    txtMandCust5.Text = objDR("Custom5Value")
                Else
                    txtCust5.Text = objDR("Custom5Value")
                End If
            End If
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
        objConnection = New SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT TZOffset, DST FROM Users WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserId)
        objDR = objCommand.ExecuteReader()

        objDR.Read()
        ddlTimeZone.SelectedValue = objDR("TZOffset")
        'if timezone is positive, disable DST option
        If Convert.ToDecimal(ddlTimeZone.SelectedValue) >= 0.0 Then
            chkDST.Checked = False
            chkDST.Enabled = False
        Else
            chkDST.Enabled = True
            chkDST.Checked = objDR("DST")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If UserInfo.UserStatus(intUserId) <> 1 Then
            'they are clocked in, => cannot change timezone setting
            ddlTimeZone.Enabled = False
            chkDST.Enabled = False

            If Not IsPostBack Then
                lblLoginText.ForeColor = Drawing.Color.Red
                lblLoginText.Text = "<br><br>Note: Cannot edit TZ settings while user is clocked in "
            End If
        End If
    End Sub
    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblLoginText.Visible = False
        loadForm()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim IsSaved As Int32 = 0
            Dim arrSQLCustomValue(5) As String
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Trace.Warn("UserEdit.aspx", "Case = '" & arrCustomStatus(1) & "'")
            Select Case arrCustomStatus(1)
                Case 0
                    arrSQLCustomValue(1) = ""
                Case 1
                    arrSQLCustomValue(1) = txtCust1.Text
                Case 2
                    arrSQLCustomValue(1) = txtMandCust1.Text
                Case Else
                    arrSQLCustomValue(1) = ""
            End Select
            Trace.Warn("UserEdit.aspx", "Input Value = '" & arrSQLCustomValue(1) & "'")
            Select Case arrCustomStatus(2)
                Case 0
                    arrSQLCustomValue(2) = ""
                Case 1
                    arrSQLCustomValue(2) = txtCust2.Text
                Case 2
                    arrSQLCustomValue(2) = txtMandCust2.Text
                Case Else
                    arrSQLCustomValue(2) = ""
            End Select
            Select Case arrCustomStatus(3)
                Case 0
                    arrSQLCustomValue(3) = ""
                Case 1
                    arrSQLCustomValue(3) = txtCust3.Text
                Case 2
                    arrSQLCustomValue(3) = txtMandCust3.Text
                Case Else
                    arrSQLCustomValue(3) = ""
            End Select
            Select Case arrCustomStatus(4)
                Case 0
                    arrSQLCustomValue(4) = ""
                Case 1
                    arrSQLCustomValue(4) = txtCust4.Text
                Case 2
                    arrSQLCustomValue(4) = txtMandCust4.Text
                Case Else
                    arrSQLCustomValue(4) = ""
            End Select
            Select Case arrCustomStatus(5)
                Case 0
                    arrSQLCustomValue(5) = ""
                Case 1
                    arrSQLCustomValue(5) = txtCust5.Text
                Case 2
                    arrSQLCustomValue(5) = txtMandCust5.Text
                Case Else
                    arrSQLCustomValue(5) = ""
            End Select
            Dim boolAdd As Boolean = True
            If UserInfo.ValidateEmail(txtEmail.Text) = False Then
                boolAdd = False
                lblLoginText.Text = "Please enter a valid email address"
            End If
            If txtAllowedDeviation.Text <> "" Then
                If Not IsNumeric(txtAllowedDeviation.Text) Then
                    boolAdd = False
                    lblLoginText.Text = "Please enter a valid number of minutes in the 'Allowed Deviation' field"
                End If
            End If
            Dim dblWage As Double
            If txtWage.Text <> "" Then
                If Not IsNumeric(Replace(txtWage.Text, "$", "")) Then
                    boolAdd = False
                    lblLoginText.Text = "Please enter a valid wage"
                Else
                    dblWage = CDbl(txtWage.Text)
                End If
            End If
            Dim strPassword As String
            If txtPassword.Text = "" Then
                Dim objCommand As New System.Data.SqlClient.SqlCommand()
                objCommand.CommandText = "SELECT Password FROM Users WHERE User_ID = @User_ID"
                objCommand.Parameters.AddWithValue("@User_ID", intUserId)
                objCommand.Parameters.AddWithValue("@Username", txtUsername.Text)
                objDR = _db.ExecuteReader(objCommand)
                If objDR.HasRows Then
                    objDR.Read()
                    strPassword = objDR("Password")
                End If
                objDR = Nothing
                objCommand = Nothing
            Else
                strPassword = Crypto.ComputeMD5Hash(txtPassword.Text)
            End If

            If boolAdd Then
                Dim objCommandEdit As New SqlCommand("sprocUpdateUsers")
                objCommandEdit.CommandType = CommandType.StoredProcedure
                objCommandEdit.Parameters.AddWithValue("@ClientID", Session("Client_ID"))
                objCommandEdit.Parameters.AddWithValue("@UserID", intUserId)
                objCommandEdit.Parameters.AddWithValue("@OfficeID", drpOffice.SelectedValue)
                Trace.Write("UsersEdit", "Value = " & drpOffice.SelectedValue & "Index = " & drpOffice.SelectedIndex)
                objCommandEdit.Parameters.AddWithValue("@Manager", chkManager.Checked)
                objCommandEdit.Parameters.AddWithValue("@TZOffset", ddlTimeZone.SelectedValue)
                objCommandEdit.Parameters.AddWithValue("@DST", chkDST.Checked)
                objCommandEdit.Parameters.AddWithValue("@Overtime", ddlOT.SelectedValue)
                objCommandEdit.Parameters.AddWithValue("@Username", txtUsername.Text)
                objCommandEdit.Parameters.AddWithValue("@Password", strPassword)
                objCommandEdit.Parameters.AddWithValue("@FirstName", txtFirstName.Text)
                objCommandEdit.Parameters.AddWithValue("@LastName", txtLastName.Text)
                objCommandEdit.Parameters.AddWithValue("@Email", txtEmail.Text)
                objCommandEdit.Parameters.AddWithValue("@HomePhone", txtHomePhone.Text)
                objCommandEdit.Parameters.AddWithValue("@CellPhone", txtCellPhone.Text)
                objCommandEdit.Parameters.AddWithValue("@WorkPhone", txtWorkPhone.Text)
                objCommandEdit.Parameters.AddWithValue("@WorkExt", txtWorkExt.Text)
                objCommandEdit.Parameters.AddWithValue("@HomeAddress1", txtAddress1.Text)
                objCommandEdit.Parameters.AddWithValue("@HomeAddress2", txtAddress2.Text)
                objCommandEdit.Parameters.AddWithValue("@City", txtCity.Text)
                objCommandEdit.Parameters.AddWithValue("@State", txtState.Text)
                objCommandEdit.Parameters.AddWithValue("@ZIPCode", txtZip.Text)
                objCommandEdit.Parameters.AddWithValue("@TimeDisplay", drpTimeDisplay.SelectedValue)
                Trace.Write("UsersEdit", "Birthday = " & drpMonth.SelectedValue & "/" & drpDay.SelectedValue & "/" & drpYear.SelectedValue)
                objCommandEdit.Parameters.AddWithValue("@Birthday", drpMonth.SelectedValue & "/" & drpDay.SelectedValue & "/" & drpYear.SelectedValue)
                objCommandEdit.Parameters.AddWithValue("@ClockGuard", chkClockGuard.Checked)
                objCommandEdit.Parameters.AddWithValue("@AllowedDeviation", txtAllowedDeviation.Text)
                objCommandEdit.Parameters.AddWithValue("@MobileAccess", chkMobileAccess.Checked)
                objCommandEdit.Parameters.AddWithValue("@CurrencyType", ddlCurrencyType.SelectedValue)
                objCommandEdit.Parameters.AddWithValue("@Wage", dblWage)
                objCommandEdit.Parameters.AddWithValue("@Custom1Value", arrSQLCustomValue(1))
                objCommandEdit.Parameters.AddWithValue("@Custom2Value", arrSQLCustomValue(2))
                objCommandEdit.Parameters.AddWithValue("@Custom3Value", arrSQLCustomValue(3))
                objCommandEdit.Parameters.AddWithValue("@Custom4Value", arrSQLCustomValue(4))
                objCommandEdit.Parameters.AddWithValue("@Custom5Value", arrSQLCustomValue(5))
                objCommandEdit.Parameters.AddWithValue("@OvernightEmployee", chkEmployeeWorksOvernight.Checked)
                objCommandEdit.Parameters.AddWithValue("@ManagerUserId", drpManagersList.SelectedValue)
                objCommandEdit.Parameters.AddWithValue("@OverTimetype", rbtnlistOverTime.SelectedValue)
                If rbtnlistOverTime.SelectedValue = 1 Then
                    objCommandEdit.Parameters.AddWithValue("@OvertimeDailyHours", ddlDailyHours.SelectedValue)
                ElseIf rbtnlistOverTime.SelectedValue = 2 Then
                    objCommandEdit.Parameters.AddWithValue("@OvertimeWeeklyHours", ddlWeeklyHours.SelectedValue)
                Else
                    objCommandEdit.Parameters.AddWithValue("@OvertimeDailyHours", ddlDailyHours.SelectedValue)
                    objCommandEdit.Parameters.AddWithValue("@OvertimeWeeklyHours", ddlWeeklyHours.SelectedValue)
                End If
                objCommandEdit.Parameters.AddWithValue("@ApproveOvertime", chkAproveOverTime.Checked)
                objCommandEdit.Parameters.AddWithValue("@IsSaved", 0)
                objCommandEdit.Parameters("@IsSaved").Direction = ParameterDirection.Output
                _db.ExecuteNonQuery(objCommandEdit)

                If Not IsDBNull(objCommandEdit.Parameters("@IsSaved").Value) Then
                    IsSaved = objCommandEdit.Parameters("@IsSaved").Value
                End If

                lblLoginText.ForeColor = Drawing.Color.Green
                lblLoginText.Text = "<br><br>User " & txtUsername.Text & " has been edited."

                objCommandEdit = Nothing
                If chkManager.Checked Then
                    If ViewState("Manager") <> True Then
                        'they are now a manager, and were not before
                        If Permissions.UsersAddManagerFull(Session("User_ID")) Then
                            Dim objCommand As New SqlCommand("sprocSaveUserPermissions")
                            'objCommand.CommandText = "INSERT INTO Permissions (User_Id, Client_ID) VALUES (@User_ID , @Client_ID)"
                            objCommand.CommandType = CommandType.StoredProcedure
                            objCommand.Parameters.AddWithValue("@ClientID", Session("Client_ID"))
                            objCommand.Parameters.AddWithValue("@UserID", intUserId)
                            _db.ExecuteNonQuery(objCommand)
                            objCommand = Nothing
                            Trace.Write("UsersAdd.aspx", "Permissions Added")

                        ElseIf Permissions.UsersAddManager(Session("User_ID")) Then
                            'execute the limited permissions add (sproc)
                            Dim objCommand As New SqlCommand("sprocInsertPermissions")
                            'objCommand.CommandText = "EXEC sprocInsertPermissions @User_ID, @Manager_ID"
                            objCommand.CommandType = CommandType.StoredProcedure
                            objCommand.Parameters.AddWithValue("@User_ID", intUserId)
                            objCommand.Parameters.AddWithValue("@Manager_ID", Session("User_ID"))
                            _db.ExecuteNonQuery(objCommand)
                            objCommand = Nothing
                            Trace.Write("UsersAdd.aspx", "Permissions Added (LIMITED)")
                        End If
                    End If
                Else
                    'they are not a manager, so delete permissions if they exist
                    Dim objCommand As New SqlCommand("sprocDeleteUsersPermissions")
                    'objCommand.CommandText = "DELETE FROM Permissions WHERE User_Id = @User_ID AND Client_Id = @Client_Id"
                    objCommand.CommandType = CommandType.StoredProcedure
                    objCommand.Parameters.AddWithValue("@ClientID", Session("Client_ID"))
                    objCommand.Parameters.AddWithValue("@UserID", intUserId)
                    _db.ExecuteNonQuery(objCommand)
                    objCommand = Nothing
                End If
                loadForm()
            Else
                lblLoginText.ForeColor = Drawing.Color.Red
                lblLoginText.Text = "<br><br>" & lblLoginText.Text
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlTimeZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTimeZone.SelectedIndexChanged
        'if timezone is positive, disable DST option
        If Convert.ToDecimal(ddlTimeZone.SelectedValue) >= 0.0 Then
            chkDST.Checked = False
            chkDST.Enabled = False
        Else
            chkDST.Enabled = True
            chkDST.Checked = False
        End If
    End Sub

    Protected Sub chkEmployeeWorksOvernight_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEmployeeWorksOvernight.CheckedChanged
        Trace.Write("Employee Works Overnite checkbox clicked")

        If Not chkEmployeeWorksOvernight.Checked Then
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "EXEC sprocCheckUserOKtoRemoveOverniteAttribute @Client_ID, @User_ID"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@User_ID", intUserId)
            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()
                If Convert.ToInt32(objDR("UserWorkingOvertime")) > 0 Then
                    chkEmployeeWorksOvernight.Checked = True
                    lblLoginText.ForeColor = Drawing.Color.Red
                    lblLoginText.Text = "<br><br>Note: Employee schedule indicates working overnite for one or more days!<br>Please adjust schedule first and try again!"
                End If
            Else
                lblLoginText.Text = String.Empty
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection = Nothing
        Else
            lblLoginText.Text = String.Empty
        End If
    End Sub
    Protected Sub rbtnlistOverTime_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rbtnlistOverTime.SelectedIndexChanged
        Try
            If rbtnlistOverTime.SelectedValue = 1 Then
                divDailyhours.Visible = True
                divWeeklyhours.Visible = False
            ElseIf rbtnlistOverTime.SelectedValue = 2 Then
                divWeeklyhours.Visible = True
                divDailyhours.Visible = False
            Else
                divWeeklyhours.Visible = True
                divDailyhours.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
