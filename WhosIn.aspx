<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WhosIn.aspx.vb" Inherits="WhosIn" title="TimeClockWizard - Who's In" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="main-content">
        <div style="margin-left: 25px;">
    <asp:Repeater ID="rptWhosIn" runat="server" OnItemDataBound="rptWhosIn_ItemDataBound">
    <HeaderTemplate>
<ul class="row-user">
<li class="column-head column-title">Name</li></ul>

<ul class="scheduled">
<li class="column-head column-title">Scheduled</li></ul>

<ul class="clockedin">
<li class="column-head column-title">Clocked In</li></ul>


<ul class="statuses">
<li class="column-head column-title">Status</li></ul>

<ul class="location">
<li class="column-head column-title">Location</li></ul>
 
<ul class="durations">
<li class="column-head column-title">Duration</li></ul>
          
        
    </HeaderTemplate>
    <ItemTemplate>
<ul class="row-user">
<li><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></li></ul>
                
<ul class="scheduled">
<li><asp:Label ID="lblScheduledTimeIn" runat="server" Text="Label"></asp:Label></li></ul>
                
<ul class="clockedin">
<li><asp:Label ID="lblTimeIn" runat="server" Text="Label"></asp:Label></li></ul>
                
<ul class="statuses">
<li><asp:Label ID="lblStatus" runat="server" Text="Label"></asp:Label></li></ul>
                
<ul class="location">
<li><asp:Image ID="imgStartLocation" runat="server" ImageAlign="AbsMiddle" /><asp:Label ID="lblStartLocation" runat="server" Text="Label"></asp:Label></li></ul>
                
<ul class="durations">
<li><asp:Label ID="lblDuration" runat="server" Text="Label"></asp:Label></li></ul>
                

    </ItemTemplate>
    <FooterTemplate>
       
    </FooterTemplate>
    </asp:Repeater>
            </div>
        </div>
</asp:Content>

