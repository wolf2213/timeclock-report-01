Imports Microsoft.VisualBasic

Public Class Constants
    Public Enum Tabs As Integer
        Timesheet = 0
        Schedule = 1
        Payroll = 2
        Users = 3
        Settings = 4
        News = 5
        Support = 6
    End Enum

    Public Enum BossTabs As Integer
        Timesheet = 0
        ClientSearch = 1
        Client = 2
    End Enum

    Public Enum Pricing As Long
        ClockGuard = 1
        MobileAccess = 1
        PhoneClockIn = 0.1
        User = 2
        Base = 4
    End Enum

    Public Enum Settings As Long
        InactivityTimeout = 900000
    End Enum

    Public Shared InactiveLogoutURL As String = "Logout.aspx?reason=inactive"

End Class
