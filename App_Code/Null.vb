Imports Microsoft.VisualBasic

Public Class Null
    Public Shared Function Replace(ByVal strInput, Optional ByVal strOutput = "") As String
        If strInput Is DBNull.Value Then
            Return strOutput
        Else
            'NOTE: This will cast value to string if
            'it isn't a string.

            Return strInput.ToString
        End If
    End Function
End Class
