Imports Microsoft.VisualBasic

Public Class Clock
    Public Shared Function ClockIn(ByVal intClientID As Int64, ByVal intUserID As Int64, ByVal boolManager As Boolean, ByVal strLocation As String, Optional ByVal boolMobileAccess As Boolean = False)
        'return int 1=>success
        'return int 2=>ERROR trying to clock in when the person is already clocked in [UserStatus is <> 1]
        'return int 3=>ERROR userStatus error.  [UserStatus is not numeric]
        HttpContext.Current.Trace.Write("Clock.ClockIn", "Begin Clock.ClockIn")

        Dim intUserStatus As String = UserInfo.UserStatus(intUserID)

        If IsNumeric(intUserStatus) Then
            If intUserStatus = 1 Then
                'currently clocked out, clock them in

                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                HttpContext.Current.Trace.Write("Clock.ClockIn", "Insert Clock In Record Begin at " & GetNow())
                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "INSERT INTO TimeRecords (User_ID, Client_ID, StartTime, StartLocation, StartLocation_ID, VerifiedBy) VALUES (@User_ID, @Client_ID, @StartTime, @StartLocation, @StartLocation_ID, @VerifiedBy)"
                objCommand.Parameters.AddWithValue("@User_ID", intUserID)
                objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
                objCommand.Parameters.AddWithValue("@StartTime", GetNow(0, True))
                objCommand.Parameters.AddWithValue("@StartLocation", strLocation)

                If Not boolMobileAccess Then
                    objCommand.Parameters.AddWithValue("@StartLocation_ID", Locations.LocationID(intClientID, strLocation))
                Else
                    objCommand.Parameters.AddWithValue("@StartLocation_ID", "98989898")
                End If

                If boolManager Then
                    objCommand.Parameters.AddWithValue("@VerifiedBy", intUserID)
                Else
                    objCommand.Parameters.AddWithValue("@VerifiedBy", 0)
                End If

                objCommand.ExecuteNonQuery()
                HttpContext.Current.Trace.Write("Clock.ClockIn", "Insert Clock In Record End")

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing


                'they are now clocked in
                'you must now check to see if they were tardy
                'DO THIS HERE BECAUSE THE RECORD NEEDS TO BE ADDED FIRST

                'the first step is to get the ID of the record they are clocked in under
                Dim intMostRecentTRID As Int64 = TimeRecords.GetMostRecentTimeID(intUserID)

                'now pass this to the tardy handler to see if there is a great enough deviation to add the record
                Dim intTardyType As Integer = Tardy.AddTardy(intMostRecentTRID, 1)

                'they are now clocked in and the tardies are handled
                'now see if there needs to be a ClockGuard alert sent
                ClockGuardClass.CheckToAlert(intUserID, intUserStatus, intTardyType)

                ClockIn = 1
                HttpContext.Current.Trace.Write("Clock.ClockIn", "Successful clock in")
            Else
                '2
                HttpContext.Current.Trace.Warn("Clock.ClockIn", "Type 2 Error Found (They are already clocked in)")
                ClockIn = 2
            End If
        Else
            '3
            ClockIn = 3
        End If
        HttpContext.Current.Trace.Write("Clock.ClockIn", "End Clock.ClockIn")
    End Function

    Public Shared Function ClockOut(ByVal intClientID As Int64, ByVal intUserID As Int64, ByVal boolManager As Boolean, ByVal strLocation As String, ByVal dtClockedIn As DateTime, Optional ByVal boolMobileAccess As Boolean = False)
        'return int 1 => success
        'return int 2 => record deleted [0 minutes]
        'return int 3 => ERROR cannot clock user out [they are not clocked in]
        'return int 4 => ERROR user stat is not numeric

        HttpContext.Current.Trace.Write("Clock.ClockOut", "Begin Clock.ClockOut")

        Dim intUserStatus As String = UserInfo.UserStatus(intUserID)
        Dim boolRecordDeleted As Boolean = False
        Dim boolPossibleError As Boolean = False

        If IsNumeric(intUserStatus) Then
            If intUserStatus = 2 Or intUserStatus = 3 Then
                'They are trying to clock out
                'the first step is to get the ID of the record they are clocked in under.  We have to do this before the EndTime is updated
                Dim intMostRecentTRID As Int64 = TimeRecords.GetMostRecentTimeID(intUserID)

                'clock them out

                Dim SQLQuery As String

                HttpContext.Current.Trace.Write("Clock.ClockOut", "dtClockedIn = " & dtClockedIn)

                Dim dtClockInTime As DateTime = dtClockedIn
                Dim dtCurrentTime As DateTime = GetNow(0, True)

                HttpContext.Current.Trace.Write("Clock.ClockOut", "dtClockInTime.Kind = " & dtClockInTime.Kind)
                HttpContext.Current.Trace.Write("Clock.ClockOut", "dtCurrentTime.Kind = " & dtCurrentTime.Kind)

                If DateDiff(DateInterval.Minute, dtClockInTime, dtCurrentTime) > 0 Then
                    SQLQuery = "UPDATE TimeRecords SET EndTime = @EndTime, EndLocation = @EndLocation, EndLocation_ID = @EndLocation_ID, PossibleError = @PossibleError, VerifiedBy = @VerifiedBy WHERE User_ID = @User_ID AND EndTime IS NULL"
                    HttpContext.Current.Trace.Write("Clock.ClockOut", "SQL set for update")
                    ClockOut = 1
                    If DateDiff(DateInterval.Minute, dtClockInTime, dtCurrentTime) > 720 Then
                        HttpContext.Current.Trace.Write("Clock.ClockOut", "DateDiff = " & DateDiff(DateInterval.Minute, dtClockedIn, GetNow(0, True)))
                        boolPossibleError = True
                    End If
                Else
                    Dim strAuditDescription As String = "Clockin Time: " & dtClockInTime.ToString() & ", Clockout Time: " & dtCurrentTime.ToString()
                    Audit.CreateNewClockitinAudit(Convert.ToInt64(HttpContext.Current.Session("Client_ID")), Convert.ToInt64(HttpContext.Current.Session("User_ID")), String.Empty, String.Empty, strAuditDescription)
                    SQLQuery = "DELETE FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL"
                    HttpContext.Current.Trace.Write("Clock.ClockOut", "SQL set for deletion [0 minutes]")
                    ClockOut = 2
                    boolRecordDeleted = True
                End If

                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = SQLQuery
                objCommand.Parameters.AddWithValue("@User_ID", intUserID)
                objCommand.Parameters.AddWithValue("@EndTime", GetNow(0, True))
                objCommand.Parameters.AddWithValue("@EndLocation", strLocation)

                If Not boolMobileAccess Then
                    objCommand.Parameters.AddWithValue("@EndLocation_ID", Locations.LocationID(intClientID, strLocation))
                Else
                    objCommand.Parameters.AddWithValue("@EndLocation_ID", "98989898")
                End If

                objCommand.Parameters.AddWithValue("@PossibleError", boolPossibleError)

                If boolManager And Not boolPossibleError Then
                    objCommand.Parameters.AddWithValue("@VerifiedBy", intUserID)
                Else
                    objCommand.Parameters.AddWithValue("@VerifiedBy", 0)
                End If

                objCommand.ExecuteNonQuery()
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                If Not boolRecordDeleted Then
                    'they are now clocked out
                    'see if they were tardy
                    Dim intTardyType As Integer = Tardy.AddTardy(intMostRecentTRID, 2)

                    'they are now clocked out and the tardies are handled
                    'now see if there needs to be a ClockGuard alert sent

                    ClockGuardClass.CheckToAlert(intUserID, intUserStatus, intTardyType)

                    'add accrued time to the applicable absence types


                    AbsencesClass.AccruePercentageTime(intUserID, DateDiff(DateInterval.Minute, dtClockedIn, GetNow(0, True)) / 60)
                Else
                    'check if there is a tardy record and delete it
                    Tardy.DeleteTardy(Tardy.CheckForTardy(intMostRecentTRID))
                End If

                'ClockOut Set Above
            Else
                HttpContext.Current.Trace.Warn("Clock.ClockOut", "Type 3 Error Found (They are not clocked in)")
                ClockOut = 3
            End If

        Else
            'there was an error, display it
            ClockOut = 4
        End If

        HttpContext.Current.Trace.Write("Clock.ClockOut", "End Clock.ClockOut")
    End Function

    Public Shared Function BreakIn(ByVal intClientID As Int64, ByVal intUserID As Int64, ByVal boolManager As Boolean, ByVal strLocation As String, Optional ByVal boolMobileAccess As Boolean = False)
        HttpContext.Current.Trace.Write("Clock.BreakIn", "Begin Clock.BreakIn")
        HttpContext.Current.Trace.Write("Clock.BreakIn", "Insert Break In Record Begin at " & GetNow(0, True))

        HttpContext.Current.Trace.Write("Clock.BreakIn", "Check to see whether or not time should be deducted for break and set the TYPE accordingly ")

        Dim intType As Integer

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT PaidBreaks FROM Preferences WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        If objDR("PaidBreaks") Then
            HttpContext.Current.Trace.Write("Clock.BreakIn", "The break is paid, set timerecord type = 3")
            intType = 3
        Else
            HttpContext.Current.Trace.Write("Clock.BreakIn", "The break is NOT paid, set timerecord type = 2")
            intType = 2
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        objConnection = New System.Data.SqlClient.SqlConnection
        HttpContext.Current.Trace.Write("Clock.BreakIn", "Insert Clock In Record Begin at " & GetNow(0, True))
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "INSERT INTO TimeRecords (User_ID, Client_ID, Type, StartTime, StartLocation, StartLocation_ID, VerifiedBy) VALUES (@User_ID, @Client_ID, @Type, @StartTime, @StartLocation, @StartLocation_ID, @VerifiedBy)"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommand.Parameters.AddWithValue("@Type", intType)
        objCommand.Parameters.AddWithValue("@StartTime", GetNow(0, True))
        objCommand.Parameters.AddWithValue("@StartLocation", strLocation)

        If Not boolMobileAccess Then
            objCommand.Parameters.AddWithValue("@StartLocation_ID", Locations.LocationID(intClientID, strLocation))
        Else
            objCommand.Parameters.AddWithValue("@StartLocation_ID", "98989898")
        End If

        If boolManager Then
            objCommand.Parameters.AddWithValue("@VerifiedBy", intUserID)
        Else
            objCommand.Parameters.AddWithValue("@VerifiedBy", 0)
        End If

        objCommand.ExecuteNonQuery()
        HttpContext.Current.Trace.Write("Clock.BreakIn", "Insert Clock In Record End")

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        BreakIn = 1
        HttpContext.Current.Trace.Write("Clock.BreakIn", "End Break In")
    End Function

    Public Shared Function BreakOut(ByVal intClientID As Int64, ByVal intUserID As Int64, ByVal boolManager As Boolean, ByVal strLocation As String, ByVal dtClockedIn As DateTime, Optional ByVal boolMobileAccess As Boolean = False)
        'return int 1 => success
        'return int 2 => record deleted [0 minutes]
        'return int 3 => ERROR cannot clock user out [they are not clocked in]
        'return int 4 => ERROR user stat is not numeric

        HttpContext.Current.Trace.Write("Clock.BreakOut", "Begin Clock.BreakOut")

        Dim intUserStatus As String = UserInfo.UserStatus(intUserID)
        Dim boolRecordDeleted As Boolean = False

        If IsNumeric(intUserStatus) Then
            If intUserStatus = 3 Then
                'They are trying to clock out
                'the first step is to get the ID of the record they are clocked in under.  We have to do this before the EndTime is updated
                Dim intMostRecentTRID As Int64 = TimeRecords.GetMostRecentTimeID(intUserID)

                'clock them out

                Dim SQLQuery As String

                If DateDiff(DateInterval.Minute, dtClockedIn, GetNow(0, True)) > 0 Then
                    SQLQuery = "UPDATE TimeRecords SET EndTime = @EndTime, EndLocation = @EndLocation, EndLocation_ID = @EndLocation_ID WHERE User_ID = @User_ID AND EndTime IS NULL AND Type <> 1"
                    HttpContext.Current.Trace.Write("Clock.BreakOut", "SQL set for update")
                    BreakOut = 1
                Else
                    Dim strAuditDescription As String = "Clockin Time: " & dtClockedIn.ToString() & ", Clockout Time (GetNow(0, True)): " & GetNow(0, True).ToString()
                    Audit.CreateNewClockitinAudit(Convert.ToInt64(HttpContext.Current.Session("Client_ID")), Convert.ToInt64(HttpContext.Current.Session("User_ID")), String.Empty, String.Empty, strAuditDescription)
                    SQLQuery = "DELETE FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL AND Type <> 1"
                    HttpContext.Current.Trace.Write("Clock.BreakOut", "SQL set for deletion [0 minutes]")
                    BreakOut = 2
                    boolRecordDeleted = True
                End If

                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = SQLQuery
                objCommand.Parameters.AddWithValue("@EndTime", GetNow(0, True))
                objCommand.Parameters.AddWithValue("@EndLocation", strLocation)
                objCommand.Parameters.AddWithValue("@EndLocation_ID", Locations.LocationID(intClientID, strLocation))
                objCommand.Parameters.AddWithValue("@User_ID", intUserID)

                objCommand.ExecuteNonQuery()
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

            Else
                HttpContext.Current.Trace.Warn("Clock.BreakOut", "Type 3 Error Found (They are not ON BREAK)")
                BreakOut = 3
            End If

        Else
            'there was an error, display it
            BreakOut = 4
        End If

        HttpContext.Current.Trace.Write("Clock.BreakOut", "End Clock.BreakOut")
    End Function

    Public Shared Function GetNow(Optional ByVal intUserID As Int64 = 0, Optional ByVal boolRound As Boolean = False) As DateTime
        HttpContext.Current.Trace.Write("Clock", "Begin GetNow")
        Dim dtTempGetNow As DateTime = Date.SpecifyKind(Now.ToUniversalTime, DateTimeKind.Local)

        If intUserID = 0 Then
            intUserID = HttpContext.Current.Session("User_ID")
        End If

        HttpContext.Current.Trace.Write("Clock.GetNow", "User ID = " & intUserID)

        Dim sngTZOffset As Single = 0
        Dim boolDST As Boolean = False

        If HttpContext.Current.Session("TZOffset") Is Nothing Then
            HttpContext.Current.Trace.Write("Clock.GetNow", "Prefs not cached, look them up")

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT TZOffset, DST FROM Users WHERE User_ID = @User_ID"
            objCommand.Parameters.AddWithValue("@User_ID", intUserID)

            objDR = objCommand.ExecuteReader()
            objDR.Read()

            sngTZOffset = objDR("TZOffset")
            boolDST = objDR("DST")

            HttpContext.Current.Session("TZOffset") = sngTZOffset
            HttpContext.Current.Session("DST") = boolDST

            objDR.Close()
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
            HttpContext.Current.Trace.Write("Clock.GetNow", "Variables set")
        Else
            HttpContext.Current.Trace.Write("Clock.GetNow", "Values are cached")
            sngTZOffset = HttpContext.Current.Session("TZOffset")
            boolDST = HttpContext.Current.Session("DST")
            HttpContext.Current.Trace.Write("Clock.GetNow", "Variables set")
        End If

        HttpContext.Current.Trace.Write("Clock.GetNow", "dtTempGetNow =  " & dtTempGetNow)
        HttpContext.Current.Trace.Write("Clock.GetNow", "boolDST =  " & boolDST)
        HttpContext.Current.Trace.Write("Clock.GetNow", "sngTZOffset =  " & sngTZOffset)
        HttpContext.Current.Trace.Write("Clock.GetNow", "Add " & sngTZOffset * 60 & " Minutes")

        dtTempGetNow = dtTempGetNow.AddMinutes(sngTZOffset * 60)

        HttpContext.Current.Trace.Write("Clock.GetNow", "Pre-DST Time = " & dtTempGetNow)
        HttpContext.Current.Trace.Write("Clock.GetNow", "IS DST =  " & dtTempGetNow.IsDaylightSavingTime)

        If dtTempGetNow.IsDaylightSavingTime And boolDST Then
            HttpContext.Current.Trace.Write("Clock.GetNow", "Currently is DST, Add an hour")
            dtTempGetNow = DateAdd(DateInterval.Hour, 1, dtTempGetNow)
        End If
        HttpContext.Current.Trace.Write("Clock.GetNow", "Post-DST Time = " & dtTempGetNow)

        Dim timeNow As String
        Dim dblMinuteFraction As Double

        If dtTempGetNow.Second < 30 Then
            timeNow = dtTempGetNow.ToShortTimeString
            dblMinuteFraction = 0
        Else
            timeNow = DateAdd(DateInterval.Minute, 1, dtTempGetNow).ToShortTimeString
            dblMinuteFraction = -0.5
        End If

        dtTempGetNow = dtTempGetNow.Date & " " & timeNow
        'These lines allow you to set your own output for debugging, DO NOT DELETE!!!!!!!!!!!!
        'dtTempGetNow = dtTempGetNow.Date & " 6:44 PM"
        'dblMinuteFraction = -0.5

        HttpContext.Current.Trace.Write("Clock.GetNow", "Removed seconds = " & dtTempGetNow)
        HttpContext.Current.Trace.Write("Clock.GetNow", "Minutes = " & dtTempGetNow.Minute + dblMinuteFraction)

        If boolRound Then
            HttpContext.Current.Trace.Write("Clock.GetNow", "Rounding requested, checking if rounding is enabled")

            If HttpContext.Current.Session("TimeRounding") Is Nothing Then
                HttpContext.Current.Trace.Write("Clock.GetNow", "TimeRounding preference not cached, looking it up")
                Dim objDR2 As System.Data.SqlClient.SqlDataReader
                Dim objCommand2 As System.Data.SqlClient.SqlCommand
                Dim objConnection2 As System.Data.SqlClient.SqlConnection
                objConnection2 = New System.Data.SqlClient.SqlConnection

                objConnection2.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection2.Open()
                objCommand2 = New System.Data.SqlClient.SqlCommand()
                objCommand2.Connection = objConnection2
                objCommand2.CommandText = "SELECT [TimeRounding] FROM [Preferences] WHERE Client_ID = @Client_ID"
                objCommand2.Parameters.AddWithValue("@Client_ID", HttpContext.Current.Session("Client_ID"))

                objDR2 = objCommand2.ExecuteReader()
                objDR2.Read()

                HttpContext.Current.Session("TimeRounding") = objDR2("TimeRounding")

                objDR2.Close()
                objCommand2 = Nothing
                objConnection2.Close()
                objConnection2 = Nothing
            Else
                HttpContext.Current.Trace.Write("Clock.GetNow", "TimeRounding preference is cached")
            End If

            Select Case HttpContext.Current.Session("TimeRounding")
                Case 0
                    HttpContext.Current.Trace.Write("Clock.GetNow", "Rounding is disabled")
                Case 1
                    HttpContext.Current.Trace.Write("Clock.GetNow", "Rounding is set to 1/4 hour (15 minute rule)")
                    Dim Clock As New Clock
                    dtTempGetNow = Clock.Round15Minute(dtTempGetNow, dblMinuteFraction)
                Case 2
                    HttpContext.Current.Trace.Write("Clock.GetNow", "Rounding is set to 1/10 hour (6 minute rule)")
                    Dim Clock As New Clock
                    dtTempGetNow = Clock.Round6Minute(dtTempGetNow, dblMinuteFraction)
            End Select


        End If

        HttpContext.Current.Trace.Write("Clock.GetNow", "Returning " & dtTempGetNow)

        GetNow = dtTempGetNow

        HttpContext.Current.Trace.Write("Clock", "End GetNow")
    End Function

    Protected Function Round15Minute(ByVal dtNow As DateTime, ByVal dblMinuteFraction As Double) As DateTime
        HttpContext.Current.Trace.Write("Clock", "Begin Round15Minute")
        Dim intMinutesToChange As Integer = 0
        Dim dtDesired As DateTime
        Dim dblCurrentMinute As Double = dtNow.Minute + dblMinuteFraction

        Select Case dblCurrentMinute
            Case Is <= 7.5
                HttpContext.Current.Trace.Write("Clock.Round15Minute", "Round to :00")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "0", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is <= 22.5
                HttpContext.Current.Trace.Write("Clock.Round15Minute", "Round to :15")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "15", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is <= 36.5
                HttpContext.Current.Trace.Write("Clock.Round15Minute", "Round to :30")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "30", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is <= 52.5
                HttpContext.Current.Trace.Write("Clock.Round15Minute", "Round to :45")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "45", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Else
                HttpContext.Current.Trace.Write("Clock.Round15Minute", "Round to :00 next hour")
                'Strip the minutes, to make adding easier
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "0", "0")
                dtNow = dtDesired
                'Add one hour for real desired time
                dtDesired = DateAdd(DateInterval.Hour, 1, dtDesired)
                intMinutesToChange = 60
        End Select

        HttpContext.Current.Trace.Write("Clock.Round15Minute", "Desired date/time is " & dtDesired)
        HttpContext.Current.Trace.Write("Clock.Round15Minute", "Requred change is " & intMinutesToChange & " minutes")

        Round15Minute = DateAdd(DateInterval.Minute, intMinutesToChange, dtNow)

        HttpContext.Current.Trace.Write("Clock", "End Round15Minute")
    End Function

    Protected Function Round6Minute(ByVal dtNow As DateTime, ByVal dblMinuteFraction As Double) As DateTime
        HttpContext.Current.Trace.Write("Clock", "Begin Round6Minute")
        Dim intMinutesToChange As Integer = 0
        Dim dtDesired As DateTime
        Dim dblCurrentMinute As Double = dtNow.Minute + dblMinuteFraction

        Select Case dblCurrentMinute
            Case Is < 3.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :00")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "0", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is < 9.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :06")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "6", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is < 15.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :12")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "12", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is < 21.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :18")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "18", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is < 27.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :24")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "24", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is < 33.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :30")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "30", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is < 39.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :36")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "36", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is < 45.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :42")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "42", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is < 51.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :48")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "48", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Is < 57.5
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :54")
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "54", "0")
                intMinutesToChange = DateDiff(DateInterval.Minute, dtNow, dtDesired)
            Case Else
                HttpContext.Current.Trace.Write("Clock.Round6Minute", "Round to :00 next hour")
                'Strip the minutes, to make adding easier
                dtDesired = DateSerial(dtNow.Year, dtNow.Month, dtNow.Day) & " " & TimeSerial(dtNow.Hour, "0", "0")
                dtNow = dtDesired
                'Add one hour for real desired time
                dtDesired = DateAdd(DateInterval.Hour, 1, dtDesired)
                intMinutesToChange = 60
        End Select

        HttpContext.Current.Trace.Write("Clock.Round6Minute", "Desired date/time is " & dtDesired)
        HttpContext.Current.Trace.Write("Clock.Round6Minute", "Requred change is " & intMinutesToChange & " minutes")

        Round6Minute = DateAdd(DateInterval.Minute, intMinutesToChange, dtNow)

        HttpContext.Current.Trace.Write("Clock", "End Round6Minute")
    End Function

    Public Shared Function CheckUserAllowedToClockInBeforeScheduleTime(ByVal intClientID As Int64, ByVal intUserID As Int64) As Boolean
        HttpContext.Current.Trace.Write("Clock", "Begin CheckUserAllowedToClockInBeforeScheduleTime")

        CheckUserAllowedToClockInBeforeScheduleTime = False

        Dim objDRClient As System.Data.SqlClient.SqlDataReader
        Dim objCommandClient As System.Data.SqlClient.SqlCommand
        Dim objConnectionClient As System.Data.SqlClient.SqlConnection

        objConnectionClient = New System.Data.SqlClient.SqlConnection
        objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnectionClient.Open()
        objCommandClient = New System.Data.SqlClient.SqlCommand()
        objCommandClient.Connection = objConnectionClient
        objCommandClient.CommandText = "EXEC sprocCheckUserCanWorkBeforeScheduleWorkTime @Client_ID, @User_ID, @Year, @WeekNum"
        objCommandClient.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommandClient.Parameters.AddWithValue("@User_ID", intUserID)
        objCommandClient.Parameters.AddWithValue("@Year", DatePart(DateInterval.Year, Today()))
        objCommandClient.Parameters.AddWithValue("@WeekNum", DatePart(DateInterval.WeekOfYear, Today()))

        objDRClient = objCommandClient.ExecuteReader()

        If objDRClient.HasRows Then
            objDRClient.Read()

            CheckUserAllowedToClockInBeforeScheduleTime = objDRClient("UserPermissionToWorkBeforeScheduleTime")
        End If

        objDRClient = Nothing
        objCommandClient = Nothing
        objConnectionClient.Close()
        objConnectionClient = Nothing

        HttpContext.Current.Trace.Write("Clock", "End CheckUserAllowedToClockInBeforeScheduleTime")
    End Function

    Public Shared Function CheckUserAllowedToClockOutAfterScheduleTime(ByVal intClientID As Int64, ByVal intUserID As Int64, ByRef dtScheduleEndTime As DateTime) As Boolean
        HttpContext.Current.Trace.Write("Clock", "Begin CheckUserAllowedToClockOutAfterScheduleTime")

        CheckUserAllowedToClockOutAfterScheduleTime = False

        Dim objDRClient As System.Data.SqlClient.SqlDataReader
        Dim objCommandClient As System.Data.SqlClient.SqlCommand
        Dim objConnectionClient As System.Data.SqlClient.SqlConnection

        objConnectionClient = New System.Data.SqlClient.SqlConnection
        objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnectionClient.Open()
        objCommandClient = New System.Data.SqlClient.SqlCommand()
        objCommandClient.Connection = objConnectionClient
        objCommandClient.CommandText = "EXEC sprocCheckUserCanWorkAfterScheduleWorkTime @Client_ID, @User_ID, @Year, @WeekNum"
        objCommandClient.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommandClient.Parameters.AddWithValue("@User_ID", intUserID)
        objCommandClient.Parameters.AddWithValue("@Year", DatePart(DateInterval.Year, Today()))
        objCommandClient.Parameters.AddWithValue("@WeekNum", DatePart(DateInterval.WeekOfYear, Today()))

        objDRClient = objCommandClient.ExecuteReader()

        If objDRClient.HasRows Then
            objDRClient.Read()

            CheckUserAllowedToClockOutAfterScheduleTime = objDRClient("UserPermissionToWorkAfterScheduleTime")
            If (Not CheckUserAllowedToClockOutAfterScheduleTime) Then
                dtScheduleEndTime = objDRClient("ScheduleEndTime")
            End If
        End If

        objDRClient = Nothing
        objCommandClient = Nothing
        objConnectionClient.Close()
        objConnectionClient = Nothing

        HttpContext.Current.Trace.Write("Clock", "End CheckUserAllowedToClockOutAfterScheduleTime")
    End Function

    Public Shared Sub ClockOutOverride(ByVal intMostRecentTRID As Int64, ByVal dtClockedOutOverride As DateTime)

        HttpContext.Current.Trace.Write("Clock.ClockOutOverride", "Begin Clock.ClockOutOverride")

        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE TimeRecords SET EndTime = @EndTime WHERE Time_ID = @Time_ID"
        objCommand.Parameters.AddWithValue("@Time_ID", intMostRecentTRID)
        objCommand.Parameters.AddWithValue("@EndTime", dtClockedOutOverride)

        objCommand.ExecuteNonQuery()
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("Clock.ClockOutOverride", "End Clock.ClockOutOverride")
    End Sub
End Class