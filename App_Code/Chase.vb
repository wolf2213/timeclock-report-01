Imports Microsoft.VisualBasic
Imports System.Net.Mail

Public Class Chase
    ''' <summary>
    ''' Creates a new transaction and stores a new profile with Chase.
    ''' </summary>
    ''' <param name="intClientID">Client ID to be billed.</param>
    ''' <param name="strCardNumber">Credit card number.</param>
    ''' <param name="strCardExpire">Credit card expiration date in mm/yyyy format.</param>
    ''' <param name="strCardVerify">Credit card verification number.</param>
    ''' <param name="strCardName">Name on credit card.</param>
    ''' <param name="dblCharge">Amount to be charged.</param>
    ''' <returns>Chase transaction ID for success, Error### and message for failure.</returns>
    ''' <remarks></remarks>
    Public Shared Function NewTransactionNewProfile(ByVal intClientID As Int64, ByVal strCardNumber As String, ByVal strCardExpire As String, ByVal strCardVerify As Integer, ByVal strCardName As String, ByVal dblCharge As Double) As String
        Dim crClass As New ChaseRemote.SendTransaction

        Try
            NewTransactionNewProfile = crClass.NewTransactionNewProfile(intClientID, strCardNumber, strCardExpire, strCardVerify, strCardName, dblCharge)
        Catch er As Exception
            NewTransactionNewProfile = "Unable to contact payment server"
        End Try
    End Function

    ''' <summary>
    ''' Creates a new transaction with Chase.
    ''' </summary>
    ''' <param name="intClientID">Client ID to be billed.</param>
    ''' <param name="dblCharge">Amount to be charged.</param>
    ''' <returns>Chase transaction ID for success, Error### and message for failure.</returns>
    ''' <remarks></remarks>
    Public Shared Function NewTransaction(ByVal intClientID As Int64, ByVal dblCharge As Double) As String
        Dim crClass As New ChaseRemote.SendTransaction

        Try
            NewTransaction = crClass.NewTransaction(intClientID, dblCharge)
        Catch er As Exception
            NewTransaction = "Unable to contact payment server"
        End Try
    End Function

    ''' <summary>
    ''' Updates an existing profile with Chase.
    ''' </summary>
    ''' <param name="intClientID">Client ID to be updated.</param>
    ''' <param name="strCardNumber">New credit card number.</param>
    ''' <param name="strCardExpire">New credit card expiration date in mm/yyyy format.</param>
    ''' <param name="strCardName">New name on credit card.</param>
    ''' <returns>Client ID for success, Error### and message for failure.</returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateProfile(ByVal intClientID As Int64, ByVal strCardNumber As String, ByVal strCardExpire As String, ByVal strCardName As String) As String
        Dim crClass As New ChaseRemote.SendTransaction

        Try
            UpdateProfile = crClass.UpdateProfile(intClientID, strCardNumber, strCardExpire, strCardName)
        Catch er As Exception
            UpdateProfile = "Unable to contact payment server"
        End Try
    End Function

    ''' <summary>
    ''' Records a transaction in the Billing and CCTransactions tables after it has been sent to Chase.
    ''' </summary>
    ''' <param name="intClientID">Client ID that was billed.</param>
    ''' <param name="strTxnRefNum">Chase transaction ID for success, or error message for failsure.</param>
    ''' <param name="dblAmount">Amount that was billed.</param>
    ''' <param name="strDescription">Description of the charge.</param>
    ''' <remarks></remarks>
    Public Shared Sub RecordTransaction(ByVal intClientID As Int64, ByVal strTxnRefNum As String, ByVal dblAmount As Double, ByVal strDescription As String)
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "INSERT INTO CCTransactions (Chase_ID, Success, Client_ID, Submitted, Amount) VALUES (@Chase_ID, 1, @Client_ID, @Submitted, @Amount)"
        objCommand.Parameters.AddWithValue("@Chase_ID", strTxnRefNum)
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommand.Parameters.AddWithValue("@Submitted", Now())
        objCommand.Parameters.AddWithValue("@Amount", dblAmount)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        'TODO: once Chase webservices are working, uncomment the code
        'objCommand.CommandText = "SELECT Transaction_ID FROM CCTransactions WHERE Chase_ID = @Chase_ID"
        'objCommand.Parameters.AddWithValue("@Chase_ID", strTxnRefNum)
        objCommand.CommandText = "SELECT Top 1 Transaction_ID FROM CCTransactions WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        'TODO: End

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        Dim intTransactionID As Int64 = objDR("Transaction_ID")

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "INSERT INTO Billing (Transaction_ID, Client_ID, BilledOn, Type, Description, Price) VALUES (@Transaction_ID, @Client_ID, @BilledOn, 5, @Description, @Price)"
        objCommand.Parameters.AddWithValue("@Transaction_ID", intTransactionID)
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommand.Parameters.AddWithValue("@BilledOn", Now())
        objCommand.Parameters.AddWithValue("@Description", strDescription)
        objCommand.Parameters.AddWithValue("@Price", dblAmount)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    ''' <summary>
    ''' Creates line items in the Billing table for a specific client.
    ''' </summary>
    ''' <param name="intClientID">Client ID to be processed.</param>
    ''' <returns>Total amount to be billed, -1 for error.</returns>
    ''' <remarks></remarks>
    Public Shared Function CreateLineItems(ByVal intClientID As Int64)
        Dim boolBillChase As Boolean = False
        Try
            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Begin Chase.CreateLineItems")
            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Processing Cleint_ID = " & intClientID)
            'get the up-to-date prices
            Dim dblPricingBase As Double = Constants.Pricing.Base
            Dim dblPricingUser As Double = Constants.Pricing.User
            Dim dblPricingClockGuard As Double = Constants.Pricing.ClockGuard
            Dim dblPricingMobileAccess As Double = Constants.Pricing.MobileAccess

            'create the variables that will be used in the sub
            Dim dblUserCharge As Double
            Dim dblClockGuardCharge As Double
            Dim dblMobileAccessCharge As Double
            Dim intNumUsers As Integer
            Dim intNumMA As Integer
            Dim intNumCG As Integer

            'find the values of the rows that need to be added
            Dim objDRClient As System.Data.SqlClient.SqlDataReader
            Dim objCommandClient As System.Data.SqlClient.SqlCommand
            Dim objConnectionClient As System.Data.SqlClient.SqlConnection

            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            objCommandClient.CommandText = "SELECT MaxUsers, MaxClockGuard, MaxMobileAccess FROM Clients WHERE Client_ID = @Client_ID"
            objCommandClient.Parameters.AddWithValue("@Client_ID", intClientID)

            objDRClient = objCommandClient.ExecuteReader()
            objDRClient.Read()

            intNumUsers = objDRClient("MaxUsers")
            intNumMA = objDRClient("MaxMobileAccess")
            intNumCG = objDRClient("MaxClockGuard")

            dblUserCharge = (intNumUsers * dblPricingUser)
            dblMobileAccessCharge = intNumMA * dblPricingMobileAccess
            dblClockGuardCharge = intNumCG * dblPricingClockGuard

            objDRClient = Nothing
            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing

            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "User Charge = " & dblUserCharge)
            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "ClockGuard Charge = " & dblClockGuardCharge)
            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "MobileAccess Charge = " & dblMobileAccessCharge)

            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Num Users = " & intNumUsers)
            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Num CG = " & intNumMA)
            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Num MA = " & intNumCG)

            'insert the billing rows
            Dim dblNewAmount As Double
            Dim intTransactionID As Int64

            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            objCommandClient.CommandText = "EXEC sprocCreateBillingRecords @Client_ID, @BilledOn, @BaseCost, @UserCost, @MobileAccessCost, @ClockGuardCost, @NumUsers, @NumMA, @NumCG, 0"
            objCommandClient.Parameters.AddWithValue("@Client_ID", intClientID)
            objCommandClient.Parameters.AddWithValue("@BilledOn", Now())
            objCommandClient.Parameters.AddWithValue("@BaseCost", dblPricingBase)
            objCommandClient.Parameters.AddWithValue("@UserCost", dblUserCharge)
            objCommandClient.Parameters.AddWithValue("@MobileAccessCost", dblClockGuardCharge)
            objCommandClient.Parameters.AddWithValue("@ClockGuardCost", dblMobileAccessCharge)
            objCommandClient.Parameters.AddWithValue("@NumUsers", intNumUsers)
            objCommandClient.Parameters.AddWithValue("@NumMA", intNumMA)
            objCommandClient.Parameters.AddWithValue("@NumCG", intNumCG)

            objDRClient = objCommandClient.ExecuteReader()
            objDRClient.Read()

            dblNewAmount = objDRClient("NewAmount")
            intTransactionID = objDRClient("Transaction_ID")

            objDRClient = Nothing
            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing

            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Initial New Amount = " & dblNewAmount)
            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Transaction ID = " & intTransactionID)
            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Begin Credit Check")

            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            objCommandClient.CommandText = "SELECT * FROM Credits WHERE Client_ID = @Client_ID AND AmountRemaining > 0 ORDER BY AddedOn ASC"
            objCommandClient.Parameters.AddWithValue("@Client_ID", intClientID)
            objDRClient = objCommandClient.ExecuteReader()

            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "BEGIN CREDIT WHILE LOOP")

            While objDRClient.Read And dblNewAmount > 0
                HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Process Credit ID = " & objDRClient("Credit_ID"))
                Dim strTotalCreditAmount As String = objDRClient("Amount")
                Dim dblCreditAmount As Double
                Dim dblNewRemainingAmount As Double

                dblCreditAmount = 0

                If objDRClient("Type") = 1 Then
                    'dollar discount
                    dblCreditAmount = objDRClient("AmountRemaining")
                    HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Dollar Discount = $" & dblCreditAmount)
                    strTotalCreditAmount = Strings.FormatCurrency(strTotalCreditAmount, 2)
                Else
                    'percentage discount
                    dblCreditAmount = (objDRClient("AmountRemaining") / 100) * dblNewAmount
                    HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Percentage Discount = %" & dblCreditAmount)
                    strTotalCreditAmount = strTotalCreditAmount & "% off"
                End If

                dblNewAmount -= dblCreditAmount

                HttpContext.Current.Trace.Write("Chase.CreateLineItems", "New CCTxn Amount = " & dblNewAmount)

                Dim objCommandRecurring As System.Data.SqlClient.SqlCommand
                Dim objConnectionRecurring As System.Data.SqlClient.SqlConnection

                If objDRClient("Recurring") Then
                    HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Recurring")
                    If objDRClient("RecurringMonths") <> 0 Then
                        'decrement recurring remaining column
                        HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Finite Recurring")
                        objConnectionRecurring = New System.Data.SqlClient.SqlConnection
                        objConnectionRecurring.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                        objConnectionRecurring.Open()
                        objCommandRecurring = New System.Data.SqlClient.SqlCommand()
                        objCommandRecurring.Connection = objConnectionRecurring
                        objCommandRecurring.CommandText = "UPDATE Credits SET AmountRemaining = @AmountRemaining, RecurringRemaining = @RecurringRemaining WHERE Credit_ID = @Credit_ID"

                        If objDRClient("RecurringRemaining") = 1 Then
                            objCommandRecurring.Parameters.AddWithValue("@AmountRemaining", 0)
                            objCommandRecurring.Parameters.AddWithValue("@RecurringRemaining", 0)
                            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Last recurrance, recurringRemaining and AmountRemaining set to 0")
                        Else
                            objCommandRecurring.Parameters.AddWithValue("@AmountRemaining", objDRClient("AmountRemaining"))
                            objCommandRecurring.Parameters.AddWithValue("@RecurringRemaining", objDRClient("RecurringRemaining") - 1)
                            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Not the last recurrance RecurringRemaining reduced by 1")
                        End If
                        objCommandRecurring.Parameters.AddWithValue("@Credit_ID", objDRClient("Credit_ID"))
                        objCommandRecurring.ExecuteNonQuery()

                        objCommandRecurring = Nothing
                        objConnectionRecurring.Close()
                        objConnectionRecurring = Nothing

                    End If
                Else
                    If objDRClient("Type") = 1 Then
                        HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Non Recurring Dollar Amount")
                        If dblNewAmount >= 0 Then
                            dblNewRemainingAmount = 0
                        Else
                            dblNewRemainingAmount = Math.Abs(dblNewAmount)
                        End If
                        'set the new flat amount remaining

                        objConnectionRecurring = New System.Data.SqlClient.SqlConnection
                        objConnectionRecurring.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                        objConnectionRecurring.Open()
                        objCommandRecurring = New System.Data.SqlClient.SqlCommand()
                        objCommandRecurring.Connection = objConnectionRecurring
                        objCommandRecurring.CommandText = "UPDATE Credits SET AmountRemaining = @AmountRemaining WHERE Credit_ID = @Credit_ID"
                        objCommandRecurring.Parameters.AddWithValue("@AmountRemaining", dblNewRemainingAmount)
                        objCommandRecurring.Parameters.AddWithValue("@Credit_ID", objDRClient("Credit_ID"))
                        objCommandRecurring.ExecuteNonQuery()

                        objCommandRecurring = Nothing
                        objConnectionRecurring.Close()
                        objConnectionRecurring = Nothing
                    Else
                        HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Non Recurring Percentage")
                        'this is a one-time percentage.  Set AmountRemaining = 0 to stop this credit
                        objConnectionRecurring = New System.Data.SqlClient.SqlConnection
                        objConnectionRecurring.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                        objConnectionRecurring.Open()
                        objCommandRecurring = New System.Data.SqlClient.SqlCommand()
                        objCommandRecurring.Connection = objConnectionRecurring
                        objCommandRecurring.CommandText = "UPDATE Credits SET AmountRemaining = 0 WHERE Credit_ID = @Credit_ID"
                        objCommandRecurring.Parameters.AddWithValue("@Credit_ID", objDRClient("Credit_ID"))
                        objCommandRecurring.ExecuteNonQuery()

                        objCommandRecurring = Nothing
                        objConnectionRecurring.Close()
                        objConnectionRecurring = Nothing
                    End If
                End If

                If dblNewAmount < 0 Then
                    dblNewAmount = 0
                End If

                HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Final dblNewAmount = " & dblNewAmount)

                'add the credit to the billing table

                HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Insert Credit Into Billing Table")
                objConnectionRecurring = New System.Data.SqlClient.SqlConnection
                objConnectionRecurring.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnectionRecurring.Open()
                objCommandRecurring = New System.Data.SqlClient.SqlCommand()
                objCommandRecurring.Connection = objConnectionRecurring
                objCommandRecurring.CommandText = "INSERT INTO Billing (Transaction_ID, Client_ID, Credit_ID, BilledOn, Description, Price) VALUES (@Transaction_ID, @Client_ID, @Credit_ID, @BilledOn, @Description, @Price)"
                objCommandRecurring.Parameters.AddWithValue("@Transaction_ID", intTransactionID)
                objCommandRecurring.Parameters.AddWithValue("@Client_ID", objDRClient("Client_ID"))
                objCommandRecurring.Parameters.AddWithValue("@Credit_ID", objDRClient("Credit_ID"))
                objCommandRecurring.Parameters.AddWithValue("@BilledOn", Now())
                objCommandRecurring.Parameters.AddWithValue("@Description", Left(objDRClient("Description"), 50))
                objCommandRecurring.Parameters.AddWithValue("@Price", dblCreditAmount - dblNewRemainingAmount)
                objCommandRecurring.ExecuteNonQuery()

                objCommandRecurring = Nothing
                objConnectionRecurring.Close()
                objConnectionRecurring = Nothing

                HttpContext.Current.Trace.Write("Chase.CreateLineItems", "Update CC Transaction")
                objConnectionRecurring = New System.Data.SqlClient.SqlConnection
                objConnectionRecurring.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnectionRecurring.Open()
                objCommandRecurring = New System.Data.SqlClient.SqlCommand()
                objCommandRecurring.Connection = objConnectionRecurring
                objCommandRecurring.CommandText = "UPDATE CCTransactions SET Amount = @Amount WHERE Transaction_ID = @Transaction_ID"
                objCommandRecurring.Parameters.AddWithValue("@Amount", dblNewAmount)
                objCommandRecurring.Parameters.AddWithValue("@Transaction_ID", intTransactionID)
                objCommandRecurring.ExecuteNonQuery()

                objCommandRecurring = Nothing
                objConnectionRecurring.Close()
                objConnectionRecurring = Nothing

            End While

            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "END CREDIT WHILE LOOP")

            objDRClient = Nothing
            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing

            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "New rows added to billing table")

            CreateLineItems = FormatNumber(dblNewAmount, 2)

            HttpContext.Current.Trace.Write("Chase.CreateLineItems", "End Chase.CreateLineItems")

        Catch ex As Exception
            Dim ErrorMail As New MailMessage()

            'set the addresses
            ErrorMail.From = New MailAddress("chase@TimeClockWizard.com", "TimeClockWizard Recurring Billing")
            ErrorMail.Subject = "TimeClockWizard Billing Error"
            ErrorMail.To.Add("info@TimeClockWizard.com")

            'set the content
            ErrorMail.Body = "Error Messsage: " & ex.Message & "<br>Stack Trace: " & ex.StackTrace & "<br>Client_ID: " & intClientID
            ErrorMail.IsBodyHtml = True

            'send the message
            Dim Errorsmtp As New SmtpClient()
            Errorsmtp.Send(ErrorMail)

            HttpContext.Current.Trace.Warn("Chase.CreateLineItems", "Error in executing code.  Warning Emails Sent")

            CreateLineItems = -1
        End Try
    End Function

    ''' <summary>
    ''' Determines whether a client is delinquent, and if so, how long they have been delinquent.
    ''' </summary>
    ''' <param name="intClientID">Client ID to check.</param>
    ''' <returns>8 for clients who are not delinquent, 0-7 for clients who are delinquent.</returns>
    ''' <remarks></remarks>
    Public Shared Function Delinquency(ByVal intClientID As Int64) As Integer
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT (7 - DATEDIFF(d, Submitted, GetDate())) AS DelinquentDays FROM CCTransactions WHERE Client_ID = @Client_ID AND Success = 0"
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        objDR = objCommand.ExecuteReader()

        objDR.Read()

        If Not objDR.HasRows Then
            Delinquency = 8
        Else
            If objDR("DelinquentDays") >= 0 Then
                Delinquency = objDR("DelinquentDays")
            Else
                Delinquency = 0
            End If
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Function

    ''' <summary>
    ''' Converts integers stored in the Billing table's Type field to a friendly name.
    ''' </summary>
    ''' <param name="intType">Integer representing a billing type.</param>
    ''' <returns>Billing type name, or Unknown for failure.</returns>
    ''' <remarks></remarks>
    Public Shared Function BillingTypeToName(ByVal intType As Integer) As String
        Select Case intType
            Case 0
                BillingTypeToName = "Credit"
            Case 1
                BillingTypeToName = "Base"
            Case 2
                BillingTypeToName = "Users"
            Case 3
                BillingTypeToName = "ClockGuard"
            Case 4
                BillingTypeToName = "MobileAccess"
            Case 5
                BillingTypeToName = "Misc"
            Case Else
                BillingTypeToName = "Unknown"
        End Select
    End Function

    ''' <summary>
    ''' Stores simplified client billing info after it has been sent to and validated by Chase.
    ''' </summary>
    ''' <param name="intClientID">Client ID to store.</param>
    ''' <param name="dtNextBillingDate">The next date the client will be billed on.</param>
    ''' <param name="intCCType">Credit card type: 1-Visa, 2-MasterCard, 3-AmEx, 4-Discover.</param>
    ''' <param name="intCCLastFour">Credit card last four digits.</param>
    ''' <param name="dtCCExpDate">Credit card expiration date.</param>
    ''' <remarks></remarks>
    Public Shared Sub StoreBillingInfo(ByVal intClientID As Int64, ByVal dtNextBillingDate As DateTime, ByVal intCCType As Integer, ByVal intCCLastFour As Integer, ByVal dtCCExpDate As DateTime)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE Clients SET [Trial] = 0, [NextBillingDate] = @NextBillingDate, [CCType] = @CCType, [CCLastFour] = @CCLastFour, [CCExpDate] = @CCExpDate, [MaxUsers] = [CurrentUsers], [MaxClockGuard] = [CurrentClockGuard], [MaxMobileAccess] = [CurrentMobileAccess] WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@NextBillingDate", dtNextBillingDate)
        objCommand.Parameters.AddWithValue("@CCType", intCCType)
        objCommand.Parameters.AddWithValue("@CCLastFour", intCCLastFour)
        objCommand.Parameters.AddWithValue("@CCExpDate", dtCCExpDate)
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)

        objCommand.ExecuteNonQuery()
    End Sub

    ''' <summary>
    ''' Updates simplified client billing info after it has been sent to and validated by Chase.
    ''' </summary>
    ''' <param name="intClientID">Client ID to store.</param>
    ''' <param name="intCCType">Credit card type: 1-Visa, 2-MasterCard, 3-AmEx, 4-Discover.</param>
    ''' <param name="intCCLastFour">Credit card last four digits.</param>
    ''' <param name="dtCCExpDate">Credit card expiration date.</param>
    ''' <remarks></remarks>
    Public Shared Sub UpdateBillingInfo(ByVal intClientID As Int64, ByVal intCCType As Integer, ByVal intCCLastFour As Integer, ByVal dtCCExpDate As DateTime)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE Clients SET [CCType] = @CCType, [CCLastFour] = @CCLastFour, [CCExpDate] = @CCExpDate WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@CCType", intCCType)
        objCommand.Parameters.AddWithValue("@CCLastFour", intCCLastFour)
        objCommand.Parameters.AddWithValue("@CCExpDate", dtCCExpDate)
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)

        objCommand.ExecuteNonQuery()
    End Sub
End Class
