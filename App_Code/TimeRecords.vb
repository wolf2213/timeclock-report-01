Imports Microsoft.VisualBasic

Public Class TimeRecords
    Public Shared Function TimeDifference(ByVal dtmStartTime, ByVal dtmEndTime) As Integer
        'this function takes two times and calculates a difference in Minutes
        If Not IsDate(dtmStartTime) Then
            dtmStartTime = CDate(Clock.GetNow())
        End If

        If Not IsDate(dtmEndTime) Then
            dtmEndTime = CDate(Clock.GetNow())
        End If

        TimeDifference = DateDiff(DateInterval.Minute, dtmStartTime, dtmEndTime)

        If TimeDifference < 0 Then
            TimeDifference = 0
        End If
        'TimeDifference = 0
    End Function

    Public Shared Function FormatMinutes(ByVal intMinutes As Integer, ByVal intUserID As Int64, ByVal boolAddSuffix As Boolean)

        Dim intRemainderMinutes As Integer = intMinutes Mod 60
        Dim intHours As Integer = Fix(intMinutes / 60)
        Dim strLabel As String

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT timeDisplay FROM Users WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        HttpContext.Current.Trace.Write("FormatMinutes", "TimeDisplay Preference Looked up for User_Id = " & intUserID)
        Dim timeDisplay As Integer = objDR("timeDisplay")

        If timeDisplay Then
            'display in decimals
            FormatMinutes = FormatNumber(intMinutes / 60, 2)
            strLabel = " hours"
            If FormatMinutes = 1 Then
                strLabel = " hour"
            End If
        Else
            Dim strRemainderMinutes As String = intRemainderMinutes.ToString
            If intRemainderMinutes <= 9 Then
                strRemainderMinutes = "0" & strRemainderMinutes
            End If
            FormatMinutes = intHours & ":" & strRemainderMinutes
            strLabel = ""
        End If

        objDR = Nothing
        objConnection.Close()
        objConnection = Nothing
        objCommand = Nothing

        'if the label is true then a label will be added 
        If boolAddSuffix Then
            FormatMinutes = FormatMinutes & strLabel
        End If
    End Function

    Public Shared Function TRClass(ByVal strNormalAlternate As String, ByVal intVerified_ID As Int64, Optional ByVal intManual_ID As Int64 = 1) As String
        'Checks if the record is verified and/or manually added, and sets the TR CSS class accordingly
        If Not intVerified_ID = 0 Then
            TRClass = "tr" & strNormalAlternate
        Else
            If intManual_ID = 0 Then
                TRClass = "trNotVerified" & strNormalAlternate
            Else
                TRClass = "trNotVerifiedManual" & strNormalAlternate
            End If
        End If
    End Function

    Public Shared Function GetMostRecentTimeID(ByVal intUserID As Int64) As Int64
        HttpContext.Current.Trace.Write("GetMostRecentTimeID", "Begin TimeRecords.GetMostRecentTimeID")
        HttpContext.Current.Trace.Write("GetMostRecentTimeID", "intUserId = " & intUserID)

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Time_ID FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            GetMostRecentTimeID = objDR("Time_ID")
            HttpContext.Current.Trace.Write("GetMostRecentTimeID", "Time_ID is found, = " & objDR("Time_ID"))
        Else
            GetMostRecentTimeID = 0
            HttpContext.Current.Trace.Write("GetMostRecentTimeID", "Time_ID is NOT found, = 0")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("GetMostRecentTimeID", "End TimeRecords.GetMostRecentTimeID")
    End Function
End Class
