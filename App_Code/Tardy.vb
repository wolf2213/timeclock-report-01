Imports Microsoft.VisualBasic

Public Class Tardy
    Public Shared Function AddTardy(ByVal intTimeID As Int64, ByVal intIn1OrOut2 As Integer) As Integer
        'this function is called when someone clocks in and out and determines whether or not a tardy record needs to be added
        'it returns the tardy type, if any:
        '0 - not tardy
        '1 - in early
        '2 - in late
        '3 - out early
        '4 - out late
        'if intInOrOut = 1 then they are clocking in
        'if intInOrOut = 2 then they are clocking out

        HttpContext.Current.Trace.Write("AddTardy", "Begin Tardy.AddTardy")
        HttpContext.Current.Trace.Write("AddTardy", "intTimeID = " & intTimeID)

        Dim strSQLQuery As String
        Dim intClientID As Int64
        Dim intUserID As Int64
        Dim dtmDBTime As DateTime
        Dim strScheduleSuffix As String

        Select Case intIn1OrOut2
            Case 1
                strSQLQuery = "SELECT Client_ID, User_ID, StartTime AS DBTime FROM TimeRecords WHERE Time_ID = @Time_ID"
                strScheduleSuffix = "Start"
            Case 2
                strSQLQuery = "SELECT Client_ID, User_ID, EndTime AS DBTime FROM TimeRecords WHERE Time_ID = @Time_ID"
                strScheduleSuffix = "End"
            Case Else
                strSQLQuery = ""
                strScheduleSuffix = ""
        End Select

        HttpContext.Current.Trace.Write("AddTardy", "TimeRecord Query")

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@Time_ID", intTimeID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            HttpContext.Current.Trace.Write("AddTardy", "TimeRecord Found")
            objDR.Read()
            intClientID = objDR("Client_ID")
            intUserID = objDR("User_ID")
            dtmDBTime = objDR("DBTime")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'get the allowed deviation
        Dim intAllowedDeviation As Integer

        HttpContext.Current.Trace.Write("AddTardy", "Deviation Query")

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT AllowedDeviation FROM Users WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            HttpContext.Current.Trace.Write("AddTardy", "Deviation Found")
            objDR.Read()
            intAllowedDeviation = objDR("AllowedDeviation")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'you now have the basic information (Start Time, Allowed Deviation, Client ID, User ID, Time ID)
        'check the schedule, calculate the difference
        'if > than the allowed deviation => add a tardy record

        Dim intDifference As Integer
        Dim intWeekNum As Integer = DatePart(DateInterval.WeekOfYear, Date.Now)
        Dim intYear As Integer = DatePart(DateInterval.Year, Date.Now)
        Dim strDayPrefix As String = Time.WeekDayName(DatePart(DateInterval.Weekday, Date.Now), 0)
        Dim strScheduleCodeCol As String = strDayPrefix & "Code"
        Dim strScheduleTimeCol As String = strDayPrefix & strScheduleSuffix

        HttpContext.Current.Trace.Write("AddTardy", "Schedule Query")

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT " & strScheduleCodeCol & " AS DayCode, " & strScheduleTimeCol & " AS ScheduledTime FROM Schedules WHERE User_ID = @User_ID AND WeekNum = @WeekNum AND Year = @Year"
        objCommand.Parameters.AddWithValue("@WeekNum", intWeekNum)
        objCommand.Parameters.AddWithValue("@Year", intYear)
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            HttpContext.Current.Trace.Write("AddTardy", strScheduleTimeCol & " Found")
            objDR.Read()

            If Not objDR("DayCode") Is DBNull.Value Then
                HttpContext.Current.Trace.Write("AddTardy", "DayCode Is Not Null")
                If IsNumeric(objDR("DayCode")) Then
                    'they have a scheduled time
                    HttpContext.Current.Trace.Write("AddTardy", "DayCode Is Numeric, User Is Scheduled To Work")
                    Dim dtStartTime As DateTime = Convert.ToDateTime(objDR("ScheduledTime"))

                    intDifference = DateDiff(DateInterval.Minute, dtStartTime, Clock.GetNow())
                    HttpContext.Current.Trace.Write("AddTardy", "intDifference = " & intDifference)

                    Dim boolEarly As Boolean = intDifference < 0
                    HttpContext.Current.Trace.Write("AddTardy", "boolEarly = " & boolEarly)

                    Dim intClockGaurdAllowedTime As Int32 = ClockGuardClass.getClockGaurdAllowedTime(intUserID, boolEarly, intIn1OrOut2)
                    'if user clock gaurd allowed time set by manager is greater than user allowed deviation set by manager, use clock gaurd allowed time 
                    'as allowed deviation
                    If intClockGaurdAllowedTime > intAllowedDeviation Then
                        intAllowedDeviation = intClockGaurdAllowedTime
                    End If
                    Dim intDeviation As Integer = intAllowedDeviation - Math.Abs(intDifference)
                    HttpContext.Current.Trace.Write("AddTardy", "intDeviation = " & intDeviation)

                    If intDeviation < 0 Then
                        'Allowed deviation was exceeded, a tardy will be added
                        HttpContext.Current.Trace.Write("AddTardy", "Allowed Deviation Exceeded")
                        If boolEarly Then
                            'User deviated early
                            HttpContext.Current.Trace.Write("AddTardy", "Deviation Early")
                            If intIn1OrOut2 = 1 Then
                                'User is clocking in
                                HttpContext.Current.Trace.Write("AddTardy", "Type 1 - In Early")
                                AddTardy = 1
                            Else
                                'User is clocking out
                                HttpContext.Current.Trace.Write("AddTardy", "Type 3 - Out Early")
                                AddTardy = 3
                            End If
                        Else
                            'User deviated late
                            HttpContext.Current.Trace.Write("AddTardy", "Deviation Late")
                            If intIn1OrOut2 = 1 Then
                                'User is clocking in
                                HttpContext.Current.Trace.Write("AddTardy", "Type 2 - In Late")
                                AddTardy = 2
                            Else
                                'User is clocking out
                                HttpContext.Current.Trace.Write("AddTardy", "Type 4 - Out Late")
                                AddTardy = 4
                            End If
                        End If
                    Else
                        'Allowed deviation was not exceeded
                        HttpContext.Current.Trace.Write("AddTardy", "Allowed Deviation Not Exceeded")
                        AddTardy = 0
                    End If
                Else
                    'No scheduled time
                    HttpContext.Current.Trace.Write("AddTardy", "DayCode Is Not Numeric, User Is Not Scheduled")
                    AddTardy = 0
                End If
            Else
                'DayCode Is Null
                HttpContext.Current.Trace.Write("AddTardy", "DayCode Is Null")
                AddTardy = 0
            End If
        Else
            'Schedule not found
            HttpContext.Current.Trace.Write("AddTardy", strScheduleTimeCol & " Not Found")
            AddTardy = 0
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If AddTardy <> 0 Then
            HttpContext.Current.Trace.Write("AddTardy", "Begin Tardy Record SQL INSERT")
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "INSERT INTO [Tardy] ([Client_ID], [User_ID], [Time_ID], [Minutes], [Type]) VALUES (@Client_ID, @User_ID, @Time_ID, @Minutes, @Type)"
            objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
            objCommand.Parameters.AddWithValue("@User_ID", intUserID)
            objCommand.Parameters.AddWithValue("@Time_ID", intTimeID)
            objCommand.Parameters.AddWithValue("@Minutes", Math.Abs(intDifference))
            objCommand.Parameters.AddWithValue("@Type", AddTardy)

            objCommand.ExecuteNonQuery()
            HttpContext.Current.Trace.Write("AddTardy", "End Tardy Record SQL INSERT")

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
        HttpContext.Current.Trace.Write("AddTardy", "End Tardy.AddTardy")
    End Function

    Public Shared Function CheckForTardy(ByVal intTimeID As Int64) As Int64
        HttpContext.Current.Trace.Write("CheckForTardy", "Begin Tardy.CheckForTardy")
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Tardy_ID FROM Tardy WHERE Time_ID = @Time_ID"
        objCommand.Parameters.AddWithValue("@Time_ID", intTimeID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            HttpContext.Current.Trace.Write("CheckForTardy", "Found Tardy_ID " & objDR("Tardy_ID"))
            CheckForTardy = objDR("Tardy_ID")
        Else
            CheckForTardy = 0
            HttpContext.Current.Trace.Write("CheckForTardy", "No Tardy_ID Found")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("CheckForTardy", "End Tardy.CheckForTardy")
    End Function


    Public Shared Sub DeleteTardy(ByVal intTardyID As Int64)
        HttpContext.Current.Trace.Write("DeleteTardy", "Begin Tardy.DeleteTardy")
        If intTardyID <> 0 Then
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "DELETE FROM Tardy WHERE Tardy_ID = @Tardy_ID"
            objCommand.Parameters.AddWithValue("@Tardy_ID", intTardyID)
            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
        HttpContext.Current.Trace.Write("DeleteTardy", "End Tardy.DeleteTardy")
    End Sub
End Class