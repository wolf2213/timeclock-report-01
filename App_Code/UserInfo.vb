Imports Microsoft.VisualBasic
Imports System.Text.RegularExpressions
Imports Microsoft.Practices.EnterpriseLibrary
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class UserInfo
    Inherits Connection
    'Dim _db As Database = DatabaseFactory.CreateDatabase("default")

    Public Shared Function UserStatus(ByVal UserID As Integer)
        HttpContext.Current.Trace.Write("UserStatus", "Begin UserStatus")
        'This function tells you the current clocked in status (checked in/out, on break) for a given user
        '1=clocked out, 2 = clocked in, 3 = on break
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        UserStatus = 0
        Dim boolOnBreak = False

        Dim objDR2 As System.Data.SqlClient.SqlDataReader
        Dim objCommand2 As System.Data.SqlClient.SqlCommand
        Dim objConnection2 As System.Data.SqlClient.SqlConnection
        objConnection2 = New System.Data.SqlClient.SqlConnection

        objConnection2.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection2.Open()
        objCommand2 = New System.Data.SqlClient.SqlCommand()
        objCommand2.Connection = objConnection2
        objCommand2.CommandText = "SELECT EndTime FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL AND Type <> 1 ORDER BY StartTime DESC"
        objCommand2.Parameters.AddWithValue("@User_ID", UserID)

        objDR2 = objCommand2.ExecuteReader()

        If objDR2.HasRows Then
            'The Person is currently on break
            HttpContext.Current.Trace.Write("UserStatus", "On Break, set status = 3")
            UserStatus = 3
            boolOnBreak = True
        End If

        objConnection.Close()
        objConnection = Nothing
        objCommand = Nothing
        objDR = Nothing


        If Not boolOnBreak Then

            objConnection2 = New System.Data.SqlClient.SqlConnection

            objConnection2.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection2.Open()
            objCommand2 = New System.Data.SqlClient.SqlCommand()
            objCommand2.Connection = objConnection2
            objCommand2.CommandText = "SELECT Type, EndTime FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL ORDER BY StartTime DESC"
            objCommand2.Parameters.AddWithValue("@User_ID", UserID)

            objDR2 = objCommand2.ExecuteReader()

            If objDR2.HasRows Then
                objDR2.Read()
                If CInt(objDR2("Type")) = 1 Then
                    'The Person is currently clocked in and not on break
                    HttpContext.Current.Trace.Write("UserStatus", "Clocked In, set status = 2")
                    UserStatus = 2
                Else

                End If
            Else
                'The Person is not clocked in
                HttpContext.Current.Trace.Write("UserStatus", "Clocked Out, set status = 1")
                UserStatus = 1
            End If

            objConnection2.Close()
            objConnection2 = Nothing
            objCommand2 = Nothing
            objDR2 = Nothing
        End If

        HttpContext.Current.Trace.Write("UserStatus", "End UserStatus")
    End Function

    Public Shared Function UsernameExists(ByVal strUsername As String, ByVal intClient_ID As Double, ByVal intActive As Integer) As Boolean
        'Checks to see if a user with the specified username and client_ID already exists, returns true or false
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As New System.Data.SqlClient.SqlCommand
        Dim objConnection As New System.Data.SqlClient.SqlConnection

        Try
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand.Connection = objConnection

            objCommand.CommandText = "SELECT User_ID FROM Users WHERE Client_ID = @Client_ID AND Username = @Username AND Active = @Active"
            objCommand.Parameters.AddWithValue("@Client_ID", intClient_ID)
            objCommand.Parameters.AddWithValue("@Username", strUsername)
            objCommand.Parameters.AddWithValue("@Active", intActive)

            objDR = objCommand.ExecuteReader

            If objDR.HasRows Then
                UsernameExists = True
            Else
                UsernameExists = False
            End If
        Catch
            UsernameExists = True
        Finally
            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End Try
    End Function

    Public Shared Function UsernameFromID(ByVal intUser_ID As Int64) As String
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As New System.Data.SqlClient.SqlCommand
        Dim objConnection As New System.Data.SqlClient.SqlConnection

        Try
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand.Connection = objConnection

            objCommand.CommandText = "SELECT Username FROM Users WHERE User_ID = @User_ID"
            objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

            objDR = objCommand.ExecuteReader

            If objDR.HasRows Then
                objDR.Read()
                UsernameFromID = objDR.Item("Username")
            Else
                UsernameFromID = "Invalid User_ID"
            End If
        Catch
            UsernameFromID = Err.Description
        Finally
            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End Try
    End Function

    Public Shared Function NameFromID(ByVal intUser_ID As Int64, Optional ByVal intMethod As Integer = 0) As String
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As New System.Data.SqlClient.SqlCommand
        Dim objConnection As New System.Data.SqlClient.SqlConnection
        Dim sqlQuery As String

        Select Case intMethod
            Case 0
                sqlQuery = "SELECT (LastName + ', ' + FirstName) AS Name FROM Users WHERE User_ID = @User_ID"
            Case 1
                sqlQuery = "SELECT (FirstName + ' ' + LastName) AS Name FROM Users WHERE User_ID = @User_ID"
            Case 2
                sqlQuery = "SELECT (FirstName) AS Name FROM Users WHERE User_ID = @User_ID"
            Case Else
                sqlQuery = "SELECT (LastName + ', ' + FirstName) AS Name FROM Users WHERE User_ID = @User_ID"
        End Select

        Try
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand.Connection = objConnection
            objCommand.CommandText = sqlQuery
            objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

            objDR = objCommand.ExecuteReader

            If objDR.HasRows Then
                objDR.Read()
                NameFromID = objDR.Item("Name")
            Else
                NameFromID = "Invalid User_ID"
            End If
        Catch
            NameFromID = Err.Description
        Finally
            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End Try
    End Function

    Public Shared Function ValidateEmail(ByVal sEmailAdd As String)
        ValidateEmail = Regex.IsMatch(sEmailAdd, "[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z_+])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9}$")
    End Function

    Public Function sprocGetCustomFields(ByVal ClientID As Int64) As DataSet
        Dim ds As New DataSet
        Dim objCommand As New SqlCommand("sprocGetCustomFields")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        Try
            ds = _db.ExecuteDataSet(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
        Return ds
    End Function

    Public Function sprocGetUsersDetail(ByVal ClientID As Int32, ByVal UserId As Int32) As DataSet
        Dim ds As New DataSet
        Dim objCommand As New SqlCommand("sprocGetUsersDetail")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        objCommand.Parameters.AddWithValue("@UserID", UserId)
        Try
            ds = _db.ExecuteDataSet(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
        Return ds
    End Function

    Public Function sprocGetSelfRegistrationpin(ByVal ClientID As Int64) As DataSet
        Dim ds As New DataSet
        Dim objCommand As New SqlCommand("sprocGetSelfRegistrationpin")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        Try
            ds = _db.ExecuteDataSet(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
        Return ds
    End Function

    Public Function sprocSaveUsers(ByVal ClientID As Int64, ByVal UserID As Int64, ByVal OfficeID As Int64, ByVal Manager As Boolean, ByVal TZOffset As Double, ByVal DST As Boolean, ByVal Overtime As Integer, ByVal Username As String, ByVal Password As String, ByVal FirstName As String, ByVal LastName As String, ByVal Email As String, ByVal HomePhone As String, ByVal CellPhone As String, ByVal WorkPhone As String, ByVal WorkExt As String, ByVal HomeAddress1 As String, ByVal HomeAddress2 As String, ByVal City As String, ByVal State As String, ByVal ZIPCode As String, ByVal TimeDisplay As Boolean, ByVal Birthday As DateTime, ByVal ClockGuard As Boolean, ByVal AllowedDeviation As Integer, ByVal MobileAccess As Boolean, ByVal CurrencyType As String, ByVal Wage As Double, ByVal Custom1Value As String, ByVal Custom2Value As String, ByVal Custom3Value As String, ByVal Custom4Value As String, ByVal Custom5Value As String, ByVal OvernightEmployee As Boolean, ByVal ManagerUserId As Int64, ByVal OverTimetype As Integer, ByVal OvertimeDailyHours As Double, ByVal OvertimeWeeklyHours As Double, ByVal ApproveOvertime As Boolean, ByRef IsSaved As Int64) As Int64

        Dim objCommand As New SqlCommand("sprocSaveUsers")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        objCommand.Parameters.AddWithValue("@UserID", UserID)
        objCommand.Parameters.AddWithValue("@OfficeID", OfficeID)
        objCommand.Parameters.AddWithValue("@Manager", Manager)
        objCommand.Parameters.AddWithValue("@TZOffset", TZOffset)
        objCommand.Parameters.AddWithValue("@DST", DST)
        objCommand.Parameters.AddWithValue("@Overtime", Overtime)
        objCommand.Parameters.AddWithValue("@Username", Username)
        objCommand.Parameters.AddWithValue("@Password", Password)
        objCommand.Parameters.AddWithValue("@FirstName", FirstName)
        objCommand.Parameters.AddWithValue("@LastName", LastName)
        objCommand.Parameters.AddWithValue("@Email", Email)
        objCommand.Parameters.AddWithValue("@HomePhone", HomePhone)
        objCommand.Parameters.AddWithValue("@CellPhone", CellPhone)
        objCommand.Parameters.AddWithValue("@WorkPhone", WorkPhone)
        objCommand.Parameters.AddWithValue("@WorkExt", WorkExt)
        objCommand.Parameters.AddWithValue("@HomeAddress1", HomeAddress1)
        objCommand.Parameters.AddWithValue("@HomeAddress2", HomeAddress2)
        objCommand.Parameters.AddWithValue("@City", City)
        objCommand.Parameters.AddWithValue("@State", State)
        objCommand.Parameters.AddWithValue("@ZIPCode", ZIPCode)
        objCommand.Parameters.AddWithValue("@TimeDisplay", TimeDisplay)
        objCommand.Parameters.AddWithValue("@Birthday", Birthday)
        objCommand.Parameters.AddWithValue("@ClockGuard", ClockGuard)
        objCommand.Parameters.AddWithValue("@AllowedDeviation", AllowedDeviation)
        objCommand.Parameters.AddWithValue("@MobileAccess", MobileAccess)
        objCommand.Parameters.AddWithValue("@CurrencyType", CurrencyType)
        objCommand.Parameters.AddWithValue("@Wage", Wage)
        objCommand.Parameters.AddWithValue("@Custom1Value", Custom1Value)
        objCommand.Parameters.AddWithValue("@Custom2Value", Custom2Value)
        objCommand.Parameters.AddWithValue("@Custom3Value", Custom3Value)
        objCommand.Parameters.AddWithValue("@Custom4Value", Custom4Value)
        objCommand.Parameters.AddWithValue("@Custom5Value", Custom5Value)
        objCommand.Parameters.AddWithValue("@OvernightEmployee", OvernightEmployee)
        objCommand.Parameters.AddWithValue("@ManagerUserId", ManagerUserId)
        objCommand.Parameters.AddWithValue("@OverTimetype", OverTimetype)
        objCommand.Parameters.AddWithValue("@OvertimeDailyHours", OvertimeDailyHours)
        objCommand.Parameters.AddWithValue("@OvertimeWeeklyHours", OvertimeWeeklyHours)
        objCommand.Parameters.AddWithValue("@ApproveOvertime", ApproveOvertime)
        objCommand.Parameters.AddWithValue("@IsSaved", IsSaved)
        objCommand.Parameters("@IsSaved").Direction = ParameterDirection.Output
        Try
            _db.ExecuteNonQuery(objCommand)
            If Not IsDBNull(objCommand.Parameters("@IsSaved").Value) Then
                IsSaved = objCommand.Parameters("@IsSaved").Value
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return IsSaved
    End Function

    Public Function sprocSaveUserPermissions(ByVal ClientID As Int64, ByVal UserID As Int64) As Integer
        Dim objCommand As New SqlCommand("sprocSaveUserPermissions")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        objCommand.Parameters.AddWithValue("@UserID", UserID)
        Try
            _db.ExecuteNonQuery(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function sprocInsertPermissions(ByVal intUserId As Int64, ByVal ManagerID As Int64) As Integer
        Dim objCommand As New SqlCommand("sprocInsertPermissions")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@User_ID", intUserId)
        objCommand.Parameters.AddWithValue("@Manager_ID", ManagerID)
        Try
            _db.ExecuteNonQuery(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function sprocDeleteUsersPermissions(ByVal ClientID As Int64, ByVal UserID As Int64) As Integer
        Dim objCommand As New SqlCommand("sprocDeleteUsersPermissions")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        objCommand.Parameters.AddWithValue("@UserID", UserID)
        Try
            _db.ExecuteNonQuery(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function sprocCheckUserOKtoRemoveOverniteAttribute(ByVal ClientID As Int64, ByVal UserID As Int64, ByRef IsCount As Integer) As Integer
        Dim objCommand As New SqlCommand("sprocCheckUserOKtoRemoveOverniteAttribute")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        objCommand.Parameters.AddWithValue("@UserID", UserID)
        objCommand.Parameters.AddWithValue("@IsCount", IsCount)
        objCommand.Parameters("@IsCount").Direction = ParameterDirection.Output
        Try
            IsCount = _db.ExecuteNonQuery(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
        Return IsCount
    End Function
End Class

