Imports Microsoft.VisualBasic

Public Class Audit
    Public Shared Sub Login(ByVal intClientID As Int64, ByVal intUserID As Int64, ByVal strIPAddress As String, ByVal strBrowser As String)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        Dim intBrowser As Integer = BrowserLookup(strBrowser)

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "INSERT INTO AuditLogin (Client_ID, User_ID, LoginTimestamp, IPAddress, Browser, BrowserOther) VALUES (@Client_ID, @User_ID, @LoginTimestamp, @IPAddress, @Browser, @BrowserOther)"
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objCommand.Parameters.AddWithValue("@LoginTimestamp", Now())
        objCommand.Parameters.AddWithValue("@IPAddress", Left(strIPAddress, 15))
        objCommand.Parameters.AddWithValue("@Browser", intBrowser)
        objCommand.Parameters.AddWithValue("@BrowserOther", Left(strBrowser, 255))
        'If intBrowser <> 0 Then
        '    objCommand.Parameters.AddWithValue("@BrowserOther", System.DBNull.Value)
        'Else
        '    objCommand.Parameters.AddWithValue("@BrowserOther", Left(strBrowser, 255))
        'End If

        objCommand.ExecuteNonQuery()
    End Sub

    Public Shared Function BrowserLookup(ByVal strUserAgent As String) As Integer
        If InStr(strUserAgent, "MSIE 7.0") <> 0 Then
            BrowserLookup = 1
        ElseIf InStr(strUserAgent, "MSIE 6.0") <> 0 Then
            BrowserLookup = 2
        ElseIf InStr(strUserAgent, "MSIE 5.5") <> 0 Then
            BrowserLookup = 3
        ElseIf InStr(strUserAgent, "Firefox/3") <> 0 Then
            BrowserLookup = 4
        ElseIf InStr(strUserAgent, "Firefox/2") <> 0 Then
            BrowserLookup = 5
        ElseIf InStr(strUserAgent, "Firefox/1") <> 0 Then
            BrowserLookup = 6
        ElseIf InStr(strUserAgent, "Opera/9") <> 0 Then
            BrowserLookup = 7
        ElseIf InStr(strUserAgent, "Opera/8") <> 0 Then
            BrowserLookup = 8
        ElseIf InStr(strUserAgent, "Version/4") <> 0 Then
            'Safari 4.x
            BrowserLookup = 9
        ElseIf InStr(strUserAgent, "Version/3") <> 0 Then
            'Safari 3.x
            BrowserLookup = 10
        Else
            BrowserLookup = 0
        End If
    End Function

    Public Shared Sub AdministrativeSupportHistory(ByVal Client_ID As Int64, ByVal BossUser_ID As Int64, ByVal dtTimestamp As DateTime, ByVal strSubject As String, ByVal strBody As String)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "INSERT INTO BossSupportHistory (Client_ID, User_ID, Boss_User_ID, Type, Timestamp, Subject, Body) VALUES (@Client_ID, @User_ID, @Boss_User_ID, @Type, @Timestamp, @Subject, @Body)"
        objCommand.Parameters.AddWithValue("@Client_ID", Client_ID)
        objCommand.Parameters.AddWithValue("@User_ID", "0")
        objCommand.Parameters.AddWithValue("@Boss_User_ID", BossUser_ID)
        objCommand.Parameters.AddWithValue("@Type", "4")
        objCommand.Parameters.AddWithValue("@Timestamp", dtTimestamp)
        objCommand.Parameters.AddWithValue("@Subject", strSubject)
        objCommand.Parameters.AddWithValue("@Body", strBody)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub
    Public Shared Sub CreateNewClockitinAudit(ByVal Client_ID As Int64, ByVal User_ID As Int64, ByVal strTableName As String, ByVal strColumnName As String, ByVal strSubject As String)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "EXEC sprocCreateNewClockitinAudit @Client_ID, @User_ID, @TableName, @ColumnName, @AuditDescription"
        objCommand.Parameters.AddWithValue("@Client_ID", Client_ID)
        objCommand.Parameters.AddWithValue("@User_ID", User_ID)
        objCommand.Parameters.AddWithValue("@TableName", strTableName)
        objCommand.Parameters.AddWithValue("@ColumnName", strColumnName)
        objCommand.Parameters.AddWithValue("@AuditDescription", strSubject)
        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub
End Class
