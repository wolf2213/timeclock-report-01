Imports Microsoft.VisualBasic

Public Class Time
    Public Shared Function DatesOfWeek(ByVal weekOfYear As Integer, ByVal year As Integer, ByVal startEnd As Integer) As Date
        'this returns the start date or the end date of a week number for a specific year
        ' if startEnd = 0 then it returns start date, if it = 1 then it returns end date

        Dim baseDate As Date = DateAdd(DateInterval.Day, 7 * (weekOfYear - 1), DateSerial(year, 1, 1))

        Dim startWeek As Date = DateAdd(DateInterval.Day, 1 - Weekday(baseDate), baseDate)
        Dim endWeek As Date = DateAdd(DateInterval.Day, 7 - Weekday(baseDate), baseDate)

        If startEnd = 0 Then
            datesOfWeek = startWeek
        Else
            datesOfWeek = endWeek
        End If
    End Function

    Public Shared Function DateOfWeekDay(ByVal intWeekOfYear As Integer, ByVal intYear As Integer, ByVal intDayOfWeek As Integer) As Date
        'For intDayOfWeek, Sunday = 0, Monday = 1, ... , Saturday = 6
        DateOfWeekDay = DateAdd(DateInterval.Day, intDayOfWeek, DatesOfWeek(intWeekOfYear, intYear, 0))
    End Function

    Public Shared Function DaySuffix(ByVal Day As Integer) As String
        Select Case Day
            Case 1, 21, 31
                DaySuffix = Day & "st"
            Case 2, 22
                DaySuffix = Day & "nd"
            Case 3, 23
                DaySuffix = Day & "rd"
            Case Else
                DaySuffix = Day & "th"
        End Select
    End Function

    Public Shared Function StripTime(ByVal DateTime As String) As String
        'returns the date
        Dim SplitDateTime As Array = Split(DateTime)
        StripTime = SplitDateTime(0)
    End Function

    Public Shared Function StripDate(ByVal DateTime As String) As String
        'returns the time
        'HttpContext.Current.Trace.Write("StripDate", "DateTime = " & DateTime)
        'HttpContext.Current.Trace.Write("StripDate", "DateTime.IndexOf(':') = " & DateTime.IndexOf(":"))
        If DateTime.IndexOf(":") > 0 Then
            Dim SplitDateTime As Array = Split(DateTime)
            StripDate = SplitDateTime(1) & " " & SplitDateTime(2)
            'HttpContext.Current.Trace.Write("StripDate", "StripDate = " & StripDate)
        Else
            StripDate = "12:00:00 AM"
        End If
    End Function

    Public Shared Function StripSeconds(ByVal DateTime As String) As String
        'returns the string (with date or without date), without the seconds
        'HttpContext.Current.Trace.Write("StripSeconds", "DateTime = " & DateTime)
        'HttpContext.Current.Trace.Write("StripSeconds", "DateTime.IndexOf(':') = " & DateTime.IndexOf(":"))
        If DateTime.IndexOf(":") > 0 Then
            Dim SplitDateTime As Array = Split(DateTime, ":")
            StripSeconds = SplitDateTime(0) & ":" & SplitDateTime(1) & " " & Right(SplitDateTime(2), 2)
        Else
            StripSeconds = DateTime & " 12:00 AM"
        End If
    End Function

    Public Shared Function QuickStrip(ByVal DateTime As String) As String
        'strips both the seconds and the date
        QuickStrip = StripSeconds(StripDate(DateTime))
    End Function

    Public Shared Function FormatLongDate(ByVal strDate As String) As String
        strDate = Left(strDate, Len(strDate) - 6)
        If Left(Right(strDate, 2), 1) = "0" Then
            Dim strDate2 As String = Left(strDate, Len(strDate) - 2)
            strDate = strDate2 & Right(strDate, 1)
        End If
        FormatLongDate = strDate
    End Function

    Public Shared Function StripWeekDayAndYear(ByVal strDate As String) As String
        Dim strDateSplit As Array = Split(strDate, ",")
        If Left(Right(strDateSplit(1), 2), 1) = "0" Then
            Dim strDate2 As String = Left(strDateSplit(1), Len(strDateSplit(1)) - 2)
            HttpContext.Current.Trace.Write("StripWeekDayAndYear", "strDate2 = " & strDate2)
            StripWeekDayAndYear = strDate2 & Right(strDateSplit(1), 1)
        Else
            StripWeekDayAndYear = Trim(strDateSplit(1))
        End If
    End Function

    Public Shared Function WeekDayName(ByVal intDay As Integer, ByVal intLength As Integer) As String
        'if intLength = 0 then return 3 letter I.e. (Sun, Mon)
        'if intlength = 1 then return full name (Sunday, Monday)

        Select Case intDay
            Case 1
                WeekDayName = "Sunday"
            Case 2
                WeekDayName = "Monday"
            Case 3
                WeekDayName = "Tuesday"
            Case 4
                WeekDayName = "Wednesday"
            Case 5
                WeekDayName = "Thursday"
            Case 6
                WeekDayName = "Friday"
            Case 7
                WeekDayName = "Saturday"
            Case Else
                WeekDayName = "Undefined"
        End Select

        If intLength = 0 And WeekDayName <> "Undefined" Then
            WeekDayName = Left(WeekDayName, 3)
        End If
    End Function

    Public Shared Function Convert24HourTo12Hour(ByVal intHour As Integer) As Integer
        If intHour > 12 Then
            Convert24HourTo12Hour = intHour - 12
        Else
            Convert24HourTo12Hour = intHour
        End If
    End Function

    Public Shared Function AMPM(ByVal intHour As Integer) As String
        If intHour > 12 Then
            AMPM = "PM"
        Else
            AMPM = "AM"
        End If
    End Function

    Public Shared Function AddZero(ByVal intNumber As Integer) As String
        If intNumber > 9 Then
            AddZero = intNumber
        Else
            AddZero = "0" & intNumber
        End If
    End Function
End Class
