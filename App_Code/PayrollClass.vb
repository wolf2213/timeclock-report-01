Imports Microsoft.VisualBasic
Imports System.Data

Public Class PayrollClass
    Public Shared Function PayrollAvailable(ByVal intClientID As Int64, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As Boolean
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        Dim strSQLQuery As String

        strSQLQuery = "SELECT Payroll_ID FROM PayrollSummary WHERE Client_ID = @Client_ID AND (StartDate <= @EndDate) AND (EndDate >= @StartDate)"

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommand.Parameters.AddWithValue("@StartDate", dtStartDate)
        objCommand.Parameters.AddWithValue("@EndDate", dtEndDate)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            PayrollAvailable = False
        Else
            PayrollAvailable = True
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Function

    Public Shared Function DateFromID(ByVal intClientID As Int64, ByVal intPayrollID As Int64, ByVal strStartOrEnd As String)
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        Dim strSQLQuery As String

        strSQLQuery = "SELECT StartDate, EndDate FROM PayrollSummary WHERE Client_ID = @Client_ID AND Payroll_ID = @Payroll_ID"

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommand.Parameters.AddWithValue("@Payroll_ID", intPayrollID)
        objDR = objCommand.ExecuteReader()

        If objDR.Read Then
            Select Case strStartOrEnd
                Case "Start"
                    DateFromID = objDR("StartDate")
                Case "End"
                    DateFromID = objDR("EndDate")
                Case Else
                    DateFromID = "You must specify Start or End"
            End Select
        Else
            DateFromID = "Invalid Payroll ID"
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Function

    Public Shared Function PayrollExists(ByVal intClientID As Int64, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As Int64
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        Dim strSQLQuery As String

        strSQLQuery = "SELECT Payroll_ID FROM PayrollSummary WHERE Client_ID = @Client_ID AND (StartDate <= @EndDate) AND (EndDate >= @StartDate)"

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        HttpContext.Current.Trace.Write("PayrollClass.PayrollExists", "dtStartDate = " & dtStartDate)
        objCommand.Parameters.AddWithValue("@StartDate", dtStartDate)
        HttpContext.Current.Trace.Write("PayrollClass.PayrollExists", "dtEndDate = " & dtEndDate)
        objCommand.Parameters.AddWithValue("@EndDate", dtEndDate)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            PayrollExists = objDR("Payroll_ID")
        Else
            PayrollExists = 0
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Function

    Public Shared Sub CreatePayroll(ByVal intClientID As Int64, ByVal intUserID As Int64, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal intOffice_ID As Int64)
        HttpContext.Current.Trace.Write("PayrollClass", "Begin CreatePayroll")
        'insert a blank record into Summary
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection
        Dim intPayrollID As Int32
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand("sprocSavePayrollSummary")
        objCommand.Connection = objConnection
        '-----------Comment by technobrains------------
        '---SP added sprocSavePayrollSummary
        'objCommand.CommandText = "INSERT INTO PayrollSummary (Client_ID, Office_ID, StartDate, EndDate) VALUES (@Client_ID, @Office_ID, @StartDate, @EndDate)"
        '-----------End Comment by technobrains------------
        objCommand.CommandType = Data.CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        objCommand.Parameters.AddWithValue("@Office_Id", intOffice_ID)
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objCommand.Parameters.AddWithValue("@StartDate", dtStartDate & " 12:00 AM")
        objCommand.Parameters.AddWithValue("@EndDate", dtEndDate & " 11:59 PM")
        objCommand.Parameters.AddWithValue("@Payroll_ID", 0)
        objCommand.Parameters("@Payroll_ID").Direction = ParameterDirection.Output
        objCommand.ExecuteNonQuery()
        If Not IsDBNull(objCommand.Parameters("@Payroll_ID").Value) Then
            intPayrollID = objCommand.Parameters("@Payroll_ID").Value
        End If
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'get the payroll ID

        '-----------Comment by technobrains------------
        'Dim objDR As System.Data.SqlClient.SqlDataReader
        'objConnection = New System.Data.SqlClient.SqlConnection
        'objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        'objConnection.Open()
        'objCommand = New System.Data.SqlClient.SqlCommand()
        'objCommand.Connection = objConnection
        'objCommand.CommandText = "SELECT MAX(Payroll_ID) AS Payroll_ID FROM PayrollSummary WHERE Client_ID = @Client_ID"
        'objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        'objDR = objCommand.ExecuteReader()
        'objDR.Read()
        '-----------End Comment by technobrains------------

        'objCommand = Nothing
        'objConnection.Close()
        'objConnection = Nothing

        'create a payrollDetail record for each employee
        '-----------Comment by technobrains------------
        'If intOffice_ID = 0 Then
        '    HttpContext.Current.Trace.Write("CreatePayroll", "sprocPayrollDetail ClientID = " & intClientID & " | StartTime = " & dtStartDate & " | EndTime = " & dtEndDate & " | Payroll_ID = " & intPayrollID & " |")
        '    objConnection = New System.Data.SqlClient.SqlConnection
        '    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        '    objConnection.Open()
        '    objCommand = New System.Data.SqlClient.SqlCommand()
        '    objCommand.Connection = objConnection
        '    objCommand.CommandTimeout = 500
        '    objCommand.CommandText = "EXEC sprocPayrollDetail @Client_ID, @StartTime, @EndTime, @Payroll_ID"
        '    objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        '    objCommand.Parameters.AddWithValue("@StartTime", dtStartDate & " 12:00 AM")
        '    objCommand.Parameters.AddWithValue("@EndTime", dtEndDate & " 11:59 PM")
        '    objCommand.Parameters.AddWithValue("@Payroll_ID", intPayrollID)
        '    objCommand.ExecuteNonQuery()
        'Else
        '    HttpContext.Current.Trace.Write("CreatePayroll", "sprocPayrollDetailOffice ClientID = " & intClientID & " | StartTime = " & dtStartDate & " | EndTime = " & dtEndDate & " | Payroll_ID = " & intPayrollID & " | Office ID = " & intOffice_ID)
        '    objConnection = New System.Data.SqlClient.SqlConnection
        '    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        '    objConnection.Open()
        '    objCommand = New System.Data.SqlClient.SqlCommand()
        '    objCommand.Connection = objConnection
        '    objCommand.CommandTimeout = 500
        '    objCommand.CommandText = "EXEC sprocPayrollDetailOffice @Client_ID, @StartTime, @EndTime, @Payroll_ID, @Office_ID"
        '    objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        '    objCommand.Parameters.AddWithValue("@StartTime", dtStartDate & " 12:00 AM")
        '    objCommand.Parameters.AddWithValue("@EndTime", dtEndDate & " 11:59 PM")
        '    objCommand.Parameters.AddWithValue("@Payroll_ID", intPayrollID)
        '    objCommand.Parameters.AddWithValue("@Office_ID", intOffice_ID)
        '    objCommand.ExecuteNonQuery()
        'End If
        '-----------End Comment by technobrains------------

        'calculate overtime if necessary
        'you have to do this for each week

        '-----------Comment by technobrains------------
        'Dim intDaysBetweenStartEnd As Integer = DateDiff(DateInterval.Day, dtStartDate, dtEndDate)
        'HttpContext.Current.Trace.Write("CreatePayroll", "intDaysBetweenStartEnd = " & intDaysBetweenStartEnd)

        'Dim dtLoopStartDate As DateTime = dtStartDate
        'Dim dtLoopEndDate As DateTime = dtStartDate

        'If intDaysBetweenStartEnd < 7 Then
        '    dtLoopEndDate = DateAdd(DateInterval.Day, intDaysBetweenStartEnd, dtLoopEndDate)
        'Else
        '    dtLoopEndDate = DateAdd(DateInterval.Day, 6, dtLoopEndDate)
        'End If

        'Dim intRemainder As Integer = 0

        'Dim intLoopNum As Double = intDaysBetweenStartEnd / 7
        'HttpContext.Current.Trace.Write("CreatePayroll", "Loop Number = " & intLoopNum)

        'If Fix(intLoopNum) < intLoopNum Then
        '    intRemainder = intDaysBetweenStartEnd Mod 7 
        'End If

        'HttpContext.Current.Trace.Write("CreatePayroll", "intRemainder = " & intRemainder)

        'Dim intDaysToIncrease As Integer
        'Dim intI As Integer = 1
        'HttpContext.Current.Trace.Write("CreatePayroll", "Begin OT Loop")

        'Dim daily As Boolean = False
        'Dim periodInc As Integer = 7

        'If daily Then
        '    periodInc = 1
        'End If

        'While dtLoopEndDate <= dtEndDate
        '    HttpContext.Current.Trace.Write("CreatePayroll", "loopStartDate = " & dtLoopStartDate & " | loopEndDate = " & dtLoopEndDate & " | IntI = " & intI)
        '    'execute sproc
        '    objConnection = New System.Data.SqlClient.SqlConnection
        '    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        '    objConnection.Open()
        '    objCommand = New System.Data.SqlClient.SqlCommand()
        '    objCommand.Connection = objConnection
        '    objCommand.CommandTimeout = 500
        '    objCommand.CommandText = "EXEC sprocPayrollOT @Payroll_ID, @Client_ID, @StartDate, @EndDate"
        '    objCommand.Parameters.AddWithValue("@Payroll_ID", intPayrollID)
        '    objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        '    objCommand.Parameters.AddWithValue("@StartDate", dtLoopStartDate & " 12:00 AM")
        '    objCommand.Parameters.AddWithValue("@EndDate", dtLoopEndDate & " 11:59 PM")
        '    'need fifth parameter to determine the number of base hours
        '    '    with conditional to make different for weeklt vs daily
        '    objCommand.ExecuteNonQuery()

        '    If daily Then
        '        intDaysToIncrease = 1
        '    Else
        '        If DateDiff(DateInterval.Day, dtLoopEndDate, dtEndDate) < 7 And intRemainder <> 0 Then
        '            intDaysToIncrease = intRemainder
        '        Else
        '            intDaysToIncrease = 6
        '        End If
        '    End If

        '    HttpContext.Current.Trace.Write("CreatePayroll", "intDaysToIncrease = " & intDaysToIncrease)

        '    dtLoopStartDate = DateAdd(DateInterval.Day, periodInc, dtLoopStartDate)
        '    dtLoopEndDate = DateAdd(DateInterval.Day, intDaysToIncrease, dtLoopStartDate)

        '    intI += 1
        'End While
        'HttpContext.Current.Trace.Write("CreatePayroll", "End OT Loop")
        'correct payroll colums
        'If intOffice_ID = 0 Then
        '    HttpContext.Current.Trace.Write("CreatePayroll", "Correct Payroll Colums (Set RegMins = RegMins - OT MINS")
        '    objConnection = New System.Data.SqlClient.SqlConnection
        '    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        '    objConnection.Open()
        '    objCommand = New System.Data.SqlClient.SqlCommand()
        '    objCommand.Connection = objConnection
        '    objCommand.CommandTimeout = 500
        '    objCommand.CommandText = "EXEC sprocPayrollCorrection @User_ID, @Client_ID, @Payroll_ID, @StartDate, @EndDate, @ConverionRate"
        '    objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        '    objCommand.Parameters.AddWithValue("@Payroll_ID", intPayrollID)
        '    objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        '    objCommand.Parameters.AddWithValue("@StartDate", dtStartDate & " 12:00 AM")
        '    objCommand.Parameters.AddWithValue("@EndDate", dtEndDate & " 11:59 PM")
        '    objCommand.Parameters.AddWithValue("@ConverionRate", Convert.ToDecimal(ConfigurationManager.AppSettings("DollarRupeeConvExchange").ToString()))
        '    objCommand.ExecuteNonQuery()
        'Else
        '    HttpContext.Current.Trace.Write("CreatePayroll", "Correct Payroll Colums (Set RegMins = RegMins - OT MINS) WITH OFFICE")
        '    objConnection = New System.Data.SqlClient.SqlConnection
        '    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        '    objConnection.Open()
        '    objCommand = New System.Data.SqlClient.SqlCommand()
        '    objCommand.Connection = objConnection
        '    objCommand.CommandTimeout = 500
        '    objCommand.CommandText = "EXEC sprocPayrollCorrectionOffice @User_ID, @Client_ID, @Payroll_ID, @StartDate, @EndDate, @Office_ID, @ConverionRate"
        '    objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        '    objCommand.Parameters.AddWithValue("@Payroll_ID", intPayrollID)
        '    objCommand.Parameters.AddWithValue("@Client_ID", intClientID)
        '    objCommand.Parameters.AddWithValue("@StartDate", dtStartDate & " 12:00 AM")
        '    objCommand.Parameters.AddWithValue("@EndDate", dtEndDate & " 11:59 PM")
        '    objCommand.Parameters.AddWithValue("@Office_ID", intOffice_ID)
        '    objCommand.Parameters.AddWithValue("@ConverionRate", Convert.ToDecimal(ConfigurationManager.AppSettings("DollarRupeeConvExchange").ToString()))
        '    objCommand.ExecuteNonQuery()
        'End If
        '-----------End Comment by technobrains------------
        HttpContext.Current.Trace.Write("PayrollClass", "End CreatePayroll")
    End Sub

    Public Shared Sub InvalidatePayroll(ByVal intPayroll_ID As Double)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE [PayrollSummary] SET Valid = 0 WHERE Payroll_ID = @Payroll_ID"
        objCommand.Parameters.AddWithValue("@Payroll_ID", intPayroll_ID)
        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

End Class
