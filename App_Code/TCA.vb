Imports Microsoft.VisualBasic

Public Class TCA
    Public Shared Sub Transfer(ByVal intClient_ID As Int64)
        HttpContext.Current.Trace.Write("TCA.vb Transfer", "Begin Sub")
        HttpContext.Current.Trace.Write("TCA.vb Transfer", "Begin Transferring data for Client_Id = " & intClient_ID)

        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        'objCommand.CommandText = "EXEC sprocTransferTest @Client_ID"
        objCommand.CommandText = "EXEC sprocTransferClient @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", intClient_ID)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("TCA.vb Transfer", "All Data has been transfered over")

        'encrypt the passwords
        HttpContext.Current.Trace.Write("TCA.vb Transfer", "Encrypt the passwords")
        Dim objDR As System.Data.SqlClient.SqlDataReader
        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT User_ID, Password FROM Users WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", intClient_ID)

        objDR = objCommand.ExecuteReader()

        HttpContext.Current.Trace.Write("TCA.vb Transfer", "Begin Loop")
        While objDR.Read
            Dim objCommandClient As System.Data.SqlClient.SqlCommand
            Dim objConnectionClient As System.Data.SqlClient.SqlConnection

            objConnectionClient = New System.Data.SqlClient.SqlConnection
            objConnectionClient.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionClient.Open()
            objCommandClient = New System.Data.SqlClient.SqlCommand()
            objCommandClient.Connection = objConnectionClient
            objCommandClient.CommandText = "UPDATE Users SET Password = @Password WHERE User_ID = @User_ID"
            objCommandClient.Parameters.AddWithValue("@Password", Crypto.ComputeMD5Hash(objDR("Password")))
            objCommandClient.Parameters.AddWithValue("@User_ID", objDR("User_ID"))
            objCommandClient.ExecuteNonQuery()

            objCommandClient = Nothing
            objConnectionClient.Close()
            objConnectionClient = Nothing
            HttpContext.Current.Trace.Write("TCA.vb Transfer", "User_ID " & objDR("User_ID") & " has been encrypted")
        End While
        HttpContext.Current.Trace.Write("TCA.vb Transfer", "End Loop")

        objDR.Close()
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("TCA.vb Transfer", "End Sub")
    End Sub
End Class
