Imports Microsoft.VisualBasic

Public Class Offices
    Public Shared Function OfficeName(ByVal intOfficeID As Int64) As String
        If intOfficeID <> 0 Then
            Dim objDROffice As System.Data.SqlClient.SqlDataReader
            Dim objCommandOffice As System.Data.SqlClient.SqlCommand
            Dim objConnectionOffice As System.Data.SqlClient.SqlConnection
            objConnectionOffice = New System.Data.SqlClient.SqlConnection

            objConnectionOffice.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionOffice.Open()
            objCommandOffice = New System.Data.SqlClient.SqlCommand()
            objCommandOffice.Connection = objConnectionOffice
            objCommandOffice.CommandText = "SELECT Name FROM OFFICES WHERE Office_ID = @intOfficeID"
            objCommandOffice.Parameters.AddWithValue("@intOfficeID", intOfficeID)

            objDROffice = objCommandOffice.ExecuteReader()
            objDROffice.Read()

            If objDROffice.HasRows Then
                OfficeName = objDROffice("Name")
            Else
                OfficeName = "Office Not Found"
            End If

            objDROffice = Nothing
            objCommandOffice = Nothing
            objConnectionOffice.Close()
            objConnectionOffice = Nothing
        Else
            OfficeName = ""
        End If
    End Function

    Public Shared Function ClientName(ByVal intClientID As Int64) As String
        If intClientID <> 0 Then
            Dim objDROffice As System.Data.SqlClient.SqlDataReader
            Dim objCommandOffice As System.Data.SqlClient.SqlCommand
            Dim objConnectionOffice As System.Data.SqlClient.SqlConnection
            objConnectionOffice = New System.Data.SqlClient.SqlConnection

            objConnectionOffice.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionOffice.Open()
            objCommandOffice = New System.Data.SqlClient.SqlCommand()
            objCommandOffice.Connection = objConnectionOffice
            objCommandOffice.CommandText = "SELECT CompanyName FROM Clients WHERE Client_ID = @intClientID"
            objCommandOffice.Parameters.AddWithValue("@intClientID", intClientID)

            objDROffice = objCommandOffice.ExecuteReader()
            objDROffice.Read()

            If objDROffice.HasRows Then
                ClientName = objDROffice("CompanyName")
            Else
                ClientName = "Company Name Not Found"
            End If

            objDROffice = Nothing
            objCommandOffice = Nothing
            objConnectionOffice.Close()
            objConnectionOffice = Nothing
        Else
            ClientName = ""
        End If
    End Function
End Class
