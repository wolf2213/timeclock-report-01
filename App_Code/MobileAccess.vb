Imports Microsoft.VisualBasic

Public Class MobileAccess
    Public Shared Sub Header(ByVal sbPage As StringBuilder)
        sbPage.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>")
        sbPage.Append("<html xmlns='http://www.w3.org/1999/xhtml' >")
        sbPage.Append("<head>")
        sbPage.Append("<title>TimeClockWizard</title>")
        sbPage.Append("<link rel='apple-touch-icon' href='images/apple-touch-icon.png'/>")
        sbPage.Append("</head>")
        sbPage.Append("<body bgcolor='#e1dfdf'>")
    End Sub

    Public Shared Sub Footer(ByVal sbPage As StringBuilder)
        sbPage.Append("</body>")
        sbPage.Append("</html>")
    End Sub
End Class
