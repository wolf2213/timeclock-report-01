Imports Microsoft.VisualBasic

Public Class Locations
    Public Shared Function LocationName(ByVal intLocationID As Int64) As String
        If intLocationID <> 0 Then
            Dim objDRLocation As System.Data.SqlClient.SqlDataReader
            Dim objCommandLocation As System.Data.SqlClient.SqlCommand
            Dim objConnectionLocation As System.Data.SqlClient.SqlConnection
            objConnectionLocation = New System.Data.SqlClient.SqlConnection

            objConnectionLocation.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionLocation.Open()
            objCommandLocation = New System.Data.SqlClient.SqlCommand()
            objCommandLocation.Connection = objConnectionLocation
            objCommandLocation.CommandText = "SELECT Name FROM Locations WHERE Location_ID = @Location_ID"
            objCommandLocation.Parameters.AddWithValue("@Location_ID", intLocationID)

            objDRLocation = objCommandLocation.ExecuteReader()
            objDRLocation.Read()

            If objDRLocation.HasRows Then
                LocationName = objDRLocation("Name")
            Else
                LocationName = "Location Not Found"
            End If

            objDRLocation = Nothing
            objCommandLocation = Nothing
            objConnectionLocation.Close()
            objConnectionLocation = Nothing
        Else
            LocationName = "Unverified"
        End If

    End Function

    Public Shared Function LocationType(ByVal intLocationID As Int64) As Integer
        '1 = Cookie, 2 = IP Address, 3 = Phone Caller ID, 4 = Mobile Access
        If intLocationID <> 0 And intLocationID <> 98989898 Then
            Dim objDRLocation As System.Data.SqlClient.SqlDataReader
            Dim objCommandLocation As System.Data.SqlClient.SqlCommand
            Dim objConnectionLocation As System.Data.SqlClient.SqlConnection
            objConnectionLocation = New System.Data.SqlClient.SqlConnection

            objConnectionLocation.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionLocation.Open()
            objCommandLocation = New System.Data.SqlClient.SqlCommand()
            objCommandLocation.Connection = objConnectionLocation
            objCommandLocation.CommandText = "SELECT Type FROM Locations WHERE Location_ID = @Location_ID"
            objCommandLocation.Parameters.AddWithValue("@Location_ID", intLocationID)

            objDRLocation = objCommandLocation.ExecuteReader()

            If objDRLocation.HasRows Then
                objDRLocation.Read()
                LocationType = objDRLocation("Type")
            Else
                LocationType = 0
            End If

            objDRLocation = Nothing
            objCommandLocation = Nothing
            objConnectionLocation.Close()
            objConnectionLocation = Nothing
        ElseIf intLocationID = 98989898 Then
            LocationType = 4
        Else
            LocationType = 0
        End If
    End Function

    Public Shared Function LocationID(ByVal intClient_ID As Int64, ByVal strLocation As String)
        HttpContext.Current.Trace.Write("Locations", "Begin Locations.LocationID")

        If strLocation <> "MobileAccess" Then
            Dim strSQLQuery As String

            If Left(strLocation, 2) = "CP" Then
                strLocation = Right(strLocation, Len(strLocation) - 2)
                strSQLQuery = "SELECT Location_ID FROM Locations WHERE Client_ID = @Client_ID AND Location_ID = @Location"
            Else
                strSQLQuery = "SELECT Location_ID FROM Locations WHERE Client_ID = @Client_ID AND Location = @Location"
            End If

            Dim objDRLocation As System.Data.SqlClient.SqlDataReader
            Dim objCommandLocation As System.Data.SqlClient.SqlCommand
            Dim objConnectionLocation As System.Data.SqlClient.SqlConnection
            objConnectionLocation = New System.Data.SqlClient.SqlConnection

            objConnectionLocation.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionLocation.Open()
            objCommandLocation = New System.Data.SqlClient.SqlCommand()
            objCommandLocation.Connection = objConnectionLocation
            objCommandLocation.CommandText = strSQLQuery
            objCommandLocation.Parameters.AddWithValue("@Client_ID", intClient_ID)
            objCommandLocation.Parameters.AddWithValue("@Location", strLocation)

            objDRLocation = objCommandLocation.ExecuteReader()

            If objDRLocation.HasRows Then
                objDRLocation.Read()
                HttpContext.Current.Trace.Write("Locations", "Location Found = " & objDRLocation("Location_ID"))
                LocationID = objDRLocation("Location_ID")
            Else
                HttpContext.Current.Trace.Write("Locations", "Location NOT Found = 0")
                LocationID = 0
            End If

            objDRLocation = Nothing
            objCommandLocation = Nothing
            objConnectionLocation.Close()
            objConnectionLocation = Nothing
            HttpContext.Current.Trace.Write("Locations", "End Locations.LocationID")
        Else
            LocationID = 98989898
        End If
    End Function
End Class
