Imports Microsoft.VisualBasic

Public Class AbsencesClass
    Public Shared Function AbsenceNameFromType(ByVal status As Integer)
        Select Case status
            Case 1
                AbsenceNameFromType = "Vacation"
            Case 2
                AbsenceNameFromType = "Holiday"
            Case 3
                AbsenceNameFromType = "Sick Day"
            Case 4
                AbsenceNameFromType = "Personal"
            Case 5
                AbsenceNameFromType = "Paid Leave"
            Case 6
                AbsenceNameFromType = "Other"
            Case Else
                AbsenceNameFromType = "Unknown"
        End Select
    End Function

    Public Shared Function AbsenceCodeFromType(ByVal status As Integer)
        Select Case status
            Case 1
                AbsenceCodeFromType = "VA"
            Case 2
                AbsenceCodeFromType = "HO"
            Case 3
                AbsenceCodeFromType = "SI"
            Case 4
                AbsenceCodeFromType = "PE"
            Case 5
                AbsenceCodeFromType = "PA"
            Case 6
                AbsenceCodeFromType = "OT"
            Case Else
                AbsenceCodeFromType = "UN"
        End Select
    End Function

    Public Shared Function AbsenceNameFromCode(ByVal strCode As String)
        Select Case strCode
            Case "VA"
                AbsenceNameFromCode = "Vacation"
            Case "HO"
                AbsenceNameFromCode = "Holiday"
            Case "SI"
                AbsenceNameFromCode = "Sick Day"
            Case "PE"
                AbsenceNameFromCode = "Personal"
            Case "PA"
                AbsenceNameFromCode = "Paid Leave"
            Case "OT"
                AbsenceNameFromCode = "Other"
            Case Else
                AbsenceNameFromCode = "Unknown"
        End Select
    End Function

    Public Shared Sub AccruePercentageTime(ByVal intUserID As Int64, ByVal dblHours As Double)
        HttpContext.Current.Trace.Write("AccruePercentageTime", "Begin accruing absence time")
        Dim strSQLQuery As String = ""

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT VacationEnabled, VacationTotal, VacationRate, HolidayEnabled, HolidayTotal, HolidayRate, SickDayEnabled, SickDayTotal, SickDayRate, PersonalEnabled, PersonalTotal, PersonalRate, PaidLeaveEnabled, PaidLeaveTotal, PaidLeaveRate, OtherEnabled, OtherTotal, OtherRate FROM AbsenceUserSettings WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            HttpContext.Current.Trace.Write("AccruePercentageTime", "Check Vacation")
            If objDR("VacationEnabled") = 1 Then
                If strSQLQuery <> "" Then
                    strSQLQuery = strSQLQuery & ", "
                End If
                HttpContext.Current.Trace.Write("AccruePercentageTime", "Add Vacation time")
                HttpContext.Current.Trace.Write("AccruePercentageTime", "Marginal Time: " & dblHours & "*" & objDR("VacationRate") & "/100 =  " & (dblHours * (objDR("VacationRate") / 100)))
                strSQLQuery = strSQLQuery & "VacationTotal = " & System.Math.Round(CDbl(objDR("VacationTotal")) + CDbl(dblHours * (objDR("VacationRate") / 100)), 3)
            End If

            HttpContext.Current.Trace.Write("AccruePercentageTime", "Check Holiday")
            If objDR("HolidayEnabled") = 1 Then
                If strSQLQuery <> "" Then
                    strSQLQuery = strSQLQuery & ", "
                End If
                HttpContext.Current.Trace.Write("AccruePercentageTime", "Add Holiday time")
                strSQLQuery = strSQLQuery & "HolidayTotal = " & System.Math.Round(CDbl(objDR("HolidayTotal")) + CDbl(dblHours * (objDR("HolidayRate") / 100)), 3)
            End If

            HttpContext.Current.Trace.Write("AccruePercentageTime", "Check Sick Day")
            If objDR("SickDayEnabled") = 1 Then
                If strSQLQuery <> "" Then
                    strSQLQuery = strSQLQuery & ", "
                End If
                HttpContext.Current.Trace.Write("AccruePercentageTime", "Add Sick Day time")
                strSQLQuery = strSQLQuery & "SickDayTotal = " & System.Math.Round(CDbl(objDR("SickDayTotal")) + CDbl(dblHours * (objDR("SickDayRate") / 100)), 3)
            End If

            HttpContext.Current.Trace.Write("AccruePercentageTime", "Check Personal")
            If objDR("PersonalEnabled") = 1 Then
                If strSQLQuery <> "" Then
                    strSQLQuery = strSQLQuery & ", "
                End If
                HttpContext.Current.Trace.Write("AccruePercentageTime", "Add Personal time")
                strSQLQuery = strSQLQuery & "PersonalTotal = " & System.Math.Round(CDbl(objDR("PersonalTotal")) + CDbl(dblHours * (objDR("PersonalRate") / 100)), 3)
            End If

            HttpContext.Current.Trace.Write("AccruePercentageTime", "Check PaidLeave")
            If objDR("PaidLeaveEnabled") = 1 Then
                If strSQLQuery <> "" Then
                    strSQLQuery = strSQLQuery & ", "
                End If
                HttpContext.Current.Trace.Write("AccruePercentageTime", "Add Paid Leave time")
                strSQLQuery = strSQLQuery & "PaidLeaveTotal = " & System.Math.Round(CDbl(objDR("PaidLeaveTotal")) + CDbl(dblHours * (objDR("PaidLeaveRate") / 100)), 3)
            End If

            HttpContext.Current.Trace.Write("AccruePercentageTime", "Check Other")
            If objDR("OtherEnabled") = 1 Then
                If strSQLQuery <> "" Then
                    strSQLQuery = strSQLQuery & ", "
                End If
                HttpContext.Current.Trace.Write("AccruePercentageTime", "Add Other time")
                strSQLQuery = strSQLQuery & "OtherTotal = " & System.Math.Round(CDbl(objDR("OtherTotal")) + CDbl(dblHours * (objDR("OtherRate") / 100)), 3)
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            If strSQLQuery <> "" Then

                strSQLQuery = "UPDATE AbsenceUserSettings SET " & strSQLQuery & " WHERE User_ID = @User_ID"

                HttpContext.Current.Trace.Write("AccruePercentageTime", "Execute Query: " & strSQLQuery)

                objConnection = New System.Data.SqlClient.SqlConnection
                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = strSQLQuery
                objCommand.Parameters.AddWithValue("@User_ID", intUserID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                HttpContext.Current.Trace.Write("AccruePercentageTime", "Applicable Times Updated")
            Else
                HttpContext.Current.Trace.Write("AccruePercentageTime", "Nothing To Update")
            End If

            HttpContext.Current.Trace.Write("AccruePercentageTime", "Exit")
        End If
    End Sub

    Public Shared Function AbsenceAvailable(ByVal intUserId As Int64, ByVal dtStart As Date, ByVal dtEnd As Date) As Boolean
        'this takes a start and end date and checks to see if there are any other absences during this time
        'you cannot have two absences overlapping in dates
        HttpContext.Current.Trace.Write("AbsenceAvailable", "Begin Function")
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Absence_ID FROM Absences WHERE User_ID = @User_ID AND Approved <> 2 AND NOT ((StartDate > @EndDate) OR (EndDate < @StartDate))"
        objCommand.Parameters.AddWithValue("@User_ID", intUserId)
        objCommand.Parameters.AddWithValue("@StartDate", dtStart)
        objCommand.Parameters.AddWithValue("@EndDate", dtEnd)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            'there is overlap
            objDR.Read()
            HttpContext.Current.Trace.Write("AbsenceAvailable", "At least one overlap found, absence_ID = " & objDR("Absence_ID"))
            AbsenceAvailable = False
        Else
            'there is not overlap
            HttpContext.Current.Trace.Write("AbsenceAvailable", "End Function")
            AbsenceAvailable = True
        End If
        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("AbsenceAvailable", "End Function")
    End Function

    Public Shared Sub AddAbsence(ByVal intUser_ID As Int64, ByVal intClient_ID As Int64, ByVal intAbsenceID As Int64, ByVal intManagerID As Int64, ByVal dblTextHours As Double)
        'NOTE THIS ADDS TO THE SCHEDULE _AND_ TIMERECORDS
        HttpContext.Current.Trace.Write("AddAbsence", "Process the request and add it to the schedule, Absence_ID = " & intAbsenceID)
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Type, StartDate, EndDate FROM Absences WHERE Absence_ID = @Absence_ID"
        objCommand.Parameters.AddWithValue("@Absence_ID", intAbsenceID)
        objDR = objCommand.ExecuteReader()
        objDR.Read()

        Dim intAbsenceType = objDR("Type")
        Dim strInputCode As String = AbsencesClass.AbsenceCodeFromType(intAbsenceType)
        Dim dtStartDate As DateTime = objDR("StartDate")
        Dim dtEndDate As DateTime = objDR("EndDate")

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("AddAbsence", "SQL Command executed, strInputCode = " & strInputCode & ", dtStartDate = " & dtStartDate & ", dtEndDate = " & dtEndDate)

        'calculate the end time for each day
        HttpContext.Current.Trace.Write("AddAbsence", "Calculating end time to set for each day...")
        Dim intNumberOfDays = DateDiff(DateInterval.Day, dtStartDate, dtEndDate) + 1
        Dim dblHours As Double = CDbl(dblTextHours)

        Dim dblHoursPerDay As Double = (dblHours / intNumberOfDays)
        Dim intHoursToInput As Integer = Fix(dblHoursPerDay)
        Dim intMinutesToInput As Integer = ((dblHoursPerDay - Fix(dblHoursPerDay)) * 60)

        HttpContext.Current.Trace.Write("AddAbsence", "dblHoursPerDay = " & dblHoursPerDay)
        HttpContext.Current.Trace.Write("AddAbsence", "intHoursToInput = " & intHoursToInput & ", intMinutesToInput =" & intMinutesToInput)
        'declare variables used in the loop

        Dim intScheduleIDToUpdate As Int64 = 0

        Dim dtLoopDate As DateTime = dtStartDate
        Dim intCurrentWeek As Integer
        Dim intCurrentyear As Integer
        Dim intCurrentMonth As Integer

        Dim strColStart As String = ""
        Dim strColEnd As String = ""
        Dim strColCode As String = ""

        Dim intCounter As Integer = 1
        'start updating/adding the times
        HttpContext.Current.Trace.Write("AddAbsence", "Start to add schedule with the While Loop")
        While dtLoopDate <= dtEndDate
            HttpContext.Current.Trace.Write("AddAbsence", "BEGIN LOOP " & intCounter & "...")
            intCurrentWeek = DatePart(DateInterval.WeekOfYear, dtLoopDate)
            intCurrentyear = DatePart(DateInterval.Year, dtLoopDate)
            intCurrentMonth = DatePart(DateInterval.Month, dtLoopDate)

            strColEnd = Time.WeekDayName(DatePart(DateInterval.Weekday, dtLoopDate), 0) & "End"
            strColStart = Time.WeekDayName(DatePart(DateInterval.Weekday, dtLoopDate), 0) & "Start"
            strColCode = Time.WeekDayName(DatePart(DateInterval.Weekday, dtLoopDate), 0) & "Code"

            HttpContext.Current.Trace.Write("AddAbsence", "Date used for input: " & intCurrentMonth & "/" & DatePart(DateInterval.Day, dtLoopDate) & "/" & intCurrentyear)

            Dim dtInputStart As DateTime = New System.DateTime(intCurrentyear, intCurrentMonth, DatePart(DateInterval.Day, dtLoopDate), 0, 0, 1)
            Dim dtInputEnd As DateTime = New System.DateTime(intCurrentyear, intCurrentMonth, DatePart(DateInterval.Day, dtLoopDate), intHoursToInput, intMinutesToInput, 1)


            HttpContext.Current.Trace.Write("AddAbsence", "Check to see if a schedule already exists for week " & intCurrentWeek)
            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT Schedule_ID FROM Schedules WHERE Year = @Year AND WeekNum = @WeekNum AND User_Id = @User_ID"
            objCommand.Parameters.AddWithValue("@Year", intCurrentyear)
            objCommand.Parameters.AddWithValue("@WeekNum", intCurrentWeek)
            objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()
                intScheduleIDToUpdate = objDR("Schedule_ID")
                HttpContext.Current.Trace.Write("AddAbsence", "Yes, There was a match => UPDATE ||| SQL SELECT ID: intScheduleIDToUpdate = " & intScheduleIDToUpdate)
            Else
                intScheduleIDToUpdate = 0
                HttpContext.Current.Trace.Write("AddAbsence", "There was no match => INSERT")
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing


            HttpContext.Current.Trace.Write("AddAbsence", "UPDATE IF " & intScheduleIDToUpdate & " <> 0")
            If intScheduleIDToUpdate = 0 Then
                'there is no schedule for the current week, so you must create one
                HttpContext.Current.Trace.Write("AddAbsence", "Begin INSERT...")

                'you have to get the start date of the current week so that you can put the dates in correctly for the insertion

                Dim dtBaseInsertDate As DateTime = Time.DatesOfWeek(intCurrentWeek, intCurrentyear, 0) & " 12:00:00 AM"

                'insert a schedule for this week
                objConnection = New System.Data.SqlClient.SqlConnection
                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "INSERT INTO Schedules (User_Id, Client_ID, Year, WeekNum, SunCode, SunStart, SunEnd, MonCode, MonStart, MonEnd, TueCode, TueStart, TueEnd, WedCode, WedStart, WedEnd, ThuCode, ThuStart, ThuEnd, FriCode, FriStart, FriEnd, SatCode, SatStart, SatEnd) VALUES (@User_ID, @Client_ID, @Year, @WeekNum, @SunCode, @SunStart, @SunEnd, @MonCode, @MonStart, @MonEnd, @TueCode, @TueStart, @TueEnd, @WedCode, @WedStart, @WedEnd, @ThuCode, @ThuStart, @ThuEnd, @FriCode, @FriStart, @FriEnd, @SatCode, @SatStart, @SatEnd)"
                objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)
                objCommand.Parameters.AddWithValue("@Client_ID", intClient_ID)
                objCommand.Parameters.AddWithValue("@Year", intCurrentyear)
                objCommand.Parameters.AddWithValue("@WeekNum", intCurrentWeek)
                objCommand.Parameters.AddWithValue("@SunCode", "NW")
                objCommand.Parameters.AddWithValue("@SunStart", dtBaseInsertDate)
                objCommand.Parameters.AddWithValue("@SunEnd", dtBaseInsertDate)
                objCommand.Parameters.AddWithValue("@MonCode", "NW")
                objCommand.Parameters.AddWithValue("@MonStart", DateAdd(DateInterval.Day, 1, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@MonEnd", DateAdd(DateInterval.Day, 1, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@TueCode", "NW")
                objCommand.Parameters.AddWithValue("@TueStart", DateAdd(DateInterval.Day, 2, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@TueEnd", DateAdd(DateInterval.Day, 2, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@WedCode", "NW")
                objCommand.Parameters.AddWithValue("@WedStart", DateAdd(DateInterval.Day, 3, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@WedEnd", DateAdd(DateInterval.Day, 3, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@ThuCode", "NW")
                objCommand.Parameters.AddWithValue("@ThuStart", DateAdd(DateInterval.Day, 4, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@ThuEnd", DateAdd(DateInterval.Day, 4, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@FriCode", "NW")
                objCommand.Parameters.AddWithValue("@FriStart", DateAdd(DateInterval.Day, 5, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@FriEnd", DateAdd(DateInterval.Day, 5, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@SatCode", "NW")
                objCommand.Parameters.AddWithValue("@SatStart", DateAdd(DateInterval.Day, 6, dtBaseInsertDate))
                objCommand.Parameters.AddWithValue("@SatEnd", DateAdd(DateInterval.Day, 6, dtBaseInsertDate))
                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                HttpContext.Current.Trace.Write("AddAbsence", "Schedule INSERTED for" & dtLoopDate)

                'the schedule has been inserted, now retrieve the ID and prepare for update

                objConnection = New System.Data.SqlClient.SqlConnection
                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT Schedule_ID FROM Schedules WHERE Year = @Year AND WeekNum = @WeekNum AND User_Id = @User_ID"
                objCommand.Parameters.AddWithValue("@Year", intCurrentyear)
                objCommand.Parameters.AddWithValue("@WeekNum", intCurrentWeek)
                objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

                objDR = objCommand.ExecuteReader()

                If objDR.HasRows Then
                    objDR.Read()
                    intScheduleIDToUpdate = objDR("Schedule_ID")
                End If

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                'ID has been retrieved from the inserted schedule
            End If

            'there is now definitely  a schedule for this week, update it
            HttpContext.Current.Trace.Write("AddAbsence", "Begin update process...")

            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "UPDATE Schedules SET [" & strColCode & "] = @strInputCode, [" & strColStart & "] = @dtStartInput, [" & strColEnd & "] = @dtEndInput WHERE Schedule_ID = @Schedule_ID"
            objCommand.Parameters.AddWithValue("@strInputCode", strInputCode)
            objCommand.Parameters.AddWithValue("@dtStartInput", dtInputStart)
            objCommand.Parameters.AddWithValue("@dtEndInput", dtInputEnd)
            objCommand.Parameters.AddWithValue("@Schedule_ID", intScheduleIDToUpdate)

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            HttpContext.Current.Trace.Write("AddAbsence", "SQL UPDATE ScheduleID " & dtLoopDate)
            'the absence has been added

            HttpContext.Current.Trace.Write("AddAbsence", "Add the timerecord for " & dtLoopDate)
            'add the time record

            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "INSERT INTO TimeRecords (User_ID, Client_ID, Absence_ID, Type, StartTime, StartLocation_ID, StartLocation, EndTime, EndLocation_ID, VerifiedBy, ManualBy) VALUES (@User_ID, @Client_ID, @Absence_ID, 4, @StartTime, 0, '', @EndTime, 0, @Manager_ID, @Manager_ID)"
            objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)
            objCommand.Parameters.AddWithValue("@Client_ID", intClient_ID)
            objCommand.Parameters.AddWithValue("@Absence_ID", intAbsenceID)
            objCommand.Parameters.AddWithValue("@StartTime", dtInputStart)
            objCommand.Parameters.AddWithValue("@EndTime", dtInputEnd)
            objCommand.Parameters.AddWithValue("@Manager_ID", intManagerID)

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            'loop through to the next day
            dtLoopDate = DateAdd(DateInterval.Day, 1, dtLoopDate)
            intCounter = intCounter + 1
        End While

        'the absence has been added to the schedule
        'now acutally approve the absence and set the hours used
        HttpContext.Current.Trace.Write("AddAbsence", "END LOOP")
        HttpContext.Current.Trace.Write("AddAbsence", "Schedule updated, now acutally approve the request")

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE Absences SET Hours = @Hours, Approved = 1, ApprovedDeniedBy = @Manager_ID WHERE Absence_ID = @Absence_ID"
        objCommand.Parameters.AddWithValue("@Hours", dblHours)
        objCommand.Parameters.AddWithValue("@Manager_Id", intManagerID)
        objCommand.Parameters.AddWithValue("@Absence_ID", intAbsenceID)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'update the employee's hours
        Dim strColToUpdate As String = Replace(AbsencesClass.AbsenceNameFromType(intAbsenceType), " ", "") & "Used"
        HttpContext.Current.Trace.Write("AddAbsence", "Update Employees Hours (col " & strColToUpdate & ")...")

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT [" & strColToUpdate & "] AS OldHours FROM AbsenceUserSettings WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)
        objDR = objCommand.ExecuteReader()
        objDR.Read()

        Dim dblUpdatedHours As Double = CDbl(objDR("OldHours") + dblHours)

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("AddAbsence", "Set them to dblUpdatedHours = " & dblUpdatedHours)

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE AbsenceUserSettings SET [" & strColToUpdate & "] = @Hours WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@Hours", dblUpdatedHours)
        objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Public Shared Sub DeleteAbsence(ByVal intAbsenceID As Int64, ByVal intUserID As Int64)
        'this does FOUR things
        ''''Gives the user back the hours that were initially negated by this absence
        ''''Deletes the absences scheduled from the schedule table (updates them to NW)
        ''''Deletes the timerecords created by this absence from the timerecords table
        ''''Deletes the absence from the asbence table
        HttpContext.Current.Trace.Write("DeleteAbsence", "BEGIN DELETE Absence_ID = " & intAbsenceID)

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Type, Hours, StartDate, EndDate FROM Absences WHERE Absence_ID = @Absence_ID"
        objCommand.Parameters.AddWithValue("@Absence_ID", intAbsenceID)
        objDR = objCommand.ExecuteReader()
        objDR.Read()

        Dim intType As Integer = objDR("Type")
        Dim dtStartDate As DateTime = objDR("StartDate")
        Dim dtEndDate As DateTime = objDR("EndDate")
        Dim dblHours As Double = objDR("Hours")

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'start hours proceess
        'the first step is to figure out what to update the hours to

        Dim sbHours As StringBuilder = New StringBuilder
        Dim strColName As String = Replace(AbsencesClass.AbsenceNameFromType(intType), " ", "")
        Dim dblCurrentHours As Double
        sbHours.Append("SELECT " & strColName & "Used AS CurrentUsed FROM AbsenceUserSettings WHERE User_ID = @User_ID")

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = sbHours.ToString
        objCommand.Parameters.AddWithValue("@Absence_ID", intAbsenceID)
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()
        objDR.Read()

        dblCurrentHours = objDR("CurrentUsed")

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("DeleteAbsence", "Current Hours Found, = " & dblCurrentHours)

        sbHours.Remove(0, sbHours.Length)

        sbHours.Append("UPDATE AbsenceUserSettings SET " & strColName & "Used = @NewUsed WHERE User_ID = @User_ID")

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = sbHours.ToString
        objCommand.Parameters.AddWithValue("@NewUsed", dblCurrentHours - dblHours)
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)

        objCommand.ExecuteNonQuery()

        sbHours = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
        HttpContext.Current.Trace.Write("DeleteAbsence", "Hours Updated Set to = " & dblCurrentHours - dblHours)

        'start schedule process
        Dim dtWhileDate As DateTime = dtStartDate
        Dim strColPrefix As String

        Dim intScheduleIDToUpdate = 0

        HttpContext.Current.Trace.Write("DeleteAbsence", "START the schedule update loop")
        While dtWhileDate <= dtEndDate
            'begin updating the schedule (set all days to NW where there was an absence)
            strColPrefix = WeekdayName(DatePart(DateInterval.Weekday, dtWhileDate), True)
            HttpContext.Current.Trace.Write("DeleteAbsence", "Update schedule day = " & dtWhileDate & " | strColPrefix = " & strColPrefix)

            Dim sbQuery As StringBuilder = New StringBuilder
            sbQuery.Append("UPDATE Schedules SET " & strColPrefix & "Code = 'NW', " & strColPrefix & "Start = '" & dtWhileDate.Date & " 12:00:00 AM', " & strColPrefix & "End = '" & dtWhileDate.Date & " 12:00:00 AM' WHERE Year = @Year AND WeekNum = @WeekNum AND User_Id = @User_ID")

            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = sbQuery.ToString
            objCommand.Parameters.AddWithValue("@Year", DatePart(DateInterval.Year, dtWhileDate))
            objCommand.Parameters.AddWithValue("@WeekNum", DatePart(DateInterval.WeekOfYear, dtWhileDate))
            objCommand.Parameters.AddWithValue("@User_ID", intUserID)

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            sbQuery.Remove(0, sbQuery.Length)
            dtWhileDate = DateAdd(DateInterval.Day, 1, dtWhileDate)
        End While
        HttpContext.Current.Trace.Write("DeleteAbsence", "STOP the schedule update while loop")

        'now delete the TimeRecords
        HttpContext.Current.Trace.Write("DeleteAbsence", "DELETE the timerecords")

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "DELETE FROM TimeRecords WHERE Absence_ID = @Absence_ID AND User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@Absence_ID", intAbsenceID)
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'now that the TimeRecords have been deleted, delete the actual absence from the ABSENCES table
        HttpContext.Current.Trace.Write("DeleteAbsence", "DELETE the actual absence")
        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "DELETE FROM Absences WHERE Absence_ID = @Absence_ID AND User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@Absence_ID", intAbsenceID)
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("DeleteAbsence", "END DELETE Absence_ID = " & intAbsenceID)
    End Sub
End Class