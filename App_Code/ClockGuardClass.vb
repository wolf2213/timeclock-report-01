Imports Microsoft.VisualBasic
Imports System.Net.mail

Public Class ClockGuardClass
    Public Shared Function CheckAlert(ByVal intUserID As Int64, ByVal intAlert As Integer) As Boolean
        Dim strSQLStatement As String = String.Empty
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        strSQLStatement = strSQLStatement + "SELECT U.ClockGuard, U.CGAlertWhen, UCGS.[ClocksInEarlyStart], UCGS.[ClocksInEarlyEnd], UCGS.[ClocksInLateStart] "
        strSQLStatement = strSQLStatement + ",UCGS.[ClocksInLateEnd], UCGS.[ClocksOutEarlyStart], UCGS.[ClocksOutEarlyEnd], UCGS.[ClocksOutLateStart], UCGS.[ClocksOutLateEnd] "
        strSQLStatement = strSQLStatement + "FROM Users U inner join dbo.[UserClockGuardSettings] UCGS on U.User_ID = UCGS.User_ID WHERE U.User_ID = @User_ID"
        objCommand.CommandText = strSQLStatement
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            If objDR("ClockGuard") = True Then
                Dim strGCAlertWhen As String = objDR("CGAlertWhen")

                If strGCAlertWhen = "" Or strGCAlertWhen = "0" Then
                    CheckAlert = False
                Else
                    Dim bitAlertValue As Integer = CInt(Right(Left(strGCAlertWhen, intAlert), 1))

                    If bitAlertValue = 1 Then
                        CheckAlert = True
                    Else
                        CheckAlert = False
                    End If
                End If
            Else
                CheckAlert = False
            End If
        Else
            'user could not be found
            CheckAlert = False
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Function

    Public Shared Function StripPhoneFormatting(ByVal strPhone As String)
        HttpContext.Current.Trace.Write("StripPhoneFormatting", "Begin ClockGuardClass.StripPhoneFormatting")
        strPhone = Trim(strPhone)
        strPhone = Replace(strPhone, " ", "")
        strPhone = Replace(strPhone, "(", "")
        strPhone = Replace(strPhone, ")", "")
        strPhone = Replace(strPhone, "-", "")
        strPhone = Replace(strPhone, ".", "")
        strPhone = Replace(strPhone, "#", "")
        strPhone = Replace(strPhone, "+", "")
        StripPhoneFormatting = strPhone
        HttpContext.Current.Trace.Write("StripPhoneFormatting", "End ClockGuardClass.StripPhoneFormatting")
    End Function

    Public Shared Function SMSEmail(ByVal intUserID As Int64, ByVal strPhone As String, Optional ByVal intPrimaryTextMessage As Int16 = 1)
        HttpContext.Current.Trace.Write("SMSEmail", "Begin ClockGuardClass.SMSEmail")
        If Not IsNumeric(strPhone) Then
            strPhone = StripPhoneFormatting(strPhone)
        End If

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        If intPrimaryTextMessage = 1 Then
        objCommand.CommandText = "SELECT CGCarrier FROM Users WHERE User_ID = @User_ID"
        Else
            objCommand.CommandText = "SELECT CGCarrier2 as CGCarrier FROM UserClockGuardSettings WHERE User_ID = @User_ID"
        End If
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            Select Case objDR("CGCarrier")
                Case 1
                    SMSEmail = strPhone & "@txt.att.net"
                Case 2
                    SMSEmail = strPhone & "@vtext.com"
                Case 3
                    SMSEmail = strPhone & "@messaging.sprintpcs.com"
                Case 4
                    SMSEmail = strPhone & "@tmomail.net"
                Case 5
                    SMSEmail = strPhone & "@messaging.nextel.com"
                Case 6
                    SMSEmail = strPhone & "@message.alltel.com"
                Case 7
                    SMSEmail = strPhone & "@mymetropcs.com"
                Case Else
                    SMSEmail = "Invalid Carrier"
            End Select
        Else
            HttpContext.Current.Trace.Warn("SMSEmail", "Could not find the domain to add to the phone number")
            SMSEmail = "DB Row not found"
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("SMSEmail", "End ClockGuardClass.SMSEmail")
    End Function

    Public Shared Sub CheckToAlert(ByVal intUserID As Int64, ByVal intInOrOut As Integer, Optional ByVal intTardyType As Integer = 0)
        'this sub checks to see if the manager should actually be alerted
        'if intInOrOut = 1 then they clocking in
        'if it is equal to 2, clock them out

        'note: the intTardyType is coming from the Tardy table in the dB
        '1=in too early, 2=in too late, 3= out too early, 4 = out too late

        HttpContext.Current.Trace.Write("CheckToAlert", "Begin ClockGuardClass.CheckToAlert")

        Dim boolCheckAlertType = False

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT ClockGuard FROM Users WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            If Not objDR("ClockGuard") Is DBNull.Value Then
                If objDR("ClockGuard") Then
                    boolCheckAlertType = True
                    HttpContext.Current.Trace.Write("CheckToAlert", "User has ClockGuard Enabled")
                End If
            End If
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If boolCheckAlertType = True Then
            If intInOrOut = 1 Then
                HttpContext.Current.Trace.Write("CheckToAlert", "Checking CG Preferences for CLOCK IN...")
                If CheckAlert(intUserID, 1) Then
                    'Alert them anytime they clock in, so alert them now because they are clocking in
                    HttpContext.Current.Trace.Write("CheckToAlert", "FOUND: Alert always at clock in")
                    AlertManager(intUserID, intInOrOut)
                Else
                    If CheckAlert(intUserID, 2) And intTardyType = 1 Then
                        'in and early + send
                        HttpContext.Current.Trace.Write("CheckToAlert", "FOUND: Alert When [Clock In + Early]")
                        AlertManager(intUserID, intInOrOut)
                    End If

                    If CheckAlert(intUserID, 3) And intTardyType = 2 Then
                        'in and late + send
                        HttpContext.Current.Trace.Write("CheckToAlert", "FOUND: Alert When [Clock In + Late]")
                        AlertManager(intUserID, intInOrOut)
                    End If
                End If
                HttpContext.Current.Trace.Write("CheckToAlert", "End Checking CG Preferences for CLOCK IN")
            ElseIf intInOrOut = 2 Then
                HttpContext.Current.Trace.Write("CheckToAlert", "Checking CG Preferences for CLOCK OUT...")
                If CheckAlert(intUserID, 4) Then
                    'Alert them anytime they clock out, so alert them now because they are clocking out
                    HttpContext.Current.Trace.Write("CheckToAlert", "FOUND: Alert always at clock out")
                    AlertManager(intUserID, intInOrOut)
                Else
                    If CheckAlert(intUserID, 5) And intTardyType = 3 Then
                        'out and early + send
                        HttpContext.Current.Trace.Write("CheckToAlert", "FOUND: Alert When [Clock Out + Early]")
                        AlertManager(intUserID, intInOrOut)
                    End If

                    If CheckAlert(intUserID, 6) And intTardyType = 4 Then
                        'out and late + send
                        HttpContext.Current.Trace.Write("CheckToAlert", "FOUND: Alert When [Clock Out + Late]")
                        AlertManager(intUserID, intInOrOut)
                    End If
                End If
                HttpContext.Current.Trace.Write("CheckToAlert", "End Checking CG Preferences for CLOCK OUT")
            End If
        End If

        HttpContext.Current.Trace.Write("CheckToAlert", "End ClockGuardClass.CheckToAlert")
    End Sub

    Public Shared Sub AlertManager(ByVal intUserID As Int64, ByVal intInOrOut As Integer)
        'this sub alerts the manager.  it is called by the sub CheckToAlert
        'by this time, it has already been determined that CG needs to alert the manager
        HttpContext.Current.Trace.Write("AlertManager", "Begin ClockGuardClass.AlertManager")

        Dim boolEmail As Boolean = False
        Dim boolText As Boolean = False
        Dim boolEmail2 As Boolean = False
        Dim boolText2 As Boolean = False

        Dim strEmail As String
        Dim strPhone As String
        Dim strEmail2 As String
        Dim strPhone2 As String
        Dim strScheduleSuffix As String

        Dim strInOrOut As String
        If intInOrOut = 1 Then
            strInOrOut = "In"
            strScheduleSuffix = "Start"
        Else
            strInOrOut = "Out"
            strScheduleSuffix = "End"
        End If

        Dim strUsersName As String = UserInfo.NameFromID(intUserID, 1)

        'get the email/phone to alert
        HttpContext.Current.Trace.Write("AlertManager", "Find Email/Phone# to alert...")
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT U.CGEmail, U.CGPhone, UCGS.CGEmail2, UCGS.CGPhone2 FROM Users U LEFT JOIN UserClockGuardSettings UCGS ON U.User_ID = UCGS.User_ID WHERE U.User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            If Not objDR("CGEmail") Is DBNull.Value Then
                If objDR("CGEmail") <> "" Then
                    boolEmail = True
                    strEmail = objDR("CGEmail")
                    HttpContext.Current.Trace.Write("AlertManager", "Email Address Found = " & strEmail)
                End If
            End If

            If Not objDR("CGPhone") Is DBNull.Value Then
                If objDR("CGPhone") <> "" Then
                    boolText = True
                    strPhone = SMSEmail(intUserID, objDR("CGPhone"))
                    HttpContext.Current.Trace.Write("AlertManager", "Phone Number Found = " & strPhone)
                End If
            End If
            If Not objDR("CGEmail2") Is DBNull.Value Then
                If objDR("CGEmail2") <> "" Then
                    boolEmail2 = True
                    strEmail2 = objDR("CGEmail2")
                    HttpContext.Current.Trace.Write("AlertManager", "Email Address Found = " & strEmail2)
                End If
            End If
            If Not objDR("CGPhone2") Is DBNull.Value Then
                If objDR("CGPhone2") <> "" Then
                    boolText2 = True
                    strPhone2 = SMSEmail(intUserID, objDR("CGPhone2"), 0)
                    HttpContext.Current.Trace.Write("AlertManager", "Phone Number Found = " & strPhone2)
                End If
            End If
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        'get the scheduled time in/out
        HttpContext.Current.Trace.Write("AlertManager", "Find scheduled time in/out to put in the email/text message")
        Dim intWeekNum As Integer = DatePart(DateInterval.WeekOfYear, Clock.GetNow())
        Dim intYear As Integer = DatePart(DateInterval.Year, Clock.GetNow())
        Dim strDayPrefix As String = Time.WeekDayName(DatePart(DateInterval.Weekday, Clock.GetNow()), 0)
        Dim strScheduleCodeCol As String = strDayPrefix & "Code"
        Dim strScheduleTimeCol As String = strDayPrefix & strScheduleSuffix

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT " & strScheduleCodeCol & " AS DayCode, " & strScheduleTimeCol & " AS ScheduledTime FROM Schedules WHERE User_ID = @User_ID AND WeekNum = @WeekNum AND Year = @Year"
        objCommand.Parameters.AddWithValue("@WeekNum", intWeekNum)
        objCommand.Parameters.AddWithValue("@Year", intYear)
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()

        Dim strSchTime As String

        If objDR.HasRows Then
            objDR.Read()

            If objDR("DayCode") = "NW" Then
                'the person is not working this day
                strSchTime = "Not Working"
            ElseIf objDR("DayCode") = "AB" Then
                'scheduled absence
                strSchTime = "Scheduled Absence"
            ElseIf objDR("DayCode") = "0" Then
                'working, not assigned an office
                strSchTime = Time.QuickStrip(objDR("ScheduledTime"))
            Else
                'working and assigned office
                strSchTime = Time.QuickStrip(objDR("ScheduledTime"))
            End If
        Else
            strSchTime = "Not Scheduled"
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("AlertManager", "scheduled time found = " & strSchTime)

        'send the alerts
        If boolEmail Then
            'EMAIL
            'create the mail message
            Dim mail As New MailMessage()

            'set the addresses
            mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
            mail.To.Add(strEmail)

            'set the content
            mail.Subject = "ClockGuard Alert For " & strUsersName
            mail.Body = strUsersName & "<br><br>Clocked " & strInOrOut & " At " & Time.QuickStrip(Clock.GetNow()) & "<br>Sheduled Time " & strInOrOut & ": " & strSchTime
            mail.IsBodyHtml = True

            'send the message
            Dim smtp As New SmtpClient()
            smtp.Send(mail)
            HttpContext.Current.Trace.Write("AlertManager", "Email Sent")
        End If

        If boolText Then
            'TEXT
            'create the mail message
            Dim mail As New MailMessage()

            'set the addresses
            mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
            HttpContext.Current.Trace.Warn(strPhone)
            mail.To.Add(strPhone)

            'set the content
            mail.Body = strUsersName & "<br><br>Clocked " & strInOrOut & " At " & Time.QuickStrip(Clock.GetNow()) & "<br>Sheduled Time " & strInOrOut & ": " & strSchTime
            mail.IsBodyHtml = True

            'send the message
            Dim smtp As New SmtpClient()
            smtp.Send(mail)
            HttpContext.Current.Trace.Write("AlertManager", "Text Message Sent")
            'Below code is mainly for testing. When client complains about not receiving clockgaurd text alerts.
            'When issue is resolved comment the code. Passing client id = 1 for testing
            'Audit.CreateNewClockitinAudit(1, intUserID, "NA", "NA", mail.Body)
        End If

        'send the alerts
        If boolEmail2 Then
            'EMAIL
            'create the mail message
            Dim mail As New MailMessage()

            'set the addresses
            mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
            mail.To.Add(strEmail2)

            'set the content
            mail.Subject = "ClockGuard Alert For " & strUsersName
            mail.Body = strUsersName & "<br><br>Clocked " & strInOrOut & " At " & Time.QuickStrip(Clock.GetNow()) & "<br>Sheduled Time " & strInOrOut & ": " & strSchTime
            mail.IsBodyHtml = True

            'send the message
            Dim smtp As New SmtpClient()
            smtp.Send(mail)
            HttpContext.Current.Trace.Write("AlertManager", "Email Sent")
        End If
        If boolText2 Then
            'TEXT
            'create the mail message
            Dim mail As New MailMessage()

            'set the addresses
            mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
            HttpContext.Current.Trace.Warn(strPhone2)
            mail.To.Add(strPhone2)

            'set the content
            mail.Body = strUsersName & "<br><br>Clocked " & strInOrOut & " At " & Time.QuickStrip(Clock.GetNow()) & "<br>Sheduled Time " & strInOrOut & ": " & strSchTime
            mail.IsBodyHtml = True

            'send the message
            Dim smtp As New SmtpClient()
            smtp.Send(mail)
            HttpContext.Current.Trace.Write("AlertManager", "Text Message Sent")
            'Below code is mainly for testing. When client complains about not receiving clockgaurd text alerts.
            'When issue is resolved comment the code. Passing client id = 1 for testing
            'Audit.CreateNewClockitinAudit(1, intUserID, "NA", "NA", mail.Body)
        End If
        HttpContext.Current.Trace.Write("AlertManager", "End ClockGuardClass.AlertManager")
    End Sub
    Public Shared Function getClockGaurdAllowedTime(ByVal intUserID As Int64, ByVal boolEarly As Boolean, ByVal intIn1OrOut2 As Int16) As Int32
        Dim strSQLStatement As String = String.Empty
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        strSQLStatement = strSQLStatement + "SELECT U.ClockGuard, U.CGAlertWhen, UCGS.[ClocksInEarlyStart], UCGS.[ClocksInEarlyEnd], UCGS.[ClocksInLateStart] "
        strSQLStatement = strSQLStatement + ",UCGS.[ClocksInLateEnd], UCGS.[ClocksOutEarlyStart], UCGS.[ClocksOutEarlyEnd], UCGS.[ClocksOutLateStart], UCGS.[ClocksOutLateEnd] "
        strSQLStatement = strSQLStatement + "FROM Users U inner join dbo.[UserClockGuardSettings] UCGS on U.User_ID = UCGS.User_ID WHERE U.User_ID = @User_ID"
        objCommand.CommandText = strSQLStatement
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objDR = objCommand.ExecuteReader()
        If objDR.HasRows Then
            objDR.Read()
            If objDR("ClockGuard") = True Then
                Dim strGCAlertWhen As String = objDR("CGAlertWhen")
                If strGCAlertWhen = "" Or strGCAlertWhen = "0" Then
                    getClockGaurdAllowedTime = 0
                Else
                    If boolEarly Then
                        If intIn1OrOut2 = 1 Then
                            getClockGaurdAllowedTime = objDR("ClocksInEarlyStart")
                        Else
                            getClockGaurdAllowedTime = objDR("ClocksOutEarlyStart")
                        End If
                    Else
                        If intIn1OrOut2 = 1 Then
                            getClockGaurdAllowedTime = objDR("ClocksInLateStart")
                        Else
                            getClockGaurdAllowedTime = objDR("ClocksOutLateStart")
                        End If
                    End If
                End If
            Else
                getClockGaurdAllowedTime = 0
            End If
        Else
            'user could not be found
            getClockGaurdAllowedTime = 0
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Function
End Class