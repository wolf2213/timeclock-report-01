﻿Imports Microsoft.VisualBasic
Imports System.Text.RegularExpressions
Imports Microsoft.Practices.EnterpriseLibrary
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class Setting
    Inherits Connection
    Public Function sprocUpdatePreferences(ByVal ClientID As Int64, ByVal QuickClockIn As Boolean, ByVal QuickClockInPassword As Boolean, ByVal SelfRegistration As Boolean, ByVal SelfRegistrationPin As String, ByVal WhosInRestriction As Boolean, ByVal PaidBreaks As Boolean, ByVal TimeRounding As Integer, ByVal MileageRate As Double, ByVal DashboardMessageText As String, ByVal DashboardMessageRed As Boolean, ByVal DashboardMessageBold As Boolean, ByVal LoginMessageText As String, ByVal LoginMessageRed As Boolean, ByVal LoginMessageBold As Boolean, ByVal BirthdayNotification As Boolean, ByVal OverTimetype As Integer, ByVal OvertimeDailyHours As Double, ByVal OvertimeWeeklyHours As Double, ByVal ApproveOvertime As Boolean, ByVal Subdomain As String, ByRef IsSaved As Int64) As Integer
        Dim objCommand As New SqlCommand("sprocUpdatePreferences")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        objCommand.Parameters.AddWithValue("@QuickClockIn", QuickClockIn)
        objCommand.Parameters.AddWithValue("@QuickClockInPassword", QuickClockInPassword)
        objCommand.Parameters.AddWithValue("@SelfRegistration", SelfRegistration)
        objCommand.Parameters.AddWithValue("@SelfRegistrationPin", SelfRegistrationPin)
        objCommand.Parameters.AddWithValue("@WhosInRestriction", WhosInRestriction)
        objCommand.Parameters.AddWithValue("@PaidBreaks", PaidBreaks)
        objCommand.Parameters.AddWithValue("@TimeRounding", TimeRounding)
        objCommand.Parameters.AddWithValue("@MileageRate", MileageRate)
        objCommand.Parameters.AddWithValue("@DashboardMessageText", DashboardMessageText)
        objCommand.Parameters.AddWithValue("@DashboardMessageRed", DashboardMessageRed)
        objCommand.Parameters.AddWithValue("@DashboardMessageBold", DashboardMessageBold)
        objCommand.Parameters.AddWithValue("@LoginMessageText", LoginMessageText)
        objCommand.Parameters.AddWithValue("@LoginMessageRed", LoginMessageRed)
        objCommand.Parameters.AddWithValue("@LoginMessageBold", LoginMessageBold)
        objCommand.Parameters.AddWithValue("@BirthdayNotification", BirthdayNotification)
        objCommand.Parameters.AddWithValue("@OverTimetype", OverTimetype)
        objCommand.Parameters.AddWithValue("@OvertimeDailyHours", OvertimeDailyHours)
        objCommand.Parameters.AddWithValue("@OvertimeWeeklyHours", OvertimeWeeklyHours)
        objCommand.Parameters.AddWithValue("@ApproveOvertime", ApproveOvertime)
        objCommand.Parameters.AddWithValue("@Subdomain", Subdomain)
        objCommand.Parameters.AddWithValue("@IsSaved", IsSaved)
        objCommand.Parameters("@IsSaved").Direction = ParameterDirection.Output
        Try
            IsSaved = _db.ExecuteNonQuery(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
        Return IsSaved
    End Function

    Public Function sprocGetPreferences(ByVal ClientID As Int64) As DataSet
        Dim ds As New DataSet
        Dim objCommand As New SqlCommand("sprocGetPreferences")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        Try
            ds = _db.ExecuteDataSet(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
        Return ds
    End Function

    Public Function sprocUpdatePreferencesLogo(ByVal ClientID As Int64, ByVal Logo() As Byte, ByVal LogoWidth As Integer, ByVal LogoHeight As Integer, ByVal MIMEType As String, ByRef IsSaved As Integer) As Integer
        Dim objCommand As New SqlCommand("sprocUpdatePreferencesLogo")
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@ClientID", ClientID)
        objCommand.Parameters.AddWithValue("@Logo", Logo)
        objCommand.Parameters.AddWithValue("@LogoWidth", LogoWidth)
        objCommand.Parameters.AddWithValue("@LogoHeight", LogoHeight)
        objCommand.Parameters.AddWithValue("@MIMEType", MIMEType)
        objCommand.Parameters.AddWithValue("@IsSaved", IsSaved)
        objCommand.Parameters("@IsSaved").Direction = ParameterDirection.Output
        Try
            IsSaved = _db.ExecuteNonQuery(objCommand)
        Catch ex As Exception
            Throw ex
        End Try
        Return IsSaved
    End Function
End Class

