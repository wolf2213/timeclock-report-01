Imports Microsoft.VisualBasic

Public Class Permissions
    Public Shared Function AccountHolder(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        AccountHolder = Permissions.CheckPermission(intUser_ID, "AccountHolder")
    End Function

    Public Shared Function ManageEmployees(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        ManageEmployees = Permissions.CheckPermission(intUser_ID, "ManageEmployees")
    End Function

    Public Shared Function TimesheetsAdd(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        TimesheetsAdd = Permissions.CheckPermission(intUser_ID, "TimesheetsAdd")
    End Function

    Public Shared Function TimesheetsDisplay(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        TimesheetsDisplay = Permissions.CheckPermission(intUser_ID, "TimesheetsDisplay")
    End Function

    Public Shared Function TimesheetsValidate(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        TimesheetsValidate = Permissions.CheckPermission(intUser_ID, "TimesheetsValidate")
    End Function

    Public Shared Function TimesheetsEdit(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        TimesheetsEdit = Permissions.CheckPermission(intUser_ID, "TimesheetsEdit")
    End Function

    Public Shared Function TimesheetsDelete(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        TimesheetsDelete = Permissions.CheckPermission(intUser_ID, "TimesheetsDelete")
    End Function

    Public Shared Function MileageDisplay(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        MileageDisplay = Permissions.CheckPermission(intUser_ID, "MileageDisplay")
    End Function

    Public Shared Function MileageValidate(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        MileageValidate = Permissions.CheckPermission(intUser_ID, "MileageValidate")
    End Function

    Public Shared Function MileageDelete(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        MileageDelete = Permissions.CheckPermission(intUser_ID, "MileageDelete")
    End Function

    Public Shared Function ScheduleEdit(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        ScheduleEdit = Permissions.CheckPermission(intUser_ID, "ScheduleEdit")
    End Function

    Public Shared Function ScheduleCopy(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        ScheduleCopy = Permissions.CheckPermission(intUser_ID, "ScheduleCopy")
    End Function

    Public Shared Function ScheduleDelete(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        ScheduleDelete = Permissions.CheckPermission(intUser_ID, "ScheduleDelete")
    End Function

    Public Shared Function AbsencesAdd(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        AbsencesAdd = Permissions.CheckPermission(intUser_ID, "AbsencesAdd")
    End Function

    Public Shared Function AbsencesApprove(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        AbsencesApprove = Permissions.CheckPermission(intUser_ID, "AbsencesApprove")
    End Function

    Public Shared Function AbsencesDelete(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        AbsencesDelete = Permissions.CheckPermission(intUser_ID, "AbsencesDelete")
    End Function

    Public Shared Function AbsencesSettings(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        AbsencesSettings = Permissions.CheckPermission(intUser_ID, "AbsencesSettings")
    End Function

    Public Shared Function PayrollAdd(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        PayrollAdd = Permissions.CheckPermission(intUser_ID, "PayrollAdd")
    End Function

    Public Shared Function PayrollDelete(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        PayrollDelete = Permissions.CheckPermission(intUser_ID, "PayrollDelete")
    End Function

    Public Shared Function PayrollView(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        PayrollView = Permissions.CheckPermission(intUser_ID, "PayrollView")
    End Function

    Public Shared Function UsersEdit(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersEdit = Permissions.CheckPermission(intUser_ID, "UsersEdit")
    End Function

    Public Shared Function UsersDelete(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersDelete = Permissions.CheckPermission(intUser_ID, "UsersDelete")
    End Function

    Public Shared Function UsersClockGuard(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersClockGuard = Permissions.CheckPermission(intUser_ID, "UsersClockGuard")
    End Function

    Public Shared Function UsersMobileAccess(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersMobileAccess = Permissions.CheckPermission(intUser_ID, "UsersMobileAccess")
    End Function

    Public Shared Function UsersRecover(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersRecover = Permissions.CheckPermission(intUser_ID, "UsersRecover")
    End Function

    Public Shared Function UsersAdd(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersAdd = Permissions.CheckPermission(intUser_ID, "UsersAdd")
    End Function

    Public Shared Function UsersAddManager(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersAddManager = Permissions.CheckPermission(intUser_ID, "UsersAddManager")
    End Function

    Public Shared Function UsersAddManagerFull(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersAddManagerFull = Permissions.CheckPermission(intUser_ID, "UsersAddManagerFull")
    End Function

    Public Shared Function UsersEditPermissions(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersEditPermissions = Permissions.CheckPermission(intUser_ID, "UsersEditPermissions")
    End Function

    Public Shared Function UsersEditPermissionsFull(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersEditPermissionsFull = Permissions.CheckPermission(intUser_ID, "UsersEditPermissionsFull")
    End Function

    Public Shared Function UsersCustomFields(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        UsersCustomFields = Permissions.CheckPermission(intUser_ID, "UsersCustomFields")
    End Function

    Public Shared Function SettingsOffices(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        SettingsOffices = Permissions.CheckPermission(intUser_ID, "SettingsOffices")
    End Function

    Public Shared Function SettingsGeneral(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        SettingsGeneral = Permissions.CheckPermission(intUser_ID, "SettingsGeneral")
    End Function

    Public Shared Function SettingsClockPoints(ByVal intUser_ID As Int64) As Boolean
        Dim Permissions As New Permissions
        SettingsClockPoints = Permissions.CheckPermission(intUser_ID, "SettingsClockPoints")
    End Function

    Protected Function CheckPermission(ByVal intUser_ID As Int64, ByVal strColumn As String) As Boolean
        HttpContext.Current.Trace.Write("Permissions", "Begin CheckPermission")
        'If Not HttpContext.Current.Session("Permission" & strColumn) Is Nothing Then
        'HttpContext.Current.Trace.Write("CheckPermission", "Cached permission '" & strColumn & "': " & HttpContext.Current.Session("Permission" & strColumn))
        'CheckPermission = HttpContext.Current.Session("Permission" & strColumn)
        'Else
        HttpContext.Current.Trace.Write("CheckPermission", "Checking permission '" & strColumn & "' for User_ID " & intUser_ID)
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT [" & strColumn & "] AS Permission FROM [Permissions] WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        If objDR.HasRows Then
            CheckPermission = objDR("Permission")
        Else
            CheckPermission = False
        End If

        'HttpContext.Current.Session("Permission" & strColumn) = CheckPermission
        HttpContext.Current.Trace.Write("CheckPermission", "Permission status: " & CheckPermission)

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
        'End If

        'HttpContext.Current.Trace.Write("Permissions", "End CheckPermission")
    End Function
End Class
