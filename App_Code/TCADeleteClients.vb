Imports Microsoft.VisualBasic

Public Class TCADeleteClients
    Public Shared Sub Delete(ByVal intClient_ID As Int64)
        HttpContext.Current.Trace.Write("TCA.vb Transfer", "Begin Sub")
        HttpContext.Current.Trace.Write("TCA.vb Transfer", "Begin Deleting data for Client_Id = " & intClient_ID)

        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection

        objConnection = New System.Data.SqlClient.SqlConnection
        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "EXEC sprocDeleteClient @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", intClient_ID)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        HttpContext.Current.Trace.Write("TCA.vb Transfer", "All Data has been Deleted")


        HttpContext.Current.Trace.Write("TCA.vb Transfer", "End Sub")
    End Sub

End Class
