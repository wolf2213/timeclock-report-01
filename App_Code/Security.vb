Imports Microsoft.VisualBasic

Public Class Security
    Public Shared Function CheckPermission(ByVal intUser_ID As String, ByVal blnUserIsManager As Boolean, Optional ByVal blnManagerRequired As Boolean = False) As Boolean
        If Not intUser_ID Is Nothing Then
            If Not blnUserIsManager And blnManagerRequired Then
                CheckPermission = False
            Else
                CheckPermission = True
            End If
        Else
            CheckPermission = False
        End If
    End Function
End Class
