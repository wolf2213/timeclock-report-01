<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AbsencesSettings.aspx.vb" Inherits="AbsenceSettings" title="TimeClockWizard - Absence Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="main-content">
                <div style="margin: 0 0 15px 42px;">
									<label>
                        Choose Absence Type:
                                    </label>
                   
                        <asp:DropDownList ID="drpAbsenceType" runat="server">
                            <asp:ListItem Value="1">Vacation</asp:ListItem>
                            <asp:ListItem Value="2">Holiday</asp:ListItem>
                            <asp:ListItem Value="3">Sick Day</asp:ListItem>
                            <asp:ListItem Value="4">Personal</asp:ListItem>
                            <asp:ListItem Value="5">Paid Leave</asp:ListItem>
                            <asp:ListItem Value="6">Other</asp:ListItem>
                        </asp:DropDownList>


                        <asp:ImageButton ID="btnSubmit" ImageUrl="~/images/mileage_add.png" ImageAlign="AbsMiddle" runat="server" OnClick="btnSubmit_Click" CssClass="arrow-btn" />

                    <div class="clear"></div>
				</div>

<div style="margin-left: 25px;">


            <asp:Repeater ID="rptUsersList" runat="server" OnItemDataBound="rptUsersList_ItemDataBound" OnItemCommand="rptUserList_ItemCommand" EnableViewState="True">
                <HeaderTemplate>
									<ul class="row-user">
										<li class="column-head column-title" style="line-height: 34px;">Name</li>
                                    </ul>

									<ul class="total-hours">
										<li class="column-head column-title">Total<asp:Literal ID="litType1" runat="server"></asp:Literal> Hours</li>
                                    </ul>

                    				<ul class="hours-used">
										<li class="column-head column-title">Hours<asp:Literal ID="litType2" runat="server"></asp:Literal> Used</li>
                                    </ul>

									<ul class="hours-avail">
										<li class="column-head column-title">Hours<asp:Literal ID="litType3" runat="server"></asp:Literal> Available</li>
                                    </ul>

									<ul class="accrue-time">
										<li class="column-head column-title">Accrue Time&nbsp;<asp:Image ID="imgHelpAccrueTime" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" /></li>
                                    </ul>

                                    <ul class="editlinks" style="width: 50px;">
                                         <li class="column-head column-title">&nbsp;</li> 
                                    </ul>
         
                </HeaderTemplate>
                <ItemTemplate>
                    <ul class="row-user">
										<li class="text-bold"><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></li>
                    </ul>

                    <ul class="total-hours">
										<li><asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label><asp:TextBox Visible="false" Width="25px" ID="txtTotal" runat="server"></asp:TextBox></li>
                    </ul>
                      
                    <ul class="hours-used">
										<li><asp:Label ID="lblUsed" runat="server" Text="Label"></asp:Label><asp:TextBox Visible="false" Width="25px" ID="txtUsed" runat="server"></asp:TextBox></li>
                    </ul>

                    <ul class="hours-avail">
										<li><asp:Label ID="lblAvailable" runat="server" Text="Label"></asp:Label></li>
                    </ul>

                    <ul class="accrue-time">
										<li><asp:ImageButton ID="btnAccrue" runat="server" /></li>
                    </ul>

                    <ul class="editlinks" style="width: 50px;">
                                         <li><asp:ImageButton ID="btnChange" runat="server" ImageUrl="~/images/edit_small.png" ToolTip="header=[Edit Hours] body=[] offsetx=[-150]" /></li> 
                    </ul>

                  
                </ItemTemplate>

                <FooterTemplate>
                   
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

