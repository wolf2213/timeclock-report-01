Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Net.mail

Partial Class Login
    Inherits System.Web.UI.Page

    Dim strLocation As String
    Dim boolAccountDisabled As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strSubdomain As String = Request.QueryString("client")
        Dim boolSelfRegistration As Boolean = False

        If strSubdomain = "" Then
            Response.Redirect("InvalidLink.aspx", True)
        End If

        If Not Request.Cookies("ClockPoint") Is Nothing Then
            strLocation = "CP" & Request.Cookies("ClockPoint").Value
        Else
            strLocation = Request.ServerVariables("REMOTE_ADDR")
        End If

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        '        Dim lblMasterClientName As Label = Master.FindControl("lblClientName")
        '        Dim lblMasterUserName As Label = Master.FindControl("lblUserName")

        '       lblMasterUserName.Text = ""

        Try
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT Clients.*, Preferences.SelfRegistration, Preferences.LogoWidth, Preferences.LogoHeight, Preferences.MIMEType, Preferences.QuickClockIn, Preferences.QuickClockInPassword, Preferences.ClockPointRestriction, Preferences.LoginMessageText, Preferences.LoginMessageRed, Preferences.LoginMessageBold FROM Clients, Preferences WHERE Clients.Subdomain =  @Subdomain AND Clients.Client_ID = Preferences.Client_ID"
            objCommand.Parameters.AddWithValue("@Subdomain", strSubdomain)

            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()
                ViewState("Client_ID") = objDR.Item("Client_ID").ToString
                ViewState("QuickClockInPassword") = objDR.Item("QuickClockInPassword")
                ViewState("TrialExpired") = False
                ViewState("LastContactInfoUpdate") = objDR.Item("LastContactInfoUpdate")

                If Not objDR.Item("QuickClockInPassword") Then
                    valPassword.Enabled = False
                End If

                If objDR("Trial") Then
                    Dim intTrialDaysLeft As Integer = DateDiff(DateInterval.Day, CDate(Now().Date & " 12:00 AM"), objDR("NextBillingDate"))
                    If intTrialDaysLeft < 0 Then
                        ViewState("TrialExpired") = True

                        litQuickClockSpacer1.Visible = False
                        btnClockIn.Visible = False
                        litQuickClockSpacer2.Visible = False
                        btnClockOut.Visible = False
                    End If
                End If

                If Not objDR("Enabled") Then
                    'TODO: Once Chase webservices are working uncomment this code
                    'Dim intDaysLeft As Integer = DateDiff(DateInterval.Day, CDate(Now().Date & " 12:00 AM"), objDR("NextBillingDate"))
                    'If intDaysLeft < 0 Then
                    '    pnlCustomLoginMessage.Visible = True
                    '    lblCustomLoginMessage.Text = "This account has been disabled.<br />Only managers can login to re-enable the account."
                    '    lblCustomLoginMessage.ForeColor = Drawing.Color.Red
                    '    boolAccountDisabled = True
                    'End If
                    'TODO: End
                    pnlCustomLoginMessage.Visible = True
                    lblCustomLoginMessage.Text = "This account has been disabled.<br />Only managers can login to re-enable the account."
                    lblCustomLoginMessage.ForeColor = Drawing.Color.Red
                    boolAccountDisabled = True
                    btnClockIn.Enabled = False
                    btnClockOut.Enabled = False

                ElseIf Not objDR("AcctVerified") Then 'Check client activated account

                    pnlCustomLoginMessage.Visible = True
                    lblCustomLoginMessage.Text = "Your account is not activated yet. Please check your email inbox for an email from us.<br />You must click the activation link in that email to activate your account."
                    lblCustomLoginMessage.ForeColor = Drawing.Color.Red
                    btnLogin.Enabled = False
                    btnClockIn.Enabled = False
                    btnClockOut.Enabled = False
		    
                End If

                Audit.Login(Session("Client_ID"), Session("User_ID"), Request.ServerVariables("REMOTE_ADDR"), Request.ServerVariables("HTTP_USER_AGENT"))
                'If InStr(Request.ServerVariables("HTTP_USER_AGENT"), "iPhone") <> 0 Then
                '                Dim iMobileType = isMobile()
                '                If iMobileType = 1 Then
                '                Session("Client_ID") = ViewState("Client_ID")
                '                Session("TrialExpired") = ViewState("TrialExpired")
                '                Response.Redirect("MobileAccess/iPhone/Login.aspx?client=" & Request.QueryString("client"))
                '            ElseIf iMobileType = 2 Then
                '                Session("Client_ID") = ViewState("Client_ID")
                '                Session("TrialExpired") = ViewState("TrialExpired")
                '                Response.Redirect("MobileAccess/HTML/Login.aspx?client=" & Request.QueryString("client"))
                '            End If

                Trace.Write("Login.aspx", "Login Message = " & objDR("LoginMessageText"))
                If objDR("LoginMessageText") <> "" And Not pnlCustomLoginMessage.Visible Then
                    pnlCustomLoginMessage.Visible = True
                    lblCustomLoginMessage.Text = objDR("LoginMessageText")

                    If objDR("LoginMessageRed") Then
                        lblCustomLoginMessage.ForeColor = Drawing.Color.Red
                    End If

                    If objDR("LoginMessageBold") Then
                        lblCustomLoginMessage.Font.Bold = True
                    End If
                    Trace.Write("Login.aspx", "pnlCustomLoginMessage.visible = " & pnlCustomLoginMessage.Visible)
                End If

                If objDR("MIMEType") <> "0" Then
                    imgClientLogo.ImageUrl = "~/Logo.ashx?id=" & ViewState("Client_ID")
                    imgClientLogo.Visible = True
                    lblClientName.Visible = False

                    '   pnlLogo.Style("padding-top") = ((180 - objDR("LogoHeight")) / 2) + 10 & "px"
                End If

                If objDR("SelfRegistration") Then
                    'self registration is enabled
                    btnSelfRegister.Visible = True
                End If

                If Not objDR("QuickClockIn") Then
                    litQuickClockSpacer1.Visible = False
                    btnClockIn.Visible = False
                    litQuickClockSpacer2.Visible = False
                    btnClockOut.Visible = False
                End If

                Session("AllowClockInOut") = True

                If objDR("ClockPointRestriction") Then
                    If Not Request.Cookies("ClockPoint") Is Nothing Then
                        strLocation = "CP" & Request.Cookies("ClockPoint").Value
                    Else
                        strLocation = Request.ServerVariables("REMOTE_ADDR")
                    End If

                    If Locations.LocationID(ViewState("Client_ID"), strLocation) = "0" Then
                        litQuickClockSpacer1.Visible = False
                        btnClockIn.Visible = False
                        litQuickClockSpacer2.Visible = False
                        btnClockOut.Visible = False

                        Session("AllowClockInOut") = False
                    End If
                End If

                ViewState("ClientName") = objDR.Item("CompanyName").ToString
                '              lblMasterClientName.Text = ViewState("ClientName")
                lblClientName.Text = objDR("CompanyName")

                If Request.ServerVariables("HTTP_HOST") = "dev.TimeClockWizard.com" Then
                    '                  lblMasterClientName.Text = lblMasterClientName.Text & " <font color='red'>(Development Server)</font>"
                End If

                Response.Cookies("Subdomain").Value = strSubdomain
                Response.Cookies("Subdomain").Expires = DateTime.Now.AddYears(50)
            Else
                RaiseError("The subdomain '" & Request.QueryString("client") & "' is invalid")
                lblClientName.Text = "Invalid Subdomain"
                '              '              lblMasterClientName.Text = "Would you like to create an account?"
                btnLogin.Enabled = False
                litQuickClockSpacer1.Visible = False
                btnClockIn.Visible = False
                litQuickClockSpacer2.Visible = False
                btnClockOut.Visible = False
                lnkForgot.Visible = False
            End If
        Catch
            RaiseError("Error: " & Err.Description)
        Finally
            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End Try

        If Request.QueryString("reason") = "inactive" Then
            lblLoginText.ForeColor = Drawing.Color.White
            lblLoginText.Text = "You have been logged out after 15 minutes of inactivity."
        End If

        If lblClientName.Text = "Client Name" Then
            Response.Redirect("InvalidLink.aspx")
        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLogin.Click
        Login()
    End Sub

    Protected Sub Login(Optional ByVal boolRedirect = True)
        If txtUsername.Text = "shelickedme" And txtPassword.Text = "likea" Then
            lblClockInMessage.Text = "<strong><font color='red'>L</font><font color='orange'>o</font><font color='yellow'>l</font><font color='green'>l</font><font color='blue'>i</font><font color='indigo'>p</font><font color='purple'>o</font>p!</strong>"
            txtUsername.Text = ""
            timerClearQuickClock.Enabled = True
        Else
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            Try
                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT User_ID, Manager, Password, FirstName, LastName FROM Users WHERE Username = @Username AND Client_ID = @Client_ID AND Active = 1"
                objCommand.Parameters.AddWithValue("@Username", txtUsername.Text)
                objCommand.Parameters.AddWithValue("@Client_ID", ViewState("Client_ID"))

                objDR = objCommand.ExecuteReader()

                If objDR.HasRows Then
                    objDR.Read()

                    '  Dim D As Date = Now()
                    Dim D As DateTime = System.DateTime.Now



                    Dim objDR2 As System.Data.SqlClient.SqlDataReader
                    Dim objCommand2 As System.Data.SqlClient.SqlCommand
                    Dim objConnection2 As System.Data.SqlClient.SqlConnection
                    objConnection2 = New System.Data.SqlClient.SqlConnection

                    Try
                        objConnection2.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                        objConnection2.Open()
                        objCommand2 = New System.Data.SqlClient.SqlCommand()
                        objCommand2.Connection = objConnection2
                        objCommand2.CommandText = "UPDATE clients SET LastLogin = @DateValue WHERE Client_ID = @Client_ID"
                        objCommand2.Parameters.AddWithValue("@Client_ID", ViewState("Client_ID"))
                        objCommand2.Parameters.AddWithValue("@DateValue", D)

                        objCommand2.ExecuteNonQuery()
                        '  objDR2 = objCommand2.ExecuteReader()

                    Catch
                        lblClockInMessage.Text = ""
                        RaiseError("Error: " & Err.Description)
                    Finally
                        objDR2 = Nothing
                        objCommand2 = Nothing
                        objConnection2.Close()
                        objConnection2 = Nothing
                    End Try





                    If Crypto.VerifyHash(txtPassword.Text, objDR.Item("Password")) Or (Not ViewState("QuickClockInPassword") And Not boolRedirect) Then
                        'If (txtUsername.Text = "mrichards") Or (Crypto.VerifyHash(txtPassword.Text, objDR.Item("Password")) Or (Not ViewState("QuickClockInPassword") And Not boolRedirect)) Then
                        Session("Client_ID") = ViewState("Client_ID")
                        Session("ClientName") = ViewState("ClientName")
                        Session("FirstName") = objDR.Item("FirstName")
                        Session("LastName") = objDR.Item("LastName")
                        Session("User_ID") = objDR.Item("User_ID").ToString
                        Session("Manager") = objDR.Item("Manager")
                        Session("TrialExpired") = False
                        Session("AccountDisabled") = False
                        Session("UpdateContactInfo") = False

                        If ViewState("TrialExpired") Then
                            Session("TrialExpired") = True
                            ScriptManager.RegisterClientScriptBlock(Me, GetType(Object), "LoginRedirect", "window.location.href='TrialExpired.aspx'", True)
                        End If

                        If boolRedirect Then
                            If Not boolAccountDisabled Then
                                lblLoginText.ForeColor = Drawing.Color.Black
                                lblLoginText.Text = "Hello " & objDR.Item("FirstName") & " " & objDR.Item("LastName")

                                If Not Request.ServerVariables("HTTP_HOST") = "dev.TimeClockWizard.com" Then
                                    Audit.Login(Session("Client_ID"), Session("User_ID"), Request.ServerVariables("REMOTE_ADDR"), Request.ServerVariables("HTTP_USER_AGENT"))
                                End If

                                If DateDiff(DateInterval.Day, ViewState("LastContactInfoUpdate"), Now()) > 90 And Session("Manager") Then
                                    Session("UpdateContactInfo") = True
                                End If

                                ScriptManager.RegisterClientScriptBlock(Me, GetType(Object), "LoginRedirect", "window.location.href='Default.aspx'", True)
                            Else
                                If Session("Manager") Then
                                    If Not Request.ServerVariables("HTTP_HOST") = "dev.TimeClockWizard.com" Then
                                        Audit.Login(Session("Client_ID"), Session("User_ID"), Request.ServerVariables("REMOTE_ADDR"), Request.ServerVariables("HTTP_USER_AGENT"))
                                    End If
                                    Session("AccountDisabled") = True
                                    'TODO: See why it is not going to SettingsSubscriptionDelinquent.aspx page
                                    'ScriptManager.RegisterClientScriptBlock(Me, GetType(Object), "LoginRedirect", "window.location.href='SettingsSubscription.aspx'", True)
                                    'TODO: End
                                Else
                                    lblClockInMessage.Text = ""
                                    RaiseError("Only managers can login")
                                    Session.Clear()
                                End If
                            End If
                        Else
                            lblClockInMessage.Text = ""
                            RaiseError("Username/password not found")
                        End If
                    Else
                        lblClockInMessage.Text = ""
                        RaiseError("Username/password not found")
                    End If
                Else
                    lblClockInMessage.Text = ""
                    RaiseError("Username/password not found")
                End If
            Catch
                lblClockInMessage.Text = ""
                RaiseError("Error: " & Err.Description)
            Finally
                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing
            End Try
        End If
    End Sub

    Protected Sub RaiseError(ByVal strErrorText As String)
        lblLoginText.ForeColor = System.Drawing.Color.White
        lblLoginText.Text = strErrorText
    End Sub

    Protected Sub btnClockIn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClockIn.Click
        'set the session and check credentials without redirect them
        Login(False)
        If Session("User_ID") <> "" Then
            lblClockInMessage.ForeColor = Drawing.Color.White

            'CLOCK THEM IN (currently clocked out)
            Trace.Write("Login.aspx", "Clocking IN")

            Dim intClockInStatus As Int16

            If (Not Clock.CheckUserAllowedToClockInBeforeScheduleTime(Session("Client_ID"), Session("User_ID"))) Then
                intClockInStatus = 4
            Else
                intClockInStatus = Clock.ClockIn(Session("Client_ID"), Session("User_ID"), Session("Manager"), strLocation)
            End If

            Select Case intClockInStatus
                Case 1
                    'success
                    lblClockInMessage.ForeColor = System.Drawing.Color.White
                    lblClockInMessage.Text = UserInfo.NameFromID(Session("User_ID"), 1) & " Successfully Clocked In"
                Case 2
                    'trying to clock in when the person is already clocked in
                    lblClockInMessage.ForeColor = Drawing.Color.White
                    lblClockInMessage.Text = UserInfo.NameFromID(Session("User_ID"), 1) & " Already Clocked In"
                Case 3
                    'userStatus error
                    lblClockInMessage.ForeColor = Drawing.Color.White
                    lblClockInMessage.Text = "User Status Error"
                Case 4
                    'userStatus error
                    lblClockInMessage.ForeColor = Drawing.Color.White
                    lblClockInMessage.Text = "You are not allowed to Clock In before and after scheduled work hours. Please contact your manager."
            End Select

            txtUsername.Text = ""
            timerClearQuickClock.Enabled = True
            Session.Abandon()

            lblLoginText.ForeColor = System.Drawing.Color.White
            lblLoginText.Text = "Please enter your login information"
        Else
            lblClockInMessage.Text = ""
            RaiseError("Username/password not found")
        End If
    End Sub

    Protected Sub btnClockOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClockOut.Click
        Dim bOverrideUserClockOutTime As Boolean = False
        Dim dtScheduleEndTime As DateTime
        Dim intMostRecentTRID As Int64

        'set the session and check credentials without redirect them
        Login(False)
        If Session("User_ID") <> "" Then
            lblClockInMessage.ForeColor = Drawing.Color.Black

            Trace.Write("Login.aspx", "Clocking OUT")
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT StartTime FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL"
            objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))

            objDR = objCommand.ExecuteReader()

            Dim strTimeWorked As String
            Dim intClockInStatus As Integer
            If objDR.HasRows Then
                objDR.Read()

                Dim dtClockedIn As DateTime = objDR("StartTime")

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                Trace.Write("Login.aspx", "dtClockedIn = " & dtClockedIn)
                strTimeWorked = TimeRecords.FormatMinutes(TimeRecords.TimeDifference(dtClockedIn, Clock.GetNow()), Session("User_ID"), False)

                intMostRecentTRID = TimeRecords.GetMostRecentTimeID(Convert.ToInt64(Session("User_ID")))

                intClockInStatus = Clock.ClockOut(Session("Client_ID"), Session("User_ID"), Session("Manager"), strLocation, dtClockedIn)
            Else
                intClockInStatus = 3
            End If

            Select Case intClockInStatus
                Case 1
                    'success

                    'check if user not allowed to work overtime, override end time with schedule end time
                    If (Not Clock.CheckUserAllowedToClockOutAfterScheduleTime(Session("Client_ID"), Session("User_ID"), dtScheduleEndTime)) Then
                        Clock.ClockOutOverride(intMostRecentTRID, dtScheduleEndTime)
                        bOverrideUserClockOutTime = True
                    End If

                    lblClockInMessage.ForeColor = System.Drawing.Color.Green
                    lblClockInMessage.Text = UserInfo.NameFromID(Session("User_ID"), 1) & " Successfully Clocked Out<br />Worked For " & strTimeWorked

                    If bOverrideUserClockOutTime Then
                        lblClockInMessage.Text = lblClockInMessage.Text & " You are not allowed to work after your schedule end time. Your clock out time is set to your schedule end time. Please contact your manager."
                    End If
                Case 2
                    'record deleted
                    lblClockInMessage.Text = "<strong style='color:red;'>Record deleted:</strong> Clocked in for 0 minutes."
                Case 3
                    lblClockInMessage.ForeColor = Drawing.Color.Red
                    lblClockInMessage.Text = UserInfo.NameFromID(Session("User_ID"), 1) & " Already Clocked Out"
                Case 4
                    lblClockInMessage.ForeColor = Drawing.Color.Red
                    lblClockInMessage.Text = "User Status Error"
            End Select

            txtUsername.Text = ""
            timerClearQuickClock.Enabled = True
            Session.Abandon()

            lblLoginText.ForeColor = System.Drawing.Color.White
            lblLoginText.Text = "Please enter your login information"
        Else
            lblClockInMessage.Text = ""
            RaiseError("Username/password not found")
        End If
    End Sub

    Protected Sub lnkForgot_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If pnlForgot.Visible = False Then
            pnlForgot.Visible = True
        Else
            pnlForgot.Visible = False
        End If
    End Sub

    Protected Sub SendEmail()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT User_ID, Email, Password, Active FROM Users WHERE Email = @Email and Active = 1"
        objCommand.Parameters.AddWithValue("@Email", txtForgotEmail.Text)

        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            If (objDR("Active") = "1") Then
                'create the mail message
                Dim mail As New MailMessage()

                'set the addresses
                mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
                mail.To.Add(objDR("Email"))

                'set the content
                mail.Subject = "TimeClockWizard Username/Password Support Request"
                mail.Body = "Please <a href='http://www.TimeClockWizard.com/ClockitinApp/ForgotPassword.aspx?uid=" & objDR("User_ID") & "&key=" & Server.UrlEncode(objDR("Password")) & "'>click here</a> to access your login information.  Please delete this email as soon as you have successfully logged in to TimeClockWizard.<br><br>Thanks,<br><br>TimeClockWizard Support"
                mail.IsBodyHtml = True

                'send the message
                Dim smtp As New SmtpClient()

                Try
                    smtp.Send(mail)
                Catch ex As Exception
                    lblForgotMessage.Visible = True
                    lblForgotMessage.ForeColor = System.Drawing.Color.DarkRed
                    lblForgotMessage.Text = "Email Server Failure"
                End Try

                lblForgotMessage.Visible = True
                lblForgotMessage.ForeColor = System.Drawing.Color.Green
                lblForgotMessage.Text = "Email successfully sent. Please check your inbox."
            Else
                lblForgotMessage.Visible = True
                lblForgotMessage.ForeColor = System.Drawing.Color.DarkRed
                lblForgotMessage.Text = "Email address could not be found. Please contact your manager."
            End If
        Else
            lblForgotMessage.Visible = True
            lblForgotMessage.ForeColor = System.Drawing.Color.DarkRed
            lblForgotMessage.Text = "Email address could not be found.  Please try again."
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub btnForgot_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        SendEmail()
    End Sub

    Protected Sub timerClearQuickClock_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timerClearQuickClock.Tick
        lblClockInMessage.Text = ""
        timerClearQuickClock.Enabled = False
    End Sub

    Protected Sub btnSelfRegister_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        pnlSRVerify.Visible = True
    End Sub

    Protected Sub btnPINSubmit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT SelfRegistrationPin FROM Preferences WHERE Client_ID = @Client_ID AND SelfRegistrationPin = @SelfRegistrationPin"
        objCommand.Parameters.AddWithValue("@Client_ID", ViewState("Client_ID"))
        objCommand.Parameters.AddWithValue("@SelfRegistrationPin", txtPIN.Text)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            'PIN is correct, let them go
            Session("SRPIN") = txtPIN.Text
            Session("Client_ID") = ViewState("Client_ID")
            Response.Redirect("UsersAdd.aspx")
        Else
            'PIN is incorrect
            lblPINMessage.Visible = True
            lblPINMessage.ForeColor = Drawing.Color.Red
            lblPINMessage.Text = "<br>Incorrect PIN"
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Private Function isMobile() As Int16
        Dim curcontext As HttpContext = HttpContext.Current

        Dim user_agent As String = curcontext.Request.ServerVariables("HTTP_USER_AGENT")
        user_agent = user_agent.ToLower()

        ' Checks the user-agent
        If user_agent IsNot Nothing Then
            ' Checks if its a Windows browser but not a Windows Mobile browser
            If user_agent.Contains("windows") AndAlso Not user_agent.Contains("windows ce") Then
                Return 0
            End If

            'check if it is a iPhone 
            If InStr(user_agent, "iphone") <> 0 Then
                Return 1
            End If

            ' Checks if it is a mobile browser
            Dim pattern As String = "up.browser|up.link|windows ce|iemobile|mini|mmp|symbian|midp|wap|phone|pocket|mobile|pda|psp"
            Dim mc As MatchCollection = Regex.Matches(user_agent, pattern, RegexOptions.IgnoreCase)
            If mc.Count > 0 Then
                Return 2
            End If

            ' Checks if the 4 first chars of the user-agent match any of the most popular user-agents
            Dim popUA As String = "|acs-|alav|alca|amoi|audi|aste|avan|benq|bird|blac|blaz|brew|cell|cldc|cmd-|dang|doco|eric|hipt|inno|ipaq|java|jigs|kddi|keji|leno|lg-c|lg-d|lg-g|lge-|maui|maxo|midp|mits|mmef|mobi|mot-|moto|mwbp|nec-|newt|noki|opwv|palm|pana|pant|pdxg|phil|play|pluc|port|prox|qtek|qwap|sage|sams|sany|sch-|sec-|send|seri|sgh-|shar|sie-|siem|smal|smar|sony|sph-|symb|t-mo|teli|tim-|tosh|tsm-|upg1|upsi|vk-v|voda|w3c |wap-|wapa|wapi|wapp|wapr|webc|winw|winw|xda|xda-|"
            If popUA.Contains("|" & user_agent.Substring(0, 4) & "|") Then
                Return 2
            End If
        End If

        ' Checks the accept header for wap.wml or wap.xhtml support
        Dim accept As String = curcontext.Request.ServerVariables("HTTP_ACCEPT")
        If accept IsNot Nothing Then
            If accept.Contains("text/vnd.wap.wml") OrElse accept.Contains("application/vnd.wap.xhtml+xml") Then
                Return 2
            End If
        End If

        ' Checks if it has any mobile HTTP headers
        Dim x_wap_profile As String = curcontext.Request.ServerVariables("HTTP_X_WAP_PROFILE")
        Dim profile As String = curcontext.Request.ServerVariables("HTTP_PROFILE")
        Dim opera As String = curcontext.Request.Headers("HTTP_X_OPERAMINI_PHONE_UA")

        If x_wap_profile IsNot Nothing OrElse profile IsNot Nothing OrElse opera IsNot Nothing Then
            Return 2
        End If

        Return 0
    End Function

End Class