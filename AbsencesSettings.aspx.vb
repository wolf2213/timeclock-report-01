
Partial Class AbsenceSettings
    Inherits System.Web.UI.Page

    Dim strColBase As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        If Not Permissions.AbsencesSettings(Session("User_ID")) Then
            Server.Transfer("Absences.aspx", False)
        End If

        Dim pnlMasterTabSchedule As Panel = Master.FindControl("pnlTabSchedule")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2AbsenceSettings As Panel = Master.FindControl("pnlTab2AbsenceSettings")

        pnlMasterTabSchedule.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Schedule
        pnlMasterTab2AbsenceSettings.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("type")) Then
                drpAbsenceType.SelectedValue = CInt(Request.QueryString("type"))
            End If
            CreateUsersList()
        End If
    End Sub

    Protected Sub CreateUsersList()
        strColBase = Replace(AbsencesClass.AbsenceNameFromType(drpAbsenceType.SelectedVAlue), " ", "")

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
            objCommand.CommandText = "SELECT Users.User_ID, Users.LastName + ', ' + Users.FirstName AS FullName, " & strColBase & "Total AS abTotal, " & strColBase & "Used AS abUsed, " & strColBase & "Enabled AS Accrue FROM Users, AbsenceUserSettings WHERE Users.User_ID = AbsenceUserSettings.User_ID AND AbsenceUserSettings.Client_ID = @Client_ID AND Users.Active = 1 AND (Users.ManagerUserId = @ManagerUserId OR Users.User_Id = @User_Id) ORDER BY FullName ASC"
            objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
        Else
            objCommand.CommandText = "SELECT Users.User_ID, Users.LastName + ', ' + Users.FirstName AS FullName, " & strColBase & "Total AS abTotal, " & strColBase & "Used AS abUsed, " & strColBase & "Enabled AS Accrue FROM Users, AbsenceUserSettings WHERE Users.User_ID = AbsenceUserSettings.User_ID AND AbsenceUserSettings.Client_ID = @Client_ID AND Users.Active = 1 ORDER BY FullName ASC"
        End If
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        rptUsersList.DataSource = objDR
        rptUsersList.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptUsersList_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Header Then
            Dim imgHelpAccrueTime As Image = e.Item.FindControl("imgHelpAccrueTime")
            imgHelpAccrueTime.ToolTip = "header=[Accrue Time] body=[Enable this feature to have TimeClockWizard automatically add available time to the user's allotted " & drpAbsenceType.SelectedItem.ToString & " time.  Once enabled, click 'Settings' to configure.]"
        End If

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblName As Label = e.Item.FindControl("lblName")
            Dim lblTotal As Label = e.Item.FindControl("lblTotal")
            Dim txtTotal As TextBox = e.Item.FindControl("txtTotal")
            Dim lblUsed As Label = e.Item.FindControl("lblUsed")
            Dim txtUsed As TextBox = e.Item.FindControl("txtUsed")
            Dim lblAvailable As Label = e.Item.FindControl("lblAvailable")
            Dim btnAccrue As ImageButton = e.Item.FindControl("btnAccrue")
            Dim btnChange As ImageButton = e.Item.FindControl("btnChange")


            lblName.Text = e.Item.DataItem("FullName")
            lblTotal.Text = e.Item.DataItem("abTotal")
            txtTotal.Text = lblTotal.Text
            lblUsed.Text = e.Item.DataItem("abUsed")
            txtUsed.Text = lblUsed.Text
            lblAvailable.Text = System.Math.Round((e.Item.DataItem("abTotal")) - (e.Item.DataItem("abUsed")), 3)

            If CInt(e.Item.DataItem("Accrue")) <> 0 Then
                btnAccrue.ImageUrl = "~/images/absences_accruing.png"
                btnAccrue.AlternateText = "Click to configure Accrual Settings"
                btnAccrue.ToolTip = "header=[Accrual Settings] body=[Click to configure]"
                btnAccrue.CommandName = "ConfigureAccrue"
            Else
                btnAccrue.ImageUrl = "~/images/absences_stopped.png"
                btnAccrue.AlternateText = "Click to start accruing Absence Time"
                btnAccrue.ToolTip = "header=[Absence Time] body=[Click to start accruing]"
                btnAccrue.CommandName = "EnableAccrue"
            End If

            btnAccrue.CommandArgument = e.Item.DataItem("User_ID")

            btnChange.CommandName = "Edit"
            btnChange.CommandArgument = e.Item.DataItem("User_ID")

        End If
    End Sub

    Protected Sub rptUserList_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim strSQLQuery As String
        Dim boolRunSQL As Boolean = True
        Dim intUser_ID As Int64 = e.CommandArgument

        strColBase = Replace(AbsencesClass.AbsenceNameFromType(drpAbsenceType.SelectedValue), " ", "")

        Select Case e.CommandName
            Case "ConfigureAccrue"
                boolRunSQL = False
                Response.Redirect("AccrualSettings.aspx?user_id=" & intUser_ID & "&type=" & drpAbsenceType.SelectedValue)
            Case "EnableAccrue"
                strSQLQuery = "UPDATE [AbsenceUserSettings] SET [" & strColBase & "Enabled] = 1 WHERE [User_ID] = @User_ID"
            Case "Edit"
                Trace.Write("AbsenceSettings.aspx", "Start edit process...")
                boolRunSQL = False

                Dim lblTotal As Label = e.Item.FindControl("lblTotal")
                Dim txtTotal As TextBox = e.Item.FindControl("txtTotal")
                Dim lblUsed As Label = e.Item.FindControl("lblUsed")
                Dim txtUsed As TextBox = e.Item.FindControl("txtUsed")
                Dim btnChange As ImageButton = e.Item.FindControl("btnChange")

                lblTotal.Visible = False
                lblUsed.Visible = False

                txtTotal.Visible = True
                txtUsed.Visible = True

                btnChange.ImageUrl = "~/images/save_small.png"
                btnChange.CommandName = "Save"
            Case "Save"
                Trace.Write("AbsenceSettings.aspx", "Start edit process...")
                boolRunSQL = False

                Dim lblTotal As Label = e.Item.FindControl("lblTotal")
                Dim txtTotal As TextBox = e.Item.FindControl("txtTotal")
                Dim lblUsed As Label = e.Item.FindControl("lblUsed")
                Dim txtUsed As TextBox = e.Item.FindControl("txtUsed")
                Dim btnChange As ImageButton = e.Item.FindControl("btnChange")

                If IsNumeric(txtTotal.Text) And IsNumeric(txtUsed.Text) Then
                    If CDbl(txtTotal.Text) >= 0 And CDbl(txtUsed.Text) >= 0 Then
                        Dim strColBase As String = Replace(AbsencesClass.AbsenceNameFromType(drpAbsenceType.SelectedValue), " ", "")

                        Dim objCommand As System.Data.SqlClient.SqlCommand
                        Dim objConnection As System.Data.SqlClient.SqlConnection
                        objConnection = New System.Data.SqlClient.SqlConnection

                        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                        objConnection.Open()
                        objCommand = New System.Data.SqlClient.SqlCommand()
                        objCommand.Connection = objConnection
                        objCommand.CommandText = "UPDATE AbsenceUserSettings SET " & strColBase & "Total = @HoursTotal, " & strColBase & "Used = @HoursUsed WHERE User_ID = @User_ID"
                        objCommand.Parameters.AddWithValue("@HoursTotal", System.Math.Round(CDbl(txtTotal.Text), 3))
                        objCommand.Parameters.AddWithValue("@HoursUsed", System.Math.Round(CDbl(txtUsed.Text), 3))
                        objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

                        objCommand.ExecuteNonQuery()

                        objCommand = Nothing
                        objConnection.Close()
                        objConnection = Nothing

                        btnChange.ImageUrl = "~/images/edit_small.png"
                        btnChange.CommandName = "Edit"

                        CreateUsersList()
                    End If
                End If
        End Select

        If boolRunSQL Then
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = strSQLQuery
            objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            CreateUsersList()
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CreateUsersList()
    End Sub
End Class
