Imports System.Net.Mail
Imports System.Data

Partial Class AddUser
    Inherits System.Web.UI.Page
    Dim arrCustomStatus(5) As Integer

    Dim objUserInfo As New UserInfo

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim intUserId As Int64 = 0
        BindonLoad()
        If Not IsPostBack Then
            rbtnlistOverTime.Attributes.Add("onclick", "return GetSelectedItem(this,'" + divDailyhours.ClientID + "','" + divWeeklyhours.ClientID + "')")
            Trace.Warn("Users", "execute LoadForm()")
            If CDbl(Session("User_ID")) = CDbl(intUserId) Then
                chkManager.Enabled = False
            End If
            Dim intCounter As Integer = Year(Clock.GetNow())
            Dim ltYear As ListItem
            While intCounter > 1899
                ltYear = New ListItem(intCounter, intCounter)
                drpYear.Items.Add(ltYear)
                intCounter = intCounter - 1
            End While

            If Request.QueryString("UserID") IsNot Nothing Then
                intUserId = Convert.ToInt64(Request.QueryString("UserID"))
            Else
                If Session("User_ID") > 0 Then
                    intUserId = Session("User_ID")
                Else
                    intUserId = 0
                End If
            End If
            Dim ds As New DataSet
            ds = objUserInfo.sprocGetUsersDetail(Convert.ToInt64(Session("Client_ID")), Convert.ToInt64(intUserId))
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0)("Custom1Enabled") = True Then
                        If ds.Tables(0).Rows(0)("Custom1Mandatory") = True Then
                            'set mandatory
                            pnlMandCust1.Visible = True
                            lblMandCust1.Text = ds.Tables(0).Rows(0)("Custom1Name")
                            valMandCust1.Enabled = True
                            valMandCust1.ErrorMessage = ds.Tables(0).Rows(0)("Custom1Name") & " is required<br>"
                            arrCustomStatus(1) = 2
                        Else
                            'set optional
                            pnlCust1.Visible = True
                            lblCust1.Text = ds.Tables(0).Rows(0)("Custom1Name")
                            arrCustomStatus(1) = 1
                        End If
                    Else
                        arrCustomStatus(1) = 0
                    End If
                    If ds.Tables(0).Rows(0)("Custom2Enabled") = True Then
                        If ds.Tables(0).Rows(0)("Custom2Mandatory") = True Then
                            'set mandatory
                            pnlMandCust2.Visible = True
                            lblMandCust2.Text = ds.Tables(0).Rows(0)("Custom2Name")
                            valMandCust2.Enabled = True
                            valMandCust2.ErrorMessage = ds.Tables(0).Rows(0)("Custom2Name") & " is required<br>"
                            arrCustomStatus(2) = 2
                        Else
                            'set optional
                            pnlCust2.Visible = True
                            lblCust2.Text = ds.Tables(0).Rows(0)("Custom2Name")
                            arrCustomStatus(2) = 1
                        End If
                    Else
                        arrCustomStatus(2) = 0
                    End If
                    If ds.Tables(0).Rows(0)("Custom3Enabled") = True Then
                        If ds.Tables(0).Rows(0)("Custom3Mandatory") = True Then
                            'set mandatory
                            pnlMandCust3.Visible = True
                            lblMandCust3.Text = ds.Tables(0).Rows(0)("Custom3Name")
                            valMandCust3.Enabled = True
                            valMandCust3.ErrorMessage = ds.Tables(0).Rows(0)("Custom3Name") & " is required<br>"
                            arrCustomStatus(3) = 2
                        Else
                            'set optional
                            pnlCust3.Visible = True
                            lblCust3.Text = ds.Tables(0).Rows(0)("Custom3Name")
                            arrCustomStatus(3) = 1
                        End If
                    Else
                        arrCustomStatus(3) = 0
                    End If
                    If ds.Tables(0).Rows(0)("Custom4Enabled") = True Then
                        If ds.Tables(0).Rows(0)("Custom4Mandatory") = True Then
                            'set mandatory
                            pnlMandCust4.Visible = True
                            lblMandCust4.Text = ds.Tables(0).Rows(0)("Custom4Name")
                            valMandCust4.Enabled = True
                            valMandCust4.ErrorMessage = ds.Tables(0).Rows(0)("Custom4Name") & " is required<br>"
                            arrCustomStatus(4) = 2
                        Else
                            'set optional
                            pnlCust4.Visible = True
                            lblCust4.Text = ds.Tables(0).Rows(0)("Custom4Name")
                            arrCustomStatus(4) = 1
                        End If
                    Else
                        arrCustomStatus(4) = 0
                    End If
                    If ds.Tables(0).Rows(0)("Custom5Enabled") = True Then
                        If ds.Tables(0).Rows(0)("Custom5Mandatory") = True Then
                            'set mandatory
                            pnlMandCust5.Visible = True
                            lblMandCust5.Text = ds.Tables(0).Rows(0)("Custom5Name")
                            valMandCust5.Enabled = True
                            valMandCust5.ErrorMessage = ds.Tables(0).Rows(0)("Custom5Name") & " is required<br>"
                            arrCustomStatus(5) = 2
                        Else
                            'set optional
                            pnlCust5.Visible = True
                            lblCust5.Text = ds.Tables(0).Rows(0)("Custom5Name")
                            arrCustomStatus(5) = 1
                        End If
                    Else
                        arrCustomStatus(5) = 0
                    End If
                End If

                If ds.Tables(1).Rows.Count > 0 Then
                    drpOffice.DataSource = ds.Tables(1)
                    drpOffice.DataTextField = "Name"
                    drpOffice.DataValueField = "Office_ID"
                    drpOffice.DataBind()
                End If
                Dim ltSelectNone As ListItem = New ListItem("None", 0)
                drpOffice.Items.Insert(0, ltSelectNone)
                If ds.Tables(2).Rows.Count > 0 Then
                    drpManagersList.DataSource = ds.Tables(2)
                    drpManagersList.DataTextField = "ManagerName"
                    drpManagersList.DataValueField = "Manager_User_ID"
                    drpManagersList.DataBind()
                End If
                Dim ltSelectManagerNone As ListItem = New ListItem("None", 0)
                drpManagersList.Items.Insert(0, ltSelectManagerNone)
                If ds.Tables(3).Rows.Count > 0 Then
                    If Request.QueryString("UserID") IsNot Nothing Then
                        ViewState("Manager") = ds.Tables(3).Rows(0)("Manager")
                        txtUsername.Text = ds.Tables(3).Rows(0)("Username").ToString()
                        txtFirstName.Text = ds.Tables(3).Rows(0)("FirstName").ToString()
                        txtLastName.Text = ds.Tables(3).Rows(0)("LastName").ToString()
                        txtEmail.Text = ds.Tables(3).Rows(0)("Email").ToString()
                        ddlOT.SelectedValue = ds.Tables(3).Rows(0)("Overtime").ToString()
                        chkManager.Checked = ds.Tables(3).Rows(0)("Manager").ToString()
                        chkClockGuard.Checked = ds.Tables(3).Rows(0)("ClockGuard").ToString()
                        chkMobileAccess.Checked = ds.Tables(3).Rows(0)("MobileAccess").ToString()
                        txtAddress1.Text = ds.Tables(3).Rows(0)("HomeAddress1").ToString()
                        txtAddress2.Text = ds.Tables(3).Rows(0)("HomeAddress2").ToString()
                        txtCity.Text = ds.Tables(3).Rows(0)("City").ToString()
                        txtState.Text = ds.Tables(3).Rows(0)("State").ToString()
                        txtZip.Text = ds.Tables(3).Rows(0)("ZIPCode").ToString()
                        txtHomePhone.Text = ds.Tables(3).Rows(0)("HomePhone").ToString()
                        txtCellPhone.Text = ds.Tables(3).Rows(0)("CellPhone").ToString()
                        txtWorkPhone.Text = ds.Tables(3).Rows(0)("WorkPhone").ToString()
                        txtWorkExt.Text = ds.Tables(3).Rows(0)("WorkExt").ToString()
                        chkEmployeeWorksOvernight.Checked = ds.Tables(3).Rows(0)("OvernightEmployee")

                        If IsNumeric(ds.Tables(3).Rows(0)("Office_ID")) Then
                            drpOffice.SelectedValue = ds.Tables(3).Rows(0)("Office_ID")
                        End If

                        If IsNumeric(ds.Tables(3).Rows(0)("ManagerUserId")) Then
                            drpManagersList.SelectedValue = ds.Tables(3).Rows(0)("ManagerUserId")
                        End If

                        ddlCurrencyType.SelectedValue = ds.Tables(3).Rows(0)("CurrencyType").ToString()
                        txtWage.Text = ds.Tables(3).Rows(0)("Wage").ToString()
                        txtAllowedDeviation.Text = ds.Tables(3).Rows(0)("AllowedDeviation").ToString()
                        drpTimeDisplay.SelectedValue = ds.Tables(3).Rows(0)("TimeDisplay").ToString()
                        Dim dtBirthday As DateTime = ds.Tables(3).Rows(0)("Birthday")
                        drpMonth.SelectedValue = dtBirthday.Month
                        drpDay.SelectedValue = dtBirthday.Day
                        drpYear.SelectedValue = dtBirthday.Year

                        Trace.Warn("UserEdit", "Mandatory = " & arrCustomStatus(1))

                        If Not ds.Tables(3).Rows(0)("Custom1Value") Is DBNull.Value Then
                            If arrCustomStatus(1) = 2 Then
                                txtMandCust1.Text = ds.Tables(3).Rows(0)("Custom1Value").ToString()
                                Trace.Warn("UserEdit", "Mand Set")
                            Else
                                txtCust1.Text = ds.Tables(3).Rows(0)("Custom1Value").ToString()
                                Trace.Warn("UserEdit", "Optional Set")
                            End If
                        End If
                        If Not ds.Tables(3).Rows(0)("Custom2Value") Is DBNull.Value Then
                            If arrCustomStatus(2) = 2 Then
                                txtMandCust2.Text = ds.Tables(3).Rows(0)("Custom2Value").ToString()
                            Else
                                txtCust2.Text = ds.Tables(3).Rows(0)("Custom2Value").ToString()
                            End If
                        End If

                        If Not ds.Tables(3).Rows(0)("Custom3Value") Is DBNull.Value Then
                            If arrCustomStatus(3) = 2 Then
                                txtMandCust3.Text = ds.Tables(3).Rows(0)("Custom3Value").ToString()
                            Else
                                txtCust3.Text = ds.Tables(3).Rows(0)("Custom3Value").ToString()
                            End If
                        End If
                        If Not ds.Tables(3).Rows(0)("Custom4Value") Is DBNull.Value Then
                            If arrCustomStatus(4) = 2 Then
                                txtMandCust4.Text = ds.Tables(3).Rows(0)("Custom4Value").ToString()
                            Else
                                txtCust4.Text = ds.Tables(3).Rows(0)("Custom4Value").ToString()
                            End If
                        End If

                        If Not ds.Tables(3).Rows(0)("Custom5Value") Is DBNull.Value Then
                            If arrCustomStatus(5) = 2 Then
                                txtMandCust5.Text = ds.Tables(3).Rows(0)("Custom5Value").ToString()
                            Else
                                txtCust5.Text = ds.Tables(3).Rows(0)("Custom5Value").ToString()
                            End If
                        End If
                        rbtnlistOverTime.SelectedValue = ds.Tables(3).Rows(0)("OverTimetype")
                        If rbtnlistOverTime.SelectedValue = "1" Then
                            divDailyhours.Style.Add("display", "block")
                            divWeeklyhours.Style.Add("display", "none")
                            ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(3).Rows(0)("OvertimeDailyHours"))
                            ddlWeeklyHours.SelectedValue = "40"
                        ElseIf rbtnlistOverTime.SelectedValue = "2" Then
                            divDailyhours.Style.Add("display", "none")
                            divWeeklyhours.Style.Add("display", "block")
                            ddlDailyHours.SelectedValue = "8"
                            ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(3).Rows(0)("OvertimeWeeklyHours"))
                        Else
                            divDailyhours.Style.Add("display", "block")
                            divWeeklyhours.Style.Add("display", "block")
                            ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(3).Rows(0)("OvertimeDailyHours"))
                            ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(3).Rows(0)("OvertimeWeeklyHours"))
                        End If
                        chkAproveOverTime.Checked = ds.Tables(3).Rows(0)("ApproveOvertime")
                        If UserInfo.UserStatus(intUserId) <> 1 Then
                            'they are clocked in, => cannot change timezone setting
                            ddlTimeZone.Enabled = False
                            chkDST.Enabled = False
                            If Not IsPostBack Then
                                lblLoginText.ForeColor = Drawing.Color.Red
                                lblLoginText.Text = "<br><br>Note: Cannot edit TZ settings while user is clocked in "
                            End If
                        End If
                    Else
                        If (Not Permissions.AccountHolder(Session("User_ID"))) And Session("manager") Then
                            drpManagersList.SelectedValue = Session("User_ID")
                            drpManagersList.Enabled = False
                        End If
                    End If
                    ddlTimeZone.SelectedValue = ds.Tables(3).Rows(0)("TZOffset")
                    If Convert.ToDecimal(ddlTimeZone.SelectedValue) >= 0.0 Then
                        chkDST.Checked = False
                        chkDST.Enabled = False
                    Else
                        chkDST.Enabled = True
                        chkDST.Checked = ds.Tables(3).Rows(0)("DST")
                    End If
                Else
                    ddlTimeZone.SelectedValue = -5.0
                    chkDST.Checked = True
                End If
            End If
        End If
    End Sub

    Protected Sub btnAddUser_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim IsSaved As Int32 = 0
        Dim dblWage As Double
        Dim intUserId As Int64
        Dim arrSQLCustomValue(5) As String
        Select Case arrCustomStatus(1)
            Case 0
                arrSQLCustomValue(1) = ""
            Case 1
                arrSQLCustomValue(1) = txtCust1.Text
            Case 2
                arrSQLCustomValue(1) = txtMandCust1.Text
            Case Else
                arrSQLCustomValue(1) = ""
        End Select

        Select Case arrCustomStatus(2)
            Case 0
                arrSQLCustomValue(2) = ""
            Case 1
                arrSQLCustomValue(2) = txtCust2.Text
            Case 2
                arrSQLCustomValue(2) = txtMandCust2.Text
            Case Else
                arrSQLCustomValue(2) = ""
        End Select

        Select Case arrCustomStatus(3)
            Case 0
                arrSQLCustomValue(3) = ""
            Case 1
                arrSQLCustomValue(3) = txtCust3.Text
            Case 2
                arrSQLCustomValue(3) = txtMandCust3.Text
            Case Else
                arrSQLCustomValue(3) = ""
        End Select

        Select Case arrCustomStatus(4)
            Case 0
                arrSQLCustomValue(4) = ""
            Case 1
                arrSQLCustomValue(4) = txtCust4.Text
            Case 2
                arrSQLCustomValue(4) = txtMandCust4.Text
            Case Else
                arrSQLCustomValue(4) = ""
        End Select

        Select Case arrCustomStatus(5)
            Case 0
                arrSQLCustomValue(5) = ""
            Case 1
                arrSQLCustomValue(5) = txtCust5.Text
            Case 2
                arrSQLCustomValue(5) = txtMandCust5.Text
            Case Else
                arrSQLCustomValue(5) = ""
        End Select

        Try
            Dim boolAdd As Boolean = True
            If UserInfo.ValidateEmail(txtEmail.Text) = False Then
                boolAdd = False
                lblLoginText.Text = "Please enter a valid email address"
            End If
            If txtAllowedDeviation.Text <> "" Then
                If Not IsNumeric(txtAllowedDeviation.Text) Then
                    boolAdd = False
                    lblLoginText.Text = "Please enter a valid number of minutes in the 'Allowed Deviation' field"
                End If
            End If

            If txtWage.Text <> "" Then
                If Not IsNumeric(Replace(txtWage.Text, "$", "")) Then
                    boolAdd = False
                    lblLoginText.Text = "Please enter a valid wage"
                Else
                    dblWage = CDbl(txtWage.Text)
                End If
            End If
            If Request.QueryString("UserID") IsNot Nothing Then
                intUserId = Convert.ToInt64(Request.QueryString("UserID"))
            Else
                intUserId = -1
                If UserInfo.UsernameExists(txtUsername.Text, Session("Client_ID"), 1) Then
                    'Username exists
                    boolAdd = False
                    lblLoginText.Text = "User " & txtUsername.Text & " already exists."
                End If
            End If
            If boolAdd Then
                Dim objUserInfo As New UserInfo
                Dim OvertimeDailyHours As Double = 0
                Dim OverTime As Integer = 0
                Dim AllowedDeviation As Integer = 0
                Dim OvertimeWeeklyHours As Double = 0
                Dim strBirthday As String
                If drpMonth.SelectedIndex = 0 Then
                    strBirthday = "1/1/1900"
                Else
                    strBirthday = drpMonth.SelectedValue & "/" & drpDay.SelectedValue & "/" & drpYear.SelectedValue
                End If
                If txtAllowedDeviation.Text <> "" Then
                    AllowedDeviation = Convert.ToInt32(txtAllowedDeviation.Text)
                End If
                If rbtnlistOverTime.SelectedValue <> "" Then
                    OverTime = rbtnlistOverTime.SelectedValue
                End If
                If OverTime <> 0 Then
                    If rbtnlistOverTime.SelectedValue = 1 Then
                        OvertimeDailyHours = (ddlDailyHours.SelectedValue) * 60
                    ElseIf rbtnlistOverTime.SelectedValue = 2 Then
                        OvertimeWeeklyHours = (ddlWeeklyHours.SelectedValue) * 60
                    Else
                        OvertimeDailyHours = (ddlDailyHours.SelectedValue) * 60
                        OvertimeWeeklyHours = (ddlWeeklyHours.SelectedValue) * 60
                    End If
                End If

                IsSaved = objUserInfo.sprocSaveUsers(Convert.ToInt64(Session("Client_ID")), intUserId, Convert.ToInt64(drpOffice.SelectedValue), chkManager.Checked, Convert.ToDouble(ddlTimeZone.SelectedValue), chkDST.Checked, ddlOT.SelectedValue, txtUsername.Text, Crypto.ComputeMD5Hash(txtPassword.Text), txtFirstName.Text, txtLastName.Text, txtEmail.Text, txtHomePhone.Text, txtCellPhone.Text, txtWorkPhone.Text, txtWorkExt.Text, txtAddress1.Text, txtAddress2.Text, txtCity.Text, txtState.Text, txtZip.Text, drpTimeDisplay.SelectedValue, strBirthday, chkClockGuard.Checked, AllowedDeviation, chkMobileAccess.Checked, ddlCurrencyType.SelectedValue, dblWage, arrSQLCustomValue(1), arrSQLCustomValue(2), arrSQLCustomValue(3), arrSQLCustomValue(4), arrSQLCustomValue(5), chkEmployeeWorksOvernight.Checked, drpManagersList.SelectedValue, OverTime, OvertimeDailyHours, OvertimeWeeklyHours, chkAproveOverTime.Checked, IsSaved)
                intUserId = IsSaved
                If intUserId > 0 Then
                    Trace.Write("UsersAdd.aspx", "UserID retrieved, = " & intUserId)
                End If
                Trace.Write("UsersAdd.aspx", "Absence Settings Added")
                If chkManager.Checked Then
                    Trace.Write("UsersAdd.aspx", "Begin add permissions section")
                    If Permissions.UsersAddManagerFull(Session("User_ID")) Then
                        objUserInfo.sprocSaveUserPermissions(Convert.ToInt64(Session("Client_ID")), intUserId)
                        Trace.Write("UsersAdd.aspx", "Permissions Added (FULL)")
                    ElseIf Permissions.UsersAddManager(Session("User_ID")) Then
                        'execute the limited permissions add (sproc)
                        objUserInfo.sprocSaveUserPermissions(intUserId, Convert.ToInt64(Session("User_ID")))
                        Trace.Write("UsersAdd.aspx", "Permissions Added (LIMITED)")
                    End If
                Else
                    If Request.QueryString("UserID") IsNot Nothing Then
                        objUserInfo.sprocDeleteUsersPermissions(Convert.ToInt64(Session("Client_ID")), intUserId)
                    End If
                End If
                lblLoginText.ForeColor = Drawing.Color.Green
                lblLoginText.Text = "<br><br>User " & txtUsername.Text & " has been added."
                If ViewState("SelfRegistration") Then
                    ViewState("SelfRegistration") = False
                    'destroy the PIN in session, it has already been used
                    Session("SRPIN") = ""
                    Response.Redirect("login.aspx")
                End If
                SendWelcomeEmail()
                Response.Redirect("users.aspx")
            Else
                lblLoginText.ForeColor = Drawing.Color.Red
                lblLoginText.Text = "<br><br>" & lblLoginText.Text
            End If
        Catch
            lblLoginText.Text = "<br><br>Error: " & Err.Description & " " & Clock.GetNow()
        Finally
        End Try
    End Sub

    Protected Sub SendWelcomeEmail()
        Dim strSubjectLine As String

        Dim strCommonBody1 As String
        Dim strCommonBody2 As String
        Dim strCommonBody3 As String

        Dim strCommonClose1 As String
        Dim strCommonClose2 As String
        Dim strCommonClose3 As String

        Dim strPlainBody As String
        Dim strHTMLBody As String

        strSubjectLine = "Welcome to TimeClockWizard"

        strCommonBody1 = "Hello " & txtFirstName.Text & ","
        strCommonBody2 = "A TimeClockWizard account has been created for you by your boss.  At the end of this email, you will find information on how to access your account."
        strCommonBody3 = "If you have any questions about using TimeClockWizard, do not hesitate to contact us.  Our email address are listed below."

        strCommonClose1 = "Sincerely,"
        strCommonClose2 = "TimeClockWizard Support"
        strCommonClose3 = "info@TimeClockWizard.com"

        strPlainBody = strCommonBody1 & vbCrLf & vbCrLf
        strPlainBody &= strCommonBody2 & vbCrLf & vbCrLf
        strPlainBody &= strCommonBody3 & vbCrLf & vbCrLf
        strPlainBody &= "----------------------------------------" & vbCrLf
        strPlainBody &= "Your TimeClockWizard Account Information" & vbCrLf & vbCrLf
        strPlainBody &= "Login URL: http://" & Request.Cookies("Subdomain").Value.ToLower & ".TimeClockWizard.com" & vbCrLf
        strPlainBody &= "Your Username: " & txtUsername.Text.ToLower & vbCrLf
        strPlainBody &= "Your Password: " & txtPassword.Text.ToLower & vbCrLf
        strPlainBody &= "----------------------------------------"
        strPlainBody &= strCommonClose1 & vbCrLf
        strPlainBody &= strCommonClose2 & vbCrLf
        strPlainBody &= strCommonClose3 & vbCrLf & vbCrLf

        strHTMLBody = "<html><head><title>" & strSubjectLine & "</title>"
        strHTMLBody &= "<style type='text/css'>"
        strHTMLBody &= "body {margin:10px;margin-top:0px;padding:0px;background-color:#FFF;font-family:Verdana;font-size:12px;color:#666;}"
        strHTMLBody &= "</style></head>"
        strHTMLBody &= "<body><center><table width='700' style='color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif; font-size: 13px;' bgcolor='#ffffff' cellspacing='0' cellpadding='0'><tbody>"
        strHTMLBody &= "<td width='100%' height='155' align='center'><img alt='TimeClock Wizard' src='http://www.timeclockwizard.com/emailimages/headers.jpg'></td></tr>"
        strHTMLBody &= "<tr><td width='100%'><table><tbody><tr>"
        strHTMLBody &= "<td width='405' valign='top' style='padding: 10px 0px 10px 30px;'><br><br>"
        strHTMLBody &= strCommonBody1 & "<br><br>"
        strHTMLBody &= strCommonBody2 & "<br><br>"
        strHTMLBody &= strCommonBody3 & "<br><br>"
        strHTMLBody &= "<div style='background-color:#EEE;padding:3px;border:1px solid #CCC;'>"
        strHTMLBody &= "<b>Your TimeClockWizard Account Information</b><br><br>"
        strHTMLBody &= "<b>Login URL:</b> <a href='http://" & Request.Cookies("Subdomain").Value.ToLower & ".TimeClockWizard.com'>http://" & Request.Cookies("Subdomain").Value.ToLower & ".TimeClockWizard.com</a><br>"
        strHTMLBody &= "<b>Your Username:</b> " & txtUsername.Text.ToLower & "<br>"
        strHTMLBody &= "<b>Your Password:</b> " & txtPassword.Text.ToLower
        strHTMLBody &= "</div>"
        strHTMLBody &= strCommonClose1 & "<br>"
        strHTMLBody &= strCommonClose2 & "<br>"
        strHTMLBody &= strCommonClose3 & "<br><br>"
        strHTMLBody &= "</td><td width='235' valign='top' style='padding: 10px 30px 10px 0px;'>"
        strHTMLBody &= "<p><img alt='Timeclock Login Box' src='http://www.timeclockwizard.com/emailimages/loginbox.jpg'></p>"
        strHTMLBody &= "<p><img alt='Advantage Box' src='http://www.timeclockwizard.com/emailimages/advantagebox.jpg' border='0'></p>"
        strHTMLBody &= "<p><img alt='Security Box' src='http://www.timeclockwizard.com/emailimages/securitybox.jpg' border='0'></p>"
        strHTMLBody &= "</td></tr></tbody></table></td></tr>"
        strHTMLBody &= "<tr><td width='100%' ><hr style='margin: 0px 30px; border: currentColor; height: 1px; background-color: rgb(153, 153, 153);' >"
        strHTMLBody &= "</td></tr></tbody></table></center>"
        strHTMLBody &= "</body></html>"

        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("info@TimeClockWizard.com", "TimeClockWizard Support")
        mail.DeliveryNotificationOptions = DeliveryNotificationOptions.Never
        mail.Subject = strSubjectLine
        mail.To.Add(txtEmail.Text.ToLower)

        'set the content
        Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString(strPlainBody, Nothing, "text/plain")
        Dim HTMLView As AlternateView = AlternateView.CreateAlternateViewFromString(strHTMLBody, Nothing, "text/html")

        Try
            Dim logo As New LinkedResource("C:\Inetpub\wwwroot\TimeClockWizard.com\ClockitinSite\Images\emaillogo2.jpg", "image/jpeg")
            logo.ContentId = "ciiLogo"
            HTMLView.LinkedResources.Add(logo)
        Catch ex As Exception

        End Try

        mail.AlternateViews.Add(plainView)
        mail.AlternateViews.Add(HTMLView)

        'send the message
        Dim smtp As New SmtpClient()
        smtp.Send(mail)
    End Sub

    Protected Sub ddlTimeZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTimeZone.SelectedIndexChanged
        'if timezone is positive, disable DST option
        If Convert.ToDecimal(ddlTimeZone.SelectedValue) >= 0.0 Then
            chkDST.Checked = False
            chkDST.Enabled = False
        Else
            chkDST.Enabled = True
            chkDST.Checked = False
        End If
    End Sub

    'Protected Sub rbtnlistOverTime_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rbtnlistOverTime.SelectedIndexChanged
    '    Try
    '        If rbtnlistOverTime.SelectedValue = 1 Then
    '            divDailyhours.Visible = True
    '            divWeeklyhours.Visible = False
    '        ElseIf rbtnlistOverTime.SelectedValue = 2 Then
    '            divWeeklyhours.Visible = True
    '            divDailyhours.Visible = False
    '        Else
    '            divWeeklyhours.Visible = True
    '            divDailyhours.Visible = True
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Public Sub BindonLoad()
        Dim pnlMasterTabUsers As Panel = Master.FindControl("pnlTabUsers")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2AddUser As Panel = Master.FindControl("pnlTab2AddUser")
        Dim intUserId As Int64 = 0
        If Request.QueryString("UserID") IsNot Nothing Then
            divHeadertxt.Attributes.Add("class", "edit-user")
            lblHeadertext.Text = "Edit"
            pnlMasterTabUsers.CssClass = "tab tabOn"
            MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users
            intUserId = Request.QueryString("UserID")
            litUsersName.Text = UserInfo.NameFromID(intUserId, 1)
            Trace.Warn("UsersEdit", "PageLoad()")
            divpassword.Visible = False
            divConpassword.Visible = False
            imgHelpAllowedDeviation.ToolTip = "offsetx=[-200] cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Allowed Deviation] body=[The number of minutes that a user must clock in after their scheduled time to be considered late.<br><br>It determines being late or early for both clocking and clocking out.<br><br>For example, 0 means they must clock in exactly when scheduled and any deviation would result in being considered early or late.]"
            imageEmployeeWorksOvernight.ToolTip = "cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Allow User Work Overnight] body=[Checking this option allows to schedule user to work overnight.<br><br>e.g., Joe work hours are from tonight 10:00pm to tomorrow morning 6:00am.<br><br>When you uncheck this option, it will check user schedule to make sure user not working overnight. If any records found going forward from this week, it will not allow you to uncheck.]"
        Else
            Dim ds As DataSet
            divpassword.Visible = True
            divConpassword.Visible = True
            divHeadertxt.Attributes.Add("class", "add-user")
            lblHeadertext.Text = "Add User"
            imgHelpEmail.ToolTip = "cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Why do we need an email address?] body=[We will send the user a welcome email, with details on how to access their new TimeClockWizard account.  Having an email address also allows users to use the password recovery feature if they forget their password.]"
            imgHelpAllowedDeviation.ToolTip = "offsetx=[-200] cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Allowed Deviation] body=[The number of minutes that a user must clock in after their scheduled time to be considered late.<br><br>It determines being late or early for both clocking and clocking out.<br><br>For example, 0 means they must clock in exactly when scheduled and any deviation would result in being considered early or late.]"
            imageEmployeeWorksOvernight.ToolTip = "cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Allow User Work Overnight] body=[Allow User Work Overnight<br><br>Allow Emp Work Overnight<br><br>Employee Overnight]"
            If Session("User_ID") = "" Then
                'possible self-registration attempt.  Check to see if it is valid

                ds = objUserInfo.sprocGetSelfRegistrationpin(Convert.ToInt64(Session("Client_ID")))
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        If Not ds.Tables(0).Rows(0)("SelfRegistration") Then
                            'they are trying to self register when it is not allowed
                            Server.Transfer("Login.aspx")
                        Else
                            If Session("SRPIN") <> ds.Tables(0).Rows(0)("SelfRegistrationPin") Then
                                'trying to brute force the page without entering the pin
                                Server.Transfer("Login.aspx")
                            End If
                        End If
                    Else
                        Server.Transfer("Login.aspx")
                    End If
                    If ds.Tables(1).Rows.Count > 0 Then
                        rbtnlistOverTime.SelectedValue = ds.Tables(1).Rows(0)("OverTimetype")
                        If rbtnlistOverTime.SelectedValue = "1" Then
                            divDailyhours.Style.Add("display", "block")
                            divWeeklyhours.Style.Add("display", "none")
                            ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(1).Rows(0)("OvertimeDailyHours"))
                            ddlWeeklyHours.SelectedValue = "40"
                        ElseIf rbtnlistOverTime.SelectedValue = "2" Then
                            divDailyhours.Style.Add("display", "none")
                            divWeeklyhours.Style.Add("display", "block")
                            ddlDailyHours.SelectedValue = "8"
                            ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(1).Rows(0)("OvertimeWeeklyHours"))
                        Else
                            divDailyhours.Style.Add("display", "block")
                            divWeeklyhours.Style.Add("display", "block")
                            ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(1).Rows(0)("OvertimeDailyHours"))
                            ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(1).Rows(0)("OvertimeWeeklyHours"))
                        End If
                        chkAproveOverTime.Checked = ds.Tables(1).Rows(0)("ApproveOvertime")
                    End If
                End If
                ViewState("SelfRegistration") = True
                pnlRequiredCheckBoxes.Visible = False
                chkMobileAccess.Checked = False
                pnlWageAndDeviation.Visible = False
            Else
                ds = objUserInfo.sprocGetSelfRegistrationpin(Convert.ToInt64(Session("Client_ID")))
                If ds.Tables(1).Rows.Count > 0 Then
                    rbtnlistOverTime.SelectedValue = ds.Tables(1).Rows(0)("OverTimetype")
                    If rbtnlistOverTime.SelectedValue = "1" Then
                        divDailyhours.Style.Add("display", "block")
                        divWeeklyhours.Style.Add("display", "none")
                        ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(1).Rows(0)("OvertimeDailyHours"))
                        ddlWeeklyHours.SelectedValue = "40"
                    ElseIf rbtnlistOverTime.SelectedValue = "2" Then
                        divDailyhours.Style.Add("display", "none")
                        divWeeklyhours.Style.Add("display", "block")
                        ddlDailyHours.SelectedValue = "8"
                        ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(1).Rows(0)("OvertimeWeeklyHours"))
                    Else
                        divDailyhours.Style.Add("display", "block")
                        divWeeklyhours.Style.Add("display", "block")
                        ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(1).Rows(0)("OvertimeDailyHours"))
                        ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(1).Rows(0)("OvertimeWeeklyHours"))
                    End If
                    chkAproveOverTime.Checked = ds.Tables(1).Rows(0)("ApproveOvertime")
                End If
                pnlMasterTabUsers.CssClass = "tab tabOn"
                MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users
                pnlMasterTab2AddUser.CssClass = "tab2 tab2On"
                If Not Permissions.UsersAdd(Session("User_ID")) Then
                    Server.Transfer("Users.aspx")
                End If
            End If
        End If
    End Sub

    Protected Sub chkEmployeeWorksOvernight_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEmployeeWorksOvernight.CheckedChanged
        Trace.Write("Employee Works Overnite checkbox clicked")
        Dim IsCount As Integer = 0
        If Not chkEmployeeWorksOvernight.Checked Then
            If Request.QueryString("UserID") IsNot Nothing Then
                Dim objUserInfo As New UserInfo
                IsCount = objUserInfo.sprocCheckUserOKtoRemoveOverniteAttribute(Convert.ToInt64(Session("Client_ID")), Convert.ToInt64(Request.QueryString("UserID")), IsCount)
            End If
            If IsCount > 0 Then
                chkEmployeeWorksOvernight.Checked = True
                lblLoginText.ForeColor = Drawing.Color.Red
                lblLoginText.Text = "<br><br>Note: Employee schedule indicates working overnite for one or more days!<br>Please adjust schedule first and try again!"
            Else
                lblLoginText.Text = String.Empty
            End If
        Else
            lblLoginText.Text = String.Empty
        End If
    End Sub

End Class
