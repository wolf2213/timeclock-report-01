
Partial Class UsersDeleted
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Permissions.UsersRecover(Session("User_ID")) Then
            Server.Transfer("Users.aspx")
        End If

        Dim pnlMasterTabUsers As Panel = Master.FindControl("pnlTabUsers")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2DeletedUsers As Panel = Master.FindControl("pnlTab2DeletedUsers")

        pnlMasterTabUsers.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users
        pnlMasterTab2DeletedUsers.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            SearchSortUsersList(txtUsersFilter.Text, ddlSortUsersBy.SelectedItem.Text, ddlSortUsersOderBy.SelectedItem.Text)
        End If
    End Sub

    'Srini Mannem - 02/07/2011 - not required any more search and sort users action combining together
    'Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    CreateUsersList()
    '    txtUsersFilter.Focus()
    'End Sub

    Protected Sub ibSoryUsersBy_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        SearchSortUsersList(txtUsersFilter.Text, ddlSortUsersBy.SelectedItem.Text, ddlSortUsersOderBy.SelectedItem.Text)
        rptUsersList.Focus()
    End Sub

    'Srini Mannem - 02/07/2011 - not required any more search and sort users action combining together
    'Protected Sub CreateUsersList()
    '    Dim objDR As System.Data.SqlClient.SqlDataReader
    '    Dim objCommand As System.Data.SqlClient.SqlCommand
    '    Dim objConnection As System.Data.SqlClient.SqlConnection
    '    objConnection = New System.Data.SqlClient.SqlConnection

    '    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
    '    objConnection.Open()
    '    objCommand = New System.Data.SqlClient.SqlCommand()
    '    objCommand.Connection = objConnection
    '    objCommand.CommandText = "SELECT User_ID, Username, (LastName + ', ' + FirstName) AS FullName, Office_ID, Manager, DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 AND (FirstName LIKE '%" & txtUsersFilter.Text & "%' OR LastName LIKE '%" & txtUsersFilter.Text & "%') ORDER BY FullName ASC"
    '    objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
    '    objDR = objCommand.ExecuteReader()

    '    rptUsersList.DataSource = objDR
    '    rptUsersList.DataBind()

    '    objDR = Nothing
    '    objCommand = Nothing
    '    objConnection.Close()
    '    objConnection = Nothing
    'End Sub

    Protected Sub SearchSortUsersList(ByVal strSearchUsersText As String, ByVal strUsersBy As String, ByVal strSortOrder As String)
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection
        Dim strSQLStatement As String = String.Empty
        Dim strSQLStatementForSearchUsers As String = String.Empty

        If (String.IsNullOrEmpty(strSearchUsersText) = False) Then
            strSQLStatementForSearchUsers = strSQLStatementForSearchUsers & " AND (FirstName LIKE '%" & strSearchUsersText & "%' OR LastName LIKE '%" & strSearchUsersText & "%')"
        End If

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection

        If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
            If (strUsersBy = "First Name") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (FirstName + ', ' + LastName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY FirstName"
            ElseIf (strUsersBy = "Last Name") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY LastName"
            ElseIf (strUsersBy = "Office") Then
                strSQLStatement = strSQLStatement & "SELECT U.User_ID, U.Username, (U.LastName + ', ' + U.FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, O.Office_ID, O.Name as 'OfficeName', U.Manager, DeletedOn"
                strSQLStatement = strSQLStatement & " FROM Users U LEFT JOIN Offices O ON U.Client_ID = O.Client_ID AND U.Office_ID = O.Office_ID WHERE U.Client_ID = @Client_ID AND U.Active = 0 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY OfficeName"
            ElseIf (strUsersBy = "Level") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, CASE WHEN Manager = 1 THEN 'Manager' ELSE 'User' END AS 'UserOrManager', DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY UserOrManager"
            ElseIf (strUsersBy = "Inactivated Date") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY DeletedOn"
            ElseIf (strUsersBy = "Assigned Manager") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY ManagerName"
            End If

            objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
        Else
            If (strUsersBy = "First Name") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (FirstName + ', ' + LastName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY FirstName"
            ElseIf (strUsersBy = "Last Name") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY LastName"
            ElseIf (strUsersBy = "Office") Then
                strSQLStatement = strSQLStatement & "SELECT U.User_ID, U.Username, (U.LastName + ', ' + U.FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, O.Office_ID, O.Name as 'OfficeName', U.Manager, DeletedOn"
                strSQLStatement = strSQLStatement & " FROM Users U LEFT JOIN Offices O ON U.Client_ID = O.Client_ID AND U.Office_ID = O.Office_ID WHERE U.Client_ID = @Client_ID AND U.Active = 0 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY OfficeName"
            ElseIf (strUsersBy = "Level") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, CASE WHEN Manager = 1 THEN 'Manager' ELSE 'User' END AS 'UserOrManager', DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY UserOrManager"
            ElseIf (strUsersBy = "Inactivated Date") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY DeletedOn"
            ElseIf (strUsersBy = "Assigned Manager") Then
                strSQLStatement = strSQLStatement & "SELECT User_ID, Username, (LastName + ', ' + FirstName) AS FullName, dbo.fnGetUserName(ManagerUserId) AS ManagerName, Office_ID, Manager, DeletedOn FROM  Users WHERE Client_ID = @Client_ID AND Active = 0 " & strSQLStatementForSearchUsers
                strSQLStatement = strSQLStatement & " ORDER BY ManagerName"
            End If
        End If

        If (strSortOrder = "Ascending") Then
            strSQLStatement = strSQLStatement & " ASC"
        Else
            strSQLStatement = strSQLStatement & " DESC"
        End If

        objCommand.CommandText = strSQLStatement
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        rptUsersList.DataSource = objDR
        rptUsersList.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptUsersList_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblName As Label = e.Item.FindControl("lblName")
            Dim lblAssignedManager As Label = e.Item.FindControl("lblAssignedManager")
            Dim lblOffice As Label = e.Item.FindControl("lblOffice")
            Dim lblLevel As Label = e.Item.FindControl("lblLevel")
            Dim lblDeletedOn As Label = e.Item.FindControl("lblDeletedOn")
            Dim btnRecover As ImageButton = e.Item.FindControl("btnRecover")

            btnRecover.ToolTip = "offsetx=[-150] header=[Activate User] body=[]"

            lblName.Text = e.Item.DataItem("FullName")

            If String.IsNullOrEmpty(e.Item.DataItem("ManagerName")) Then
                lblAssignedManager.Text = ""
            Else
                lblAssignedManager.Text = e.Item.DataItem("ManagerName")
            End If

            If e.Item.DataItem("Office_ID") Is DBNull.Value Then
                'if the office isnt set it causes an error, so do this check
                lblOffice.Text = ""
            Else
                lblOffice.Text = Offices.OfficeName(e.Item.DataItem("Office_ID"))
            End If

            If e.Item.DataItem("Manager") Then
                lblLevel.Text = "Manager"
            Else
                lblLevel.Text = "User"
            End If

            If e.Item.DataItem("DeletedOn") Is DBNull.Value Then
                lblDeletedOn.Text = ""
            Else
                lblDeletedOn.Text = e.Item.DataItem("DeletedOn")
            End If

            Dim boolUserTaken As Boolean = UserInfo.UsernameExists(e.Item.DataItem("Username"), Session("Client_ID"), 1)
            Trace.Write("UsersDeleted", "Uesr taken = " & boolUserTaken)

            If boolUserTaken Then
                btnRecover.CommandName = "UpdateUsername"
                btnRecover.CommandArgument = e.Item.DataItem("User_ID")
            Else
                btnRecover.CommandName = "RecoverUser"
                btnRecover.CommandArgument = e.Item.DataItem("User_ID")
            End If

        End If
    End Sub

    Protected Sub rptUserList_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim intUser_ID As Int64 = e.CommandArgument

        Trace.Write("UsersDeleted", "Command Name = " & e.CommandName)

        Select Case e.CommandName
            Case "UpdateUsername"
                pnlChangeUsername.Visible = True

                Dim objDR As System.Data.SqlClient.SqlDataReader
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT Username, FirstName FROM Users WHERE User_ID = @User_ID"
                objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)
                objDR = objCommand.ExecuteReader()

                If objDR.HasRows Then
                    objDR.Read()
                    txtChangeUser.Text = objDR("Username")
                    litChangeUser.Text = objDR("FirstName")
                    litChangeUserID.text = intUser_ID

                End If
                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing
            Case "RecoverUser"
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "UPDATE Users SET Active = 1 WHERE User_ID = @User_ID"
                objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                lblMessage.visible = True

                SearchSortUsersList(txtUsersFilter.Text, ddlSortUsersBy.SelectedItem.Text, ddlSortUsersOderBy.SelectedItem.Text)
        End Select
    End Sub

    Protected Sub btnChangeUser_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim boolActivate As Boolean = False

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT User_ID FROM Users WHERE Client_ID = @Client_ID AND Username = @Username AND Active = 1"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@Username", txtChangeUser.Text)
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            lblChangeError.visible = True
        Else
            boolActivate = True
        End If
        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing


        If boolActivate Then
            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "UPDATE Users SET Active = 1, Username = @Username WHERE User_ID = @User_ID"
            objCommand.Parameters.AddWithValue("@User_ID", litChangeUserID.Text)
            objCommand.Parameters.AddWithValue("@Username", txtChangeUser.Text)

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            pnlChangeUsername.Visible = False

            lblMessage.visible = True

            SearchSortUsersList(txtUsersFilter.Text, ddlSortUsersBy.SelectedItem.Text, ddlSortUsersOderBy.SelectedItem.Text)
        End If
    End Sub
End Class
