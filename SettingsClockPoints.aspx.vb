
Partial Class SettingsClockPoints
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabSettings As Panel = Master.FindControl("pnlTabSettings")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2ClockPoints As Panel = Master.FindControl("pnlTab2ClockPoints")

        pnlMasterTabSettings.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Settings
        pnlMasterTab2ClockPoints.CssClass = "tab2 tab2On"

        If Not Permissions.SettingsClockPoints(Session("User_ID")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            btnAddClockPoint.ToolTip = "header=[Add New ClockPoint] body=[]"
            imgHelpClockPoints.ToolTip = "cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[ClockPoints] body=[ClockPoints help you make sure users clock in and out where they are supposed to.  When a user clocks in from a ClockPoint a <img src='images/timesheet_computer.png' style='vertical-align:middle;'> icon appears on the timesheet rather than the <img src='images/timesheet_computer0.png' style='vertical-align:middle;'> icon.  This makes it easy to catch when users are clocking in/out from unverified locations.]"
            CreateClockPointList()
            DisplayDescription()
            CheckCookie()
            CheckRestriction()
        End If
    End Sub

    Protected Sub rblType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DisplayDescription()
    End Sub

    Protected Sub DisplayDescription()
        Select Case rblType.SelectedValue
            Case "1"
                lblTypeDescription.Text = "Make this computer a ClockPoint. Do not clear your browser's cookies.  This will remove the ClockPoint."
                lblTypeDescription.Visible = True
                lblIPLabel.Visible = False
                txtIPAddress.Visible = False
            Case "2"
                lblTypeDescription.Text = "Make every device that uses this internet connection (" & Request.ServerVariables("REMOTE_ADDR") & ") a ClockPoint.  We recommend this option only to offices using a static IP address."
                lblTypeDescription.Visible = True
                lblIPLabel.Visible = False
                txtIPAddress.Visible = False
            Case "3"
                lblTypeDescription.Visible = False
                lblIPLabel.Visible = True
                txtIPAddress.Visible = True
        End Select
    End Sub

    Protected Sub CheckCookie()

        rblType.Enabled = True
        rblType.Visible = True
        btnAddClockPoint.Visible = True
        btnAddClockPoint.Enabled = True
        txtClockPointName.Visible = True
        lblExample.Visible = True
        lblNameLabel.Visible = True

        'check cookie
        Dim boolCookie As Boolean = False
        If Not Request.Cookies("ClockPoint") Is Nothing Then
            Trace.Write("SettingsClockPoints", "Cookie = " & Request.Cookies("ClockPoint").Value)
            Trace.Write("SettingsClockPoints", "Cookie Detected")

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT Name FROM Locations WHERE Location_ID = @Location_ID AND Client_ID = @Client_ID"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@Location_ID", Request.Cookies("ClockPoint").Value)

            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                rblType.SelectedValue = "2"
                rblType.Items.Item(0).Enabled = False
                lblTypeDescription.Text = "You already have this current computer specifed as a ClockPoint."
                boolCookie = True
                rblType.SelectedValue = 2
            Else
                Request.Cookies("ClockPoint").Expires = DateTime.Now.AddMinutes(-10)
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If

        'check IP address
        Dim objDRIP As System.Data.SqlClient.SqlDataReader
        Dim objCommandIP As System.Data.SqlClient.SqlCommand
        Dim objConnectionIP As System.Data.SqlClient.SqlConnection
        objConnectionIP = New System.Data.SqlClient.SqlConnection

        objConnectionIP.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnectionIP.Open()
        objCommandIP = New System.Data.SqlClient.SqlCommand()
        objCommandIP.Connection = objConnectionIP
        objCommandIP.CommandText = "SELECT Name FROM Locations WHERE Location = @Location AND Client_ID = @Client_ID"
        objCommandIP.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommandIP.Parameters.AddWithValue("@Location", Request.ServerVariables("REMOTE_ADDR"))

        objDRIP = objCommandIP.ExecuteReader()

        If objDRIP.HasRows Then
            rblType.SelectedValue = "1"
            rblType.Items.Item(1).Enabled = False
            lblTypeDescription.Text = "You already have this internet connection specifed as a ClockPoint."
            rblType.SelectedValue = 1

            If boolCookie Then
                rblType.Items.Item(1).Enabled = False
                rblType.SelectedValue = 3
                lblIPLabel.Visible = True
                txtIPAddress.Visible = True

                lblTypeDescription.Text = "Both the current internet connection and this specific computer are already ClockPoints."
            End If
        End If

        objDRIP = Nothing
        objCommandIP = Nothing
        objConnectionIP.Close()
        objConnectionIP = Nothing
    End Sub

    Protected Sub CheckRestriction()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT ClockPointRestriction FROM Preferences WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        objDR.Read()

        If objDR("ClockPointRestriction") Then
            rblClockPointRestriction.SelectedValue = "1"
        Else
            rblClockPointRestriction.SelectedValue = "0"
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub btnSaveClockPointRestriction_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE Preferences SET ClockPointRestriction = @ClockPointRestriction WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@ClockPointRestriction", rblClockPointRestriction.SelectedValue)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If rblClockPointRestriction.SelectedValue = "1" Then
            lblChanged.Text = "<br />ClockPoint Restricition is now Enabled"
        Else
            lblChanged.Text = "<br />ClockPoint Restricition is now Disabled"
        End If

        lblChanged.Visible = True
        timMsg.Enabled = True
    End Sub

    Protected Sub CreateClockPointList()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM Locations WHERE Client_ID = @Client_ID ORDER BY Name ASC"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        rptClockPointList.DataSource = objDR
        rptClockPointList.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptClockPointList_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblClockPointName As Label = e.Item.FindControl("lblClockPointName")
            Dim lblType As Label = e.Item.FindControl("lblType")
            Dim imgEdit As ImageButton = e.Item.FindControl("imgEdit")
            Dim imgDelete As ImageButton = e.Item.FindControl("imgDelete")
            Dim lblDeleteClockPointName As Label = e.Item.FindControl("lblDeleteClockPointName")
            Dim btnConfirmDelete As Button = e.Item.FindControl("btnConfirmDelete")
            Dim btnCancelDelete As Button = e.Item.FindControl("btnCancelDelete")
            Dim txtEditName As TextBox = e.Item.FindControl("txtEditName")
            Dim btnEditSave As ImageButton = e.Item.FindControl("btnEditSave")
            Dim btnEditCancel As ImageButton = e.Item.FindControl("btnEditCancel")


            lblClockPointName.Text = e.Item.DataItem("Name")

            If e.Item.DataItem("Type") = 1 Then
                lblType.Text = "Computer"
            Else
                lblType.Text = "Internet Connection (" & e.Item.DataItem("Location") & ")"
            End If

            imgEdit.CommandArgument = e.Item.DataItem("Location_ID")
            imgEdit.ToolTip = "offsetx=[-150] header=[Edit ClockPoint] body=[]"
            imgDelete.CommandArgument = e.Item.DataItem("Location_ID")
            imgDelete.ToolTip = "offsetx=[-150] header=[Delete ClockPoint] body=[]"

            lblDeleteClockPointName.Text = e.Item.DataItem("Name")

            btnConfirmDelete.CommandName = "ConfirmDelete"
            btnConfirmDelete.CommandArgument = e.Item.DataItem("Location_ID")

            btnCancelDelete.CommandName = "CancelDelete"
            btnCancelDelete.CommandArgument = e.Item.DataItem("Location_ID")

            txtEditName.Text = e.Item.DataItem("Name")

            btnEditSave.CommandArgument = e.Item.DataItem("Location_ID")
            btnEditSave.ToolTip = "offsetx=[-150] header=[Save Changes] body=[]"
            btnEditCancel.CommandArgument = e.Item.DataItem("Location_ID")
            btnEditCancel.ToolTip = "offsetx=[-150] header=[Cancel Changes] body=[]"
        End If
    End Sub

    Protected Sub rptClockPointList_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim intClockPoint_ID As Int64 = e.CommandArgument
        Dim pnlName As Panel = e.Item.FindControl("pnlName")
        Dim pnlConfirmDelete As Panel = e.Item.FindControl("pnlConfirmDelete")
        Dim pnlEdit As Panel = e.Item.FindControl("pnlEdit")
        Dim pnlNormalButtons As Panel = e.Item.FindControl("pnlNormalButtons")
        Dim pnlEditButtons As Panel = e.Item.FindControl("pnlEditButtons")
        Dim txtEditName As TextBox = e.Item.FindControl("txtEditName")
        Dim lblEditMessage As Label = e.Item.FindControl("lblEditMessage")

        lblEditMessage.Visible = False

        Select Case e.CommandName
            Case "ClockPointEdit"
                pnlName.Visible = False
                pnlEdit.Visible = True
                pnlConfirmDelete.Visible = False
                pnlNormalButtons.Visible = False
                pnlEditButtons.Visible = True

            Case "ClockPointDelete"
                pnlName.Visible = False
                pnlEdit.Visible = False
                pnlConfirmDelete.Visible = True
                pnlNormalButtons.Visible = True
                pnlEditButtons.Visible = False
            Case "CancelDelete"
                pnlName.Visible = True
                pnlEdit.Visible = False
                pnlConfirmDelete.Visible = False
                pnlNormalButtons.Visible = True
                pnlEditButtons.Visible = False

            Case "ConfirmDelete"
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "DELETE FROM Locations WHERE Client_ID = @Client_ID AND Location_ID = @Location_ID"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objCommand.Parameters.AddWithValue("@Location_ID", intClockPoint_ID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "UPDATE Users SET Office_ID = 0 WHERE Office_ID = @Office_ID AND Client_ID = @Client_ID"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objCommand.Parameters.AddWithValue("@Office_ID", intClockPoint_ID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                CreateClockPointList()
                checkCookie()
            Case "EditCancel"
                txtEditName.Text = Locations.LocationName(intClockPoint_ID)

                pnlName.Visible = True
                pnlEdit.Visible = False
                pnlConfirmDelete.Visible = False
                pnlNormalButtons.Visible = True
                pnlEditButtons.Visible = False
            Case "EditSave"
                Dim boolSave As Boolean = True

                If txtEditName.Text = "" Then
                    boolSave = False
                    lblEditMessage.Visible = True
                    lblEditMessage.Text = "Please choose a name"
                    lblEditMessage.ForeColor = Drawing.Color.Red
                Else
                    If Not checkNameAvailability(txtEditName.Text, intClockPoint_ID) Then
                        boolSave = False
                        lblEditMessage.Visible = True
                        lblEditMessage.Text = "<br>This name is already being used"
                        lblEditMessage.ForeColor = Drawing.Color.Red
                    End If
                End If

                If boolSave Then
                    Dim objCommand As System.Data.SqlClient.SqlCommand
                    Dim objConnection As System.Data.SqlClient.SqlConnection
                    objConnection = New System.Data.SqlClient.SqlConnection

                    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                    objConnection.Open()
                    objCommand = New System.Data.SqlClient.SqlCommand()
                    objCommand.Connection = objConnection
                    objCommand.CommandText = "UPDATE Locations SET Name = @Name WHERE Location_ID = @Location_ID"
                    objCommand.Parameters.AddWithValue("@Name", txtEditName.Text)
                    objCommand.Parameters.AddWithValue("@Location_ID", intClockPoint_ID)

                    objCommand.ExecuteNonQuery()

                    objCommand = Nothing
                    objConnection.Close()
                    objConnection = Nothing

                    CreateClockPointList()
                End If
        End Select
    End Sub

    Protected Sub btnAddClockPoint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddClockPoint.Click
        If txtClockPointName.Text <> "" Then
            If checkNameAvailability(txtClockPointName.Text) Then
                Dim strIP As String

                Dim objCommandAdd As System.Data.SqlClient.SqlCommand
                Dim objConnectionAdd As System.Data.SqlClient.SqlConnection
                objConnectionAdd = New System.Data.SqlClient.SqlConnection

                objConnectionAdd.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnectionAdd.Open()
                objCommandAdd = New System.Data.SqlClient.SqlCommand()
                objCommandAdd.Connection = objConnectionAdd
                objCommandAdd.CommandText = "INSERT INTO Locations ([Client_ID], [Type], [Name], [Location]) VALUES (@Client_ID, @Type, @Name, @Location)"

                Select Case rblType.SelectedValue
                    Case "1"
                        strIP = "0"
                    Case "2"
                        strIP = Request.ServerVariables("REMOTE_ADDR")
                    Case "3"
                        strIP = txtIPAddress.Text
                    Case Else
                        strIP = ""
                End Select

                objCommandAdd.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objCommandAdd.Parameters.AddWithValue("@Type", rblType.SelectedValue)
                objCommandAdd.Parameters.AddWithValue("@Name", txtClockPointName.Text)
                objCommandAdd.Parameters.AddWithValue("@Location", strIP)
                objCommandAdd.ExecuteNonQuery()

                objCommandAdd = Nothing
                objConnectionAdd.Close()
                objConnectionAdd = Nothing

                'set the cookie if necessary
                If rblType.SelectedValue = "1" Then
                    Dim intNewLocationId As Int64
                    Dim objDR As System.Data.SqlClient.SqlDataReader
                    Dim objCommand As System.Data.SqlClient.SqlCommand
                    Dim objConnection As System.Data.SqlClient.SqlConnection
                    objConnection = New System.Data.SqlClient.SqlConnection

                    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                    objConnection.Open()
                    objCommand = New System.Data.SqlClient.SqlCommand()
                    objCommand.Connection = objConnection
                    objCommand.CommandText = "SELECT MAX(Location_ID) AS NewLocationID FROM Locations WHERE Client_ID = @Client_ID AND Type = 1"
                    objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                    objDR = objCommand.ExecuteReader()
                    objDR.Read()
                    intNewLocationId = objDR("NewLocationID")

                    objDR = Nothing
                    objCommand = Nothing
                    objConnection.Close()
                    objConnection = Nothing

                    'clear current cookie
                    Response.Cookies("ClockPoint").Expires = DateTime.Now.AddYears(-1)

                    'reset cookie
                    Response.Cookies("ClockPoint").Expires = DateTime.Now.AddYears(50)
                    Response.Cookies("ClockPoint").Value = intNewLocationId

                    checkCookie()
                End If

                txtClockPointName.Text = ""

                'display success message
                lblAddMessage.Visible = True
                lblAddMessage.ForeColor = Drawing.Color.Green
                lblAddMessage.Text = "ClockPoint Successfully Added"
                timMsg.Enabled = True

                CreateClockPointList()

            Else
                'same name, display error
                lblAddMessage.Visible = True
                lblAddMessage.ForeColor = Drawing.Color.Red
                lblAddMessage.Text = "Error: There is already an office named " & txtClockPointName.Text
                timMsg.Enabled = True
            End If
        End If
    End Sub

    Protected Function checkNameAvailability(ByVal strOfficeName As String, Optional ByVal intEditLocationId As Int64 = 0)
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        Dim strSQLQuery As String

        If intEditLocationId = 0 Then
            strSQLQuery = "SELECT Location_ID FROM Locations WHERE [Name] = @Name AND Client_ID = @Client_ID"
        Else
            strSQLQuery = "SELECT [Location_ID] FROM [Locations] WHERE [Name] = @Name AND [Client_ID] = @Client_ID AND [Location_ID] <> @Location_ID"
        End If

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection

        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@Location_ID", intEditLocationId)
        objCommand.Parameters.AddWithValue("@Name", strOfficeName)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR = objCommand.ExecuteReader()
        objDR.Read()

        If objDR.HasRows Then
            checkNameAvailability = False
        Else
            checkNameAvailability = True
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Function

    Protected Sub timMsg_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        lblAddMessage.Text = ""
        lblAddMessage.Visible = False
        lblChanged.Visible = False
    End Sub
End Class