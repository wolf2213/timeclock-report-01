<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SettingsSubscription.aspx.vb" Inherits="SettingsSubscription" title="TimeClockWizard - My Subscription" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script language="javascript" type="text/javascript">
        function showUpdateBox() {
            document.getElementById('<%=pnlCreditCardStored.ClientID%>').style.display = 'none';
            document.getElementById('<%=pnlCreditCardInfo.ClientID%>').style.display = 'block';
        }
        
        function disableUpdateButton() {
            document.getElementById('<%=btnUpdate.ClientID%>').disabled = true;
            document.getElementById('<%=btnUpdate.ClientID%>').value = 'Processing...';
        }
        
        function disableActivateButton() {
            document.getElementById('<%=btnActivate.ClientID%>').disabled = true;
            document.getElementById('<%=btnActivate.ClientID%>').value = 'Processing...';
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
                <div class="main-content">

 <div style="margin-left: 25px; float: left;"> 

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="width: 500px; text-align: center; background: #004a71; padding: 15px 0; margin-bottom: 20px;">

                        <asp:Panel ID="pnlTrialExpiration" runat="server" style="text-align:center;" Visible="false" ForeColor="White">
                            Your free trial expires <asp:Label ID="lblTrialExpirationDays" runat="server" Text="0"></asp:Label><asp:Label ID="lblTrialExpirationDate" runat="server" Text="0"></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="pnlTrialExpired" runat="server" style="text-align:center;" Visible="false" ForeColor="White">
                            Your free trial expired on <asp:Label ID="lblTrialExpiredOn" runat="server" Text="0"></asp:Label>
                        </asp:Panel>
</div>
            

<div class="clear"></div>
                <br />

<div style="width: 300px;">

                   <h4>Account Usage</h4>

                        <asp:Panel ID="pnlAccountUsage" runat="server">
                           
                           
                    <div class="form-holder" style="width: 278px;">

											    <div class="field-holder" style="padding-right: 0px;">

                            <div align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="110px"><strong>Base Account Fee</strong></td>
                                    <td width="60px"><asp:Label ID="lblBaseFee" runat="server" Text=""></asp:Label></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>User Accounts</strong></td>
                                    <td><asp:Label ID="lblUserFeeTotal" runat="server" Text=""></asp:Label></td>
                                    <td>(<asp:Label ID="lblUsersTotal" runat="server" Text=""></asp:Label> Users)</td>
                                </tr>
                                <tr>
                                    <td><br /><strong>Total</strong></td>
                                    <td><br /><asp:Label ID="lblUsageTotal" runat="server" Text=""></asp:Label></td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                            </div>

                                                </div>

                    </div>
                        </asp:Panel>
            </div>

<div class="clear"></div>
                <br />

<div style="width: 300px;">
										<h4>Additional Features</h4>

                        <asp:Panel ID="pnlAdditionalFeatures" runat="server">
                            
                    <div class="form-holder" style="width: 278px;">

											    <div class="field-holder" style="padding-right: 0px;">


                            <div align="left">
                                <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="110px"><strong>ClockGuard</strong></td>
                                    <td width="60px"><asp:Label ID="lblClockGuardFee" runat="server" Text=""></asp:Label></td>
                                    <td>(<asp:Label ID="lblClockGuardUsers" runat="server" Text=""></asp:Label> Users)</td>
                                </tr>
                                <tr>
                                    <td><strong>Mobile Access</strong></td>
                                    <td><asp:Label ID="lblMobileAccessFee" runat="server" Text=""></asp:Label></td>
                                    <td>(<asp:Label ID="lblMobileAccessUsers" runat="server" Text=""></asp:Label> Users)</td>
                                </tr>
                                <tr>
                                    <td><br /><strong>Total</strong></td>
                                    <td><br /><asp:Label ID="lblFeaturesTotal" runat="server" Text=""></asp:Label></td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                            </div>
                                                    </div>
                        </div>
                        </asp:Panel>
             </div>
            
<div class="clear"></div>
                <br />

<div style="width: 530px;">
    <div class="form-holder" style="width: 530px;">
        <div class="field-holder">
                        <asp:Panel ID="pnlCurrentSummary" runat="server" style="width:530px;">
                            <div align="center">
                            <asp:Panel ID="pnlCredits" runat="server" style="width:530px;" Visible="false">
                                <h4>Credits</h4>
                                <asp:Repeater ID="rptCredits" runat="server">
                                    <HeaderTemplate>
                                        <table cellspacing="0px" cellpadding="0px" width="310px">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td width="230px" align="left">
                                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                            </td>
                                            <td width="80px" align="left">
                                                <asp:Label ID="lblAmount" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                                <h4>Current Monthly Total: <asp:Label ID="lblMonthlyTotal" runat="server" Text=""></asp:Label></h4>
                         
                            <br />
                            <asp:Label ID="lblBillingDateTrial" runat="server" Text="You will be billed once you activate your account" CssClass="subtitle" Visible="false"></asp:Label>
                            <asp:Label ID="lblBillingDate" runat="server" Text="You will be billed on Date" CssClass="subtitle"></asp:Label>
                            <asp:Panel ID="pnlCancelAccount" runat="server">
                                <br />
                                <asp:Button ID="btnCancelAccount" runat="server" Text="Click here to cancel your account" />
                            </asp:Panel>
                            </div>
                        </asp:Panel>
                        

                        <asp:Panel ID="pnlCancelConfirm" runat="server" CssClass="blueBox" style="width:530px;" Visible="false">
                            <h4>We're sorry to see you go</h4>
                            <br /><br />
                            Once your account has been cancelled, it will remain active until your next billing date.
                            <br />
                            After that date has passed, your account will be deactivated and you will not be billed.
                            <br /><br />
                            Enter your password and click Confirm to finalize<asp:Label ID="lblCancelIncorrectPass" runat="server" ForeColor="Red" Visible="false"><br />Incorrect password</asp:Label>
                            <br />
                            <asp:TextBox ID="txtCancelPassword" runat="server" TextMode="Password"></asp:TextBox>
                            <br />
                            <asp:Button ID="btnCancelConfirm" runat="server" Text="Confirm" />
                        </asp:Panel>


                        <asp:Panel ID="pnlAccountCancelled" runat="server" style="width:530px;" Visible="false">
                            <h4>Your account has been cancelled.</h4>
                            <br />
                            <h4>Your account will be disabled on <asp:Label ID="lblCancelDisable" runat="server"></asp:Label>.</h4> 
                            <br /><br />
                            <asp:Button ID="btnEnableAccount" runat="server" Text="Click here to re-activate your account" />
                        </asp:Panel>
                     
            <asp:Panel ID="pnlCreditCardStored" runat="server">
                <table style="width:530px;" align="center">
                    <tr>
                        <td colspan="2" align="center">
                            <strong>Your credit card information is already on file</strong>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td width="271px" align="right"><strong>Type</strong>&nbsp;&nbsp;&nbsp;</td>
                        <td width="271px" align="left"><asp:Label ID="lblCCType" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="right"><strong>Last Four Digits</strong>&nbsp;&nbsp;&nbsp;</td>
                        <td align="left"><asp:Label ID="lblCCLastFour" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="right"><strong>Expiration Date</strong>&nbsp;&nbsp;&nbsp;</td>
                        <td align="left"><asp:Label ID="lblCCExpDate" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <br />
                            <asp:Button ID="btnChangeCCInfo" runat="server" Text="Update Information" OnClientClick="showUpdateBox()" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>


            <asp:Panel ID="pnlCreditCardInfo" runat="server" CssClass="hide" DefaultButton="btnActivate">
                <table style="width:530px;" align="center">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblCCInfoPrompt" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Name as it appears on card&nbsp;&nbsp;&nbsp;</td>
                        <td align="left"><asp:TextBox ID="txtCardName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right">Card Type&nbsp;&nbsp;&nbsp;</td>
                        <td align="left">
                        <asp:DropDownList ID="drpCardType" runat="server">
                                <asp:ListItem Value="1">Visa</asp:ListItem>
                                <asp:ListItem Value="2">MasterCard</asp:ListItem>
                                <asp:ListItem Value="3">American Express</asp:ListItem>
                                <asp:ListItem Value="4">Discover</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Card Number&nbsp;&nbsp;&nbsp;</td>
                        <td align="left"><asp:TextBox ID="txtCardNumber" runat="server" MaxLength="16"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right">Expiration Date&nbsp;&nbsp;&nbsp;</td>
                        <td align="left">
                            <asp:DropDownList ID="drpExpMonth" runat="server">
                                <asp:ListItem Value="01">01 - Jan</asp:ListItem>
                                <asp:ListItem Value="02">02 - Feb</asp:ListItem>
                                <asp:ListItem Value="03">03 - Mar</asp:ListItem>
                                <asp:ListItem Value="04">04 - Apr</asp:ListItem>
                                <asp:ListItem Value="05">05 - May</asp:ListItem>
                                <asp:ListItem Value="06">06 - Jun</asp:ListItem>
                                <asp:ListItem Value="07">07 - Jul</asp:ListItem>
                                <asp:ListItem Value="08">08 - Aug</asp:ListItem>
                                <asp:ListItem Value="09">09 - Sep</asp:ListItem>
                                <asp:ListItem Value="10">10 - Oct</asp:ListItem>
                                <asp:ListItem Value="11">11 - Nov</asp:ListItem>
                                <asp:ListItem Value="12">12 - Dec</asp:ListItem>
                            </asp:DropDownList>
                            /
                            <asp:DropDownList ID="drpExpYear" runat="server">
                                <asp:ListItem Value="08">2008</asp:ListItem>
                                <asp:ListItem Value="09">2009</asp:ListItem>
                                <asp:ListItem Value="10">2010</asp:ListItem>
                                <asp:ListItem Value="11">2011</asp:ListItem>
                                <asp:ListItem Value="12">2012</asp:ListItem>
                                <asp:ListItem Value="13">2013</asp:ListItem>
                                <asp:ListItem Value="14">2014</asp:ListItem>
                                <asp:ListItem Value="15">2015</asp:ListItem>
                                <asp:ListItem Value="16">2016</asp:ListItem>
                                <asp:ListItem Value="17">2017</asp:ListItem>
                                <asp:ListItem Value="18">2018</asp:ListItem>
                                <asp:ListItem Value="19">2019</asp:ListItem>
                                <asp:ListItem Value="20">2020</asp:ListItem>
                                <asp:ListItem Value="21">2021</asp:ListItem>
                                <asp:ListItem Value="22">2022</asp:ListItem>
                                <asp:ListItem Value="23">2023</asp:ListItem>
                                <asp:ListItem Value="24">2024</asp:ListItem>
                                <asp:ListItem Value="25">2025</asp:ListItem>
                                <asp:ListItem Value="26">2026</asp:ListItem>
                                <asp:ListItem Value="27">2027</asp:ListItem>
                                <asp:ListItem Value="28">2028</asp:ListItem>
                                <asp:ListItem Value="29">2029</asp:ListItem>
                                <asp:ListItem Value="30">2030</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Card Verification Code&nbsp;&nbsp;&nbsp;</td>
                        <td align="left"><asp:TextBox ID="txtCardVerify" runat="server" Width="50px" MaxLength="4"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <br />
                            <asp:Button ID="btnActivate" runat="server" Text="Activate Account" OnClientClick="disableActivateButton()" UseSubmitBehavior="false"/>
                            <asp:Button ID="btnUpdate" runat="server" Text="Update Information" Visible="false" OnClientClick="disableUpdateButton()" UseSubmitBehavior="false"/>
                            <br />
                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    Processing...
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                </table>
            </asp:Panel>


            <asp:Panel ID="pnlCreditCardApproved" runat="server" Visible="false">
                <table class="yellowBox" style="width:542px;" align="center">
                    <tr>
                        <td align="center">
                            <strong>Your credit card has been approved<asp:Label ID="lblActivated" runat="server" Text=", and your account is now active" Visible="false"></asp:Label>.</strong>
                            <br />
                            Your next billing date is shown above.
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            </div>
</div>
    </div>

        </ContentTemplate>
    </asp:UpdatePanel>
     <br /><br />
    <div style="text-align:center;">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Terms.aspx">Terms of Service</asp:HyperLink>
        <br /><br />
        If you have any questions, please contact us via phone at (877) 646-4446 or email at <a href="mailto:info@timeclockwizard.com">info@timeclockwizard.com</a>.
    </div>
  </div>
</div>
</asp:Content>

