<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="News.aspx.vb" Inherits="News" title="TimeClockWizard - News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <span style="font-size: 16pt">Payroll and Mobile Access Updates<br />
        <span style="font-size: 9pt"><span style="font-size: 8pt">posted Monday, August 25,
            2008</span><br />
            <br />
            <span style="font-size: 10pt">
                We appreciate all of the feature requests being sent to us from our users and have done our best to incorporate these into Clockitin.  We are happy to announce the following feature requests have been added:
            </span></span></span>
    <ul>
        <li>Generate payroll for a specific office</li>
        <li>View the IP Address for a Mobile Access clock in</li>
        <li>Manually add breaks</li>
    </ul>
    <hr />
    <span style="font-size: 16pt">More New Features<br />
        <span style="font-size: 9pt"><span style="font-size: 8pt">posted Thursday, August 14,
            2008</span><br />
            <br />
            <span style="font-size: 10pt">We have added a couple more new features, again based
                on user requests:
            </span></span></span>
    <ul>
        <li>Added an option for Quick Clock In/Out to not require a password</li>
        <li>HTML tags are now allowed in Login and Dashboard messages</li>
    </ul>
    <hr />
    <span style="font-size: 16pt">First Batch of New Features<br />
        <span style="font-size: 9pt"><span style="font-size: 8pt">posted Wednesday, August 13,
            2008</span><br />
            <br />
            <span style="font-size: 10pt">We have added our first small batch of new features, based
                on user requests:
            </span></span></span>
    <ul>
        <li>Who's In page can be restricted to Managers only</li>
        <li>ClockPoint IP addresses can be added manually</li>
    </ul>
    <span style="font-size: 16pt"><span style="font-size: 9pt"><span style="font-size: 10pt">
    </span></span></span>Please continue to let us know what features you want to see
    in Clockitin!  And remember, weekly updates will begin in September.
    <hr />
    <span style="font-size: 16pt">Welcome Time Clock America subscribers!<br />
        <span style="font-size: 9pt"><span style="font-size: 8pt">posted Monday, August 11,
            2008</span><br />
            <br />
            <span style="font-size: 10pt">Everyone here at Clockitin is very excited to have our
                Time Clock America subscribers using our new and improved system.&nbsp; We are transferring
                accounts gradually over the next few weeks, and we should be done by early September.&nbsp;
                Once we have completed the conversion, we are going to start an aggressive update
                program for Clockitin.&nbsp; We will be adding new features weekly, and we'll be
                updating this News tab with information on what we have added each week.&nbsp; We
                encourage everyone to submit new feature requests in the Support tab.&nbsp; This
                will allow us to ensure that Clockitin fits the needs of our subscribers.<br />
                <br />
                -Clockitin Development Team</span></span></span>
</asp:Content>

