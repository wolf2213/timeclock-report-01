<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TimesheetRequests.aspx.vb" Inherits="TimesheetRequests" title="TimeClockWizard - Timesheet Requests" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    <div class="main-content">
        <div style="margin-left: 25px;">

        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            
                 <asp:Repeater ID="rptTimesheetRequests" OnItemDataBound="rptTimesheetRequests_ItemDataBound" OnItemCommand="rptTimesheetRequests_ItemCommand" EnableViewState="True" runat="server">
                    <HeaderTemplate>
<ul class="row-user">
<li class="column-head column-title" style="line-height: 34px;">Name</li></ul>

<ul class="type">
<li class="column-head column-title">Type</li></ul>

<ul class="date-requested">
<li class="column-head column-title">Date Requested</li></ul>

<ul class="requested-time">
<li class="column-head column-title">Requested Time</li></ul>

<ul class="duration">
<li class="column-head column-title">Duration&nbsp;<asp:Image ID="imgHelpDuration" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" /></li></ul>

<ul class="effect">
<li class="column-head column-title">Effect&nbsp;<asp:Image ID="imgHelpEffect" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" /></li></ul>

<ul class="note">
<li class="column-head column-title">Note</li></ul>

<ul class="status">
<li class="column-head column-title"><asp:Literal ID="litStatus" Visible="false" runat="server"></asp:Literal></li></ul>

<ul class="editlinks" style="width: 50px;">
 <li class="column-head column-title">&nbsp;</li> </ul>


                               
                    </HeaderTemplate>
                    <ItemTemplate>
<ul class="row-user">
<li><asp:Literal ID="litChangeType" Visible="false" runat="server"></asp:Literal>
                                <asp:Literal ID="litUserID" Visible="false" runat="server"></asp:Literal>
                                <asp:Label ID="lblName" runat="server" Text=""></asp:Label></li></ul>
                                
<ul class="type">
<li><asp:Image ID="imgType" ImageAlign="absmiddle" runat="server" />
                                <asp:Image ID="imgTRType" ImageAlign="AbsMiddle" runat="server" /></li></ul>
                                
<ul class="date-requested">
<li><asp:Label ID="lblReqDateTime" runat="server" Text=""></asp:Label></li></ul>
                                
<ul class="requested-time">
<li><asp:Label ID="lblDates" runat="server" Text=""></asp:Label></li></ul>
                                
<ul class="duration">
<li><asp:Label ID="lblDuration" runat="server" Text=""></asp:Label></li></ul>
                                
<ul class="effect">
<li><asp:Label ID="lblEffect" runat="server" Text=""></asp:Label></li></ul>
                                
<ul class="note">
<li><asp:Label ID="lblNote" runat="server" Text=""></asp:Label></li></ul>
                                
<ul class="status">
<li><asp:Image ID="imgStatus" Visible="false" runat="server" /></li></ul>
                                
<ul class="editlinks" style="width: 50px;">
<li>
                                <asp:ImageButton ID="btnDelete" CommandName="Delete" ImageUrl="~/images/delete_small.png" AlternateText="Delete this request" runat="server" />
                                <asp:ImageButton ID="btnApprove" CommandName="Approve" ImageUrl="~/images/check-green_small.png" AlternateText="Approve this request"  runat="server" />
                                <asp:ImageButton ID="btnDeny" CommandName="Deny" ImageUrl="~/images/red-x_small.png" AlternateText="Deny this request" runat="server" />
                           </li> </ul>
                    </ItemTemplate>
 
                    <FooterTemplate>
                      
                    </FooterTemplate>
                </asp:Repeater>
            </div>
    </div>
               </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

