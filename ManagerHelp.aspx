﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ManagerHelp.aspx.vb" Inherits="ManagerHelp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>

<div class="page">

    <div class="userManual" style="width:1000px;">
            
      <div style="padding-left:30px;padding-top:10px;font-family:Arial;font-size:16px;color:#1f497d;font-weight:bold;">TimeClockWizard User Manual</div>
            <br />

      <div style="padding-left:50px;">
       <ol>

          <li class="heading_bold" >1. <a href="#C1" class="heading_bold_link">Introduction</a></li>
          <li class="heading_bold" >2. <a href="#C2" class="heading_bold_link">Terms and Conditions</a></li>
          <li class="heading_bold" >3. <a href="#C4" class="heading_bold_link">Manager’s User Guide</a></li>
         
</ol>
          </div>
<br />

            <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">
      1. <a name="C1" style="text-decoration:none;">Introduction:</a>  <br /><br />
                </div>
<div style="padding-left:30px;font-family:Arial;font-size:13px;color:#000000;">
       <p> TimeClockWizard is a Time & Attendance Program that helps you to take the pain out of creating payroll and managing the tardiness, absenteeism and controlling the payroll costs. The learning curve for TimeClockWizard is very short.</p>
    </div> 
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
    <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">2. <a name="C2" style="text-decoration:none;">Terms and conditions:</a><br /><br /></div>

            <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#000000;">
   <p>To know the terms and conditions:</p>
                </div>
                <br />

                <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;">
<ol >
          <li >1. Click on “Signup Now” button in the TimeClockWizard page. </li>
     
    <li >2. Click on Terms of service on the right hand side to read the terms of service to know about the terms and conditions.  </li>
</ol>
                    </div>
        
        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>




       <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">3. <a name="C4" style="text-decoration:none;">Managers User Guide:</a> <br /><br /></div>

<div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;">

       <ol >
          <li class="heading_bold_link" >1. <a href="#C6" class="heading_bold_link"> Log in</a></li>
         <li class="heading_bold_link" >2. <a href="#C7" class="heading_bold_link">How to setup your Company in TimeClockWizard</a></li>
           </ol>
    </div>

        <div style="padding-left:75px;font-family:Arial;font-size:13px;color:#000000;">
            <ol>
          <li class="heading_bold_link" >a. <a href="#C8" class="heading_bold_link">Custom Login Screen</a></li>

        <li class="heading_bold_link" >b. <a href="#C9" class="heading_bold_link">Users/Employees</a></li>
        <li class="heading_bold_link" >c. <a href="#C10" class="heading_bold_link">Scheduling and Payroll Costs</a></li>

                </ol>
            </div>

        <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;">

            <ol>

        <li class="heading_bold_link" >3. <a href="#C11" class="heading_bold_link">Management Tools</a></li>
         
          
                
                </ol>
            </div>  
        
        <div style="padding-left:75px;font-family:Arial;font-size:13px;color:#000000;">   
            <ol>  
                
         <li class="heading_bold_link" >a <a href="#C12" class="heading_bold_link">Home </a> </li>  
         
        <li class="heading_bold_link" >b. <a href="#C13" class="heading_bold_link">Schedule</a></li>
         
         <li class="heading_bold_link" >c. <a href="#C14" class="heading_bold_link">Payroll</a></li>
         
         <li class="heading_bold_link" >d. <a href="#C15" class="heading_bold_link">Users</a></li>
         
         <li class="heading_bold_link" >e. <a href="#C16" class="heading_bold_link">Settings</a></li>
        </ol>
            </div>

                <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;">

            <ol>

         
        <li class="heading_bold_link" >4. <a href="#C17" class="heading_bold_link">Clock Guard</a></li>
         
        <li class="heading_bold_link" >5. <a href="#C18" class="heading_bold_link">Mobile Access</a></li>
</ol>

                    </div>
            
            <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
       
         
         
         <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">1. <a name="C6" style="text-decoration:none;">Log In</a> <br /><br /></div> 


         <div style="padding-left:30px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">How to Log in to TimeClockWizard:<br /><br /></div>

                    <div style="padding-left:75px;">
         <ol >
          <li >1. Open any internet browser and enter the custom login page URL (TimeClockWizard program will send you this URL information in the email when you register for a free trail).<br />
           
           Note: Please create a shortcut on all the desktops that you want your employees to ClockIn/ClockOut.</li>
           
          <li >2. Enter the username and password and click on “log in” button to log in to your account.</li>
        </ol>

                        </div> 

           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>


          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">How to Clockin/Clock Out:          <br /><br /></div>
<div style="padding-left:75px;">

          <ol >
          <li >1. Open the TimeClockWizard login page.</li>
           
           <li >2. To Clockin: Enter username, password and click on “log In” button to login Or Enter username, password and click on “Clockin” button to Clockin. This way you don’t have to log in to your account to Clockin.</li>
           
          <li >3. To Clock Out: Enter username, password and click on “Clock Out” button to Clock out or Enter username, password and click on “log in” page to login and then Click on the “Clock Out”  button on top left hand side of this page.</li>
        </ol>
    
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#000000;font-weight:bold;">How to Start Break/Stop Break:<br /><br /></div>

<div style="padding-left:75px;">
           
          <p> *Users can only use this option after they Clock in and.</p>
           
        <ol >
          <li >1. Open the TimeClockWizard login page.</li>
           
          <li >2. Enter username, password and click on “login” button to login</li>
           
            <li >3. Click on “Start Break” button to start break.</li>
           
            <li >4. Click on “End Break” button to end break.</li>
        </ol>   
    
    </div>

     <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">2.<a name="C7" style="text-decoration:none;"> How to setup Your Company in TimeClockWizard</a> <br /><br /></div> 




           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">a. <a name="C8" style="text-decoration:none;">Custom Login Screen  </a> <br /><br /></div> 


           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">Set custom Login Screen Logo <br /><br /></div> 

                    <div style="padding-left:75px;">


           <p> It is very easy to add a Logo to your Custom Login Screen.           </p>
        <ol >
          <li >1. Log in to TimeClockWizard program.</li>
           
           <li >2. Click on “Settings” tab on the top.</li>
           
          <li >3. Click on General preferences.</li>
           
          <li >4. Click on “Browse” button under Login Screen Logo.</li>
           
          <li >5. It will open a “Choose files to upload” window. Select the logo and Click “Open” to select the logo.</li>
           
         <li >6. Click on “Upload” button.</li>
        </ol>   
                        
                        </div>
                     
           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>


           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">b. <a name="C9" style="text-decoration:none;"> Users/Employees  </a> <br /><br /></div> 


           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">Add Users/Employees:           <br /><br /></div> 
<div style="padding-left:75px;">

           <ol >
          <li >1. Log in to TimeClockWizard program.</li>
           
          <li >2. Click on “Users” tab on the top.</li>
           
          <li >3. Click on “Add User” under “Users” tab.</li>
           
          <li >4. Enter the new user information in Mandatory (Required), Optional Information fields and then click on “Save Changes” button to create a new user.</li>
        </ol>   
    </div>
    
     <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To Edit My/User/Employee Info:        <br /><br /></div> 

<div style="padding-left:75px;">
           <ol >
          <li >1. Log in to TimeClockWizard program.</li>
           
           <li >2. Click on “Users” tab on the top</li>
           
         <li >3. Enter the user name in search for users box and click enter or click on the Active user list scroll down the page until you find the user. Now click on green Pencil to edit the user info/My info.</li>
           
         <li >4. "Edit User" button to save the changes.</li>
        </ol>  
    
    </div>
                      
           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>


           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">Activate a User:          <br /><br /></div> 
<div style="padding-left:75px;">

           <ol >
          <li >1. Log in to TimeClockWizard program.</li>
           
          <li >2. Click on “Users” tab on the top.</li>
           
         <li >3. Click on “Inactive Users” button.</li>
           
          <li >4. Enter the user name in search for users box and click enter to get the user record or click on the Inactive user list and scroll down the page until you find the user.</li>
           
           <li >5. Click on Blue refresh arrow button to activate user. Now you can find this user under active users list.</li>
        </ol>  
    </div>
                      
           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">Inactive a user:           <br /><br /></div>

                    <div style="padding-left:75px;">

           <ol >
          <li >1. Log in to TimeClockWizard program.</li>
           
          <li >2. Click on “Users” tab on the top.</li>
           
          <li >3. Click on “Active Users” button.</li>
           
         <li >4. Enter the user name in search for users box and click enter to get the user record or click on the Active user list scroll down the page until you find the user.</li>
           
          <li >5. Click on Red color “X” button. Now you can find this user under inactive users list. </li>
        </ol> 
                        </div>

                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>      
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">c. <a name="C10" style="text-decoration:none;">Scheduling and Payroll Costs  </a>   <br /><br /></div>         
           
         
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To View/Add/Edit Schedule:<br /><br /></div>

                    <div style="padding-left:75px;">

        <ol >
          <li >1. Log in to TimeClockWizard program.</li>
           
            <li >2. Click on “Schedule” tab.</li>
           
           <li >3. To add schedule locate the user/employee record and click on “Edit Schedule    button (“Pencil Image”).</li>
           
            <li >4. Click on “Working” button to enter working hours.</li>
           
            <li >5. After entering the work hours click on save button to save schedule or click cancel button to cancel the schedule.</li>
           
           <li >6. To edit the schedule of an user/employee, Locate the user/employee record and click on the “Pencil” image to edit.</li>
        </ol>  
                </div>        
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>


           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">Delete/Copy Schedule to another week     <br /><br /></div>
<div style="padding-left:75px;">

        <ol >
          <li >1. Log in to TimeClockWizard program.</li>
           
          <li >2. Click on “Schedule” tab.</li>
           
          <li >3. Click on “View/Edit Schedule” tab.</li>
           
          <li >4. To delete the schedule, locate the user/employees record and click on Edit schedule (Pencil Image) button.</li>
           
         <li >5. Click on the week drop down list (Located on the Top left corner of the page)Select the week.</li>
           
         <li >6. Click on “Copy this Schedule to another week” button.</li>
           
         <li >7. It will prompt you with this message “This will replace any existing schedule for the destination week” in red color text.</li>
           
          <li >8. Click on “Copy Schedule” button.</li>
</ol>  

    </div>

          <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div> 


           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">4. <a name="C11" style="text-decoration:none;">Management Tools</a> <br /><br /></div>

        <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;"> a.<a name="C12" style="text-decoration:none;">Home</a> <br /><br /></div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">1. My Dashboard        </<br /><br /></div>

                    <div style="padding-left:50px;">
          <p>
            Displays Current Clockin/out status, My alerts and My schedule for the current week.</p>
                       </div> 
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>



            <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">2. Timesheets           <br /><br /></div>

<div style="padding-left:50px;">
            Sometimes users forgot to Clock in or Clock out. Then they can use time sheets to request their manager to adjust the Clockin or Clockout hours. Mangers can use this feature to check/approve/deny user’s requests.

    </div>
           
           
      
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To add a record/change users clockin/out time:<br /><br /></div>

<div style="padding-left:75px;">
           
         <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Timesheet” tab. </li>          
         
         <li>3. Select user name from the “Manually add a Record” options list.</li>
           
         <li>4. Click on red color “X” to deny the request.</li>
           
         <li>5. Click on Green check mark to approve the time request.</li></ol>
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>


           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To display/edit timesheets:          <br /><br /></div>


                    <div style="padding-left:75px;">


         <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Timesheet” tab.</li>
           
          <li>3. Select from and to date, select sort option, user name from the “Display Timesheets” drop down list.</li>
           
          <li>4. Click on Pencil image to edit the record.</li>
           
          <li>5. After entering the work hours click on save button to save changes or click cancel button to cancel.</li></ol>

                        </div>
           
           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>

          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">3. Timesheet Requests<br /><br /></div>

                    <div style="padding-left:50px;">
           
          To check/approve/deny users Time Sheet Requests:

                        </div>

                    <div style="padding-left:75px;">
           
         <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Timesheet Requests” tab.</li>
           
         <li>3. View the available time sheet requests.</li>
           
         <li>4. Click on red color “X” to deny the request.</li>
           
          <li>5. Click on Green check mark to approve the time request.</li></ol>
                     </div>   
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
         <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">4. Who’s In:<br /><br /></div>
          <div style="padding-left:50px;">          
                    Displays the list of Clocked in users.
              </div>

              <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">5. Mileage<br /><br /></div>

                    <div style="padding-left:50px;"> 
           
          How to enter/check/verify Mileage:

                        </div>

                    <div style="padding-left:75px;"> 
           
        <ol><li>1.  Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Mileage” tab.</li>
           
         <li>3. Select the date and enter total number of miles for that day.</li>
           
          <li>4. Select a date from the Display mileage,  Select user from “for” drop down list.</li>
           
           <li>5. Click on grey color check mark to verify the record, click “X” to delete the mileage.</li></ol>
                     </div>   
                        
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">b.<a name="C13" style="text-decoration:none;"> Schedule</a><br /><br /></div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">1. View/Edit Schedule<br /><br /></div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To View/Add/Edit Schedule:<br /><br /></div>

                     <div style="padding-left:75px;">
           
           <ol ><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Schedule” tab.</li>
           
           <li>3. To add schedule locate the user/employee record and click on “Edit Schedule button (“Pencil Image”).</li>
           
           <li>4. Click on “Working” button to enter working hours.</li>
           
           <li>5. After entering the work hours click on save button to save schedule or click cancel button to cancel the schedule.</li>
           
           <li>6. To edit the schedule of an user/employee, Locate the user/employee record and click on the “Pencil” image to edit.</li></ol>
                   </div>      
                         
                         <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">Delete/Copy Schedule to another week    <br /><br /></div>

<div style="padding-left:75px;">
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Schedule” tab.</li>
           
          <li>3. Click on “View/Edit Schedule” tab.</li>
           
         <li>4. To delete the schedule, locate the user/employees record and click on Edit schedule (Pencil Image) button.</li>
           
          <li>5. Click on the week drop down list (Located on the Top left corner of the page)Select the week.</li>
           
          <li>6. Click on “Copy this Schedule to another week” button.</li>
           
         <li>7. It will prompt you with this message “This will replace any existing schedule for the destination week” in red color text.</li>
           
           <li>8. Click on “Copy Schedule” button.</li></ol>
    
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
          
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">2. Absences <br /><br /></div>

          <div style="padding-left:50px;"> 
              How to view/Add absences:
              </div>

                    <div style="padding-left:75px;">
           
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Schedule” tab.</li>
           
          <li>3. Click on “Absence” tab.</li>
           
          <li>4. Choose the user from the choose user drop down list.</li>
           
           <li>5. Add Absence for the employee.</li>
           
           <li>6. Enter type, hours, Start Date, End Date, Note and click on “Add Absence” button.</li></ol>
                    </div>    
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">3. Approve Absences          <br /><br /></div>

           <div style="padding-left:50px;"> 
               How to view/Approve absences: 
               </div>

                    <div style="padding-left:75px;">
           
          <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Schedule” tab. </li>
           
            <li>3. Click on “Approve Absences” tab. </li>
           
            <li>4. Click on green check mark to approve the absence request. </li>
           
           <li>5. Click on red color “X” to deny the request. </li></ol>
                   </div>     
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">4. Absence Settings           <br /><br /></div>


          <div style="padding-left:50px;"> 
               How to set AbsenceSettings:

              </div>

                    <div style="padding-left:75px;">
           
          <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Schedule” tab.</li>
           
           <li>3. Choose Absence type</li>
           
           <li>4. Click on Green check mark to edit total hours and used hours.</li>
           
           <li>5. Click save button to save the changes.</li></ol>
                      </div>  
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">Add/Stop Accrue Time:           <br /><br /></div>

                    <div style="padding-left:75px;">
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Schedule” tab.</li>
           
           <li>3. Click on “Enable” button, now this becomes a “Settings” button.</li>
           
           <li>4. Click on Green “Settings” button.</li>
           
          <li>5. Accurue personal hours and click save button to save the changes.</li>
           
         <li>6. To stop Accuruing click on “Stop Accuruing Vacation Time” link.</li></ol>
                   </div>     
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">c.<a name="C14" style="text-decoration:none;"> Payroll     </a>      <br /><br /></div>

          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">Manage Payroll<br /><br /></div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To Genarate Payroll<br /><br /></div>

                    <div style="padding-left:75px;">
           
          <ol> <li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Payroll” tab.</li>
           
          <li>3. Select Start date and end date and click “Generate Payroll” button.</li></ol>

                        </div>

           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">d.<a name="C15" style="text-decoration:none;"> Users  </a>      <br /><br /></div>


           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">1. Activate User<br /><br /></div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">Activate a User:          <br /><br /></div>

                    <div style="padding-left:75px;">

           <ol><li>1. Log in to TimeClockWizard program.</li> 
           
        <li>2. Click on “Users” tab on the top.</li>
           
         <li>3. Click on “Inactive Users” button.</li>
           
          <li>4. Enter the user name in search for users box and click enter to get the user record or click on the Inactive user list and scroll down the page until you find the user.</li>
           
         <li>5. Click on Blue refresh arrow button to activate user. Now you can find this user under active users list.</li></ol>
                    </div>    
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To see all the Active Users:         <br /><br /></div>
<div style="padding-left:75px;">

          <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Users” tab on the top.</li>
           
         <li>3. Click on Active users tab.</li></ol>
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To search for an Active User:          <br /><br /></div>

<div style="padding-left:75px;">
         <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Users” tab.</li>
           
         <li>3. Enter the user name in "Search for Users" box and click enter to get the user record or click on the active user list and scroll down the page until you find the user.</li></ol>
    
    </div>

    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">2. Inactive User           <br /><br /></div>


         <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;"> Inactive a user: <br /><br /></div>

<div style="padding-left:75px;">
           
          <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Users” tab on the top.</li>
           
          <li>3. Click on “Active Users” button.</li>
           
          <li>4. Enter the user name in search for users box and click enter to get the user record or click on the Active user list scroll down the page until you find the user.</li>
           
          <li>5. Click on Red color “X” button. Now you can find this user under inactive users list.</li></ol>
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To see all Inactive Users:           <br /><br /></div>
                    <div style="padding-left:75px;">

          <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Users” tab on the top.</li>
           
         <li>3. Click on Inactive users tab.</li></ol>
                     </div>   
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To search for an Inactive User:          <br /><br /></div>

                    <div style="padding-left:75px;">


         <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Users” tab.</li>
           
         <li>3. Enter the user name in “Search for Users” box and click enter to get the user record or click on the Inactive user list and scroll down the page until you find the user.</li></ol>
                    </div>    
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           
           
           
           
           
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">3. Add Users   <br /><br /></div>
        <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;"> Add Users/Employees:<br /><br /></div>

                     <div style="padding-left:75px;">
           
          <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Users” tab on the top.</li>
           
           <li>3. Click on “Add User” under “Users” tab.</li>
           
           <li>4. Enter the new user information in Mandatory (Required), Optional Information fields and then click on “Save Changes” button to create a new user.</li></ol>
                       </div>  
                         <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To Edit My Info/User Info           <br /><br /></div>
<div style="padding-left:75px;">

           <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Users” tab on the top.</li>
           
          <li>3. Enter the user name in search for users box and click enter or click on the Active user list scrolldown the page until you find the user name. Now click on green Pencil to edit the user info/My info.</li></ol>
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">4. Custom Fields            <br /><br /></div>

<div style="padding-left:50px;">
          <p> Manager can add the extra fields to store more information about the user. The custom fields can be viewed from the edit user page. If the field is selected as mandatory when it was created then it displays on the Mandatory Information box or if the field is selected as an optional then it will displays on the optional Information box.</p>
    
    </div>
    
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To Create Custom Fields:           <br /><br /></div>
<div style="padding-left:75px;">

          <ol><li>1. Click on “Custom Fields” button.</li>
           
           <li>2. Check the box, enter the field name and select the one option from the drop down list.</li>
           
           <li>3. Click “Save” button to create the custom fields.</li></ol>
    </div>
    
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">How to Delete Custom Fields:    <br /><br /></div>
<div style="padding-left:75px;">

           <ol><li>1. Click on “Custom Fields” button.</li>
           
          <li>2. Uncheck the box which you want to delete, delete the field name.</li>
           
           <li>3. Click “Save” button to delete the custom field.</li></ol>
    
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">e. <a name="C16" style="text-decoration:none;">Settings  </a>  <br /><br /></div>



          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;"> 1. General Prefernces<br /><br /></div>


           
         <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">a. Account Settings          <br /><br /></div>

<div style="padding-left:50px;">
          <p> Subdomain, Quick Clock In/Out, Self Registration, Who’s In Restriction settings can be changed under this option.</p>
    </div>
    
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">b. Log in Screen Logo          <br /><br /></div>

<div style="padding-left:50px;">

           <p>To Upload/Change Log in screen logo</p>

    </div>

                    <div style="padding-left:75px;">
           
          <ol> <li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Settings” tab on the top.</li>
           
           <li>3. Click on General preferences.</li>
           
           <li>4. Click on “Browse” button under Login Screen Logo.</li>
           
          <li>5. It will open a “Choose files to upload” window. Select the logo and Click “Open” to select the logo.</li>
           
          <li>6. Click on "Upload" button.</li></ol>
                        
                        </div>
                        
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           
           
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">c. Employee Compensation           <br /><br /></div>

<div style="padding-left:50px;">

           <p>Paid Breaks, Time Rounding, Mileage settings can be changed under this section.</p>

    </div>
           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>


           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">d. Messaging and Alerts<br /><br /></div>
<div style="padding-left:50px;">

        <p>Login Message, Dashboard Message, Birthday Notifications can be changed under this section.</p>
    
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
            <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">2. Offices    <br /><br /></div>


          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">How to Add/Edit/Delete an office:<br /><br /></div>

<div style="padding-left:75px;">
           
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Settings” tab on the top.</li>
           
           <li>3. Click on "Offices" tab.</li>
           
          <li>4. To Add: Enter the office name or location in “Add New Office” box and click on green “+” to add a new office.</li>
           
           <li>5. To Edit: Locate the office name. Click on the Pencil icon to edit the office. After editing the office click on save button to save the changes or click on cancel changes button to cancel the changes.</li>
           
           <li>6. To delete: Locate the office name. Click on the “X” icon to delete the office. Click on “Delete” button to delete or click on “Cancel” to cancel the delete.</li></ol>

    </div>

           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>



         <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">3. Clock Points           <br /><br /></div>

                    <div style="padding-left:50px;">

           <p>Clock Points help you make sure users clockin and out where they are supposed to. When a user clockin from a Clock Point location then the computer icon in the timesheet will appear in “Blue” color. If it is not from the Clock Point Location then the computer icon will appear in “Red” color. This makes it easy to catch when users are clocking in/out from unverified loactions.</p>
                        
                 </div>       
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">How to Add/Edit Clock Points:           <br /><br /></div>

<div style="padding-left:75px;">

           <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Settings” tab on the top.</li>
           
           <li>3. Click on “Clock Points”.</li>
           
           <li>4. Select the option from the Add New Clock Point list.</li>
           
          <li>5. Select the Clock point restriction option.</li>
           
           <li>6. Click “Save” button to save the changes.</li>
           
          <li>7. To Edit: Locate the office name. Click on the Pencil icon to edit the Clock Point location. After editing the location click on save button to save the changes or click on cancel changes button to cancel the changes.</li></ol>
    </div>
    
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">4. Business Information          <br /><br /></div>


          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">To Update the Business Contact or Company Information:<br /><br /></div>

<div style="padding-left:75px;">

           
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Settings” tab on the top.</li>
           
           <li>3. Click on “Business Information” tab.</li>
           
          <li>4. Enter or update the Company information or Primary contact information.</li>
           
           <li>5. Click “Update information” to save the changes.</li></ol>
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
     <!--
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">5. My Subscription           <br /><br /></div>


          <div style="padding-left:50px;"> To see Account Usage, Additional features information and current monthly total:

              </div>

                    <div style="padding-left:75px;">
           
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Settings” tab on the top.</li>
           
           <li>3. Click on “My Subscription” tab.</li></ol>
                        </div>

                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;">To Activate account/Update the Credit Card information:    <br /><br /></div>

<div style="padding-left:75px;">

           <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Settings” tab on the top.</li>
           
          <li>3. Click on “My Subscription” tab.</li>
           
          <li>4. Enter the new credit card information and click on “Activate Account” button to activate or click on “Update Information” button to update the credit card information.</li></ol>

    </div>

                     <div style="padding-left:50px;">
           
        <p>  NOTE: “Activate Button” will appears only once when you created this account. Once the account is activated then it becomes as an “Update Information” button.</p>
                         
                       </div>  
                         
                         <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;">To cancel your TimeClockWizard account:          </div>

<div style="padding-left:75px;">

         <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Settings” tab on the top.</li>
           
         <li>3. Click on “My Subscription” tab.</li>
           
         <li>4. Click on “Click here to cancel your account” button.</li></ol>
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">6. Invoice History          <br /><br /></div>


          <div style="padding-left:50px;">How to check Invoice History:<br /><br /></div>

                    <div style="padding-left:50px;">
           
          *Managers can only do this job.

                        <br /><br /></div>

                    <div style="padding-left:75px;">
           
         <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Settings” tab on the top.</li>
           
          <li>3. Click on Invoice History.</li></ol>
                        
                        </div>
                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           -->
          <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">5. <a name="C17" style="text-decoration:none;">Clock Guard: </a>       <br /><br /></div>

<div style="padding-left:50px;">
         <p> Clock Guard allows you to make sure that your employees clock in and out when they are suppossed to. Customize ClockGuard to alert you via email or text message when an employee clocks in or out early, late or every time.</p>
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
          <div style="padding-left:50px;">How to enable Clock Guard on a user:           <br /><br /></div>

 <div style="padding-left:75px;">
          <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Users” tab on the top.</li>
           
         <li>3. Click on Active users tab.</li>
           
          <li>4. Click on the “Enable” button under ClockGuard on the employee line whom you want enable the Clockguard. Now the “Enable” button becomes as “Settings” button.</li></ol>
     </div>
     
     <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           
           
          <div style="padding-left:50px;">How to disable Clock Guard on a user:          <br /><br /></div>
 <div style="padding-left:75px;">

          <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Users” tab on the top.</li>
           
          <li>3. Click on Active users tab.</li>
           
          <li>4. Click on the “Pencil Image” to edit user settings. Uncheck the ClockGuard check box on the mandatory information box or click on the “Settings” button under ClockGuard on the employee line whom you want disable the Clockguard.</li>
           
          <li>5. Click on “Disable” on the top of the page after the “ClockGuard Settings for Employee name”. Now Clock Guard is disabling on this user.</li></ol>
     </div>
     
     <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;">How to get Clock Guard alerts:           <br /><br /></div>

<div style="padding-left:75px;">
           <ol><li>1. Log in to TimeClockWizard program.</li>
           
         <li>2. Click on “Users” tab on the top.</li>
           
           <li>3. Click on Active users tab.</li>
           
           <li>4. Click on “Settings” button. Under the Alert me when option boxes check the boxes when you want to get alerts and also check how you want to receive the alerts by checking the Alert me by options.</li>
           
           <li>5. Click on “Save Settings” button to save the changes.</li></ol>
    
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;">How to cancel Clock Guard alerts:            <br /><br /></div>
<div style="padding-left:75px;">

          <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Users” tab on the top.</li>
           
          <li>3. Click on Active users tab.</li>
           
           <li>4. Click on “Settings” button. Under the Alert me when option boxes uncheck the boxes and also uncheck the Alert me by options.</li>
           
           <li>5. Click on “Save Settings” button to save the changes.</li></ol>
    </div>
    
    <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;font-family:Arial;font-size:13px;color:#1f497d;font-weight:bold;">6.<a name="C18" style="text-decoration:none;"> Mobile Access:   </a>        <br /><br /></div>

                    <div style="padding-left:50px;">

          <p> Mobile Access allows users to access TimeClockWizard from a portable device like a cell phone, smart phone, or PDA. They can clock in and out, view their schedule and recent time records, and even add mileage.</p>
                        
                        </div>

                        <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;">How to enable Mobile Access on a user:          <br /><br /></div>
 <div style="padding-left:75px;">

          <ol><li>1. Log in to TimeClockWizard program.</li>
           
           <li>2. Click on “Users” tab on the top.</li>
           
          <li>3. Click on Active users tab.</li>
           
          <li>4. Click on the “Enable” button under Mobile Access on the employee line whom you want enable the Mobile Access.</li></ol>
     </div>
     
     <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>
       </div>
           
           <div style="padding-left:50px;">How to Disable Mobile Access on a user:         <br /><br /></div>

 <div style="padding-left:75px;">
          <ol><li>1. Log in to TimeClockWizard program.</li>
           
          <li>2. Click on “Users” tab on the top.</li>
           
          <li>3. Click on Active users tab.</li>
           
           <li>4. Click on the “Disable” button under Mobile Access on the employee line whom you want disable the Mobile Access.</li></ol>

     </div>

           <div class="top_text">
         <div align="right"><a href="#" class="top_text_link">Top </a></div>


       

      </div>
    </div>
  </div> 

</div>

</asp:Content>

