<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Absences.aspx.vb" Inherits="Absences" title="TimeClockWizard - Absences" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <div class="main-content" style="width:1050px;">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="float: left; margin-left: 25px;">

                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>

                                <div style="width: 300px;">
                                <asp:Panel ID="pnlRequestAbsence" runat="server">

                                    <h4>REQUEST ABSENCE</h4>
                                             <div style="border: 1px solid #c6ced0; padding: 30px 0 0 30px;">

                                    <div style="margin-bottom: 5px;">
															<label style="margin-right: 44px;">type:</label>
																                                         
                                           
                                                <asp:DropDownList ID="drpRequestType" runat="server">
                                                    <asp:ListItem Value="1">Vacation</asp:ListItem>
                                                    <asp:ListItem Value="2">Holiday</asp:ListItem>
                                                    <asp:ListItem Value="3">Sick Day</asp:ListItem>
                                                    <asp:ListItem Value="4">Personal</asp:ListItem>
                                                    <asp:ListItem Value="5">Paid Leave</asp:ListItem>
                                                    <asp:ListItem Value="6">Other</asp:ListItem>
                                                </asp:DropDownList>
                                            
															<div class="clear"></div>
														</div>


                                           <div style="margin-bottom: 5px;">
															<label style="margin-right: 5px;">Start Date:</label>

                                                <asp:Label ID="lblDateStart" runat="server" Text="Date"></asp:Label>
                                                <asp:ImageButton ID="btnShowHideCalStart" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/images/arrowdown_small.png" OnClick="btnShowHideCalStart_Click" /><br />
                                                <div class="calendar">

                           <asp:Calendar ID="calAbsenceStart" runat="server" BackColor="White" BorderColor="#c6ced0"
                                Font-Names="Verdana" ForeColor="Black" Visible="false" OnSelectionChanged="calAbsenceStart_SelectionChanged">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="White" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#c6ced0" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True"/>
                                <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>

 
                                                </div>    
                                            </div>

                                            <div style="margin-bottom: 5px;">
															<label style="margin-right: 11px;">End Date:</label>

                                            
                                                <asp:Label ID="lblDateEnd" runat="server" Text="Date"></asp:Label>
                                                <asp:ImageButton ID="btnShowHideCalEnd" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/images/arrowdown_small.png" OnClick="btnShowHideCalEnd_Click" /><br />
                                                <div class="calendar">

                           <asp:Calendar ID="calAbsenceEnd" runat="server" BackColor="White" BorderColor="#c6ced0"
                                Font-Names="Verdana" ForeColor="Black" Visible="false" OnSelectionChanged="calAbsenceEnd_SelectionChanged">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="White" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#c6ced0" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True"/>
                                <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>


                                                </div> 
                                              </div> 


                                            <div>
                                                <label style="float: left; margin-right: 53px;">Note:</label>
                                                <asp:TextBox ID="txtNote" Width="150" Height="100" runat="server" TextMode="MultiLine"></asp:TextBox>
                                           
                                            <div class="clear"></div>
                                             </div>


                                            <div style="margin: 12px 0 20px 84px;" >

                                                <asp:Button ID="btnAddRequest" runat="server" Text="Add Absence" OnClick="btnAddRequest_Click" CssClass="add-absence-btn" />

                                                <asp:Label ID="lblRequestError" runat="server" Visible="false" Text=""></asp:Label>
                                            </div>


                                        <asp:UpdateProgress ID="prgClockInOut" runat="server">
                                            <ProgressTemplate>
                                                Processing...
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>



                                     </div> <!-- 1strow-bottom-holder -->
 
                                     
                       
                                </asp:Panel>
                                </div>


                            </ContentTemplate>
                        </asp:UpdatePanel>
                </div> <!-- end 1strow -->



 <div style="margin-left: 350px; width: 630px;">
                        <asp:Repeater ID="rptAbsences" OnItemDataBound="rptAbsences_ItemDataBound" OnItemCommand="rptAbsences_ItemCommand" EnableViewState="True" runat="server">
                            <HeaderTemplate>
                                <ul class="absence-type">
<li class="column-head column-title">ABSENCE TYPE</li></ul>

<ul class="date-start">
<li class="column-head column-title">START DATE</li></ul>

<ul class="date-end">
<li class="column-head column-title">END DATE</li></ul>

<ul class="hours-used" style="width: 115px;">
<li class="column-head column-title">HOURS USED</li></ul>

<ul class="approved">
<li class="column-head column-title">APPROVED</li></ul>

<ul class="editlinks" style="width: 50px;">
 <li class="column-head column-title">&nbsp;</li> </ul>

                            </HeaderTemplate>


                           <ItemTemplate>
<ul class="absence-type">
<li><asp:Image ID="imgNote" ImageAlign="AbsMiddle" runat="server" /><asp:Label ID="lblType" runat="server" Text=""></asp:Label></li></ul>
                                        
<ul class="date-start">
<li> <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label></li></ul>

<ul class="date-end">
<li><asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label></li></ul>
                                       
<ul class="hours-used" style="width: 115px;">
<li><asp:Label ID="lblHours" runat="server" Text=""></asp:Label></li></ul> 
                                
<ul class="approved">
<li><asp:Image ID="imgApproved" Visible="false" runat="server" /></li></ul>   
                                
<ul class="editlinks" style="width: 50px;">
<li><asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete_small.png" AlternateText="Cancel this request." /></li> </ul>                                                                                           
                                        
                                
         
                            </ItemTemplate>


                        <FooterTemplate>

                        </FooterTemplate>
                    </asp:Repeater>
</div>

           


                   
        </ContentTemplate>
    </asp:UpdatePanel>
        </div>
</asp:Content>