
Partial Class UserPersonalEdit
    Inherits System.Web.UI.Page

    Dim arrCustomStatus(5) As Integer
    '0 = Not Enabled, 1 = Optional, 2 = Mandatory

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabUsers As Panel = Master.FindControl("pnlTabUsers")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Edit As Panel = Master.FindControl("pnlTab2Edit")

        pnlMasterTabUsers.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users
        pnlMasterTab2Edit.CssClass = "tab2 tab2On"

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM CustomFields WHERE Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        objDR.Read()

        If objDR("Custom1Enabled") = True Then
            If objDR("Custom1Mandatory") = True Then
                'set mandatory
                If objDR("Custom1UserEdit") Then
                    pnlMandCust1.Visible = True
                End If
                lblMandCust1.Text = objDR("Custom1Name")
                valMandCust1.Enabled = True
                valMandCust1.ErrorMessage = objDR("Custom1Name") & " is required<br>"
                arrCustomStatus(1) = 2
            Else
                'set optional
                If objDR("Custom1UserEdit") Then
                    pnlCust1.Visible = True
                End If
                lblCust1.Text = objDR("Custom1Name")
                arrCustomStatus(1) = 1
            End If
        Else
            arrCustomStatus(1) = 0
        End If

        If objDR("Custom2Enabled") = True Then
            If objDR("Custom2Mandatory") = True Then
                'set mandatory
                If objDR("Custom2UserEdit") Then
                    pnlMandCust2.Visible = True
                End If
                lblMandCust2.Text = objDR("Custom2Name")
                valMandCust2.Enabled = True
                valMandCust2.ErrorMessage = objDR("Custom2Name") & " is required<br>"
                arrCustomStatus(2) = 2
            Else
                'set optional
                If objDR("Custom2UserEdit") Then
                    pnlCust2.Visible = True
                End If
                lblCust2.Text = objDR("Custom2Name")
                arrCustomStatus(2) = 1
            End If
        Else
            arrCustomStatus(2) = 0
        End If

        If objDR("Custom3Enabled") = True Then
            If objDR("Custom3Mandatory") = True Then
                'set mandatory
                If objDR("Custom3UserEdit") Then
                    pnlMandCust3.Visible = True
                End If
                lblMandCust3.Text = objDR("Custom3Name")
                valMandCust3.Enabled = True
                valMandCust3.ErrorMessage = objDR("Custom3Name") & " is required<br>"
                arrCustomStatus(3) = 2
            Else
                'set optional
                If objDR("Custom3UserEdit") Then
                    pnlCust3.Visible = True
                End If
                lblCust3.Text = objDR("Custom3Name")
                arrCustomStatus(3) = 1
            End If
        Else
            arrCustomStatus(3) = 0
        End If

        If objDR("Custom4Enabled") = True Then
            If objDR("Custom4Mandatory") = True Then
                'set mandatory
                If objDR("Custom4UserEdit") Then
                    pnlMandCust4.Visible = True
                End If
                lblMandCust4.Text = objDR("Custom4Name")
                valMandCust4.Enabled = True
                valMandCust4.ErrorMessage = objDR("Custom4Name") & " is required<br>"
                arrCustomStatus(4) = 2
            Else
                'set optional
                If objDR("Custom4UserEdit") Then
                    pnlCust4.Visible = True
                End If
                lblCust4.Text = objDR("Custom4Name")
                arrCustomStatus(4) = 1

            End If
        Else
            arrCustomStatus(4) = 0
        End If

        If objDR("Custom5Enabled") = True Then
            If objDR("Custom5Mandatory") = True Then
                'set mandatory
                If objDR("Custom5UserEdit") Then
                    pnlMandCust5.Visible = True
                End If
                lblMandCust5.Text = objDR("Custom5Name")
                valMandCust5.Enabled = True
                valMandCust5.ErrorMessage = objDR("Custom5Name") & " is required<br>"
                arrCustomStatus(5) = 2
            Else
                'set optional
                If objDR("Custom5UserEdit") Then
                    pnlCust5.Visible = True
                End If
                lblCust5.Text = objDR("Custom5Name")
                arrCustomStatus(5) = 1

            End If
        Else
            arrCustomStatus(5) = 0
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If Not IsPostBack Then
            loadForm()
        End If
    End Sub

    Protected Sub loadForm()
        Trace.Warn("UserPersonalEdit.aspx", "execute LoadForm()")

        'all the forms have been initialized, now set default values
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT dbo.fnGetUserName(ManagerUserId) as ManagerName, * FROM Users WHERE User_ID = @User_ID AND Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            txtUsername.Text = objDR("Username")
            txtFirstName.Text = objDR("FirstName")
            txtLastName.Text = objDR("LastName")
            txtEmail.Text = objDR("Email")
            txtAddress1.Text = objDR("HomeAddress1")
            txtAddress2.Text = objDR("HomeAddress2")
            txtCity.Text = objDR("City")
            txtState.Text = objDR("State")
            txtZip.Text = objDR("ZIPCode")
            txtHomePhone.Text = objDR("HomePhone")
            txtCellPhone.Text = objDR("CellPhone")
            txtWorkPhone.Text = objDR("WorkPhone")
            txtWorkExt.Text = objDR("WorkExt")
            txtManagerName.Text = objDR("ManagerName")

            drpTimeDisplay.SelectedValue = objDR("TimeDisplay").ToString

            Trace.Warn("UserEdit.aspx", "Mandatory = " & arrCustomStatus(1))
            If Not objDR("Custom1Value") Is DBNull.Value Then
                If arrCustomStatus(1) = 2 Then
                    txtMandCust1.Text = objDR("Custom1Value")
                    Trace.Warn("UserEdit.aspx", "Mand Set")
                Else
                    txtCust1.Text = objDR("Custom1Value")
                    Trace.Warn("UserEdit.aspx", "Optional Set")
                End If
            End If

            If Not objDR("Custom2Value") Is DBNull.Value Then
                If arrCustomStatus(2) = 2 Then
                    txtMandCust2.Text = objDR("Custom2Value")
                Else
                    txtCust2.Text = objDR("Custom2Value")
                End If
            End If

            If Not objDR("Custom3Value") Is DBNull.Value Then
                If arrCustomStatus(3) = 2 Then
                    txtMandCust3.Text = objDR("Custom3Value")
                Else
                    txtCust3.Text = objDR("Custom3Value")
                End If
            End If

            If Not objDR("Custom4Value") Is DBNull.Value Then
                If arrCustomStatus(4) = 2 Then
                    txtMandCust4.Text = objDR("Custom4Value")
                Else
                    txtCust4.Text = objDR("Custom4Value")
                End If
            End If

            If Not objDR("Custom5Value") Is DBNull.Value Then
                If arrCustomStatus(5) = 2 Then
                    txtMandCust5.Text = objDR("Custom5Value")
                Else
                    txtCust5.Text = objDR("Custom5Value")
                End If
            End If
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    '   Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '       lblLoginText.Visible = False
    '       loadForm()
    '   End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim arrSQLCustomValue(5) As String
        Trace.Warn("UserEdit.aspx", "Case = '" & arrCustomStatus(1) & "'")
        Select Case arrCustomStatus(1)
            Case 0
                arrSQLCustomValue(1) = ""
            Case 1
                arrSQLCustomValue(1) = txtCust1.Text
            Case 2
                arrSQLCustomValue(1) = txtMandCust1.Text
            Case Else
                arrSQLCustomValue(1) = ""
        End Select

        Trace.Warn("UserEdit.aspx", "Input Value = '" & arrSQLCustomValue(1) & "'")

        Select Case arrCustomStatus(2)
            Case 0
                arrSQLCustomValue(2) = ""
            Case 1
                arrSQLCustomValue(2) = txtCust2.Text
            Case 2
                arrSQLCustomValue(2) = txtMandCust2.Text
            Case Else
                arrSQLCustomValue(2) = ""
        End Select

        Select Case arrCustomStatus(3)
            Case 0
                arrSQLCustomValue(3) = ""
            Case 1
                arrSQLCustomValue(3) = txtCust3.Text
            Case 2
                arrSQLCustomValue(3) = txtMandCust3.Text
            Case Else
                arrSQLCustomValue(3) = ""
        End Select

        Select Case arrCustomStatus(4)
            Case 0
                arrSQLCustomValue(4) = ""
            Case 1
                arrSQLCustomValue(4) = txtCust4.Text
            Case 2
                arrSQLCustomValue(4) = txtMandCust4.Text
            Case Else
                arrSQLCustomValue(4) = ""
        End Select

        Select Case arrCustomStatus(5)
            Case 0
                arrSQLCustomValue(5) = ""
            Case 1
                arrSQLCustomValue(5) = txtCust5.Text
            Case 2
                arrSQLCustomValue(5) = txtMandCust5.Text
            Case Else
                arrSQLCustomValue(5) = ""
        End Select

        Dim boolAdd As Boolean = True

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        Trace.Write("UserPersonalEdit.aspx", "Username = " & txtUsername.Text)
        Trace.Write("UserPersonalEdit.aspx", "User_ID = " & Session("User_ID"))

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT User_ID FROM Users WHERE Username = @Username AND User_ID <> @User_ID AND Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@Username", txtUsername.Text)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            'Username exists
            boolAdd = False
            lblLoginText.Text = "User " & txtUsername.Text & " already exists."
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        If UserInfo.ValidateEmail(txtEmail.Text) = False Then
            boolAdd = False
            lblLoginText.Text = "Please enter a valid email address"
        End If

        If boolAdd Then
            Dim objCommandEdit As System.Data.SqlClient.SqlCommand
            Dim objConnectionEdit As System.Data.SqlClient.SqlConnection
            objConnectionEdit = New System.Data.SqlClient.SqlConnection

            objConnectionEdit.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionEdit.Open()
            objCommandEdit = New System.Data.SqlClient.SqlCommand()
            objCommandEdit.Connection = objConnectionEdit
            objCommandEdit.CommandText = "UPDATE Users SET Username = @Username, FirstName = @FirstName, LastName = @LastName, Email = @Email, HomePhone = @HomePhone, CellPhone = @CellPhone, WorkPhone = @WorkPhone, WorkExt = @WorkExt, HomeAddress1 = @HomeAddress1, HomeAddress2 = @HomeAddress2, City = @City, State = @State, ZIPCode = @ZIPCode, TimeDisplay = @TimeDisplay, Custom1Value = @Custom1Value, Custom2Value = @Custom2Value, Custom3Value = @Custom3Value, Custom4Value = @Custom4Value, Custom5Value = @Custom5Value WHERE User_ID = @User_ID AND Client_ID = @Client_ID"
            objCommandEdit.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommandEdit.Parameters.AddWithValue("@User_ID", Session("User_ID"))
            objCommandEdit.Parameters.AddWithValue("@Username", txtUsername.Text)
            objCommandEdit.Parameters.AddWithValue("@FirstName", txtFirstName.Text)
            objCommandEdit.Parameters.AddWithValue("@LastName", txtLastName.Text)
            objCommandEdit.Parameters.AddWithValue("@Email", txtEmail.Text)
            objCommandEdit.Parameters.AddWithValue("@HomePhone", txtHomePhone.Text)
            objCommandEdit.Parameters.AddWithValue("@CellPhone", txtCellPhone.Text)
            objCommandEdit.Parameters.AddWithValue("@WorkPhone", txtWorkPhone.Text)
            objCommandEdit.Parameters.AddWithValue("@WorkExt", txtWorkExt.Text)
            objCommandEdit.Parameters.AddWithValue("@HomeAddress1", txtAddress1.Text)
            objCommandEdit.Parameters.AddWithValue("@HomeAddress2", txtAddress2.Text)
            objCommandEdit.Parameters.AddWithValue("@City", txtCity.Text)
            objCommandEdit.Parameters.AddWithValue("@State", txtState.Text)
            objCommandEdit.Parameters.AddWithValue("@ZIPCode", txtZip.Text)
            objCommandEdit.Parameters.AddWithValue("@TimeDisplay", drpTimeDisplay.SelectedValue)
            objCommandEdit.Parameters.AddWithValue("@Custom1Value", arrSQLCustomValue(1))
            objCommandEdit.Parameters.AddWithValue("@Custom2Value", arrSQLCustomValue(2))
            objCommandEdit.Parameters.AddWithValue("@Custom3Value", arrSQLCustomValue(3))
            objCommandEdit.Parameters.AddWithValue("@Custom4Value", arrSQLCustomValue(4))
            objCommandEdit.Parameters.AddWithValue("@Custom5Value", arrSQLCustomValue(5))

            objCommandEdit.ExecuteNonQuery()

            lblLoginText.ForeColor = Drawing.Color.Green
            lblLoginText.Text = "<br><br>User " & txtUsername.Text & " has been edited."

            objCommandEdit = Nothing
            objConnectionEdit.Close()
            objConnectionEdit = Nothing

            loadForm()
        Else
            lblLoginText.ForeColor = Drawing.Color.Red
            lblLoginText.Text = "<br><br>" & lblLoginText.Text
        End If
    End Sub
End Class
