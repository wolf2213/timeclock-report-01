
Partial Class PayrollEmployeeDetail
    Inherits System.Web.UI.Page
    Dim intTotalMinutes As Int64 = 0
    Dim boolFirstPageBreak As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabPayroll As Panel = Master.FindControl("pnlTabPayroll")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Payroll As Panel = Master.FindControl("pnlTab2Payroll")
        Dim pnlMasterTab2ViewPayroll As Panel = Master.FindControl("pnlTab2ViewPayroll")

        pnlMasterTabPayroll.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Payroll
        pnlMasterTab2Payroll.CssClass = "tab2 tab2On"
        pnlMasterTab2ViewPayroll.CssClass = "tab2 tab2On"

        If Permissions.PayrollView(Session("User_ID")) Then
            lnkBack.NavigateUrl = "PayrollDetail.aspx?id=" & Request.QueryString("pid") & "&dates=" & Request.QueryString("dates")
        Else
            lnkBack.NavigateUrl = "Payroll.aspx"
        End If

        litPayrollTitleDates.Text = Request.QueryString("dates")

        If Not IsPostBack Then
            GetUsers()
        End If
    End Sub

    Protected Sub GetUsers()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        Dim strSQLQuery As String

        If Permissions.PayrollView(Session("User_ID")) Then
            If Request.QueryString("uid") = "" Then
                strSQLQuery = "SELECT PayrollDetail.User_ID, Users.User_ID FROM PayrollDetail, Users WHERE PayrollDetail.User_ID = Users.User_ID AND PayrollDetail.Client_ID = @Client_ID AND PayrollDetail.Payroll_ID = @Payroll_ID ORDER BY Users.LastName, Users.FirstName"
            Else
                strSQLQuery = "SELECT TOP 1 PayrollDetail.User_ID FROM PayrollDetail WHERE PayrollDetail.User_ID = @User_ID AND PayrollDetail.Client_ID = @Client_ID AND PayrollDetail.Payroll_ID = @Payroll_ID"
            End If
        Else
            strSQLQuery = "SELECT TOP 1 PayrollDetail.User_ID FROM PayrollDetail WHERE PayrollDetail.User_ID = @SessionUser_ID AND PayrollDetail.Client_ID = @Client_ID AND PayrollDetail.Payroll_ID = @Payroll_ID"
        End If

        Trace.Write("PayrollEmployeeDetail.aspx", "strSQLQuery = " & strSQLQuery)

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLQuery
        If Request.QueryString("uid") <> "" Then
            objCommand.Parameters.AddWithValue("@User_ID", Request.QueryString("uid"))
        End If
        objCommand.Parameters.AddWithValue("@SessionUser_ID", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@Payroll_ID", Request.QueryString("pid"))

        objDR = objCommand.ExecuteReader()

        rptUsers.DataSource = objDR
        rptUsers.DataBind()
    End Sub

    Protected Sub rptUsers_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptUsers.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim pnlPageBreak As Panel = e.Item.FindControl("pnlPageBreak")
            Dim pnlUserInfo As Panel = e.Item.FindControl("pnlUserInfo")
            Dim lblUserName As Label = e.Item.FindControl("lblUserName")
            Dim rptEmployeeDetail As Repeater = e.Item.FindControl("rptEmployeeDetail")

            intTotalMinutes = 0
            lblUserName.Text = UserInfo.NameFromID(e.Item.DataItem("User_ID"), 0)

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT * FROM TimeRecords WHERE User_ID = @User_ID AND Payroll_ID = @Payroll_ID ORDER BY StartTime"
            objCommand.Parameters.AddWithValue("@User_ID", e.Item.DataItem("User_ID"))
            objCommand.Parameters.AddWithValue("@Payroll_ID", Request.QueryString("pid"))
            objDR = objCommand.ExecuteReader()

            rptEmployeeDetail.DataSource = objDR
            rptEmployeeDetail.DataBind()

            If Not boolFirstPageBreak Then
                pnlPageBreak.Visible = False
                boolFirstPageBreak = True
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
    End Sub

    Protected Sub rptEmployeeDetail_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Header Then
            Dim litName As Literal = e.Item.FindControl("litName")
            Dim litPayrollDates As Literal = e.Item.FindControl("litPayrollDates")
            Dim litDate As Literal = e.Item.FindControl("litDate")

            'litName.Text = UserInfo.NameFromID(e.Item.DataItem("User_ID"), 1)
            litPayrollDates.Text = Request.QueryString("dates")
            litDate.Text = Clock.GetNow()
        End If

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblDate As Label = e.Item.FindControl("lblDate")
            Dim lblStartTime As Label = e.Item.FindControl("lblStartTime")
            Dim lblEndTime As Label = e.Item.FindControl("lblEndTime")
            Dim lblType As Label = e.Item.FindControl("lblType")
            Dim lblTotal As Label = e.Item.FindControl("lblTotal")

            lblDate.Text = CDate(e.Item.DataItem("StartTime")).Date
            lblStartTime.Text = Time.QuickStrip(e.Item.DataItem("StartTime"))

            If CDate(e.Item.DataItem("StartTime")).Date = CDate(e.Item.DataItem("EndTime")).Date Then
                lblEndTime.Text = Time.QuickStrip(e.Item.DataItem("EndTime"))
            Else
                lblEndTime.Text = Time.QuickStrip(e.Item.DataItem("EndTime")) & " (" & Time.StripTime(e.Item.DataItem("EndTime")) & ")"
            End If


            Dim strType As String = ""

            Select Case e.Item.DataItem("Type")
                Case 1
                    strType = "Normal"
                    intTotalMinutes += TimeRecords.TimeDifference(e.Item.DataItem("StartTime").ToString, e.Item.DataItem("EndTime").ToString)
                Case 2
                    strType = "Break"
                    intTotalMinutes -= TimeRecords.TimeDifference(e.Item.DataItem("StartTime").ToString, e.Item.DataItem("EndTime").ToString)
                Case 3
                    strType = "Paid Break"
                Case 4
                    intTotalMinutes += TimeRecords.TimeDifference(e.Item.DataItem("StartTime").ToString, e.Item.DataItem("EndTime").ToString)
                    Dim objDR As System.Data.SqlClient.SqlDataReader
                    Dim objCommand As System.Data.SqlClient.SqlCommand
                    Dim objConnection As System.Data.SqlClient.SqlConnection
                    objConnection = New System.Data.SqlClient.SqlConnection

                    objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                    objConnection.Open()
                    objCommand = New System.Data.SqlClient.SqlCommand()
                    objCommand.Connection = objConnection
                    objCommand.CommandText = "SELECT Type FROM Absences WHERE Absence_ID = @Absence_ID"
                    objCommand.Parameters.AddWithValue("@Absence_ID", e.Item.DataItem("Absence_ID"))
                    objDR = objCommand.ExecuteReader()

                    objDR.Read()
                    strType = AbsencesClass.AbsenceNameFromType(objDR("Type"))

                    objDR = Nothing
                    objCommand = Nothing
                    objConnection.Close()
                    objConnection = Nothing
            End Select

            lblType.Text = strType
            lblTotal.Text = TimeRecords.FormatMinutes(TimeRecords.TimeDifference(e.Item.DataItem("StartTime").ToString, e.Item.DataItem("EndTime").ToString), Session("User_ID"), True)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblTotalHours As Label = e.Item.FindControl("lblTotalHours")
            lblTotalHours.Text = TimeRecords.FormatMinutes(intTotalMinutes, Session("User_ID"), True)
        End If
    End Sub
End Class
