
Partial Class Payroll
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabPayroll As Panel = Master.FindControl("pnlTabPayroll")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Payroll As Panel = Master.FindControl("pnlTab2Payroll")
        Dim pnlMasterTab2ViewPayroll As Panel = Master.FindControl("pnlTab2ViewPayroll")

        pnlMasterTabPayroll.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Payroll
        pnlMasterTab2Payroll.CssClass = "tab2 tab2On"
        pnlMasterTab2ViewPayroll.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT DATEADD(d, 1, MAX(EndDate)) AS UseAsStart FROM PayrollSummary WHERE Client_ID = @Client_ID"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()

                Dim dtStartDate As DateTime

                If objDR("UseAsStart") Is DBNull.Value Then
                    dtStartDate = Now.Date
                Else
                    dtStartDate = objDR("UseAsStart")
                End If

                If dtStartDate > Now.Date Then
                    calPayrollStart.SelectedDate = Date.Today
                    lblDateStart.Text = calPayrollStart.SelectedDate
                Else
                    calPayrollStart.SelectedDate = dtStartDate.Date
                    lblDateStart.Text = calPayrollStart.SelectedDate
                End If
            Else
                calPayrollStart.SelectedDate = Date.Today
                lblDateStart.Text = calPayrollStart.SelectedDate
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            calPayrollEnd.SelectedDate = Date.Today
            lblDateEnd.Text = calPayrollEnd.SelectedDate

            If Not Permissions.PayrollAdd(Session("User_ID")) Then
                pnlAddPayroll.Visible = False
                pnlHelpPayroll.Visible = True
            End If

            CreatePayrollList()
            BindDropDown()
        End If
    End Sub

    Protected Sub BindDropDown()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM Offices WHERE Client_ID = @Client_ID ORDER BY [Name] ASC"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR = objCommand.ExecuteReader()

        ddlOffice.DataSource = objDR
        ddlOffice.DataTextField = "Name"
        ddlOffice.DataValueField = "Office_ID"
        ddlOffice.DataBind()

        Dim ltSelectAll As ListItem = New ListItem("All", 0)
        ddlOffice.Items.Insert(0, ltSelectAll)

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub CreatePayrollList()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        Dim strSQLQuery As String

        If Permissions.PayrollView(Session("User_ID")) Then
            strSQLQuery = "SELECT * FROM PayrollSummary WHERE Client_ID = @Client_ID ORDER BY StartDate DESC"
        Else
            strSQLQuery = "SELECT PD.Payroll_ID, PD.Wage, PD.OTWage, PD.RegularMinutes, PD.OTMinutes AS OvertimeMinutes, PD.TotalOtherPay AS OtherPayment, 1 AS Valid, U.CurrencyType "
            strSQLQuery = strSQLQuery & "FROM PayrollDetail PD INNER JOIN Users U ON PD.User_ID = U.User_ID WHERE PD.User_ID = @User_ID ORDER BY PD.Payroll_ID DESC"
        End If

        Trace.Write("Payroll.aspx", "strSQLQuery = " & strSQLQuery)

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLQuery
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objDR = objCommand.ExecuteReader()

        rptPayrollSummary.DataSource = objDR
        rptPayrollSummary.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptPayrollSummary_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Header Then
            Dim litOfficeHeader As Literal = e.Item.FindControl("litOfficeHeader")

            If Not Permissions.PayrollView(Session("User_ID")) Then
                litOfficeHeader.Visible = False
            End If
        End If

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim imgInvalid As Image = e.Item.FindControl("imgInvalid")
            Dim lblDates As Label = e.Item.FindControl("lblDates")
            Dim lblOffice As Label = e.Item.FindControl("lblOffice")
            Dim lblTotalHours As Label = e.Item.FindControl("lblTotalHours")
            Dim lblWagePayment As Label = e.Item.FindControl("lblWagePayment")
            Dim lblOtherPayment As Label = e.Item.FindControl("lblOtherPayment")
            Dim lblTotalPayment As Label = e.Item.FindControl("lblTotalPayment")
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
            Dim btnDetail As ImageButton = e.Item.FindControl("btnDetail")

            Dim strDateDisplay As String = ""
            Dim dtStartDate As DateTime
            Dim dtEndDate As DateTime

            If Permissions.PayrollView(Session("User_ID")) Then
                dtStartDate = e.Item.DataItem("StartDate")
                dtEndDate = e.Item.DataItem("EndDate")
            Else
                dtStartDate = PayrollClass.DateFromID(Session("Client_ID"), e.Item.DataItem("Payroll_ID"), "Start")
                dtEndDate = PayrollClass.DateFromID(Session("Client_ID"), e.Item.DataItem("Payroll_ID"), "End")
            End If

            dtStartDate = dtStartDate.Date
            dtEndDate = dtEndDate.Date

            If dtStartDate.Year = Now.Year And dtEndDate.Year = Now.Year Then
                strDateDisplay = MonthName(dtStartDate.Month, True) & " " & dtStartDate.Day & " - " & MonthName(dtEndDate.Month, True) & " " & dtEndDate.Day
            Else
                strDateDisplay = MonthName(dtStartDate.Month, True) & " " & dtStartDate.Day & ", " & dtStartDate.Year & " - " & MonthName(dtEndDate.Month, True) & " " & dtEndDate.Day & ", " & dtEndDate.Year
            End If

            If e.Item.DataItem("Valid") = False Then
                imgInvalid.Visible = True
                lblDates.ForeColor = Drawing.Color.Red
            End If

            lblDates.Text = strDateDisplay

            If Permissions.PayrollView(Session("User_ID")) Then
                Select Case e.Item.DataItem("Office_ID")
                    Case 0
                        lblOffice.Text = "All"
                    Case Else
                        lblOffice.Text = Offices.OfficeName(e.Item.DataItem("Office_ID"))
                End Select
            End If

            lblTotalHours.Text = TimeRecords.FormatMinutes(e.Item.DataItem("RegularMinutes") + e.Item.DataItem("OvertimeMinutes"), Session("User_ID"), False)

            If Permissions.PayrollView(Session("User_ID")) Then
                lblWagePayment.Text = Strings.FormatCurrency(e.Item.DataItem("WagePayment"), 2)
                lblOtherPayment.Text = Strings.FormatCurrency(e.Item.DataItem("OtherPayment"), 2)
                lblTotalPayment.Text = Strings.FormatCurrency((e.Item.DataItem("WagePayment") + e.Item.DataItem("OtherPayment")), 2)
            Else
                Dim dblWagePayment As Double
                Dim dblTotalPayment As Double

                dblWagePayment = (e.Item.DataItem("RegularMinutes") / 60) * e.Item.DataItem("Wage") + (e.Item.DataItem("OvertimeMinutes") / 60) * e.Item.DataItem("OTWage")
                dblTotalPayment = (((e.Item.DataItem("RegularMinutes") / 60) * e.Item.DataItem("Wage")) + (e.Item.DataItem("OvertimeMinutes") / 60) * e.Item.DataItem("OTWage")) + e.Item.DataItem("OtherPayment")

                If e.Item.DataItem("CurrencyType") = "1" Then 'Indian Rupees
                    lblWagePayment.Text = "Rs." & Strings.FormatCurrency(dblWagePayment, 2).Substring(1)
                    lblOtherPayment.Text = "Rs." & Strings.FormatCurrency(e.Item.DataItem("OtherPayment"), 2).Substring(1)
                    lblTotalPayment.Text = "Rs." & Strings.FormatCurrency(dblTotalPayment, 2).Substring(1)
                Else
                lblWagePayment.Text = Strings.FormatCurrency(dblWagePayment, 2)
                lblOtherPayment.Text = Strings.FormatCurrency(e.Item.DataItem("OtherPayment"), 2)
                lblTotalPayment.Text = Strings.FormatCurrency(dblTotalPayment, 2)
                End If
            End If

            If Permissions.PayrollDelete(Session("User_ID")) Then
                btnDelete.CommandArgument = e.Item.DataItem("Payroll_ID")
                btnDelete.CommandName = "Delete"
            Else
                btnDelete.Visible = False
            End If

            btnDetail.CommandArgument = e.Item.DataItem("Payroll_ID")
            btnDetail.CommandName = "Details"
        End If
    End Sub

    Protected Sub rptPayrollSummary_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim intPayrollID As Int64 = e.CommandArgument
        Dim lblDates As Label = e.Item.FindControl("lblDates")

        Select Case e.CommandName
            Case "Delete"
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "EXEC sprocPayrollDelete @Payroll_ID"
                objCommand.Parameters.AddWithValue("@Payroll_ID", intPayrollID)
                objCommand.ExecuteNonQuery()

                CreatePayrollList()

            Case "Details"
                Response.Redirect("PayrollDetail.aspx?id=" & intPayrollID & "&dates=" & lblDates.Text)
        End Select
    End Sub

    '   Protected Sub btnShowHideCalStart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '      If calPayrollStart.Visible Then
    '         calPayrollStart.Visible = False
    '        btnShowHideCalStart.ImageUrl = "~/images/arrowdown_small.png"
    '   Else
    '      calPayrollStart.Visible = True
    '     btnShowHideCalStart.ImageUrl = "~/images/arrowup_small.png"

    '    calPayrollEnd.Visible = False
    '   btnShowHideCalEnd.ImageUrl = "~/images/arrowdown_small.png"
    ' End If
    ' End Sub

    'Protected Sub calPayrollStart_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '   lblDateStart.Text = calPayrollStart.SelectedDate
    '  calPayrollStart.Visible = False
    ' btnShowHideCalStart.ImageUrl = "~/images/arrowdown_small.png"
    'End Sub

    'Protected Sub btnShowHideCalEnd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '   If calPayrollEnd.Visible Then
    '      calPayrollEnd.Visible = False
    '     btnShowHideCalEnd.ImageUrl = "~/images/arrowdown_small.png"
    'Else
    '   calPayrollEnd.Visible = True
    '  btnShowHideCalEnd.ImageUrl = "~/images/arrowup_small.png"

    ' calPayrollStart.Visible = False
    'btnShowHideCalStart.ImageUrl = "~/images/arrowdown_small.png"
    ' End If
    'End Sub

    'Protected Sub calPayrollEnd_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '   lblDateEnd.Text = calPayrollEnd.SelectedDate
    '  calPayrollEnd.Visible = False
    ' btnShowHideCalEnd.ImageUrl = "~/images/arrowdown_small.png"
    ' End Sub

    Protected Sub btnAddPayroll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim boolAddPayroll As Boolean = True

        lblError.ForeColor = Drawing.Color.Red

        If CDate(lblDateStart.Text) < CDate(lblDateEnd.Text) Then
            If CDate(lblDateEnd.Text) > Now.Date Then
                boolAddPayroll = False
                lblError.Visible = True
                lblError.Text = "<br>Payroll cannot contain dates in the future.<br><br>"
            End If

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection

            If ddlOffice.SelectedValue = 0 Then
                objCommand.CommandText = "SELECT User_ID FROM TimeRecords WHERE Client_ID = @Client_ID AND EndTime IS NULL AND StartTime BETWEEN @StartTime AND @EndTime"
            Else
                objCommand.CommandText = "SELECT TimeRecords.User_ID FROM TimeRecords, Users WHERE TimeRecords.User_Id = Users.User_Id AND Users.Office_ID = @Office_ID AND TimeRecords.Client_ID = @Client_ID AND EndTime IS NULL AND StartTime BETWEEN @StartTime AND @EndTime"
            End If
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@StartTime", CDate(lblDateStart.Text) & " 12:00 AM")
            objCommand.Parameters.AddWithValue("@EndTime", CDate(lblDateEnd.Text) & " 11:59 PM")
            objCommand.Parameters.AddWithValue("@Office_ID", ddlOffice.SelectedValue)
            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()
                boolAddPayroll = False
                lblError.Visible = True
                lblError.Text = "<br>" & UserInfo.NameFromID(objDR("User_ID"), 1) & " is still clocked in.  Payroll cannot be generated when someone clocked in between the payroll start date and end date and has not yet clocked out.<br><br>"
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection

            If ddlOffice.SelectedValue = 0 Then
                objCommand.CommandText = "SELECT Time_ID, User_ID FROM TimeRecords WHERE Payroll_ID <> 0 AND Client_ID = @Client_ID AND StartTime BETWEEN @StartTime AND @EndTime"
            Else
                objCommand.CommandText = "SELECT Time_ID, TimeRecords.User_ID FROM TimeRecords, Users WHERE TimeRecords.User_ID = Users.User_ID AND Users.Office_ID = @Office_ID AND Payroll_ID <> 0 AND TimeRecords.Client_ID = @Client_ID AND StartTime BETWEEN @StartTime AND @EndTime"
            End If
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@StartTime", CDate(lblDateStart.Text) & " 12:00 AM")
            objCommand.Parameters.AddWithValue("@EndTime", CDate(lblDateEnd.Text) & " 11:59 PM")
            objCommand.Parameters.AddWithValue("@Office_ID", ddlOffice.SelectedValue)
            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()
                boolAddPayroll = False
                lblError.Visible = True
                lblError.Text = "<br>A user that would be included in the payroll already has had a payroll run on their time records for this date range.  Please either change your selection, or delete the other payroll.<br><br>"
                'While objDR.HasRows
                'Trace.Write("Payroll.aspx", "Time_Id = " & objDR("Time_ID") & " User_ID = " & objDR("User_ID"))
                'End While
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection

            If ddlOffice.SelectedValue = 0 Then
                objCommand.CommandText = "SELECT COUNT(Time_ID) AS RecordCount FROM TimeRecords WHERE Client_ID = @Client_ID AND EndTime IS NOT NULL AND StartTime BETWEEN @StartTime AND @EndTime "
            Else
                objCommand.CommandText = "SELECT COUNT(TimeRecords.Time_ID) AS RecordCount FROM TimeRecords, Users WHERE TimeRecords.User_ID = Users.User_ID AND Users.Office_Id = @Office_Id AND TimeRecords.Client_ID = @Client_ID AND TimeRecords.EndTime IS NOT NULL AND TimeRecords.StartTime BETWEEN @StartTime AND @EndTime "
            End If
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@Office_ID", ddlOffice.SelectedValue)
            objCommand.Parameters.AddWithValue("@StartTime", CDate(lblDateStart.Text) & " 12:00 AM")
            objCommand.Parameters.AddWithValue("@EndTime", CDate(lblDateEnd.Text) & " 11:59 PM")
            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()
                If objDR("RecordCount") = 0 Then
                    boolAddPayroll = False
                    lblError.Visible = True
                    lblError.Text = "<br> There are no time records in this date range.<br><br>"
                End If
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            If boolAddPayroll Then
                PayrollClass.CreatePayroll(Session("Client_ID"), Session("User_ID"), lblDateStart.Text, lblDateEnd.Text, ddlOffice.SelectedValue)
                CreatePayrollList()

                lblError.Visible = True
                lblError.ForeColor = Drawing.Color.Green
                lblError.Text = "<br>Payroll successfully created.<br><br>"
            End If
        Else
            lblError.Visible = True
            lblError.Text = "<br>Start date must be before the end date.<br><br>"
        End If
    End Sub
End Class
