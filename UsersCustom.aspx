<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UsersCustom.aspx.vb" Inherits="UsersCustom" title="TimeClockWizard - Custom Fields" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="main-content">

            <div class="box1-info" style="width: 550px;">
										<h4>MANAGE CUSTOM FIELDS</h4>
										<div class="form-holder" style="width: 528px;">

<p style="font-family: arial; font-size: 14px; color: #454545; margin-bottom: 20px;"><asp:Label ID="lblMessage" Text="Check the box to activate the custom field and then enter the name of the field.<br>These custom fields can then be edited for each employee." runat="server"></asp:Label></p>
        
            <div class="field-holder">
                        <asp:CheckBox ID="chk1" OnCheckedChanged="chk1_CheckedChanged" AutoPostBack="true" runat="server"/>
                    <span class="left" style="float: none;">
													<div class="right">
                        <asp:TextBox ID="txt1" Enabled="false" runat="server"></asp:TextBox>
                    </div>
												</span>
                <span class="left" style="float: none;">
													<div class="right">
                        <asp:DropDownList ID="drp1" Enabled="false" runat="server">
                            <asp:ListItem Value="0" Text="Optional"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Mandatory"></asp:ListItem>
                        </asp:DropDownList>
                   </div>
												</span>
											</div>

                <div class="field-holder">
                        <asp:CheckBox ID="chk2" OnCheckedChanged="chk2_CheckedChanged" AutoPostBack="true" runat="server" />
                    <span class="left" style="float: none;">
													<div class="right">
                        <asp:TextBox ID="txt2" Enabled="false" runat="server"></asp:TextBox>
                    </div>
												</span>
												<span class="left" style="float: none;">
													<div class="right">
                        <asp:DropDownList ID="drp2" Enabled="false" runat="server">
                            <asp:ListItem Value="0" Text="Optional"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Mandatory"></asp:ListItem>
                        </asp:DropDownList>
                  </div>
												</span>
											</div>

                <div class="field-holder">
                        <asp:CheckBox ID="chk3" OnCheckedChanged="chk3_CheckedChanged" AutoPostBack="true" runat="server" />
                   <span class="left" style="float: none;">
													<div class="right">
                        <asp:TextBox ID="txt3" Enabled="false" runat="server"></asp:TextBox>
                   </div>
												</span>
												<span class="left" style="float: none;">
													<div class="right">
                        <asp:DropDownList ID="drp3" Enabled="false" runat="server">
                            <asp:ListItem Value="0" Text="Optional"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Mandatory"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
												</span>
											</div>


               <div class="field-holder">
                        <asp:CheckBox ID="chk4" OnCheckedChanged="chk4_CheckedChanged" AutoPostBack="true" runat="server" />
                    <span class="left" style="float: none;">
													<div class="right">
                        <asp:TextBox ID="txt4" Enabled="false" runat="server"></asp:TextBox>
                   </div>
												</span>
												<span class="left" style="float: none;">
													<div class="right">
                        <asp:DropDownList ID="drp4" Enabled="false" runat="server">
                            <asp:ListItem Value="0" Text="Optional"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Mandatory"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
												</span>
											</div>


                            <div class="field-holder">
                        <asp:CheckBox ID="chk5" OnCheckedChanged="chk5_CheckedChanged" AutoPostBack="true" runat="server" />
                    <span class="left" style="float: none;">
													<div class="right">
                        <asp:TextBox ID="txt5" Enabled="false" runat="server"></asp:TextBox>
                     </div>
												</span>
												<span class="left" style="float: none;">
													<div class="right">
                        <asp:DropDownList ID="drp5" Enabled="false" runat="server">
                            <asp:ListItem Value="0" Text="Optional"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Mandatory"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
												</span>
											</div>

                        <div><asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="save-now-btn" />
                    </div>
										</div> <!-- end of formholder -->
									</div> <!-- box1-info -->
										<div class="clear"></div>

</div>
        </ContentTemplate> 
    </asp:UpdatePanel>
</asp:Content>

