
Partial Class UsersCustom
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Permissions.UsersCustomFields(Session("User_ID")) Then
            Server.Transfer("Users.aspx")
        End If

        Dim pnlMasterTabUsers As Panel = Master.FindControl("pnlTabUsers")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2CustomFields As Panel = Master.FindControl("pnlTab2CustomFields")

        pnlMasterTabUsers.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users
        pnlMasterTab2CustomFields.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT * FROM CustomFields WHERE Client_ID = @Client_ID"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objDR = objCommand.ExecuteReader()

            objDR.Read()

            If objDR("Custom1Enabled") = True Then
                chk1.Checked = True
                txt1.Enabled = True
                txt1.Text = objDR("Custom1Name")
                drp1.Enabled = True
                If objDR("Custom1Mandatory") = True Then
                    drp1.SelectedIndex = 1
                Else
                    drp1.SelectedIndex = 0
                End If
            End If

            If objDR("Custom2Enabled") = True Then
                chk2.Checked = True
                txt2.Enabled = True
                txt2.Text = objDR("Custom2Name")
                drp2.Enabled = True
                If objDR("Custom2Mandatory") = True Then
                    drp2.SelectedIndex = 1
                Else
                    drp2.SelectedIndex = 0
                End If
            End If

            If objDR("Custom3Enabled") = True Then
                chk3.Checked = True
                txt3.Enabled = True
                txt3.Text = objDR("Custom3Name")
                drp3.Enabled = True
                If objDR("Custom3Mandatory") = True Then
                    drp3.SelectedIndex = 1
                Else
                    drp3.SelectedIndex = 0
                End If
            End If

            If objDR("Custom4Enabled") = True Then
                chk4.Checked = True
                txt4.Enabled = True
                txt4.Text = objDR("Custom4Name")
                drp4.Enabled = True
                If objDR("Custom4Mandatory") = True Then
                    drp4.SelectedIndex = 1
                Else
                    drp4.SelectedIndex = 0
                End If
            End If

            If objDR("Custom5Enabled") = True Then
                chk5.Checked = True
                txt5.Enabled = True
                txt5.Text = objDR("Custom5Name")
                drp5.Enabled = True
                If objDR("Custom5Mandatory") = True Then
                    drp5.SelectedIndex = 1
                Else
                    drp5.SelectedIndex = 0
                End If
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
    End Sub


    Protected Sub chk1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk1.CheckedChanged
        toggleEnable(sender, txt1, drp1)
    End Sub

    Protected Sub chk2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk2.CheckedChanged
        toggleEnable(sender, txt2, drp2)
    End Sub

    Protected Sub chk3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk3.CheckedChanged
        toggleEnable(sender, txt3, drp3)
    End Sub

    Protected Sub chk4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk4.CheckedChanged
        toggleEnable(sender, txt4, drp4)
    End Sub

    Protected Sub chk5_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk5.CheckedChanged
        toggleEnable(sender, txt5, drp5)
    End Sub

    Protected Sub toggleEnable(ByVal chk As CheckBox, ByVal txt As TextBox, ByVal drp As DropDownList)
        If chk.Checked = True Then
            txt.Enabled = True
            drp.Enabled = True
        Else
            txt.Enabled = False
            drp.Enabled = False
        End If
    End Sub

    Protected Function sqlCleanse(ByVal strInput As String)
        sqlCleanse = Replace(strInput, "'", "/")
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strSQL As String = ""

        If chk1.Checked = True And txt1.Text <> "" Then
            strSQL = strSQL & "Custom1Enabled = 1, Custom1Name = '" & sqlCleanse(txt1.Text) & "', Custom1Mandatory = " & drp1.SelectedIndex & ", "
        ElseIf chk1.Checked = False Then
            strSQL = strSQL & "Custom1Enabled = 0, "
        End If

        If chk2.Checked = True And txt2.Text <> "" Then
            strSQL = strSQL & "Custom2Enabled = 1, Custom2Name = '" & sqlCleanse(txt2.Text) & "', Custom2Mandatory = " & drp2.SelectedIndex & ", "
        ElseIf chk2.Checked = False Then
            strSQL = strSQL & "Custom2Enabled = 0, "
        End If

        If chk3.Checked = True And txt3.Text <> "" Then
            strSQL = strSQL & "Custom3Enabled = 1, Custom3Name = '" & sqlCleanse(txt3.Text) & "', Custom3Mandatory = " & drp3.SelectedIndex & ", "
        ElseIf chk3.Checked = False Then
            strSQL = strSQL & "Custom3Enabled = 0, "
        End If

        If chk4.Checked = True And txt4.Text <> "" Then
            strSQL = strSQL & "Custom4Enabled = 1, Custom4Name = '" & sqlCleanse(txt4.Text) & "', Custom4Mandatory = " & drp4.SelectedIndex & ", "
        ElseIf chk4.Checked = False Then
            strSQL = strSQL & "Custom4Enabled = 0, "
        End If

        If chk5.Checked = True And txt5.Text <> "" Then
            strSQL = strSQL & "Custom5Enabled = 1, Custom5Name = '" & sqlCleanse(txt5.Text) & "', Custom5Mandatory = " & drp5.SelectedIndex & ", "
        ElseIf chk5.Checked = False Then
            strSQL = strSQL & "Custom5Enabled = 0, "
        End If

        If strSQL <> "" Then
            strSQL = Trim(strSQL)
            strSQL = Left(strSQL, CInt(Len(strSQL) - 1))
            strSQL = "UPDATE CustomFields SET " & strSQL & " WHERE Client_ID = @Client_ID"
        End If

        Try
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = strSQL
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            lblMessage.Text = "Custom fields updated successfully."
        Catch ex As Exception
            lblMessage.ForeColor = Drawing.Color.DarkRed
            lblMessage.Text = "Please make sure that the field names do not exceed 20 characters."
        End Try

        'lblMessage.text = "Custom fields updated successfully."

    End Sub
End Class
