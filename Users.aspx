<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="Users.aspx.vb" Inherits="Users" Title="TimeClockWizard - Active Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="main-content">
                <div class="edit-user">
                    <strong>Edit My User Information </strong>
                </div>
                <div style="margin-left: 25px;">
                    <asp:Panel ID="pnlUsersFilter" runat="server" DefaultButton="ibSoryUsersBy">
                        Search for Users
                        <asp:TextBox ID="txtUsersFilter" runat="server" Height="22px"></asp:TextBox>
                        &nbsp;Sort Users By:
                        <asp:DropDownList ID="ddlSortUsersBy" runat="server">
                            <asp:ListItem Value="1">First Name</asp:ListItem>
                            <asp:ListItem Value="2" Selected="True">Last Name</asp:ListItem>
                            <asp:ListItem Value="3">Manager</asp:ListItem>
                            <asp:ListItem Value="4">Office</asp:ListItem>
                            <asp:ListItem Value="5">Assigned Manager</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlSortUsersOderBy" runat="server" Height="22px">
                            <asp:ListItem Value="1" Selected="True">Ascending</asp:ListItem>
                            <asp:ListItem Value="2">Decending</asp:ListItem>
                        </asp:DropDownList>
                        <asp:ImageButton ID="ibSoryUsersBy" runat="server" ImageUrl="~/images/mileage_add.png"
                            ImageAlign="AbsMiddle" OnClick="ibSoryUsersBy_Click" />
                    </asp:Panel>
                </div>
                <br />
                <asp:Panel ID="pnlClockGuardHelp" runat="server" Visible="False">
                    <table width="100%" cellspacing="10" class="helpBox">
                        <tr>
                            <td valign="top">
                                <asp:Image ID="imgLargeHelp_CG" ImageUrl="~/images/help_large.png" ImageAlign="top"
                                    runat="server" />
                            </td>
                            <td>
                                <span class="subtitle">What is ClockGuard?</span>
                                <br />
                                <span class="helpText">ClockGuard allows you to make sure that your employees clock
                                    in and out when they are suppossed to. Customize ClockGuard to alert you via email
                                    or text message when an employee clocks in or out early, late or every time.</span>
                                <span style="line-height: 5px;">
                                    <br />
                                    <br />
                                </span><span class="helpText">To enable ClockGuard for an employee, click the corresponding
                                    "Disabled" button in the ClockGuard column.</span> <span style="line-height: 5px;">
                                        <br />
                                        <br />
                                    </span><span class="helpText">Once enabled, click on the "Settings" button to configure
                                        ClockGuard for the selected user, or to disable it.</span> <span style="line-height: 5px;">
                                            <br />
                                            <br />
                                        </span><span class="helpText" style="color: #a9a9a9;">$<asp:Literal ID="litClockGuardPrice"
                                            runat="server"></asp:Literal>
                                            per month per user. Text messages may result in fees from your cellular carrier.</span>
                                <span style="line-height: 5px;">
                                    <br />
                                    <br />
                                </span>
                                <asp:LinkButton ID="btnCloseClockGuardHelp" runat="server" OnClick="btnCloseClockGuardHelp_Click">Close</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlMobileAccessHelp" runat="server" Visible="False">
                    <table width="100%" cellspacing="10" class="helpBox">
                        <tr>
                            <td valign="top">
                                <asp:Image ID="imgLargeHelp_Mobile" ImageUrl="~/images/help_large.png" ImageAlign="top"
                                    runat="server" />
                            </td>
                            <td>
                                <span class="subtitle">What is Mobile Access?</span>
                                <br />
                                <span class="helpText">Mobile Access allows users to access TimeClockWizard from a portable
                                    device like a cell phone, smart phone, or PDA. They can clock in and out, view their
                                    schedule and recent time records, and even add mileage. <span style="line-height: 5px;">
                                        <br />
                                        <br />
                                    </span>We fully support iPhone, BlackBerry, and Windows Mobile devices. However,
                                    most mobile devices with a web browser will have no trouble with Mobile Access.
                                    <span style="line-height: 5px;">
                                        <br />
                                        <br />
                                    </span>Mobile Access is available at <strong>m.<asp:Literal ID="litMASubdomain" runat="server"></asp:Literal>.timeclockwizard.com</strong>
                                    <span style="line-height: 5px;">
                                        <br />
                                        <br />
                                    </span><span class="helpText">To enable Mobile Access for a user, click the corresponding
                                        "Enable" button in the Mobile Access column.</span> <span style="line-height: 5px;">
                                            <br />
                                            <br />
                                        </span><span class="helpText" style="color: #a9a9a9;">$<asp:Literal ID="litMobileAccessPrice"
                                            runat="server"></asp:Literal>
                                            per month per user.</span> <span style="line-height: 5px;">
                                                <br />
                                                <br />
                                            </span>
                                    <asp:LinkButton ID="btnCloseMobileAccessHelp" runat="server" OnClick="btnCloseMobileAccessHelp_Click">Close</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div style="margin-left: 25px;">
                    <asp:Repeater ID="rptUsersList" runat="server" OnItemDataBound="rptUsersList_ItemDataBound"
                        OnItemCommand="rptUserList_ItemCommand" EnableViewState="True">
                        <HeaderTemplate>
                            <ul class="row-user">
                                <li class="column-head column-title">Name</li>
                            </ul>
                            <ul class="assigned-manager">
                                <li class="column-head column-title">Assigned</li>
                            </ul>
                            <ul class="offices">
                                <li class="column-head column-title">Office</li>
                            </ul>
                            <ul class="manager">
                                <li class="column-head column-title">Manager</li>
                            </ul>
                            <ul class="clockquard">
                                <li class="column-head column-title">ClockGuard&nbsp;<asp:ImageButton ID="btnHelpClockGuard"
                                    ImageUrl="~/images/help_small.png" CommandName="ClockGuardHelp" CommandArgument="0"
                                    ImageAlign="AbsMiddle" runat="server" /></li>
                            </ul>
                            <ul class="mobile">
                                <li class="column-head column-title">Mobile&nbsp;<asp:ImageButton ID="btnHelpMobileAccess"
                                    ImageUrl="~/images/help_small.png" CommandName="MobileAccessHelp" CommandArgument="0"
                                    ImageAlign="AbsMiddle" runat="server" /></li>
                            </ul>
                            <ul class="editlinks" style="width: 75px;">
                                <li class="column-head column-title">&nbsp;</li>
                            </ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <ul class="row-user">
                                <li>
                                    <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></li>
                            </ul>
                            <ul class="assigned-manager">
                                <li>
                                    <asp:Label ID="lblAssignedManager" runat="server" Text="Label"></asp:Label></li>
                            </ul>
                            <ul class="offices">
                                <li>
                                    <asp:Label ID="lblOffice" runat="server" Text="Label"></asp:Label></li>
                            </ul>
                            <ul class="manager">
                                <li>
                                    <asp:Image ID="imgManager" runat="server" ImageUrl="~/images/check-green_small.png"
                                        Visible="false" AlternateText="Manager" /></li>
                            </ul>
                            <ul class="clockquard">
                                <li>
                                    <asp:ImageButton ID="btnClockGuard" runat="server" ImageAlign="AbsMiddle" /></li>
                            </ul>
                            <ul class="mobile">
                                <li>
                                    <asp:ImageButton ID="btnMobileAccess" runat="server" ImageAlign="AbsMiddle" /></li>
                            </ul>
                            <ul class="editlinks" style="width: 75px;">
                                <li>
                                    <asp:ImageButton ID="btnPermissions" ImageUrl="~/images/permissions.png" AlternateText="Edit Permissions"
                                        runat="server" />&nbsp;&nbsp;<asp:ImageButton ID="btnEdit" ImageUrl="~/images/edit_small.png"
                                            AlternateText="Edit User" runat="server" />&nbsp;&nbsp;<asp:ImageButton ID="btnDelete"
                                                ImageUrl="~/images/delete_small.png" AlternateText="Inactivate User" runat="server" /></li>
                            </ul>
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
