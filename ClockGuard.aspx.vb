
Partial Class ClockGuard
    Inherits System.Web.UI.Page
    Dim intUserId As Int64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabUsers As Panel = Master.FindControl("pnlTabUsers")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim strSQLStatement As String = String.Empty

        pnlMasterTabUsers.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users

        intUserId = Request.QueryString("user_id")

        If Not IsPostBack Then
            LoadDropDownListsWithMinutes()
            ltrlClocksInEarlyTime.Visible = False
            ddlClocksInEarlyTime.Visible = False
            ltrlClocksInLateTime.Visible = False
            ddlClocksInLateTime.Visible = False
            ltrlClocksOutEarlyTime.Visible = False
            ddlClocksOutEarlyTime.Visible = False
            ltrlClocksOutLateTime.Visible = False
            ddlClocksOutLateTime.Visible = False

            litFullUserName.Text = UserInfo.NameFromID(intUserId, 1)
            litFirstName1.Text = UserInfo.NameFromID(intUserId, 2)
            litFirstName2.Text = litFirstName1.Text
            litFirstName3.Text = litFirstName1.Text
            litFirstName4.Text = litFirstName1.Text
            litFirstName5.Text = litFirstName1.Text
            litFirstName6.Text = litFirstName1.Text

            chkIn.Checked = ClockGuardClass.CheckAlert(intUserId, 1)
            chkInEarly.Checked = ClockGuardClass.CheckAlert(intUserId, 2)
            chkInLate.Checked = ClockGuardClass.CheckAlert(intUserId, 3)
            chkOut.Checked = ClockGuardClass.CheckAlert(intUserId, 4)
            chkOutEarly.Checked = ClockGuardClass.CheckAlert(intUserId, 5)
            chkOutLate.Checked = ClockGuardClass.CheckAlert(intUserId, 6)

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            strSQLStatement = strSQLStatement + "SELECT U.[CGEmail], U.[CGPhone], U.[CGCarrier] ,UCGS.[ClocksInEarlyStart], UCGS.[ClocksInEarlyEnd], UCGS.[ClocksInLateStart] "
            strSQLStatement = strSQLStatement + ",UCGS.[ClocksInLateEnd], UCGS.[ClocksOutEarlyStart], UCGS.[ClocksOutEarlyEnd], UCGS.[ClocksOutLateStart], UCGS.[ClocksOutLateEnd], UCGS.[SecondPreference], UCGS.[CGEmail2], UCGS.[CGPhone2], UCGS.[CGCarrier2], UCGS.[DoNotAllowClockInBeforeSchStartTime], UCGS.[DoNotAllowEmpWorkAfterSchEndTime] "
            strSQLStatement = strSQLStatement + "FROM [Users] U left join dbo.UserClockGuardSettings UCGS on U.User_ID = UCGS.User_ID WHERE U.[Client_ID] = @Client_ID AND U.[User_ID] = @User_ID AND U.[ClockGuard] = 1"

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = strSQLStatement
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@User_ID", intUserId)
            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()

                'if Check In Early checked
                If chkInEarly.Checked Then
                    ltrlClocksInEarlyTime.Visible = True
                    ddlClocksInEarlyTime.Visible = True
                    If (Not objDR("ClocksInEarlyStart") Is DBNull.Value) Then
                        ddlClocksInEarlyTime.SelectedValue = objDR("ClocksInEarlyStart")
                    End If
                End If

                'if Check In Late checked
                If chkInLate.Checked Then
                    ltrlClocksInLateTime.Visible = True
                    ddlClocksInLateTime.Visible = True
                    If (Not objDR("ClocksInLateStart") Is DBNull.Value) Then
                        ddlClocksInLateTime.SelectedValue = objDR("ClocksInLateStart")
                    End If
                End If

                'if Check Out Early checked
                If chkOutLate.Checked Then
                    ltrlClocksOutEarlyTime.Visible = True
                    ddlClocksOutEarlyTime.Visible = True
                    If (Not objDR("ClocksOutEarlyStart") Is DBNull.Value) Then
                        ddlClocksOutEarlyTime.SelectedValue = objDR("ClocksOutEarlyStart")
                    End If
                End If

                'if Check Out Late checked
                If chkOutLate.Checked Then
                    ltrlClocksOutLateTime.Visible = True
                    ddlClocksOutLateTime.Visible = True
                    If (Not objDR("ClocksOutLateStart") Is DBNull.Value) Then
                        ddlClocksOutLateTime.SelectedValue = objDR("ClocksOutLateStart")
                    End If
                End If

                'note: these if statemnets CANNOT be combined
                If (Not objDR("CGPhone") Is DBNull.Value) Then
                    If (objDR("CGPhone") <> "") Then
                        chkText.Checked = True
                        txtPhone.Visible = True
                        drpCarrier.Visible = True
                        drpCarrier.SelectedValue = objDR("CGCarrier")
                        txtPhone.Text = objDR("CGPhone")
                    End If
                End If

                'note: these if statemnets CANNOT be combined
                If Not (objDR("CGEmail") Is DBNull.Value) Then
                    If (objDR("CGEmail") <> "") Then
                        chkEmail.Checked = True
                        txtEmail.Visible = True
                        txtEmail.Text = objDR("CGEmail")
                    End If
                End If

                'note: these if statemnets CANNOT be combined
                If (Not objDR("CGPhone2") Is DBNull.Value) Then
                    If (objDR("CGPhone2") <> "") Then
                        chkText2.Checked = True
                        txtPhone2.Visible = True
                        drpCarrier2.Visible = True
                        drpCarrier2.SelectedValue = objDR("CGCarrier2")
                        txtPhone2.Text = objDR("CGPhone2")
                    End If
                End If

                'note: these if statemnets CANNOT be combined
                If Not (objDR("CGEmail2") Is DBNull.Value) Then
                    If (objDR("CGEmail2") <> "") Then
                        chkEmail2.Checked = True
                        txtEmail2.Visible = True
                        txtEmail2.Text = objDR("CGEmail2")
                    End If
                End If

                'display do not allow clock gaurd settings
                If (Not objDR("DoNotAllowClockInBeforeSchStartTime") Is DBNull.Value) Then
                    chkDoNotAllowClockInBeforeSchStartTime.Checked = objDR("DoNotAllowClockInBeforeSchStartTime")
                Else
                    chkDoNotAllowClockInBeforeSchStartTime.Checked = False
                End If
                If (Not objDR("DoNotAllowEmpWorkAfterSchEndTime") Is DBNull.Value) Then
                    chkDoNotAllowUserWorkAfterScheduleEndTime.Checked = objDR("DoNotAllowEmpWorkAfterSchEndTime")
                Else
                    chkDoNotAllowUserWorkAfterScheduleEndTime.Checked = False
                End If

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing
            Else
                UpdatePanel1.Visible = False
                litFullUserName.Text = "Invalid User_ID"
                btnDisable.Enabled = False
                btnDisable.Text = "Please try again"
            End If
        End If
    End Sub

    Protected Sub chkText_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkText.Checked = True Then
            txtPhone.Visible = True
            drpCarrier.Visible = True
        Else
            txtPhone.Visible = False
            drpCarrier.Visible = False
        End If
    End Sub

    Protected Sub chkText2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkText2.Checked = True Then
            txtPhone2.Visible = True
            drpCarrier2.Visible = True
        Else
            txtPhone2.Visible = False
            drpCarrier2.Visible = False
        End If
    End Sub

    Protected Sub chkEmail_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkEmail.Checked = True Then
            txtEmail.Visible = True
        Else
            txtEmail.Visible = False
        End If
    End Sub

    Protected Sub chkEmail2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkEmail2.Checked = True Then
            txtEmail2.Visible = True
        Else
            txtEmail2.Visible = False
        End If
    End Sub

    Protected Sub btnDisable_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim intUserId As Int64 = Request.QueryString("user_id")

        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE [Users] SET [ClockGuard] = 0 WHERE [Client_ID] = @Client_ID AND [User_ID] = @User_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@User_ID", intUserId)

        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        Response.Redirect("Users.aspx")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("Users.aspx")
    End Sub

    Protected Sub chkIn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkIn.Checked = True Then
            chkInEarly.Checked = True
            chkInEarly.Enabled = False
            chkInLate.Checked = True
            chkInLate.Enabled = False

            ltrlClocksInEarlyTime.Visible = False
            ddlClocksInEarlyTime.Visible = False
            ltrlClocksInLateTime.Visible = False
            ddlClocksInLateTime.Visible = False
        Else
            chkInEarly.Checked = False
            chkInEarly.Enabled = True
            chkInLate.Checked = False
            chkInLate.Enabled = True
        End If
    End Sub

    Protected Sub chkInEarly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkInEarly.Checked = False Then
            chkIn.Checked = False

            ltrlClocksInEarlyTime.Visible = False
            ddlClocksInEarlyTime.Visible = False
        Else
            ltrlClocksInEarlyTime.Visible = True
            ddlClocksInEarlyTime.Visible = True
        End If
    End Sub

    Protected Sub chkInLate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkInLate.Checked = False Then
            chkIn.Checked = False

            ltrlClocksInLateTime.Visible = False
            ddlClocksInLateTime.Visible = False
        Else
            ltrlClocksInLateTime.Visible = True
            ddlClocksInLateTime.Visible = True
        End If
    End Sub

    Protected Sub chkOut_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkOut.Checked = True Then
            chkOutEarly.Checked = True
            chkOutEarly.Enabled = False
            chkOutLate.Checked = True
            chkOutLate.Enabled = False

            ltrlClocksOutEarlyTime.Visible = False
            ddlClocksOutEarlyTime.Visible = False
            ltrlClocksOutLateTime.Visible = False
            ddlClocksOutLateTime.Visible = False
        Else
            chkOutEarly.Checked = False
            chkOutEarly.Enabled = True
            chkOutLate.Checked = False
            chkOutLate.Enabled = True
        End If
    End Sub

    Protected Sub chkOutEarly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkOutEarly.Checked = False Then
            chkOut.Checked = False

            ltrlClocksOutEarlyTime.Visible = False
            ddlClocksOutEarlyTime.Visible = False
        Else
            ltrlClocksOutEarlyTime.Visible = True
            ddlClocksOutEarlyTime.Visible = True
        End If
    End Sub

    Protected Sub chkOutLate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkOutLate.Checked = False Then
            chkOut.Checked = False

            ltrlClocksOutLateTime.Visible = False
            ddlClocksOutLateTime.Visible = False
        Else
            ltrlClocksOutLateTime.Visible = True
            ddlClocksOutLateTime.Visible = True
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim boolUpdate As Boolean = True

        Dim CGAlertWhen As String = ""

        If chkIn.Checked = True Then
            CGAlertWhen = "111"
        Else
            CGAlertWhen = "0"
            If chkInEarly.Checked = True Then
                CGAlertWhen = CGAlertWhen & "1"
            Else
                CGAlertWhen = CGAlertWhen & "0"
            End If
            If chkInLate.Checked = True Then
                CGAlertWhen = CGAlertWhen & "1"
            Else
                CGAlertWhen = CGAlertWhen & "0"
            End If
        End If

        If chkOut.Checked = True Then
            CGAlertWhen = CGAlertWhen & "111"
        Else
            CGAlertWhen = CGAlertWhen & "0"
            If chkOutEarly.Checked = True Then
                CGAlertWhen = CGAlertWhen & "1"
            Else
                CGAlertWhen = CGAlertWhen & "0"
            End If
            If chkOutLate.Checked = True Then
                CGAlertWhen = CGAlertWhen & "1"
            Else
                CGAlertWhen = CGAlertWhen & "0"
            End If
        End If

        If chkDoNotAllowClockInBeforeSchStartTime.Checked = False And chkDoNotAllowUserWorkAfterScheduleEndTime.Checked = False And CGAlertWhen = "000000" Then
            boolUpdate = False
            lblSubmitMessage.ForeColor = Drawing.Color.DarkRed
            lblSubmitMessage.Text = "You have no 'Alert Me When' or 'Do Not Allow' boxes checked.  If you would like to disable ClockGuard, please click the disable link at the top of the page.<br /><br />"
        End If

        If chkDoNotAllowClockInBeforeSchStartTime.Checked = False And chkDoNotAllowUserWorkAfterScheduleEndTime.Checked = False And chkText.Checked = False And chkEmail.Checked = False And chkText2.Checked = False And chkEmail2.Checked = False Then
            boolUpdate = False
            lblSubmitMessage.ForeColor = Drawing.Color.DarkRed
            lblSubmitMessage.Text = "You must choose at least one alert type.  To stop using ClockGuard for this user, click the 'Disable' link next to the title.<br /><br />"
        End If

        If chkText.Checked = True Then
            If txtPhone.Text = "" Then
                boolUpdate = False
                lblSubmitMessage.ForeColor = Drawing.Color.DarkRed
                lblSubmitMessage.Text = "If you would like to be alerted via text message, please enter a phone number in the text box.  Otherwise, uncheck the text message alert check box.<br /><br />"

                Dim rawPhone As String = ClockGuardClass.StripPhoneFormatting(txtPhone.Text)

                If Not IsNumeric(rawPhone) Or Len(rawPhone) < 10 Then
                    boolUpdate = False
                    lblSubmitMessage.ForeColor = Drawing.Color.DarkRed
                    lblSubmitMessage.Text = "Please enter a valid phone number, including area code.<br /><br />"
                End If
            End If
        End If

        If chkText2.Checked = True Then
            If txtPhone2.Text = "" Then
                boolUpdate = False
                lblSubmitMessage.ForeColor = Drawing.Color.DarkRed
                lblSubmitMessage.Text = "If you would like to be alerted via text message, please enter a phone number in the text box.  Otherwise, uncheck the text message alert check box.<br /><br />"

                Dim rawPhone As String = ClockGuardClass.StripPhoneFormatting(txtPhone2.Text)

                If Not IsNumeric(rawPhone) Or Len(rawPhone) < 10 Then
                    boolUpdate = False
                    lblSubmitMessage.ForeColor = Drawing.Color.DarkRed
                    lblSubmitMessage.Text = "Please enter a valid phone number, including area code.<br /><br />"
                End If
            End If
        End If

        If chkEmail.Checked = True And txtEmail.Text = "" Then
            boolUpdate = False
            lblSubmitMessage.ForeColor = Drawing.Color.DarkRed
            lblSubmitMessage.Text = "If you would like to be alerted via email, please enter an email address in the text box.  Otherwise, uncheck the email alert check box.<br /><br />"
        End If

        If chkEmail2.Checked = True And txtEmail2.Text = "" Then
            boolUpdate = False
            lblSubmitMessage.ForeColor = Drawing.Color.DarkRed
            lblSubmitMessage.Text = "If you would like to be alerted via email, please enter an email address in the text box.  Otherwise, uncheck the email alert check box.<br /><br />"
        End If

        Dim strSQLEmail As String
        Dim strSQLPhone As String
        Dim intSQLCarrier As Int16

        If chkEmail.Checked = False Then
            strSQLEmail = ""
        Else
            strSQLEmail = txtEmail.Text
        End If

        If chkText.Checked = False Then
            strSQLPhone = ""
            intSQLCarrier = 0
        Else
            strSQLPhone = txtPhone.Text
            intSQLCarrier = drpCarrier.SelectedValue
        End If

        If boolUpdate = True Then
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "UPDATE [Users] SET [CGAlertWhen] = @CGAlertWhen, [CGEmail] = @CGEmail, [CGPhone] = @CGPhone, [CGCarrier] = @CGCarrier WHERE [Client_ID] = @Client_ID AND [User_ID] = @User_ID"
            objCommand.Parameters.AddWithValue("@CGAlertWhen", CGAlertWhen)
            objCommand.Parameters.AddWithValue("@CGEmail", strSQLEmail)
            objCommand.Parameters.AddWithValue("@CGPhone", strSQLPhone)
            objCommand.Parameters.AddWithValue("@CGCarrier", intSQLCarrier)
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@User_ID", intUserId)

            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            SaveUserClockGuardTimers(CGAlertWhen)

            lblSubmitMessage.ForeColor = Drawing.Color.Green
            lblSubmitMessage.Text = "ClockGuard settings successfully updated.<br><br>"
        End If
    End Sub

    Private Sub SaveUserClockGuardTimers(ByVal strUserAlerts As String)
        Dim strSQLStatement As String = String.Empty
        Dim strSQLEmail2 As String
        Dim strSQLPhone2 As String
        Dim intSQLCarrier2 As Int16

        If chkEmail2.Checked = False Then
            strSQLEmail2 = ""
        Else
            strSQLEmail2 = txtEmail2.Text
        End If

        If chkText2.Checked = False Then
            strSQLPhone2 = ""
            intSQLCarrier2 = 0
        Else
            strSQLPhone2 = txtPhone2.Text
            intSQLCarrier2 = drpCarrier2.SelectedValue
        End If

        strSQLStatement = "EXEC spAddUpdateUserClockGaurdSettings @User_ID, @ClocksInEarlyStart, @ClocksInEarlyEnd, @ClocksInLateStart, @ClocksInLateEnd, @ClocksOutEarlyStart, @ClocksOutEarlyEnd, "
        strSQLStatement = strSQLStatement + "@ClocksOutLateStart, @ClocksOutLateEnd, @SecondPreference, @CGEmail2, @CGPhone2, @CGCarrier2, @DoNotAllowClockInBeforeSchStartTime, @DoNotAllowEmpWorkAfterSchEndTime"

        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = strSQLStatement
        objCommand.Parameters.AddWithValue("@User_ID", intUserId)
        objCommand.Parameters.AddWithValue("@ClocksInEarlyStart", Convert.ToInt16(ddlClocksInEarlyTime.SelectedValue.ToString()))
        objCommand.Parameters.AddWithValue("@ClocksInEarlyEnd", 0)
        objCommand.Parameters.AddWithValue("@ClocksInLateStart", Convert.ToInt16(ddlClocksInLateTime.SelectedValue.ToString()))
        objCommand.Parameters.AddWithValue("@ClocksInLateEnd", 0)
        objCommand.Parameters.AddWithValue("@ClocksOutEarlyStart", Convert.ToInt16(ddlClocksOutEarlyTime.SelectedValue.ToString()))
        objCommand.Parameters.AddWithValue("@ClocksOutEarlyEnd", 0)
        objCommand.Parameters.AddWithValue("@ClocksOutLateStart", Convert.ToInt16(ddlClocksOutLateTime.SelectedValue.ToString()))
        objCommand.Parameters.AddWithValue("@ClocksOutLateEnd", 0)
        objCommand.Parameters.AddWithValue("@SecondPreference", 0)
        objCommand.Parameters.AddWithValue("@CGEmail2", strSQLEmail2)
        objCommand.Parameters.AddWithValue("@CGPhone2", strSQLPhone2)
        objCommand.Parameters.AddWithValue("@CGCarrier2", intSQLCarrier2)
        objCommand.Parameters.AddWithValue("@DoNotAllowClockInBeforeSchStartTime", IIf(chkDoNotAllowClockInBeforeSchStartTime.Checked, 1, 0))
        objCommand.Parameters.AddWithValue("@DoNotAllowEmpWorkAfterSchEndTime", IIf(chkDoNotAllowUserWorkAfterScheduleEndTime.Checked, 1, 0))
        objCommand.ExecuteNonQuery()

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Private Sub LoadDropDownListsWithMinutes()

        Dim intIndex As Int16

        For intIndex = 5 To 120 Step 5
            ddlClocksInEarlyTime.Items.Add(New ListItem(intIndex.ToString(), intIndex.ToString()))
        Next intIndex
        For intIndex = 5 To 120 Step 5
            ddlClocksInLateTime.Items.Add(New ListItem(intIndex.ToString(), intIndex.ToString()))
        Next intIndex
        For intIndex = 5 To 120 Step 5
            ddlClocksOutEarlyTime.Items.Add(New ListItem(intIndex.ToString(), intIndex.ToString()))
        Next intIndex
        For intIndex = 5 To 120 Step 5
            ddlClocksOutLateTime.Items.Add(New ListItem(intIndex.ToString(), intIndex.ToString()))
        Next intIndex

        ddlClocksInEarlyTime.SelectedValue = "30"
        ddlClocksInLateTime.SelectedValue = "30"
        ddlClocksOutEarlyTime.SelectedValue = "30"
        ddlClocksOutLateTime.SelectedValue = "30"
    End Sub
End Class
