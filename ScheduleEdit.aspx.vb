
Partial Class ScheduleEdit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabSchedule As Panel = Master.FindControl("pnlTabSchedule")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2ViewSchedule As Panel = Master.FindControl("pnlTab2ViewSchedule")
        Dim pnlMasterTab2EditSchedule As Panel = Master.FindControl("pnlTab2EditSchedule")

        pnlMasterTabSchedule.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Schedule
        pnlMasterTab2ViewSchedule.CssClass = "tab2 tab2On"
        pnlMasterTab2EditSchedule.CssClass = "tab2 tab2On"

        timerLogout.Interval = Constants.Settings.InactivityTimeout
        timerLogout.Enabled = True

        If Permissions.ScheduleCopy(Session("User_ID")) Then
            btnCopySchedule.Visible = True
        Else
            btnCopySchedule.Visible = False
        End If

        If Not IsPostBack Then
            ViewState("CopyMode") = False
            ViewState("intWeekOfYear") = DatePart(DateInterval.WeekOfYear, Clock.GetNow())
            ViewState("intYear") = Year(Clock.GetNow())
            lblStartDate.Text = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, Clock.GetNow()), Year(Clock.GetNow()), 0)
            lblEndDate.Text = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, Clock.GetNow()), Year(Clock.GetNow()), 1)
            calScheduleDisp.SelectedDates.SelectRange(lblStartDate.Text, lblEndDate.Text)

            'to handle a week (last week of the year) overlaps between two years
            If (Year(Clock.GetNow()) > Year(calScheduleDisp.SelectedDate)) Then
                ViewState("intWeekOfYear") = DatePart(DateInterval.WeekOfYear, calScheduleDisp.SelectedDate)
                ViewState("intYear") = Year(calScheduleDisp.SelectedDate)
            End If

            If Session("Manager") Then
                '  upnlCostGuard.Visible = True
                upnlCostGuardTop.Visible = True
            End If

            GetSchedules()
        Else
            If Not ViewState("CopyMode") Then
                ViewState("intWeekOfYear") = DatePart(DateInterval.WeekOfYear, calScheduleDisp.SelectedDate)
                ViewState("intYear") = Year(calScheduleDisp.SelectedDate)
            End If
        End If

        Trace.Write("ScheduleEdit", "ViewState('intWeekOfYear') = " & ViewState("intWeekOfYear"))
        Trace.Write("ScheduleEdit", "ViewState('intYear') = " & ViewState("intYear"))
    End Sub

    Protected Sub GetSchedules()
        lblSunDate.Text = Time.StripWeekDayAndYear(Date.Parse(Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0)).ToLongDateString)
        lblMonDate.Text = Time.StripWeekDayAndYear(Date.Parse(Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1)).ToLongDateString)
        lblTueDate.Text = Time.StripWeekDayAndYear(Date.Parse(Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2)).ToLongDateString)
        lblWedDate.Text = Time.StripWeekDayAndYear(Date.Parse(Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3)).ToLongDateString)
        lblThuDate.Text = Time.StripWeekDayAndYear(Date.Parse(Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4)).ToLongDateString)
        lblFriDate.Text = Time.StripWeekDayAndYear(Date.Parse(Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5)).ToLongDateString)
        lblSatDate.Text = Time.StripWeekDayAndYear(Date.Parse(Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6)).ToLongDateString)

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
            objCommand.CommandText = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName, Wage, CurrencyType FROM [Users] WHERE (([Client_ID] = @Client_ID) AND ([Active] = 1) AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id)) ORDER BY LastName ASC"
            objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
            objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
        Else
            objCommand.CommandText = "SELECT User_ID, (LastName + ', ' + FirstName) AS FullName, Wage, CurrencyType FROM [Users] WHERE (([Client_ID] = @Client_ID) AND ([Active] = 1)) ORDER BY LastName ASC"
        End If
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        rptSchedules.DataSource = objDR
        rptSchedules.DataBind()

        objDR.Close()
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        UpdateCostGuard()
    End Sub

    Protected Sub UpdateCostGuard()
        ' lblCostGuardTotal.Text = Strings.FormatCurrency(ViewState("CostGuardPay"), 2)
        lblCostGuardTotalTop.Text = Strings.FormatCurrency(ViewState("CostGuardPay"), 2)
    End Sub

    '    Protected Sub btnCostGuardUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCostGuardUpdate.Click
    '        ViewState("CostGuardPay") = 0
    '        GetSchedules()
    '       UpdateCostGuard()
    '    End Sub

    Protected Sub weekpicker_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim caldate As Date
        ' calScheduleDisp.SelectedDate = weekpicker.Text

        caldate = weekpicker.Text
        '  caldate = calScheduleDisp.SelectedDate
        lblStartDate.Text = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, caldate), Year(caldate), 0)
        lblEndDate.Text = Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, caldate), Year(caldate), 1)
        '   calScheduleDisp.SelectedDates.SelectRange(lblStartDate.Text, lblEndDate.Text)
        '   calScheduleDisp.Visible = False
        '    btnShowHideCalDisp.ImageUrl = "~/images/arrowdown_small.png"

        '  GetSchedules()
        ' UpdatePanel1.Update()

        If Not ViewState("CopyMode") Then
            lblCopySuccess.Visible = False
            ViewState("intWeekOfYear") = DatePart(DateInterval.WeekOfYear, caldate)
            ViewState("intYear") = Year(caldate)
            ViewState("CostGuardPay") = 0
            GetSchedules()
            UpdatePanel1.Update()

        End If
    End Sub

    '   Protected Sub btnShowHideCalDisp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowHideCalDisp.Click
    '       If calScheduleDisp.Visible Then
    '           calScheduleDisp.Visible = False
    '         btnShowHideCalDisp.ImageUrl = "~/images/arrowdown_small.png"
    '    Else
    '       calScheduleDisp.Visible = True
    '      btnShowHideCalDisp.ImageUrl = "~/images/arrowup_small.png"
    '   End If
    ' End Sub

    Protected Sub btnCopySchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCopySchedule.Click
        ViewState("CopyMode") = True

        Dim dtStart As Date = lblStartDate.Text
        Dim dtEnd As Date = lblEndDate.Text

        dtStart = DateAdd(DateInterval.Day, 7, dtStart)
        dtEnd = DateAdd(DateInterval.Day, 7, dtEnd)

        lblWeek.Text = "Destination Week"
        lblStartDate.Text = dtStart
        lblEndDate.Text = dtEnd

        calScheduleDisp.SelectedDates.SelectRange(dtStart, dtEnd)
        btnCopySchedule.Visible = False
        btnScheduleCopy.Visible = True
        If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
            lblCopyWarning.Text = "This will replace any existing schedule for the destination week for below users only."
        Else
            lblCopyWarning.Text = "This will replace any existing schedule for the destination week for all the users."
        End If
        lblCopyWarning.Visible = True
        lblCopySuccess.Visible = False
    End Sub

    Protected Sub btnScheduleCopy_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnScheduleCopy.Click
        Trace.Write("ScheduleEdit", "Begin ScheduleCopy")
        Dim dtStart As Date = lblStartDate.Text
        Dim dtEnd As Date = lblEndDate.Text

        Trace.Write("ScheduleCopy", "ViewState('intWeekOfYear') = " & ViewState("intWeekOfYear"))
        Trace.Write("ScheduleCopy", "ViewState('intYear') = " & ViewState("intYear"))

        Dim intNewWeekOfYear As Integer = DatePart(DateInterval.WeekOfYear, dtStart)
        Dim intDiffWeek As Integer = intNewWeekOfYear - ViewState("intWeekOfYear")
        Dim intNewYear As Integer = DatePart(DateInterval.Year, dtStart)
        Dim intDiffYear As Integer = intNewYear - ViewState("intYear")

        Trace.Write("ScheduleCopy", "intNewWeekOfYear = " & intNewWeekOfYear)
        Trace.Write("ScheduleCopy", "intDiffWeek = " & intDiffWeek)
        Trace.Write("ScheduleCopy", "intNewYear = " & intNewYear)
        Trace.Write("ScheduleCopy", "intDiffYear = " & intDiffYear)

        Trace.Write("ScheduleCopy", "Copying Schedule")
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "EXEC sprocScheduleCopy @Client_ID, @User_ID, @Year, @WeekNum, @DiffWeek, @DiffYear"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@Year", ViewState("intYear"))
        objCommand.Parameters.AddWithValue("@WeekNum", ViewState("intWeekOfYear"))
        objCommand.Parameters.AddWithValue("@DiffWeek", intDiffWeek)
        objCommand.Parameters.AddWithValue("@DiffYear", intDiffYear)
        objCommand.ExecuteNonQuery()
        Trace.Write("ScheduleCopy", "Copy Complete")

        ViewState("intWeekOfYear") = intNewWeekOfYear
        ViewState("intYear") = intNewYear
        lblWeek.Text = "Week"
        lblCopySuccess.Visible = True
        btnCopySchedule.Visible = True
        btnScheduleCopy.Visible = False
        lblCopyWarning.Text = ""
        lblCopyWarning.Visible = False
        ViewState("CopyMode") = False

        GetSchedules()
        UpdatePanel1.Update()
        Trace.Write("ScheduleEdit", "End ScheduleCopy")
    End Sub

    Protected Sub rptSchedules_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptSchedules.ItemDataBound
        Dim bEmpWorksOvernight As Boolean

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            pnlScheduleRow.CssClass = "schTR tr" & litAlternate.Text
            If litAlternate.Text = "Normal" Then
                litAlternate.Text = "Alternate"
            Else
                litAlternate.Text = "Normal"
            End If

            If Permissions.ScheduleEdit(Session("User_ID")) Then
                btnEditSchedule.CommandArgument = e.Item.DataItem("User_ID")
            Else
                btnEditSchedule.Visible = False
            End If
            btnSaveSchedule.CommandArgument = e.Item.DataItem("User_ID")
            btnCancel.CommandArgument = e.Item.DataItem("User_ID")
            If Permissions.ScheduleDelete(Session("User_ID")) Then
                btnDeleteSchedule.CommandArgument = e.Item.DataItem("User_ID")
            Else
                btnDeleteSchedule.Visible = False
            End If

            lblName.Text = e.Item.DataItem("FullName")

            'check if employee works overnight
            bEmpWorksOvernight = bCheckEmpWorksOvernight(e.Item.DataItem("User_ID"))

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT * FROM [Schedules] WHERE (([User_ID] = @User_ID) AND ([Year] = @Year) AND ([WeekNum] = @WeekNum))"
            objCommand.Parameters.AddWithValue("@User_ID", e.Item.DataItem("User_ID"))
            objCommand.Parameters.AddWithValue("@Year", ViewState("intYear"))
            objCommand.Parameters.AddWithValue("@WeekNum", ViewState("intWeekOfYear"))
            objDR = objCommand.ExecuteReader()

            litCurrencyType.Text = e.Item.DataItem("CurrencyType")
            litWage.Text = e.Item.DataItem("Wage")
            litSunStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
            litSunEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
            litMonStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
            litMonEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
            litTueStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
            litTueEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
            litWedStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
            litWedEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
            litThuStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
            litThuEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
            litFriStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
            litFriEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
            litSatStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"
            litSatEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"
            chkSunEmployeeWorksOvernight.Visible = bEmpWorksOvernight
            chkMonEmployeeWorksOvernight.Visible = bEmpWorksOvernight
            chkTueEmployeeWorksOvernight.Visible = bEmpWorksOvernight
            chkWedEmployeeWorksOvernight.Visible = bEmpWorksOvernight
            chkThuEmployeeWorksOvernight.Visible = bEmpWorksOvernight
            chkFriEmployeeWorksOvernight.Visible = bEmpWorksOvernight
            chkSatEmployeeWorksOvernight.Visible = bEmpWorksOvernight

            chkSunEmployeeWorksOvernight.Checked = False
            chkMonEmployeeWorksOvernight.Checked = False
            chkTueEmployeeWorksOvernight.Checked = False
            chkWedEmployeeWorksOvernight.Checked = False
            chkThuEmployeeWorksOvernight.Checked = False
            chkFriEmployeeWorksOvernight.Checked = False
            chkSatEmployeeWorksOvernight.Checked = False

            If objDR.HasRows Then
                objDR.Read()

                litSunCode.Text = objDR("SunCode")
                If DatePart(DateInterval.Hour, objDR("SunStart")) <> 0 Then
                    litSunStart.Text = objDR("SunStart")
                Else
                    litSunStart.Text = objDR("SunStart") & " 12:00 AM"
                End If
                If DatePart(DateInterval.Hour, objDR("SunEnd")) <> 0 Then
                    litSunEnd.Text = objDR("SunEnd")
                Else
                    litSunEnd.Text = objDR("SunEnd") & " 12:00 AM"
                End If
                If DateDiff(DateInterval.Hour, Convert.ToDateTime(objDR("SunStart")), Convert.ToDateTime(objDR("SunEnd"))) >= 24 Then
                    chkSunEmployeeWorksOvernight.Checked = bEmpWorksOvernight
                End If

                litMonCode.Text = objDR("MonCode")
                If DatePart(DateInterval.Hour, objDR("MonStart")) <> 0 Then
                    litMonStart.Text = objDR("MonStart")
                Else
                    litMonStart.Text = objDR("MonStart") & " 12:00 AM"
                End If
                If DatePart(DateInterval.Hour, objDR("MonEnd")) <> 0 Then
                    litMonEnd.Text = objDR("MonEnd")
                Else
                    litMonEnd.Text = objDR("MonEnd") & " 12:00 AM"
                End If
                If DateDiff(DateInterval.Hour, Convert.ToDateTime(objDR("MonStart")), Convert.ToDateTime(objDR("MonEnd"))) >= 24 Then
                    chkMonEmployeeWorksOvernight.Checked = bEmpWorksOvernight
                End If

                litTueCode.Text = objDR("TueCode")
                If DatePart(DateInterval.Hour, objDR("TueStart")) <> 0 Then
                    litTueStart.Text = objDR("TueStart")
                Else
                    litTueStart.Text = objDR("TueStart") & " 12:00 AM"
                End If
                If DatePart(DateInterval.Hour, objDR("TueEnd")) <> 0 Then
                    litTueEnd.Text = objDR("TueEnd")
                Else
                    litTueEnd.Text = objDR("TueEnd") & " 12:00 AM"
                End If
                If DateDiff(DateInterval.Hour, Convert.ToDateTime(objDR("TueStart")), Convert.ToDateTime(objDR("TueEnd"))) >= 24 Then
                    chkTueEmployeeWorksOvernight.Checked = bEmpWorksOvernight
                End If

                litWedCode.Text = objDR("WedCode")
                If DatePart(DateInterval.Hour, objDR("WedStart")) <> 0 Then
                    litWedStart.Text = objDR("WedStart")
                Else
                    litWedStart.Text = objDR("WedStart") & " 12:00 AM"
                End If
                If DatePart(DateInterval.Hour, objDR("WedEnd")) <> 0 Then
                    litWedEnd.Text = objDR("WedEnd")
                Else
                    litWedEnd.Text = objDR("WedEnd") & " 12:00 AM"
                End If
                If DateDiff(DateInterval.Hour, Convert.ToDateTime(objDR("WedStart")), Convert.ToDateTime(objDR("WedEnd"))) >= 24 Then
                    chkWedEmployeeWorksOvernight.Checked = bEmpWorksOvernight
                End If

                litThuCode.Text = objDR("ThuCode")
                If DatePart(DateInterval.Hour, objDR("ThuStart")) <> 0 Then
                    litThuStart.Text = objDR("ThuStart")
                Else
                    litThuStart.Text = objDR("ThuStart") & " 12:00 AM"
                End If
                If DatePart(DateInterval.Hour, objDR("ThuEnd")) <> 0 Then
                    litThuEnd.Text = objDR("ThuEnd")
                Else
                    litThuEnd.Text = objDR("ThuEnd") & " 12:00 AM"
                End If
                If DateDiff(DateInterval.Hour, Convert.ToDateTime(objDR("ThuStart")), Convert.ToDateTime(objDR("ThuEnd"))) >= 24 Then
                    chkThuEmployeeWorksOvernight.Checked = bEmpWorksOvernight
                End If

                litFriCode.Text = objDR("FriCode")
                If DatePart(DateInterval.Hour, objDR("FriStart")) <> 0 Then
                    litFriStart.Text = objDR("FriStart")
                Else
                    litFriStart.Text = objDR("FriStart") & " 12:00 AM"
                End If
                If DatePart(DateInterval.Hour, objDR("FriEnd")) <> 0 Then
                    litFriEnd.Text = objDR("FriEnd")
                Else
                    litFriEnd.Text = objDR("FriEnd") & " 12:00 AM"
                End If
                If DateDiff(DateInterval.Hour, Convert.ToDateTime(objDR("FriStart")), Convert.ToDateTime(objDR("FriEnd"))) >= 24 Then
                    chkFriEmployeeWorksOvernight.Checked = bEmpWorksOvernight
                End If

                litSatCode.Text = objDR("SatCode")
                If DatePart(DateInterval.Hour, objDR("SatStart")) <> 0 Then
                    litSatStart.Text = objDR("SatStart")
                Else
                    litSatStart.Text = objDR("SatStart") & " 12:00 AM"
                End If
                If DatePart(DateInterval.Hour, objDR("SatEnd")) <> 0 Then
                    litSatEnd.Text = objDR("SatEnd")
                Else
                    litSatEnd.Text = objDR("SatEnd") & " 12:00 AM"
                End If
                If DateDiff(DateInterval.Hour, Convert.ToDateTime(objDR("SatStart")), Convert.ToDateTime(objDR("SatEnd"))) >= 24 Then
                    chkSatEmployeeWorksOvernight.Checked = bEmpWorksOvernight
                End If

                ScheduleDayProcess(objDR("SunCode"), litSunCodeOrig, objDR("SunStart"), objDR("SunEnd"), pnlScheduleSunChooser, lblSunStatus, pnlScheduleSun, lblSunStart, lblSunDash, lblSunEnd, lblSunOffice, txtSunStartHour, txtSunStartMin, drpSunStartAMPM, txtSunEndHour, txtSunEndMin, drpSunEndAMPM, drpSunOffice, btnSunNoOffice, lblSunNoOfficeSep, btnSunAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(objDR("MonCode"), litMonCodeOrig, objDR("MonStart"), objDR("MonEnd"), pnlScheduleMonChooser, lblMonStatus, pnlScheduleMon, lblMonStart, lblMonDash, lblMonEnd, lblMonOffice, txtMonStartHour, txtMonStartMin, drpMonStartAMPM, txtMonEndHour, txtMonEndMin, drpMonEndAMPM, drpMonOffice, btnMonNooffice, lblMonNoOfficeSep, btnMonAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(objDR("TueCode"), litTueCodeOrig, objDR("TueStart"), objDR("TueEnd"), pnlScheduleTueChooser, lblTueStatus, pnlScheduleTue, lblTueStart, lblTueDash, lblTueEnd, lblTueOffice, txtTueStartHour, txtTueStartMin, drpTueStartAMPM, txtTueEndHour, txtTueEndMin, drpTueEndAMPM, drpTueOffice, btnTueNoOffice, lblTueNoOfficeSep, btnTueAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(objDR("WedCode"), litWedCodeOrig, objDR("WedStart"), objDR("WedEnd"), pnlScheduleWedChooser, lblWedStatus, pnlScheduleWed, lblWedStart, lblWedDash, lblWedEnd, lblWedOffice, txtWedStartHour, txtWedStartMin, drpWedStartAMPM, txtWedEndHour, txtWedEndMin, drpWedEndAMPM, drpWedOffice, btnWedNoOffice, lblWedNoOfficeSep, btnWedAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(objDR("ThuCode"), litThuCodeOrig, objDR("ThuStart"), objDR("ThuEnd"), pnlScheduleThuChooser, lblThuStatus, pnlScheduleThu, lblThuStart, lblThuDash, lblThuEnd, lblThuOffice, txtThuStartHour, txtThuStartMin, drpThuStartAMPM, txtThuEndHour, txtThuEndMin, drpThuEndAMPM, drpThuOffice, btnThuNoOffice, lblThuNoOfficeSep, btnThuAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(objDR("FriCode"), litFriCodeOrig, objDR("FriStart"), objDR("FriEnd"), pnlScheduleFriChooser, lblFriStatus, pnlScheduleFri, lblFriStart, lblFriDash, lblFriEnd, lblFriOffice, txtFriStartHour, txtFriStartMin, drpFriStartAMPM, txtFriEndHour, txtFriEndMin, drpFriEndAMPM, drpFriOffice, btnFriNoOffice, lblFriNoOfficeSep, btnFriAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(objDR("SatCode"), litSatCodeOrig, objDR("SatStart"), objDR("SatEnd"), pnlScheduleSatChooser, lblSatStatus, pnlScheduleSat, lblSatStart, lblSatDash, lblSatEnd, lblSatOffice, txtSatStartHour, txtSatStartMin, drpSatStartAMPM, txtSatEndHour, txtSatEndMin, drpSatEndAMPM, drpSatOffice, btnSatNoOffice, lblSatNoOfficeSep, btnSatAssignOffice, btnDeleteSchedule)

                ScheduleTotalsProcess(e.Item.DataItem("User_ID"), litCurrencyType.Text, litWage.Text, lblWeekTotalHours, lblWeekTotalPay)

            Else
                pnlScheduleDays.Visible = False
                pnlScheduleNone.Visible = True
                lblWeekTotalHours.Text = "Not Scheduled"
                lblWeekTotalPay.Visible = False
            End If
        End If
    End Sub

    Protected Sub ScheduleTotalsProcess(ByVal intUser_ID As Int64, ByVal intCurrencyType As Integer, ByVal intWage As Integer, ByVal lblTotalHours As Label, ByVal lblTotalPay As Label)
        Dim objDR2 As System.Data.SqlClient.SqlDataReader
        Dim objCommand2 As System.Data.SqlClient.SqlCommand
        Dim objConnection2 As System.Data.SqlClient.SqlConnection
        objConnection2 = New System.Data.SqlClient.SqlConnection

        objConnection2.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection2.Open()
        objCommand2 = New System.Data.SqlClient.SqlCommand()
        objCommand2.Connection = objConnection2
        objCommand2.CommandText = "EXEC sprocScheduleWeekUserTotals @User_ID, @Year, @WeekNum"
        objCommand2.Parameters.AddWithValue("@User_ID", intUser_ID)
        objCommand2.Parameters.AddWithValue("@Year", ViewState("intYear"))
        objCommand2.Parameters.AddWithValue("@WeekNum", ViewState("intWeekOfYear"))
        objDR2 = objCommand2.ExecuteReader()

        objDR2.Read()
        Dim intTotalMinutes As Integer = objDR2("TotalMinutes")
        Dim intTotalWage As Double = 0.0
        '1 is Indian Currency
        If intCurrencyType = 1 Then
            'convert rupees into dollars
            intTotalWage = (intTotalMinutes / 60) * (intWage / Convert.ToDouble(ConfigurationManager.AppSettings("DollarRupeeConvExchange").ToString()))
        Else
            intTotalWage = (intTotalMinutes / 60) * intWage
        End If
        ViewState("CostGuardPay") += intTotalWage

        lblTotalHours.Text = TimeRecords.FormatMinutes(intTotalMinutes, Session("User_ID"), False) & " hours"
        If Session("Manager") Or (Session("User_ID") = intUser_ID) Then
            lblTotalHours.Text &= ","
            lblTotalPay.Text = "$" & intTotalWage
        End If

        objDR2 = Nothing
        objCommand2 = Nothing
        objConnection2.Close()
        objConnection2 = Nothing
    End Sub

    Protected Sub ScheduleDayProcess(ByVal strDayCode As String, ByVal litDayCodeOrig As Literal, ByVal dtStart As DateTime, ByVal dtEnd As DateTime, ByVal pnlScheduleChooser As Panel, ByVal lblStatus As Label, ByVal pnlSchedule As Panel, ByVal lblStart As Label, ByVal lblDash As Label, ByVal lblEnd As Label, ByVal lblOffice As Label, ByVal txtStartHour As TextBox, ByVal txtStartMin As TextBox, ByVal drpStartAMPM As DropDownList, ByVal txtEndHour As TextBox, ByVal txtEndMin As TextBox, ByVal drpEndAMPM As DropDownList, ByVal drpOffice As DropDownList, ByVal btnNoOffice As LinkButton, ByVal lblNoOfficeSep As Label, ByVal btnAssignOffice As LinkButton, ByVal btnDeleteSchedule As ImageButton)
        If IsNumeric(strDayCode) Then
            pnlScheduleChooser.Visible = False
            pnlSchedule.Visible = True
            litDayCodeOrig.Text = strDayCode
            lblStart.Text = Time.QuickStrip(dtStart)
            txtStartMin.Text = Time.AddZero(DatePart(DateInterval.Minute, dtStart))
            txtStartMin.BorderColor = Drawing.Color.Empty
            If Hour(dtStart) < 13 Then
                txtStartHour.Text = Hour(dtStart)
                drpStartAMPM.SelectedValue = "AM"
            Else
                txtStartHour.Text = Hour(dtStart) - 12
                drpStartAMPM.SelectedValue = "PM"
            End If
            If txtStartHour.Text = "0" Then
                txtStartHour.Text = "12"
            End If
            txtStartHour.BorderColor = Drawing.Color.Empty
            lblEnd.Text = Time.QuickStrip(dtEnd)
            If (dtStart.Date < dtEnd.Date) Then
                lblEnd.Text = Time.QuickStrip(dtEnd) & " (" & dtEnd.ToString("MM/dd/yyyy") & ")"
            End If
            txtEndMin.Text = Time.AddZero(DatePart(DateInterval.Minute, dtEnd))
            txtEndMin.BorderColor = Drawing.Color.Empty
            If Hour(dtEnd) < 13 Then
                txtEndHour.Text = Hour(dtEnd)
                drpEndAMPM.SelectedValue = "AM"
            Else
                txtEndHour.Text = Hour(dtEnd) - 12
                drpEndAMPM.SelectedValue = "PM"
            End If
            If txtEndHour.Text = "0" Then
                txtEndHour.Text = "12"
            End If
            txtEndHour.BorderColor = Drawing.Color.Empty
            If strDayCode <> "0" Then
                lblOffice.Text = Offices.OfficeName(strDayCode)
                drpOffice.Visible = True
                btnNoOffice.Visible = True
                lblNoOfficeSep.Visible = True
                btnAssignOffice.Visible = False
            Else
                lblOffice.Visible = False
                drpOffice.Visible = False
                btnNoOffice.Visible = False
                lblNoOfficeSep.Visible = False
                btnAssignOffice.Visible = True
            End If
            lblDash.Visible = True
        ElseIf strDayCode <> "NW" Then
            pnlScheduleChooser.Visible = True
            lblStatus.Text = AbsencesClass.AbsenceNameFromCode(strDayCode)
            lblStatus.ForeColor = Drawing.Color.DarkRed
            pnlSchedule.Visible = False
            litDayCodeOrig.Text = strDayCode
            lblStart.Text = AbsencesClass.AbsenceNameFromCode(strDayCode)
            lblStart.ForeColor = Drawing.Color.DarkRed
            If DateDiff(DateInterval.Minute, dtStart, dtEnd) > 0 Then
                lblOffice.Text = "Hours: " & TimeRecords.FormatMinutes(DateDiff(DateInterval.Minute, dtStart, dtEnd), Session("User_ID"), False)
                lblOffice.ForeColor = Drawing.Color.DarkRed
            Else
                lblOffice.Visible = False
            End If
            lblDash.Visible = False
            lblEnd.Visible = False
            txtStartHour.Text = ""
            txtStartHour.BorderColor = Drawing.Color.Empty
            txtStartMin.Text = ""
            txtStartMin.BorderColor = Drawing.Color.Empty
            drpStartAMPM.SelectedValue = "AM"
            txtEndHour.Text = ""
            txtEndHour.BorderColor = Drawing.Color.Empty
            txtEndMin.Text = ""
            txtEndMin.BorderColor = Drawing.Color.Empty
            drpEndAMPM.SelectedValue = "AM"
        Else
            pnlScheduleChooser.Visible = True
            lblStatus.Text = "Not Scheduled"
            lblStatus.ForeColor = Drawing.Color.Black
            pnlSchedule.Visible = False
            litDayCodeOrig.Text = strDayCode
            lblStart.Text = "Not Scheduled"
            lblStart.ForeColor = Drawing.Color.Gray
            lblDash.Visible = False
            lblEnd.Visible = False
            lblOffice.Visible = False
            txtStartHour.Text = ""
            txtStartHour.BorderColor = Drawing.Color.Empty
            txtStartMin.Text = ""
            txtStartMin.BorderColor = Drawing.Color.Empty
            drpStartAMPM.SelectedValue = "AM"
            txtEndHour.Text = ""
            txtEndHour.BorderColor = Drawing.Color.Empty
            txtEndMin.Text = ""
            txtEndMin.BorderColor = Drawing.Color.Empty
            drpEndAMPM.SelectedValue = "AM"
        End If
    End Sub

    Protected Sub ScheduleDelete(ByVal intUser_ID As Int64)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "DELETE FROM [Schedules] WHERE (([User_ID] = @User_ID) AND ([Year] = @Year) AND ([WeekNum] = @WeekNum))"
        objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)
        objCommand.Parameters.AddWithValue("@Year", ViewState("intYear"))
        objCommand.Parameters.AddWithValue("@WeekNum", ViewState("intWeekOfYear"))
        objCommand.ExecuteNonQuery()
    End Sub

    Protected Sub ScheduleInsert(ByVal intUser_ID As Int64, ByVal strSunCode As String, ByVal dtSunStart As String, ByVal dtSunEnd As String, ByVal strMonCode As String, ByVal dtMonStart As String, ByVal dtMonEnd As String, ByVal strTueCode As String, ByVal dtTueStart As String, ByVal dtTueEnd As String, ByVal strWedCode As String, ByVal dtWedStart As String, ByVal dtWedEnd As String, ByVal strThuCode As String, ByVal dtThuStart As String, ByVal dtThuEnd As String, ByVal strFriCode As String, ByVal dtFriStart As String, ByVal dtFriEnd As String, ByVal strSatCode As String, ByVal dtSatStart As String, ByVal dtSatEnd As String)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "INSERT INTO [Schedules] ([User_ID], [Client_ID], [Year], [WeekNum], [SunCode], [SunStart], [SunEnd], [MonCode], [MonStart], [MonEnd], [TueCode], [TueStart], [TueEnd], [WedCode], [WedStart], [WedEnd], [ThuCode], [ThuStart], [ThuEnd], [FriCode], [FriStart], [FriEnd], [SatCode], [SatStart], [SatEnd]) VALUES (@User_ID, @Client_ID, @Year, @WeekNum, @SunCode, @SunStart, @SunEnd, @MonCode, @MonStart, @MonEnd, @TueCode, @TueStart, @TueEnd, @WedCode, @WedStart, @WedEnd, @ThuCode, @ThuStart, @ThuEnd, @FriCode, @FriStart, @FriEnd, @SatCode, @SatStart, @SatEnd)"
        objCommand.Parameters.AddWithValue("@User_ID", intUser_ID)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@Year", ViewState("intYear"))
        objCommand.Parameters.AddWithValue("@WeekNum", ViewState("intWeekOfYear"))
        objCommand.Parameters.AddWithValue("@SunCode", strSunCode)
        objCommand.Parameters.AddWithValue("@SunStart", dtSunStart)
        objCommand.Parameters.AddWithValue("@SunEnd", dtSunEnd)
        objCommand.Parameters.AddWithValue("@MonCode", strMonCode)
        objCommand.Parameters.AddWithValue("@MonStart", dtMonStart)
        objCommand.Parameters.AddWithValue("@MonEnd", dtMonEnd)
        objCommand.Parameters.AddWithValue("@TueCode", strTueCode)
        objCommand.Parameters.AddWithValue("@TueStart", dtTueStart)
        objCommand.Parameters.AddWithValue("@TueEnd", dtTueEnd)
        objCommand.Parameters.AddWithValue("@WedCode", strWedCode)
        objCommand.Parameters.AddWithValue("@WedStart", dtWedStart)
        objCommand.Parameters.AddWithValue("@WedEnd", dtWedEnd)
        objCommand.Parameters.AddWithValue("@ThuCode", strThuCode)
        objCommand.Parameters.AddWithValue("@ThuStart", dtThuStart)
        objCommand.Parameters.AddWithValue("@ThuEnd", dtThuEnd)
        objCommand.Parameters.AddWithValue("@FriCode", strFriCode)
        objCommand.Parameters.AddWithValue("@FriStart", dtFriStart)
        objCommand.Parameters.AddWithValue("@FriEnd", dtFriEnd)
        objCommand.Parameters.AddWithValue("@SatCode", strSatCode)
        objCommand.Parameters.AddWithValue("@SatStart", dtSatStart)
        objCommand.Parameters.AddWithValue("@SatEnd", dtSatEnd)
        objCommand.ExecuteNonQuery()
    End Sub

    Protected Sub rptSchedules_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs) Handles rptSchedules.ItemCommand
        'Sun
        Dim pnlScheduleSunChooser As Panel = e.Item.FindControl("pnlScheduleSunChooser")
        Dim pnlScheduleSun As Panel = e.Item.FindControl("pnlScheduleSun")
        Dim txtSunStartHour As TextBox = e.Item.FindControl("txtSunStartHour")
        Dim txtSunStartMin As TextBox = e.Item.FindControl("txtSunStartMin")
        Dim drpSunStartAMPM As DropDownList = e.Item.FindControl("drpSunStartAMPM")
        Dim lblSunDash As Label = e.Item.FindControl("lblSunDash")
        Dim txtSunEndHour As TextBox = e.Item.FindControl("txtSunEndHour")
        Dim txtSunEndMin As TextBox = e.Item.FindControl("txtSunEndMin")
        Dim drpSunEndAMPM As DropDownList = e.Item.FindControl("drpSunEndAMPM")
        Dim drpSunOffice As DropDownList = e.Item.FindControl("drpSunOffice")
        Dim lblSunStart As Label = e.Item.FindControl("lblSunStart")
        Dim lblSunEnd As Label = e.Item.FindControl("lblSunEnd")
        Dim lblSunOffice As Label = e.Item.FindControl("lblSunOffice")
        Dim btnSunNoOffice As LinkButton = e.Item.FindControl("btnSunNoOffice")
        Dim lblSunNoOfficeSep As Label = e.Item.FindControl("lblSunNoOfficeSep")
        Dim btnSunAssignOffice As LinkButton = e.Item.FindControl("btnSunAssignOffice")
        Dim chkSunEmployeeWorksOvernight As CheckBox = e.Item.FindControl("chkSunEmployeeWorksOvernight")
        'Mon
        Dim pnlScheduleMonChooser As Panel = e.Item.FindControl("pnlScheduleMonChooser")
        Dim pnlScheduleMon As Panel = e.Item.FindControl("pnlScheduleMon")
        Dim txtMonStartHour As TextBox = e.Item.FindControl("txtMonStartHour")
        Dim txtMonStartMin As TextBox = e.Item.FindControl("txtMonStartMin")
        Dim drpMonStartAMPM As DropDownList = e.Item.FindControl("drpMonStartAMPM")
        Dim lblMonDash As Label = e.Item.FindControl("lblMonDash")
        Dim txtMonEndHour As TextBox = e.Item.FindControl("txtMonEndHour")
        Dim txtMonEndMin As TextBox = e.Item.FindControl("txtMonEndMin")
        Dim drpMonEndAMPM As DropDownList = e.Item.FindControl("drpMonEndAMPM")
        Dim drpMonOffice As DropDownList = e.Item.FindControl("drpMonOffice")
        Dim lblMonStart As Label = e.Item.FindControl("lblMonStart")
        Dim lblMonEnd As Label = e.Item.FindControl("lblMonEnd")
        Dim lblMonOffice As Label = e.Item.FindControl("lblMonOffice")
        Dim btnMonNoOffice As LinkButton = e.Item.FindControl("btnMonNoOffice")
        Dim lblMonNoOfficeSep As Label = e.Item.FindControl("lblMonNoOfficeSep")
        Dim btnMonAssignOffice As LinkButton = e.Item.FindControl("btnMonAssignOffice")
        Dim chkMonEmployeeWorksOvernight As CheckBox = e.Item.FindControl("chkMonEmployeeWorksOvernight")
        'Tue
        Dim pnlScheduleTueChooser As Panel = e.Item.FindControl("pnlScheduleTueChooser")
        Dim pnlScheduleTue As Panel = e.Item.FindControl("pnlScheduleTue")
        Dim txtTueStartHour As TextBox = e.Item.FindControl("txtTueStartHour")
        Dim txtTueStartMin As TextBox = e.Item.FindControl("txtTueStartMin")
        Dim drpTueStartAMPM As DropDownList = e.Item.FindControl("drpTueStartAMPM")
        Dim lblTueDash As Label = e.Item.FindControl("lblTueDash")
        Dim txtTueEndHour As TextBox = e.Item.FindControl("txtTueEndHour")
        Dim txtTueEndMin As TextBox = e.Item.FindControl("txtTueEndMin")
        Dim drpTueEndAMPM As DropDownList = e.Item.FindControl("drpTueEndAMPM")
        Dim drpTueOffice As DropDownList = e.Item.FindControl("drpTueOffice")
        Dim lblTueStart As Label = e.Item.FindControl("lblTueStart")
        Dim lblTueEnd As Label = e.Item.FindControl("lblTueEnd")
        Dim lblTueOffice As Label = e.Item.FindControl("lblTueOffice")
        Dim btnTueNoOffice As LinkButton = e.Item.FindControl("btnTueNoOffice")
        Dim lblTueNoOfficeSep As Label = e.Item.FindControl("lblTueNoOfficeSep")
        Dim btnTueAssignOffice As LinkButton = e.Item.FindControl("btnTueAssignOffice")
        Dim chkTueEmployeeWorksOvernight As CheckBox = e.Item.FindControl("chkTueEmployeeWorksOvernight")
        'Wed
        Dim pnlScheduleWedChooser As Panel = e.Item.FindControl("pnlScheduleWedChooser")
        Dim pnlScheduleWed As Panel = e.Item.FindControl("pnlScheduleWed")
        Dim txtWedStartHour As TextBox = e.Item.FindControl("txtWedStartHour")
        Dim txtWedStartMin As TextBox = e.Item.FindControl("txtWedStartMin")
        Dim drpWedStartAMPM As DropDownList = e.Item.FindControl("drpWedStartAMPM")
        Dim lblWedDash As Label = e.Item.FindControl("lblWedDash")
        Dim txtWedEndHour As TextBox = e.Item.FindControl("txtWedEndHour")
        Dim txtWedEndMin As TextBox = e.Item.FindControl("txtWedEndMin")
        Dim drpWedEndAMPM As DropDownList = e.Item.FindControl("drpWedEndAMPM")
        Dim drpWedOffice As DropDownList = e.Item.FindControl("drpWedOffice")
        Dim lblWedStart As Label = e.Item.FindControl("lblWedStart")
        Dim lblWedEnd As Label = e.Item.FindControl("lblWedEnd")
        Dim lblWedOffice As Label = e.Item.FindControl("lblWedOffice")
        Dim btnWedNoOffice As LinkButton = e.Item.FindControl("btnWedNoOffice")
        Dim lblWedNoOfficeSep As Label = e.Item.FindControl("lblWedNoOfficeSep")
        Dim btnWedAssignOffice As LinkButton = e.Item.FindControl("btnWedAssignOffice")
        Dim chkWedEmployeeWorksOvernight As CheckBox = e.Item.FindControl("chkWedEmployeeWorksOvernight")
        'Thu
        Dim pnlScheduleThuChooser As Panel = e.Item.FindControl("pnlScheduleThuChooser")
        Dim pnlScheduleThu As Panel = e.Item.FindControl("pnlScheduleThu")
        Dim txtThuStartHour As TextBox = e.Item.FindControl("txtThuStartHour")
        Dim txtThuStartMin As TextBox = e.Item.FindControl("txtThuStartMin")
        Dim drpThuStartAMPM As DropDownList = e.Item.FindControl("drpThuStartAMPM")
        Dim lblThuDash As Label = e.Item.FindControl("lblThuDash")
        Dim txtThuEndHour As TextBox = e.Item.FindControl("txtThuEndHour")
        Dim txtThuEndMin As TextBox = e.Item.FindControl("txtThuEndMin")
        Dim drpThuEndAMPM As DropDownList = e.Item.FindControl("drpThuEndAMPM")
        Dim drpThuOffice As DropDownList = e.Item.FindControl("drpThuOffice")
        Dim lblThuStart As Label = e.Item.FindControl("lblThuStart")
        Dim lblThuEnd As Label = e.Item.FindControl("lblThuEnd")
        Dim lblThuOffice As Label = e.Item.FindControl("lblThuOffice")
        Dim btnThuNoOffice As LinkButton = e.Item.FindControl("btnThuNoOffice")
        Dim lblThuNoOfficeSep As Label = e.Item.FindControl("lblThuNoOfficeSep")
        Dim btnThuAssignOffice As LinkButton = e.Item.FindControl("btnThuAssignOffice")
        Dim chkThuEmployeeWorksOvernight As CheckBox = e.Item.FindControl("chkThuEmployeeWorksOvernight")
        'Fri
        Dim pnlScheduleFriChooser As Panel = e.Item.FindControl("pnlScheduleFriChooser")
        Dim pnlScheduleFri As Panel = e.Item.FindControl("pnlScheduleFri")
        Dim txtFriStartHour As TextBox = e.Item.FindControl("txtFriStartHour")
        Dim txtFriStartMin As TextBox = e.Item.FindControl("txtFriStartMin")
        Dim drpFriStartAMPM As DropDownList = e.Item.FindControl("drpFriStartAMPM")
        Dim lblFriDash As Label = e.Item.FindControl("lblFriDash")
        Dim txtFriEndHour As TextBox = e.Item.FindControl("txtFriEndHour")
        Dim txtFriEndMin As TextBox = e.Item.FindControl("txtFriEndMin")
        Dim drpFriEndAMPM As DropDownList = e.Item.FindControl("drpFriEndAMPM")
        Dim drpFriOffice As DropDownList = e.Item.FindControl("drpFriOffice")
        Dim lblFriStart As Label = e.Item.FindControl("lblFriStart")
        Dim lblFriEnd As Label = e.Item.FindControl("lblFriEnd")
        Dim lblFriOffice As Label = e.Item.FindControl("lblFriOffice")
        Dim btnFriNoOffice As LinkButton = e.Item.FindControl("btnFriNoOffice")
        Dim lblFriNoOfficeSep As Label = e.Item.FindControl("lblFriNoOfficeSep")
        Dim btnFriAssignOffice As LinkButton = e.Item.FindControl("btnFriAssignOffice")
        Dim chkFriEmployeeWorksOvernight As CheckBox = e.Item.FindControl("chkFriEmployeeWorksOvernight")
        'Sat
        Dim pnlScheduleSatChooser As Panel = e.Item.FindControl("pnlScheduleSatChooser")
        Dim pnlScheduleSat As Panel = e.Item.FindControl("pnlScheduleSat")
        Dim txtSatStartHour As TextBox = e.Item.FindControl("txtSatStartHour")
        Dim txtSatStartMin As TextBox = e.Item.FindControl("txtSatStartMin")
        Dim drpSatStartAMPM As DropDownList = e.Item.FindControl("drpSatStartAMPM")
        Dim lblSatDash As Label = e.Item.FindControl("lblSatDash")
        Dim txtSatEndHour As TextBox = e.Item.FindControl("txtSatEndHour")
        Dim txtSatEndMin As TextBox = e.Item.FindControl("txtSatEndMin")
        Dim drpSatEndAMPM As DropDownList = e.Item.FindControl("drpSatEndAMPM")
        Dim drpSatOffice As DropDownList = e.Item.FindControl("drpSatOffice")
        Dim lblSatStart As Label = e.Item.FindControl("lblSatStart")
        Dim lblSatEnd As Label = e.Item.FindControl("lblSatEnd")
        Dim lblSatOffice As Label = e.Item.FindControl("lblSatOffice")
        Dim btnSatNoOffice As LinkButton = e.Item.FindControl("btnSatNoOffice")
        Dim lblSatNoOfficeSep As Label = e.Item.FindControl("lblSatNoOfficeSep")
        Dim btnSatAssignOffice As LinkButton = e.Item.FindControl("btnSatAssignOffice")
        Dim chkSatEmployeeWorksOvernight As CheckBox = e.Item.FindControl("chkSatEmployeeWorksOvernight")

        Dim pnlScheduleRow As Panel = e.Item.FindControl("pnlScheduleRow")
        Dim litOriginalClass As Literal = e.Item.FindControl("litOriginalClass")
        Dim litScheduleVisible As Literal = e.Item.FindControl("litScheduleVisible")
        Dim litCurrencyType As Literal = e.Item.FindControl("litCurrencyType")
        Dim litWage As Literal = e.Item.FindControl("litWage")
        Dim lblWeekTotalHours As Label = e.Item.FindControl("lblWeekTotalHours")
        Dim lblWeekTotalPay As Label = e.Item.FindControl("lblWeekTotalPay")
        Dim litSunCode As Literal = e.Item.FindControl("litSunCode")
        Dim litSunCodeOrig As Literal = e.Item.FindControl("litSunCodeOrig")
        Dim litSunStart As Literal = e.Item.FindControl("litSunStart")
        Dim litSunEnd As Literal = e.Item.FindControl("litSunEnd")
        Dim litMonCode As Literal = e.Item.FindControl("litMonCode")
        Dim litMonCodeOrig As Literal = e.Item.FindControl("litMonCodeOrig")
        Dim litMonStart As Literal = e.Item.FindControl("litMonStart")
        Dim litMonEnd As Literal = e.Item.FindControl("litMonEnd")
        Dim litTueCode As Literal = e.Item.FindControl("litTueCode")
        Dim litTueCodeOrig As Literal = e.Item.FindControl("litTueCodeOrig")
        Dim litTueStart As Literal = e.Item.FindControl("litTueStart")
        Dim litTueEnd As Literal = e.Item.FindControl("litTueEnd")
        Dim litWedCode As Literal = e.Item.FindControl("litWedCode")
        Dim litWedCodeOrig As Literal = e.Item.FindControl("litWedCodeOrig")
        Dim litWedStart As Literal = e.Item.FindControl("litWedStart")
        Dim litWedEnd As Literal = e.Item.FindControl("litWedEnd")
        Dim litThuCode As Literal = e.Item.FindControl("litThuCode")
        Dim litThuCodeOrig As Literal = e.Item.FindControl("litThuCodeOrig")
        Dim litThuStart As Literal = e.Item.FindControl("litThuStart")
        Dim litThuEnd As Literal = e.Item.FindControl("litThuEnd")
        Dim litFriCode As Literal = e.Item.FindControl("litFriCode")
        Dim litFriCodeOrig As Literal = e.Item.FindControl("litFriCodeOrig")
        Dim litFriStart As Literal = e.Item.FindControl("litFriStart")
        Dim litFriEnd As Literal = e.Item.FindControl("litFriEnd")
        Dim litSatCode As Literal = e.Item.FindControl("litSatCode")
        Dim litSatCodeOrig As Literal = e.Item.FindControl("litSatCodeOrig")
        Dim litSatStart As Literal = e.Item.FindControl("litSatStart")
        Dim litSatEnd As Literal = e.Item.FindControl("litSatEnd")
        Dim pnlScheduleDays As Panel = e.Item.FindControl("pnlScheduleDays")
        Dim pnlScheduleNone As Panel = e.Item.FindControl("pnlScheduleNone")
        Dim pnlScheduleEdit As Panel = e.Item.FindControl("pnlScheduleEdit")
        Dim btnEditSchedule As ImageButton = e.Item.FindControl("btnEditSchedule")
        Dim btnSaveSchedule As ImageButton = e.Item.FindControl("btnSaveSchedule")
        Dim btnCancel As ImageButton = e.Item.FindControl("btnCancel")
        Dim btnDeleteSchedule As ImageButton = e.Item.FindControl("btnDeleteSchedule")

        Select Case e.CommandName
            Case "EditSchedule"
                If pnlScheduleRow.CssClass <> "schSaved" Then
                    litOriginalClass.Text = pnlScheduleRow.CssClass
                End If
                litScheduleVisible.Text = pnlScheduleDays.Visible
                pnlScheduleRow.Height = 136
                pnlScheduleRow.CssClass = "schSelected"
                pnlScheduleDays.Visible = False
                pnlScheduleNone.Visible = False
                pnlScheduleEdit.Visible = True
                btnEditSchedule.Visible = False
                btnSaveSchedule.Visible = True
                btnCancel.Visible = True
                If lblWeekTotalHours.Text <> "Not Scheduled" Then
                    If Permissions.ScheduleDelete(Session("User_ID")) Then
                        btnDeleteSchedule.Visible = True
                    End If
                Else
                    btnDeleteSchedule.Visible = False
                End If

                If btnDeleteSchedule.Visible Then
                    If Not IsNumeric(litSunCode.Text) And litSunCode.Text <> "NW" Then
                        btnDeleteSchedule.ImageUrl = "~/images/delete_small-disabled.png"
                        btnDeleteSchedule.Enabled = False
                        btnDeleteSchedule.AlternateText = "You cannot delete a schedule when it contains absences."
                        btnDeleteSchedule.ToolTip = "header=[Delete] body=[You cannot delete a schedule when it contains absences.] offsetx=[-150]"
                    End If
                    If Not IsNumeric(litMonCode.Text) And litMonCode.Text <> "NW" Then
                        btnDeleteSchedule.ImageUrl = "~/images/delete_small-disabled.png"
                        btnDeleteSchedule.Enabled = False
                        btnDeleteSchedule.AlternateText = "You cannot delete a schedule when it contains absences."
                        btnDeleteSchedule.ToolTip = "header=[Delete] body=[You cannot delete a schedule when it contains absences.] offsetx=[-150]"
                    End If
                    If Not IsNumeric(litTueCode.Text) And litTueCode.Text <> "NW" Then
                        btnDeleteSchedule.ImageUrl = "~/images/delete_small-disabled.png"
                        btnDeleteSchedule.Enabled = False
                        btnDeleteSchedule.AlternateText = "You cannot delete a schedule when it contains absences."
                        btnDeleteSchedule.ToolTip = "header=[Delete] body=[You cannot delete a schedule when it contains absences.] offsetx=[-150]"
                    End If
                    If Not IsNumeric(litWedCode.Text) And litWedCode.Text <> "NW" Then
                        btnDeleteSchedule.ImageUrl = "~/images/delete_small-disabled.png"
                        btnDeleteSchedule.Enabled = False
                        btnDeleteSchedule.AlternateText = "You cannot delete a schedule when it contains absences."
                        btnDeleteSchedule.ToolTip = "header=[Delete] body=[You cannot delete a schedule when it contains absences.] offsetx=[-150]"
                    End If
                    If Not IsNumeric(litThuCode.Text) And litThuCode.Text <> "NW" Then
                        btnDeleteSchedule.ImageUrl = "~/images/delete_small-disabled.png"
                        btnDeleteSchedule.Enabled = False
                        btnDeleteSchedule.AlternateText = "You cannot delete a schedule when it contains absences."
                        btnDeleteSchedule.ToolTip = "header=[Delete] body=[You cannot delete a schedule when it contains absences.] offsetx=[-150]"
                    End If
                    If Not IsNumeric(litFriCode.Text) And litFriCode.Text <> "NW" Then
                        btnDeleteSchedule.ImageUrl = "~/images/delete_small-disabled.png"
                        btnDeleteSchedule.Enabled = False
                        btnDeleteSchedule.AlternateText = "You cannot delete a schedule when it contains absences."
                        btnDeleteSchedule.ToolTip = "header=[Delete] body=[You cannot delete a schedule when it contains absences.] offsetx=[-150]"
                    End If
                    If Not IsNumeric(litSatCode.Text) And litSatCode.Text <> "NW" Then
                        btnDeleteSchedule.ImageUrl = "~/images/delete_small-disabled.png"
                        btnDeleteSchedule.Enabled = False
                        btnDeleteSchedule.AlternateText = "You cannot delete a schedule when it contains absences."
                        btnDeleteSchedule.ToolTip = "header=[Delete] body=[You cannot delete a schedule when it contains absences.] offsetx=[-150]"
                    End If
                End If

                Dim objDR As System.Data.SqlClient.SqlDataReader
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "EXEC sprocSelectAllOffices @Client_ID"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objDR = objCommand.ExecuteReader()

                If objDR.HasRows Then
                    drpSunOffice.DataSource = objDR
                    drpSunOffice.DataTextField = "Name"
                    drpSunOffice.DataValueField = "Office_ID"
                    drpSunOffice.DataBind()
                    If litSunCode.Text <> "0" And IsNumeric(litSunCode.Text) Then
                        drpSunOffice.SelectedValue = litSunCode.Text
                    End If

                    objDR.Close()
                    objDR = objCommand.ExecuteReader()

                    drpMonOffice.DataSource = objDR
                    drpMonOffice.DataTextField = "Name"
                    drpMonOffice.DataValueField = "Office_ID"
                    drpMonOffice.DataBind()
                    If litMonCode.Text <> "0" And IsNumeric(litMonCode.Text) Then
                        drpMonOffice.SelectedValue = litMonCode.Text
                    End If

                    objDR.Close()
                    objDR = objCommand.ExecuteReader()

                    drpTueOffice.DataSource = objDR
                    drpTueOffice.DataTextField = "Name"
                    drpTueOffice.DataValueField = "Office_ID"
                    drpTueOffice.DataBind()
                    If litTueCode.Text <> "0" And IsNumeric(litTueCode.Text) Then
                        drpTueOffice.SelectedValue = litTueCode.Text
                    End If

                    objDR.Close()
                    objDR = objCommand.ExecuteReader()

                    drpWedOffice.DataSource = objDR
                    drpWedOffice.DataTextField = "Name"
                    drpWedOffice.DataValueField = "Office_ID"
                    drpWedOffice.DataBind()
                    If litWedCode.Text <> "0" And IsNumeric(litWedCode.Text) Then
                        drpWedOffice.SelectedValue = litWedCode.Text
                    End If

                    objDR.Close()
                    objDR = objCommand.ExecuteReader()

                    drpThuOffice.DataSource = objDR
                    drpThuOffice.DataTextField = "Name"
                    drpThuOffice.DataValueField = "Office_ID"
                    drpThuOffice.DataBind()
                    If litThuCode.Text <> "0" And IsNumeric(litThuCode.Text) Then
                        drpThuOffice.SelectedValue = litThuCode.Text
                    End If

                    objDR.Close()
                    objDR = objCommand.ExecuteReader()

                    drpFriOffice.DataSource = objDR
                    drpFriOffice.DataTextField = "Name"
                    drpFriOffice.DataValueField = "Office_ID"
                    drpFriOffice.DataBind()
                    If litFriCode.Text <> "0" And IsNumeric(litFriCode.Text) Then
                        drpFriOffice.SelectedValue = litFriCode.Text
                    End If

                    objDR.Close()
                    objDR = objCommand.ExecuteReader()

                    drpSatOffice.DataSource = objDR
                    drpSatOffice.DataTextField = "Name"
                    drpSatOffice.DataValueField = "Office_ID"
                    drpSatOffice.DataBind()
                    If litSatCode.Text <> "0" And IsNumeric(litSatCode.Text) Then
                        drpSatOffice.SelectedValue = litSatCode.Text
                    End If
                Else
                    drpSunOffice.Visible = False
                    btnSunNoOffice.Visible = False
                    lblSunNoOfficeSep.Visible = False
                    btnSunAssignOffice.Visible = True
                    btnSunAssignOffice.Enabled = False
                    btnSunAssignOffice.Text = "No Offices"

                    drpMonOffice.Visible = False
                    btnMonNoOffice.Visible = False
                    lblMonNoOfficeSep.Visible = False
                    btnMonAssignOffice.Visible = True
                    btnMonAssignOffice.Enabled = False
                    btnMonAssignOffice.Text = "No Offices"

                    drpTueOffice.Visible = False
                    btnTueNoOffice.Visible = False
                    lblTueNoOfficeSep.Visible = False
                    btnTueAssignOffice.Visible = True
                    btnTueAssignOffice.Enabled = False
                    btnTueAssignOffice.Text = "No Offices"

                    drpWedOffice.Visible = False
                    btnWedNoOffice.Visible = False
                    lblWedNoOfficeSep.Visible = False
                    btnWedAssignOffice.Visible = True
                    btnWedAssignOffice.Enabled = False
                    btnWedAssignOffice.Text = "No Offices"

                    drpThuOffice.Visible = False
                    btnThuNoOffice.Visible = False
                    lblThuNoOfficeSep.Visible = False
                    btnThuAssignOffice.Visible = True
                    btnThuAssignOffice.Enabled = False
                    btnThuAssignOffice.Text = "No Offices"

                    drpFriOffice.Visible = False
                    btnFriNoOffice.Visible = False
                    lblFriNoOfficeSep.Visible = False
                    btnFriAssignOffice.Visible = True
                    btnFriAssignOffice.Enabled = False
                    btnFriAssignOffice.Text = "No Offices"

                    drpSatOffice.Visible = False
                    btnSatNoOffice.Visible = False
                    lblSatNoOfficeSep.Visible = False
                    btnSatAssignOffice.Visible = True
                    btnSatAssignOffice.Enabled = False
                    btnSatAssignOffice.Text = "No Offices"
                End If

                objDR.Close()
                objDR = objCommand.ExecuteReader()

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing
            Case "Working"
                Select Case e.CommandArgument
                    Case "Sun"
                        pnlScheduleSunChooser.Visible = False
                        pnlScheduleSun.Visible = True
                        If litSunCodeOrig.Text = "NW" Then
                            litSunCode.Text = "0"
                        Else
                            litSunCode.Text = litSunCodeOrig.Text
                        End If
                    Case "Mon"
                        pnlScheduleMonChooser.Visible = False
                        pnlScheduleMon.Visible = True
                        If litMonCodeOrig.Text = "NW" Then
                            litMonCode.Text = "0"
                        Else
                            litMonCode.Text = litMonCodeOrig.Text
                        End If
                    Case "Tue"
                        pnlScheduleTueChooser.Visible = False
                        pnlScheduleTue.Visible = True
                        If litTueCodeOrig.Text = "NW" Then
                            litTueCode.Text = "0"
                        Else
                            litTueCode.Text = litTueCodeOrig.Text
                        End If
                    Case "Wed"
                        pnlScheduleWedChooser.Visible = False
                        pnlScheduleWed.Visible = True
                        If litWedCodeOrig.Text = "NW" Then
                            litWedCode.Text = "0"
                        Else
                            litWedCode.Text = litWedCodeOrig.Text
                        End If
                    Case "Thu"
                        pnlScheduleThuChooser.Visible = False
                        pnlScheduleThu.Visible = True
                        If litThuCodeOrig.Text = "NW" Then
                            litThuCode.Text = "0"
                        Else
                            litThuCode.Text = litThuCodeOrig.Text
                        End If
                    Case "Fri"
                        pnlScheduleFriChooser.Visible = False
                        pnlScheduleFri.Visible = True
                        If litFriCodeOrig.Text = "NW" Then
                            litFriCode.Text = "0"
                        Else
                            litFriCode.Text = litFriCodeOrig.Text
                        End If
                    Case "Sat"
                        pnlScheduleSatChooser.Visible = False
                        pnlScheduleSat.Visible = True
                        If litSatCodeOrig.Text = "NW" Then
                            litSatCode.Text = "0"
                        Else
                            litSatCode.Text = litSatCodeOrig.Text
                        End If
                End Select
            Case "NotWorking"
                Select Case e.CommandArgument
                    Case "Sun"
                        pnlScheduleSunChooser.Visible = True
                        pnlScheduleSun.Visible = False
                        litSunCode.Text = "NW"
                        'litSunStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
                        'litSunEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
                    Case "Mon"
                        pnlScheduleMonChooser.Visible = True
                        pnlScheduleMon.Visible = False
                        litMonCode.Text = "NW"
                        'litMonStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
                        'litMonEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
                    Case "Tue"
                        pnlScheduleTueChooser.Visible = True
                        pnlScheduleTue.Visible = False
                        litTueCode.Text = "NW"
                        'litTueStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
                        'litTueEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
                    Case "Wed"
                        pnlScheduleWedChooser.Visible = True
                        pnlScheduleWed.Visible = False
                        litWedCode.Text = "NW"
                        'litWedStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
                        'litWedEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
                    Case "Thu"
                        pnlScheduleThuChooser.Visible = True
                        pnlScheduleThu.Visible = False
                        litThuCode.Text = "NW"
                        'litThuStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
                        'litThuEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
                    Case "Fri"
                        pnlScheduleFriChooser.Visible = True
                        pnlScheduleFri.Visible = False
                        litFriCode.Text = "NW"
                        'litFriStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
                        'litFriEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
                    Case "Sat"
                        pnlScheduleSatChooser.Visible = True
                        pnlScheduleSat.Visible = False
                        litSatCode.Text = "NW"
                        'litSatStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"
                        'litSatEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"
                        Trace.Warn("Sat", "litSatCode.Text = " & litSatCode.Text & ", litSatStart.Text = " & litSatStart.Text & ", litSatEnd.Text = " & litSatEnd.Text)
                End Select
            Case "NoOffice"
                Select Case e.CommandArgument
                    Case "Sun"
                        litSunCode.Text = "0"
                        drpSunOffice.Visible = False
                        btnSunNoOffice.Visible = False
                        lblSunNoOfficeSep.Visible = False
                        btnSunAssignOffice.Visible = True
                    Case "Mon"
                        litMonCode.Text = "0"
                        drpMonOffice.Visible = False
                        btnMonNoOffice.Visible = False
                        lblMonNoOfficeSep.Visible = False
                        btnMonAssignOffice.Visible = True
                    Case "Tue"
                        litTueCode.Text = "0"
                        drpTueOffice.Visible = False
                        btnTueNoOffice.Visible = False
                        lblTueNoOfficeSep.Visible = False
                        btnTueAssignOffice.Visible = True
                    Case "Wed"
                        litWedCode.Text = "0"
                        drpWedOffice.Visible = False
                        btnWedNoOffice.Visible = False
                        lblWedNoOfficeSep.Visible = False
                        btnWedAssignOffice.Visible = True
                    Case "Thu"
                        litThuCode.Text = "0"
                        drpThuOffice.Visible = False
                        btnThuNoOffice.Visible = False
                        lblThuNoOfficeSep.Visible = False
                        btnThuAssignOffice.Visible = True
                    Case "Fri"
                        litFriCode.Text = "0"
                        drpFriOffice.Visible = False
                        btnFriNoOffice.Visible = False
                        lblFriNoOfficeSep.Visible = False
                        btnFriAssignOffice.Visible = True
                    Case "Sat"
                        litSatCode.Text = "0"
                        drpSatOffice.Visible = False
                        btnSatNoOffice.Visible = False
                        lblSatNoOfficeSep.Visible = False
                        btnSatAssignOffice.Visible = True
                End Select
            Case "AssignOffice"
                Select Case e.CommandArgument
                    Case "Sun"
                        litSunCode.Text = drpSunOffice.SelectedValue
                        drpSunOffice.Visible = True
                        btnSunNoOffice.Visible = True
                        lblSunNoOfficeSep.Visible = True
                        btnSunAssignOffice.Visible = False
                    Case "Mon"
                        litMonCode.Text = drpMonOffice.SelectedValue
                        drpMonOffice.Visible = True
                        btnMonNoOffice.Visible = True
                        lblMonNoOfficeSep.Visible = True
                        btnMonAssignOffice.Visible = False
                    Case "Tue"
                        litTueCode.Text = drpTueOffice.SelectedValue
                        drpTueOffice.Visible = True
                        btnTueNoOffice.Visible = True
                        lblTueNoOfficeSep.Visible = True
                        btnTueAssignOffice.Visible = False
                    Case "Wed"
                        litWedCode.Text = drpWedOffice.SelectedValue
                        drpWedOffice.Visible = True
                        btnWedNoOffice.Visible = True
                        lblWedNoOfficeSep.Visible = True
                        btnWedAssignOffice.Visible = False
                    Case "Thu"
                        litThuCode.Text = drpThuOffice.SelectedValue
                        drpThuOffice.Visible = True
                        btnThuNoOffice.Visible = True
                        lblThuNoOfficeSep.Visible = True
                        btnThuAssignOffice.Visible = False
                    Case "Fri"
                        litFriCode.Text = drpFriOffice.SelectedValue
                        drpFriOffice.Visible = True
                        btnFriNoOffice.Visible = True
                        lblFriNoOfficeSep.Visible = True
                        btnFriAssignOffice.Visible = False
                    Case "Sat"
                        litSatCode.Text = drpSatOffice.SelectedValue
                        drpSatOffice.Visible = True
                        btnSatNoOffice.Visible = True
                        lblSatNoOfficeSep.Visible = True
                        btnSatAssignOffice.Visible = False
                End Select
            Case "SaveSchedule"
                Trace.Write("ScheduleEdit", "SaveSchedule Begin")
                Dim boolErrorFound As Boolean = False
                Dim bEmpWorksOvernight As Boolean = bCheckEmpWorksOvernight(Convert.ToInt32(e.CommandArgument))

                If IsNumeric(litSunCode.Text) Then
                    boolErrorFound = VerifyTimes(boolErrorFound, txtSunStartHour, txtSunStartMin, drpSunStartAMPM, txtSunEndHour, txtSunEndMin, drpSunEndAMPM, chkSunEmployeeWorksOvernight)

                    lblSunStart.Visible = True
                    lblSunStart.ForeColor = Drawing.Color.Black
                    lblSunStart.Text = txtSunStartHour.Text & ":" & txtSunStartMin.Text & " " & drpSunStartAMPM.SelectedValue
                    litSunStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " " & lblSunStart.Text
                    lblSunEnd.Visible = True
                    lblSunEnd.ForeColor = Drawing.Color.Black
                    lblSunEnd.Text = txtSunEndHour.Text & ":" & txtSunEndMin.Text & " " & drpSunEndAMPM.SelectedValue
                    litSunEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " " & lblSunEnd.Text
                    'if emp works overnight, add one day to end date.
                    If chkSunEmployeeWorksOvernight.Checked Then
                        litSunEnd.Text = Convert.ToDateTime(litSunEnd.Text).AddDays(1).ToString()
                    End If
                    If drpSunOffice.Visible Then
                        litSunCode.Text = drpSunOffice.SelectedValue
                    Else
                        litSunCode.Text = "0"
                    End If
                    If litSunCode.Text <> "0" Then
                        lblSunOffice.Visible = True
                        lblSunOffice.Text = drpSunOffice.SelectedItem.Text
                    Else
                        lblSunOffice.Visible = False
                    End If
                ElseIf litSunCode.Text <> "NW" Then
                    litSunStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
                    litSunEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
                Else
                    litSunStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
                    litSunEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
                End If

                Trace.Write("SaveSchedule", "Sun boolErrorFound = " & boolErrorFound)

                If IsNumeric(litMonCode.Text) Then
                    boolErrorFound = VerifyTimes(boolErrorFound, txtMonStartHour, txtMonStartMin, drpMonStartAMPM, txtMonEndHour, txtMonEndMin, drpMonEndAMPM, chkMonEmployeeWorksOvernight)

                    lblMonStart.Visible = True
                    lblMonStart.ForeColor = Drawing.Color.Black
                    lblMonStart.Text = txtMonStartHour.Text & ":" & txtMonStartMin.Text & " " & drpMonStartAMPM.SelectedValue
                    litMonStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " " & lblMonStart.Text
                    lblMonEnd.Visible = True
                    lblMonEnd.ForeColor = Drawing.Color.Black
                    lblMonEnd.Text = txtMonEndHour.Text & ":" & txtMonEndMin.Text & " " & drpMonEndAMPM.SelectedValue
                    litMonEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " " & lblMonEnd.Text
                    'if emp works overnight, add one day to end date.
                    If chkMonEmployeeWorksOvernight.Checked Then
                        litMonEnd.Text = Convert.ToDateTime(litMonEnd.Text).AddDays(1).ToString()
                    End If
                    If drpMonOffice.Visible Then
                        litMonCode.Text = drpMonOffice.SelectedValue
                    Else
                        litMonCode.Text = "0"
                    End If
                    If litMonCode.Text <> "0" Then
                        lblMonOffice.Visible = True
                        lblMonOffice.Text = drpMonOffice.SelectedItem.Text
                    Else
                        lblMonOffice.Visible = False
                    End If
                ElseIf litMonCode.Text <> "NW" Then
                    litMonStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
                    litMonEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
                Else
                    litMonStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
                    litMonEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
                End If

                Trace.Write("SaveSchedule", "Mon boolErrorFound = " & boolErrorFound)

                If IsNumeric(litTueCode.Text) Then
                    boolErrorFound = VerifyTimes(boolErrorFound, txtTueStartHour, txtTueStartMin, drpTueStartAMPM, txtTueEndHour, txtTueEndMin, drpTueEndAMPM, chkTueEmployeeWorksOvernight)

                    lblTueStart.Visible = True
                    lblTueStart.ForeColor = Drawing.Color.Black
                    lblTueStart.Text = txtTueStartHour.Text & ":" & txtTueStartMin.Text & " " & drpTueStartAMPM.SelectedValue
                    litTueStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " " & lblTueStart.Text
                    lblTueEnd.Visible = True
                    lblTueEnd.ForeColor = Drawing.Color.Black
                    lblTueEnd.Text = txtTueEndHour.Text & ":" & txtTueEndMin.Text & " " & drpTueEndAMPM.SelectedValue
                    litTueEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " " & lblTueEnd.Text
                    'if emp works overnight, add one day to end date.
                    If chkTueEmployeeWorksOvernight.Checked Then
                        litTueEnd.Text = Convert.ToDateTime(litTueEnd.Text).AddDays(1).ToString()
                    End If
                    If drpTueOffice.Visible Then
                        litTueCode.Text = drpTueOffice.SelectedValue
                    Else
                        litTueCode.Text = "0"
                    End If
                    If litTueCode.Text <> "0" Then
                        lblTueOffice.Visible = True
                        lblTueOffice.Text = drpTueOffice.SelectedItem.Text
                    Else
                        lblTueOffice.Visible = False
                    End If
                ElseIf litTueCode.Text <> "NW" Then
                    litTueStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
                    litTueEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
                Else
                    litTueStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
                    litTueEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
                End If

                Trace.Write("SaveSchedule", "Tue boolErrorFound = " & boolErrorFound)

                If IsNumeric(litWedCode.Text) Then
                    boolErrorFound = VerifyTimes(boolErrorFound, txtWedStartHour, txtWedStartMin, drpWedStartAMPM, txtWedEndHour, txtWedEndMin, drpWedEndAMPM, chkWedEmployeeWorksOvernight)

                    lblWedStart.Visible = True
                    lblWedStart.ForeColor = Drawing.Color.Black
                    lblWedStart.Text = txtWedStartHour.Text & ":" & txtWedStartMin.Text & " " & drpWedStartAMPM.SelectedValue
                    litWedStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " " & lblWedStart.Text
                    lblWedEnd.Visible = True
                    lblWedEnd.ForeColor = Drawing.Color.Black
                    lblWedEnd.Text = txtWedEndHour.Text & ":" & txtWedEndMin.Text & " " & drpWedEndAMPM.SelectedValue
                    litWedEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " " & lblWedEnd.Text
                    'if emp works overnight, add one day to end date.
                    If chkWedEmployeeWorksOvernight.Checked Then
                        litWedEnd.Text = Convert.ToDateTime(litWedEnd.Text).AddDays(1).ToString()
                    End If
                    If drpWedOffice.Visible Then
                        litWedCode.Text = drpWedOffice.SelectedValue
                    Else
                        litWedCode.Text = "0"
                    End If
                    If litWedCode.Text <> "0" Then
                        lblWedOffice.Visible = True
                        lblWedOffice.Text = drpWedOffice.SelectedItem.Text
                    Else
                        lblWedOffice.Visible = False
                    End If
                ElseIf litWedCode.Text <> "NW" Then
                    litWedStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
                    litWedEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
                Else
                    litWedStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
                    litWedEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
                End If

                Trace.Write("SaveSchedule", "Wed boolErrorFound = " & boolErrorFound)

                If IsNumeric(litThuCode.Text) Then
                    boolErrorFound = VerifyTimes(boolErrorFound, txtThuStartHour, txtThuStartMin, drpThuStartAMPM, txtThuEndHour, txtThuEndMin, drpThuEndAMPM, chkThuEmployeeWorksOvernight)

                    lblThuStart.Visible = True
                    lblThuStart.ForeColor = Drawing.Color.Black
                    lblThuStart.Text = txtThuStartHour.Text & ":" & txtThuStartMin.Text & " " & drpThuStartAMPM.SelectedValue
                    litThuStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " " & lblThuStart.Text
                    lblThuEnd.Visible = True
                    lblThuEnd.ForeColor = Drawing.Color.Black
                    lblThuEnd.Text = txtThuEndHour.Text & ":" & txtThuEndMin.Text & " " & drpThuEndAMPM.SelectedValue
                    litThuEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " " & lblThuEnd.Text
                    'if emp works overnight, add one day to end date.
                    If chkThuEmployeeWorksOvernight.Checked Then
                        litThuEnd.Text = Convert.ToDateTime(litThuEnd.Text).AddDays(1).ToString()
                    End If
                    If drpThuOffice.Visible Then
                        litThuCode.Text = drpThuOffice.SelectedValue
                    Else
                        litThuCode.Text = "0"
                    End If
                    If litThuCode.Text <> "0" Then
                        lblThuOffice.Visible = True
                        lblThuOffice.Text = drpThuOffice.SelectedItem.Text
                    Else
                        lblThuOffice.Visible = False
                    End If
                ElseIf litThuCode.Text <> "NW" Then
                    litThuStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
                    litThuEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
                Else
                    litThuStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
                    litThuEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
                End If

                Trace.Write("SaveSchedule", "Thu boolErrorFound = " & boolErrorFound)

                If IsNumeric(litFriCode.Text) Then
                    boolErrorFound = VerifyTimes(boolErrorFound, txtFriStartHour, txtFriStartMin, drpFriStartAMPM, txtFriEndHour, txtFriEndMin, drpFriEndAMPM, chkFriEmployeeWorksOvernight)

                    lblFriStart.Visible = True
                    lblFriStart.ForeColor = Drawing.Color.Black
                    lblFriStart.Text = txtFriStartHour.Text & ":" & txtFriStartMin.Text & " " & drpFriStartAMPM.SelectedValue
                    litFriStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " " & lblFriStart.Text
                    lblFriEnd.Visible = True
                    lblFriEnd.ForeColor = Drawing.Color.Black
                    lblFriEnd.Text = txtFriEndHour.Text & ":" & txtFriEndMin.Text & " " & drpFriEndAMPM.SelectedValue
                    litFriEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " " & lblFriEnd.Text
                    'if emp works overnight, add one day to end date.
                    If chkFriEmployeeWorksOvernight.Checked Then
                        litFriEnd.Text = Convert.ToDateTime(litFriEnd.Text).AddDays(1).ToString()
                    End If
                    If drpFriOffice.Visible Then
                        litFriCode.Text = drpFriOffice.SelectedValue
                    Else
                        litFriCode.Text = "0"
                    End If
                    If litFriCode.Text <> "0" Then
                        lblFriOffice.Visible = True
                        lblFriOffice.Text = drpFriOffice.SelectedItem.Text
                    Else
                        lblFriOffice.Visible = False
                    End If
                ElseIf litFriCode.Text <> "NW" Then
                    litFriStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
                    litFriEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
                Else
                    litFriStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
                    litFriEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
                End If

                Trace.Write("SaveSchedule", "Fri boolErrorFound = " & boolErrorFound)

                If IsNumeric(litSatCode.Text) Then
                    boolErrorFound = VerifyTimes(boolErrorFound, txtSatStartHour, txtSatStartMin, drpSatStartAMPM, txtSatEndHour, txtSatEndMin, drpSatEndAMPM, chkSatEmployeeWorksOvernight)

                    lblSatStart.Visible = True
                    lblSatStart.ForeColor = Drawing.Color.Black
                    lblSatStart.Text = txtSatStartHour.Text & ":" & txtSatStartMin.Text & " " & drpSatStartAMPM.SelectedValue
                    litSatStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " " & lblSatStart.Text
                    lblSatEnd.Visible = True
                    lblSatEnd.ForeColor = Drawing.Color.Black
                    lblSatEnd.Text = txtSatEndHour.Text & ":" & txtSatEndMin.Text & " " & drpSatEndAMPM.SelectedValue
                    litSatEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " " & lblSatEnd.Text
                    'if emp works overnight, add one day to end date.
                    If chkSatEmployeeWorksOvernight.Checked Then
                        litSatEnd.Text = Convert.ToDateTime(litSatEnd.Text).AddDays(1).ToString()
                    End If
                    If drpSatOffice.Visible Then
                        litSatCode.Text = drpSatOffice.SelectedValue
                    Else
                        litSatCode.Text = "0"
                    End If
                    If litSatCode.Text <> "0" Then
                        lblSatOffice.Visible = True
                        lblSatOffice.Text = drpSatOffice.SelectedItem.Text
                    Else
                        lblSatOffice.Visible = False
                    End If
                ElseIf litSatCode.Text <> "NW" Then
                    litSatStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"
                    litSatEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"
                Else
                    litSatStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"
                    litSatEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"
                End If

                If chkSunEmployeeWorksOvernight.Checked And txtMonStartHour.Visible Then
                    If Convert.ToDateTime(litSunEnd.Text) <= Convert.ToDateTime(litMonStart.Text) Then
                        txtSunEndHour.BorderColor = Drawing.Color.Empty
                        txtSunEndMin.BorderColor = Drawing.Color.Empty
                        drpSunEndAMPM.BorderColor = Drawing.Color.Empty
                        txtMonStartHour.BorderColor = Drawing.Color.Empty
                        txtMonStartMin.BorderColor = Drawing.Color.Empty
                        drpMonStartAMPM.BorderColor = Drawing.Color.Empty
                    Else
                        txtSunEndHour.CssClass = "textBoxError"
                        txtSunEndMin.CssClass = "textBoxError"
                        drpSunEndAMPM.CssClass = "textBoxError"
                        txtMonStartHour.CssClass = "textBoxError"
                        txtMonStartMin.CssClass = "textBoxError"
                        drpMonStartAMPM.CssClass = "textBoxError"
                        boolErrorFound = True
                    End If
                End If
                If chkMonEmployeeWorksOvernight.Checked And txtTueStartHour.Visible Then
                    If Convert.ToDateTime(litMonEnd.Text) <= Convert.ToDateTime(litTueStart.Text) Then
                        txtMonEndHour.BorderColor = Drawing.Color.Empty
                        txtMonEndMin.BorderColor = Drawing.Color.Empty
                        drpMonEndAMPM.BorderColor = Drawing.Color.Empty
                        txtTueStartHour.BorderColor = Drawing.Color.Empty
                        txtTueStartMin.BorderColor = Drawing.Color.Empty
                        drpTueStartAMPM.BorderColor = Drawing.Color.Empty
                    Else
                        txtMonEndHour.CssClass = "textBoxError"
                        txtMonEndMin.CssClass = "textBoxError"
                        drpMonEndAMPM.CssClass = "textBoxError"
                        txtTueStartHour.CssClass = "textBoxError"
                        txtTueStartMin.CssClass = "textBoxError"
                        drpTueStartAMPM.CssClass = "textBoxError"
                        boolErrorFound = True
                    End If
                End If
                If chkTueEmployeeWorksOvernight.Checked And txtWedStartHour.Visible Then
                    If Convert.ToDateTime(litTueEnd.Text) <= Convert.ToDateTime(litWedStart.Text) Then
                        txtTueEndHour.BorderColor = Drawing.Color.Empty
                        txtTueEndMin.BorderColor = Drawing.Color.Empty
                        drpTueEndAMPM.BorderColor = Drawing.Color.Empty
                        txtWedStartHour.BorderColor = Drawing.Color.Empty
                        txtWedStartMin.BorderColor = Drawing.Color.Empty
                        drpWedStartAMPM.BorderColor = Drawing.Color.Empty
                    Else
                        txtTueEndHour.CssClass = "textBoxError"
                        txtTueEndMin.CssClass = "textBoxError"
                        drpTueEndAMPM.CssClass = "textBoxError"
                        txtWedStartHour.CssClass = "textBoxError"
                        txtWedStartMin.CssClass = "textBoxError"
                        drpWedStartAMPM.CssClass = "textBoxError"
                        boolErrorFound = True
                    End If
                End If
                If chkWedEmployeeWorksOvernight.Checked And txtThuStartHour.Visible Then
                    If Convert.ToDateTime(litWedEnd.Text) <= Convert.ToDateTime(litThuStart.Text) Then
                        txtWedEndHour.BorderColor = Drawing.Color.Empty
                        txtWedEndMin.BorderColor = Drawing.Color.Empty
                        drpWedEndAMPM.BorderColor = Drawing.Color.Empty
                        txtThuStartHour.BorderColor = Drawing.Color.Empty
                        txtThuStartMin.BorderColor = Drawing.Color.Empty
                        drpThuStartAMPM.BorderColor = Drawing.Color.Empty
                    Else
                        txtWedEndHour.CssClass = "textBoxError"
                        txtWedEndMin.CssClass = "textBoxError"
                        drpWedEndAMPM.CssClass = "textBoxError"
                        txtThuStartHour.CssClass = "textBoxError"
                        txtThuStartMin.CssClass = "textBoxError"
                        drpThuStartAMPM.CssClass = "textBoxError"
                        boolErrorFound = True
                    End If
                End If
                If chkThuEmployeeWorksOvernight.Checked And txtFriStartHour.Visible Then
                    If Convert.ToDateTime(litThuEnd.Text) <= Convert.ToDateTime(litFriStart.Text) Then
                        txtThuEndHour.BorderColor = Drawing.Color.Empty
                        txtThuEndMin.BorderColor = Drawing.Color.Empty
                        drpThuEndAMPM.BorderColor = Drawing.Color.Empty
                        txtFriStartHour.BorderColor = Drawing.Color.Empty
                        txtFriStartMin.BorderColor = Drawing.Color.Empty
                        drpFriStartAMPM.BorderColor = Drawing.Color.Empty
                    Else
                        txtThuEndHour.CssClass = "textBoxError"
                        txtThuEndMin.CssClass = "textBoxError"
                        drpThuEndAMPM.CssClass = "textBoxError"
                        txtFriStartHour.CssClass = "textBoxError"
                        txtFriStartMin.CssClass = "textBoxError"
                        drpFriStartAMPM.CssClass = "textBoxError"
                        boolErrorFound = True
                    End If
                End If
                If chkFriEmployeeWorksOvernight.Checked And txtSatStartHour.Visible Then
                    If Convert.ToDateTime(litFriEnd.Text) <= Convert.ToDateTime(litSatStart.Text) Then
                        txtFriEndHour.BorderColor = Drawing.Color.Empty
                        txtFriEndMin.BorderColor = Drawing.Color.Empty
                        drpFriEndAMPM.BorderColor = Drawing.Color.Empty
                        txtSatStartHour.BorderColor = Drawing.Color.Empty
                        txtSatStartMin.BorderColor = Drawing.Color.Empty
                        drpSatStartAMPM.BorderColor = Drawing.Color.Empty
                    Else
                        txtFriEndHour.CssClass = "textBoxError"
                        txtFriEndMin.CssClass = "textBoxError"
                        drpFriEndAMPM.CssClass = "textBoxError"
                        txtSatStartHour.CssClass = "textBoxError"
                        txtSatStartMin.CssClass = "textBoxError"
                        drpSatStartAMPM.CssClass = "textBoxError"
                        boolErrorFound = True
                    End If
                End If
                If chkSatEmployeeWorksOvernight.Checked And txtSunStartHour.Visible Then
                    If Convert.ToDateTime(litSatEnd.Text) <= Convert.ToDateTime(litSunStart.Text).AddDays(7) Then
                        txtSatEndHour.BorderColor = Drawing.Color.Empty
                        txtSatEndMin.BorderColor = Drawing.Color.Empty
                        drpSatEndAMPM.BorderColor = Drawing.Color.Empty
                        txtSunStartHour.BorderColor = Drawing.Color.Empty
                        txtSunStartMin.BorderColor = Drawing.Color.Empty
                        drpSunStartAMPM.BorderColor = Drawing.Color.Empty
                    Else
                        txtSatEndHour.CssClass = "textBoxError"
                        txtSatEndMin.CssClass = "textBoxError"
                        drpSatEndAMPM.CssClass = "textBoxError"
                        txtSunStartHour.CssClass = "textBoxError"
                        txtSunStartMin.CssClass = "textBoxError"
                        drpSunStartAMPM.CssClass = "textBoxError"
                        boolErrorFound = True
                    End If
                End If

                Trace.Write("SaveSchedule", "Sat boolErrorFound = " & boolErrorFound)

                If Not boolErrorFound Then
                    Trace.Write("SaveSchedule", "Saving Schedule")
                    pnlScheduleRow.Height = 36
                    pnlScheduleRow.CssClass = "schSaved"
                    pnlScheduleDays.Visible = True
                    pnlScheduleEdit.Visible = False
                    btnEditSchedule.Visible = True
                    btnSaveSchedule.Visible = False
                    btnCancel.Visible = False
                    btnDeleteSchedule.Visible = False

                    Trace.Write("SaveSchedule", "Deleting Schedule")
                    ScheduleDelete(e.CommandArgument)
                    Trace.Write("SaveSchedule", "Inserting Schedule")
                    ScheduleInsert(e.CommandArgument, litSunCode.Text, litSunStart.Text, litSunEnd.Text, litMonCode.Text, litMonStart.Text, litMonEnd.Text, litTueCode.Text, litTueStart.Text, litTueEnd.Text, litWedCode.Text, litWedStart.Text, litWedEnd.Text, litThuCode.Text, litThuStart.Text, litThuEnd.Text, litFriCode.Text, litFriStart.Text, litFriEnd.Text, litSatCode.Text, litSatStart.Text, litSatEnd.Text)

                    Trace.Write("SaveSchedule", "Processing Totals")
                    ScheduleTotalsProcess(e.CommandArgument, litCurrencyType.Text, litWage.Text, lblWeekTotalHours, lblWeekTotalPay)

                    Trace.Write("SaveSchedule", "Processing Days")
                    ScheduleDayProcess(litSunCode.Text, litSunCodeOrig, litSunStart.Text, litSunEnd.Text, pnlScheduleSunChooser, lblSunStatus, pnlScheduleSun, lblSunStart, lblSunDash, lblSunEnd, lblSunOffice, txtSunStartHour, txtSunStartMin, drpSunStartAMPM, txtSunEndHour, txtSunEndMin, drpSunEndAMPM, drpSunOffice, btnSunNoOffice, lblSunNoOfficeSep, btnSunAssignOffice, btnDeleteSchedule)
                    ScheduleDayProcess(litMonCode.Text, litMonCodeOrig, litMonStart.Text, litMonEnd.Text, pnlScheduleMonChooser, lblMonStatus, pnlScheduleMon, lblMonStart, lblMonDash, lblMonEnd, lblMonOffice, txtMonStartHour, txtMonStartMin, drpMonStartAMPM, txtMonEndHour, txtMonEndMin, drpMonEndAMPM, drpMonOffice, btnMonNoOffice, lblMonNoOfficeSep, btnMonAssignOffice, btnDeleteSchedule)
                    ScheduleDayProcess(litTueCode.Text, litTueCodeOrig, litTueStart.Text, litTueEnd.Text, pnlScheduleTueChooser, lblTueStatus, pnlScheduleTue, lblTueStart, lblTueDash, lblTueEnd, lblTueOffice, txtTueStartHour, txtTueStartMin, drpTueStartAMPM, txtTueEndHour, txtTueEndMin, drpTueEndAMPM, drpTueOffice, btnTueNoOffice, lblTueNoOfficeSep, btnTueAssignOffice, btnDeleteSchedule)
                    ScheduleDayProcess(litWedCode.Text, litWedCodeOrig, litWedStart.Text, litWedEnd.Text, pnlScheduleWedChooser, lblWedStatus, pnlScheduleWed, lblWedStart, lblWedDash, lblWedEnd, lblWedOffice, txtWedStartHour, txtWedStartMin, drpWedStartAMPM, txtWedEndHour, txtWedEndMin, drpWedEndAMPM, drpWedOffice, btnWedNoOffice, lblWedNoOfficeSep, btnWedAssignOffice, btnDeleteSchedule)
                    ScheduleDayProcess(litThuCode.Text, litThuCodeOrig, litThuStart.Text, litThuEnd.Text, pnlScheduleThuChooser, lblThuStatus, pnlScheduleThu, lblThuStart, lblThuDash, lblThuEnd, lblThuOffice, txtThuStartHour, txtThuStartMin, drpThuStartAMPM, txtThuEndHour, txtThuEndMin, drpThuEndAMPM, drpThuOffice, btnThuNoOffice, lblThuNoOfficeSep, btnThuAssignOffice, btnDeleteSchedule)
                    ScheduleDayProcess(litFriCode.Text, litFriCodeOrig, litFriStart.Text, litFriEnd.Text, pnlScheduleFriChooser, lblFriStatus, pnlScheduleFri, lblFriStart, lblFriDash, lblFriEnd, lblFriOffice, txtFriStartHour, txtFriStartMin, drpFriStartAMPM, txtFriEndHour, txtFriEndMin, drpFriEndAMPM, drpFriOffice, btnFriNoOffice, lblFriNoOfficeSep, btnFriAssignOffice, btnDeleteSchedule)
                    ScheduleDayProcess(litSatCode.Text, litSatCodeOrig, litSatStart.Text, litSatEnd.Text, pnlScheduleSatChooser, lblSatStatus, pnlScheduleSat, lblSatStart, lblSatDash, lblSatEnd, lblSatOffice, txtSatStartHour, txtSatStartMin, drpSatStartAMPM, txtSatEndHour, txtSatEndMin, drpSatEndAMPM, drpSatOffice, btnSatNoOffice, lblSatNoOfficeSep, btnSatAssignOffice, btnDeleteSchedule)

                    Trace.Write("SaveSchedule", "Schedule Saved")
                End If
                Trace.Write("ScheduleEdit", "SaveSchedule End")
            Case "Cancel"
                pnlScheduleRow.Height = 36
                pnlScheduleRow.CssClass = litOriginalClass.Text
                If litScheduleVisible.Text Then
                    pnlScheduleDays.Visible = True
                    pnlScheduleNone.Visible = False
                Else
                    pnlScheduleDays.Visible = False
                    pnlScheduleNone.Visible = True
                End If
                pnlScheduleEdit.Visible = False
                btnEditSchedule.Visible = True
                btnSaveSchedule.Visible = False
                btnCancel.Visible = False
                btnCancel.Visible = False
                btnDeleteSchedule.Visible = False

                litSunCode.Text = litSunCodeOrig.Text
                litMonCode.Text = litMonCodeOrig.Text
                litTueCode.Text = litTueCodeOrig.Text
                litWedCode.Text = litWedCodeOrig.Text
                litThuCode.Text = litThuCodeOrig.Text
                litFriCode.Text = litFriCodeOrig.Text
                litSatCode.Text = litSatCodeOrig.Text

                ScheduleDayProcess(litSunCode.Text, litSunCodeOrig, litSunStart.Text, litSunEnd.Text, pnlScheduleSunChooser, lblSunStatus, pnlScheduleSun, lblSunStart, lblSunDash, lblSunEnd, lblSunOffice, txtSunStartHour, txtSunStartMin, drpSunStartAMPM, txtSunEndHour, txtSunEndMin, drpSunEndAMPM, drpSunOffice, btnSunNoOffice, lblSunNoOfficeSep, btnSunAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litMonCode.Text, litMonCodeOrig, litMonStart.Text, litMonEnd.Text, pnlScheduleMonChooser, lblMonStatus, pnlScheduleMon, lblMonStart, lblMonDash, lblMonEnd, lblMonOffice, txtMonStartHour, txtMonStartMin, drpMonStartAMPM, txtMonEndHour, txtMonEndMin, drpMonEndAMPM, drpMonOffice, btnMonNoOffice, lblMonNoOfficeSep, btnMonAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litTueCode.Text, litTueCodeOrig, litTueStart.Text, litTueEnd.Text, pnlScheduleTueChooser, lblTueStatus, pnlScheduleTue, lblTueStart, lblTueDash, lblTueEnd, lblTueOffice, txtTueStartHour, txtTueStartMin, drpTueStartAMPM, txtTueEndHour, txtTueEndMin, drpTueEndAMPM, drpTueOffice, btnTueNoOffice, lblTueNoOfficeSep, btnTueAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litWedCode.Text, litWedCodeOrig, litWedStart.Text, litWedEnd.Text, pnlScheduleWedChooser, lblWedStatus, pnlScheduleWed, lblWedStart, lblWedDash, lblWedEnd, lblWedOffice, txtWedStartHour, txtWedStartMin, drpWedStartAMPM, txtWedEndHour, txtWedEndMin, drpWedEndAMPM, drpWedOffice, btnWedNoOffice, lblWedNoOfficeSep, btnWedAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litThuCode.Text, litThuCodeOrig, litThuStart.Text, litThuEnd.Text, pnlScheduleThuChooser, lblThuStatus, pnlScheduleThu, lblThuStart, lblThuDash, lblThuEnd, lblThuOffice, txtThuStartHour, txtThuStartMin, drpThuStartAMPM, txtThuEndHour, txtThuEndMin, drpThuEndAMPM, drpThuOffice, btnThuNoOffice, lblThuNoOfficeSep, btnThuAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litFriCode.Text, litFriCodeOrig, litFriStart.Text, litFriEnd.Text, pnlScheduleFriChooser, lblFriStatus, pnlScheduleFri, lblFriStart, lblFriDash, lblFriEnd, lblFriOffice, txtFriStartHour, txtFriStartMin, drpFriStartAMPM, txtFriEndHour, txtFriEndMin, drpFriEndAMPM, drpFriOffice, btnFriNoOffice, lblFriNoOfficeSep, btnFriAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litSatCode.Text, litSatCodeOrig, litSatStart.Text, litSatEnd.Text, pnlScheduleSatChooser, lblSatStatus, pnlScheduleSat, lblSatStart, lblSatDash, lblSatEnd, lblSatOffice, txtSatStartHour, txtSatStartMin, drpSatStartAMPM, txtSatEndHour, txtSatEndMin, drpSatEndAMPM, drpSatOffice, btnSatNoOffice, lblSatNoOfficeSep, btnSatAssignOffice, btnDeleteSchedule)
            Case "DeleteSchedule"
                ScheduleDelete(e.CommandArgument)

                pnlScheduleRow.Height = 36
                pnlScheduleRow.CssClass = litOriginalClass.Text
                pnlScheduleDays.Visible = False
                pnlScheduleNone.Visible = True
                pnlScheduleEdit.Visible = False
                btnEditSchedule.Visible = True
                btnSaveSchedule.Visible = False
                btnCancel.Visible = False
                btnDeleteSchedule.Visible = False

                litSunCode.Text = "NW"
                litSunStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
                litSunEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 0) & " 12:00 AM"
                litMonCode.Text = "NW"
                litMonStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
                litMonEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 1) & " 12:00 AM"
                litTueCode.Text = "NW"
                litTueStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
                litTueEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 2) & " 12:00 AM"
                litWedCode.Text = "NW"
                litWedStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
                litWedEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 3) & " 12:00 AM"
                litThuCode.Text = "NW"
                litThuStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
                litThuEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 4) & " 12:00 AM"
                litFriCode.Text = "NW"
                litFriStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
                litFriEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 5) & " 12:00 AM"
                litSatCode.Text = "NW"
                litSatStart.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"
                litSatEnd.Text = Time.DateOfWeekDay(ViewState("intWeekOfYear"), ViewState("intYear"), 6) & " 12:00 AM"

                ScheduleDayProcess(litSunCode.Text, litSunCodeOrig, litSunStart.Text, litSunEnd.Text, pnlScheduleSunChooser, lblSunStatus, pnlScheduleSun, lblSunStart, lblSunDash, lblSunEnd, lblSunOffice, txtSunStartHour, txtSunStartMin, drpSunStartAMPM, txtSunEndHour, txtSunEndMin, drpSunEndAMPM, drpSunOffice, btnSunNoOffice, lblSunNoOfficeSep, btnSunAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litMonCode.Text, litMonCodeOrig, litMonStart.Text, litMonEnd.Text, pnlScheduleMonChooser, lblMonStatus, pnlScheduleMon, lblMonStart, lblMonDash, lblMonEnd, lblMonOffice, txtMonStartHour, txtMonStartMin, drpMonStartAMPM, txtMonEndHour, txtMonEndMin, drpMonEndAMPM, drpMonOffice, btnMonNoOffice, lblMonNoOfficeSep, btnMonAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litTueCode.Text, litTueCodeOrig, litTueStart.Text, litTueEnd.Text, pnlScheduleTueChooser, lblTueStatus, pnlScheduleTue, lblTueStart, lblTueDash, lblTueEnd, lblTueOffice, txtTueStartHour, txtTueStartMin, drpTueStartAMPM, txtTueEndHour, txtTueEndMin, drpTueEndAMPM, drpTueOffice, btnTueNoOffice, lblTueNoOfficeSep, btnTueAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litWedCode.Text, litWedCodeOrig, litWedStart.Text, litWedEnd.Text, pnlScheduleWedChooser, lblWedStatus, pnlScheduleWed, lblWedStart, lblWedDash, lblWedEnd, lblWedOffice, txtWedStartHour, txtWedStartMin, drpWedStartAMPM, txtWedEndHour, txtWedEndMin, drpWedEndAMPM, drpWedOffice, btnWedNoOffice, lblWedNoOfficeSep, btnWedAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litThuCode.Text, litThuCodeOrig, litThuStart.Text, litThuEnd.Text, pnlScheduleThuChooser, lblThuStatus, pnlScheduleThu, lblThuStart, lblThuDash, lblThuEnd, lblThuOffice, txtThuStartHour, txtThuStartMin, drpThuStartAMPM, txtThuEndHour, txtThuEndMin, drpThuEndAMPM, drpThuOffice, btnThuNoOffice, lblThuNoOfficeSep, btnThuAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litFriCode.Text, litFriCodeOrig, litFriStart.Text, litFriEnd.Text, pnlScheduleFriChooser, lblFriStatus, pnlScheduleFri, lblFriStart, lblFriDash, lblFriEnd, lblFriOffice, txtFriStartHour, txtFriStartMin, drpFriStartAMPM, txtFriEndHour, txtFriEndMin, drpFriEndAMPM, drpFriOffice, btnFriNoOffice, lblFriNoOfficeSep, btnFriAssignOffice, btnDeleteSchedule)
                ScheduleDayProcess(litSatCode.Text, litSatCodeOrig, litSatStart.Text, litSatEnd.Text, pnlScheduleSatChooser, lblSatStatus, pnlScheduleSat, lblSatStart, lblSatDash, lblSatEnd, lblSatOffice, txtSatStartHour, txtSatStartMin, drpSatStartAMPM, txtSatEndHour, txtSatEndMin, drpSatEndAMPM, drpSatOffice, btnSatNoOffice, lblSatNoOfficeSep, btnSatAssignOffice, btnDeleteSchedule)

                lblWeekTotalHours.Text = "Not Scheduled"
                lblWeekTotalPay.Visible = False
        End Select
    End Sub

    Protected Function VerifyTimes(ByVal boolErrorFound As Boolean, ByVal txtStartHour As TextBox, ByVal txtStartMin As TextBox, ByVal drpStartAMPM As DropDownList, ByVal txtEndHour As TextBox, ByVal txtEndMin As TextBox, ByVal drpEndAMPM As DropDownList, ByVal chkMonEmployeeWorksOvernight As CheckBox) As Boolean
        'Returns False if times verify, True otherwise
        Dim boolDoCompare As Boolean = True

        If Not IsNumeric(txtStartHour.Text) Then
            Trace.Write("VerifyTimes", "Fail 1")
            txtStartHour.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtStartHour.Text > 12 Then
            Trace.Write("VerifyTimes", "Fail 2")
            txtStartHour.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtStartHour.Text < 0 Then
            Trace.Write("VerifyTimes", "Fail 3")
            txtStartHour.CssClass = "textBoxError"
            boolDoCompare = False
        Else
            Trace.Write("VerifyTimes", "Pass 1")
            txtStartHour.BorderColor = Drawing.Color.Empty
            txtStartHour.BackColor = Drawing.Color.Empty
        End If

        If Not IsNumeric(txtStartMin.Text) Then
            Trace.Write("VerifyTimes", "Fail 4")
            txtStartMin.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtStartMin.Text > 59 Then
            Trace.Write("VerifyTimes", "Fail 5")
            txtStartMin.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtStartMin.Text < 0 Then
            Trace.Write("VerifyTimes", "Fail 6")
            txtStartMin.CssClass = "textBoxError"
            boolDoCompare = False
        Else
            Trace.Write("VerifyTimes", "Pass 2")
            txtStartMin.BorderColor = Drawing.Color.Empty
            txtStartMin.BackColor = Drawing.Color.Empty
        End If

        If Not IsNumeric(txtEndHour.Text) Then
            Trace.Write("VerifyTimes", "Fail 7")
            txtEndHour.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtEndHour.Text > 12 Then
            Trace.Write("VerifyTimes", "Fail 8")
            txtEndHour.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtEndHour.Text < 0 Then
            Trace.Write("VerifyTimes", "Fail 9")
            txtEndHour.CssClass = "textBoxError"
            boolDoCompare = False
        Else
            Trace.Write("VerifyTimes", "Pass 3")
            txtEndHour.BorderColor = Drawing.Color.Empty
            txtEndHour.BackColor = Drawing.Color.Empty
        End If

        If Not IsNumeric(txtEndMin.Text) Then
            Trace.Write("VerifyTimes", "Fail 10")
            txtEndMin.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtEndMin.Text > 59 Then
            Trace.Write("VerifyTimes", "Fail 11")
            txtEndMin.CssClass = "textBoxError"
            boolDoCompare = False
        ElseIf txtEndMin.Text < 0 Then
            Trace.Write("VerifyTimes", "Fail 12")
            txtEndMin.CssClass = "textBoxError"
            boolDoCompare = False
        Else
            Trace.Write("VerifyTimes", "Pass 4")
            txtEndMin.BorderColor = Drawing.Color.Empty
            txtEndMin.BackColor = Drawing.Color.Empty
        End If

        If boolDoCompare Then
            Dim dtStart As DateTime = "6/27/2008 " & txtStartHour.Text & ":" & txtStartMin.Text & " " & drpStartAMPM.SelectedValue
            Dim dtEnd As DateTime = "6/27/2008 " & txtEndHour.Text & ":" & txtEndMin.Text & " " & drpEndAMPM.SelectedValue

            'if emp (user) works on same day or works over night (allow to work less than 48 hours)
            If (DateDiff(DateInterval.Minute, dtStart, dtEnd) > 0) Or (chkMonEmployeeWorksOvernight.Checked And (DateDiff(DateInterval.Hour, dtStart, dtEnd.AddDays(1)) < 48)) Then
                txtEndHour.BorderColor = Drawing.Color.Empty
                txtEndMin.BorderColor = Drawing.Color.Empty
                drpEndAMPM.BorderColor = Drawing.Color.Empty
                If Not boolErrorFound Then
                    VerifyTimes = False
                Else
                    VerifyTimes = True
                End If
            Else
                VerifyTimes = True
                txtEndHour.CssClass = "textBoxError"
                txtEndMin.CssClass = "textBoxError"
                drpEndAMPM.CssClass = "textBoxError"
            End If
        Else
            VerifyTimes = True
        End If
    End Function

    Protected Sub timerLogout_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timerLogout.Tick
        Response.Redirect(Constants.InactiveLogoutURL)
    End Sub

    Protected Function bCheckEmpWorksOvernight(ByVal iUserID As Integer) As Boolean
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT OvernightEmployee FROM [Users] WHERE [User_ID] = " & iUserID
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            bCheckEmpWorksOvernight = objDR("OvernightEmployee")
        Else
            bCheckEmpWorksOvernight = False
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection = Nothing
    End Function

    Protected Sub btnCostGuardUpdateTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCostGuardUpdateTop.Click
        ViewState("CostGuardPay") = 0
        GetSchedules()
        UpdateCostGuard()
    End Sub
End Class
