<%@ Page Language="VB" Debug="true" MasterPageFile="~/MasterPage2.master" AutoEventWireup="false" CodeFile="ForgotPassword.aspx.vb" Inherits="ForgotPassword" title="TimeClockWizard - Forgot Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
 <div class="main-content" style="width:100%;">  
     <center>    
    <div style="position:absolute; top:200px; width:100%; margin:0px auto;">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

              

            <div style="width:100%; margin:0px auto;">
                <asp:Panel ID="pnlChange" runat="server">

                    <div style="width: 492px; height:300px; text-align: center; background-image:url(images/passwordbox.jpg); background-repeat:no-repeat; padding: 15px 0; margin-bottom: 20px;">
                    <table style="width: 492px; border: 0px;">
                    <tr>
                        <td align="center" colspan="2" style="height: 64px; text-align: center">
                            <asp:Label ID="lblLoginText" runat="server" Text="Please enter your new password" ForeColor="White"></asp:Label>
                        </td>
                    </tr>

<tr>
                        <td width="175" style="height: 26px; text-align: right; color:white;" align="right">
                            <strong>Username:</strong>

                                </td>
                                <td style="height: 26px; text-align: left; color:#FFFFFF;">&nbsp;&nbsp;
                                    <asp:Literal ID="litUsername" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        <tr><td colspan="2" height="10px"></td></tr>
                            <tr>
                        <td width="175" style="height: 20px; text-align: right; color:white;" align="right"><strong>Password:</strong></td>
                                <td style="height: 20px; text-align: left;">&nbsp;&nbsp;
                                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" Font-Size="Large" CssClass="rounded"></asp:TextBox>
                                </td>
                            </tr>
                        <tr><td colspan="2" height="10px"></td></tr>

                          <tr>
                        <td width="175" style="height: 20px; text-align: right; color:white;" align="right"><strong>Confirm Password:</strong>
                                </td>
                                <td style="height: 20px; text-align: left;">&nbsp;&nbsp;
                                    <asp:TextBox ID="txtConfirm" TextMode="Password" runat="server" Font-Size="Large" CssClass="rounded"></asp:TextBox>
                                </td>
                            </tr>
                        <tr><td colspan="2" height="10px"></td></tr>
                            <tr>
                                <td colspan="2">
                                    <asp:ImageButton ID="btnSubmit" runat="server" Text="Change Password" OnClick="btnSubmit_Click" ImageUrl="~/images/password-btn.png" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:RequiredFieldValidator ID="valUsername" runat="server" ControlToValidate="txtConfirm" ErrorMessage="'New Password' field is required<br />" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="valCompare" ControlToValidate="txtConfirm" ControlToCompare="txtPassword" runat="server" ErrorMessage="Passwords do not match"></asp:CompareValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlError" Visible="false" runat="server">
                    <strong style="color:Red;"><asp:Literal ID="litInitialError" runat="server"></asp:Literal></strong>
                </asp:Panel>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
            </div>
                  </center>
     </div>
</asp:Content>

