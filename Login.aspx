<%@ Page Language="VB" Debug="true" Trace="false" MasterPageFile="~/MasterPage2.master" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" title="TimeClockWizard Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
 <div class="main-content" style="width:100%;">   
    <br />

    
  <center>    
    <div style="position:absolute; top:150px; width:100%; margin:0px auto;">

        <asp:Panel ID="pnlLogo" runat="server" style="font-size:x-large;">
            <asp:Label ID="lblClientName" runat="server" Text="Client Name"></asp:Label>
            <asp:Image ID="imgClientLogo" runat="server" Visible="false" />
        </asp:Panel>
</div>

            <div style="position:absolute; top:250px; width:100%; margin:0px auto;">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
                <asp:Panel ID="pnlLogin" runat="server">

                    <div style="width: 492px; height:235px; text-align: center; background-image:url(images/loginbox.jpg); background-repeat:no-repeat; padding: 15px 0; margin-bottom: 20px;">
                    <table style="width: 492px; border: 0px;">
                    <tr>
                        <td align="center" colspan="2" style="height: 64px; text-align: center">
                            <asp:Label ID="lblLoginText" runat="server" Text="Please enter your login information" ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="175" style="height: 26px; text-align: right; color:white;" align="right">
                            <strong>Username:</strong>
                                <asp:RequiredFieldValidator ID="valUsername" runat="server" ControlToValidate="txtUsername"
                                    ErrorMessage="<br />Required" Display="Dynamic" Font-Size="Small" ForeColor="White"></asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 26px; text-align: left;">&nbsp;&nbsp;
                            <asp:TextBox ID="txtUsername" runat="server" Font-Size="Large" CssClass="rounded"></asp:TextBox>
                        </td>
                    </tr>
                        <tr><td colspan="2" height="10px"></td></tr>
                    <tr>
                        <td width="175" style="height: 20px; text-align: right; color:white;" align="right">
                            <strong>Password:</strong>
                                <asp:RequiredFieldValidator ID="valPassword" runat="server" ControlToValidate="txtPassword"
                                    ErrorMessage="<br />Required" Display="Dynamic" Font-Size="Small" ForeColor="White"></asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 20px; text-align: left;">&nbsp;&nbsp;
                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Font-Size="Large" CssClass="rounded"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="height: 20px;">
                            <br />
                            <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/images/loginbutton.jpg" />
                            <asp:Literal ID="litQuickClockSpacer1" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;"></asp:Literal>
                            <asp:ImageButton ID="btnClockIn" runat="server" ImageUrl="~/images/clockinbutton.jpg" />
                            <asp:Literal ID="litQuickClockSpacer2" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;"></asp:Literal>
                            <asp:ImageButton ID="btnClockOut" runat="server" ImageUrl="~/images/clockoutbutton.jpg" />
                            <asp:Timer ID="timerClearQuickClock" runat="server" Interval="4000" Enabled="false">
                            </asp:Timer>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblClockInMessage" runat="server" ForeColor="White" ></asp:Label>
                        </td>
                    </tr>
                </table>
                        </div>
            </asp:Panel>
        


        </ContentTemplate>
    </asp:UpdatePanel>
        </div>
</center>

<br /><br />
    <center>
   
<div style="position:absolute; top:500px; width:100%; margin:0px auto;">

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
           <div style="font-size:12px;" >
            <asp:LinkButton ID="lnkForgot" CausesValidation="false" runat="server" OnClick="lnkForgot_Click">Forgot Username or Password</asp:LinkButton>
            <asp:Panel ID="pnlForgot" Visible="false" runat="server">
                <br />
                        Please enter the e-mail address that is listed in your TimeClockWizard account, and your
                        username and password will be e-mailed to you.
                        <br />
                        <br />
                        <asp:TextBox ID="txtForgotEmail" runat="server"></asp:TextBox><asp:ImageButton ID="btnForgot"
                            ImageUrl="~/images/mileage_add.png" ImageAlign="AbsMiddle" CausesValidation="false"
                            runat="server" OnClick="btnForgot_Click" />
                        <br />
                        <br />
                <asp:Label ID="lblForgotMessage" runat="server" Visible="false" Text=""></asp:Label>
            </asp:Panel>
                    <br />
                    <br />
         
                <asp:ImageButton ID="btnSelfRegister" runat="server" CausesValidation="false" Visible="false" ImageUrl="~/images/self-register.png" OnClick="btnSelfRegister_Click"/>
                <asp:Panel ID="pnlSRVerify" Visible="false" runat="server">
                <br />
                Enter PIN: <asp:TextBox ID="txtPIN" Width="60" runat="server" CausesValidation="false"></asp:TextBox><asp:ImageButton ID="btnPINSubmit" CausesValidation="false" ImageAlign="AbsMiddle" ImageUrl="~/images/mileage_add.png" runat="server" OnClick="btnPINSubmit_Click" />
                <asp:Label ID="lblPINMessage" runat="server" Visible="false"></asp:Label>
                </asp:Panel>

               <br /><br />
             
    <asp:Panel ID="pnlCustomLoginMessage" Visible="false" runat="server"><font style="font-weight:bold; font-size:16px; color:#f00;">MESSAGE</font><br /><br />
        <div style="width:500px; text-align: center; font-size:14px;">
        <asp:Label ID="lblCustomLoginMessage" runat="server"></asp:Label>
        </div>
        <br />
        <br />
    </asp:Panel> 
                    
           
       </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>




        </center>




    </div>
</asp:Content>