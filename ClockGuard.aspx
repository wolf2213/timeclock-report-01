<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ClockGuard.aspx.vb" Inherits="ClockGuard" title="TimeClockWizard - ClockGuard Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

<div style="margin-left: 25px; float: left;">  
    <asp:Image ID="Shield" ImageUrl="~/images/shield.png" ImageAlign="AbsMiddle" runat="server" />
    <span class="title">ClockGuard Settings For <asp:Literal ID="litFullUserName" runat="server"></asp:Literal></span>
    (<asp:LinkButton ID="btnDisable" runat="server" OnClick="btnDisable_Click">Disable</asp:LinkButton>)
    <br /><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <span class="subtitle">Do Not Allow</span>
            <div class="preferencesBox">
                <table cellpadding="5px" cellspacing="0px">
                    <tr>
                        <td>
                            <asp:Literal ID="litDoNotAllowClockInBeforeSchStartTime" runat="server"></asp:Literal> Do Not Allow User Clock In Before Schedule Start Time&nbsp;<asp:Image ID="Image55" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" ToolTip="cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Do Not Clock In Before Schedule Start Time] body=[Checking this setting will not allow user to Clock In before his/her schedule start time. Message will be displayed to user. It will not alert manager.]" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkDoNotAllowClockInBeforeSchStartTime" runat="server" />
                        </td>
                    </tr>  
                    <tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litDoNotAllowUserWorkAfterScheduleEndTime" runat="server"></asp:Literal> Do Not Allow User Work After Schedule End Time&nbsp;<asp:Image ID="Image56" ImageAlign="AbsMiddle" ImageUrl="~/images/help_small.png" runat="server" ToolTip="cssheader=[boHeader bo200pxWide] cssbody=[boBody bo200pxWide] header=[Do Not Allow User Work After Schedule End Time] body=[Checking this setting will not allow user to work after his/her schedule end time. If user Clock Out after schedule time, his/her time record clock out time is set to schedule end time. Message will be displayed to user. It will not alert manager.]" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkDoNotAllowUserWorkAfterScheduleEndTime" runat="server" />
                        </td>
                    </tr>  
                </table>
            </div>
         <br /><br />
        <span class="subtitle">Alert Me When</span>
            <div class="preferencesBox">
                <table cellpadding="5px" cellspacing="0px">
                    <tr>
                        <td>
                            <asp:Literal ID="litFirstName1" runat="server"></asp:Literal> Clocks In
                        </td>
                        <td>
                            <asp:CheckBox ID="chkIn" runat="server" AutoPostBack="true" OnCheckedChanged="chkIn_CheckedChanged" />
                        </td>
                    </tr>  
                    <tr>
                        <td>
                            <asp:Literal ID="litFirstName2"  runat="server"></asp:Literal> Clocks In Early
                        </td>
                        <td>
                            <asp:CheckBox ID="chkInEarly" AutoPostBack="true" runat="server" OnCheckedChanged="chkInEarly_CheckedChanged" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlClocksInEarlyTime" Visible="true" runat="server" >
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Literal ID="ltrlClocksInEarlyTime" runat="server">Minitues Before</asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litFirstName3" runat="server"></asp:Literal> Clocks In Late
                        </td>
                        <td>
                            <asp:CheckBox ID="chkInLate" AutoPostBack="true" runat="server" OnCheckedChanged="chkInLate_CheckedChanged" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlClocksInLateTime" Visible="true" runat="server" >
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Literal ID="ltrlClocksInLateTime" runat="server">Minitues After</asp:Literal>
                        </td>
                    </tr>
                    <tr><td colspan="2" style="height:10px;"></td></tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litFirstName4" runat="server"></asp:Literal> Clocks Out
                        </td>
                        <td>
                            <asp:CheckBox ID="chkOut" AutoPostBack="true" runat="server" OnCheckedChanged="chkOut_CheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litFirstName5" runat="server"></asp:Literal> Clocks Out Early
                        </td>
                        <td>
                            <asp:CheckBox ID="chkOutEarly" AutoPostBack="true" runat="server" OnCheckedChanged="chkOutEarly_CheckedChanged" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlClocksOutEarlyTime" Visible="true" runat="server" >
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Literal ID="ltrlClocksOutEarlyTime" runat="server">Minitues Before</asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litFirstName6" runat="server"></asp:Literal> Clocks Out Late
                        </td>
                        <td>
                            <asp:CheckBox ID="chkOutLate" AutoPostBack="true" runat="server" OnCheckedChanged="chkOutLate_CheckedChanged" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlClocksOutLateTime" Visible="true" runat="server" >
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Literal ID="ltrlClocksOutLateTime" runat="server">Minitues After</asp:Literal>
                        </td>
                    </tr>
                </table>
            </div>
         <br /><br />
         <span class="subtitle">Alert Me By</span>
            <div class="preferencesBox">
                <table cellspacing="0px">
                    <tr>
                        <td valign="middle">
                            Primary Email
                        </td>
                        <td valign="middle" style="height:26px; padding-left:10px;">
                            <asp:CheckBox ID="chkEmail" AutoPostBack="true" OnCheckedChanged="chkEmail_CheckedChanged" runat="server" />
                            <asp:TextBox ID="txtEmail" Visible="false" runat="server" Width="200px"></asp:TextBox>
                        </td>
                        <td valign="middle">
                            Primary Text Message
                        </td>
                        <td valign="middle" style="height:26px; padding-left:10px;">
                            <asp:CheckBox ID="chkText" AutoPostBack="true" OnCheckedChanged="chkText_CheckedChanged" runat="server" />
                            <asp:TextBox ID="txtPhone" Visible="false" runat="server" Width="100px"></asp:TextBox>
                            <asp:DropDownList ID="drpCarrier" Visible="false" runat="server">
                                <asp:ListItem Value="1">AT&amp;T</asp:ListItem>
                                <asp:ListItem Value="2">Verizon</asp:ListItem>
					            <asp:ListItem Value="3">Sprint</asp:ListItem>
					            <asp:ListItem Value="4">T-Mobile</asp:ListItem>
					            <asp:ListItem Value="5">Nextel</asp:ListItem>
					            <asp:ListItem Value="6">Alltel</asp:ListItem>
					            <asp:ListItem Value="7">MetroPCS</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            Secondary Email
                        </td>
                        <td valign="middle" style="height:26px; padding-left:10px;">
                            <asp:CheckBox ID="chkEmail2" AutoPostBack="true" OnCheckedChanged="chkEmail2_CheckedChanged" runat="server" />
                            <asp:TextBox ID="txtEmail2" Visible="false" runat="server" Width="200px"></asp:TextBox>
                        </td>
                        <td valign="middle">
                            Secondary Text Message
                        </td>
                        <td valign="middle" style="height:26px; padding-left:10px;">
                            <asp:CheckBox ID="chkText2" AutoPostBack="true" OnCheckedChanged="chkText2_CheckedChanged" runat="server" />
                            <asp:TextBox ID="txtPhone2" Visible="false" runat="server" Width="100px"></asp:TextBox>
                            <asp:DropDownList ID="drpCarrier2" Visible="false" runat="server">
                                <asp:ListItem Value="1">AT&amp;T</asp:ListItem>
                                <asp:ListItem Value="2">Verizon</asp:ListItem>
					            <asp:ListItem Value="3">Sprint</asp:ListItem>
					            <asp:ListItem Value="4">T-Mobile</asp:ListItem>
					            <asp:ListItem Value="5">Nextel</asp:ListItem>
					            <asp:ListItem Value="6">Alltel</asp:ListItem>
					            <asp:ListItem Value="7">MetroPCS</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <br /><br />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnSubmit" runat="server" Text="Save Settings" OnClick="btnSubmit_Click" />
            <br /><br />
            <asp:Label ID="lblSubmitMessage" runat="server" Text=""></asp:Label>
            <span style="color:GrayText;"> Text messages may result in fees from your cellular carrier.</span>
        </ContentTemplate>
    </asp:UpdatePanel>

    </div>
</asp:Content>
