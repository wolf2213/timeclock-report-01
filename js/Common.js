﻿
function isNumeric(evt) {
    var charCode = evt.which;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function IsNumericPhone(evt) {
    var charCode = evt.which;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        if (charCode == 40 || charCode == 41 || charCode == 43) {
            return true;
        }
        else {
            return false;
        }
    }
    return true;
}
function GetSelectedItem(obj, divdaily, divweekly) {
    var radio = obj.getElementsByTagName("input");
    var divDailyhours = document.getElementById(divdaily);
    var divWeeklyhours = document.getElementById(divweekly);
    var val;
    for (var i = 0; i < radio.length; i++) {
        if (radio[i].checked) {
            radio[i].checked;
            val = radio[i].value;
        }
    }
    if (val == 1) {
        divDailyhours.style.display = 'block';
        divWeeklyhours.style.display = 'none';
    }
    else if (val == 2) {
        divDailyhours.style.display = 'none';
        divWeeklyhours.style.display = 'block';
    }
    else {
        divDailyhours.style.display = 'block';
        divWeeklyhours.style.display = 'block';
    }
}
