
Partial Class TrialExpired
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Manager") Then
            Response.Redirect("SettingsSubscription.aspx")
        End If
    End Sub
End Class
