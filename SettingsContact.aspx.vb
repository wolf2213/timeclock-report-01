
Partial Class SettingsContact
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabSettings As Panel = Master.FindControl("pnlTabSettings")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2BusinessInfo As Panel = Master.FindControl("pnlTab2BusinessInfo")

        pnlMasterTabSettings.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Settings
        pnlMasterTab2BusinessInfo.CssClass = "tab2 tab2On"

        If Not Permissions.AccountHolder(Session("User_ID")) Then
            Response.Redirect("Default.aspx")
        End If

        If Session("UpdateContactInfo") Then
            pnlUpdateContactInfo.Visible = True
        End If

        If Not IsPostBack Then
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT [CompanyName], [Address], [Address2], [City], [State], [Zip], [First_name], [Last_name], [Email], [Phone] FROM Clients WHERE Client_ID = @Client_ID"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objDR = objCommand.ExecuteReader()
            objDR.Read()

            txtCompanyName.Text = Trim(objDR("CompanyName"))
            txtAddress1.Text = Trim(Null.Replace(objDR("Address")))
            txtAddress2.Text = Trim(Null.Replace(objDR("Address2")))
            txtCity.Text = Trim(Null.Replace(objDR("City")))
            txtState.Text = Trim(Null.Replace(objDR("State")))
            txtZip.Text = Trim(Null.Replace(objDR("Zip")))

            txtFirstName.Text = Trim(objDR("First_name"))
            txtLastName.Text = Trim(objDR("Last_name"))
            txtEmail.Text = Trim(objDR("Email"))
            txtPhone.Text = Trim(Null.Replace(objDR("Phone")))

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim boolUpdate As Boolean = True

        If UserInfo.ValidateEmail(txtEmail.Text) = False Then
            boolUpdate = False
            lblMessage.Visible = True
            lblMessage.Text = "Please enter a valid email address"
            lblMessage.ForeColor = Drawing.Color.Red
        End If

        If boolUpdate Then
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "UPDATE Clients SET CompanyName = @CompanyName, First_name = @First_name, Last_name = @Last_name, Email = @Email, Phone = @Phone, Address = @Address, Address2 = @Address2, City = @City, State = @State, Zip = @Zip, LastContactInfoUpdate = @LastContactInfoUpdate WHERE Client_ID = @Client_ID"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text)
            objCommand.Parameters.AddWithValue("@First_name", txtFirstName.Text)
            objCommand.Parameters.AddWithValue("@Last_name", txtLastName.Text)
            objCommand.Parameters.AddWithValue("@Email", txtEmail.Text)
            objCommand.Parameters.AddWithValue("@Phone", txtPhone.Text)
            objCommand.Parameters.AddWithValue("@Address", txtAddress1.Text)
            objCommand.Parameters.AddWithValue("@Address2", txtAddress2.Text)
            objCommand.Parameters.AddWithValue("@City", txtCity.Text)
            objCommand.Parameters.AddWithValue("@State", txtState.Text)
            objCommand.Parameters.AddWithValue("@Zip", Trim(txtZip.Text))
            objCommand.Parameters.AddWithValue("@LastContactInfoUpdate", Now.Date & " 12:00 AM")
            objCommand.ExecuteNonQuery()

            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            If Session("UpdateContactInfo") = True Then
                Session("UpdateContactInfo") = False
                Response.Redirect("Default.aspx")
            End If

            lblMessage.Visible = True
            lblMessage.ForeColor = Drawing.Color.Green
            lblMessage.Text = "Contact information successfully updated."
        End If
    End Sub
End Class
