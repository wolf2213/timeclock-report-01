<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SettingsTransactionHistory.aspx.vb" Inherits="SettingsTransactionHistory" title="TimeClockWizard - Transaction History" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0px" cellspacing="0px">
                <tr>
                    <td valign="top" style="padding-right:10px;">
                        <asp:Panel ID="pnlTransactionChooser" runat="server" CssClass="greyBox">
                            <span class="title">
                                Choose Invoice
                            </span>
                            <br />
                            <asp:Repeater ID="rptTransactionChooser" runat="server" OnItemDataBound="rptTransactionChooser_ItemDataBound" OnItemCommand="rptTransactionChooser_ItemCommand" EnableViewState="True">
                                <HeaderTemplate>
                                    <table cellspacing="0px" cellpadding="5px">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="trAlternate">
                                        <td>
                                            <asp:Image ID="imgDelinquent" runat="server" ImageUrl="~/images/warning_small.png" Visible="false" ImageAlign="AbsMiddle" AlternateText="Warning: This transaction is unpaid.  Please pay it by going to the 'My Subscription' page to ensure uninterrupted service." />
                                            <asp:LinkButton ID="btnDate" runat="server"></asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAmount" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                    </td>
                    <td valign="top">
                        <asp:Repeater ID="rptDetail" runat="server" OnItemDataBound="rptDetail_ItemDataBound" EnableViewState="True">
                            <HeaderTemplate>
                                <table cellspacing="0px" cellpadding="5px" style="max-width:500px;">
                                    <tr class="trHeader">
                                        <th width="410px">Description</th>
                                        <th>Price</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="trNormal">
                                    <td>
                                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPrice" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="trAlternate">
                                    <td>
                                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPrice" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                <tr>
                                    <td align="right">
                                        <strong>Total: </strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                </table>
                                <br />
                                <center>
                                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                </center>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

