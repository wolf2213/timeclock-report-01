<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserChangePassword.aspx.vb" Inherits="UserChangePassword" title="TimeClockWizard - Change My Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="main-content">
        <div class="edit-password">


            									<strong>Change My Password</strong>
            <asp:Label ID="lblLoginText" runat="server" Text=""></asp:Label>  
								</div>
                 
            <br /><br />
        <div class="box1-info">
										<h4>mandatory information</h4>
<div class="form-holder" style="padding-bottom: 40px;">



                <asp:Panel ID="pnlChange" runat="server">

    											<div class="field-holder">
											<label>Current Password:</label>
												<span class="left">
													<div class="right"> 
                                <asp:TextBox ID="txtCurrent" TextMode="password" runat="server"></asp:TextBox>
                                                  </div></span>  
												<div class="clear"></div>
											    </div>



                                                <div class="field-holder">
											<label>New Password:</label>
												<span class="left">
													<div class="right"> 
                                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                                               </div></span>
												<div class="clear"></div>
											    </div>


<div class="field-holder">
											<label>Confirm Password:</label>
                                                   <span class="left">
													<div class="right">                    
                                 <asp:TextBox ID="txtConfirm" TextMode="Password" runat="server"></asp:TextBox>
                                                  </div></span>  
												<div class="clear"></div>
											</div>

                           <br /><br />
                    <div style="text-align:center;">
                    <asp:ImageButton ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" ImageUrl="~/images/chg-password-btn.png" />
                    </div>

                                
                                <asp:RequiredFieldValidator ID="valCurrent" runat="server" ControlToValidate="txtCurrent" ErrorMessage="The 'Current Password' field is required<br />" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="valPass1" runat="server" ControlToValidate="txtPassword" ErrorMessage="The 'New Password' field is required<br />" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="valCompare" ControlToValidate="txtConfirm" ControlToCompare="txtPassword" runat="server" ErrorMessage="Passwords do not match"></asp:CompareValidator>
                            
                   
                </asp:Panel>
                 <asp:Label ID="lblMessage" Visible="false" runat="server" Text="Label"></asp:Label>
     </div>
            </div>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>

