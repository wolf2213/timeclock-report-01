<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserPersonalEdit.aspx.vb" Inherits="UserPersonalEdit" title="TimeClockWizard - Edit My User Information" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="main-content">
        <div class="edit-user">
									<strong>Edit My User Information</strong>
            <asp:Label ID="lblLoginText" runat="server" Text=""></asp:Label>
								</div>
        <br /><br />
        <div class="box1-info">
										<h4>mandatory information</h4>
<div class="form-holder" style="padding-bottom: 40px;">


 											<div class="field-holder">
											<label>username</label>
                                                   <span class="left">
													<div class="right">                    
                                <asp:TextBox ID="txtUsername" MaxLength="16" runat="server"></asp:TextBox>
                                                  </div></span>  
												<div class="clear"></div>
											</div>

                       <div class="field-holder" style="margin-top: 20px;">
											<label>first name</label>
											<span class="left">
													<div class="right"> 

                                <asp:TextBox ID="txtFirstName" MaxLength="25" runat="server"></asp:TextBox>
                          </div></span>
												<div class="clear"></div>
											</div>


                                        <div class="field-holder">
											<label>last name</label>
											<span class="left">
													<div class="right"> 

                                        <asp:TextBox ID="txtLastName" MaxLength="25" runat="server"></asp:TextBox>                          </div></span>
												<div class="clear"></div>
											</div>

   <div class="field-holder" style="margin: 25px 0;">
											<label>email address</label>
												<span class="left">
													<div class="right">                           
                                <asp:TextBox ID="txtEmail" MaxLength="50" runat="server"></asp:TextBox>
                                       </div>
												</span>                 

												<div class="clear"></div>
												
											</div>

                           
                            <asp:Panel ID="pnlMandCust1" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblMandCust1" runat="server" Text="Custom Field 1"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtMandCust1" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlMandCust2" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblMandCust2" runat="server" Text="Custom Field 2"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtMandCust2" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlMandCust3" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblMandCust3" runat="server" Text="Custom Field 3"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtMandCust3" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlMandCust4" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblMandCust4" runat="server" Text="Custom Field 4"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtMandCust4" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlMandCust5" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblMandCust5" runat="server" Text="Custom Field 5"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtMandCust5" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
              
	
										</div> <!-- end of formholder -->
									</div> <!-- end of mandatory -->




									<div class="box2-info">
										<h4>optional information</h4>
										<div class="form-holder" style="padding-bottom: 55px;">


											<div class="field-holder">
											<label>address line 1</label>
												<span class="left">
													<div class="right">

                                <asp:TextBox ID="txtAddress1" MaxLength="50" runat="server"></asp:TextBox>
                           </div>
												</span>
												<div class="clear"></div>
											</div>

                            <div class="field-holder">
											<label>address line 2</label>
												<span class="left">
													<div class="right">
                                <asp:TextBox ID="txtAddress2" MaxLength="50" runat="server"></asp:TextBox>
                            </div>
												</span>
												<div class="clear"></div>
										</div>


                            <div class="field-holder" style="margin-bottom: 20px;">
											<label>city, state, zip</label>
												<span class="left" style="margin-left:4px;">
													<div class="right">


                                <asp:TextBox ID="txtCity" MaxLength="25"  Width="70px" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtState" MaxLength="2" Width="20px" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtZip" MaxLength="5" Width="40px" runat="server"></asp:TextBox>
                            </div>
												</span>
											<div class="clear"></div>
										</div>


                                <div class="field-holder">
											<label>home phone</label>
												<span class="left">
													<div class="right">
                                <asp:TextBox ID="txtHomePhone" MaxLength="14" runat="server"></asp:TextBox>
                                                    </div>
												</span>
											<div class="clear"></div>
								</div>

                             <div class="field-holder">
											<label>cell phone</label>
												<span class="left">
													<div class="right">
                                <asp:TextBox ID="txtCellPhone" MaxLength="14" runat="server"></asp:TextBox>
                           </div>
												</span>
												<div class="clear"></div>
											</div>


                        <div class="field-holder">
											<label>work phone</label>
												<span class="left">
													<div class="right">
                                <asp:TextBox ID="txtWorkPhone" MaxLength="14" runat="server"></asp:TextBox>
                           </div>
												</span>
										<div class="clear"></div>
									</div>


                        <div class="field-holder" style="margin-bottom: 25px;">
											<label>work extention</label>
												<span class="left" style="float: none; margin-left: 32px;">
													<div class="right">
                               
                                <asp:TextBox ID="txtWorkExt" Width="40" MaxLength="5" runat="server"></asp:TextBox>
                           </div>
												</span>
										<div class="clear"></div>
									</div>


                        <div class="field-holder">
											<label>time display</label>
												<span class="left" style="float: none; margin-left: 52px;">
													<div class="right">
                                <asp:DropDownList ID="drpTimeDisplay" runat="server">
                                    <asp:ListItem Value="False">Hours:Minutes (6:30)</asp:ListItem>
                                    <asp:ListItem Value="True">Decimal (6.5)</asp:ListItem>
                                </asp:DropDownList>
                          </div>
												</span>
										<div class="clear"></div>
									</div>


                        <div class="field-holder">
											<label>Manager</label>
												<span class="left">
													<div class="right">
                                <asp:TextBox ID="txtManagerName" MaxLength="50" runat="server" Enabled="false"></asp:TextBox>
                           </div>
												</span>
										<div class="clear"></div>
									</div>



                            <asp:Panel ID="pnlCust1" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblCust1" runat="server" Text="Custom Field 1"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtCust1" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlCust2" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblCust2" runat="server" Text="Custom Field 2"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtCust2" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlCust3" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblCust3" runat="server" Text="Custom Field 3"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtCust3" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlCust4" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblCust4" runat="server" Text="Custom Field 4"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtCust4" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlCust5" Visible="false" runat="server">
                                <tr>
                                    <td style="height: 26px; text-align: right;" align="right">
                                        <asp:Label ID="lblCust5" runat="server" Text="Custom Field 5"></asp:Label>:</td>
                                    <td style="height: 26px; text-align: left;">
                                        <asp:TextBox ID="txtCust5" MaxLength="50" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
 
</div> <!-- end of formholder -->
									</div> <!-- end of optional -->


                <div>
<asp:Button ID="btnEdit" runat="server" Text="Edit User" OnClick="btnEdit_Click" CssClass="edit-btn" />&nbsp;&nbsp;&nbsp;&nbsp;
               
               </div>
										<div class="clear"></div>


             <!-- Validation -->
                <asp:Panel ID="pnlValidation" Visible="true" runat="server">
                    <center>
                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtUsername" ID="valUsername" runat="server" ErrorMessage="Username is required<br>"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtFirstName" ID="valFirstName" runat="server" ErrorMessage="First Name is required<br>"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtLastName" ID="valLastName" runat="server" ErrorMessage="Last Name is required<br>"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtEmail" ID="valEmail" runat="server" ErrorMessage="Email Address is required<br>"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust1" ID="valMandCust1" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust2" ID="valMandCust2" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust3" ID="valMandCust3" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust4" ID="valMandCust4" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" Enabled="false" ControlToValidate="txtMandCust5" ID="valMandCust5" runat="server" ErrorMessage=""></asp:RequiredFieldValidator>
                    </center>
                </asp:Panel>
            <!-- Validation -->
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

