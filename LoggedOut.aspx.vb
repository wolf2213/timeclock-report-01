
Partial Class LoggedOut
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.Cookies("ClockitinApp") Is Nothing Then
            Server.Transfer("Login.aspx?client=" & Response.Cookies("ClockitinApp")("Subdomain"))
        End If
    End Sub
End Class
