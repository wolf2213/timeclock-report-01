<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SettingsContact.aspx.vb" Inherits="SettingsContact" title="TimeClockWizard - Business Contact Information" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="main-content">
								<div class="page-heading">
									<h1>Business Contact Info</h1>
								</div>
								
									

            <asp:Panel ID="pnlUpdateContactInfo" runat="server" Visible="false" style="width:500px; background-color:#FF9999; border:3px solid #FF0000; text-align:center; margin-bottom:20px; padding-top:10px; padding-bottom:10px;">
                Please make sure your contact information is up-to-date.<br />
                Correct any errors, and click "Update Information" to confirm.<br />
                <strong>Please click "Update Information" even if you are not making any changes.</strong>
            </asp:Panel>


            <div class="box1-info">
										<h4>company information</h4>

                        <div class="form-holder" style="">

											<div class="field-holder">
											<label>Company Name:</label>
												<span class="left">
													<div class="right">
                            <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                                                        </div>
												</span>
                            <asp:RequiredFieldValidator ID="rfvCompanyName" runat="server" ControlToValidate="txtCompanyName" ErrorMessage="* You must provide a company name"></asp:RequiredFieldValidator>
                        
												<div class="clear"></div>
											</div>

                            <div class="field-holder">
											<label>Address Line 1:</label>
												<span class="left">
													<div class="right">
                            <asp:TextBox ID="txtAddress1" MaxLength="50" runat="server"></asp:TextBox>
                        </div>
												</span>
												<div class="clear"></div>
											</div>
                    <div class="field-holder">
											<label>Address Line 2:</label>
												<span class="left">
													<div class="right">
                            <asp:TextBox ID="txtAddress2" MaxLength="50" runat="server"></asp:TextBox>
                        </div>
												</span>
												<div class="clear"></div>
											</div>

                        <div class="field-holder">
											<label>City, State, Zip:</label>
												<span class="left" style="margin-left:4px;">
													<div class="right">
                            <asp:TextBox ID="txtCity" MaxLength="25"  Width="70px" runat="server"></asp:TextBox>
                            <asp:TextBox ID="txtState" MaxLength="2" Width="20px" runat="server"></asp:TextBox>
                            <asp:TextBox ID="txtZip" MaxLength="5" Width="40px" runat="server"></asp:TextBox>
                        </div>
												</span>
											<div class="clear"></div>
            </div>
										</div> <!-- end of formholder -->
									</div> <!-- box1-info -->


            <div class="box2-info">
										<h4>primary company information</h4>
										<div class="form-holder" style="">
											<div class="field-holder">
											<label>First Name:</label>
												<span class="left">
													<div class="right"><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                        </div>
												</span>
												<div class="clear"></div>
											</div>
                            <div class="field-holder">
											<label>Last Name:</label>
												<span class="left">
													<div class="right">
                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                        </div>
												</span>
												<div class="clear"></div>
											</div>
                    <div class="field-holder">
											<label>Email Address:</label>
												<span class="left">
													<div class="right">
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                                        </div>
												</span>
                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="* You must provide an email address"></asp:RequiredFieldValidator>
                        
												<div class="clear"></div>
											</div>
                    <div class="field-holder">
											<label>Phone Number:</label>
												<span class="left">
													<div class="right">
                            <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                                                        </div>
												</span>
                            <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtEmail" ErrorMessage="* You must provide a phone number"></asp:RequiredFieldValidator>
                        
												<div class="clear"></div>
											</div>
            </div> <!-- end of formholder -->
									</div> <!-- box2-info -->
            <div><asp:Button ID="btnSave" runat="server" Text="Update Information" OnClick="btnSave_Click" CssClass="update-info-btn" />
            <br /><br />
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></div>

                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

