<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TrialExpired.aspx.vb" Inherits="TrialExpired" title="TimeClockWizard - Trial Expired" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div style="font-size:16pt; text-align:center;"><strong>Your free trial has expired</strong></div>
    <br />  
    <div style="font-size:9pt; text-align:center;">Please have a Clockitin Account Holder at your
        organization login and activate this account in order to continue using the service.</div>
    
</asp:Content>

