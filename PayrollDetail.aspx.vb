
Partial Class PayrollDetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If
        Dim pnlMasterTabPayroll As Panel = Master.FindControl("pnlTabPayroll")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Payroll As Panel = Master.FindControl("pnlTab2Payroll")
        Dim pnlMasterTab2ViewPayroll As Panel = Master.FindControl("pnlTab2ViewPayroll")
        pnlMasterTabPayroll.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Payroll
        pnlMasterTab2Payroll.CssClass = "tab2 tab2On"
        pnlMasterTab2ViewPayroll.CssClass = "tab2 tab2On"
        If Not Permissions.PayrollView(Session("User_ID")) Then
            Server.Transfer("PayrollEmployeeDetail.aspx?pid=" & Request.QueryString("id") & "&dates=" & Request.QueryString("dates"))
        End If
        lnkDetailView.NavigateUrl = "PayrollEmployeeDetail.aspx?pid=" & Request.QueryString("id") & "&dates=" & Request.QueryString("dates")
        If Not IsPostBack Then
            lblDates.Text = Request.QueryString("dates")
            CreatePayrollDetail()
        End If
    End Sub

    Protected Sub rptPayrollDetail_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblUser As Label = e.Item.FindControl("lblUser")
            Dim lblRegularHours As Label = e.Item.FindControl("lblRegularHours")
            Dim lblRegularPay As Label = e.Item.FindControl("lblRegularPay")
            Dim lblOTHours As Label = e.Item.FindControl("lblOTHours")
            Dim lblOTPay As Label = e.Item.FindControl("lblOTPay")
            Dim lblOtherPay As Label = e.Item.FindControl("lblOtherPay")
            Dim lblGrossPay As Label = e.Item.FindControl("lblGrossPay")
            Dim btnMoreDetail As ImageButton = e.Item.FindControl("btnMoreDetail")
            Dim dblRegularPayment As Double = e.Item.DataItem("Wage") * (e.Item.DataItem("RegularMinutes") / 60)
            Dim dblOTPayment As Double = e.Item.DataItem("OTWage") * (e.Item.DataItem("OTMinutes") / 60)

            lblUser.Text = UserInfo.NameFromID(e.Item.DataItem("User_ID"), 0)
            lblRegularHours.Text = TimeRecords.FormatMinutes(e.Item.DataItem("RegularMinutes"), Session("User_ID"), False)
            lblOTHours.Text = TimeRecords.FormatMinutes(e.Item.DataItem("OTMinutes"), Session("User_ID"), False)
            If e.Item.DataItem("CurrencyType") = "1" Then 'Indian rupees
                lblRegularPay.Text = "Rs." & Strings.FormatCurrency(dblRegularPayment, 2, TriState.UseDefault, TriState.False, TriState.True).Substring(1)
                lblOTPay.Text = "Rs." & Strings.FormatCurrency(dblOTPayment, 2, TriState.UseDefault, TriState.False, TriState.True).Substring(1)
                lblOtherPay.Text = "Rs." & Strings.FormatCurrency(e.Item.DataItem("TotalOtherPay"), 2, TriState.UseDefault, TriState.False, TriState.True).Substring(1)
                lblGrossPay.Text = "Rs." & Strings.FormatCurrency(dblRegularPayment + dblOTPayment + e.Item.DataItem("TotalOtherPay"), 2, TriState.UseDefault, TriState.False, TriState.True).Substring(1)
            Else
                lblRegularPay.Text = Strings.FormatCurrency(dblRegularPayment, 2, TriState.UseDefault, TriState.False, TriState.True)
                lblOTPay.Text = Strings.FormatCurrency(dblOTPayment, 2, TriState.UseDefault, TriState.False, TriState.True)
                lblOtherPay.Text = Strings.FormatCurrency(e.Item.DataItem("TotalOtherPay"), 2, TriState.UseDefault, TriState.False, TriState.True)
                lblGrossPay.Text = Strings.FormatCurrency(dblRegularPayment + dblOTPayment + e.Item.DataItem("TotalOtherPay"), 2, TriState.UseDefault, TriState.False, TriState.True)
            End If

            btnMoreDetail.CommandArgument = e.Item.DataItem("User_ID")
            btnMoreDetail.CommandName = "EmployeeDetail"
        End If
    End Sub

    Protected Sub rptPayrollDetail_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim intUserId As Int64 = e.CommandArgument

        Select Case e.CommandName
            Case "EmployeeDetail"
                Response.Redirect("PayrollEmployeeDetail.aspx?uid=" & intUserId & "&pid=" & Request.QueryString("id") & "&dates=" & Request.QueryString("dates"))
        End Select

    End Sub

    Protected Sub CreatePayrollDetail()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT PayrollDetail.*, Users.CurrencyType FROM PayrollDetail, Users WHERE PayrollDetail.User_ID = Users.User_ID AND PayrollDetail.Client_ID = @Client_ID AND PayrollDetail.Payroll_ID = @Payroll_ID ORDER BY Users.LastName, Users.FirstName"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@Payroll_ID", Request.QueryString("id"))
        objDR = objCommand.ExecuteReader()

        rptPayrollDetail.DataSource = objDR
        rptPayrollDetail.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub
End Class
