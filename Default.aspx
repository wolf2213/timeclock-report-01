﻿<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" title="TimeClockWizard - My Dashboard" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Literal ID="litAlternate" runat="server" Text="Normal" Visible="false"></asp:Literal>
    <script language="javascript" type="text/javascript">
        var preid = "ctl00_MainContent_";
        var ClkInMin;
        var Endmin;        

       function btnClockIn(e,t)
       {
           var d = new Date;
           ClkInMin = d.getMinutes();
       }

       function btnClockOut(e, t) {
            var d1 = new Date;
           var CloclOutMin = d1.getMinutes();
           var timeDiff = (CloclOutMin - Endmin) * 60;
           var Jvalue = document.getElementById("ctl00_MainContent_TextBox1").value;           
           if (timeDiff <= Jvalue) {
               if (premsgbox("Are you sure you want to clock out?", "btnClockOut")) {
                   return true;
               }
               else {
                   return false;
               }
           }
       }
	   
       function btnBreakIn(e, t) {
           var d1 = new Date;
           var BrKIn = d1.getMinutes();
           var timeDiff = (BrKIn - ClkInMin)*60;
           var Jvalue = document.getElementById("ctl00_MainContent_TextBox1").value;                     
           if (timeDiff <= Jvalue) {
               if (premsgbox("Are you sure you want to start break?", "btnBreakIn")) {
                   return true;
               }
               else {
                   return false;
               }
           }
           
       }
	   
       function btnBreakOut(e, t) {
           var d = new Date;
           Endmin = d.getMinutes();
       }

       function premsgbox(msg, cntrl) {
           try {
               document.getElementById(preid + cntrl).focus();               
               var r = confirm(msg);
               if (r == true) {
                   return true;
               }
               else {
                   return false;
               }
           } catch (error) { }
       }

    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div class="main-content" style="width:1050px;">
 <div style="margin-left: 25px; float: left;"> 

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

<div style="width: 300px; text-align: center; background: #004a71; padding: 15px 0; margin-bottom: 20px;">
									
                        <asp:Label ID="lblNoClockPoint" runat="server" Text="This computer is not a ClockPoint<br />" Visible="false" ForeColor="White"></asp:Label>
                        <asp:ImageButton ID="btnClockIn"  runat="server" OnClick=" btnClockIn_Click" OnClientClick = "javascript:return btnClockIn(event,this);" ImageUrl="~/images/clockin-btn.png" />

                        <asp:ImageButton ID="btnClockOut" runat="server" OnClick=" btnClockOut_Click" OnClientClick ="javascript:return btnClockOut(event,this);" ImageUrl="~/images/clockout-btn.png" />
                        
                        <asp:ImageButton ID="btnBreakIn" runat="server" Visible="false" OnClick="btnBreakIn_Click" OnClientClick ="javascript:return btnBreakIn(event,this);" ImageUrl="~/images/startbreak-btn.png"  />

                        <asp:ImageButton ID="btnBreakOut" runat="server" Visible="false" OnClick="btnBreakOut_Click" OnClientClick ="javascript:return btnBreakOut(event,this);" ImageUrl="~/images/endbreak-btn.png" />
                       


                        <br />
                            <asp:UpdateProgress ID="prgClockInOut" runat="server">
                                <ProgressTemplate>
                                    Processing...
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:Label ID="lblClockInMessage" runat="server" Text="Message" ForeColor="White"></asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
    
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div style="width: 300px;">
									<h4>My Alerts</h4>

                <div style="border: 1px solid #c6ced0; padding-top: 30px;">

    <div style="margin-left:30px;">
                    <asp:Label ID="lblAlertDashboard" runat="server" Text="This is a custom Dashboard alert<br /><br />"></asp:Label>
                    <asp:ImageButton ID="btnAlertErrors" runat="server" ImageUrl="~/images/timesheet_error.png" ImageAlign="AbsMiddle" AlternateText="Possible erroneous records" ToolTip="header=[Possible erroneous records] body=[] cssheader=[boHeader bo200pxWide]" />&nbsp;<asp:Label ID="lblAlertErrorsCount" runat="server" Text="0"></asp:Label><asp:Label ID="lblAlertErrorsLabel" runat="server" Text=" possible erroneous time"></asp:Label><asp:Label ID="lblAlertErrorsSuffix" runat="server" Text="s"></asp:Label>

<br />
                    <asp:ImageButton ID="btnAlertTimesheet" runat="server" ImageUrl="~/images/timesheet_edited.png" ImageAlign="AbsMiddle" AlternateText="Pending timesheet requests" ToolTip="header=[Pending timesheet requests] body=[] cssheader=[boHeader bo200pxWide]" />&nbsp;<asp:Label ID="lblAlertTimesheetCount" runat="server" Text="0"></asp:Label><asp:Label ID="lblAlertTimesheetLabel" runat="server" Text=" pending timesheet request"></asp:Label><asp:Label ID="lblAlertTimesheetSuffix" runat="server" Text="s"></asp:Label>
                    <br />
                    <asp:ImageButton ID="btnAlertAbsences" runat="server" ImageUrl="~/images/timesheet_absence.png" ImageAlign="AbsMiddle" AlternateText="Pending absence requests" ToolTip="header=[Pending absence requests] body=[] cssheader=[boHeader bo200pxWide]" />&nbsp;<asp:Label ID="lblAlertAbsencesCount" runat="server" Text="0"></asp:Label><asp:Label ID="lblAlertAbsencesLabel" runat="server" Text=" pending absence request"></asp:Label><asp:Label ID="lblAlertAbsencesSuffix" runat="server" Text="s"></asp:Label>

        <br /><br />

        </div>

                </div>

                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
      <br />
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <div style="width: 300px;">
									<h4><asp:Label ID="lblScheduleTitle" runat="server" Text="Label"></asp:Label></h4>

<div style="border: 1px solid #c6ced0; text-align: center; padding-top: 30px;">

    <div style="text-align:left;">

        <asp:Label ID="lblScheduleError" runat="server" Text="Label"></asp:Label>
                    <asp:Panel ID="pnlSchedule" runat="server">            
                       <table width="300px" cellpadding="5px" cellspacing="0px">
                           <tr><td align="right" valign="top" style="font-size:18px;"><b>Sunday:&nbsp;</b></td><td align="left" valign="top" style="font-size:15px;"><asp:Label ID="lblSchSun" runat="server" Text="Label"></asp:Label></td></tr>
                           <tr><td align="right" valign="top" style="font-size:18px;"><b>Monday:&nbsp;</b></td><td align="left" valign="top" style="font-size:15px;"><asp:Label ID="lblSchMon" runat="server" Text="Label"></asp:Label></td></tr>
                           <tr><td align="right" valign="top" style="font-size:18px;"><b>Tuesday:&nbsp;</b></td><td align="left" valign="top" style="font-size:15px;"><asp:Label ID="lblSchTue" runat="server" Text="Label"></asp:Label></td></tr>
                           <tr><td align="right" valign="top" style="font-size:18px;"><b>Wednesday:&nbsp;</b></td><td align="left" valign="top" style="font-size:15px;"><asp:Label ID="lblSchWed" runat="server" Text="Label"></asp:Label></td></tr>
                           <tr><td align="right" valign="top" style="font-size:18px;"><b>Thursday:&nbsp;</b></td><td align="left" valign="top" style="font-size:15px;"><asp:Label ID="lblSchThu" runat="server" Text="Label"></asp:Label></td></tr>
                           <tr><td align="right" valign="top" style="font-size:18px;"><b>Friday:&nbsp;</b></td><td align="left" valign="top" style="font-size:15px;"><asp:Label ID="lblSchFri" runat="server" Text="Label"></asp:Label></td></tr>
                           <tr><td align="right" valign="top" style="font-size:18px;"><b>Saturday:&nbsp;</b></td><td align="left" valign="top" style="font-size:15px;"><asp:Label ID="lblSchSat" runat="server" Text="Label"></asp:Label></td></tr>
                       </table>
                        <br />
            </asp:Panel>

        </div>
    </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

</div>



                <asp:UpdatePanel ID="upnlUserTimeRecords" runat="server">
                    <ContentTemplate>
                         <div style="margin-left: 350px; width: 550px;">

                        <asp:Repeater ID="rptUserTimeRecords" runat="server" OnItemDataBound="rptUserRecords_ItemDataBound">
                            <HeaderTemplate>

                                    <ul class="time-Date" style="width: 170px;">
											<li class="column-head column-title">Date</li>
                                        </ul>

                                    <ul class="time-Start" style="width: 120px;">
											<li class="column-head column-title">Start</li>
                                        </ul>

                                     <ul class="time-End" style="width: 120px;">
											<li class="column-head column-title">End</li>
                                        </ul>

                                     <ul class="time-Duration" style="width: 120px;">
											<li class="column-head column-title">Duration</li>
                                        </ul>

                            </HeaderTemplate>
                            <ItemTemplate>
                                    <asp:Literal ID="litRepeater" runat="server" Text="1" Visible="false"></asp:Literal>
                                    <asp:Panel ID="pnlTimeRow" runat="server">
                                        <asp:Panel ID="pnlTimeView" runat="server">

<ul class="time-Date" style="width: 170px;">
											<li><asp:Image ID="imgError" runat="server" ImageUrl="~/images/timesheet_error.png" ImageAlign="AbsMiddle" AlternateText="Possible error" ToolTip="header=[Possible error] body=[]" Visible="false" /><asp:Image ID="imgRecordType" runat="server" ImageUrl="~/images/timesheet_clockinout.png" ImageAlign="AbsMiddle" AlternateText="Clock In/Out" />&nbsp;<asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label></li>
                                        </ul>

<ul class="time-Start" style="width: 120px;">
											<li><asp:Image ID="imgStartLocation" runat="server" ImageAlign="AbsMiddle" />&nbsp;<asp:Label ID="lblStartTime" runat="server" Text="StartTime"></asp:Label></li>
                                        </ul>

<ul class="time-End" style="width: 120px;">
											<li><asp:Image ID="imgEndLocation" runat="server" ImageAlign="AbsMiddle" />&nbsp;<asp:Label ID="lblEndTime" runat="server" Text="EndTime"></asp:Label></li>
                                        </ul>

 <ul class="time-Duration" style="width: 120px;">
											<li><asp:Image ID="imgVerified" runat="server" ImageUrl="~/images/timesheet_unverified.png" ImageAlign="AbsMiddle" AlternateText="Unverified" />&nbsp;<asp:Label ID="lblDuration" runat="server" Text="Duration"></asp:Label></li>
                                        </ul>

                                        </asp:Panel>
                                    </asp:Panel>
                            </ItemTemplate>
                            <FooterTemplate>
                                    <asp:Literal ID="litRepeater" runat="server" Text="1" Visible="false"></asp:Literal>
                                    <ul class="time-Date" style="width: 120px;"><li>&nbsp;</li></ul>
                                    <ul class="time-Start" style="width: 120px;"><li>&nbsp;</li></ul>
                                    <ul class="time-End" style="width: 120px;"><li>&nbsp;</li></ul>
                                    <ul class="time-Duration" style="width: 120px;"><li><asp:Label ID="lblTotalDuration" runat="server" Text="Duration"></asp:Label> Total</li></ul>
                                       
                              
                            </FooterTemplate>
                            <SeparatorTemplate>
                              
                            </SeparatorTemplate>
                        </asp:Repeater>
                                        
     </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
     
    <asp:TextBox ID="TextBox1" runat="server" style ="display :none" Text = '<%#Eval("vals")%>'></asp:TextBox>  
        </div>      
</asp:Content>