
Partial Class MobileAccess_iPhone_DoLogin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT User_ID, Manager, Password, FirstName, LastName, MobileAccess FROM Users WHERE Username = @Username AND Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@Username", Request.Form("username"))
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            If Crypto.VerifyHash(Request.Form("password"), objDR.Item("Password")) Then
                If objDR("MobileAccess") Then
                    Session("FirstName") = objDR.Item("FirstName")
                    Session("LastName") = objDR.Item("LastName")
                    Session("User_ID") = objDR.Item("User_ID").ToString
                    Session("Manager") = objDR.Item("Manager")

                    Response.Redirect("Home.aspx")
                Else
                    Response.Redirect("Login.aspx?error=2")
                End If
            Else
                Response.Redirect("Login.aspx?error=1")
            End If
            Else
                Response.Redirect("Login.aspx?error=1")
            End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub
End Class
