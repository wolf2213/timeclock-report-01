
Partial Class MobileAccess_iPhone_Home
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("User_ID") = "" Then
            Response.Redirect("AccessHelp.aspx")
        End If

        Dim sbTest As New StringBuilder

        If Request.QueryString("int") = "1" Then
            sbTest.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>")
            sbTest.Append("<html xmlns='http://www.w3.org/1999/xhtml'>")
            sbTest.Append("<head>")
            sbTest.Append("<title>Clockitin</title>")
            sbTest.Append("<meta name='viewport' content='width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;'/>")
            sbTest.Append("<style type='text/css' media='screen'>@import 'iui/iui.css';</style>")
            sbTest.Append("<style type='text/css' media='screen'>@import 'iui/iui-custom.css';</style>")
            sbTest.Append("<script type='application/x-javascript' src='iui/iui.js'></script>")
            sbTest.Append("</head>")
            sbTest.Append("<body>")
            sbTest.Append("<div class='toolbar'>")
            sbTest.Append("<h1 id='pageTitle'></h1>")
        Else
            sbTest.Append("<div class='toolbar'>")
        End If

        sbTest.Append("<a class='button' href='Logout.aspx?client=")
        sbTest.Append(Session("Subdomain"))
        sbTest.Append("' target='_self'>Logout</a>")
        sbTest.Append("</div>")
        sbTest.Append("<div id='home' class='panel' title='Clockitin' selected='true'>")
        sbTest.Append("<h2><img src='images/mobileaccess.png' align='absmiddle'>&nbsp;Mobile Access Home</h2>")
        sbTest.Append("<fieldset>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<a href='Clock.aspx'>&nbsp;&nbsp;<img src='images/clock.png' align='absmiddle' class='rowImage'>&nbsp;&nbsp;Clock In/Out</a>")
        sbTest.Append("</div>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<a href='Mileage.aspx'>&nbsp;&nbsp;<img src='images/mileage.png' align='absmiddle' class='rowImage'>&nbsp;&nbsp;Add Mileage</a>")
        sbTest.Append("</div>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<a href='Timesheet.aspx'>&nbsp;&nbsp;<img src='images/timesheet.png' align='absmiddle' class='rowImage'>&nbsp;&nbsp;My Current Timesheet</a>")
        sbTest.Append("</div>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<a href='Schedule.aspx'>&nbsp;&nbsp;<img src='images/schedule.png' align='absmiddle' class='rowImage'>&nbsp;&nbsp;My Schedule</a>")
        sbTest.Append("</div>")
        sbTest.Append("</fieldset>")
        sbTest.Append("</div>")

        If Request.QueryString("int") = "1" Then
            sbTest.Append("</body>")
            sbTest.Append("</html>")
        End If

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub
End Class
