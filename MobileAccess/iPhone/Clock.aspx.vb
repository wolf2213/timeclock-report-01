
Partial Class MobileAccess_iPhone_Clock
    Inherits System.Web.UI.Page
    Dim ClockMessage As String
    Dim boolShowClockIn As Boolean = False
    Dim boolShowClockOut As Boolean = False
    Dim boolShowBreakIn As Boolean = False
    Dim boolShowBreakOut As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("User_ID") = "" Then
            Response.Redirect("AccessHelp.aspx")
        End If

        Select Case Request.QueryString("action")
            Case "1"
                ClockIn()
            Case "2"
                ClockOut()
            Case "3"
                BreakIn()
            Case "4"
                BreakOut()
            Case Else
                InitializePage()
        End Select

        Dim sbTest As New StringBuilder
        sbTest.Append("<div class='toolbar'>")
        sbTest.Append("<a class='button backButton' href='Home.aspx?int=1' target='_self'>Home</a>")
        sbTest.Append("</div>")
        sbTest.Append("<div id='clock' class='panel' title='Clockitin' selected='true'>")
        sbTest.Append("<h2><img src='images/clock.png' align='absmiddle'>&nbsp;")
        sbTest.Append(ClockMessage)
        sbTest.Append("</h2>")
        sbTest.Append("<fieldset>")
        If boolShowClockIn Then
            sbTest.Append("<div class='row'>")
            sbTest.Append("<a href='Clock.aspx?action=1'>&nbsp;&nbsp;Clock In</a>")
            sbTest.Append("</div>")
        End If
        If boolShowClockOut Then
            sbTest.Append("<div class='row'>")
            sbTest.Append("<a href='Clock.aspx?action=2'>&nbsp;&nbsp;Clock Out</a>")
            sbTest.Append("</div>")
        End If
        If boolShowBreakOut Then
            sbTest.Append("<div class='row'>")
            sbTest.Append("<a href='Clock.aspx?action=3'>&nbsp;&nbsp;Start Break</a>")
            sbTest.Append("</div>")
        End If
        If boolShowBreakIn Then
            sbTest.Append("<div class='row'>")
            sbTest.Append("<a href='Clock.aspx?action=4'>&nbsp;&nbsp;End Break</a>")
            sbTest.Append("</div>")
        End If
        sbTest.Append("</fieldset>")
        sbTest.Append("</div>")

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub

    Protected Sub ClockIn()
        Dim intClockStatus = Clock.ClockIn(Session("Client_ID"), Session("User_ID"), Session("Manager"), Request.UserHostAddress, True)

        Select Case intClockStatus
            Case 1
                'success
                'reload the page
                InitializePage()
            Case 2
                'trying to clock in when the person is already clocked in
                ClockMessage = "Already Clocked In"
            Case 3
                'userStatus error
                ClockMessage = "User Status Error"
        End Select
    End Sub

    Protected Sub ClockOut()
        Dim intClockStatus = Clock.ClockOut(Session("Client_ID"), Session("User_ID"), Session("Manager"), Request.UserHostAddress, Session("ClockedIn"), True)

        Select Case intClockStatus
            Case 1
                'success
                'reload page
                InitializePage()
            Case 2
                'record deleted
                'reload page
                InitializePage()
                ClockMessage = "<strong style='color:red;'>Record deleted:</strong> Clocked in for 0 minutes."
            Case 3
                ClockMessage = "Already Clocked Out"
            Case 4
                ClockMessage = "User Status Error"
        End Select
    End Sub

    Protected Sub BreakIn()
        Dim intClockStatus = Clock.BreakIn(Session("Client_ID"), Session("User_ID"), Session("Manager"), Request.UserHostAddress, True)

        Select Case intClockStatus
            Case 1
                'clocked in to the break successfully, reload the page
                InitializePage()
            Case Else
                ClockMessage = "Break In Error. Status = " & intClockStatus
        End Select
    End Sub

    Protected Sub BreakOut()
        Dim intClockStatus = Clock.BreakOut(Session("Client_ID"), Session("User_ID"), Session("Manager"), Request.UserHostAddress, Session("dtClockedIn"), True)

        Select Case intClockStatus
            Case 1
                'success
                'reload page
                InitializePage()
            Case 2
                'record deleted
                'reload page
                InitializePage()
                ClockMessage = "<strong style='color:red;'>Break deleted:</strong> Clocked in for 0 minutes."
            Case 3
                ClockMessage = "Not On Break"
            Case 4
                ClockMessage = "User Status Error"
        End Select
    End Sub

    Protected Sub InitializePage()
        Dim intUserStatus As String = UserInfo.UserStatus(Session("User_ID"))

        If IsNumeric(intUserStatus) Then
            If intUserStatus = 1 Then
                'clocked out
                ClockMessage = "Currently clocked out"

                boolShowClockIn = True
                boolShowClockOut = False
                boolShowBreakIn = False
                boolShowBreakOut = False
            ElseIf intUserStatus = 2 Then
                'clocked in
                Dim objDR As System.Data.SqlClient.SqlDataReader
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT StartTime FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL"
                objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))

                objDR = objCommand.ExecuteReader()
                objDR.Read()

                Session("ClockedIn") = objDR("StartTime")

                ClockMessage = "Clocked in for <b>" & TimeRecords.FormatMinutes(TimeRecords.TimeDifference(objDR("StartTime"), Clock.GetNow()), Session("User_ID"), True) & "</b>"

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                boolShowClockIn = False
                boolShowClockOut = True
                boolShowBreakIn = False
                boolShowBreakOut = True
            ElseIf intUserStatus = 3 Then
                'on break
                Dim objDR As System.Data.SqlClient.SqlDataReader
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "SELECT StartTime FROM TimeRecords WHERE User_ID = @User_ID AND EndTime IS NULL AND Type <> 1"
                objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))

                objDR = objCommand.ExecuteReader()
                objDR.Read()

                Session("ClockedIn") = objDR("StartTime")

                ClockMessage = "On Break for <b>" & TimeRecords.FormatMinutes(TimeRecords.TimeDifference(objDR("StartTime"), Clock.GetNow()), Session("User_ID"), True) & "</b>"

                objDR = Nothing
                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                boolShowClockIn = False
                boolShowClockOut = True
                boolShowBreakIn = True
                boolShowBreakOut = False
            End If
        Else
            'there was an error, display it
            ClockMessage = intUserStatus
        End If
    End Sub
End Class
