
Partial Class MobileAccess_iPhone_Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("client") <> "" Then
            GetClientInfo()
        ElseIf Session("Client_ID") Is Nothing Then
            Response.Redirect("AccessHelp.aspx?full=1")
        End If

        CheckDelinquency()
        LoginScreen()
    End Sub

    Protected Sub GetClientInfo()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Client_ID, Trial, NextBillingDate FROM Clients WHERE Subdomain = @Subdomain"
        objCommand.Parameters.AddWithValue("@Subdomain", Request.QueryString("client"))

        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            Session("Client_ID") = objDR("Client_ID")
            Session("Subdomain") = Request.QueryString("client")

            If objDR("Trial") Then
                Dim intTrialDaysLeft As Integer = DateDiff(DateInterval.Day, CDate(Now().Date & " 12:00 AM"), objDR("NextBillingDate"))
                If intTrialDaysLeft < 0 Then
                    Session("TrialExpired") = True
                End If
            End If
        Else
            Response.Redirect("AccessHelp.aspx?full=1")
        End If
    End Sub

    Protected Sub CheckDelinquency()
        If Chase.Delinquency(Session("Client_ID")) <> 8 Then
            Session("Delinquent") = True
        Else
            Session("Delinquent") = False
        End If
    End Sub

    Protected Sub LoginScreen()
        Dim sbTest As New StringBuilder

        If Request.QueryString("error") = "" Then
            sbTest.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>")
            sbTest.Append("<html xmlns='http://www.w3.org/1999/xhtml' >")
            sbTest.Append("<head>")
            sbTest.Append("<title>TimeClockWizard</title>")
            sbTest.Append("<meta name='viewport' content='width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;'/>")
            sbTest.Append("<link rel='apple-touch-icon' href='images/apple-touch-icon.png'/>")
            sbTest.Append("<style type='text/css' media='screen'>@import 'iui/iui.css';</style>")
            sbTest.Append("<style type='text/css' media='screen'>@import 'iui/iui-custom.css';</style>")
            sbTest.Append("<script type='application/x-javascript' src='iui/iui.js'></script>")
            sbTest.Append("</head>")
            sbTest.Append("<body>")
            sbTest.Append("<div class='toolbar'>")
            sbTest.Append("<h1 id='pageTitle'></h1>")
            sbTest.Append("</div>")
        End If

        sbTest.Append("<form action='DoLogin.aspx' method='post' id='login' class='panel' title='TimeClockWizard' selected='true'>")

        If Request.QueryString("error") = "" Then
            If Not Session("TrialExpired") And Not Session("Delinquent") Then
                sbTest.Append("<h2>Login</h2>")
            ElseIf Session("TrialExpired") Then
                sbTest.Append("<h2 style='color:red;'>Your free trial has expired</h2>")
            ElseIf Session("Delinquent") Then
                sbTest.Append("<h2 style='color:red;'>Your account is delinquent</h2>")
            End If
        ElseIf Request.QueryString("error") = "1" Then
            sbTest.Append("<h2 style='color:red;'>Username/password not found</h2>")
        ElseIf Request.QueryString("error") = "2" Then
            sbTest.Append("<h2 style='color:red;'>Mobile Access is not enabled for your user account</h2>")
        End If

        If Not Session("TrialExpired") And Not Session("Delinquent") Then
            sbTest.Append("<fieldset>")
            sbTest.Append("<div class='row'>")
            sbTest.Append("<label>Username</label>")
            sbTest.Append("<input type='text' name='username' value=''/>")
            sbTest.Append("</div>")
            sbTest.Append("<div class='row'>")
            sbTest.Append("<label>Password</label>")
            sbTest.Append("<input type='password' name='password' value=''/>")
            sbTest.Append("</div>")
            sbTest.Append("</fieldset>")
            sbTest.Append("<fieldset>")
            sbTest.Append("<div class='row'>")
            sbTest.Append("<label>Remember Me</label>")
            sbTest.Append("<div class='toggle' onclick=''><span class='thumb'></span><span class='toggleOn'>ON</span><span class='toggleOff'>OFF</span></div>")
            sbTest.Append("</div>")
            sbTest.Append("</fieldset>")
            sbTest.Append("<a href='#' type='submit' class='whiteButton'>Login</a>")
        End If

        sbTest.Append("</form>")


        If Request.QueryString("error") = "" Then
            sbTest.Append("</body>")
            sbTest.Append("</html>")
        End If

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub
End Class
