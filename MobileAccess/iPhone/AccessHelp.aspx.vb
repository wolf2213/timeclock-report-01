
Partial Class MobileAccess_iPhone_AccessHelp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sbTest As New StringBuilder

        If Request.QueryString("full") = "1" Then
            sbTest.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>")
            sbTest.Append("<html xmlns='http://www.w3.org/1999/xhtml' >")
            sbTest.Append("<head>")
            sbTest.Append("<title>Clockitin</title>")
            sbTest.Append("<meta name='viewport' content='width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;'/>")
            sbTest.Append("<link rel='apple-touch-icon' href='images/apple-touch-icon.png'/>")
            sbTest.Append("<style type='text/css' media='screen'>@import 'iui/iui.css';</style>")
            sbTest.Append("<style type='text/css' media='screen'>@import 'iui/iui-custom.css';</style>")
            sbTest.Append("<script type='application/x-javascript' src='iui/iui.js'></script>")
            sbTest.Append("</head>")
            sbTest.Append("<body>")
        End If

        sbTest.Append("<div class='toolbar'>")
        sbTest.Append("<h1 id='pageTitle'></h1>")
        sbTest.Append("</div>")
        sbTest.Append("<div id='help' class='panel' title='Clockitin' selected='true'>")
        sbTest.Append("<h2>An error has occured</h2>")
        sbTest.Append("If you were trying to login, you may have used an invalid URL.  Please go to your company's main URL (<i>subdomain</i>.TimeClockWizard.com) to login.")
        sbTest.Append("<br><br>")
        sbTest.Append("If you were trying to use the application, your session may have timed out.  This is for your security.  Please login again.")
        sbTest.Append("</div>")

        If Request.QueryString("full") = "1" Then
            sbTest.Append("</body>")
            sbTest.Append("</html>")
        End If

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub
End Class
