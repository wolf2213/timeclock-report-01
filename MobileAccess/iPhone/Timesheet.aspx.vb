
Partial Class MobileAccess_iPhone_Timesheet
    Inherits System.Web.UI.Page
    Dim sbTest As New StringBuilder
    Dim intTotalMinutes As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("User_ID") = "" Then
            Response.Redirect("AccessHelp.aspx")
        End If

        sbTest.Append("<div class='toolbar'>")
        sbTest.Append("<a class='button backButton' href='Home.aspx?int=1' target='_self'>Home</a>")
        sbTest.Append("</div>")
        sbTest.Append("<div id='timesheet' class='panel' title='TimeClockWizard' selected='true'>")
        sbTest.Append("<h2><img src='images/timesheet.png' align='absmiddle'>&nbsp;My Current Timesheet</h2>")
        sbTest.Append("<table style='width:100%; font-size:14px;' cellpadding='0' cellspacing='0'>")
        sbTest.Append("<tr style='font-weight:bold;'><td>&nbsp;</td><td>Start</td><td>End</td><td>&nbsp;</td></tr>")
        BuildTimesheet()
        sbTest.Append("<tr style='font-weight:bold;'><td>&nbsp;</td><td>&nbsp;</td><td style='text-align:right;'>Total&nbsp;</td><td>")
        sbTest.Append(TimeRecords.FormatMinutes(intTotalMinutes, Session("User_ID"), False))
        sbTest.Append("</td></tr>")
        sbTest.Append("</table>")
        sbTest.Append("</div>")

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub

    Protected Sub BuildTimesheet()
        Dim dtmStartDate As Date

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT MAX(EndDate) AS NewStartDate FROM PayrollSummary WHERE Client_Id = @Client_ID"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            If Null.Replace(objDR("NewStartDate"), "null") <> "null" Then
                dtmStartDate = DateAdd(DateInterval.Day, 1, objDR("NewStartDate"))
            Else
                dtmStartDate = Now.AddDays(-14)
            End If
        Else
            dtmStartDate = Now.AddDays(-14)
        End If

        Dim strStartDate As String = dtmStartDate.Date & " 12:00 AM"

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM TimeRecords WHERE (User_ID = @User_ID AND ([StartTime] BETWEEN @StartTime AND @EndTime)) ORDER BY [StartTime] DESC, [Type] DESC"
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@StartTime", strStartDate)
        objCommand.Parameters.AddWithValue("@EndTime", CDate(Time.DatesOfWeek(DatePart(DateInterval.WeekOfYear, Clock.GetNow()), Year(Clock.GetNow()), 1) & " 11:59 PM"))

        objDR = objCommand.ExecuteReader()

        Dim strBGColor As String = "FFF"

        While objDR.Read()
            sbTest.Append("<tr style='background-color:#")
            sbTest.Append(strBGColor)
            sbTest.Append(";'><td>")
            sbTest.Append("<img src='images/timesheet_type")
            sbTest.Append(objDR("Type"))
            sbTest.Append(".png' align='absmiddle'>&nbsp;")
            sbTest.Append(Time.StripTime(objDR("StartTime")))
            sbTest.Append("</td><td>")
            sbTest.Append(Time.QuickStrip(objDR("StartTime")))
            sbTest.Append("</td><td>")
            If Not objDR("EndTime") Is DBNull.Value Then
                sbTest.Append(Time.QuickStrip(objDR("EndTime")))
            Else
                sbTest.Append("&nbsp;")
            End If
            sbTest.Append("</td><td")
            If Not objDR("EndTime") Is DBNull.Value Then
                sbTest.Append(">")
            Else
                sbTest.Append(" style='font-style:italic;'>")
            End If
            sbTest.Append(TimeRecords.FormatMinutes(TimeRecords.TimeDifference(objDR("StartTime"), objDR("EndTime")), Session("User_ID"), False))
            sbTest.Append("</td></tr>")


            If CInt(objDR("Type")) = 2 Then
                intTotalMinutes -= TimeRecords.TimeDifference(objDR("StartTime"), objDR("EndTime"))
            Else
                intTotalMinutes += TimeRecords.TimeDifference(objDR("StartTime"), objDR("EndTime"))
            End If

            If strBGColor = "FFF" Then
                strBGColor = "EEE"
            Else
                strBGColor = "FFF"
            End If
        End While
    End Sub
End Class
