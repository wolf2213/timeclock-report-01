
Partial Class MobileAccess_iPhone_Schedule
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("User_ID") = "" Then
            Response.Redirect("AccessHelp.aspx")
        End If

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM [Schedules] WHERE (([User_ID] = @User_ID) AND ([Year] = @Year) AND (WeekNum = @WeekNum))"
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objCommand.Parameters.AddWithValue("@WeekNum", DatePart(DateInterval.WeekOfYear, Date.Now))
        objCommand.Parameters.AddWithValue("@Year", DatePart(DateInterval.Year, Date.Now))

        objDR = objCommand.ExecuteReader()

        Dim boolScheduled As Boolean

        If objDR.HasRows Then
            objDR.Read()
            boolScheduled = True
        Else
            boolScheduled = False
        End If

        Dim sbTest As New StringBuilder

        sbTest.Append("<div class='toolbar'>")
        sbTest.Append("<a class='button backButton' href='Home.aspx?int=1' target='_self'>Home</a>")
        sbTest.Append("</div>")
        sbTest.Append("<div id='schedule' class='panel' title='TimeClockWizard' selected='true'>")
        sbTest.Append("<h2><img src='images/schedule.png' align='absmiddle'>&nbsp;My Schedule for This Week</h2>")
        sbTest.Append("<fieldset>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<label>Sunday</label>")
        sbTest.Append("<label class='rowRight'>")
        If boolScheduled Then
            sbTest.Append(ScheduleTimeDisplay(objDR("SunCode"), objDR("SunStart"), objDR("SunEnd")))
        Else
            sbTest.Append("Not Working")
        End If
        sbTest.Append("</label>")
        sbTest.Append("</div>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<label>Monday</label>")
        sbTest.Append("<label class='rowRight'>")
        If boolScheduled Then
            sbTest.Append(ScheduleTimeDisplay(objDR("MonCode"), objDR("MonStart"), objDR("MonEnd")))
        Else
            sbTest.Append("Not Working")
        End If
        sbTest.Append("</label>")
        sbTest.Append("</div>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<label>Tuesday</label>")
        sbTest.Append("<label class='rowRight'>")
        If boolScheduled Then
            sbTest.Append(ScheduleTimeDisplay(objDR("TueCode"), objDR("TueStart"), objDR("TueEnd")))
        Else
            sbTest.Append("Not Working")
        End If
        sbTest.Append("</label>")
        sbTest.Append("</div>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<label>Wednesday</label>")
        sbTest.Append("<label class='rowRight'>")
        If boolScheduled Then
            sbTest.Append(ScheduleTimeDisplay(objDR("WedCode"), objDR("WedStart"), objDR("WedEnd")))
        Else
            sbTest.Append("Not Working")
        End If
        sbTest.Append("</label>")
        sbTest.Append("</div>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<label>Thursday</label>")
        sbTest.Append("<label class='rowRight'>")
        If boolScheduled Then
            sbTest.Append(ScheduleTimeDisplay(objDR("ThuCode"), objDR("ThuStart"), objDR("ThuEnd")))
        Else
            sbTest.Append("Not Working")
        End If
        sbTest.Append("</label>")
        sbTest.Append("</div>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<label>Friday</label>")
        sbTest.Append("<label class='rowRight'>")
        If boolScheduled Then
            sbTest.Append(ScheduleTimeDisplay(objDR("FriCode"), objDR("FriStart"), objDR("FriEnd")))
        Else
            sbTest.Append("Not Working")
        End If
        sbTest.Append("</label>")
        sbTest.Append("</div>")
        sbTest.Append("<div class='row'>")
        sbTest.Append("<label>Saturday</label>")
        sbTest.Append("<label class='rowRight'>")
        If boolScheduled Then
            sbTest.Append(ScheduleTimeDisplay(objDR("SatCode"), objDR("SatStart"), objDR("SatEnd")))
        Else
            sbTest.Append("Not Working")
        End If
        sbTest.Append("</label>")
        sbTest.Append("</div>")
        sbTest.Append("</fieldset>")
        sbTest.Append("</div>")

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub

    Protected Function ScheduleTimeDisplay(ByVal strDayCode As String, ByVal dtmStartTime As Date, ByVal dtmEndTime As Date)
        'scheduleTimeDisplay(Container.DataItem("SunCode"), Container.DataItem("SunStart"), Container.DataItem("SunEnd")
        If Not IsNumeric(strDayCode) Then
            'the person is not working this day
            If strDayCode = "NW" Then
                ScheduleTimeDisplay = "Not Working"
            Else
                ScheduleTimeDisplay = AbsencesClass.AbsenceNameFromCode(strDayCode) & " (" & DateDiff(DateInterval.Hour, dtmStartTime, dtmEndTime) & " hours)"
            End If
        ElseIf strDayCode = "0" Then
            'working, not assigned an office
            ScheduleTimeDisplay = Time.QuickStrip(dtmStartTime) & " - " & Time.QuickStrip(dtmEndTime)
        Else
            'working and assigned office
            ScheduleTimeDisplay = Time.QuickStrip(dtmStartTime) & " - " & Time.QuickStrip(dtmEndTime) ' & "<br>" & Offices.OfficeName(CInt(strDayCode))
        End If
    End Function
End Class
