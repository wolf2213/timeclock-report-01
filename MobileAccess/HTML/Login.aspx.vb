
Partial Class MobileAccess_HTML_Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("client") <> "" Then
            GetClientInfo()
        ElseIf Session("Client_ID") Is Nothing Then
            Response.Redirect("AccessHelp.aspx?full=1")
        End If

        If InStr(Request.UserAgent, "iPhone") Then
            Response.Redirect("../iPhone/Login.aspx")
        End If

        CheckDelinquency()
        LoginScreen()
    End Sub

    Protected Sub GetClientInfo()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Client_ID, Trial, NextBillingDate FROM Clients WHERE Subdomain = @Subdomain"
        objCommand.Parameters.AddWithValue("@Subdomain", Request.QueryString("client"))

        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            Session("Client_ID") = objDR("Client_ID")
            Session("Subdomain") = Request.QueryString("client")

            If objDR("Trial") Then
                Dim intTrialDaysLeft As Integer = DateDiff(DateInterval.Day, CDate(Now().Date & " 12:00 AM"), objDR("NextBillingDate"))
                If intTrialDaysLeft < 0 Then
                    Session("TrialExpired") = True
                End If
            End If
        Else
            Response.Redirect("AccessHelp.aspx?full=1")
        End If
    End Sub

    Protected Sub CheckDelinquency()
        If Chase.Delinquency(Session("Client_ID")) <> 8 Then
            Session("Delinquent") = True
        Else
            Session("Delinquent") = False
        End If
    End Sub

    Protected Sub LoginScreen()
        Dim sbTest As New StringBuilder

        MobileAccess.Header(sbTest)

        sbTest.Append("<form action='DoLogin.aspx' method='post' id='login' class='panel' title='TimeClockWizard' selected='true'>")

        If Request.QueryString("error") = "" Then
            If Not Session("TrialExpired") And Not Session("Delinquent") Then
                sbTest.Append("<h2>Login</h2>")
            ElseIf Session("TrialExpired") Then
                sbTest.Append("<h2 style='color:red;'>Your free trial has expired</h2>")
            ElseIf Session("Delinquent") Then
                sbTest.Append("<h2 style='color:red;'>Your account is delinquent</h2>")
            End If
        ElseIf Request.QueryString("error") = "1" Then
            sbTest.Append("<h2 style='color:red;'>Username/password not found</h2>")
        ElseIf Request.QueryString("error") = "2" Then
            sbTest.Append("<h2 style='color:red;'>Mobile Access is not enabled for your user account</h2>")
        End If

        If Not Session("TrialExpired") And Not Session("Delinquent") Then
            sbTest.Append("<fieldset>")
            sbTest.Append("Username&nbsp;")
            sbTest.Append("<input type='text' name='username' value=''/>")
            sbTest.Append("<br>")
            sbTest.Append("Password&nbsp;")
            sbTest.Append("<input type='password' name='password' value=''/>")
            sbTest.Append("</fieldset>")
            sbTest.Append("<br>")
            sbTest.Append("<fieldset>")
            sbTest.Append("<label>")
            sbTest.Append("<input type='checkbox' name='remember' value='1'>")
            sbTest.Append("&nbsp;Remember Me")
            sbTest.Append("</label>")
            sbTest.Append("</fieldset>")
            sbTest.Append("<br>")
            sbTest.Append("<input type='submit' value='Login'>")
        End If

        sbTest.Append("</form>")

        MobileAccess.Footer(sbTest)

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub
End Class
