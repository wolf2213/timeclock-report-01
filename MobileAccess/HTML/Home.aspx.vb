
Partial Class MobileAccess_HTML_Home
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("User_ID") = "" Then
            Response.Redirect("AccessHelp.aspx")
        End If

        Dim sbTest As New StringBuilder

        MobileAccess.Header(sbTest)

        sbTest.Append("<h2><img src='images/mobileaccess.png' align='absmiddle' border='0'>&nbsp;Mobile Access Home</h2>")
        sbTest.Append("<fieldset>")
        sbTest.Append("&nbsp;&nbsp;<a href='Clock.aspx'><img src='images/clock.png' align='absmiddle' border='0'>&nbsp;&nbsp;Clock In/Out</a>")
        sbTest.Append("<br>")
        sbTest.Append("&nbsp;&nbsp;<a href='Mileage.aspx'><img src='images/mileage.png' align='absmiddle' border='0'>&nbsp;&nbsp;Add Mileage</a>")
        sbTest.Append("<br>")
        sbTest.Append("&nbsp;&nbsp;<a href='Timesheet.aspx'><img src='images/timesheet.png' align='absmiddle' border='0'>&nbsp;&nbsp;My Current Timesheet</a>")
        sbTest.Append("<br>")
        sbTest.Append("&nbsp;&nbsp;<a href='Schedule.aspx'><img src='images/schedule.png' align='absmiddle' border='0'>&nbsp;&nbsp;My Schedule</a>")
        sbTest.Append("</fieldset>")

        sbTest.Append("<br><a href='Logout.aspx?client=")
        sbTest.Append(Session("Subdomain"))
        sbTest.Append("'>Logout</a>")

        MobileAccess.Footer(sbTest)

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub
End Class
