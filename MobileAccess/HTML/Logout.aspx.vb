
Partial Class MobileAccess_HTML_Logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("User_ID") = ""
        Session("Manager") = False
        Response.Redirect("Login.aspx?logout=1&client=" & Request.QueryString("client"))
    End Sub
End Class
