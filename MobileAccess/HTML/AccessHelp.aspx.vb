
Partial Class MobileAccess_HTML_AccessHelp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sbTest As New StringBuilder

        MobileAccess.Header(sbTest)

        sbTest.Append("<h2>An error has occured</h2>")
        sbTest.Append("If you were trying to login, you may have used an invalid URL.  Please go to your company's main URL (m.<i>subdomain</i>.TimeClockWizard.com) to login.")
        sbTest.Append("<br><br>")
        sbTest.Append("If you were trying to use the application, your session may have timed out.  This is for your security.  Please login again.")

        MobileAccess.Footer(sbTest)

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub
End Class
