
Partial Class MobileAccess_HTML_Mileage
    Inherits System.Web.UI.Page
    Dim strMileageMessage As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("User_ID") = "" Then
            Response.Redirect("AccessHelp.aspx")
        End If

        strMileageMessage = ""

        If Request.Form("miles") <> "" Then
            AddMileage()
        End If

        Dim sbTest As New StringBuilder
        MobileAccess.Header(sbTest)

        sbTest.Append("<form action='Mileage.aspx' method='post'>")
        sbTest.Append("<h2><img src='images/mileage.png' align='absmiddle'>&nbsp;Add Mileage</h2>")
        sbTest.Append("<fieldset>")
        sbTest.Append("Miles&nbsp;")
        sbTest.Append("<input type='text' name='miles' value=''/>")
        sbTest.Append("</fieldset>")
        sbTest.Append("<br>")
        sbTest.Append("<input type='submit' value='Add Mileage'>")
        sbTest.Append(strMileageMessage)
        sbTest.Append("</form>")

        sbTest.Append("<a href='Home.aspx'>Home</a>")

        sbTest.Append("<br><a href='Logout.aspx?client=")
        sbTest.Append(Session("Subdomain"))
        sbTest.Append("'>Logout</a>")

        MobileAccess.Footer(sbTest)

        Response.ClearContent()
        Response.Write(sbTest.ToString)
    End Sub

    Protected Sub AddMileage()
        If IsNumeric(Request.Form("miles")) Then
            If CInt(Request.Form("miles")) > 0 Then
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "INSERT INTO Mileage (User_ID, Client_ID, DateTime, Miles, VerifiedBy) VALUES (@User_ID, @Client_ID, @DateTime, @Miles, @VerifiedBy)"
                objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objCommand.Parameters.AddWithValue("@DateTime", Clock.GetNow())
                objCommand.Parameters.AddWithValue("@Miles", Request.Form("miles"))
                If Permissions.MileageValidate(Session("User_ID")) Then
                    objCommand.Parameters.AddWithValue("@VerifiedBy", Session("User_ID"))
                Else
                    objCommand.Parameters.AddWithValue("@VerifiedBy", 0)
                End If

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                strMileageMessage = "<br><span>Mileage added successfully</span>"
            Else
                strMileageMessage = "<br><span style='color:red;'>Invalid mileage</span>"
            End If
        Else
            strMileageMessage = "<br><span style='color:red;'>Invalid mileage</span>"
        End If
    End Sub
End Class
