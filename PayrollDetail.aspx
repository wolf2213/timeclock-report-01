<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="PayrollDetail.aspx.vb" Inherits="PayrollDetail" Title="TimeClockWizard - Payroll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0px" cellspacing="0px" width="100%">
        <tr>
            <td align="left">
                <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="Payroll.aspx" ToolTip="header=[Back] body=[]"><img src="images/back.png" border="0" style="vertical-align:middle;" /></asp:HyperLink>&nbsp;&nbsp;<span
                    class="title">Payroll for</span>
                <asp:Label ID="lblDates" runat="server" Text="Dates" CssClass="title"></asp:Label>&nbsp;&nbsp;
                <span class="doNotPrint">
                    <asp:HyperLink ID="lnkDetailView" runat="server" Text="View Details for All Users"></asp:HyperLink></span>
            </td>
            <td align="center">
                <span class="doNotPrint">
                    <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/images/print-btn.png" OnClientClick="window.print()" /></span>
            </td>
        </tr>
    </table>
    <br />
    <div style="float: left; margin-left: 25px;">
        <asp:Repeater ID="rptPayrollDetail" runat="server" OnItemDataBound="rptPayrollDetail_ItemDataBound"
            OnItemCommand="rptPayrollDetail_ItemCommand">
            <HeaderTemplate>
                <ul style="border-left: 1px solid #c6ced0; width: 110px; float: left; font-size: 10px;">
                    <li style="text-align: center;" class="column-head column-title">Name</li>
                </ul>
                <ul style="width: 140px; float: left; font-size: 10px;">
                    <li style="text-align: center;" class="column-head column-title">Regular Hours</li>
                </ul>
                <ul style="width: 140px; float: left; font-size: 10px;">
                    <li style="text-align: center;" class="column-head column-title">Regular Pay</li>
                </ul>
                <ul style="width: 140px; float: left; font-size: 10px;">
                    <li style="text-align: center;" class="column-head column-title">Overtime Hours</li>
                </ul>
                <ul style="width: 140px; float: left; font-size: 10px;">
                    <li style="text-align: center;" class="column-head column-title">Overtime Pay</li>
                </ul>
                <ul style="width: 130px; float: left; font-size: 10px;">
                    <li style="text-align: center;" class="column-head column-title">Bonuses</li>
                </ul>
                <ul style="width: 130px; float: left; font-size: 10px;">
                    <li style="text-align: center;" class="column-head column-title">Gross Pay</li>
                </ul>
                <ul style="border-right: 1px solid #c6ced0; width: 40px; float: left; font-size: 10px;">
                    <li style="text-align: center;" class="column-head column-title">&nbsp;</li>
                </ul>
            </HeaderTemplate>
            <ItemTemplate>
                <ul style="width: 110px; float: left; font-size: 13px;">
                    <li style="text-align: center; height: 34px; line-height: 34px;">
                        <asp:Label ID="lblUser" runat="server" Text="Label" CssClass="text-bold"></asp:Label>
                    </li>
                </ul>
                <ul style="width: 140px; float: left; font-size: 13px;">
                    <li style="text-align: center; height: 34px; line-height: 34px;">
                        <asp:Label ID="lblRegularHours" runat="server" Text="Label"></asp:Label></li>
                </ul>
                <ul style="width: 140px; float: left; font-size: 13px;">
                    <li style="text-align: center; height: 34px; line-height: 34px;">
                        <asp:Label ID="lblRegularPay" runat="server" Text="Label"></asp:Label></li>
                </ul>
                <ul style="width: 140px; float: left; font-size: 13px;">
                    <li style="text-align: center; height: 34px; line-height: 34px;">
                        <asp:Label ID="lblOTHours" runat="server" Text="Label"></asp:Label></li>
                </ul>
                <ul style="width: 140px; float: left; font-size: 13px;">
                    <li style="text-align: center; height: 34px; line-height: 34px;">
                        <asp:Label ID="lblOTPay" runat="server" Text="Label"></asp:Label></li>
                </ul>
                <ul style="width: 130px; float: left; font-size: 13px;">
                    <li style="text-align: center; height: 34px; line-height: 34px;">
                        <asp:Label ID="lblOtherPay" runat="server" Text="Label"></asp:Label></li>
                </ul>
                <ul style="width: 130px; float: left; font-size: 13px;">
                    <li style="text-align: center; height: 34px; line-height: 34px;">
                        <asp:Label ID="lblGrossPay" runat="server" Text="Label"></asp:Label></li>
                </ul>
                <ul style="width: 40px; float: left; font-size: 13px;">
                    <li style="text-align: center; height: 34px; line-height: 34px;">
                        <asp:ImageButton ID="btnMoreDetail" runat="server" ImageUrl="~/images/zoom.png" AlternateText="User's Payroll Details"
                            ToolTip="header=[User's Payroll Details] body=[] offsetx=[-150]" /></li>
                </ul>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
