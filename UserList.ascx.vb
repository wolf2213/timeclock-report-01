
Partial Class UserList
    Inherits System.Web.UI.UserControl

    Public Event UserChanged(ByVal sender As Object, ByVal e As CommandEventArgs)
    Public Event OfficeChanged(ByVal sender As Object, ByVal e As CommandEventArgs)
    Public Event AllUsersSelected(ByVal sender As Object, ByVal e As CommandEventArgs)
    Public Event SelectedUsersSelected(ByVal sender As Object, ByVal e As CommandEventArgs)
    Public InitialSelection As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim objDROffices As System.Data.SqlClient.SqlDataReader
            Dim objCommandOffices As System.Data.SqlClient.SqlCommand
            Dim objConnectionOffices As System.Data.SqlClient.SqlConnection
            objConnectionOffices = New System.Data.SqlClient.SqlConnection

            objConnectionOffices.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionOffices.Open()
            objCommandOffices = New System.Data.SqlClient.SqlCommand()
            objCommandOffices.Connection = objConnectionOffices
            objCommandOffices.CommandText = "SELECT Office_ID, Name FROM Offices WHERE Client_ID = @Client_ID ORDER BY Name ASC"
            objCommandOffices.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))

            objDROffices = objCommandOffices.ExecuteReader()

            rblOffices.DataSource = objDROffices
            rblOffices.DataTextField = "Name"
            rblOffices.DataValueField = "Office_ID"
            rblOffices.DataBind()

            objDROffices = Nothing
            objCommandOffices = Nothing
            objConnectionOffices.Close()
            objConnectionOffices = Nothing

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            If (Not Permissions.AccountHolder(Session("User_ID"))) And (Permissions.ManageEmployees(Session("User_ID"))) Then
                objCommand.CommandText = "SELECT User_ID, (LastName + ', ' + FirstName) As FullName FROM [Users] WHERE ([Client_ID] = @Client_ID AND (ManagerUserId = @ManagerUserId OR User_Id = @User_Id) AND Active = 1) ORDER BY LastName ASC"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
                objCommand.Parameters.AddWithValue("@ManagerUserId", Session("User_ID"))
                objCommand.Parameters.AddWithValue("@User_Id", Session("User_ID"))
            Else
                objCommand.CommandText = "SELECT User_ID, (LastName + ', ' + FirstName) As FullName FROM [Users] WHERE ([Client_ID] = @Client_ID AND Active = 1) ORDER BY LastName ASC"
                objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            End If

            objDR = objCommand.ExecuteReader()

            cblUsers.DataSource = objDR
            cblUsers.DataTextField = "FullName"
            cblUsers.DataValueField = "User_ID"
            cblUsers.DataBind()

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            Select Case InitialSelection
                Case 0
                    lblChosenUser.Text = "All Users"
                    rbAllUsers.Checked = True
                Case 1
                    lblChosenUser.Text = Session("LastName") & ", " & Session("FirstName")
                    rbSelectedUsers.Checked = True
                    cblUsers.SelectedValue = Session("User_ID")
            End Select
        End If
    End Sub

    Protected Sub rbAllUsers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rbAllUsers.Checked Then
            Dim li As New ListItem

            For Each li In cblUsers.Items
                li.Selected = True
            Next

            For Each li In rblOffices.Items
                li.Selected = False
            Next

            rbSelectedUsers.Checked = False

            lblChosenUser.Text = "All Users"

            ShowHideUserChooser()

            Dim Args As New CommandEventArgs("AllUsers", 1)
            RaiseEvent AllUsersSelected(Me, Args)
        End If
    End Sub

    Protected Sub rbSelectedUsers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        rbAllUsers.Checked = False

        Dim li As New ListItem

        For Each li In rblOffices.Items
            li.Selected = False
        Next

        For Each li In cblUsers.Items
            li.Selected = False
        Next

        lblChosenUser.Text = "No Selected Users..."
    End Sub

    Protected Sub rblOffices_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim li As New ListItem

        For Each li In cblUsers.Items
            li.Selected = False
        Next

        rbAllUsers.Checked = False
        rbSelectedUsers.Checked = False

        lblChosenUser.Text = rblOffices.SelectedItem.Text

        ShowHideUserChooser()

        Dim Args As New CommandEventArgs("rblOffices", rblOffices.SelectedValue)
        RaiseEvent OfficeChanged(Me, Args)
    End Sub

    Protected Sub cblUsers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        rbSelectedUsers.Checked = True

        rbAllUsers.Checked = False

        Dim li As New ListItem
        Dim SelectedPrefix As String
        Dim SelectedCount As Integer = 0
        Dim SelectedPlural As String = ""

        For Each li In rblOffices.Items
            li.Selected = False
        Next

        For Each li In cblUsers.Items
            If li.Selected Then
                SelectedCount = SelectedCount + 1
                SelectedPlural = "s"
            End If
        Next

        If SelectedCount = 0 Then
            SelectedPrefix = "No"
            SelectedPlural = "s"
        ElseIf SelectedCount = 1 Then
            SelectedPrefix = SelectedCount
            SelectedPlural = ""
        Else
            SelectedPrefix = SelectedCount
            SelectedPlural = "s"
        End If

        lblChosenUser.Text = SelectedPrefix & " Selected User" & SelectedPlural & "..."

        Dim Args As New CommandEventArgs("cblUsers", cblUsers.Items)
        RaiseEvent UserChanged(Me, Args)
    End Sub

    Protected Sub btnShowHideUserChooser_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowHideUserChooser()
    End Sub

    Protected Sub ShowHideUserChooser()
        If pnlUserChooser.Visible Then
            pnlUserChooser.Visible = True
            btnShowHideUserChooser.ImageUrl = "~/images/userlist_down.png"
        Else
            pnlUserChooser.Visible = True
            btnShowHideUserChooser.ImageUrl = "~/images/userlist_up.png"
        End If
    End Sub

End Class