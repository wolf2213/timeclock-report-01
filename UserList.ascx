<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UserList.ascx.vb" Inherits="UserList" %>

<table cellpadding="0px" cellspacing="0px" border="0" style="width:180px;">
    <tr>
        <td>
            <div class="userChooserBox">
               <!-- <asp:Label ID="lblChosenUser" runat="server" Text=""></asp:Label> -->
                <div style="position:absolute; top:0px; right:1px;">
                    <asp:ImageButton ID="btnShowHideUserChooser" runat="server" ImageUrl="~/images/userlist_down.png" OnClick="btnShowHideUserChooser_Click" Visible="false" /></div>
            </div>
            <asp:Panel ID="pnlUserChooser" runat="server" CssClass="userChooserList" Visible="true">
                <asp:RadioButton ID="rbAllUsers" runat="server" Text="All Users" CssClass="userChooserRBs" OnCheckedChanged="rbAllUsers_CheckedChanged" AutoPostBack="true" />
                <asp:RadioButtonList ID="rblOffices" runat="server" OnSelectedIndexChanged="rblOffices_SelectedIndexChanged" AutoPostBack="true"></asp:RadioButtonList>
                <asp:RadioButton ID="rbSelectedUsers" runat="server" Text="Selected Users" CssClass="userChooserRBs" OnCheckedChanged="rbSelectedUsers_CheckedChanged" AutoPostBack="true" />
                <hr style="color:#DDD; height:1px; margin-left:10px; margin-right:10px;" />
                <asp:CheckBoxList ID="cblUsers" runat="server" OnSelectedIndexChanged="cblUsers_SelectedIndexChanged" AutoPostBack="true"></asp:CheckBoxList>
            </asp:Panel>
        </td>
    </tr>
</table>