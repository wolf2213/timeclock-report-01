Imports System.Net.mail

Partial Class Support
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        If Not Session("Manager") Then
            pnlIsAccountHolder.Visible = False
        End If

        Dim pnlMasterTabSupport As Panel = Master.FindControl("pnlTabSupport")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Support As Panel = Master.FindControl("pnlTab2Support")

        pnlMasterTabSupport.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Support
        pnlMasterTab2Support.CssClass = "tab2 tab2On"

        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT Email FROM Users WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            txtEmailAddress.Text = objDR("Email")
        End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim boolSendMail As Boolean = True

        If txtSubject.Text = "" Then
            lblMessage.Visible = True
            lblMessage.Text = "The subject field is required"
            lblMessage.ForeColor = System.Drawing.Color.Red
            boolSendMail = False
        End If

        If txtMessage.Text = "" Then
            lblMessage.Visible = True
            lblMessage.Text = "The message field is required"
            lblMessage.ForeColor = System.Drawing.Color.Red
            boolSendMail = False
        End If

        If txtMessage.Text = "" And txtSubject.Text = "" Then
            lblMessage.Visible = True
            lblMessage.Text = "Both the message and subject fields are required"
            lblMessage.ForeColor = System.Drawing.Color.Red
            boolSendMail = False
        End If

        If boolSendMail Then
            Dim strSubjectPrefix As String = ""

            Select Case ddlType.SelectedValue
                Case 1
                    strSubjectPrefix = "Billing"
                Case 2
                    strSubjectPrefix = "Technical Support"
                Case 3
                    strSubjectPrefix = "Feature Request"
                Case 4
                    strSubjectPrefix = "Other"
            End Select

            Dim strCustom() As String = New String() {"Client ID=" & Session("Client_ID")}

            Dim SmarterTrack As New SmarterTrack.svcTickets
            Dim strNewTicketID As String = SmarterTrack.CreateTicket("WebService", "WebService", 3, txtEmailAddress.Text, strSubjectPrefix & ": " & txtSubject.Text, txtMessage.Text, False, True).RequestResult

            SmarterTrack.AddTicketNoteHtml("WebService", "WebService", strNewTicketID, "Comment", "<table style='font-size:11px;'><tr><td><b>Company</b></td><td>" & Session("ClientName") & " (" & Session("Client_ID") & ")</td></tr><tr><td><b>Name</b></td><td>" & Session("FirstName") & " " & Session("LastName") & " (" & Session("User_ID") & ")</tr><tr><td><b>Manager</b></td><td>" & Session("Manager") & "</td></tr></table><a href='http://www.TimeClockWizard.com/ClockitinApp/Boss/FindClient.aspx?id=" & Session("Client_ID") & "' target='_blank'>View in Boss</a><br><br><b>Browser:</b> " & Request.ServerVariables("HTTP_USER_AGENT"))
            SmarterTrack.SetTicketCustomFields("WebService", "WebService", strNewTicketID, strCustom)

            lblMessage.Visible = True
            lblMessage.Text = "Message successfully sent"
            lblMessage.ForeColor = System.Drawing.Color.Green
            timHideMessage.Enabled = True
        End If
    End Sub

    Protected Sub timHideMessage_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMessage.Visible = False
        lblMessage.Text = ""
        timHideMessage.Enabled = False
    End Sub
End Class
