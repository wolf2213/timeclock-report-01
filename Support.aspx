<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Support.aspx.vb" Inherits="Support" title="TimeClockWizard - Support" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="false">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <center>
                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="right">
                            Category:
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlType" runat="server">
                                <asp:ListItem Value="1">Billing</asp:ListItem>
                                <asp:ListItem Value="2">Technical Support</asp:ListItem>
                                <asp:ListItem Value="3">Feature Requests</asp:ListItem>
                                <asp:ListItem Value="4">Other</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                     </tr>
                     <tr>
                        <td align="right">
                            Your Email Address:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmailAddress" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Subject:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">
                            Message:
                        </td>
                        <td valign="top" align="left">
                            <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="15" Columns="40"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnSend" runat="server" Text="Send Message" OnClick="btnSend_Click" />
                <br />
                <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                <br /><br />
                <asp:Panel ID="pnlIsAccountHolder" runat="server">
                    <span class="title">
                        Contact us by phone at (877) 646-4446
                    </span>
                    <br />
                    Phone support is available from 10am to 6pm Eastern time.<br /><strong><em>Please use the above form for any urgent requests.</em></strong>
                    <br /><br />
                    Contact us by email at <asp:HyperLink ID="lnkEmail" Text="info@timeclockwizard.com" NavigateUrl="mailto:info@timeclockwizard.com" runat="server"></asp:HyperLink>
                    <br />
                    Please use the direct email address only if you are unable to use the above form.
                </asp:Panel>
            </center>
            <asp:Timer ID="timHideMessage" runat="server" Interval="2000" OnTick="timHideMessage_Tick" Enabled="false">
            </asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

