
Partial Class UserPermissions
    Inherits System.Web.UI.Page

    Dim intUserID As Int64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabUsers As Panel = Master.FindControl("pnlTabUsers")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")

        pnlMasterTabUsers.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Users

        intUserID = Request.QueryString("user_id")
        litFullUserName.Text = UserInfo.NameFromID(intUserID, 1)

        intUserID = Request.QueryString("user_ID")

        If Not IsPostBack Then
            LoadValues()
        End If
    End Sub

    Protected Sub LoadValues()


        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM Permissions WHERE User_ID = @User_ID AND Client_ID = @Client_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()

            chkAccountHolder.Checked = objDR("AccountHolder")
            chkTimesheetsAdd.Checked = objDR("TimesheetsAdd")
            chkTimesheetsDisplay.Checked = objDR("TimesheetsDisplay")
            chkTimesheetsValidate.Checked = objDR("TimesheetsValidate")
            chkTimesheetsEdit.Checked = objDR("TimesheetsEdit")
            chkTimesheetsDelete.Checked = objDR("TimesheetsDelete")
            chkMileageDisplay.Checked = objDR("MileageDisplay")
            chkMileageValidate.Checked = objDR("MileageValidate")
            chkMileageDelete.Checked = objDR("MileageDelete")
            chkScheduleEdit.Checked = objDR("ScheduleEdit")
            chkScheduleCopy.Checked = objDR("ScheduleCopy")
            chkScheduleDelete.Checked = objDR("ScheduleDelete")
            chkAbsencesAdd.Checked = objDR("AbsencesAdd")
            chkAbsencesApprove.Checked = objDR("AbsencesApprove")
            chkAbsencesDelete.Checked = objDR("AbsencesDelete")
            chkAbsencesSettings.Checked = objDR("AbsencesSettings")
            chkPayrollAdd.Checked = objDR("PayrollAdd")
            chkPayrollDelete.Checked = objDR("PayrollDelete")
            chkPayrollView.Checked = objDR("PayrollView")
            chkUsersEdit.Checked = objDR("UsersEdit")
            chkUsersDelete.Checked = objDR("UsersDelete")
            chkUsersClockGuard.Checked = objDR("UsersClockGuard")
            chkUsersMobileAccess.Checked = objDR("UsersMobileAccess")
            chkUsersRecover.Checked = objDR("UsersRecover")
            chkUsersAdd.Checked = objDR("UsersAdd")
            chkUsersAddManager.Checked = objDR("UsersAddManager")
            chkUsersAddManagerFull.Checked = objDR("UsersAddManagerFull")
            chkUsersEditPermissions.Checked = objDR("UsersEditPermissions")
            chkUsersEditPermissionsFull.Checked = objDR("UsersEditPermissionsFull")
            chkUsersCustomFields.Checked = objDR("UsersCustomFields")
            chkManageEmployees.Checked = objDR("ManageEmployees")
            chkSettingsOffices.Checked = objDR("SettingsOffices")
            chkSettingsGeneral.Checked = objDR("SettingsGeneral")
            chkSettingsClockPoints.Checked = objDR("SettingsClockPoints")

            If objDR("AccountHolder") Then
                chkAccountHolder.Checked = objDR("AccountHolder")
                chkTimesheetsAdd.Checked = True
                chkTimesheetsDisplay.Checked = True
                chkTimesheetsValidate.Checked = True
                chkTimesheetsEdit.Checked = True
                chkTimesheetsDelete.Checked = True
                chkMileageDisplay.Checked = True
                chkMileageValidate.Checked = True
                chkMileageDelete.Checked = True
                chkScheduleEdit.Checked = True
                chkScheduleCopy.Checked = True
                chkScheduleDelete.Checked = True
                chkAbsencesAdd.Checked = True
                chkAbsencesApprove.Checked = True
                chkAbsencesDelete.Checked = True
                chkAbsencesSettings.Checked = True
                chkPayrollAdd.Checked = True
                chkPayrollDelete.Checked = True
                chkPayrollView.Checked = True
                chkUsersEdit.Checked = True
                chkUsersDelete.Checked = True
                chkUsersClockGuard.Checked = True
                chkUsersMobileAccess.Checked = True
                chkUsersRecover.Checked = True
                chkUsersAdd.Checked = True
                chkUsersAddManager.Checked = True
                chkUsersAddManagerFull.Checked = True
                chkUsersEditPermissions.Checked = True
                chkUsersEditPermissionsFull.Checked = True
                chkUsersCustomFields.Checked = True
                chkManageEmployees.Checked = True
                chkSettingsOffices.Checked = True
                chkSettingsGeneral.Checked = True
                chkSettingsClockPoints.Checked = True

                chkTimesheetsAdd.Enabled = False
                chkTimesheetsDisplay.Enabled = False
                chkTimesheetsValidate.Enabled = False
                chkTimesheetsEdit.Enabled = False
                chkTimesheetsDelete.Enabled = False
                chkMileageDisplay.Enabled = False
                chkMileageValidate.Enabled = False
                chkMileageDelete.Enabled = False
                chkScheduleEdit.Enabled = False
                chkScheduleCopy.Enabled = False
                chkScheduleDelete.Enabled = False
                chkAbsencesAdd.Enabled = False
                chkAbsencesApprove.Enabled = False
                chkAbsencesDelete.Enabled = False
                chkAbsencesSettings.Enabled = False
                chkPayrollAdd.Enabled = False
                chkPayrollDelete.Enabled = False
                chkPayrollView.Enabled = False
                chkUsersEdit.Enabled = False
                chkUsersDelete.Enabled = False
                chkUsersClockGuard.Enabled = False
                chkUsersMobileAccess.Enabled = False
                chkUsersRecover.Enabled = False
                chkUsersAdd.Enabled = False
                chkUsersAddManager.Enabled = False
                chkUsersAddManagerFull.Enabled = False
                chkUsersEditPermissions.Enabled = False
                chkUsersEditPermissionsFull.Enabled = False
                chkUsersCustomFields.Enabled = False
                chkManageEmployees.Enabled = False
                chkSettingsOffices.Enabled = False
                chkSettingsGeneral.Enabled = False
                chkSettingsClockPoints.Enabled = False
            Else
                If Not objDR("TimesheetsDisplay") Then
                    chkTimesheetsAdd.Checked = False
                    chkTimesheetsDisplay.Checked = False
                    chkTimesheetsValidate.Checked = False
                    chkTimesheetsEdit.Checked = False
                    chkTimesheetsDelete.Checked = False

                    chkTimesheetsAdd.Enabled = False
                    chkTimesheetsValidate.Enabled = False
                    chkTimesheetsEdit.Enabled = False
                    chkTimesheetsDelete.Enabled = False
                Else
                    If Not objDR("TimesheetsValidate") Then
                        chkTimesheetsAdd.Checked = False
                        chkTimesheetsValidate.Checked = False
                        chkTimesheetsEdit.Checked = False
                        chkTimesheetsDelete.Checked = False

                        chkTimesheetsAdd.Enabled = False
                        chkTimesheetsEdit.Enabled = False
                        chkTimesheetsDelete.Enabled = False
                    End If
                End If

                If Not objDR("ScheduleEdit") Then
                    chkScheduleEdit.Checked = False
                    chkScheduleCopy.Checked = False
                    chkScheduleDelete.Checked = False

                    chkScheduleCopy.Enabled = False
                    chkScheduleDelete.Enabled = False
                End If

                If Not objDR("PayrollView") Then
                    chkPayrollView.Checked = False
                    chkPayrollAdd.Checked = False
                    chkPayrollDelete.Checked = False

                    chkPayrollAdd.Enabled = False
                    chkPayrollDelete.Enabled = False
                End If

                If Not objDR("UsersAdd") Then
                    chkUsersAdd.Checked = False
                    chkUsersAddManager.Checked = False
                    chkUsersAddManagerFull.Checked = False

                    chkUsersAddManager.Enabled = False
                    chkUsersAddManagerFull.Enabled = False
                End If

            End If
        End If
        objDR = Nothing
        objConnection.Close()
        objConnection = Nothing
        objCommand = Nothing

        If Not Permissions.UsersAddManagerFull(Session("User_ID")) Then
            'this manager does not have the permissions to edit all permissions
            'only show the permissions that this manager can change

            objConnection = New System.Data.SqlClient.SqlConnection
            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT * FROM Permissions WHERE User_ID = @User_ID"
            objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()

                pnlAccountHolder.visible = objDR("AccountHolder")
                pnlTimesheetsAdd.Visible = objDR("TimesheetsAdd")
                pnlTimesheetsDisplay.Visible = objDR("TimesheetsDisplay")
                pnlTimesheetsValidate.Visible = objDR("TimesheetsValidate")
                pnlTimesheetsEdit.Visible = objDR("TimesheetsEdit")
                pnlTimesheetsDelete.Visible = objDR("TimesheetsDelete")
                pnlMileageDisplay.Visible = objDR("MileageDisplay")
                pnlMileageValidate.Visible = objDR("MileageValidate")
                pnlMileageDelete.Visible = objDR("MileageDelete")
                pnlScheduleEdit.Visible = objDR("ScheduleEdit")
                pnlScheduleCopy.Visible = objDR("ScheduleCopy")
                pnlScheduleDelete.Visible = objDR("ScheduleDelete")
                pnlAbsencesAdd.Visible = objDR("AbsencesAdd")
                pnlAbsencesApprove.Visible = objDR("AbsencesApprove")
                pnlAbsencesDelete.Visible = objDR("AbsencesDelete")
                pnlAbsencesSettings.Visible = objDR("AbsencesSettings")
                pnlPayrollAdd.Visible = objDR("PayrollAdd")
                pnlPayrollDelete.Visible = objDR("PayrollDelete")
                pnlPayrollView.Visible = objDR("PayrollView")
                pnlUsersEdit.Visible = objDR("UsersEdit")
                pnlUsersDelete.Visible = objDR("UsersDelete")
                pnlUsersClockGuard.Visible = objDR("UsersClockGuard")
                pnlUsersMobileAccess.Visible = objDR("UsersMobileAccess")
                pnlUsersRecover.Visible = objDR("UsersRecover")
                pnlUsersAdd.Visible = objDR("UsersAdd")
                pnlUsersAddManager.Visible = objDR("UsersAddManager")
                pnlUsersAddManagerFull.Visible = objDR("UsersAddManagerFull")
                pnlUsersEditPermissions.Visible = objDR("UsersEditPermissions")
                pnlUsersEditPermissionsFull.Visible = objDR("UsersEditPermissionsFull")
                pnlUsersCustomFields.Visible = objDR("UsersCustomFields")
                pnlSettingsOffices.Visible = objDR("SettingsOffices")
                pnlSettingsGeneral.Visible = objDR("SettingsGeneral")
                pnlSettingsClockPoints.Visible = objDR("SettingsClockPoints")
                'manager can't change "Manage Employees" checkbox.
                pnlManageEmployees.Visible = False
            End If

            objDR = Nothing
            objConnection.Close()
            objConnection = Nothing
            objCommand = Nothing
        End If
    End Sub

    Protected Sub chkAccountHolder_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAccountHolder.CheckedChanged
        Trace.Write("UserPermissions.aspx", "chkAccountHolder_CheckedChanged fired")
        If chkAccountHolder.Checked Then
            chkAccountHolder.Checked = True
            chkTimesheetsAdd.Checked = True
            chkTimesheetsDisplay.Checked = True
            chkTimesheetsValidate.Checked = True
            chkTimesheetsEdit.Checked = True
            chkTimesheetsDelete.Checked = True
            chkMileageDisplay.Checked = True
            chkMileageValidate.Checked = True
            chkMileageDelete.Checked = True
            chkScheduleEdit.Checked = True
            chkScheduleCopy.Checked = True
            chkScheduleDelete.Checked = True
            chkAbsencesAdd.Checked = True
            chkAbsencesApprove.Checked = True
            chkAbsencesDelete.Checked = True
            chkAbsencesSettings.Checked = True
            chkPayrollAdd.Checked = True
            chkPayrollDelete.Checked = True
            chkPayrollView.Checked = True
            chkUsersEdit.Checked = True
            chkUsersDelete.Checked = True
            chkUsersClockGuard.Checked = True
            chkUsersMobileAccess.Checked = True
            chkUsersRecover.Checked = True
            chkUsersAdd.Checked = True
            chkUsersAddManager.Checked = True
            chkUsersAddManagerFull.Checked = True
            chkUsersEditPermissions.Checked = True
            chkUsersEditPermissionsFull.Checked = True
            chkUsersCustomFields.Checked = True
            chkManageEmployees.Checked = True
            chkSettingsOffices.Checked = True
            chkSettingsGeneral.Checked = True
            chkSettingsClockPoints.Checked = True

            chkTimesheetsAdd.Enabled = False
            chkTimesheetsDisplay.Enabled = False
            chkTimesheetsValidate.Enabled = False
            chkTimesheetsEdit.Enabled = False
            chkTimesheetsDelete.Enabled = False
            chkMileageDisplay.Enabled = False
            chkMileageValidate.Enabled = False
            chkMileageDelete.Enabled = False
            chkScheduleEdit.Enabled = False
            chkScheduleCopy.Enabled = False
            chkScheduleDelete.Enabled = False
            chkAbsencesAdd.Enabled = False
            chkAbsencesApprove.Enabled = False
            chkAbsencesDelete.Enabled = False
            chkAbsencesSettings.Enabled = False
            chkPayrollAdd.Enabled = False
            chkPayrollDelete.Enabled = False
            chkPayrollView.Enabled = False
            chkUsersEdit.Enabled = False
            chkUsersDelete.Enabled = False
            chkUsersClockGuard.Enabled = False
            chkUsersMobileAccess.Enabled = False
            chkUsersRecover.Enabled = False
            chkUsersAdd.Enabled = False
            chkUsersAddManager.Enabled = False
            chkUsersAddManagerFull.Enabled = False
            chkUsersEditPermissions.Enabled = False
            chkUsersEditPermissionsFull.Enabled = False
            chkUsersCustomFields.Enabled = False
            chkManageEmployees.Enabled = False
            chkSettingsOffices.Enabled = False
            chkSettingsGeneral.Enabled = False
            chkSettingsClockPoints.Enabled = False
        Else
            chkAccountHolder.Checked = False

            chkTimesheetsAdd.Enabled = True
            chkTimesheetsDisplay.Enabled = True
            chkTimesheetsValidate.Enabled = True
            chkTimesheetsEdit.Enabled = True
            chkTimesheetsDelete.Enabled = True
            chkMileageDisplay.Enabled = True
            chkMileageValidate.Enabled = True
            chkMileageDelete.Enabled = True
            chkScheduleEdit.Enabled = True
            chkScheduleCopy.Enabled = True
            chkScheduleDelete.Enabled = True
            chkAbsencesAdd.Enabled = True
            chkAbsencesApprove.Enabled = True
            chkAbsencesDelete.Enabled = True
            chkAbsencesSettings.Enabled = True
            chkPayrollAdd.Enabled = True
            chkPayrollDelete.Enabled = True
            chkPayrollView.Enabled = True
            chkUsersEdit.Enabled = True
            chkUsersDelete.Enabled = True
            chkUsersClockGuard.Enabled = True
            chkUsersMobileAccess.Enabled = True
            chkUsersRecover.Enabled = True
            chkUsersAdd.Enabled = True
            chkUsersAddManager.Enabled = True
            chkUsersAddManagerFull.Enabled = True
            chkUsersEditPermissions.Enabled = True
            chkUsersEditPermissionsFull.Enabled = True
            chkUsersCustomFields.Enabled = True
            chkManageEmployees.Enabled = True
            chkSettingsOffices.Enabled = True
            chkSettingsGeneral.Enabled = True
            chkSettingsClockPoints.Enabled = True
        End If
    End Sub

    Protected Sub chkScheduleEdit_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkScheduleEdit.CheckedChanged
        If Not chkScheduleEdit.Checked Then
            chkScheduleEdit.Checked = False
            chkScheduleCopy.Checked = False
            chkScheduleDelete.Checked = False

            chkScheduleCopy.Enabled = False
            chkScheduleDelete.Enabled = False
        Else
            chkScheduleCopy.Enabled = True
            chkScheduleDelete.Enabled = True
        End If
    End Sub

    Protected Sub chkPayrollView_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPayrollView.CheckedChanged
        If Not chkPayrollView.Checked Then
            chkPayrollView.Checked = False
            chkPayrollAdd.Checked = False
            chkPayrollDelete.Checked = False

            chkPayrollAdd.Enabled = False
            chkPayrollDelete.Enabled = False
        Else
            chkPayrollAdd.Enabled = True
            chkPayrollDelete.Enabled = True
        End If
    End Sub

    Protected Sub chkUsersAdd_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkUsersAdd.CheckedChanged
        If Not chkUsersAdd.Checked Then
            chkUsersAdd.Checked = False
            chkUsersAddManager.Checked = False
            chkUsersAddManagerFull.Checked = False

            chkUsersAddManager.Enabled = False
            chkUsersAddManagerFull.Enabled = False
        Else
            chkUsersAddManager.Enabled = True
            chkUsersAddManagerFull.Enabled = True
        End If
    End Sub

    Protected Sub chkTimesheetsDisplay_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTimesheetsDisplay.CheckedChanged
        If Not chkTimesheetsDisplay.Checked Then
            chkTimesheetsAdd.Checked = False
            chkTimesheetsDisplay.Checked = False
            chkTimesheetsValidate.Checked = False
            chkTimesheetsEdit.Checked = False
            chkTimesheetsDelete.Checked = False

            chkTimesheetsAdd.Enabled = False
            chkTimesheetsValidate.Enabled = False
            chkTimesheetsEdit.Enabled = False
            chkTimesheetsDelete.Enabled = False
        Else
            chkTimesheetsValidate.Enabled = True
        End If
    End Sub

    Protected Sub chkTimesheetsValidate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTimesheetsValidate.CheckedChanged
        If Not chkTimesheetsValidate.Checked Then
            chkTimesheetsAdd.Checked = False
            chkTimesheetsValidate.Checked = False
            chkTimesheetsEdit.Checked = False
            chkTimesheetsDelete.Checked = False

            chkTimesheetsAdd.Enabled = False
            chkTimesheetsEdit.Enabled = False
            chkTimesheetsDelete.Enabled = False
        Else
            chkTimesheetsAdd.Enabled = True
            chkTimesheetsEdit.Enabled = True
            chkTimesheetsDelete.Enabled = True
        End If
    End Sub

    Protected Sub btnSet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSet.Click
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE Permissions SET AccountHolder = @AccountHolder, TimesheetsAdd = @TimesheetsAdd, TimesheetsDisplay = @TimesheetsDisplay, TimesheetsValidate = @TimesheetsValidate, TimesheetsEdit = @TimesheetsEdit, TimesheetsDelete = @TimesheetsDelete, MileageDisplay = @MileageDisplay, MileageValidate = @MileageValidate, MileageDelete = @MileageDelete, ScheduleEdit = @ScheduleEdit, ScheduleCopy = @ScheduleCopy, ScheduleDelete = @ScheduleDelete, AbsencesAdd = @AbsencesAdd, AbsencesApprove = @AbsencesApprove, AbsencesDelete = @AbsencesDelete, AbsencesSettings = @AbsencesSettings, PayrollAdd = @PayrollAdd, PayrollDelete = @PayrollDelete, PayrollView = @PayrollView, UsersEdit = @UsersEdit, UsersDelete = @UsersDelete, UsersClockGuard = @UsersClockGuard, UsersMobileAccess = @UsersMobileAccess, UsersRecover = @UsersRecover, UsersAdd = @UsersAdd, UsersAddManager = @UsersAddManager, UsersAddManagerFull = @UsersAddManagerFull, UsersEditPermissions = @UsersEditPermissions, UsersEditPermissionsFull = @UsersEditPermissionsFull, UsersCustomFields = @UsersCustomFields, ManageEmployees = @ManageEmployees, SettingsOffices = @SettingsOffices, SettingsGeneral = @SettingsGeneral, SettingsClockPoints = @SettingsClockPoints WHERE User_Id = @User_ID AND Client_Id = @Client_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objCommand.Parameters.AddWithValue("@AccountHolder", chkAccountHolder.Checked)
        objCommand.Parameters.AddWithValue("@TimesheetsAdd", chkTimesheetsAdd.Checked)
        objCommand.Parameters.AddWithValue("@TimesheetsDisplay", chkTimesheetsDisplay.Checked)
        objCommand.Parameters.AddWithValue("@TimesheetsValidate", chkTimesheetsValidate.Checked)
        objCommand.Parameters.AddWithValue("@TimesheetsEdit", chkTimesheetsEdit.Checked)
        objCommand.Parameters.AddWithValue("@TimesheetsDelete", chkTimesheetsDelete.Checked)
        objCommand.Parameters.AddWithValue("@MileageDisplay", chkMileageDisplay.Checked)
        objCommand.Parameters.AddWithValue("@MileageValidate", chkMileageValidate.Checked)
        objCommand.Parameters.AddWithValue("@MileageDelete", chkMileageDelete.Checked)
        objCommand.Parameters.AddWithValue("@ScheduleEdit", chkScheduleEdit.Checked)
        objCommand.Parameters.AddWithValue("@ScheduleCopy", chkScheduleCopy.Checked)
        objCommand.Parameters.AddWithValue("@ScheduleDelete", chkScheduleDelete.Checked)
        objCommand.Parameters.AddWithValue("@AbsencesAdd", chkAbsencesAdd.Checked)
        objCommand.Parameters.AddWithValue("@AbsencesApprove", chkAbsencesApprove.Checked)
        objCommand.Parameters.AddWithValue("@AbsencesDelete", chkAbsencesDelete.Checked)
        objCommand.Parameters.AddWithValue("@AbsencesSettings", chkAbsencesSettings.Checked)
        objCommand.Parameters.AddWithValue("@PayrollAdd", chkPayrollAdd.Checked)
        objCommand.Parameters.AddWithValue("@PayrollDelete", chkPayrollDelete.Checked)
        objCommand.Parameters.AddWithValue("@PayrollView", chkPayrollView.Checked)
        objCommand.Parameters.AddWithValue("@UsersEdit", chkUsersEdit.Checked)
        objCommand.Parameters.AddWithValue("@UsersDelete", chkUsersDelete.Checked)
        objCommand.Parameters.AddWithValue("@UsersClockGuard", chkUsersClockGuard.Checked)
        objCommand.Parameters.AddWithValue("@UsersMobileAccess", chkUsersMobileAccess.Checked)
        objCommand.Parameters.AddWithValue("@UsersRecover", chkUsersRecover.Checked)
        objCommand.Parameters.AddWithValue("@UsersAdd", chkUsersAdd.Checked)
        objCommand.Parameters.AddWithValue("@UsersAddManager", chkUsersAddManager.Checked)
        objCommand.Parameters.AddWithValue("@UsersAddManagerFull", chkUsersAddManagerFull.Checked)
        objCommand.Parameters.AddWithValue("@UsersEditPermissions", chkUsersEditPermissions.Checked)
        objCommand.Parameters.AddWithValue("@UsersEditPermissionsFull", chkUsersEditPermissionsFull.Checked)
        objCommand.Parameters.AddWithValue("@UsersCustomFields", chkUsersCustomFields.Checked)
        objCommand.Parameters.AddWithValue("@ManageEmployees", chkManageEmployees.Checked)
        objCommand.Parameters.AddWithValue("@SettingsOffices", chkSettingsOffices.Checked)
        objCommand.Parameters.AddWithValue("@SettingsGeneral", chkSettingsGeneral.Checked)
        objCommand.Parameters.AddWithValue("@SettingsClockPoints", chkSettingsClockPoints.Checked)

        objCommand.ExecuteNonQuery()

        'if manager no loger managing employees, reassign all employees to account holder.
        If Not chkManageEmployees.Checked Then
            objCommand.CommandText = "EXEC sprocRemovingManagerManageEmployeesAttribute @ClientID, @ManagerID"
            objCommand.Parameters.AddWithValue("@ClientID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@ManagerID", intUserID)

            objCommand.ExecuteNonQuery()
        End If

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing

        LoadValues()

        lblMessage.Text = "<br><br>Permissions successfully updated<br><br>"
        lblMessage.ForeColor = Drawing.Color.Green
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        LoadValues()
    End Sub
End Class
