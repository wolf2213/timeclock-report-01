
Partial Class SettingsTransactionHistory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabSettings As Panel = Master.FindControl("pnlTabSettings")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2TransactionHistory As Panel = Master.FindControl("pnlTab2TransactionHistory")

        pnlMasterTabSettings.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Settings
        pnlMasterTab2TransactionHistory.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT MAX(Transaction_ID) AS RecentID FROM CCTransactions WHERE Client_ID = @Client_ID"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objDR = objCommand.ExecuteReader()

            If objDR.Read Then
                Trace.Write("SettingsTransactionHistory.aspx", "ViewState('Transaction_ID') set to " & objDR("RecentID"))
                ViewState("Transaction_ID") = objDR("RecentID")
            End If

            If ViewState("Transaction_ID") Is DBNull.Value Then
                Trace.Write("SettingsTransactionHistory.aspx", "ViewState('Transaction_ID') set to 0")
                ViewState("Transaction_ID") = 0
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

            CreateTransactionList()
            CreateBillingList()
        End If
    End Sub

    Protected Sub CreateTransactionList()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT TOP 12 * FROM CCTransactions WHERE Client_ID = @Client_ID ORDER BY Submitted DESC"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        objDR = objCommand.ExecuteReader()

        rptTransactionChooser.DataSource = objDR
        rptTransactionChooser.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptTransactionChooser_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim btnDate As LinkButton = e.Item.FindControl("btnDate")
            Dim lblAmount As Label = e.Item.FindControl("lblAmount")

            btnDate.Text = Strings.FormatDateTime(e.Item.DataItem("Submitted"), DateFormat.ShortDate)
            lblAmount.Text = Strings.FormatCurrency(e.Item.DataItem("Amount"))

            btnDate.CommandName = "SelectTransaction"
            btnDate.CommandArgument = e.Item.DataItem("Transaction_ID")
        End If
    End Sub

    Protected Sub rptTransactionChooser_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim intTransactionID As Int64 = e.CommandArgument

        Select Case e.CommandName
            Case "SelectTransaction"
                ViewState("Transaction_ID") = intTransactionID
                CreateBillingList()
        End Select
    End Sub

    Protected Sub CreateBillingList()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        Dim intTransactionID As Int64
        If ViewState("Transaction_ID") > 0 Then
            intTransactionID = ViewState("Transaction_ID")

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT * FROM Billing WHERE Transaction_ID = @Transaction_ID AND Client_ID = @Client_ID"
            objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommand.Parameters.AddWithValue("@Transaction_ID", intTransactionID)
            objDR = objCommand.ExecuteReader()

            rptDetail.DataSource = objDR
            rptDetail.DataBind()

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
    End Sub

    Protected Sub rptDetail_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblDescription As Label = e.Item.FindControl("lblDescription")
            Dim lblPrice As Label = e.Item.FindControl("lblPrice")

            lblDescription.Text = e.Item.DataItem("Description")
            lblPrice.Text = Strings.FormatCurrency(e.Item.DataItem("Price"))

            If e.Item.DataItem("Type") = 0 Then
                'credit
                lblDescription.ForeColor = Drawing.Color.Green
                lblPrice.ForeColor = Drawing.Color.Green
                lblPrice.Text = "(" & lblPrice.Text & ")"
            End If
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblTotal As Label = e.Item.FindControl("lblTotal")
            Dim lblMessage As Label = e.Item.FindControl("lblMessage")

            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT * FROM CCTransactions WHERE Transaction_ID = @Transaction_ID"
            objCommand.Parameters.AddWithValue("@Transaction_ID", ViewState("Transaction_ID"))
            objDR = objCommand.ExecuteReader()

            objDR.Read()

            lblTotal.Text = Strings.FormatCurrency(objDR("Amount"), 2)
            lblTotal.Font.Bold = True

            If objDR("Success") = 0 Then
                lblMessage.ForeColor = System.Drawing.Color.Red
                lblMessage.Text = "Warning: We unsuccessfully attempted to bill the credit card on file on " & Strings.FormatDateTime(objDR("Submitted"), DateFormat.ShortDate) & ".<br />Please update your credit card information in the 'My Subscription' tab to ensure uninterrupted service."
            Else
                lblMessage.forecolor = System.Drawing.Color.Black
                lblMessage.text = "Invoice successfully billed on " & Strings.FormatDateTime(objDR("Submitted"), DateFormat.ShortDate)
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing
        End If
    End Sub
End Class
