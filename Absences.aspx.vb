
Partial Class Absences
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        If Permissions.AbsencesAdd(Session("User_ID")) Then
            Server.Transfer("AbsencesAdd.aspx", False)
        End If

        Dim pnlMasterTabSchedule As Panel = Master.FindControl("pnlTabSchedule")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2RequestAbsence As Panel = Master.FindControl("pnlTab2RequestAbsence")

        pnlMasterTabSchedule.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Schedule
        pnlMasterTab2RequestAbsence.CssClass = "tab2 tab2On"

        If Not IsPostBack Then
            lblDateStart.Text = Time.StripTime(Clock.GetNow())
            lblDateEnd.Text = Time.StripTime(Clock.GetNow())
            ShowRequests()
            CalculateHoursAvailable()
        End If
    End Sub

    Protected Sub ShowRequests()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT * FROM Absences WHERE User_ID = @User_ID ORDER BY StartDate DESC"
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))
        objDR = objCommand.ExecuteReader()

        rptAbsences.DataSource = objDR
        rptAbsences.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptAbsences_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblType As Label = e.Item.FindControl("lblType")
            Dim lblStartDate As Label = e.Item.FindControl("lblStartDate")
            Dim lblEndDate As Label = e.Item.FindControl("lblEndDate")
            Dim lblHours As Label = e.Item.FindControl("lblHours")
            Dim imgApproved As Image = e.Item.FindControl("imgApproved")
            Dim imgNote As Image = e.Item.FindControl("imgNote")
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

            btnDelete.ToolTip = "[offsetx=[-150] header=[Delete Request] body=[]"

            lblType.Text = AbsencesClass.AbsenceNameFromType(e.Item.DataItem("Type"))
            lblStartDate.Text = e.Item.DataItem("StartDate")
            lblEndDate.Text = e.Item.DataItem("EndDate")

            If e.Item.DataItem("Hours") <> 0 Then
                lblHours.Text = e.Item.DataItem("Hours")
            End If

            If e.Item.DataItem("Approved") = 1 Then
                imgApproved.Visible = "True"
                imgApproved.ImageUrl = "~/images/check-green_small.png"
                imgApproved.AlternateText = "This request has been approved by a manager."
                imgApproved.ToolTip = "header=[Absence Approved] body=[]"


                btnDelete.ToolTip = "header=[offsetx=[-150] header=[Delete Absence] body=[]"
            ElseIf e.Item.DataItem("Approved") = 2 Then
                imgApproved.Visible = "True"
                imgApproved.ImageUrl = "~/images/red-x_small.png"
                imgApproved.AlternateText = "This request has been denied by a manager."
                imgApproved.ToolTip = "header=[Absence Denied] body=[]"

                btnDelete.CommandArgument = e.Item.DataItem("Absence_ID")
                btnDelete.CommandName = "Delete"
                btnDelete.Visible = True
            Else
                btnDelete.CommandArgument = e.Item.DataItem("Absence_ID")
                btnDelete.CommandName = "Delete"
                btnDelete.Visible = True
            End If

            If e.Item.DataItem("Note") <> "" Then
                imgNote.AlternateText = "Note: " & e.Item.DataItem("Note")
                imgNote.ImageUrl = "~/images/note.png"
                imgNote.ToolTip = "header=[Note] body=[" & e.Item.DataItem("Note") & "]"
            Else
                imgNote.ImageUrl = "~/images/placeholder.png"
            End If
        End If
    End Sub

    Protected Sub rptAbsences_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim strCommandName As String = e.CommandName
        Dim intAbsenceID As Int64 = e.CommandArgument

        Select Case strCommandName
            Case "Delete"
                Dim objCommand As System.Data.SqlClient.SqlCommand
                Dim objConnection As System.Data.SqlClient.SqlConnection
                objConnection = New System.Data.SqlClient.SqlConnection

                objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
                objConnection.Open()
                objCommand = New System.Data.SqlClient.SqlCommand()
                objCommand.Connection = objConnection
                objCommand.CommandText = "DELETE FROM Absences WHERE Absence_ID = @Absence_ID"
                objCommand.Parameters.AddWithValue("@Absence_ID", intAbsenceID)

                objCommand.ExecuteNonQuery()

                objCommand = Nothing
                objConnection.Close()
                objConnection = Nothing

                ShowRequests()
        End Select
    End Sub

    Protected Sub CalculateHoursAvailable()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT VacationTotal - VacationUsed AS Vacation, HolidayTotal - HolidayUsed AS Holiday, SickDayTotal - SickDayUsed AS SickDay, PersonalTotal - PersonalUsed AS Personal, PaidLeaveTotal - PaidLeaveUsed AS PaidLeave, OtherTotal - OtherUsed AS Other FROM [AbsenceUserSettings] WHERE [User_ID] = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", Session("User_ID"))

        objDR = objCommand.ExecuteReader()
        objDR.Read()


        '       If objDR.HasRows Then
        'lblVacation.Text = System.Math.Round(objDR("Vacation"), 3) & " Hours"
        '       lblHoliday.Text = System.Math.Round(objDR("Holiday"), 3) & " Hours"
        '       lblSickDay.Text = System.Math.Round(objDR("SickDay"), 3) & " Hours"
        '       lblPersonal.Text = System.Math.Round(objDR("Personal"), 3) & " Hours"
        '       lblPaidLeave.Text = System.Math.Round(objDR("PaidLeave"), 3) & " Hours"
        '        lblOther.Text = System.Math.Round(objDR("Other"), 3) & " Hours"
        '       Else
        '       lblVacation.Text = "0 Hours"
        '       lblHoliday.Text = "0 Hours"
        '       lblSickDay.Text = "0 Hours"
        '        lblPersonal.Text = "0 Hours"
        '       lblPaidLeave.Text = "0 Hours"
        '       lblOther.Text = "0 Hours"
        '       End If

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub btnShowHideCalStart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If calAbsenceStart.Visible Then
            calAbsenceStart.Visible = False
            btnShowHideCalStart.ImageUrl = "~/images/arrowdown_small.png"
        Else
            calAbsenceStart.Visible = True
            btnShowHideCalStart.ImageUrl = "~/images/arrowup_small.png"
        End If
    End Sub

    Protected Sub calAbsenceStart_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblDateStart.Text = calAbsenceStart.SelectedDate
        calAbsenceStart.Visible = False
        btnShowHideCalStart.ImageUrl = "~/images/arrowdown_small.png"
    End Sub

    Protected Sub btnShowHideCalEnd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If calAbsenceEnd.Visible Then
            calAbsenceEnd.Visible = False
            btnShowHideCalEnd.ImageUrl = "~/images/arrowdown_small.png"
        Else
            calAbsenceEnd.Visible = True
            btnShowHideCalEnd.ImageUrl = "~/images/arrowup_small.png"
        End If
    End Sub

    Protected Sub calAbsenceEnd_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblDateEnd.Text = calAbsenceEnd.SelectedDate
        calAbsenceEnd.Visible = False
        btnShowHideCalEnd.ImageUrl = "~/images/arrowdown_small.png"
    End Sub

    Protected Sub btnAddRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtStartTime As Date = lblDateStart.Text
        Dim dtEndTime As Date = lblDateEnd.Text
        Dim boolAdd As Boolean = True


        If DateDiff(DateInterval.Day, dtStartTime, dtEndTime) < 0 Then
            boolAdd = False
            lblRequestError.ForeColor = Drawing.Color.Red
            lblRequestError.Text = "<br><br>Start date cannot be after the end date."
            lblRequestError.Visible = True
        End If

        If AbsencesClass.AbsenceAvailable(Session("User_ID"), lblDateStart.Text, lblDateEnd.Text) = False Then
            boolAdd = False
            lblRequestError.ForeColor = Drawing.Color.Red
            lblRequestError.Text = "<br><br>Absences cannot overlap."
            lblRequestError.Visible = True
        End If

        If boolAdd Then
            Dim objCommandAdd As System.Data.SqlClient.SqlCommand
            Dim objConnectionAdd As System.Data.SqlClient.SqlConnection
            objConnectionAdd = New System.Data.SqlClient.SqlConnection

            objConnectionAdd.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnectionAdd.Open()
            objCommandAdd = New System.Data.SqlClient.SqlCommand()
            objCommandAdd.Connection = objConnectionAdd
            objCommandAdd.CommandText = "INSERT INTO Absences (Client_ID, User_ID, Type, StartDate, EndDate, Note) VALUES (@Client_ID, @User_ID, @Type, @StartDate, @EndDate, @Note)"
            objCommandAdd.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
            objCommandAdd.Parameters.AddWithValue("@User_ID", Session("User_ID"))
            objCommandAdd.Parameters.AddWithValue("@Type", drpRequestType.SelectedValue)
            objCommandAdd.Parameters.AddWithValue("@StartDate", lblDateStart.Text)
            objCommandAdd.Parameters.AddWithValue("@EndDate", lblDateEnd.Text)
            objCommandAdd.Parameters.AddWithValue("@Note", txtNote.Text)

            objCommandAdd.ExecuteNonQuery()

            objCommandAdd = Nothing
            objConnectionAdd.Close()
            objConnectionAdd = Nothing

            lblRequestError.ForeColor = Drawing.Color.Green
            lblRequestError.Text = "<br><br>Request successfully submitted."
            lblRequestError.Visible = True

            ShowRequests()

        End If
    End Sub
End Class
