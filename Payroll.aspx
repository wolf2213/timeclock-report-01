<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="Payroll.aspx.vb" Inherits="Payroll" Title="TimeClockWizard - Payroll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
        AsyncPostBackTimeout="600">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlHelpPayroll" CssClass="blueBox" runat="server" Visible="false">
                <span class="subtitle">View Your Payroll</span>
                <br />
                <br />
                Click the
                <img src="images/zoom.png" alt="Details" />
                button to view details<br />
                for a specific payroll.
            </asp:Panel>
            <div style="float: left;">
                <asp:Panel ID="pnlAddPayroll" CssClass="generate-payroll" runat="server">
                    <h4>
                        GENERATE PAYROLL</h4>
                    <div style="padding: 30px 0 0 45px;">
                        <label>
                            Start Date:</label>
                        <asp:TextBox ID="lblDateStart" runat="server" Width="70"></asp:TextBox>
                        <br />
                        <div class="calendar">
                            <asp:Calendar ID="calPayrollStart" runat="server" BackColor="White" BorderColor="#c6ced0"
                                Font-Names="Verdana" ForeColor="Black" Visible="false">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="White" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#c6ced0" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" />
                                <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </div>
                        <div class="clear">
                        </div>
                        <label>
                            End Date:</label>
                        <asp:TextBox ID="lblDateEnd" runat="server" Width="70"></asp:TextBox>
                        <br />
                        <div class="calendar">
                            <asp:Calendar ID="calPayrollEnd" runat="server" BackColor="White" BorderColor="#c6ced0"
                                Font-Names="Verdana" ForeColor="Black" Visible="false">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="White" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#c6ced0" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" />
                                <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </div>
                        <div class="clear">
                        </div>
                        <label>
                            Office:</label>
                        <span class="left" style="float: none;">
                            <div>
                                <asp:DropDownList ID="ddlOffice" runat="server">
                                </asp:DropDownList>
                            </div>
                        </span>
                        <div class="clear">
                        </div>
                        <div style="margin: 20px 0 35px;">
                            <asp:Button ID="btnAddPayroll" runat="server" Text="Generate Payroll" OnClick="btnAddPayroll_Click"
                                CssClass="gen-payroll-btn" />
                        </div>
                        <asp:Label ID="lblError" ForeColor="red" runat="server" Visible="false"></asp:Label>
                    </div>
                </asp:Panel>
            </div>
            <div style="float: left; margin-left: 25px;">
                <asp:Repeater ID="rptPayrollSummary" runat="server" OnItemDataBound="rptPayrollSummary_ItemDataBound"
                    OnItemCommand="rptPayrollSummary_ItemCommand">
                    <HeaderTemplate>
                        <ul class="dates" style="border-left: 1px solid #c6ced0; width:200px;">
                            <li class="column-head column-title">Dates</li>
                        </ul>
                        <ul class="office">
                            <li class="column-head column-title">
                                <asp:Literal ID="litOfficeHeader" runat="server" Text="Office"></asp:Literal></li></ul>
                        <ul class="total-hours">
                            <li class="column-head column-title">Total Hours </li>
                        </ul>
                        <ul class="wage-pay">
                            <li class="column-head column-title">Wage Pay </li>
                        </ul>
                        <ul class="other-pay">
                            <li class="column-head column-title">Other Pay </li>
                        </ul>
                        <ul class="gross-pay">
                            <li class="column-head column-title">Gross Pay </li>
                        </ul>
                        <ul class="buttonlinks" style="border-right: 1px solid #c6ced0;">
                            <li class="column-head column-title">&nbsp; </li>
                        </ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Image ID="imgInvalid" runat="server" ImageUrl="~/images/warning_small.png" AlternateText="This payroll is invalid.  This means that a record in this payroll's date range was either edited, deleted, or added after the payroll was generated.  Delete this payroll and recreate another with the same date range to guarantee accurate data."
                            ToolTip="header=[Payroll Invalidated] body=[A record in this payroll's date range was either edited, deleted, or added after the payroll was generated.  Delete this payroll and recreate another with the same date range to guarantee accurate data.]"
                            ImageAlign="AbsMiddle" Visible="false" />
                        <ul class="dates" style="width:200px;">
                            <li>
                                <asp:Label ID="lblDates" runat="server" Text="Label"></asp:Label></li>
                        </ul>
                        <ul class="office">
                            <li>
                                <asp:Label ID="lblOffice" runat="server" Text=""></asp:Label></li>
                        </ul>
                        <ul class="total-hours">
                            <li>
                                <asp:Label ID="lblTotalHours" runat="server" Text="Label"></asp:Label></li>
                        </ul>
                        <ul class="wage-pay">
                            <li>
                                <asp:Label ID="lblWagePayment" runat="server" Text="Label"></asp:Label></li>
                        </ul>
                        <ul class="other-pay">
                            <li>
                                <asp:Label ID="lblOtherPayment" runat="server" Text="Label"></asp:Label></li>
                        </ul>
                        <ul class="gross-pay">
                            <li>
                                <asp:Label ID="lblTotalPayment" runat="server" Text="Label"></asp:Label></li>
                        </ul>
                        <ul class="buttonlinks">
                            <li>
                                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete_small.png"
                                    AlternateText="Delete Payroll" ToolTip="header=[Delete Payroll] body=[] offsetx=[-150]" />
                                &nbsp;&nbsp;
                                <asp:ImageButton ID="btnDetail" runat="server" ImageUrl="~/images/zoom.png" AlternateText="Payroll Details"
                                    ToolTip="header=[Payroll Details] body=[] offsetx=[-150]" />
                            </li>
                        </ul>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
