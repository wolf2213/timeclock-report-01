Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Settings
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        Dim pnlMasterTabSettings As Panel = Master.FindControl("pnlTabSettings")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2Preferences As Panel = Master.FindControl("pnlTab2Preferences")
        pnlMasterTabSettings.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Settings
        pnlMasterTab2Preferences.CssClass = "tab2 tab2On"

        If Not Permissions.SettingsGeneral(Session("User_ID")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            rbtnlistOverTime.Attributes.Add("onclick", "return GetSelectedItem(this,'" + divDailyhours.ClientID + "','" + divWeeklyhours.ClientID + "')")
            btnDeleteLogo.ToolTip = "header=[Delete Logo] body=[]"
            LoadData()
        End If
    End Sub

    Protected Sub LoadData()
        Dim objsetting As New Setting
        Dim ds As New DataSet
        ds = objsetting.sprocGetPreferences(Convert.ToInt64(Session("Client_ID")))
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("QuickClockIn") = False Then
                    rblQuickClockIn.SelectedValue = 2
                Else
                    If ds.Tables(0).Rows(0)("QuickClockInPassword") Then
                        rblQuickClockIn.SelectedValue = 0
                    Else
                        rblQuickClockIn.SelectedValue = 1
                    End If
                End If
                txtSubdomain.Text = ds.Tables(0).Rows(0)("Subdomain")
                rblSelfRegistration.SelectedValue = ds.Tables(0).Rows(0)("SelfRegistration").ToString()
                rblWhosInRestriction.SelectedValue = ds.Tables(0).Rows(0)("WhosInRestriction").ToString()
                rblBreaks.SelectedValue = ds.Tables(0).Rows(0)("PaidBreaks").ToString()
                rblTimeRounding.SelectedValue = ds.Tables(0).Rows(0)("TimeRounding").ToString()
                txtMileagePay.Text = ds.Tables(0).Rows(0)("MileageRate").ToString()
                txtLoginMessage.Text = Null.Replace(ds.Tables(0).Rows(0)("LoginMessageText"), "")
                chkLoginMessageRed.Checked = ds.Tables(0).Rows(0)("LoginMessageRed")
                chkLoginMessageBold.Checked = ds.Tables(0).Rows(0)("LoginMessageBold")
                txtDashboardMessage.Text = Null.Replace(ds.Tables(0).Rows(0)("DashboardMessageText"), "")
                chkDashboardMessageRed.Checked = ds.Tables(0).Rows(0)("DashboardMessageRed")
                chkDashboardMessageBold.Checked = ds.Tables(0).Rows(0)("DashboardMessageBold")
                rblBirthdayNotification.SelectedValue = ds.Tables(0).Rows(0)("BirthdayNotification").ToString()
                rbtnlistOverTime.SelectedValue = Convert.ToString(ds.Tables(0).Rows(0)("OverTimetype"))
                If rbtnlistOverTime.SelectedValue = 1 Then
                    divDailyhours.Style.Add("display", "block")
                    divWeeklyhours.Style.Add("display", "none")
                    ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(0).Rows(0)("OvertimeDailyHours"))
                    ddlWeeklyHours.SelectedValue = "40"
                ElseIf rbtnlistOverTime.SelectedValue = 2 Then
                    divDailyhours.Style.Add("display", "none")
                    divWeeklyhours.Style.Add("display", "block")
                    ddlDailyHours.SelectedValue = "8"
                    ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(0).Rows(0)("OvertimeWeeklyHours"))
                Else
                    divDailyhours.Style.Add("display", "block")
                    divWeeklyhours.Style.Add("display", "block")
                    ddlDailyHours.SelectedValue = Convert.ToString(ds.Tables(0).Rows(0)("OvertimeDailyHours"))
                    ddlWeeklyHours.SelectedValue = Convert.ToString(ds.Tables(0).Rows(0)("OvertimeWeeklyHours"))
                End If
                chkAproveOverTime.Checked = ds.Tables(0).Rows(0)("ApproveOvertime")
                If Null.Replace(ds.Tables(0).Rows(0)("SelfRegistrationPin"), "nulltest") <> "nulltest" Then
                    txtPin.Text = ds.Tables(0).Rows(0)("SelfRegistrationPin")
                End If
                If ds.Tables(0).Rows(0)("SelfRegistration") Then
                    pnlPin.Visible = True
                End If
                If ds.Tables(0).Rows(0)("MIMEType") <> "0" Then
                    LoadNewImage()
                Else
                    ResetLogoText()
                End If
            End If
        End If
    End Sub

    Protected Sub LoadNewImage()
        imgLogo.Visible = True
        btnDeleteLogo.Visible = True
        imgLogo.ImageUrl = "~/Logo.ashx?id=" & Session("Client_ID")
        lblLogoMessage.Text = "To change your logo, select the new one above and click Upload.<br />Max dimensions 380x180, max size 100KB<br />To stop using a logo, click the 'delete' button."
    End Sub

    Protected Sub ResetLogoText()
        imgLogo.Visible = False
        btnDeleteLogo.Visible = False
        btnUploadLogo.Visible = True
        lblLogoMessage.ForeColor = Drawing.Color.Gray
        lblLogoMessage.Text = "To upload your logo, select it above and click Upload.<br />Max dimensions 380x180, max size 100KB"
    End Sub

    Protected Sub btnSaveAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SaveData(lblAccountMessage)
    End Sub

    Protected Sub btnSaveCompensation_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SaveData(lblCompensationMessage)
    End Sub

    Protected Sub btnSaveMessage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SaveData(lblMessageMessage)
    End Sub

    Protected Sub SaveData(ByVal lblMessage As Label)
        Dim boolValidationError As Boolean = False
        Dim IsSaved As Int64 = 0
        If rblSelfRegistration.SelectedValue = "True" Then
            If txtPin.Text = "" Then
                lblMessage.Text = "<br>In order to enable self registration, you must set the PIN users will have to enter to register.<br /><br />"
                lblMessage.ForeColor = Drawing.Color.Red
                boolValidationError = True
            End If
        End If
        Dim objsetting As New Setting
        If Not boolValidationError Then
            Dim boolQuickClockInAccess As Boolean = False
            Dim boolQuickClockInPassword As Boolean = False
            Dim OverTime As Integer = 0
            Dim OvertimeDailyHours As Double = 0
            Dim OvertimeWeeklyHours As Double = 0
            Select Case rblQuickClockIn.SelectedValue
                Case 0
                    boolQuickClockInAccess = True
                    boolQuickClockInPassword = True
                Case 1
                    boolQuickClockInAccess = True
                    boolQuickClockInPassword = False
                Case 2
                    boolQuickClockInAccess = False
                    boolQuickClockInPassword = True
            End Select
            If rbtnlistOverTime.SelectedValue <> "" Then
                OverTime = rbtnlistOverTime.SelectedValue
            End If
            If OverTime <> 0 Then
                If rbtnlistOverTime.SelectedValue = 1 Then
                    OvertimeDailyHours = (ddlDailyHours.SelectedValue) * 60
                ElseIf rbtnlistOverTime.SelectedValue = 2 Then
                    OvertimeWeeklyHours = (ddlWeeklyHours.SelectedValue) * 60
                Else
                    OvertimeDailyHours = (ddlDailyHours.SelectedValue) * 60
                    OvertimeWeeklyHours = (ddlWeeklyHours.SelectedValue) * 60
                End If
            End If

            IsSaved = objsetting.sprocUpdatePreferences(Convert.ToInt64(Session("Client_ID")), boolQuickClockInAccess, boolQuickClockInPassword, rblSelfRegistration.SelectedValue, txtPin.Text, rblWhosInRestriction.SelectedValue, rblBreaks.SelectedValue, rblTimeRounding.SelectedValue, txtMileagePay.Text, Trim(txtDashboardMessage.Text), Trim(chkDashboardMessageRed.Checked), Trim(chkDashboardMessageBold.Checked), Trim(txtLoginMessage.Text), chkLoginMessageRed.Checked, chkLoginMessageBold.Checked, rblBirthdayNotification.SelectedValue, OverTime, OvertimeDailyHours, OvertimeWeeklyHours, chkAproveOverTime.Checked, txtSubdomain.Text, IsSaved)
            If IsSaved = -1 Then
                lblMessage.Text = "<br>The subdomain you entered is already in use, please choose another.<br /><br />"
                lblMessage.ForeColor = Drawing.Color.Red
            Else
                LoadData()
                Session("WhosInRestriction") = rblWhosInRestriction.SelectedValue
                lblMessage.ForeColor = Drawing.Color.Green
                lblMessage.Text = "<br />Settings update successful"
                timerHideSuccessMessage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub btnUploadLogo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadLogo.Click
        Dim boolImageError As Boolean = False
        Dim extension As String = Path.GetExtension(fuLogo.FileName).ToLower()
        Dim intLogoWidth As Integer
        Dim IsSaved As Integer
        Dim intLogoHeight As Integer
        Dim MIMEType As String = Nothing

        Select Case extension
            Case ".gif"
                MIMEType = "image/gif"
            Case ".jpg", ".jpeg", ".jpe"
                MIMEType = "image/jpeg"
            Case ".png"
                MIMEType = "image/png"
            Case Else
                'Invalid file type uploaded
                lblLogoMessage.Text = "Please only upload PNG, GIF or JPEG images.<br /><br />"
                lblLogoMessage.ForeColor = Drawing.Color.Red
                boolImageError = True
        End Select
        Trace.Write("Settings.aspx", "MIME TYPE = " & MIMEType)
        If Not boolImageError Then
            Dim intLogoSize As Double = fuLogo.PostedFile.InputStream.Length
            If intLogoSize < 102400 Then
                Dim bitmapLogo As New System.Drawing.Bitmap(fuLogo.PostedFile.InputStream)
                If bitmapLogo.Width <= 380 And bitmapLogo.Height <= 180 Then
                    'Dimensions are OK
                    intLogoWidth = bitmapLogo.Width
                    intLogoHeight = bitmapLogo.Height
                Else
                    lblLogoMessage.Text = "The image you uploaded is too large.<br />Max width: 380, Max height: 180<br /><br />"
                    lblLogoMessage.ForeColor = Drawing.Color.Red
                    boolImageError = True
                End If
                bitmapLogo.Dispose()
            Else
                lblLogoMessage.Text = "Please upload an image that is smaller than 100KB.<br /><br />"
                lblLogoMessage.ForeColor = Drawing.Color.Red
                boolImageError = True
            End If
        End If

        If Not boolImageError Then
            'Connect to the database and insert a new record into Products
            Trace.Write("Settings.aspx", "No error, uploading")
            Dim objsetting As New Setting
            Dim imageBytes(fuLogo.PostedFile.InputStream.Length) As Byte
            fuLogo.PostedFile.InputStream.Position = 0
            fuLogo.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
            IsSaved = objsetting.sprocUpdatePreferencesLogo(Session("Client_ID"), imageBytes, intLogoWidth, intLogoHeight, MIMEType, IsSaved)
            Trace.Write("Settings.aspx", "New Image Uploaded")
            LoadNewImage()
        End If
    End Sub

    Protected Sub btnDeleteLogo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim IsSaved As Integer
        Dim objsetting As New Setting
        IsSaved = objsetting.sprocUpdatePreferencesLogo(Session("Client_ID"), Nothing, 0, 0, 0, IsSaved)
        ResetLogoText()
    End Sub

    Protected Sub timerHideSuccessMessage_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timerHideSuccessMessage.Tick
        lblAccountMessage.Text = ""
        lblCompensationMessage.Text = ""
        lblMessageMessage.Text = ""
        timerHideSuccessMessage.Enabled = False
    End Sub

    Protected Sub rblSelfRegistration_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rblSelfRegistration.SelectedValue = "True" Then
            pnlPin.Visible = True
        Else
            pnlPin.Visible = False
        End If
    End Sub

    'Protected Sub rbtnlistOverTime_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rbtnlistOverTime.SelectedIndexChanged
    '    Try
    '        If rbtnlistOverTime.SelectedValue = 1 Then
    '            divDailyhours.Visible = True
    '            divWeeklyhours.Visible = False
    '        ElseIf rbtnlistOverTime.SelectedValue = 2 Then
    '            divWeeklyhours.Visible = True
    '            divDailyhours.Visible = False
    '        Else
    '            divWeeklyhours.Visible = True
    '            divDailyhours.Visible = True
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
End Class
