<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Mileage.aspx.vb" Inherits="Mileage" title="TimeClockWizard - Mileage" %>
<%@ Register TagPrefix="cii" TagName="UserList" Src="~/UserList.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div class="main-content" style="width:1050px;">
 <div style="margin-left: 25px; float: left;">       
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

								

                        <asp:Panel ID="pnlInputMileage" runat="server" DefaultButton="btnAddMileage">
<div style="width: 445px; text-align: center; background: #004a71; padding: 15px 0; margin-bottom: 20px;">
									<h4 style="padding: 12px 0;">INPUT MILEAGE</h4>

    <div><label style="color: #fff">Date</label>
                                
                            
 <asp:Textbox ID="lblDate" runat="server" Width="70px"></asp:Textbox>                           
                            
 </div>


<center>
<asp:Calendar ID="calMileage" runat="server" BackColor="White" BorderColor="#c6ced0"
                                Font-Names="Verdana" ForeColor="Black" Visible="false">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="White" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#c6ced0" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True"/>
                                <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
</center>




                            <br />
                            <label style="color: #fff">Miles</label>
											<span class="left" style="float: none;">
												<div class="right">
                            <asp:TextBox ID="txtMileage" runat="server" Width="50px"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="btnAddMileage" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/images/mileage_add.png" OnClick="btnAddMileage_Click" ToolTip="header=[Add Mileage] body=[]"/>
                                                    </div>
											</span>
                            <br />
                            <asp:RangeValidator ID="valMileage" runat="server" ControlToValidate="txtMileage"
                                Display="Dynamic" ErrorMessage="Invalid mileage" MaximumValue="999" MinimumValue="1"
                                SetFocusOnError="True"></asp:RangeValidator>
                            <asp:UpdateProgress ID="prgMileage" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    Processing...
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                           <p class="notice"> <asp:Label ID="lblSuccess" runat="server" ForeColor="White" Visible="false" Text="Mileage was added successfully"></asp:Label></p>

    </div>

                        </asp:Panel>

    
                    </ContentTemplate>
                </asp:UpdatePanel>


                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div style="width: 445px;">
									<h4>DISPLAY MILEAGE</h4>
                           <div style="border: 1px solid #c6ced0; text-align: center; padding-top: 30px;">
										<p style="font-size: 14px; margin-bottom:10px"><span style="font-weight: bold;">Week</span>
                           <asp:Label ID="lblStartDate" runat="server" Text="Date"></asp:Label>
                           -
                           <asp:Label ID="lblEndDate" runat="server" Text="Date"></asp:Label>

<asp:TextBox ID="weekpicker" Text="" runat="server" ClientIDMode="Static" Width="15px" OnTextChanged="weekpicker_SelectionChanged" AutoPostBack="True" CssClass="search1" ForeColor="White" >
          </asp:TextBox>


</p>
<center>
<asp:ImageButton ID="btnShowHideCalDisp" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/images/arrowdown_small.png" />




<asp:Calendar ID="calMileageDisp" runat="server" BackColor="White" BorderColor="#c6ced0"
                                Font-Names="Verdana" ForeColor="Black" Visible="false">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="White" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#c6ced0" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True"/>
                                <TitleStyle BackColor="#c6ced0" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
</center>

                           <br />
                           <p style="font-size: 12px; margin-bottom:20px"><asp:Label ID="lblUserFullName" runat="server" Text=""></asp:Label>
                               </p>
									</div>
								
                           <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                                <ProgressTemplate>
                                    Processing...
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            </div>
								
                    </ContentTemplate>
                </asp:UpdatePanel>
</div> <!-- end of first row -->
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <div style="float: left; margin-left: 20px; width: 500px;">
									<div>


                        <asp:Repeater ID="rptUsers" runat="server" OnItemDataBound="rptUsers_ItemDataBound" EnableViewState="true">
                            <ItemTemplate>
                                <asp:Label ID="lblNoMileage" runat="server" Text="&nbsp;&nbsp;-No Mileage" BackColor="#DDDDDD" Width="272px"></asp:Label>
                                <asp:Literal ID="litNoMileageBRs" runat="server" Text="<br />"></asp:Literal>

                                <asp:Repeater ID="rptMileage" runat="server" OnItemDataBound="rptMileage_ItemDataBound" OnItemCommand="rptMileage_ItemCommand" EnableViewState="true">
                                    <HeaderTemplate>
                                        <ul class="mile-date" style="width: 120px;">
											<li class="column-head column-title">date</li>
                                        </ul>

                                         <ul class="miles" style="width: 120px;">
											<li class="column-head column-title">miles</li>
                                         </ul>

                                         <ul class="verified" style="width: 120px;">
											<li class="column-head column-title">verified</li>
                                         </ul>

                                         <ul class="xclose" style="width: 120px;">
											<li class="column-head column-title"></li>
                                         </ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                            <ul class="mile-date" style="width: 120px;">
											<li><asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label></li>
                                                </ul>
                                        <ul class="miles" style="width: 120px;">
											<li><%#Container.DataItem("Miles")%></li>
                                         </ul>

                                            <ul class="verified" style="width: 120px;">
											<li><asp:ImageButton ID="btnVerify" runat="server" ImageUrl="~/images/timesheet_unverified.png" ImageAlign="AbsMiddle" AlternateText="Unverified" Enabled="false" /></li>
                                         </ul>
                                           
                                        <ul class="xclose" style="width: 120px;">
											<li><asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete_small.png" AlternateText="Delete" CssClass="smallButton" CommandName="Delete" CommandArgument='<%#Container.DataItem("Mileage_ID")%>' /></li>
                                         </ul>

                                            
                                    </ItemTemplate>
          
                                    <FooterTemplate>
                                           Total Miles: <%#TotalMiles()%>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </ItemTemplate>
                            <SeparatorTemplate>
                            <br />
                            </SeparatorTemplate>
                        </asp:Repeater>
                    </ContentTemplate>
                </asp:UpdatePanel>
       </div>
</asp:Content>