Partial Class ForgotPassword
    Inherits System.Web.UI.Page

    Dim strPassword
    Dim intUserID

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        '       Dim lblMasterClientName As Label = Master.FindControl("lblClientName")
        '        Dim lblMasterUserName As Label = Master.FindControl("lblUserName")

        '       lblMasterUserName.Text = ""

        strPassword = Replace(Server.UrlDecode(Request.QueryString("key")), " ", "+")
        intUserID = Request.QueryString("uid")

        Try
            Dim objDR As System.Data.SqlClient.SqlDataReader
            Dim objCommand As System.Data.SqlClient.SqlCommand
            Dim objConnection As System.Data.SqlClient.SqlConnection
            objConnection = New System.Data.SqlClient.SqlConnection

            objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objConnection.Open()
            objCommand = New System.Data.SqlClient.SqlCommand()
            objCommand.Connection = objConnection
            objCommand.CommandText = "SELECT Username FROM Users WHERE User_ID = @User_ID AND Password = @Password"
            objCommand.Parameters.AddWithValue("@User_ID", intUserID)
            objCommand.Parameters.AddWithValue("@Password", strPassword)

            objDR = objCommand.ExecuteReader()

            If objDR.HasRows Then
                objDR.Read()
                litUsername.Text = objDR("Username")
            Else
                pnlChange.Visible = False
                pnlError.Visible = True
                litInitialError.Text = "Error: User not found (key mismatch).<br>User_ID:" & intUserID & "<br>Key:" & strPassword
            End If

            objDR = Nothing
            objCommand = Nothing
            objConnection.Close()
            objConnection = Nothing

        Catch ex As Exception
            pnlChange.Visible = False
            pnlError.Visible = True
            litInitialError.Text = "Error: General Error."
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim strRedirectionUrl As String
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "UPDATE Users SET Password = @Password WHERE User_ID = @User_ID"
        objCommand.Parameters.AddWithValue("@User_ID", intUserID)
        objCommand.Parameters.AddWithValue("@Password", Crypto.ComputeMD5Hash(txtPassword.Text))
        objCommand.ExecuteNonQuery()

        objCommand.CommandText = "SELECT C.subdomain FROM Clients C inner join Users U on C.client_id = U.client_id WHERE C.AcctDeleted = 0 and U.User_ID = @User_ID"
        objDR = objCommand.ExecuteReader()

        If objDR.HasRows Then
            objDR.Read()
            strRedirectionUrl = "http://" + objDR("subdomain") + ".TimeClockWizard.com"
        Else
            strRedirectionUrl = "Login.aspx"
        End If

        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
        objDR = Nothing

        Response.Redirect(strRedirectionUrl)
    End Sub
End Class
