
Partial Class WhosIn
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Security.CheckPermission(Session("User_ID"), Session("Manager")) Then
            Response.Redirect("LoggedOut.aspx")
        End If

        If Session("WhosInRestriction") And Not Session("Manager") Then
            Response.Redirect("Default.aspx")
        End If

        Dim pnlMasterTabHome As Panel = Master.FindControl("pnlTabHome")
        Dim MasterMultiView1 As MultiView = Master.FindControl("MultiView1")
        Dim pnlMasterTab2WhosIn As Panel = Master.FindControl("pnlTab2WhosIn")

        pnlMasterTabHome.CssClass = "tab tabOn"
        MasterMultiView1.ActiveViewIndex = Constants.Tabs.Timesheet
        pnlMasterTab2WhosIn.CssClass = "tab2 tab2On"

        GetWhosInRecords()
    End Sub

    Protected Sub GetWhosInRecords()
        Dim objDR As System.Data.SqlClient.SqlDataReader
        Dim objCommand As System.Data.SqlClient.SqlCommand
        Dim objConnection As System.Data.SqlClient.SqlConnection
        objConnection = New System.Data.SqlClient.SqlConnection

        objConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
        objConnection.Open()
        objCommand = New System.Data.SqlClient.SqlCommand()
        objCommand.Connection = objConnection
        objCommand.CommandText = "SELECT (Users.LastName + ', ' + Users.FirstName) AS FullName, TimeRecords.User_ID, TimeRecords.StartTime, TimeRecords.StartLocation, TimeRecords.StartLocation_ID, TimeRecords.EndTime, TimeRecords.EndLocation, TimeRecords.EndLocation_ID FROM TimeRecords, Users WHERE Users.User_ID = TimeRecords.User_ID AND TimeRecords.Client_ID = @Client_ID AND TimeRecords.EndTime IS NULL AND Type = 1 ORDER BY LastName ASC"
        objCommand.Parameters.AddWithValue("@Client_ID", Session("Client_ID"))
        'objCommand.Parameters.AddWithValue("@OrderCol", "LastName")

        objDR = objCommand.ExecuteReader()

        rptWhosIn.DataSource = objDR
        rptWhosIn.DataBind()

        objDR = Nothing
        objCommand = Nothing
        objConnection.Close()
        objConnection = Nothing
    End Sub

    Protected Sub rptWhosIn_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblName As Label = e.Item.FindControl("lblName")
            Dim lblScheduledTimeIn As Label = e.Item.FindControl("lblScheduledTimeIn")
            Dim lblTimeIn As Label = e.Item.FindControl("lblTimeIn")
            Dim lblStatus As Label = e.Item.FindControl("lblStatus")
            Dim imgStartLocation As Image = e.Item.FindControl("imgStartLocation")
            Dim lblStartLocation As Label = e.Item.FindControl("lblStartLocation")
            Dim lblDuration As Label = e.Item.FindControl("lblDuration")


            lblName.Text = e.Item.DataItem("FullName")

            Dim objScheduleDR As System.Data.SqlClient.SqlDataReader
            Dim objScheduleCommand As System.Data.SqlClient.SqlCommand
            Dim objScheduleConnection As System.Data.SqlClient.SqlConnection
            objScheduleConnection = New System.Data.SqlClient.SqlConnection

            Dim CurrentDayCode As String = WeekdayName(DatePart(DateInterval.Weekday, (e.Item.DataItem("StartTime"))), True) & "Code"
            Dim CurrentDayString As String = WeekdayName(DatePart(DateInterval.Weekday, (e.Item.DataItem("StartTime"))), True) & "Start"
            Dim CurrentDayEnd As String = WeekdayName(DatePart(DateInterval.Weekday, (e.Item.DataItem("StartTime"))), True) & "End"

            objScheduleConnection.ConnectionString = ConfigurationManager.ConnectionStrings("default").ToString
            objScheduleConnection.Open()
            objScheduleCommand = New System.Data.SqlClient.SqlCommand()
            objScheduleCommand.Connection = objScheduleConnection
            objScheduleCommand.CommandText = "SELECT " & CurrentDayCode & " AS sqlCurrentDayCode, " & CurrentDayString & " AS sqlCurrentDayStart, " & CurrentDayEnd & " AS sqlCurrentDayEnd FROM Schedules WHERE User_ID = @User_ID AND [Year] = @Year AND WeekNum = @WeekNum"
            objScheduleCommand.Parameters.AddWithValue("@User_ID", e.Item.DataItem("User_ID"))
            objScheduleCommand.Parameters.AddWithValue("@Year", Year(e.Item.DataItem("StartTime")))
            objScheduleCommand.Parameters.AddWithValue("@WeekNum", DatePart(DateInterval.WeekOfYear, e.Item.DataItem("StartTime")))

            objScheduleDR = objScheduleCommand.ExecuteReader()

            If objScheduleDR.HasRows Then
                objScheduleDR.Read()
                If objScheduleDR("sqlCurrentDayCode") Is System.DBNull.Value Then
                    lblScheduledTimeIn.Text = "Not Scheduled"
                ElseIf Not IsNumeric(objScheduleDR("sqlCurrentDayCode")) Then
                    If (objScheduleDR("sqlCurrentDayCode")) = "NW" Then
                        lblScheduledTimeIn.Text = "Not Scheduled"
                    Else
                        lblScheduledTimeIn.Text = AbsencesClass.AbsenceNameFromCode(objScheduleDR("sqlCurrentDayCode"))
                    End If
                ElseIf IsNumeric(objScheduleDR("sqlCurrentDayCode")) Then
                    If CInt(objScheduleDR("sqlCurrentDayCode")) <> 0 Then
                        lblScheduledTimeIn.Text = Time.QuickStrip(objScheduleDR("sqlCurrentDayStart")) & " - " & Time.QuickStrip(objScheduleDR("sqlCurrentDayEnd")) & " at " & Offices.OfficeName(objScheduleDR("sqlCurrentDayCode"))
                    End If
                Else
                    lblScheduledTimeIn.Text = Time.QuickStrip(objScheduleDR("sqlCurrentDayStart")) & " - " & Time.QuickStrip(objScheduleDR("sqlCurrentDayEnd"))
                End If
            Else
                lblScheduledTimeIn.Text = "Not Scheduled"
            End If

            objScheduleDR = Nothing
            objScheduleCommand = Nothing
            objScheduleConnection.Close()
            objScheduleConnection = Nothing

            If Time.StripTime(e.Item.DataItem("StartTime")) = Time.StripTime(Clock.GetNow()) Then
                lblTimeIn.Text = Time.QuickStrip(e.Item.DataItem("StartTime"))
            Else
                lblTimeIn.Text = Time.QuickStrip(e.Item.DataItem("StartTime")) & " (" & Time.StripTime(e.Item.DataItem("StartTime")) & ")"
            End If

            Select Case UserInfo.UserStatus(e.Item.DataItem("User_ID"))
                Case 2
                    lblStatus.ForeColor = Drawing.Color.Green
                    lblStatus.Text = "Working"
                Case 3
                    lblStatus.ForeColor = Drawing.Color.Blue
                    lblStatus.Text = "On Break"
            End Select

            lblStartLocation.Text = e.Item.DataItem("StartLocation")

            If e.Item.DataItem("StartLocation_ID") = 0 Then
                'unverified IP
                imgStartLocation.ImageUrl = "~/images/timesheet_computer0.png"
                lblStartLocation.Text = e.Item.DataItem("StartLocation")
            ElseIf e.Item.DataItem("StartLocation_ID") = 99999999 Then
                'unverified Phone
                lblStartLocation.Text = e.Item.DataItem("StartLocation")
                imgStartLocation.ImageUrl = "~/images/timesheet_phone0.png"

            Else
                If Locations.LocationType(e.Item.DataItem("StartLocation_ID")) = 1 Or Locations.LocationType(e.Item.DataItem("StartLocation_ID")) = 2 Then
                    'verified ip
                    imgStartLocation.ImageUrl = "~/images/timesheet_computer.png"
                    lblStartLocation.Text = Locations.LocationName(e.Item.DataItem("StartLocation_ID"))
                Else
                    'verified phone
                    imgStartLocation.ImageUrl = "~/images/timesheet_phone.png"
                    lblStartLocation.Text = Locations.LocationName(e.Item.DataItem("StartLocation_ID"))
                End If
            End If

            lblDuration.Text = TimeRecords.FormatMinutes(TimeRecords.TimeDifference(e.Item.DataItem("StartTime").ToString, e.Item.DataItem("EndTime").ToString), Session("User_ID"), True)
        End If
    End Sub
End Class
