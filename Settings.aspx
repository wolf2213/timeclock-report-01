<%@ Page Language="VB" Trace="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    ValidateRequest="false" CodeFile="Settings.aspx.vb" Inherits="Settings" Title="TimeClockWizard - General Preferences" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="js/Common.js" type="text/javascript"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div class="main-content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="box1-info" style="width: 1000px;">
                    <h4>
                        Account Settings</h4>
                    <div class="form-holder" style="width: 950px; margin-bottom: 20px;">
                        <h5>
                            Subdomain</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <span>Access TimeClockWizard by going to http://</span> <span class="left" style="float: none;">
                                <div class="right">
                                    <asp:TextBox Width="75px" ID="txtSubdomain" runat="server"></asp:TextBox>
                                </div>
                            </span><span>.timeclockwizard.com</span>
                        </div>
                        <br />
                        <h5>
                            Quick Clock In/Out</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <div class="radio-btn">
                                <asp:RadioButtonList ID="rblQuickClockIn" runat="server">
                                    <asp:ListItem Value="0" Text="Enabled, users can quickly clock in or out from the login screen without having to access the full application"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Enabled, users can quickly clock in or out from the login screen and they only have to enter their username"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Disabled, force users to clock in or out through the application dashboard"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <br />
                        <h5>
                            Self Registration</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <div class="radio-btn">
                                <asp:RadioButtonList ID="rblSelfRegistration" runat="server" OnSelectedIndexChanged="rblSelfRegistration_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem Value="False" Text="Disabled, new users can only be added by managers with the necessary permissions"></asp:ListItem>
                                    <asp:ListItem Value="True" Text="Enabled, new users can add themselves by clicking a link on the login page"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <asp:Panel ID="pnlPin" Visible="false" runat="server">
                            &nbsp;&nbsp;&nbsp;Pin:
                            <asp:TextBox ID="txtPin" Width="60" runat="server"></asp:TextBox>
                            Users must enter this PIN in order to register themselves
                        </asp:Panel>
                        <br />
                        <h5>
                            Who's In Restriction</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <div class="radio-btn">
                                <asp:RadioButtonList ID="rblWhosInRestriction" runat="server">
                                    <asp:ListItem Value="False" Text="Disabled, all users can view the Who's In page"></asp:ListItem>
                                    <asp:ListItem Value="True" Text="Enabled, only Managers can view the Who's In page"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <br />
                        <h5>
                            Logo Upload</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <asp:Image ID="imgLogo" runat="server" />
                            <br />
                            <asp:FileUpload ID="fuLogo" runat="server" />&nbsp;&nbsp;<asp:Button ID="btnUploadLogo"
                                runat="server" Text="Upload" Height="22px" />&nbsp;&nbsp;<asp:ImageButton ID="btnDeleteLogo"
                                    ImageAlign="AbsMiddle" ImageUrl="~/images/delete_small.png" AlternateText="Delete logo"
                                    runat="server" OnClick="btnDeleteLogo_Click" />
                            <br />
                            <asp:Label ID="lblLogoMessage" ForeColor="gray" runat="server"></asp:Label>
                        </div>
                        <div>
                            <asp:Button ID="btnSaveAccount" runat="server" Text="Save All Settings" OnClick="btnSaveAccount_Click"
                                CssClass="save-all-setting" />
                            <asp:Label ID="lblAccountMessage" ForeColor="green" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID = "btnUploadLogo" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="box2-info" style="width: 1000px;">
                    <h4>
                        Employee Compensation</h4>
                    <div class="form-holder" style="width: 950px; margin-bottom: 20px;">
                        <h5>
                            Paid Breaks</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <div class="radio-btn">
                                <asp:RadioButtonList ID="rblBreaks" runat="server">
                                    <asp:ListItem Value="True" Text="Yes, pay employees for breaks"></asp:ListItem>
                                    <asp:ListItem Value="False" Text="No, do not count future break time in payroll"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <br />
                        <h5>
                            Time Rounding</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <div class="radio-btn">
                                <asp:RadioButtonList ID="rblTimeRounding" runat="server">
                                    <asp:ListItem Value="0" Text="Disabled, use exact time"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Enabled, round all times to the nearest 1/4 hour (15 minute rule)"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Enabled, round all times to the nearest 1/10 hour (6 minute rule)"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <br />
                        <h5>
                            OverTime</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <div class="radio-btn">
                                <asp:RadioButtonList ID="rbtnlistOverTime" runat="server">
                                    <asp:ListItem Value="1" Text="Daily Overtime Pay"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Weekly Overtime Pay"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Both (Daily and Weekly)"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div id="divDailyhours" runat="server">
                                &nbsp;&nbsp;&nbsp;&nbsp; <span style="color: #454545; display: inline-block; font-family: arial;
                                    font-size: 14px; font-weight: bold; line-height: 27px; text-transform: capitalize;">
                                    Daily overtime will be applied after</span>
                                <asp:DropDownList ID="ddlDailyHours" runat="server">
                                    <asp:ListItem Value="0" Text="0"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                    <asp:ListItem Selected="True" Value="8" Text="8"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                    <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                    <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                    <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                    <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                    <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                    <asp:ListItem Value="18" Text="18"></asp:ListItem>
                                    <asp:ListItem Value="19" Text="19"></asp:ListItem>
                                    <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                    <asp:ListItem Value="21" Text="21"></asp:ListItem>
                                    <asp:ListItem Value="22" Text="22"></asp:ListItem>
                                    <asp:ListItem Value="23" Text="23"></asp:ListItem>
                                    <asp:ListItem Value="24" Text="24"></asp:ListItem>
                                </asp:DropDownList>
                                <span style="color: #454545; display: inline-block; font-family: arial; font-size: 14px;
                                    font-weight: bold; line-height: 27px; text-transform: capitalize;">hours</span>
                            </div>
                            <div id="divWeeklyhours" runat="server">
                                &nbsp;&nbsp;&nbsp;&nbsp; <span style="color: #454545; display: inline-block; font-family: arial;
                                    font-size: 14px; font-weight: bold; line-height: 27px; text-transform: capitalize;">
                                    Weekly overtime will be applied after</span>
                                <asp:DropDownList ID="ddlWeeklyHours" runat="server">
                                    <asp:ListItem Value="0" Text="0"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                    <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                    <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                    <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                    <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                    <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                    <asp:ListItem Value="18" Text="18"></asp:ListItem>
                                    <asp:ListItem Value="19" Text="19"></asp:ListItem>
                                    <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                    <asp:ListItem Value="21" Text="21"></asp:ListItem>
                                    <asp:ListItem Value="22" Text="22"></asp:ListItem>
                                    <asp:ListItem Value="23" Text="23"></asp:ListItem>
                                    <asp:ListItem Value="24" Text="24"></asp:ListItem>
                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>
                                    <asp:ListItem Value="26" Text="26"></asp:ListItem>
                                    <asp:ListItem Value="27" Text="27"></asp:ListItem>
                                    <asp:ListItem Value="28" Text="28"></asp:ListItem>
                                    <asp:ListItem Value="29" Text="29"></asp:ListItem>
                                    <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                    <asp:ListItem Value="31" Text="31"></asp:ListItem>
                                    <asp:ListItem Value="32" Text="32"></asp:ListItem>
                                    <asp:ListItem Value="33" Text="33"></asp:ListItem>
                                    <asp:ListItem Value="34" Text="34"></asp:ListItem>
                                    <asp:ListItem Value="35" Text="35"></asp:ListItem>
                                    <asp:ListItem Value="36" Text="36"></asp:ListItem>
                                    <asp:ListItem Value="37" Text="37"></asp:ListItem>
                                    <asp:ListItem Value="38" Text="38"></asp:ListItem>
                                    <asp:ListItem Value="39" Text="39"></asp:ListItem>
                                    <asp:ListItem Selected="True" Value="40" Text="40"></asp:ListItem>
                                    <asp:ListItem Value="41" Text="41"></asp:ListItem>
                                    <asp:ListItem Value="42" Text="42"></asp:ListItem>
                                    <asp:ListItem Value="43" Text="43"></asp:ListItem>
                                    <asp:ListItem Value="44" Text="44"></asp:ListItem>
                                    <asp:ListItem Value="45" Text="45"></asp:ListItem>
                                    <asp:ListItem Value="46" Text="46"></asp:ListItem>
                                    <asp:ListItem Value="47" Text="47"></asp:ListItem>
                                    <asp:ListItem Value="48" Text="48"></asp:ListItem>
                                    <asp:ListItem Value="48" Text="48"></asp:ListItem>
                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                    <asp:ListItem Value="51" Text="51"></asp:ListItem>
                                    <asp:ListItem Value="52" Text="52"></asp:ListItem>
                                    <asp:ListItem Value="53" Text="53"></asp:ListItem>
                                    <asp:ListItem Value="54" Text="54"></asp:ListItem>
                                    <asp:ListItem Value="55" Text="55"></asp:ListItem>
                                    <asp:ListItem Value="56" Text="56"></asp:ListItem>
                                    <asp:ListItem Value="57" Text="57"></asp:ListItem>
                                    <asp:ListItem Value="58" Text="58"></asp:ListItem>
                                    <asp:ListItem Value="59" Text="59"></asp:ListItem>
                                    <asp:ListItem Value="60" Text="60"></asp:ListItem>
                                    <asp:ListItem Value="61" Text="61"></asp:ListItem>
                                    <asp:ListItem Value="62" Text="62"></asp:ListItem>
                                    <asp:ListItem Value="63" Text="63"></asp:ListItem>
                                    <asp:ListItem Value="64" Text="64"></asp:ListItem>
                                    <asp:ListItem Value="65" Text="65"></asp:ListItem>
                                    <asp:ListItem Value="66" Text="66"></asp:ListItem>
                                    <asp:ListItem Value="67" Text="67"></asp:ListItem>
                                    <asp:ListItem Value="68" Text="68"></asp:ListItem>
                                    <asp:ListItem Value="69" Text="69"></asp:ListItem>
                                    <asp:ListItem Value="70" Text="70"></asp:ListItem>
                                    <asp:ListItem Value="71" Text="71"></asp:ListItem>
                                    <asp:ListItem Value="72" Text="72"></asp:ListItem>
                                    <asp:ListItem Value="73" Text="73"></asp:ListItem>
                                    <asp:ListItem Value="74" Text="74"></asp:ListItem>
                                    <asp:ListItem Value="75" Text="75"></asp:ListItem>
                                    <asp:ListItem Value="76" Text="76"></asp:ListItem>
                                    <asp:ListItem Value="77" Text="77"></asp:ListItem>
                                    <asp:ListItem Value="78" Text="78"></asp:ListItem>
                                    <asp:ListItem Value="79" Text="79"></asp:ListItem>
                                    <asp:ListItem Value="80" Text="80"></asp:ListItem>
                                </asp:DropDownList>
                                <span style="color: #454545; display: inline-block; font-family: arial; font-size: 14px;
                                    font-weight: bold; line-height: 27px; text-transform: capitalize;">hours</span>
                            </div>
                            <br />
                            <div>
                                <span style="color: #454545; display: inline-block; font-family: arial; font-size: 14px;
                                    font-weight: bold; line-height: 27px; text-transform: capitalize;">
                                    <asp:CheckBox ID="chkAproveOverTime" runat="server" Text="Approve OverTime" />
                                </span>
                            </div>
                        </div>
                        <br />
                        <h5>
                            Mileage</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <span>Pay</span> <span class="left" style="float: none;">
                                <div class="right">
                                    <asp:TextBox ID="txtMileagePay" Width="30" MaxLength="5" runat="server"></asp:TextBox>
                                </div>
                            </span><span>cents per mile</span>
                        </div>
                        <div>
                            <asp:Button ID="btnSaveCompensation" runat="server" Text="Save All Settings" OnClick="btnSaveCompensation_Click"
                                CssClass="save-all-setting" />
                            <asp:Label ID="lblCompensationMessage" ForeColor="green" runat="server"></asp:Label></div>
                    </div>
                    <!-- end of formholder -->
                    <h4>
                        MESSAGE & ALERTS</h4>
                    <div class="form-holder" style="width: 950px;">
                        <h5>
                            Login Message</h5>
                        <asp:TextBox ID="txtLoginMessage" TextMode="MultiLine" Width="400" Height="60" runat="server"></asp:TextBox>
                        <br />
                        <small>HTML tags are allowed</small>
                        <br />
                        <asp:CheckBox ID="chkLoginMessageRed" runat="server" ForeColor="Red" Text="Red text" />&nbsp;&nbsp;<asp:CheckBox
                            ID="chkLoginMessageBold" runat="server" Font-Bold="true" Text="Bold text" />
                        <br />
                        <br />
                        <h5>
                            Dashboard Message</h5>
                        <asp:TextBox ID="txtDashboardMessage" TextMode="MultiLine" Width="400" Height="60"
                            runat="server"></asp:TextBox>
                        <br />
                        <small>HTML tags are allowed</small>
                        <br />
                        <asp:CheckBox ID="chkDashboardMessageRed" runat="server" ForeColor="Red" Text="Red text" />&nbsp;&nbsp;<asp:CheckBox
                            ID="chkDashboardMessageBold" runat="server" Font-Bold="true" Text="Bold text" />
                        <br />
                        <br />
                        <h5>
                            Birthday Notification</h5>
                        <div style="font-family: arial; font-size: 14px; color: #454545; margin-top: 15px;">
                            <div class="radio-btn">
                                <asp:RadioButtonList ID="rblBirthdayNotification" runat="server">
                                    <asp:ListItem Value="True" Text="Enabled, alert users when another user's birthday is approaching on the dashboard"></asp:ListItem>
                                    <asp:ListItem Value="False" Text="Disabled, do not notfiy users of upcoming birthdays"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div>
                            <asp:Button ID="btnSaveMessage" runat="server" Text="Save All Settings" OnClick="btnSaveMessage_Click"
                                CssClass="save-all-setting" />
                            <asp:Label ID="lblMessageMessage" ForeColor="green" runat="server"></asp:Label></div>
                    </div>
                    <asp:Timer ID="timerHideSuccessMessage" runat="server" Interval="2000" Enabled="false">
                    </asp:Timer>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
